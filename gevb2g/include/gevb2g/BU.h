#ifndef _gevb2g_BU_h_
#define _gevb2g_BU_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "interface/shared/i2ogevb2g.h"
#include "toolbox/math/random.h"
#include "gevb2g/ResourcePool.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/PerformanceMeter.h"
#include "toolbox/mem/Reference.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/ActionListener.h"

// Log4CPLUS
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"

//
// cgicc
//
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "toolbox/task/Timer.h"

namespace gevb2g
{

	class BU : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public toolbox::task::TimerListener
	{
		protected:

			//
			//! performance meters
			//
			//toolbox::PerformanceMeter* dataPf_;

			//
			//! current bandwidth measurements
			//
			//xdata::Double dataBw_;
			//xdata::Double dataRate_;
			//xdata::Double dataLatency_ ;
			void timeExpired (toolbox::task::TimerEvent& e);
			void actionPerformed (xdata::Event& event);
			double std_deviation (std::vector<double>& samples, double mean);
			//toolbox::fsm::FiniteStateMachine fsm_;
			toolbox::TimeVal lastTime_; // used to measure time interval for measuring rate
			std::map<size_t, std::vector<double> > measurementHistory_;
			xdata::UnsignedLong numberOfSamples_;
			xdata::String sampleTime_;
			xdata::UnsignedLong index_;     // current sample

		public:

			XDAQ_INSTANTIATOR();

			void Default (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);
			void downloadMeasurements (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

			// ! HOWTO: performance history
			//
			//	if ( perf_.benchmark() ) {
			//		addMeasurement(0,throughput , rate, latency);
			//
			//		current_ = (current_ + 1 ) % 5;
			//    	if ( current_ == 0 )
			//				restartMeasurements(0);
			//	}

			BU (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception);

			//
			//! Message called upon receive of a packet from a Filter Unit
			//
			void allocate (toolbox::mem::Reference* ref);

			//
			//! Message called when filter unit requests an event to be discarded
			//
			virtual void discard (toolbox::mem::Reference * ref);

			//
			//! Send a completed event to a filter unit using FU_TAKE if a request is pending and an event is completed.
			//
			void matchFilterRequests ();

			//
			//! Called when a filter unit asks more data of an event fragment. Not implemented.
			//
			virtual void collect (toolbox::mem::Reference * ref);

			//
			//! Message called upon receive of a packet from a Readout Unit
			//
			void cache (toolbox::mem::Reference* ref);
			void packedcache (toolbox::mem::Reference* ref);

			/*! Method called to add a Builder Unit resource to the
			 list of free resources. If a packet to the Event
			 Manager can be filled with free resource Identifiers,
			 it will be sent by this function.
			 */
			void available (U32 freeResourceId, U32 context);

			// Implementation of xdaqApplication interfaces

			void ParameterGet (std::list<std::string> & paramNames);

			//! Helper function to print out exported parameters
			void verbose ();

			xoap::MessageReference Configure (xoap::MessageReference message) throw (xoap::exception::Exception);
			xoap::MessageReference Enable (xoap::MessageReference message) throw (xoap::exception::Exception);
			xoap::MessageReference Halt (xoap::MessageReference message) throw (xoap::exception::Exception);

			//! Extract context field from the I2O message and log it for debugging purposes
			U32 getContext ();

		protected:

			// clear resources and fifos of any spurious elements
			void clear ();

			bool stopped_; // indicates when the BU has to halt
			xdata::UnsignedLong availableFrameSize_;
			//xdata::UnsignedLong	samples_;
			size_t maxResourcesPerFrame_;
			xdata::UnsignedLong maxResources_;
			xdata::UnsignedLong maxDataFrameSize_;
			size_t maxFragments_;
			xdata::Boolean filterDisable_;
			xdata::UnsignedLong frameRecvCounter_;
			U32 context_;
			xdata::UnsignedLong counter_;
			xdata::UnsignedLong currentSize_; // the size of the currently received token message
			toolbox::BSem * lockAvailable_;
			//double 		totalMB_;
			size_t current_;
			xdata::String evmClassName_;
			xdata::UnsignedLong evmClassInstance_;
			xdata::String ruClassName_;
			I2O_TID evmTid_;
			gevb2g::ResourcePool* resourcePool_;
			toolbox::mem::Reference * availableRef_;

			xdata::UnsignedLong lastReceivedFrameSize_;
			// Simple implementation to FU interface (no collect)
			// Full event only
			// All requests from FU and completed events are queued
			// For each request in pendingAllocate and for each event in
			// eventCompleted an SGL is generated and delivered to the FU.
			//
			//

			typedef struct
			{
					I2O_TRANSACTION_CONTEXT context;
					I2O_TID tid;
			} FilterUnitRequest;

			toolbox::rlist<size_t> * eventCompleted_;
			toolbox::rlist<FilterUnitRequest> * pendingAllocate_;

			std::map<I2O_TID, const xdaq::ApplicationDescriptor*> fus_;

			toolbox::mem::Pool* pool_;

			xdata::Boolean createPool_;
			xdata::String poolName_;

			const xdaq::ApplicationDescriptor* evmDescriptor_;
			I2O_TID tid_;

			/*
			 struct Measurement
			 {
			 Measurement()
			 {
			 latency = 0.0;
			 rate = 0.0;
			 bandwidth = 0.0;
			 }

			 Measurement(double l, double r, double b)
			 {
			 latency = l;
			 rate = r;
			 bandwidth = b;
			 }

			 double latency;
			 double bandwidth;
			 double rate;

			 };
			 std::map<unsigned long, Measurement, std::less<unsigned long> > measurementHistory_;
			 */
			xdata::String state_;

	};
}

#endif

