#ifndef _gevb2g_InputEmulator_H
#define _gevb2g_InputEmulator_H

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "interface/shared/i2ogevb2g.h"
#include "toolbox/math/random.h"
#include "toolbox/BSem.h"
#include "toolbox/Task.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "toolbox/mem/MemoryPoolFactory.h"

//AP
#include <boost/random.hpp>
#include <boost/random/lognormal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <cmath>

#include "toolbox/task/WorkLoop.h"

// Log4CPLUS
#include "log4cplus/logger.h"

namespace gevb2g
{
	class InputEmulator : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
	{
		public:
			XDAQ_INSTANTIATOR();

			InputEmulator (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception);

			xoap::MessageReference Configure (xoap::MessageReference message) throw (xoap::exception::Exception);
			xoap::MessageReference Enable (xoap::MessageReference message) throw (xoap::exception::Exception);
			xoap::MessageReference Halt (xoap::MessageReference message) throw (xoap::exception::Exception);

			// Stepwise execution. Calling this function creates and sends one message.
			// Rate_ variable must be 0 when using this function
			xoap::MessageReference Next (xoap::MessageReference message) throw (xoap::exception::Exception);

			// Injected Errors
			xoap::MessageReference InjectZeroLengthMessage (xoap::MessageReference message) throw (xoap::exception::Exception);
			xoap::MessageReference InjectLongLengthMessage (xoap::MessageReference message) throw (xoap::exception::Exception);
			xoap::MessageReference InjectPartLengthMismatchFrameSize (xoap::MessageReference message) throw (xoap::exception::Exception);
			xoap::MessageReference InjectTotalLengthMismatch (xoap::MessageReference message) throw (xoap::exception::Exception);
		protected:

			int svc ();
			bool process (toolbox::task::WorkLoop* wl);
			void actionPerformed (xdata::Event& e);

			void logConfiguration ();

			bool stopped_;
			//toolbox::math::LogNormalGen * lognorm_;
			boost::variate_generator<boost::mt19937, boost::lognormal_distribution<> > * lognorm_;
			// exported variables
			size_t triggers_;
			xdata::UnsignedLong stdDev_;
			xdata::UnsignedLong mean_;
			xdata::UnsignedLong minFragmentSize_;
			xdata::UnsignedLong maxFragmentSize_;
			xdata::UnsignedLong frameSendCounter_;
			xdata::UnsignedLong maxDataFrameSize_;
			U32 bx_;

			xdata::UnsignedLong rate_; // data production rate in Hz e.g. 10Hz, 100, 1000 (1Khz), 10000 (10KHz)

			size_t yieldCounter_;
			bool veryFirst_;

			I2O_TID destination_; // internal
			xdata::String destinationClassName_; //exported for configuration
			xdata::UnsignedLong destinationClassInstance_; //exported for configuration
			xdata::Boolean fixedSize_;  // true then use pMean_ always, false use random generator

			enum ErrorType
			{
				NoError = 0,
				ZeroLengthMessage = 1,
				LongLengthMessage = 2,
				PartLengthMismatchFrameSize = 3,
				TotalLengthMismatch = 4
			};

			toolbox::mem::Reference* createData (U32 bx, ErrorType injectedError);

			ErrorType injectedError_;

			toolbox::BSem wait_; // sync. mutex for single step execution. Is initialised with BSem::Empty in CTOR
			const xdaq::ApplicationDescriptor * ruDescriptor_;
			toolbox::mem::Pool* pool_;

			xdata::Boolean createPool_;
			xdata::String poolName_;

			I2O_TID tid_;

			toolbox::task::WorkLoop* workloop_;
			toolbox::task::ActionSignature* process_;

			xdata::UnsignedLong commitedPoolSize_;

	};
}

#endif
