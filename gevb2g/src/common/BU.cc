#include "gevb2g/BU.h"
#include "i2o/utils/AddressMap.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/Method.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"

#include "toolbox/hexdump.h"
#include "toolbox/mem/HeapAllocator.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

XDAQ_INSTANTIATOR_IMPL (gevb2g::BU)

gevb2g::BU::BU (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this)
{
	i2o::bind(this, &gevb2g::BU::cache, I2O_CACHE, XDAQ_ORGANIZATION_ID);
	i2o::bind(this, &gevb2g::BU::packedcache, I2O_PACKED_CACHE, XDAQ_ORGANIZATION_ID);
	i2o::bind(this, &gevb2g::BU::allocate, I2O_ALLOCATE_N, XDAQ_ORGANIZATION_ID);
	i2o::bind(this, &gevb2g::BU::collect, I2O_COLLECT, XDAQ_ORGANIZATION_ID);
	i2o::bind(this, &gevb2g::BU::discard, I2O_DISCARD_N, XDAQ_ORGANIZATION_ID);

	xoap::bind(this, &gevb2g::BU::Configure, "Configure", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::BU::Enable, "Enable", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::BU::Halt, "Halt", XDAQ_NS_URI);

	xgi::framework::deferredbind(this, this, &gevb2g::BU::Default, "Default");
	xgi::bind(this, &gevb2g::BU::downloadMeasurements, "downloadMeasurements");

	// set default values
	//current_ = 0;
	//samples_ 	= 0;
	//totalMB_ 	= 0;
	index_ = 0;

	maxResources_ = 256;
	availableFrameSize_ = 1024;
	maxDataFrameSize_ = 4096;
	filterDisable_ = true;

	//dataBw_  = 0.0;
	//dataRate_  = 0.0;
	//dataLatency_  = 0.0;
	stopped_ = true;

	ruClassName_ = "RU"; //default readout unit name
	evmClassName_ = "EVM"; //default event manager
	evmClassInstance_ = 0;

	createPool_ = true;
	poolName_ = "buPool";

	lastReceivedFrameSize_ = 0;

	this->state_ = "halted";

	// export variables to control client
	//getApplicationInfoSpace()->fireItemAvailable ("samples", &samples_);
	getApplicationInfoSpace()->fireItemAvailable("maxResources", &maxResources_);
	getApplicationInfoSpace()->fireItemAvailable("availableFrameSize", &availableFrameSize_);
	getApplicationInfoSpace()->fireItemAvailable("maxDataFrameSize", &maxDataFrameSize_);
	getApplicationInfoSpace()->fireItemAvailable("filterDisable", &filterDisable_);
	getApplicationInfoSpace()->fireItemAvailable("frameRecvCounter", &frameRecvCounter_);

	getApplicationInfoSpace()->fireItemAvailable("createPool", &createPool_);
	getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);

	//getApplicationInfoSpace()->fireItemAvailable ("dataBw",&dataBw_);
	//getApplicationInfoSpace()->fireItemAvailable ("dataRate",&dataRate_);
	//getApplicationInfoSpace()->fireItemAvailable ("dataLatency",&dataLatency_);

	// Count all completed events. Reset counter at Configure time
	getApplicationInfoSpace()->fireItemAvailable("eventCounter", &counter_);

	// auto detection of EVM
	getApplicationInfoSpace()->fireItemAvailable("evmClassName", &evmClassName_);
	getApplicationInfoSpace()->fireItemAvailable("evmClassInstance", &evmClassInstance_);
	getApplicationInfoSpace()->fireItemAvailable("ruClassName", &ruClassName_);

	getApplicationInfoSpace()->fireItemAvailable("numberOfSamples", &numberOfSamples_);
	getApplicationInfoSpace()->fireItemAvailable("sampleTime", &sampleTime_);
	getApplicationInfoSpace()->fireItemAvailable("currentSize", &currentSize_);
	getApplicationInfoSpace()->fireItemAvailable("lastReceivedFrameSize", &lastReceivedFrameSize_);
	getApplicationInfoSpace()->fireItemAvailable("state", &state_);

	context_ = 0;
	lockAvailable_ = new toolbox::BSem(toolbox::BSem::FULL);

	eventCompleted_->setName("eventCompleted");
	pendingAllocate_->setName("pendingAllocate");

	std::set<const xdaq::ApplicationDescriptor*> rus;

	try
	{
		rus = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("gevb2g::RU");
		maxFragments_ = rus.size();
	}
	catch (xdaq::exception::Exception& e)
	{
		XCEPT_RETHROW(xcept::Exception, "No BU application instance found. Client cannot be configured.", e);
	}

	// FUs
	std::set<const xdaq::ApplicationDescriptor*> fus;

	try
	{
		fus = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("gevb2g::FU");
	}
	catch (xdaq::exception::Exception& e)
	{
		XCEPT_RETHROW(xcept::Exception, "No FU application instance found. Client cannot be configured.", e);
	}

	std::set<const xdaq::ApplicationDescriptor*>::iterator iter;

	for (iter = fus.begin(); iter != fus.end(); iter++)
	{
		fus_[i2o::utils::getAddressMap()->getTid(*iter)] = *iter;
	}

	//
	eventCompleted_ =  toolbox::rlist<size_t>::create("udapl-bu-event-completed");
        pendingAllocate_ = toolbox::rlist<FilterUnitRequest>::create("udapl-bu-pending-allocate");

	tid_ = i2o::utils::getAddressMap()->getTid(this->getApplicationDescriptor());

	//dataPf_ = new toolbox::PerformanceMeter();
	resourcePool_ = new gevb2g::ResourcePool(this);

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

void gevb2g::BU::timeExpired (toolbox::task::TimerEvent& e)
{
	lockAvailable_->take();

	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	double delta = (double) now - (double) lastTime_;
	double rate = counter_ / delta;
	counter_ = 0;
	lastTime_ = now;

	if (!stopped_ && ((unsigned long) index_ > 1)) // discard first two
	{
		if (measurementHistory_[currentSize_][index_ % numberOfSamples_] == 0) // fill ring and then discard measurement history
		{
			measurementHistory_[currentSize_][index_ % numberOfSamples_] = rate;
		}
	}
	index_++;

	lockAvailable_->give();

}

void gevb2g::BU::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		counter_ = 0;

		currentSize_ = 0;
		index_ = 0;

		try
		{
			xdata::UnsignedLongT instance = (xdata::UnsignedLongT) evmClassInstance_;
			evmDescriptor_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("gevb2g::EVM", instance);
			evmTid_ = i2o::utils::getAddressMap()->getTid(evmDescriptor_);
		}
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW(xcept::Exception, "No Server application instance found. Client cannot be configured.", e);
		}

		try
		{

			toolbox::task::Timer * timer = 0;
			if (!toolbox::task::getTimerFactory()->hasTimer("urn:benchmark:mstreamio2g-timer"))
			{
				timer = toolbox::task::getTimerFactory()->createTimer("urn:benchmark:mstreamio2g-timer");

			}
			else
			{
				timer = toolbox::task::getTimerFactory()->getTimer("urn:benchmark:mstreamio2g-timer");
			}

			toolbox::TimeVal startTime;
			startTime = toolbox::TimeVal::gettimeofday();
			toolbox::TimeInterval interval;
			interval.fromString(sampleTime_.toString()); // in seconds

			lastTime_ = toolbox::TimeVal::gettimeofday();

			timer->scheduleAtFixedRate(startTime, this, interval, 0, "CheckRateTimer");
		}
		catch (toolbox::task::exception::Exception& te)
		{
			std::cout << "Cannot run timer" << xcept::stdformat_exception_history(te) << std::endl;
		}

		try
		{
			if (createPool_)
			{
				toolbox::mem::HeapAllocator* a = new toolbox::mem::HeapAllocator();
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
			}
			else
			{
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
			}
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not set up memory pool, %s (exiting thread)", e.what()));
			return;
		}

	}
}

void gevb2g::BU::allocate (toolbox::mem::Reference* ref)
{
	if (stopped_)
	{
		ref->release();
		return;
	}

	PI2O_ALLOCATE_N_MESSAGE_FRAME frame = (PI2O_ALLOCATE_N_MESSAGE_FRAME) ref->getDataLocation();

	if (!filterDisable_)
	{
		FilterUnitRequest request;
		request.context = frame->PvtMessageFrame.TransactionContext;
		request.tid = frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress;
		for (size_t i = 0; i < frame->n; i++)
		{
			pendingAllocate_->push_back(request);
		}

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), toolbox::toString("allocate %d event(s) for filter unit", frame->n));

		this->matchFilterRequests();

	}
	else
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "filter interface is disabled, cannot allocate event to FU");
	}
	ref->release();
}

void gevb2g::BU::discard (toolbox::mem::Reference * ref)
{
	if (stopped_)
	{
		ref->release();
		return;
	}

	PI2O_DISCARD_N_MESSAGE_FRAME frame = (PI2O_DISCARD_N_MESSAGE_FRAME) ref->getDataLocation();

	if (!filterDisable_)
	{
		for (size_t i = 0; i < frame->n; i++)
		{
			U32 resource = frame->eventHandle[i];

			LOG4CPLUS_DEBUG(this->getApplicationLogger(), toolbox::toString("discard event and make resource %d available", resource));

			resourcePool_->clear(resource);
			U32 ctx = getContext();
			resourcePool_->available(resource, ctx);
			this->available(resource, ctx);
		}

	}
	else
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "filter interface is disabled, cannot discard event");
	}
	ref->release();
}

void gevb2g::BU::matchFilterRequests ()
{
	while ((!pendingAllocate_->empty()) && (!eventCompleted_->empty()))
	{
		// prepare SGL with for resource
		size_t resource = eventCompleted_->front();
		eventCompleted_->pop_front();

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), toolbox::toString("found a completed event (resource: %d) and deliver to FU", resource));

		FilterUnitRequest request = pendingAllocate_->front();
		pendingAllocate_->pop_front();

		int sgllen = resourcePool_->sglLen(resource);

		// allocate frame for take message
		size_t frameSize = sizeof(I2O_FU_TAKE_MESSAGE_FRAME) - sizeof(I2O_V2_SGE_FRAGMENT_ELEMENT_U32) + sgllen;
		//BufRef * ref = xdaq::frameAlloc(frameSize); 
		toolbox::mem::Reference* ref = 0;
		try
		{
			// allocate frame
			ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, frameSize);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "failed to get frame", e);
		}

		PI2O_FU_TAKE_MESSAGE_FRAME frame = (PI2O_FU_TAKE_MESSAGE_FRAME) ref->getDataLocation();

		// Standard I2O message contents
		//
		frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
		frame->PvtMessageFrame.XFunctionCode = I2O_FU_TAKE_EVENT;
		frame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
		frame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
		frame->PvtMessageFrame.StdMessageFrame.TargetAddress = request.tid;
		frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
		frame->PvtMessageFrame.TransactionContext = request.context;
		char sglOffset = sizeof(I2O_PRIVATE_MESSAGE_FRAME) + sizeof(U32);
		frame->PvtMessageFrame.StdMessageFrame.VersionOffset = (sglOffset << 2);

		//
		// Take frame contents
		//
		frame->eventHandle = resource;

		//
		// Fill SGL info
		//
		resourcePool_->retrieve(resource, (I2O_V2_SGE_FRAGMENT_ELEMENT_U32*) &(frame->Fragment32));
		frame->PvtMessageFrame.StdMessageFrame.MessageSize = frameSize >> 2;
		//xdaq::frameSend(ref); 
		ref->setDataSize(frameSize);
		getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), fus_[request.tid]);
	}
}

void gevb2g::BU::collect (toolbox::mem::Reference * ref)
{
	LOG4CPLUS_ERROR(this->getApplicationLogger(), "collect method is not implemented");
	ref->release();
}

void gevb2g::BU::packedcache (toolbox::mem::Reference* ref)
{
	PI2O_PACKED_CACHE_MESSAGE_FRAME frame = (PI2O_PACKED_CACHE_MESSAGE_FRAME)(ref->getDataLocation());
	size_t elements = frame->frames;
/*
	if (ref != 0)
	{
		PI2O_PACKED_CACHE_MESSAGE_FRAME frame = (PI2O_PACKED_CACHE_MESSAGE_FRAME)(ref->getDataLocation());
		std::cout << "Received packed frame, contains " << frame->frames << " messages of total size (msg)" << (frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2) << ", total size (ref) " << ref->getDataSize() << std::endl;
	}
*/
	char* offset = (char*)ref->getDataLocation() + sizeof(I2O_PACKED_CACHE_MESSAGE_FRAME) ;
	for (size_t i = 0; i < elements; i++)
	{
		PI2O_CACHE_MESSAGE_FRAME subframe = (PI2O_CACHE_MESSAGE_FRAME)(offset);

		size_t len = subframe->PvtMessageFrame.StdMessageFrame.MessageSize << 2;

		//std::cout << "Sub - packet size = " << len << std::endl;

		toolbox::mem::Reference* reference = 0;
		try
		{
			// allocate frame
			reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, len);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "failed to get frame", e);
		}

		memcpy(reference->getDataLocation(), offset, len);

		this->cache(reference);

		offset += len;
	}

	ref->release();
}

void gevb2g::BU::cache (toolbox::mem::Reference* ref)
{
	if (stopped_)
	{
		ref->release();
		return;
	}

	//LO to cope with live reset
	lockAvailable_->take();

	// handle chain of cache fragments

	toolbox::mem::Reference * current = ref;
	toolbox::mem::Reference * chain = current;

	while (current != (toolbox::mem::Reference*) 0)
	{
		// remove element from chain
		chain = current->getNextReference();
		current->setNextReference((toolbox::mem::Reference*) 0);

		frameRecvCounter_++;

		PI2O_CACHE_MESSAGE_FRAME frame = (PI2O_CACHE_MESSAGE_FRAME) current->getDataLocation();
		size_t size = frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2;
		lastReceivedFrameSize_ = size;
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), toolbox::toString("element totalLength: %d partLength: %d, resourceId: %d", frame->totalLength, frame->partLength, frame->resourceId));

		//
		// SGL MTU Size check???
		//
		if (size > maxDataFrameSize_)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), "Cache size " << size << " and maxDataFrameSize " << maxDataFrameSize_ << "do not match");
			ref->release();

			//LO to cope with live reset 
			lockAvailable_->give();
			return;
		}

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), toolbox::toString("received %d sgl elements", frame->elements));

		bool completed = false;

		try
		{
			completed = resourcePool_->appendZeroCopy(frame->resourceId, frame->fragmentId, current, frame->partLength, frame->totalLength, (unsigned long) frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress, frame->context, sizeof(I2O_CACHE_MESSAGE_FRAME)); // last param is offset to data
		}
		catch (...)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), toolbox::toString("bu[%d] fragmentId:%d resourceId:%d totalLength:%d partLength:%d, received from tid %d", this->getApplicationDescriptor()->getInstance(), frame->fragmentId, frame->resourceId, frame->totalLength, frame->partLength, frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress));

			resourcePool_->showFragmentHistory(frame->resourceId, frame->fragmentId);

			ref->release();

			// to cope with live reset
			lockAvailable_->give();
			return;
		}

		if (completed)
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), toolbox::toString("fragment %d completed", frame->resourceId));
			completed = false;

			//dataPf_->addSample(resourcePool_->size(frame->resourceId));

			if (filterDisable_)
			{
				counter_++;
				/*
				 if (counter_ % samples_ == 0)
				 {
				 //LOG4CPLUS_INFO (this->getApplicationLogger(), "collected " << counter_  << " events");
				 dataBw_        =  dataPf_->bandwidth();
				 dataRate_        =  dataPf_->rate();
				 dataLatency_        =  dataPf_->latency();
				 std::cout << "Size " << resourcePool_->size(frame->resourceId) << "Bandwidth " << dataPf_->bandwidth() << "\t Rate " << dataPf_->rate() << "\t Latency" << dataPf_->latency() << std::endl;
				 size_t currentSize = resourcePool_->size(frame->resourceId);
				 std::map<unsigned long, Measurement, std::less<unsigned long> >::iterator it =  measurementHistory_.find(currentSize);
				 if ( it != measurementHistory_.end() )
				 {
				 if ( measurementHistory_[currentSize].bandwidth < dataPf_->bandwidth() )
				 {
				 measurementHistory_[currentSize] = Measurement(dataPf_->latency(), dataPf_->rate(), dataPf_->bandwidth());
				 }
				 }
				 else
				 {
				 measurementHistory_[currentSize] = Measurement(dataPf_->latency(), dataPf_->rate(), dataPf_->bandwidth());
				 }

				 }
				 */

				resourcePool_->clear(frame->resourceId);
				U32 ctx = getContext();
				resourcePool_->available(frame->resourceId, ctx);
				this->available(frame->resourceId, ctx);
			}
			else
			{
				eventCompleted_->push_back(frame->resourceId);
				this->matchFilterRequests();
			}
		}

		// move to next element in chain
		current = chain;
	} // end while	

	//LO to cope with live reset
	lockAvailable_->give();
}

void gevb2g::BU::available (U32 freeResourceId, U32 context)
{
	PI2O_AVAILABLE_MESSAGE_FRAME framePtr;
	if (availableRef_ == 0)
	{
		//availableRef_  = xdaq::frameAlloc(availableFrameSize_);
		try
		{
			// allocate frame
			availableRef_ = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, availableFrameSize_);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "failed to get frame", e);
		}

		framePtr = (PI2O_AVAILABLE_MESSAGE_FRAME) availableRef_->getDataLocation();
		framePtr->resources = 0;
	}

	framePtr = (PI2O_AVAILABLE_MESSAGE_FRAME) availableRef_->getDataLocation();
	framePtr->resource[framePtr->resources].resourceId = freeResourceId;
	framePtr->resource[framePtr->resources].context = context;
	LOG4CPLUS_DEBUG(getApplicationLogger(), "before sending available resources " << framePtr->resources << " context" << framePtr->resource[framePtr->resources].context);
	framePtr->resources++;

	if (framePtr->resources >= maxResourcesPerFrame_)
	{
		framePtr->PvtMessageFrame.StdMessageFrame.MessageSize = availableFrameSize_ >> 2;
		framePtr->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
		framePtr->PvtMessageFrame.XFunctionCode = I2O_AVAILABLE;
		framePtr->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
		framePtr->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
		framePtr->PvtMessageFrame.StdMessageFrame.TargetAddress = evmTid_;
		framePtr->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
		framePtr->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
		framePtr->destination = this->getApplicationDescriptor()->getInstance();

		//DEBUG
		//if ( framePtr->resource[0].context == 0x100 || framePtr->resource[0].context == 0xff )
		//{
		//char a;
		//std::cout << "USER INPUT:" <<std::endl;
		//std::cin >> a;
		//std::cout << "---begin---" << std::endl;
		//toolbox::hexdump(availableRef_->getDataLocation(),framePtr->PvtMessageFrame.StdMessageFrame.MessageSize << 2 );
		//std::cout << "---end-----" << std::endl;
		//}
// DEBUG

		//xdaq::frameSend(availableRef_);
		availableRef_->setDataSize(availableFrameSize_);
		try
		{
			this->getApplicationContext()->postFrame(availableRef_, this->getApplicationDescriptor(), evmDescriptor_);
		}
		catch (xdaq::exception::Exception &e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));

		}

		availableRef_ = 0;
	}
}

xoap::MessageReference gevb2g::BU::Configure (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	this->state_ = "configured";
	maxResourcesPerFrame_ = (availableFrameSize_ - (sizeof(I2O_AVAILABLE_MESSAGE_FRAME) + sizeof(ShipRequest))) / sizeof(ShipRequest);

	if (maxResourcesPerFrame_ > maxResources_)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "optimized configuration: number of resources " << maxResources_ << " was not sufficient to fill 'available' frame, set to max resources " << maxResourcesPerFrame_);
		maxResourcesPerFrame_ = maxResources_;
	}

	availableRef_ = 0;
	frameRecvCounter_ = 0;
	context_ = 0;

	// clear any spurious element in queues and resource pool
	this->clear();

	// resize if pending allocate is missing
	eventCompleted_->resize(maxResources_ * 2);

	resourcePool_->init(maxResources_, maxFragments_, maxDataFrameSize_, false);

	this->verbose();

	counter_ = 0;

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Configured");

	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("ConfigureResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

}

xoap::MessageReference gevb2g::BU::Enable (xoap::MessageReference message) throw (xoap::exception::Exception)
{

	lockAvailable_->take();
	this->state_ = "enabled";
	stopped_ = false;
	if (measurementHistory_.find(currentSize_) == measurementHistory_.end())
	{
		std::vector<double> samples;
		samples.resize(numberOfSamples_);
		for (std::vector<double>::iterator i = samples.begin(); i != samples.end(); i++)
		{
			*i = 0.0;
		}
		measurementHistory_[currentSize_] = samples;

	}
	index_ = 0;

	for (size_t i = 0; i < maxResources_; i++)
	{
		U32 ctx = getContext();
		resourcePool_->available(i, ctx);
		available(i, ctx);
	}

	lockAvailable_->give();

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Enabled");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("EnableResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

	return xoap::createMessage();
}

void gevb2g::BU::verbose ()
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), toolbox::toString("gathering %d resources per (available) frame", maxResourcesPerFrame_));
}

xoap::MessageReference gevb2g::BU::Halt (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	this->state_ = "halted";
	// inhibit cache
	stopped_ = true;

	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("HaltResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;
}

void gevb2g::BU::clear ()
{

	// flush fifos
	while (!eventCompleted_->empty())
		eventCompleted_->pop_front();
	while (!pendingAllocate_->empty())
		pendingAllocate_->pop_front();

	// clear memory
	resourcePool_->reset();

	if (availableRef_ != (toolbox::mem::Reference*) 0)
	{
		availableRef_->release();
	}
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Cleared BU internal fifos and resources");
}

U32 gevb2g::BU::getContext ()
{
	U32 ctx = (this->getApplicationDescriptor()->getInstance() << 16) + (context_++ & 0x0000ffff);
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), toolbox::toString("generated context: %08x", ctx));
	return (ctx);
}

void gevb2g::BU::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string measurementURL;
	measurementURL = "/";
	measurementURL += getApplicationDescriptor()->getURN();
	measurementURL += "/downloadMeasurements";
	*out << "<a href=\"" << measurementURL << "\">" << "Download measurements" << "</a><br /><br />" << std::endl;

	*out << cgicc::hr() << std::endl;
	*out << "<br />" << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");

	*out << cgicc::thead();

	*out << cgicc::tr();
	*out << cgicc::th("Size");
	*out << cgicc::th("Rate Samples");
	*out << cgicc::th("Bandwidth(MB/s - IEC Standard)");
	*out << cgicc::th("Mean(Hz)");
	*out << cgicc::th("RMS(Hz)");
	*out << cgicc::tr() << std::endl;

	*out << cgicc::thead();
	*out << cgicc::tbody();

	lockAvailable_->take();
	for (std::map<size_t, std::vector<double> >::iterator i = measurementHistory_.begin(); i != measurementHistory_.end(); i++)
	{
		*out << cgicc::tr();
		*out << cgicc::td() << (*i).first << cgicc::td();
		*out << cgicc::td();

		double avarage = 0.0;
		double throughput = 0.0;
		for (std::vector<double>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			if (j != (*i).second.begin()) *out << ",";
			*out << (size_t) * j;

			avarage += *j;
		}

		*out << cgicc::td();
		avarage = avarage / (*i).second.size();
		throughput = avarage * (*i).first / 1000000.0;
		out->precision(5);
		*out << cgicc::td() << throughput << cgicc::td();
		*out << cgicc::td() << avarage << cgicc::td();
		*out << cgicc::td() << this->std_deviation((*i).second, avarage) << cgicc::td();

		*out << cgicc::tr();
	}

	lockAvailable_->give();

	*out << cgicc::tbody();
	*out << cgicc::table();
	*out << "<p>" << std::endl;
}

double gevb2g::BU::std_deviation (std::vector<double> & samples, double mean)
{
	double sum_of_square = 0;

	for (std::vector<double>::iterator j = samples.begin(); j != samples.end(); j++)
	{
		sum_of_square += (*j - mean) * (*j - mean);
	}
	return sqrt(sum_of_square / samples.size());
}

void gevb2g::BU::downloadMeasurements (xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception)
{
	std::string disposition = toolbox::toString("attachment; filename=%s.ascii;", "measurements");
	out->getHTTPResponseHeader().addHeader("Content-Type", "application/csv");
	out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
	out->getHTTPResponseHeader().addHeader("Expires", "0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "post-check=0, pre-check=0");
	out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");

	lockAvailable_->take();
	*out << "#" << toolbox::toString("%s(%d)", getApplicationDescriptor()->getClassName().c_str(), (unsigned int) getApplicationDescriptor()->getInstance()) << std::endl;

	for (std::map<size_t, std::vector<double> >::iterator i = measurementHistory_.begin(); i != measurementHistory_.end(); i++)
	{
		*out << (*i).first << ",";
		for (std::vector<double>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			if (j != (*i).second.begin()) *out << ",";
			out->precision(5);
			*out << (size_t) * j;

		}

		*out << std::endl;
	}

	lockAvailable_->give();

}

