#include "gevb2g/InputEmulator.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "i2o/utils/AddressMap.h"
#include "xcept/tools.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

// Log4CPLUS
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"

#include "toolbox/task/WorkLoopFactory.h"

#include "xdaq/ApplicationRegistry.h"
#include "xgi/framework/Method.h"

#include "toolbox/TimeVal.h"
#include "toolbox/utils.h"

XDAQ_INSTANTIATOR_IMPL (gevb2g::InputEmulator)

gevb2g::InputEmulator::InputEmulator (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this), wait_(toolbox::BSem::EMPTY)
{
	xoap::bind(this, &gevb2g::InputEmulator::InjectZeroLengthMessage, "InjectZeroLengthMessage", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::InputEmulator::InjectLongLengthMessage, "InjectLongLengthMessage", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::InputEmulator::InjectPartLengthMismatchFrameSize, "InjectPartLengthMismatchFrameSize", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::InputEmulator::InjectTotalLengthMismatch, "InjectTotalLengthMismatch", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::InputEmulator::Next, "Next", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::InputEmulator::Configure, "Configure", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::InputEmulator::Enable, "Enable", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::InputEmulator::Halt, "Halt", XDAQ_NS_URI);

	stopped_ = true;
	stdDev_ = 0;
	mean_ = 0;
	minFragmentSize_ = 0;
	maxFragmentSize_ = 0;
	rate_ = 1; // 1 Hz
	fixedSize_ = true;

	createPool_ = true;
	poolName_ = "iePool";

	yieldCounter_ = 0;

	veryFirst_ = true;

	this->commitedPoolSize_ = 0x100000;

	destinationClassName_ = "RU";
	destinationClassInstance_ = 0;
	// export variables
	//
	getApplicationInfoSpace()->fireItemAvailable("StdDev", &stdDev_);
	getApplicationInfoSpace()->fireItemAvailable("Mean", &mean_);
	getApplicationInfoSpace()->fireItemAvailable("MinFragmentSize", &minFragmentSize_);
	getApplicationInfoSpace()->fireItemAvailable("MaxFragmentSize", &maxFragmentSize_);
	getApplicationInfoSpace()->fireItemAvailable("destinationClassName", &destinationClassName_);
	getApplicationInfoSpace()->fireItemAvailable("destinationClassInstance", &destinationClassInstance_);
	getApplicationInfoSpace()->fireItemAvailable("maxDataFrameSize", &maxDataFrameSize_);
	getApplicationInfoSpace()->fireItemAvailable("frameSendCounter", &frameSendCounter_);
	getApplicationInfoSpace()->fireItemAvailable("fixedSize", &fixedSize_);
	getApplicationInfoSpace()->fireItemAvailable("rate", &rate_);

	getApplicationInfoSpace()->fireItemAvailable("createPool", &createPool_);
	getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);

	getApplicationInfoSpace()->fireItemAvailable("commitedPoolSize", &commitedPoolSize_);

	tid_ = i2o::utils::getAddressMap()->getTid(this->getApplicationDescriptor());
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	this->stopped_ = true;


}
void gevb2g::InputEmulator::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
		{
			if (createPool_)
			{
				toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(commitedPoolSize_);
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
			}
			else
			{
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
			}


			toolbox::mem::Allocator* ca = dynamic_cast<toolbox::mem::Allocator *>(pool_->getAllocator());
			if (ca->isCommittedSizeSupported())
			{
				size_t committedSize = ca->getCommittedSize();
				pool_->setHighThreshold((unsigned long) (committedSize * 0.9));
				pool_->setLowThreshold((unsigned long) (committedSize * 0.5));
			}
			else
			{
				LOG4CPLUS_FATAL(getApplicationLogger(), "Failed to set threshold on pool. Pool is not a supported pool type");
			}
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not set up memory pool, %s (exiting thread)", e.what()));
			return;
		}

		size_t instance = this->getApplicationDescriptor()->getInstance();

		process_ = toolbox::task::bind(this, &gevb2g::InputEmulator::process, "process");

		try
		{
			std::stringstream wlss;
			wlss << "gevb2g-InputEmulator-" << instance;
			toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "polling")->activate();
		}
		catch (toolbox::task::exception::Exception& e)
		{
			std::stringstream ss;
			ss << "Failed to submit workloop ";

			std::cerr << ss.str() << std::endl;
		}
		catch (std::exception& se)
		{
			std::stringstream ss;
			ss << "Failed to submit notification to worker thread, caught standard exception '";
			ss << se.what() << "'";
			std::cerr << ss.str() << std::endl;
		}
		catch (...)
		{
			std::cerr << "Failed to submit notification to worker pool, caught unknown exception" << std::endl;
		}

	}
}
xoap::MessageReference gevb2g::InputEmulator::Configure (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	bx_ = 0;

	ruDescriptor_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptor(destinationClassName_, destinationClassInstance_);

	destination_ = i2o::utils::getAddressMap()->getTid(ruDescriptor_);

	//destination_ = xdaq::getTid(destinationClassName_, destinationClassInstance_);

	frameSendCounter_ = 0;

	this->logConfiguration();

	stopped_ = true;

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Successfully configured");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("ConfigureResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

}

//int gevb2g::InputEmulator::svc()
bool gevb2g::InputEmulator::process (toolbox::task::WorkLoop* wl)
{
	i2o::MethodSignature* method = 0;
	try
	{
		xdaq::Application* application = this->getApplicationContext()->getApplicationRegistry()->getApplication(ruDescriptor_->getLocalId());
		U16 code = ((U16) I2O_SUPER_FRAGMENT_READY) | XDAQ_ORGANIZATION_ID;
		method = dynamic_cast<i2o::MethodSignature*>(application->getMethod(code));
	}
	catch (xdaq::exception::ApplicationNotFound& e)
	{
		//XCEPT_RETHROW(pt::frl::exception::Exception, "Failed to get method callback to send to", e);
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Failed to get method callback to send to, " << e.what());
		return true;
	}

	// use the instance number as a seed for random distribution
	// Segmentation fault in SLC6: we should debug it why. For the moment we use the fixed size.
	//lognorm_ = new toolbox::math::LogNormalGen(this->getApplicationDescriptor()->getInstance(), mean_, stdDev_, minFragmentSize_, maxFragmentSize_);

	//AP BOOST!!!
	boost::mt19937 rng;
	rng.seed((unsigned int) this->getApplicationDescriptor()->getInstance());
	lognorm_ = new boost::variate_generator<boost::mt19937, boost::lognormal_distribution<> >(rng, boost::lognormal_distribution<>(mean_, stdDev_));

	while (!stopped_)
	{

		//std::cout << "Sending " << rate_.toString() << " messages" << std::endl;

		for ( xdata::UnsignedLongT i = 0; i < (xdata::UnsignedLongT) rate_; i++ )
		{

			toolbox::mem::Reference * ref = createData(bx_++, injectedError_);
			injectedError_ = NoError;

			frameSendCounter_++;


			//PI2O_DATA_READY_MESSAGE_FRAME frameTest = (PI2O_DATA_READY_MESSAGE_FRAME)(ref->getDataLocation());
			//U32 sizeTest = frameTest->PvtMessageFrame.StdMessageFrame.MessageSize << 2;
			//std::cout << "%%%%%%%%%%%%%%%%%%%%%%%%%% created size " << sizeTest << " ref size is " << ref->getDataSize() << std::endl;
			//	xdaq::frameSend(ref);
			//this->getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), ruDescriptor_);
			try
			{
				method->invoke(ref);
				if (veryFirst_)
				{
					//std::cout << "Sleep 3sec after sending first message burst is " << burst << " delay is (us)" << delay << std::endl;
					sleep(1);
					veryFirst_ = false;
				}
			}
			catch (i2o::exception::Exception& e)
			{
				ref->release();
				std::stringstream ss;
				ss << "Exception occured during i2o method dispatch";
				LOG4CPLUS_ERROR(this->getApplicationLogger(), toolbox::toString("Discarded bx %d because frameSend failed.", bx_));
			}
		}

		/*
		toolbox::TimeVal endTime = toolbox::TimeVal::gettimeofday();
		suseconds_t delta = endTime.usec() - startTime.usec();

		if ( delta < 1000000 )
		{
			std::cout << "Time to sleep: " << (1000000 - endTime.usec()) << std::endl;
			toolbox::u_sleep(1000000 - endTime.usec());
		}
		//sleep remaining time
		 */
	}

	delete lognorm_;
	return false;
}

void gevb2g::InputEmulator::logConfiguration ()
{
	if (fixedSize_)
	{
		size_t reservedSize = sizeof(I2O_DATA_READY_MESSAGE_FRAME);

		size_t dataSize = maxDataFrameSize_ - reservedSize;

		// Calculate, how many blocks of I2O messages are
		// necessary to transmit the data. For fixed size,
		// totalSize = pMean_
		//
		size_t nBlocks;
		if ((mean_ % dataSize) != 0)
		{
			nBlocks = (size_t)((mean_ + dataSize) / dataSize);
		}
		else
		{
			nBlocks = (size_t)((mean_) / dataSize);
		}

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Use fixed total data size " << mean_ << " bytes and " << nBlocks << " data blocks per fragment");
	}
	else
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), "use variable/random size input data fragments (use mean, stddev)");
	}
}

toolbox::mem::Reference * gevb2g::InputEmulator::createData (U32 bx, ErrorType injectedError)
{
	U32 totalSize = 0;

	if (fixedSize_)
	{
		totalSize = mean_;
	}
	else
	{
		//totalSize =  lognorm_->getRandomSize();
		//totalSize = rand() % (8042) + 128;
		totalSize = round((*lognorm_)());
		if (totalSize > maxFragmentSize_)
		{
			totalSize = maxFragmentSize_;
		}
		else
		{
			if (totalSize < minFragmentSize_)
			{
				totalSize = minFragmentSize_;
			}
		}
		//std::cout << "%%%%%%%%%%%%%%%%%%%%%%%%%% created size " << totalSize << std::endl;
	}

	// Make sure that the  size if rounded to 4 bytes
	totalSize += 3;
	size_t module = totalSize % 4;
	totalSize -= module;

	size_t reservedSize = sizeof(I2O_SUPER_FRAGMENT_READY_MESSAGE_FRAME);

	size_t dataSize = maxDataFrameSize_ - reservedSize;

	// Calculate, how many blocks of I2O messages are
	// necessary to transmit the data
	//
	size_t nBlocks;
	if ((totalSize % dataSize) != 0)
	{
		nBlocks = (size_t)((totalSize + dataSize) / dataSize);
	}
	else
	{
		nBlocks = (size_t)((totalSize) / dataSize);
	}

	size_t remainingSize = totalSize;
	size_t copySize;

	toolbox::mem::Reference* last = 0;
	toolbox::mem::Reference* first = 0;

	// Fill all blocks
	//
	//
	for (size_t j = 0; j < nBlocks; j++)
	{
		if (pool_->isHighThresholdExceeded())
		{
			while (pool_->isLowThresholdExceeded())
			{
				LOG4CPLUS_DEBUG(getApplicationLogger(), "yield till low threshold reached");
				//this->yield(1);
				//std::cout << ".";
				::sched_yield();
				yieldCounter_++;
			}
			//std::cout << std::endl;

		}
		// Allocate maximum configured size
		//
		//toolbox::mem::Reference * newRef = xdaq::frameAlloc(maxDataFrameSize_);
		toolbox::mem::Reference* newRef = 0;
		try
		{
			// allocate frame
			newRef = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxDataFrameSize_);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

		copySize = ((size_t) remainingSize < dataSize ? remainingSize : dataSize); // MIN calculation

		remainingSize = remainingSize - copySize;

		char * buf = (char*) newRef->getDataLocation();
		sprintf(&buf[reservedSize], "dummy block %d of %d bytes", (int) j, (int) copySize);

		PI2O_SUPER_FRAGMENT_READY_MESSAGE_FRAME frame = (PI2O_SUPER_FRAGMENT_READY_MESSAGE_FRAME)(newRef->getDataLocation());
		frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
		frame->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
		frame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
		frame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
		frame->PvtMessageFrame.StdMessageFrame.TargetAddress = destination_;
		frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
		frame->PvtMessageFrame.XFunctionCode = I2O_SUPER_FRAGMENT_READY;
		frame->PvtMessageFrame.StdMessageFrame.MessageSize = (copySize + sizeof(I2O_SUPER_FRAGMENT_READY_MESSAGE_FRAME)) >> 2;
		frame->totalLength = totalSize;
		frame->partLength = copySize;

		newRef->setDataSize(copySize + sizeof(I2O_SUPER_FRAGMENT_READY_MESSAGE_FRAME));

		size_t * endOfFrame = (size_t *) ((char*) (newRef->getDataLocation()) + copySize + sizeof(I2O_SUPER_FRAGMENT_READY_MESSAGE_FRAME) - 8);
		*endOfFrame = 0xcafecafecafecafe;
		if (first == (toolbox::mem::Reference*) 0)
		{
			first = newRef;
			last = first;
		}
		else
		{
			last->setNextReference(newRef);
			last = newRef;
		}
	}

	//
	// Modify the message according to the injected error
	//
	switch (injectedError)
	{
		case ZeroLengthMessage:
		{
			//
			// Set the first I2O frame in a chain to 0 bytes length
			//

			PI2O_DATA_READY_MESSAGE_FRAME frame = (PI2O_DATA_READY_MESSAGE_FRAME)(first->getDataLocation());
			frame->PvtMessageFrame.StdMessageFrame.MessageSize = sizeof(I2O_DATA_READY_MESSAGE_FRAME) >> 2;
			frame->totalLength = 0;
			frame->partLength = 0;

			//
			// if it's a chain, free the other elements
			//
			if (first->getNextReference() != 0)
			{
				(first->getNextReference())->release();
				first->setNextReference((toolbox::mem::Reference*) 0);
			}
		}
			break;

		case LongLengthMessage:
		{
			// free current chain
			first->release();

			// Allocate maximum configured size + addition size
			//
			//toolbox::mem::Reference * ref = xdaq::frameAlloc(maxDataFrameSize_*2);
			toolbox::mem::Reference * ref = 0;
			try
			{
				// allocate frame
				ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxDataFrameSize_ * 2);
			}
			catch (toolbox::mem::exception::Exception & e)
			{
				XCEPT_RETHROW(xdaq::exception::Exception, "failed to get frame", e);
			}

			PI2O_DATA_READY_MESSAGE_FRAME frame = (PI2O_DATA_READY_MESSAGE_FRAME)(ref->getDataLocation());
			frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
			frame->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
			frame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
			frame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
			frame->PvtMessageFrame.StdMessageFrame.TargetAddress = destination_;
			frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
			frame->PvtMessageFrame.XFunctionCode = I2O_DATA_READY;
			frame->PvtMessageFrame.StdMessageFrame.MessageSize = (maxDataFrameSize_ * 2) >> 2;
			frame->totalLength = maxDataFrameSize_ * 2 - sizeof(I2O_DATA_READY_MESSAGE_FRAME);
			frame->partLength = frame->totalLength;

			return ref;
		}
			break;

		case PartLengthMismatchFrameSize:
		{
			PI2O_DATA_READY_MESSAGE_FRAME frame = (PI2O_DATA_READY_MESSAGE_FRAME)(first->getDataLocation());
			frame->partLength -= 4; // substract  4 bytes to the correct size
		}
			break;

		case TotalLengthMismatch:
		{
			PI2O_DATA_READY_MESSAGE_FRAME frame = (PI2O_DATA_READY_MESSAGE_FRAME)(first->getDataLocation());
			frame->totalLength -= 4; // substract  4 bytes to the correct size
		}
			break;

		default:
			break;

	}

	return first;
}

xoap::MessageReference gevb2g::InputEmulator::Enable (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	yieldCounter_ = 0;
	stopped_ = false;
	//this->activate();
	size_t instance = this->getApplicationDescriptor()->getInstance();

	try
	{
		std::stringstream wlss;
		wlss << "gevb2g-InputEmulator-" << instance;
		toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		std::stringstream ss;
		ss << "Failed to submit workloop ";

		std::cerr << ss.str() << std::endl;
	}
	catch (std::exception& se)
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception '";
		ss << se.what() << "'";
		std::cerr << ss.str() << std::endl;
	}
	catch (...)
	{
		std::cerr << "Failed to submit notification to worker pool, caught unknown exception" << std::endl;
	}

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Enable, starting to create data.");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("EnableResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

}

xoap::MessageReference gevb2g::InputEmulator::Halt (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	std::cout << "Yield count : " << yieldCounter_ << std::endl;
	stopped_ = true;
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Halt, data creation stopped.");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("HaltResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

}

xoap::MessageReference gevb2g::InputEmulator::InjectZeroLengthMessage (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Injecting a ZeroLengthMessage");
	injectedError_ = ZeroLengthMessage;
	xoap::MessageReference reply = xoap::createMessage();
	return reply;
}

xoap::MessageReference gevb2g::InputEmulator::InjectLongLengthMessage (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Injecting a LongLengthMessage");
	injectedError_ = LongLengthMessage;
	xoap::MessageReference reply = xoap::createMessage();
	return reply;
}

xoap::MessageReference gevb2g::InputEmulator::InjectPartLengthMismatchFrameSize (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Injecting a PartLengthMismatchFrameSize");
	injectedError_ = PartLengthMismatchFrameSize;
	xoap::MessageReference reply = xoap::createMessage();
	return reply;
}

xoap::MessageReference gevb2g::InputEmulator::InjectTotalLengthMismatch (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Injecting a TotalLengthMismatch");
	injectedError_ = TotalLengthMismatch;
	xoap::MessageReference reply = xoap::createMessage();
	return reply;
}

xoap::MessageReference gevb2g::InputEmulator::Next (xoap::MessageReference message) throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Creating and sending one message frame");
	wait_.give();
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("NextResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

}
