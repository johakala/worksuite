#!/bin/sh

echo "host " $1 " lid "$2 " value " $3

curl --stderr /dev/null \
-H "SOAPAction: urn:xdaq-application:lid=$2" \
-d "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"
	  xmlns:SOAP-ENV=\"http://schemasxmlsoap.org/soap/envelope/\"
	  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
	  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
	<SOAP-ENV:Header>
	</SOAP-ENV:Header>
	<SOAP-ENV:Body>
		<xdaq:ParameterSet xmlns:xdaq=\"urn:xdaq-soap:3.0\">
			<p:properties xmlns:p=\"urn:xdaq-application:gevb2g::InputEmulator\" xsi:type=\"soapenc:Struct\">
				<p:Mean xsi:type=\"xsd:unsignedLong\">$3</p:Mean>
			</p:properties>
		</xdaq:ParameterSet>
	</SOAP-ENV:Body>
	</SOAP-ENV:Envelope>" $1
