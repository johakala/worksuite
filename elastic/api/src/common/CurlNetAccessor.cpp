// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius    		             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLExceptMsgs.hpp>

#include "elastic/api/CurlNetAccessor.hpp"
#include "elastic/api/CurlURLInputStream.hpp"






elastic::api::CurlNetAccessor::CurlNetAccessor()
{
	initCurl();
}


elastic::api::CurlNetAccessor::~CurlNetAccessor()
{
	cleanupCurl();
}


//
// Global once-only init and cleanup of curl
//
// The init count used here is not thread protected; we assume
// that creation of the CurlNetAccessor will be serialized by
// the application. If the application is also using curl, then
// care must be taken that curl is initialized only once, by some
// other means, or by overloading these methods.
//
int elastic::api::CurlNetAccessor::fgCurlInitCount = 0;

void
elastic::api::CurlNetAccessor::initCurl()
{
	if (fgCurlInitCount++ == 0)
		curl_global_init(	0
						  | CURL_GLOBAL_ALL			// Initialize all curl modules
					//	  | CURL_GLOBAL_WIN32		// Initialize Windows sockets first
					//	  | CURL_GLOBAL_SSL			// Initialize SSL first
						  );
}


void
elastic::api::CurlNetAccessor::cleanupCurl()
{
	if (fgCurlInitCount > 0 && --fgCurlInitCount == 0)
		curl_global_cleanup();
}


elastic::api::CurlURLInputStream* elastic::api::CurlNetAccessor::makeNew(const XMLURL&  urlSource, elastic::api::HTTPMethod fHTTPMethod, std::list<std::string> & headers, const elastic::api::NetHTTPInfo* httpInfo)
{
	// Just create a CurlURLInputStream
	// We defer any checking of the url type for curl in CurlURLInputStream
	std::cout << "------>>>>>" << std::endl;
	CurlURLInputStream* retStrm = new CurlURLInputStream(urlSource, fHTTPMethod, headers, httpInfo);
	return retStrm;            
}



