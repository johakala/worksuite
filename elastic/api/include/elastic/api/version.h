// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius                        *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for elastic::api
//
#ifndef _elastic_api_Version_h_
#define _elastic_api_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define ELASTICAPI_VERSION_MAJOR 1
#define ELASTICAPI_VERSION_MINOR 3
#define ELASTICAPI_VERSION_PATCH 1
// If any previous versions available E.g. #define ELASTICAPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef ELASTICAPI_PREVIOUS_VERSIONS

//
// Template macros
//
#define ELASTICAPI_VERSION_CODE PACKAGE_VERSION_CODE(ELASTICAPI_VERSION_MAJOR,ELASTICAPI_VERSION_MINOR,ELASTICAPI_VERSION_PATCH)
#ifndef ELASTICAPI_PREVIOUS_VERSIONS
#define ELASTICAPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(ELASTICAPI_VERSION_MAJOR,ELASTICAPI_VERSION_MINOR,ELASTICAPI_VERSION_PATCH)
#else 
#define ELASTICAPI_FULL_VERSION_LIST  ELASTICAPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(ELASTICAPI_VERSION_MAJOR,ELASTICAPI_VERSION_MINOR,ELASTICAPI_VERSION_PATCH)
#endif 

namespace elasticapi
{
	const std::string package  =  "elasticapi";
	const std::string versions =  ELASTICAPI_FULL_VERSION_LIST;
	const std::string summary = "Elasticsearch XDAQ interface";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, P.Roberts, D. Simelevicius";
	const std::string link = "http://www.elasticsearch.org/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
