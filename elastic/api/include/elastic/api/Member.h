// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius    		             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _elastic_api_Member_h_
#define _elastic_api_Member_h_

#include "xdaq/Application.h"

#include "elastic/api/exception/Exception.h"
#include "elastic/api/Cluster.h"

#include "toolbox/BSem.h"
#include "toolbox/Properties.h"

namespace elastic
{
	namespace api
	{
		class Member : public xdaq::Object
		{
			public:

				Member (xdaq::Application * owner, toolbox::Properties & p) throw (xdaq::exception::Exception);

				Member (xdaq::Application * owner, std::list<std::string> & urls, toolbox::Properties & p) throw (xdaq::exception::Exception);

				Member (xdaq::Application * owner, const std::string & tribeurl, toolbox::Properties & p) throw (xdaq::exception::Exception);

				Cluster& joinCluster (const std::string & url);

				std::set<std::string> getAvailableClusters();

				std::string getTribeURL();

				// standardized HTML output for this eventing::member, can be embedded in an application hyperdaq page (e.g. in a tab)
				std::string clustersToHTML();

			protected:

				std::map<std::string, elastic::api::Cluster*> esClusters_;

			private:

				toolbox::BSem mutex_;

				std::string tribeurl_;

				toolbox::Properties properties_;

		};
	}
}

#endif
