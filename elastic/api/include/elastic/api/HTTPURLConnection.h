// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius    		             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef elastic_api_HTTPURLConnection_h_
#define elastic_api_HTTPURLConnection_h_

#include "elastic/api/exception/Exception.h"
#include <string>

namespace elastic
{

	namespace api
	{

		class HTTPURLConnection
		{
			public:

				HTTPURLConnection() throw (elastic::api::exception::Exception);
				~HTTPURLConnection();

				//! receive  from established HTTP connection and returns data into a BufRef
				std::string receiveFrom() throw (elastic::api::exception::Exception);

				//! send data to an established HTTP connection
				void sendTo
				(
						char * path,
						char * host,
						unsigned int port,
						const char * buf,
						size_t len
				)
				throw (elastic::api::exception::Exception);

				//! send data to an established HTTP connection
				void sendTo
				(
						char * path,
						char * host,
						unsigned int port,
						const char * buf,
						size_t len,
						const char* soapAction
				)
				throw (elastic::api::exception::Exception);


				//! close connection
				void close();

				//! connect to URL
				void connect(const std::string& host, unsigned int port) throw (elastic::api::exception::Exception);

				//! send buffer of given lenght
				void send(const char * buf, size_t len)  throw (elastic::api::exception::Exception);

			protected:

				//! receive len characters into buf
				ssize_t receive(char * buf , size_t len )  throw (elastic::api::exception::Exception);


				//! Helper to re-creating a socket
				void open() throw (elastic::api::exception::Exception);

				std::string extractMIMEBoundary(const char * buf , size_t size) throw (elastic::api::exception::Exception);

				void writeHttpPostMIMEHeader
				(
						char * path,
						char * host,
						unsigned int port,
						std::string & boundaryStr,
						size_t len
				)
				throw (elastic::api::exception::Exception);

				void writeHttpPostMIMEHeader
				(
						char * path,
						char * host,
						unsigned int port,
						std::string & boundaryStr,
						size_t len,
						const char* soapAction
				)
				throw (elastic::api::exception::Exception);

				void writeHttpPostHeader
				(
						char * path,
						char * host,
						unsigned int port,
						size_t len,
						const char* soapAction
				)
				throw (elastic::api::exception::Exception);

				void writeHttpPostHeader
				(
						char * path,
						char * host,
						unsigned int port,
						size_t len
				)
				throw (elastic::api::exception::Exception);

				int socket_;
		};

	}}
#endif
