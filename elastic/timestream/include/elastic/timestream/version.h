// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius                        *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _elastic_timestream_version_h_
#define _elastic_timestream_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define ELASTICTIMESTREAM_VERSION_MAJOR 2
#define ELASTICTIMESTREAM_VERSION_MINOR 4
#define ELASTICTIMESTREAM_VERSION_PATCH 0
// If any previous versions available E.g. #define ELASTICTIMESTREAM_PREVIOUS_VERSIONS ""
#undef ELASTICTIMESTREAM_PREVIOUS_VERSIONS

//
// Template macros
//
#define ELASTICTIMESTREAM_VERSION_CODE PACKAGE_VERSION_CODE(ELASTICTIMESTREAM_VERSION_MAJOR,ELASTICTIMESTREAM_VERSION_MINOR,ELASTICTIMESTREAM_VERSION_PATCH)
#ifndef ELASTICTIMESTREAM_PREVIOUS_VERSIONS
#define ELASTICTIMESTREAM_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(ELASTICTIMESTREAM_VERSION_MAJOR,ELASTICTIMESTREAM_VERSION_MINOR,ELASTICTIMESTREAM_VERSION_PATCH)
#else 
#define ELASTICTIMESTREAM_FULL_VERSION_LIST  ELASTICTIMESTREAM_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(ELASTICTIMESTREAM_VERSION_MAJOR,ELASTICTIMESTREAM_VERSION_MINOR,ELASTICTIMESTREAM_VERSION_PATCH)
#endif 

namespace elastictimestream 
{
	const std::string package  =  "elastictimestream";
	const std::string versions =  ELASTICTIMESTREAM_FULL_VERSION_LIST;
	const std::string description = "XDAQ plugin for enabling monitoring on time based indexes on ElasticSearch";
	const std::string authors = "Luciano Orsini, Penelope Roberts, Dainius Simelevicius";
	const std::string summary = "Elasticsearch XDAQ time based streamer";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
