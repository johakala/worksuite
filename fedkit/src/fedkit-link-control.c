/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-link-control.c,v 1.1 2007/10/09 08:19:01 cano Exp $
*/
static char *rcsid_link_control = "@(#) $Id: fedkit-link-control.c,v 1.1 2007/10/09 08:19:01 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_link_control);
#endif
#include "fedkit.h"

inline void _fedkit_link_error (int * status, int error)
{
	if (status != NULL) {
		*status = error;
	}
}

/**
 * Enables or disables a given link (never tested, but should work)
 * @param receiver pointer to the receiver
 * @param link 0 or 1 for the selection of the link, -1 for all the links, other invalid
 * 1 is invalid on single link receivers
 * @param enable 0 to disable the link(s), other to enable the link(s)
 * @return FK_OK in case of success of various error codes
 */
int fedkit_merge_link_enable (struct fedkit_receiver * receiver, int link, int enable)
{
	uint32_t target = 0;
	if (receiver == NULL) {
		return FK_error;
	}
	if (link == 0) {
		target = _FK_CSR_LINK0_ENABLE;
	} else if (link == 1) {
		target = _FK_CSR_LINK1_ENABLE;
	} else if (link == -1) {
		target = _FK_CSR_ALL_LINKS_ENABLE;
	} else {
		return FK_error;
	}
	if (enable) {
		receiver->map[_FK_CSR_OFFSET/4] |= target;
	} else {
		receiver->map[_FK_CSR_OFFSET/4] &= ~target;
	}
	return FK_OK;
}
