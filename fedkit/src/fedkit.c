/*
  fedkit.c
 
 Main source for fedkit library.

 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit.c,v 1.2 2009/02/27 17:19:02 cano Exp $

*/
static char *rcsid_fedkit_c = "@(#) $Id: fedkit.c,v 1.2 2009/02/27 17:19:02 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_fedkit_c);
#endif
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "fedkit.h"
#include "fedkit-private.h"
#include "fedkit-data-fifo.h"
#include "fedkit-wc-fifo.h"
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

/*** debug purposes */
#include <syslog.h>
#include <unistd.h>
#include "kalloc-tracker.h"

#ifndef NO_FEDKIT_PTHREAD
#ifndef __KERNEL__
#include <pthread.h>
#endif
#endif


#ifdef I2ODEBUG
unsigned fedkit_debuglevel = 0;
#endif

/**
 * @function _fedkit_error
 * @param code the error code
 * @param status pointer to the status (could nbe NULL)
 * this function is private to the imlementation of fedkit
 */
inline void _fedkit_error (int code, int * status)
{
    if (status)
        *status = code;
}

/**
 * @function _fedkit_enable_link
 * @param rec : the fedkit receiver strucuture
 */
inline void _fedkit_enable_link (struct fedkit_receiver * rec)
{
    /* disable and re-enam=ble the link (for new FRL-complyant models) */
    rec->map[_FK_CSR_OFFSET/4] |= _FK_CSR_SLINK_DOWN;
    sleep (_FK_SLINK_SLEEP);
    rec->map[_FK_CSR_OFFSET/4] |= _FK_CSR_SLINK_UP;
    /*rec->map[_FK_CSR_OFFSET/4] = rec->interrupts_enabled | _FK_CSR_LINK_CTRL;*/
    // sleep (_FK_SLINK_SLEEP);
    // rec->map[_FK_CSR_OFFSET/4] |= _FK_CSR_SOFT_RESET;
    rec->map[_FK_CSR_OFFSET/4] |= _FK_CSR_LINK_CTRL;
    rec->link_enabled = _FK_CSR_LINK_CTRL;
}

/**
 * @function _fedkit_disable_link
 * @param rec : the fedkit receiver strucuture
 */
inline void _fedkit_disable_link (struct fedkit_receiver * rec)
{
    rec->map[_FK_CSR_OFFSET/4] |= _FK_CSR_SLINK_DOWN;    
    rec->map[_FK_CSR_OFFSET/4] &= ~_FK_CSR_LINK_CTRL;
    /*rec->map[_FK_CSR_OFFSET/4] = rec->interrupts_enabled;*/
    rec->link_enabled = 0;
   
}

/* ---------------------------------------------------------------------- */

/** 
 * @function _fedkit_set_board_block_sizes
 *
 * Tells the fedkit PCI card about the block size we are using.
 */
 
inline void _fedkit_set_board_block_sizes (struct fedkit_receiver * rec)
{
    rec->map[_FK_BKSZ_OFFSET/4] = rec->block_size - rec->header_size;
}

/* ---------------------------------------------------------------------- */

/**
 * @function _fedkit_free_frag
 * frees the fragment strucuture itself. This function suposes the data blocks
 * are already freed. This will just free the data table and return the fragment 
 * structure in the free fragment pool of the receiver structure.
 * @param frag the fragment
 * @param receiver the receiver
 */
inline void _fedkit_free_frag (struct fedkit_fragment * frag, struct fedkit_receiver * receiver)
{
    idprintf ("Releasing fragment 0x%p\n", frag);
    if (frag->data_blocks != NULL) {
        free (frag->data_blocks);
        frag->data_blocks = NULL;
    }
    frag->block_number = -1;
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    frag->next = receiver->free_fragments;
    receiver->free_fragments = frag;
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
}

/**
 * @function _fedkit_alloc_frag
 * @param block_number : the number of blocks the framgnet will have to hold
 * @param receiver pointer to the receiver structure
 */
inline struct fedkit_fragment * _fedkit_alloc_frag (struct fedkit_receiver * receiver, int block_number)
{
    struct fedkit_fragment * ret = NULL;
	
    /* first find a structure */
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    if (receiver->free_fragments != NULL) {
        ret = receiver->free_fragments;
        receiver->free_fragments = ret->next;
        ret->next=NULL;
        idprintf ("Recycling fragment 0x%p\n", ret);
    } else {
        ret = (struct fedkit_fragment *)malloc(sizeof(struct fedkit_fragment)); 
        idprintf ("New fragment in game 0x%p\n", ret);
    }
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
	
    /* now allocate space to hold the pointers to data buffers (malloc all the time... improvable?) */
    if (ret != NULL) {
        ret->data_blocks = (struct _fedkit_data_block **) malloc (sizeof (struct _fedkit_data_block *[block_number]));
        if (ret->data_blocks == NULL) {
            _fedkit_free_frag (ret, receiver);
            ret = NULL;
        }
    }
    return ret;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_open
 * @return pointer to the fedkit struct or NULL in case of failure
 * @param index index to the board
 * @param status pointer to the integer holding status information
 *
 * 'open' a card for receiving slink data.
 *
 */
struct fedkit_receiver * fedkit_open (int index, int * status) /* native version */
{
    struct fedkit_receiver * rec;
    char filename [100];
    struct _fedkit_board_addresses addresses;
	
    /* First, allocation */
    rec = malloc (sizeof (struct fedkit_receiver));
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_init (&(rec->global_mutex), NULL);
    pthread_mutex_lock (&(rec->global_mutex));
#endif
    if (rec == NULL) {
        edprintf ("Could not allocate fedkit_receiver structure\n");
        _fedkit_error (FK_out_of_memory, status);
        goto FKO_alloc;
    }
    
    /* establish contact with the driver */
    sprintf (filename, "/dev/fedkit_receiver%d", index);
    rec->fd = open (filename, O_RDWR);
    if (rec->fd == -1) {
        edprintf ("Could not open fedkit-receiver special file\n");
        perror (filename);
        _fedkit_error (FK_IOerror, status);
        goto FKO_open;
    }

    /* The fact of opening the file succesfully means that the
       board is present, and now reserved */
    
    /* we now have to get the base addresses and to map them to the userspace (open step) */
    /* get base addresses */
    if (-1 == ioctl (rec->fd, FEDKIT_GET_BOARD_ADDRESSES, &addresses)) {
        edprintf ("Could not get adresses of receiver\n");
        perror (filename);
        _fedkit_error (FK_IOerror, status);
        goto FKO_addresses;
    }
    rec->base_address = addresses.base_address_0;
    rec->base_address2 = addresses.base_address_1;
    rec->FPGA_version = addresses.FPGA_version;
    rec->index = index;
    rec->suspended = 0;

    /* mmap the base addresses */
    rec->map = (uint32_t*)mmap (NULL, _FK_BAR0_range, PROT_READ | PROT_WRITE, MAP_SHARED, rec->fd, 
                           (__off_t) rec->base_address);
    if (rec->map == MAP_FAILED) {
        edprintf ("Could not map base address0\n");
        perror (filename);
        _fedkit_error (FK_IOerror, status);
        goto FKO_mmap;        
    }

    if (rec->base_address2 != 0) {
        rec->map2 = (uint32_t*)mmap (NULL, _FK_BAR1_range, PROT_READ | PROT_WRITE, MAP_SHARED, rec->fd, 
                                (__off_t) rec->base_address2);
        if (rec->map2 == MAP_FAILED) {
            edprintf ("Could not map second base address\n");
            perror (filename);
            _fedkit_error (FK_IOerror, status);
            goto FKO_mmap2;
        }
    } else {
        rec->map2 = MAP_FAILED;
    }
        
    /* first operation, software reset */
    idprintf ("Will reset\n");
    rec->map[_FK_CSR_OFFSET/4] |= _FK_CSR_SOFT_RESET;
    idprintf ("Have reseted with reset : 0x%08x\n", rec->map[_FK_CSR_OFFSET/4] | _FK_CSR_SOFT_RESET);
    
    /* Set the default receive pattern */
    rec->map[_FK_CSR_OFFSET/4] = (rec->map[_FK_CSR_OFFSET/4] & ~_FK_CSR_ALL_LINKS_ENABLE) | _FK_CSR_DEFAULT_LINK_PATTERN;
	
    /* block allocation et al. will be done in fedkit_start  after parameter setting by user */
	
    rec->block_size = FK_DEFAULT_BLOCK_SIZE;
    rec->block_number = FK_DEFAULT_BLOCK_NUMBER;
    rec->header_size = FK_DELAULT_HEADER_SIZE;
    rec->started = 0;
	
    rec->free_fragments = NULL;
	
    rec->link_enabled = 0;
    /*rec->interrupts_enabled = 0;*/
	
    rec->noalloc = 0;
	
    rec->free_data_block_descriptors = NULL;
    rec->allocated_data_blocks = NULL;
    rec->data_blocks_in_board = NULL;
    rec->data_blocks_physical = NULL;
    rec->data_fifo_physical = NULL;
    rec->data_fifo_user = MAP_FAILED;
    rec->data_blocks_user = MAP_FAILED;
    rec->wc_fifo_physical = NULL;
    
    /* now we initialise the headers */
    rec->data_blocks_in_board_insert = &(rec->data_blocks_in_board);
	
    rec->wc_fifo = MAP_FAILED;
    rec->data_fifo = NULL;
    rec->data_fifo_tab = NULL;
    /*rec->pmap_list = NULL;*/
    rec->data_list =NULL;
    
    idprintf ("\n");
	
#ifdef DEBUG_DATA_POINTERS
    rec->last_pointer_in = 0xFFFFFFFF;
    rec->min_pointer_in = 0xFFFFFFFF;
    rec->max_pointer_in = 0xFFFFFFFF;
    rec->last_pointer_out = 0xFFFFFFFF;
    rec->pointer_in_count = 0;
    rec->pointer_out_count = 0;
    iprintf ("Just initialised the pointer checking structure\n");
#endif
	
    rec->frag_header_size = FEDKIT_HEADER_SIZE;
    rec->frag_trailer_size = FEDKIT_TRAILER_SIZE;
	
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(rec->global_mutex));
#endif
    return rec;
    
    /* error conditions unpiling */
FKO_mmap2:
    if (rec->map != MAP_FAILED) munmap ((void *)rec->map, _FK_BAR0_range);
FKO_mmap:
FKO_addresses:
    close (rec->fd);
FKO_open:
    memset (rec, 0xEC, sizeof (struct fedkit_receiver));
    free (rec);
FKO_alloc:
    return NULL;
}

/* ---------------------------------------------------------------------- */

/**
 * @function _fedkit_alloc_data_fifo allocates the data FIFO
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 */
int _fedkit_handle_data_fifo (struct fedkit_receiver *receiver)
{
    int ret = FK_OK;

    if (receiver->data_fifo_user == NULL || receiver->data_fifo_user == MAP_FAILED) {
        edprintf ("in  _fedkit_handle_data_fifo data fifo was not allocated\n");
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKHDF_notalloc;
    }
	
    /* fifo allocated in the dbuff we have to pass its address to the driver.
     * we will comunicate with interrupt service routine through this struct.
     * the struct is located at the end of the data blocks
     */
    receiver->data_fifo = (struct _FK_data_fifo *) (receiver->data_fifo_user +
                                                    (receiver->block_number + 1) * sizeof (void *));
    receiver->data_fifo->read = 0;
    receiver->data_fifo->write = 0;
    receiver->data_fifo->done_read = 0;
    receiver->data_fifo->done_write = 0;
    receiver->data_fifo->int_disabled = 1;
    receiver->data_fifo->size = (receiver->block_number + 1);
    receiver->data_fifo_tab = receiver->data_fifo_user;
    return 0;
FKHDF_notalloc:
    return ret;
}

/* ---------------------------------------------------------------------- */

/**
 * @function _fedkit_alloc_data_blocks allocates the free blocks and feeds them to the FIFO
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 * This function is not called when using the noalloc version of fedkit_start
 */
int _fedkit_handle_data_blocks (struct fedkit_receiver *receiver)
{
    int ret = 0;
    int i;
	
    /* check that data zone was allocated */
    if (receiver->data_blocks_user == NULL || receiver->data_blocks_user == MAP_FAILED) {
        edprintf ("in _fedkit_handle_data_blocks : data buffers were not allocated\n");
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKHDB_free_FIFO;
    }
 
    /* loop over all fragment buffers */
    for (i=0; i< receiver->block_number; i++) {
        struct _fedkit_data_list * temp_list;
        struct _fedkit_data_block * temp_block;
		
        /* allocate the data block structure */
        temp_block = *(receiver->data_blocks_in_board_insert) = malloc (sizeof (struct _fedkit_data_block));
        if (*(receiver->data_blocks_in_board_insert) == NULL) {
            edprintf ("fedkit failure while allocating data block structures\n");
            _fedkit_error (FK_out_of_memory, &ret);
            goto FKHDB_free_data_blocks;
        }
		
        /* store all information in block.

	   TODO: this is where consecutiveness of the allocated memory is assumed. */
        
        // TODO: 64bit update here (size_t was int) 
        temp_block->user_address = (uint32_t *)((size_t)receiver->data_blocks_user + i * receiver->block_size);

        /* bus/physical address if for board use only. it points to data zone, after header */

        // TODO: 64bit update here (size_t was int) 
        temp_block->bus_address = (void *)((size_t)receiver->data_blocks_physical + i * receiver->block_size); 

        /* kernel address no used in fedkit -> don't care */
        temp_block->kernel_address = NULL;

        /* unused user_handle (this is for noalloc scheme) */
        temp_block->user_handle = NULL;
		
        /* add the block struct in the list - list is only used at deallocation time */
        temp_list = malloc (sizeof (struct _fedkit_data_list));
        if (temp_list == NULL) {
            free (temp_block);
            edprintf ("fedkit failure while allocating data block list structures\n");
            _fedkit_error (FK_out_of_memory, &ret);
            goto FKHDB_free_data_blocks;
        }
        temp_list->next = receiver->allocated_data_blocks;
        temp_list->block = temp_block;
        receiver->allocated_data_blocks = temp_list;
		
        /* push the block in the free fifo list (this should not fail) */
	// TODO: 64bit: size_t was int here
        if (_fedkit_feed_shared_fifo(receiver, (void *)((size_t)temp_block->bus_address + receiver->header_size))) {
            edprintf ("fedkit failure while filling the free FIFO (unexpected failure)\n");
            _fedkit_error (FK_out_of_memory, &ret);
            goto FKHDB_free_data_blocks;
        }
		
        /* still have to reference the block in our own queue */
        *(receiver->data_blocks_in_board_insert) = temp_block;
        temp_block->next = NULL;
        receiver->data_blocks_in_board_insert = &(temp_block->next);

    } /* loop over all fragment buffers */

#ifdef DEBUG_DATA_POINTERS
    _fedkit_pointer_ubercheck (receiver, "_fedkit_alloc_data_blocks");
#endif
    return 0;
	
FKHDB_free_data_blocks:
    /*_fedkit_dealloc_data_blocks (receiver);*/
FKHDB_free_FIFO:
    return -1;
}

/* ---------------------------------------------------------------------- */

/**
 * @function _fedkit_alloc_datablock_descriptor recycles or allocates a data block descriptor
 * @param receiver pointer to the reciever structure
 * @returns pointer to struct _fedkit_data_block
 */
 
struct _fedkit_data_block * _fedkit_alloc_datablock_descriptor (struct fedkit_receiver * receiver)
{
    struct _fedkit_data_block * ret;
    if (receiver->free_data_block_descriptors) {
        ret = receiver->free_data_block_descriptors;
        receiver->free_data_block_descriptors = ret->next;
    } else {
        ret = (struct _fedkit_data_block * )malloc (sizeof (struct _fedkit_data_block));
    }
    return ret;
} 


/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_start 
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 *
 * Starts the reception of fragments.
 *
 *
 */
int fedkit_start (struct fedkit_receiver *receiver) /* native version */
{
    int ret = 0;
    struct _fedkit_receiver_parameters params;
    struct _fedkit_memory_blocks mem_blocks;
    
    if (receiver == NULL) {
        edprintf ("NULL receiver\n");
        _fedkit_error (FK_IOerror, &ret);
        return ret;
    }
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    idprintf ("\n");
    if (receiver->started != 0) {
        edprintf ("fedkit receiver already started\n");
        _fedkit_error (FK_IOerror, &ret);
        goto FKS_lock;
    }

    /* -------------------- */

    /* we start ! no subsequent changes on the block sizes */
    idprintf ("\n");
    receiver->started = 1;
    receiver->noalloc = 0;
	
    /* we set the block size in the board (block size - header size) */
    idprintf ("will set block sizes\n");
    /*sleep (1);*/
    _fedkit_set_board_block_sizes (receiver);

    /* Pass the parameters to the driver */
    params.block_number = receiver->block_number;
    params.block_size = receiver->block_size - receiver->header_size;
    params.do_allocate_blocks = 1;
    if (ioctl (receiver->fd, FEDKIT_SETGET_RECEIVER_PARAMS, &params)) {
        _fedkit_error (FK_invalid_parameter, &ret);
        goto FKS_params;
    }

    /* we ask the driver to allocate the blocks in memory for us */
    if (ioctl (receiver->fd, FEDKIT_GET_RECEIVER_MEMBLOCKS, &mem_blocks)) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKS_nomem;
    }

    /* -------------------- */

    /* now memory map and take note of the result (we are in alloc mode)*/
    /* sanity check */
    if (mem_blocks.wc_fifo_physical == NULL 
        || mem_blocks.free_blocks_fifo_physical == NULL
        || mem_blocks.data_blocks_physical == NULL) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKS_nomem;
    }

    /* wc_fifo */
    receiver->wc_fifo_physical = mem_blocks.wc_fifo_physical;

    // this internally calls kfedkit_mmap
    receiver->wc_fifo = mmap (NULL, sizeof (struct _FK_wc_fifo), PROT_READ | PROT_WRITE,
                              MAP_SHARED, receiver->fd, (__off_t) receiver->wc_fifo_physical);
    if (receiver->wc_fifo == MAP_FAILED) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKS_mmap;
    }

    /* data_fifo the data fifo is prepared by the driver */
    receiver->data_fifo_physical = mem_blocks.free_blocks_fifo_physical;
    receiver->data_fifo_user = mmap (NULL, 

                                     /** this quantity was previously called shared_fifo_size

                                         TODO: check: it looks like the size argument does not 
                                               exactly match the one which is used for other 
                                               mmap and munmap calls ?! (receiver->block_number+1 
                                               is used here while receiver->block_number elsewhere ?)
                                       */
                                     sizeof (struct _FK_data_fifo) +
                                     receiver->block_number * sizeof (void *), 


                                     PROT_READ | PROT_WRITE,
                                     MAP_SHARED, receiver->fd, 
                                     (__off_t) receiver->data_fifo_physical);
    if (receiver->data_fifo_user == MAP_FAILED) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKS_mmap;
    }
    receiver->data_fifo = (struct _FK_data_fifo *) ((void *)receiver->data_fifo_user + 
                                                    (receiver->block_number + 1) * sizeof (void *));
    receiver->data_fifo_tab  = receiver->data_fifo_user;
    /* data_blocks */
    receiver->data_blocks_physical = mem_blocks.data_blocks_physical;
    receiver->data_blocks_user = mmap (NULL, receiver->block_size * receiver->block_number, 
                                       PROT_READ | PROT_WRITE,
                                       MAP_SHARED, receiver->fd, 
                                       (__off_t) receiver->data_blocks_physical);
    if (receiver->data_blocks_user == MAP_FAILED) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKS_mmap;
    }

    if (_fedkit_handle_data_fifo (receiver)) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKS_data_fifo;
    }

    if (_fedkit_handle_data_blocks (receiver)) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKS_data_blocks;
    }
    /* data fifo was already registered automatically */
	
    /* take care of the wc fifo */
    receiver->wc_fifo->read = _FK_wc_fifo_mask;
    receiver->wc_fifo->write = _FK_wc_fifo_mask;

    /* give address of wc FIFO to board */
    _fedkit_send_wc_fifo_params_to_board (receiver);

    /* -------------------- */
	
    /* we just have to enable the interrupts and the board will start to ask for data blocks */
#ifndef FEDKIT_NO_INTERRUPT
    _fedkit_enable_FIFO_interrupts (receiver);    
#else
    _fedkit_noint_handle_blocks (receiver);
#endif

    /* enable the link */
      _fedkit_enable_link (receiver);
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
    return ret; /* happy */
	
    /* -------------------- */

FKS_data_blocks:
FKS_data_fifo:
FKS_mmap:
    /* something went wrong so we unmap everything that can be unmapped */
    if (receiver->wc_fifo != MAP_FAILED) munmap (receiver->wc_fifo, sizeof (struct _FK_wc_fifo));
    if (receiver->data_fifo_user != MAP_FAILED) munmap (receiver->data_fifo_user, sizeof (struct _FK_data_fifo) +
                                                  receiver->block_number * sizeof (void *));
    if (receiver->data_blocks_user != MAP_FAILED) munmap (receiver->data_blocks_user, receiver->block_size * receiver->block_number);
FKS_nomem:
FKS_params:
FKS_lock:
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
    receiver->started = 0; /* something failed, we're not started */
    return ret;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_start_noalloc
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 */
int fedkit_start_noalloc (struct fedkit_receiver *receiver) /* native version */
{
    int ret = 0;
    struct _fedkit_receiver_parameters params;
    struct _fedkit_memory_blocks mem_blocks;
    
    if (receiver == NULL) {
        edprintf ("NULL receiver\n");
        _fedkit_error (FK_IOerror, &ret);
        goto FKSNA_open;
    }
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    idprintf ("\n");
    if (receiver->started != 0) {
        edprintf ("fedkit receiver already started\n");
        _fedkit_error (FK_IOerror, &ret);
        goto FKSNA_lock;
    }
    /* we start ! no subsequent changes on the block sizes */
    idprintf ("\n");
    receiver->started = 1;
    receiver->noalloc = 1;
	
    /* we set the block size in the board (block size - header size) */
    idprintf ("will set block sizes\n");
    /*sleep (1);*/
    _fedkit_set_board_block_sizes (receiver);
    /* Pass the parameters to the driver */
    params.block_number = receiver->block_number;
    params.block_size = receiver->block_size - receiver->header_size;
    params.do_allocate_blocks = 0;
    if (ioctl (receiver->fd, FEDKIT_SETGET_RECEIVER_PARAMS, &params)) {
        _fedkit_error (FK_invalid_parameter, &ret);
        goto FKSNA_params;
    }
    /* we ask the driver to allocate the blocks in memory for us */
    if (ioctl (receiver->fd, FEDKIT_GET_RECEIVER_MEMBLOCKS, &mem_blocks)) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKSNA_nomem;
    }
    /* now memory map and take note of the result (we are in alloc mode)*/
    /* sanity check */
    if (mem_blocks.wc_fifo_physical == NULL 
        || mem_blocks.free_blocks_fifo_physical == NULL) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKSNA_nomem;
    }
    /* wc_fifo */
    receiver->wc_fifo_physical = mem_blocks.wc_fifo_physical;
    receiver->wc_fifo = mmap (NULL, sizeof (struct _FK_wc_fifo), PROT_READ | PROT_WRITE,
                              MAP_SHARED, receiver->fd, (__off_t) receiver->wc_fifo_physical);
    if (receiver->wc_fifo == MAP_FAILED) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKSNA_mmap;
    }
    /* data_fifo don't take care of the dat blocks. Nothing allocated */
    /* receiver->data_blocks_physical = mem_blocks.data_blocks_physical; */
    receiver->data_blocks_physical = NULL;
    receiver->data_blocks_user = MAP_FAILED;
    
    /* Take care of the data blocks FIFO */
    receiver->data_fifo_physical = mem_blocks.free_blocks_fifo_physical;
    receiver->data_fifo_user = mmap (NULL, sizeof (struct _FK_data_fifo) +
                                     receiver->block_number * sizeof (void *), 
                                     PROT_READ | PROT_WRITE,
                                     MAP_SHARED, receiver->fd, 
                                     (__off_t) receiver->data_fifo_physical);
    if (receiver->data_fifo_user == MAP_FAILED) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKSNA_mmap;
    }

    if (_fedkit_handle_data_fifo (receiver)) {
        _fedkit_error (FK_out_of_DMA_memory, &ret);
        goto FKSNA_data_fifo;
    }

    /* data fifo was already registered automatically */
	
    /* take care of the wc fifo */
    receiver->wc_fifo->read = _FK_wc_fifo_mask;
    receiver->wc_fifo->write = _FK_wc_fifo_mask;
    /* give address of wc FIFO to board */
    _fedkit_send_wc_fifo_params_to_board (receiver);
	
    /* we just have to enable the interrupts and the board will start to ask for data blocks */
#ifndef FEDKIT_NO_INTERRUPT
    _fedkit_enable_FIFO_interrupts (receiver);    
#else
    _fedkit_noint_handle_blocks (receiver);
#endif

    /* enable the link */
     _fedkit_enable_link (receiver);
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
    return ret; /* happy */
	
FKSNA_data_fifo:
FKSNA_mmap:
    /* something went wrong so we unmap everything that can be unmapped */
    if (receiver->wc_fifo != MAP_FAILED) munmap (receiver->wc_fifo, sizeof (struct _FK_wc_fifo));
    if (receiver->data_fifo_user != MAP_FAILED) munmap (receiver->data_fifo_user, sizeof (struct _FK_data_fifo) +
                                                  receiver->block_number * sizeof (void *));
    if (receiver->data_blocks_user != MAP_FAILED) munmap (receiver->data_blocks_user, receiver->block_size * receiver->block_number);
FKSNA_nomem:
FKSNA_params:
FKSNA_lock:
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
FKSNA_open:
    return ret;
}

//----------------------------------------------------------------------

int fedkit_provide_block (struct fedkit_receiver * receiver, void *user_address, 
                          void *kernel_address, void *bus_address, void *user_handle)
{
    struct _fedkit_data_block * temp_block;
    int ret = 0;
	
    if (receiver == NULL) {
        eprintf ("Trying to provide blocks to NULL receiver in fedkit_provide_block\n");
        return FK_error;
    }

    if (receiver->suspended) {
        edprintf ("Trying to provide blocks to a suspended receiver. Blocks not provided\n");
        return FK_suspended;
    }
	
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    /* allocate the data block structure */
    temp_block = _fedkit_alloc_datablock_descriptor (receiver);
    if (temp_block == NULL) {
        edprintf ("fedkit failure while allocating data block structures\n");
        _fedkit_error (FK_out_of_memory, &ret);
        goto FKPB_block_alloc;
    }
	
    /* store all information in block */
    temp_block->user_address = user_address;

    /* bus address if for board use only. it points to data zone, after header */
    temp_block->bus_address = bus_address; 

    /* kernel address */
    temp_block->kernel_address =  kernel_address;

    /* unused user_handle (this is for noalloc scheme) */
    temp_block->user_handle = user_handle;

    /* push the block in the free fifo list (this should not fail) */
    // TODO: 64bit update here (size_t was int) 
    if (_fedkit_feed_shared_fifo(receiver, 
				 (void *)((size_t)temp_block->bus_address + receiver->header_size))) 
    {
        edprintf ("fedkit failure while filling the free FIFO in fedkit_provide_block\n");
        _fedkit_error (FK_out_of_memory, &ret);
        free (temp_block);
        goto FKPB_fifo_full;
    }

    /* still have to reference the block in our own queue */
    *(receiver->data_blocks_in_board_insert) = temp_block;
    temp_block->next = NULL;
    receiver->data_blocks_in_board_insert = &(temp_block->next);
	
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif

#ifdef DEBUG_DATA_POINTERS
    _fedkit_pointer_ubercheck (receiver, "fedkit_provide_block");
#endif
#ifndef FEDKIT_NO_INTERRUPT
    _fedkit_enable_FIFO_interrupts (receiver);
#else
    _fedkit_noint_handle_blocks (receiver);
#endif
	
FKPB_fifo_full:
FKPB_block_alloc:
    pthread_mutex_unlock (&(receiver->global_mutex));
    return ret;
}

//----------------------------------------------------------------------
						 
/**
 * @function fedkit_frag_user_handle
 * @return pointer provided by the user for this block or NULL if no handle was provided/index is out of range
 * @param fragment pointer to the framgent
 * @param index index to the block for which the user handle is wanted
 */
void * fedkit_frag_user_handle (struct fedkit_fragment * frag, int index)
{
    if (frag == NULL) return NULL;
    if ((index < 0) || (index >= frag->block_number)) return NULL;
    if (frag->data_blocks == NULL) {
        eprintf ("Got NULL pointer as frag->block_number in fedkit_frag_user_handle.\n");
        return NULL;
    }
    return frag->data_blocks[index]->user_handle;
}

/**
 * @function fedkit_frag_release_norecycle
 * @param fragment pointer to the fragment structure
 */
void fedkit_frag_release_norecycle (struct fedkit_fragment * fragment)
{
    int i;

    idprintf ("fedkit frag release\n");
    if (fragment == NULL) {
        return;
    } else if (fragment->block_number == -1) {
        eprintf ("Got -1 as block number in fedkit_frag_release_norecycle. Perhaps a double release of the fragment?\n");
        return;
    } else if (fragment->data_blocks == NULL) {
        eprintf ("Got NULL pointer as frag->block_number in fedkit_frag_release_norecycle.\n");
        return;
    } else if (fragment->receiver == NULL){
        eprintf ("Got NULL pointer as frag->receiver in fedkit_frag_release_norecycle.\n");
        return;
    } else {
        idprintf ("bn= %d\n", fragment->block_number);
#ifndef NO_FEDKIT_PTHREAD
        pthread_mutex_lock (&(fragment->receiver->global_mutex));
#endif
        for (i=0;i< fragment->block_number; i++) {
            _fedkit_free_datablock_descriptor (fragment->receiver, fragment->data_blocks[i]);
        }
#ifndef NO_FEDKIT_PTHREAD
        pthread_mutex_unlock (&(fragment->receiver->global_mutex));
#endif
        _fedkit_free_frag (fragment, fragment->receiver);
    }
}

/**
 * @function fedkit_link_is_up returns non zero if the links reports to be up
 * @param receiver pointer to the receiver structure
 * This function only works on some modifierfedkit receivers (user shoud know if he has one)
 * @return 0 if the lin is not reported as up by the hardware, non-zero otherwise
 */
int fedkit_link_is_up (struct fedkit_receiver * receiver)
{
    return receiver->map[_FK_CSR_OFFSET/4] & _FK_CSR_LINK_IS_UP;
}

/**
 * @function fedkit_set_block_size
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param block_size the block size in bytes (has to be 64 bits aligned)
 */
int fedkit_set_block_size (struct fedkit_receiver * receiver, int block_size) {
    if (receiver->started != 0) {
        edprintf ("fedkit_set_block_size : board already started\n");
        return FK_board_already_started;
    } else if (block_size > FK_MAX_BLOCK_SIZE) {
        edprintf ("fedkit_set_block_size : size too big\n");
        return FK_size_too_big;
    } else if (block_size < FK_MIN_BLOCK_SIZE || receiver->header_size >= block_size) {
        edprintf ("fedkit_set_block_size : size too small\n");
        return FK_size_too_small;
    } else if ((block_size & ~FK_SIZE_ALIGN_MASK) != 0) {
        edprintf ("fedkit_set_block_size : size not aligned\n");
        return FK_size_not_aligned;
    }
    receiver->block_size = block_size;
    return 0;
}

/**
 * @function fedkit_get_block_size
 * @return positive block size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int fedkit_get_block_size (struct fedkit_receiver * receiver)
{
    return receiver->block_size;
}

/**
 * @function fedkit_set_block_number
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param block_number the block number to be allocated
 */
int fedkit_set_block_number (struct fedkit_receiver * receiver, int block_number)
{
    if (receiver->started != 0) {
        edprintf ("fedkit_set_block_number : board already started\n");
        return FK_board_already_started;
    } else if (block_number < 2 * _FK_free_data_fifo_depth) {
        edprintf ("fedkit_set_block_number : number too small (%d at least)\n", _FK_free_data_fifo_depth * 2);
        return FK_number_too_small;
    }
    receiver->block_number = block_number;
    return 0;
}

/**
 * @function fedkit_get_block_number
 * @return positive block size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int fedkit_get_block_number (struct fedkit_receiver * receiver)
{
    return receiver->block_number;
}

/**
 * @function fedkit_set_header_size
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param header_size the header size in bytes (has to be 64 bits aligned)
 */
int fedkit_set_header_size (struct fedkit_receiver * receiver, int header_size)
{
    if (receiver->started != 0) {
        edprintf ("fedkit_set_header_size : board already started\n");
        return FK_board_already_started;
    } else if (header_size > FK_MAX_BLOCK_SIZE || header_size >= receiver->block_size) {
        edprintf ("fedkit_set_header_size : size too big\n");
        return FK_size_too_small;
    } else if ((header_size & ~FK_SIZE_ALIGN_MASK) != 0) {
        edprintf ("fedkit_set_header_size : size not aligned\n");
        return FK_size_not_aligned;
    }
    receiver->header_size = header_size;
    return 0;
}

/**
 * @function fedkit_get_header_size
 * @return positive header size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int fedkit_get_header_size (struct fedkit_receiver * receiver)
{
    return receiver->header_size;
}


/**
 * @function fedkit_set_receive_pattern actually tries to put the pattern in the hardware
 * @return zero on success (FK_OK), negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param header_size the header size in bytes (has to be 64 bits aligned)
 */
int fedkit_set_receive_pattern (struct fedkit_receiver * receiver, int receive_pattern)
{
    if (receiver == NULL) {
        return FK_error;
    }
    fedkitdump_start(receiver);
    receive_pattern = (receive_pattern << _FK_CSR_LINKS_SHIFT) & _FK_CSR_ALL_LINKS_ENABLE;
    if (!(receive_pattern & _FK_CSR_ALL_LINKS_ENABLE)) {      /* The user tries to enable no link. */
        return FK_invalid_pattern;
    }
    receiver->map[_FK_CSR_OFFSET/4] = 
        (receiver->map[_FK_CSR_OFFSET/4] & ~_FK_CSR_ALL_LINKS_ENABLE) | receive_pattern;
    return FK_OK;
}

/**
 * @function fedkit_get_receive_pattern retrive the current pattern from the hardware
 * @return positive header size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int fedkit_get_receive_pattern (struct fedkit_receiver * receiver)
{
    return (receiver->map[_FK_CSR_OFFSET/4] & _FK_CSR_ALL_LINKS_ENABLE) >> _FK_CSR_LINKS_SHIFT;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_get_fragment 
 * @returns pointer to the fragment structure or NULL on error
 * @param receiver pointer to the fedkit receiver structure
 * @param status pointer to the integer holding status information
 */
struct fedkit_fragment * fedkit_frag_get (struct fedkit_receiver * receiver, int * status)
{
    struct _FK_wc_fifo_element wc_element;
    struct fedkit_fragment * ret = NULL;
    int i;
	
    if (receiver==NULL) {
        eprintf ("Trying to frag get on a NULL receiver.");
        return NULL; 
    }
    if (receiver->suspended) {
        edprintf ("Trying to get fragment from a suspended receiver. Not fragment provided\n");
        _fedkit_error (FK_suspended, status);
        return NULL;
    } 

#ifdef FEDKIT_NO_INTERRUPT
    _fedkit_noint_handle_blocks (receiver);
#endif

    wc_element = _fedkit_pop_wc (receiver);
    idprintf ("Popped word count element: {wc=%d,no_fragment=%d,truncated=%d,CRC_error=%d}\n", wc_element.wc, wc_element.no_fragment, 
              wc_element.truncated, wc_element.CRC_error);
    if (wc_element.no_fragment) {
        edprintf ("fedkit get fragment : word count FIFO empty\n");
        _fedkit_error (FK_no_event_available, status);
    } else {
        int block_number = (wc_element.wc -1) / (receiver->block_size - receiver->header_size) + 1;
		
        idprintf ("Poped word count : %d\n", wc_element.wc);
        if (NULL == (ret = _fedkit_alloc_frag (receiver, block_number))) {
            edprintf ("fedkit get fragment : word count FIFO empty\n");
            _fedkit_error (FK_no_event_available, status);
        } else {
            idprintf("Alloc OK\n");
            ret->FED_number = FEDKIT_FRAGMENT_UNANALYSED;
            ret->receiver = receiver;
            ret->block_size = receiver->block_size;
            ret->header_size = receiver->header_size;
            ret->wc = wc_element.wc;
            ret->block_number = block_number;
            ret->truncated = wc_element.truncated;
	    ret->FEDCRC_error = wc_element.FEDCRC_error;
            ret->CRC_error = wc_element.CRC_error;
            idprintf ("frag : bs=%d, hs=%d, wc=%d, bn=%d\n",
                      ret->block_size, ret->header_size, ret->wc, ret->block_number);
            for (i=0; i<block_number; i++) {
                ret->data_blocks[i] = _fedkit_pop_block_in_board (receiver);
                idprintf ("block = 0x%p\n", ret->data_blocks[i]->bus_address);
                if (ret->data_blocks[i] == NULL) { /* big problem, we should have a block available... 
                                                      everything is likely to be broken */
                    int j;
					
                    edprintf ("Major internal error : blocks registered as in board are less than expected\n");
                    eprintf ("Major internal error : blocks registered as in board are less than expected\n");
                    fedkit_dump_info (receiver);
                    _fedkit_error (FK_internal_error, status);
                    for (j=i-1; j>=0; j--) {
                        _fedkit_free_data_block (receiver, ret->data_blocks[j]);
                    }
                    _fedkit_free_frag (ret, receiver);
                    ret = NULL;
                    break;
                }

#ifdef DEBUG_DATA_POINTERS
                {
                    uint32_t pointer = (uint32_t)ret->data_blocks[i]->bus_address + ret->receiver->header_size;
					
                    ret->receiver->map[0x14/4] = pointer;

                    if (ret->receiver->min_pointer_in > pointer) {
                        eprintf ("POINTER_DEBUG : pointer_out out of range min=%x, now=%x\n", 
                                 ret->receiver->min_pointer_in, pointer);
                    }
                    if (ret->receiver->max_pointer_in < pointer) {
                        eprintf ("POINTER_DEBUG : pointer_out out of range max=%x, now=%x\n", 
                                 ret->receiver->max_pointer_in, pointer);
                    }
                    if (ret->receiver->last_pointer_out == 0xFFFFFFFF) {
                        ret->receiver->last_pointer_out = pointer;
                    } else if (ret->receiver->last_pointer_out + ret->receiver->block_size + 
                               (ret->receiver->noalloc?4096:0)!= pointer) {
                        if ((ret->receiver->last_pointer_out == ret->receiver->max_pointer_in) && 
                            (ret->receiver->min_pointer_in == pointer)) {
                            /* normal case, happy */
                        } else {
                            eprintf ("POINTER_DEBUG : unexpected pointer_out min=%x, max=%x, last=%x, expected=%x, block=%x had=%x\n", 
                                     ret->receiver->min_pointer_in, ret->receiver->max_pointer_in, ret->receiver->last_pointer_out, 
                                     ret->receiver->last_pointer_out + ret->receiver->block_size+ (ret->receiver->noalloc?4096:0),
                                     ret->receiver->block_size, pointer);
                        }
                    }
                    ret->receiver->last_pointer_out = pointer;
                    ret->receiver->pointer_out_count++;
                    if (ret->receiver->pointer_out_count > ret->receiver->pointer_in_count) {
                        eprintf ("POINTER_DEBUG : pointer out count> pointer in count: out: %x in: %x\n", 
                                 ret->receiver->pointer_out_count, ret->receiver->pointer_in_count);
                    }
                }
                _fedkit_pointer_ubercheck (ret->receiver, "fedkit_frag_get");
#endif


            } /* loop from 0 to block_number-1 */
        } /* if successfully _fedkit_alloc_frag */
    }
    return ret;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_frag_release
 * @param fragment the framgment to be freed
 * frees the fragment space and data blocks to the right receiver (original receiver)
 */
void fedkit_frag_release (struct fedkit_fragment * fragment)
{
    int i;
	
    if (fragment->receiver->suspended) {
        edprintf ("Trying to release a fragment from a suspended block. No block recycled\n");
        return;
    }

    idprintf ("fedkit frag release\n");
    if (fragment == NULL) {
        return;
    } else if (fragment->block_number == -1) {
        eprintf ("Got -1 as block number in fedkit_frag_release. Perhaps a double release of the fragment?\n");
        return;
    } else if (fragment->data_blocks == NULL) {
        eprintf ("Got NULL pointer as frag->block_number in fedkit_frag_release.\n");
        return;
    } else {
        idprintf ("bn= %d\n", fragment->block_number);
        for (i=0;i< fragment->block_number; i++) {
            /* push the block in the free fifo list (this should not fail) */
            _fedkit_free_data_block (fragment->receiver, fragment->data_blocks[i]);
            /*if (_fedkit_feed_shared_fifo(fragment->receiver, fragment->data_blocks[i]->bus_address)) {
              eprintf ("puting event back in the free FIFO (unexpected failure)\n");
              edprintf ("puting event back in the free FIFO (unexpected failure)\n");*/
				/* the block is lost until we close receiver */
            /*} else {
              idprintf ("Pushing back block 0x%08x\n", (int)fragment->data_blocks[i]);*/
				/* still have to reference the block in our own queue */
				/**(fragment->receiver->data_blocks_in_board_insert) = fragment->data_blocks[i];
                                   fragment->data_blocks[i]->next = NULL;
                                   fragment->receiver->data_blocks_in_board_insert = &(fragment->data_blocks[i]->next);
                                   fragment->data_blocks[i] = NULL;
                                   }*/
            fragment->data_blocks[i] = NULL;
        }
        idprintf ("\n");
#ifndef FEDKIT_NO_INTERRUPT
        _fedkit_enable_FIFO_interrupts (fragment->receiver); /* RE-enable the interrupt, in case of... */   
#else
        _fedkit_noint_handle_blocks (fragment->receiver);
#endif
        _fedkit_free_frag (fragment, fragment->receiver);
    }
}

/**
 * @function fedkit_close
 * @param receiver the receiver relese
 * stop activity from the board. frees up the memory. This function is silent on errors because
 * they are unrecoverable and only imply failing feeing of resources.
 */
void fedkit_close (struct fedkit_receiver *receiver) /* native version */
{
    struct _fedkit_data_block * db;

    
    idprintf ("fedkit close 1\n");
    if (receiver == NULL) return;
    if ((((size_t)(receiver->map)) & 0xFFFFFFFF) == 0xECECECEC) {
        eprintf ("WARNING: probably trying to close an already closed receiver. This program might segfault.\n");
    }
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif

    idprintf ("fedkit close 2\n");

    // TODO: shouldn't these move to the kernel driver (e.g. to 
    // kfedkit_unregister_receiver and kfedkit_unregister_sender) ? 

    // COMMENTED OUT 2011-06-23
    // receiver->map[_FK_CSR_OFFSET/4] &= ~(_FK_CSR_HALF_EMPTY_INT_ENABLE | _FK_CSR_EMPTY_INT_ENABLE);
#ifndef FEDKIT_NO_RESET
    // receiver->map[_FK_CSR_OFFSET/4] |= _FK_CSR_SOFT_RESET;
#endif
	
    while (receiver->allocated_data_blocks != NULL) {
        struct _fedkit_data_list * temp_list = receiver->allocated_data_blocks;
        free (temp_list->block);
        receiver->allocated_data_blocks = temp_list->next;
        free (temp_list);
    }
    idprintf ("fedkit close 4\n");
    while (receiver->free_fragments != NULL) {
        struct fedkit_fragment * temp = receiver->free_fragments;
        receiver->free_fragments = receiver->free_fragments->next;
        free (temp);
    }
	
    /* cleanup free data block descriptors */
    while (receiver->free_data_block_descriptors != NULL) {
        db = receiver->free_data_block_descriptors;
        receiver->free_data_block_descriptors = db->next;
        free (db);
    }
	
    /* cleanup data blocks in board in case of noalloc scheme */
    // TODO: CHECK WHETHER THIS FREED MEMORY IS WHAT THE 
    //       CARD IS ASKED TO WRITE TO ?!
    if (receiver->noalloc) {
        while (receiver->data_blocks_in_board != NULL) {
            db = receiver->data_blocks_in_board;
            receiver->data_blocks_in_board = db->next;
            free (db);
        }
    }

    /* unmap all the memory that used to be maped */
    if (receiver->wc_fifo != MAP_FAILED) munmap (receiver->wc_fifo, sizeof (struct _FK_wc_fifo));
    if (receiver->data_fifo_user != MAP_FAILED) munmap (receiver->data_fifo_user, sizeof (struct _FK_data_fifo) +
                                                  receiver->block_number * sizeof (void *));
    if (receiver->data_blocks_user != MAP_FAILED) munmap (receiver->data_blocks_user, receiver->block_size * receiver->block_number);
    if (receiver->map != MAP_FAILED) munmap ((void *)receiver->map, _FK_BAR0_range);
    if (receiver->map2 != MAP_FAILED) munmap ((void *)receiver->map2, _FK_BAR1_range);
	
    idprintf ("fedkit close 5\n");

    // this will call the corresponding kernel function to
    // release the board
    close (receiver->fd);
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
    pthread_mutex_destroy (&(receiver->global_mutex));
#endif
    memset (receiver, 0xEC, sizeof (struct fedkit_receiver));
    free (receiver);
    idprintf ("fedkit close finished\n");
}


/**
 * Comparson is done on a byte-by-byte basis
 * @param fragment the fragment to compare with the expected data
 * @param expected_data a pointer to the buffer containing expected data (DAQ headers
 * are part of the expected data
 * @param expected_size size of the data in the expected_buffer, headers included 
 * (so expected_size = fragment_size + 3*8)
 * @param error_position placeholder for position of non-matching byte
 * @return error code corresponding to the found error
 */
int fedkit_frag_check (struct fedkit_fragment * fragment, void * expected_data, int expected_size, int *error_position)
{
    int i=0;

    if (expected_size < fedkit_frag_size (fragment)) {
        *error_position = -1;
        return FK_size_too_big;
    } else if (expected_size > fedkit_frag_size (fragment)) {
        *error_position = -1;
        return FK_size_too_small;
    } else {
        for (i=0; i<(fedkit_frag_size (fragment) + 3*8); i++) {
            if (((char *)(fragment->data_blocks[i/fragment->block_size]->user_address))[i%fragment->block_size]
                != ((char *)expected_data)[i]) {
                *error_position = i;
                return FK_data_mismatch;
            }
        }
    }
    *error_position = -1;
    return FK_OK;
}

/**
 * @function fedkit dumps various receiver information 
 * @param receiver pointer to the receiver structure
 */
void fedkit_dump_info (struct fedkit_receiver * rec)
{
    int prev_suspend;
	
    if (rec == NULL) {
        iprintf ("*** fedkit receiver dump  : NULL receiver\n");
        return;
    } else if (!(rec->started)) {
        iprintf ("*** fedkit receiver dump  : not started\n");
        return;
    }
    if (rec->wc_fifo != NULL && rec->wc_fifo != MAP_FAILED) {
        iprintf ("*** fedkit receiver dump  : \n"
                 " - wc FIFO read=0x%08x, write=0x%08x\n",
                 rec->wc_fifo->read, rec->wc_fifo->write);
    }
    if (rec->data_fifo != NULL) {
        iprintf (" - fedkit DATA fifo dump : \n"
                 "    read       = 0x%08x\n"
                 "    write      = 0x%08x\n"
                 "    size       = 0x%08x\n"
                 "    done_read  = 0x%08x\n"
                 "    done_write = 0x%08x\n",
                 rec->data_fifo->read,
                 rec->data_fifo->write,
                 rec->data_fifo->size,
                 rec->data_fifo->done_read,
                 rec->data_fifo->done_write);
        iprintf ("fedkit block fifo free slot : %d\n", 
                 _fedkit_fifo_free_slot_count(rec->data_fifo->write, rec->data_fifo->read, rec->data_fifo->size));
    }
#ifdef DEBUG_DATA_POINTERS
    iprintf ("in m/l/M %08x/%08x/%08x out l %08x\n",
             rec->min_pointer_in, rec->last_pointer_in, rec->max_pointer_in,
             rec->last_pointer_out);
    iprintf ("incount = %x, out_count = %x\n",
             rec->pointer_in_count, rec->pointer_out_count);
#endif
    prev_suspend = rec->suspended;
    rec->suspended = 1;
    iprintf ("pointers in fedkit = %d\n", fedkit_susp_get_handle_number(rec));
    rec->suspended = prev_suspend;
    iprintf ("\n\n");
}


/**
 * @function fedkit_get_error_string returns a const char * containting the representation of the error.
 * @param error_number error code to translate to string
 */
/* my perl command to extract this from fedkit.h's enum definition:
perl -e 'for (<>) { if (/(FK_[a-zA-Z_]*)/) {print "case $1:\nreturn \"$1\";\nbreak;\n";}}'
*/
const char * fedkit_get_error_string (int error_number)
{
    switch (error_number) {
    case FK_OK:
        return "FK_OK";
        break;
    case FK_IOerror:
        return "FK_IOerror";
        break;
    case FK_out_of_memory:
        return "FK_out_of_memory";
        break;
    case FK_size_too_small:
        return "FK_size_too_small";
        break;
    case FK_size_too_big:
        return "FK_size_too_big";
        break;
    case FK_number_too_small:
        return "FK_number_too_small";
        break;
    case FK_size_not_aligned:
        return "FK_size_not_aligned";
        break;
    case FK_board_already_started:
        return "FK_board_already_started";
        break;
    case FK_out_of_DMA_memory:
        return "FK_out_of_DMA_memory";
        break;
    case FK_no_event_available:
        return "FK_no_event_available";
        break;
    case FK_internal_error:
        return "FK_internal_error";
        break;
    case FK_data_mismatch:
        return "FK_data_mismatch";
        break;
    case FK_overflow:
        return "FK_overflow";
        break;
    case FK_suspended:
        return "FK_suspended";
        break;
    case FK_size_mismatch:
        return "FK_size_mismatch";
        break;
    case FK_error:
        return "FK_error";
        break;
    case FK_invalid_pattern:
        return "FK_invalid_pattern";
        break;
    case FK_invalid_parameter:
        return "FK_invalid_parameter";
        break;
    case FK_empty:
        return "FK_empty";
        break;
    default:
        return "Unknown error";
        break;
    }
    return "internal error in fedkit_get_error_string, you're in trouble...";
}

/**
 * @function fedkit_suspend suspends operations on a fedkit receiver
 * @param receiver pointer to the receiver structure
 */
void fedkit_suspend (struct fedkit_receiver * receiver)
{
    receiver->suspended = 1;
}

/**
 * @function fedkit_resume 
 * @param receiver pointer to the receiver structure
 */
void fedkit_resume (struct fedkit_receiver * receiver)
{
    receiver->suspended =0;
}

/**
 * @function fedkit_susp_get_handle_number
 * @param receiver pointer to the receiver structure
 * @return number of handles or -1 if receiver is not suspended
 */
int fedkit_susp_get_handle_number (struct fedkit_receiver * receiver)
{
    struct _fedkit_data_block *db = receiver->data_blocks_in_board;
    int count = 0;
    if (!receiver->suspended) return -1;
    while (db != NULL) {
        count ++;
        db=db->next;
    }
    return count;
}

/**
 * @function fedkit_susp_get_handle
 * @param receiver pointer to the receiver structure
 * @param index index of the handle to retrieve
 * @return the handle pointer provided with the clock when registered (possibly NULL) or NULL if the 
 * receiver is not suspended or if index is out of range
 */
void * fedkit_susp_get_handle (struct fedkit_receiver * receiver, int index)
{
    struct _fedkit_data_block *db = receiver->data_blocks_in_board;
    if (!receiver->suspended) return NULL;
    if (index < 0) return NULL;
    while (db != NULL && index-- > 0) {
        db=db->next;
    }
    if (db == NULL) return NULL;
    return db->user_handle;
}

/**
 * @function fedkit_autosend_generated (xxx word_count, uint16_t seed, xxx event_trigger_number);
 * @return status information
 * @param rec pointer to the receiver structure (returned by fedkit_open)
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @see fedkit_sender_open
 */
int fedkit_autosend_generated (struct fedkit_receiver * rec, uint16_t word_count, uint16_t seed, uint32_t event_trigger_number)
{
    int ret = FK_OK;
    // printf ("TOTO");
    if (rec->suspended) {
        edprintf ("Trying to autosend on a suspended receiver. Failed\n");
        return FK_suspended;
    }
	
    if (rec->map2 == MAP_FAILED) {
        return FK_IOerror;
    }
    /* hardware supported overflow protection */
    if (rec->map[_FK_CSR_OFFSET] & _FK_CSR_AUTOSEND_FULL) {
        return FK_overflow;	
    }
	
    rec->map2[_FK_SENDER_GEVENT/4] = event_trigger_number;
    rec->map2[_FK_SENDER_GEVENT/4] = word_count;
    rec->map2[_FK_SENDER_GEVENT/4] = seed;
    return ret;
}

/**
 * @function fedkit_is_autosender
 * @return 0 if not autosender, non-0 is is autosender
 * @param receiver pointer to the receiver structure (returned by fedkit_open)
 * @see fedkit_open
 */
int fedkit_is_autosender (struct fedkit_receiver * receiver)
{
    return (receiver->map2!=NULL) && (receiver->map2!=MAP_FAILED);
}

/**
 * @function fedkit_get_FPGA_version
 * @param receiver pointer to the receiver structure
 * @return version of the receiver's FPGA
 */
uint32_t fedkit_get_FPGA_version (struct fedkit_receiver * receiver)
{
    if (receiver != NULL) {
        return receiver->FPGA_version;
    } else {
        return 0xFFFFFFFF;
    }
}

/**
 *
 *
 *
 *
 */
void fedkitdump_start (struct fedkit_receiver * receiver)
{
 /* enable the link */
   _fedkit_enable_link (receiver);
    
    sleep(1);
     receiver->map[_FK_CSR_OFFSET/4] |= _FK_CSR_SOFT_RESET;
     receiver->map[_FK_CSR_OFFSET/4] |= _FK_CSR_SOFT_RESET;
}

/**
 * Put the receiver in link dump mode
 * @param receiver pointer to the receiver
 * @return FK_OK in case of success
 */
int fedkit_enable_link_dump (struct fedkit_receiver * receiver)
{
    uint32_t csr;
  
    if (receiver == NULL) {
        edprintf ("fedkit_enable_link_dump: Null receiver\n");
        return FK_IOerror;
    }
    
      
    csr = receiver->map[_FK_CSR_OFFSET/4];
    if ((csr & _FK_CSR_ALL_LINKS_ENABLE) == _FK_CSR_ALL_LINKS_ENABLE) {
        edprintf ("fedkit_enable_link_dump: both links can't be enabled\n");
        return FK_IOerror;
    }
    receiver->map[_FK_CSR_OFFSET/4] |= _FK_CSR_LINK_DEBUG;
    return FK_OK;
}

/**
 * Dump one link word plus status
 * @param receiver pointer to the receiver structure
 * @param lsw pointer where least significant 32 bits word goes
 * @param msw pointer where most significant 32 bits word goes
 * @param control is set to true iff this is a contorl word
 * @return FK_OK in case of success, FK_empty if nothing to read, other incase of error
 */ 
int fedkit_link_dump (struct fedkit_receiver * receiver, uint32_t *lsw, uint32_t *msw, int * control)
{
    /** marked this variable as unused because we use it to
        force a read from a fedkit hardware register but
        we also have -Werror and (implicitly) -Wunused-but-set-parameter

        See also http://gcc.gnu.org/onlinedocs/gcc/Variable-Attributes.html#Variable-Attributes 
    */        
    uint32_t dummy __attribute__ ((unused));

    if (receiver == NULL) {
        edprintf ("fedkit_link_dump: Null receiver\n");
        return FK_IOerror;
    }
    if (0 == (receiver->map[_FK_CSR_OFFSET/4] & _FK_CSR_LINK_DEBUG)){
        edprintf ("fedkit_link_dump: link dump mode not enabled\n");
        return FK_IOerror;
    }
    if (0 == (receiver->map[_FK_FIFOSTAT_OFFSET/4] & _FK_FIFOSTAT_PCI)) {
        return FK_empty;
    } else {
        *lsw = receiver->map[_FK_DUMPDATA0_OFFSET/4];
        *msw = receiver->map[_FK_DUMPDATA1_OFFSET/4];
        *control = receiver->map[_FK_DUMPCTRL0_OFFSET/4] & _FK_DUMPCTRL0_K;
        dummy = receiver->map[_FK_DUMPCTRL1_OFFSET/4];
    }
    return FK_OK;
}

/**
 * Restores normal operations after an link dump session
 * @param receiver pointer to the receiver structure.
 */
int fedkit_disable_link_dump (struct fedkit_receiver * receiver)
{
    if (receiver == NULL) {
        edprintf ("fedkit_disable_link_dump: Null receiver\n");
        return FK_IOerror;
    }
    receiver->map[_FK_CSR_OFFSET/4] &= ~_FK_CSR_LINK_DEBUG;
    return FK_OK;
}


/* ---------------------------------------------------------------------- */
#ifdef I2ODEBUG
/** this is called on library initialization, 
    see http://www.faqs.org/docs/Linux-HOWTO/Program-Library-HOWTO.html#INIT-AND-CLEANUP */
void __attribute__ ((constructor)) fedkit_init_loglevel(void)
{
  char *value = getenv("FEDKIT_DEBUGLEVEL");
  if (value == 0 || *value == '\0')
    {
      // not specified by the user
      fedkit_debuglevel = 0;
      return;
    }

  /* this ignores any parsing errors... */
  fedkit_debuglevel = (int) atol(value);
}

// void __attribute__ ((constructor)) fedkit_init_loglevel(void)
// {
//   printf("HERE\n");
// }

#endif
