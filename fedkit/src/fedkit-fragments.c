/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-fragments.c,v 1.2 2009/02/27 17:19:02 cano Exp $
*/
static char *rcsid_fragment = "@(#) $Id: fedkit-fragments.c,v 1.2 2009/02/27 17:19:02 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_fragment);
#endif
#include "fedkit.h"
#include "fedkit-private.h"

/**
 * @function fedkit_frag_size
 * @return the positive payload size of the framgment (in bytes). User headers not included. Or negative error code
 * @param receiver pointer to the fragment structure
 */
int fedkit_frag_size (struct fedkit_fragment *fragment)
{
	/*idprintf ("In fedkit_frag_size : sz=%d\n", fragment->wc);*/
    return fragment->wc/* - (fragment->block_number * fragment->header_size)*/;
}

/**
 * @function fedkit_frag_header_size
 * @return the positive user header size of the fragment (in bytes). Or negative error code
 * @param receiver pointer to the fragment structure
 */
int fedkit_frag_header_size (struct fedkit_fragment *fragment)
{
	return fragment->header_size;
}

/**
 * @function fedkit_frag_block_size
 * @return the positive block size (in bytes). Or negative error code.
 * @param receiver pointer to the fragment structure
 */
int fedkit_frag_block_size (struct fedkit_fragment *fragment)
{
	return fragment->block_size;
}

/**
 * @function fedkit_frag_block_number
 * @return the positive number of blocks in the fragment. Or negative error code.
 * @param receiver pointer to the fragment structure
 */
int fedkit_frag_block_number (struct fedkit_fragment *fragment)
{
	return fragment->block_number;
}

/**
 * @function fedkit_frag_block
 * @return a pointer to the block of numberindex  from a fragment. NULL if index off limit
 * @param receiver pointer to the fragment structure
 * @param index number of the block required
 * blocks are numbered starting from 0
 */
void * fedkit_frag_block (struct fedkit_fragment *fragment, int index)
{
	if (fragment == NULL) {
		return NULL;
	} else if (fragment->data_blocks == NULL) {
		eprintf ("Got NULL pointer as frag->block_number in fedkit_frag_block.\n");
		return NULL;
	} else if (index < 0 || index >= fragment->block_number) {
		return NULL;
	}
	return (void *) fragment->data_blocks[index]->user_address;
}

/**
 * @function _fedkit_frag_analyse proceed with the analysis of the fragment. 
 * This function just has to be called once per fragment (called automatically).
 * @param fragment pointer to the fragment structure
 */
void _fedkit_frag_analyse (struct fedkit_fragment * fragment)
{
    int total_size;
    int remaining_size;
    int data_block_size;
    int trailer_index;
    int trailer_block;
    int trailer_size = 0; /* Avoids justified warning, but that's OK (intricate conditions ) */
    int FED_count;
    int i;
	
    if (fragment == NULL || fragment->FED_number != FEDKIT_FRAGMENT_UNANALYSED) return;
    /* first we find out where the last trailer (containing the last word count) is, and follow 
       string back to the first FED */
    /* all sizes in 64 bits words, indexes in 64 bits words */
    if (((fragment->block_size - fragment->header_size) %8 != 0) ||
        fragment->block_size %8 != 0 ||
        fragment->header_size %8 != 0) {
        fragment->FED_number = -FK_size_not_aligned;
        return;
    }
    data_block_size = (fragment->block_size - fragment->header_size) / 8;
    remaining_size = total_size = fragment->wc / 8;
    FED_count = 0;
    while (remaining_size > 0) {
        trailer_index = (remaining_size - 1) % data_block_size + (fragment->header_size / 8);
        trailer_block = (remaining_size - 1) / data_block_size;
        if (trailer_block >= fragment->block_number) {
            fragment->FED_number = -FK_size_mismatch;
            return;
        }
        /* user addresses are uint32_t blocks, wc is located in Trailer[56..32] on intel, with uint32_t table and endianness, 
           it's on the last uint32_t (trailer_index*2+1) */
        trailer_size = fragment->data_blocks[trailer_block]->user_address[trailer_index*2+1] & 0xFFFFFF;
        if (trailer_size < 1) {
            fragment->FED_number = -FK_size_mismatch;
            return;  
        }
        remaining_size -= trailer_size;
        FED_count++;
    }
    if (remaining_size != 0) {
        iprintf ("In _fedkit_frag_analyse, got wrong remaining size. Remaining size = %d, FED_count = %d, last trailer_size = %d\n",
                 remaining_size, FED_count, trailer_size);
        fragment->FED_number = -FK_size_mismatch;
        return;
    }
    if (FED_count <=0 || FED_count > FEDKIT_FRAGMENT_MAX_FEDS) {
        fragment->FED_number = -FK_overflow;
    }
    /* now that we know how many FEDs we have, we can start to fill the structures*/
    remaining_size = total_size = fragment->wc / 8;
    for (i=FED_count-1; i>=0; i--) {
        trailer_index = (remaining_size - 1) % data_block_size + (fragment->header_size / 8);
        trailer_block = (remaining_size - 1) / data_block_size;
        trailer_size = fragment->data_blocks[trailer_block]->user_address[trailer_index*2+1] & 0xFFFFFF;
        fragment->FED_size[i] = trailer_size;
        remaining_size -= trailer_size;
        fragment->FED_offset[i] = remaining_size % data_block_size + (fragment->header_size / 8);
        fragment->FED_block[i] = remaining_size / data_block_size;
        fragment->FED_first_payload_block[i] = fragment->FED_block [i] + 
            (fragment->FED_offset[i] - (fragment->header_size /8) 
             + fragment->receiver->frag_header_size) 
            / data_block_size; 
        fragment->FED_last_payload_block[i] = fragment->FED_block [i] + 
            (fragment->FED_offset[i] - (fragment->header_size /8) 
             + fragment->FED_size[i] - fragment->receiver->frag_trailer_size - 1) 
            / data_block_size;
        fragment->FED_first_payload_offset[i] = 
            (fragment->FED_offset[i] - (fragment->header_size / 8) + 
             fragment->receiver->frag_header_size) % data_block_size + (fragment->header_size / 8);
    }
    if (remaining_size != 0) {
        fragment->FED_number = -FK_size_mismatch;
        return;
        iprintf ("In _fedkit_frag_analyse (2nd pass), got wrong remaining size. Remaining size = %d, FED_count = %d, last trailer_size = %d\n",
                 remaining_size, FED_count, trailer_size);
    }
    fragment->FED_number = FED_count;
    return;
}

/**
 * @function fedkit_frag_get_FED_number
 * @param frag pointer to the fragment structure
 * @return -Error in case of error or positive number of FEDs in fragment
 */
int fedkit_frag_get_FED_number (struct fedkit_fragment * fragment)
{
	if (fragment->FED_number == FEDKIT_FRAGMENT_UNANALYSED) {
		_fedkit_frag_analyse (fragment);
	}
	return (fragment->FED_number);
}

/**
 * @function fedkit_frag_get_FED_trailer
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return pointer to the word or NULL pointer in case of error
 */
void * fedkit_frag_get_FED_trailer (struct fedkit_fragment * fragment, int FED_index)
{
	int block, index, data_block_size;
	if (fragment == NULL) {	
		return NULL;
	}
	if (fragment->FED_number == FEDKIT_FRAGMENT_UNANALYSED) {
		_fedkit_frag_analyse (fragment);
	}
	if (fragment->FED_number < 0)
		return NULL;
	if (FED_index >= fragment->FED_number)
		return NULL;
	data_block_size = (fragment->block_size - fragment->header_size) / 8;
	block = fragment->FED_block[FED_index] + 
		(fragment->FED_offset[FED_index] - fragment->header_size + fragment->FED_size[FED_index]) / data_block_size;
	index = (fragment->FED_offset[FED_index] - (fragment->header_size / 8) + fragment->FED_size[FED_index] - 1) 
		% data_block_size + (fragment->header_size / 8);
	return (void *)  &(fragment->data_blocks[block]->user_address[2*index]);
}

/**
 * @function fedkit_frag_get_FED_word
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @param index index of the word
 * @return pointer to the word or NULL pointer in case of error
 */
void * fedkit_frag_get_FED_word (struct fedkit_fragment * fragment, int FED_index, int index)
{
	int block, offset, data_block_size;
	if (fragment->FED_number == FEDKIT_FRAGMENT_UNANALYSED) {
		_fedkit_frag_analyse (fragment);
	}
	if (fragment->FED_number < 0)
		return NULL;
	if (FED_index >= fragment->FED_number || FED_index < 0)
		return NULL;
	if (index >= fragment->FED_size[FED_index]) {
		return NULL;
	}
	data_block_size = (fragment->block_size - fragment->header_size) / 8;
	block = fragment->FED_block [FED_index] + 
		(fragment->FED_offset[FED_index] - (fragment->header_size /8) + index) 
		/ data_block_size;
	offset = (fragment->FED_offset[FED_index] - (fragment->header_size / 8) + index) 
		% data_block_size + (fragment->header_size / 8);
	return (void *)  &(fragment->data_blocks[block]->user_address[2*offset]);
}

/**
 *	@function fedkit_frag_get_FED_word_pci
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @param index index of the word
 * @return pci address of the word or NULL in case of error
 */
uint32_t fedkit_frag_get_FED_word_pci (struct fedkit_fragment * fragment, int FED_index, int index)
{
	int block, offset, data_block_size;
	if (fragment == NULL) {
		return -1;
	}
	if (fragment->FED_number == FEDKIT_FRAGMENT_UNANALYSED) {
		_fedkit_frag_analyse (fragment);
	}
	if (fragment->FED_number < 0)
		return 0;
	if (FED_index >= fragment->FED_number || FED_index < 0)
		return 0;
	if (index >= fragment->FED_size[FED_index]) {
		return 0;
	}
	data_block_size = (fragment->block_size - fragment->header_size) / 8;
	block = fragment->FED_block [FED_index] + 
		(fragment->FED_offset[FED_index] - (fragment->header_size /8) + index) 
		/ data_block_size;
	offset = (fragment->FED_offset[FED_index] - (fragment->header_size / 8) + index) 
		% data_block_size + (fragment->header_size / 8);

	// TODO: find arguments why this is correct
	{
	  size_t tmp = (size_t)&(fragment->data_blocks[block]->bus_address[2*offset]);
	  return (uint32_t) tmp;
	}
}

/**
 * @Function fedkit_get_FED_block_number 
 * @param fragment pointer to the fragment structure 
 * @param FED_index index of the FED 
 * @return the number of blocks on which this FED's PAYLOAD (not
 * including Slink headers) spans. Used in conjuction with
 * ...block_pointer and ...block_size, this functions allows the user
 * to handle easyly the payload of a fragment.
 * Returns -1 in case of error
 */
int fedkit_frag_get_FED_block_number (struct fedkit_fragment * fragment, int FED_index)
{
    if (fragment == NULL) return -1;
    if (fragment->FED_number == FEDKIT_FRAGMENT_UNANALYSED) _fedkit_frag_analyse (fragment);
    if (fragment->FED_number < 0) return -1;
    if (FED_index >= fragment->FED_number || FED_index < 0) return -1; 
    if (fragment->FED_size[FED_index] == fragment->receiver->frag_header_size + fragment->receiver->frag_trailer_size)
        return 0;
    return fragment->FED_last_payload_block[FED_index] - fragment->FED_first_payload_block[FED_index] + 1;
}

/**
 * @function fedkit_get_FED_block_number 
 * @param fragment pointer to the fragment structure 
 * @param FED_index index of the FED 
 * @param index index of the block
 * @return a pointer to the beginning of the block on which this FED's
 * PAYLOAD (not including Slink headers) spans. Used in conjuction
 * with ...block_pointer and ...block_size, this functions allows the
 * user to handle easyly the payload of a fragment.  NULL in case of problem */
void * fedkit_frag_get_FED_block_pointer (struct fedkit_fragment * fragment, int FED_index, int index)
{
    void * ret = NULL;
    int offset, block;
    if (index >= 0 && index < fedkit_frag_get_FED_block_number (fragment, FED_index)) {
        /* on 1st block, offset might be different, after, it's fixed */
        offset = (index == 0)?
            fragment->FED_first_payload_offset[FED_index]
            :fragment->header_size/8;
        block = fragment->FED_first_payload_block[FED_index] + index;
        /* We got the block, we got the offset. Easy now */
        ret = (void *)  &(fragment->data_blocks[block]->user_address[2*offset]);
    } 
    return ret;
}


/**
 * @function fedkit_get_FED_block_number 
 * @param fragment pointer to the fragment structure 
 * @param FED_index index of the FED 
 * @param index index of the block
 * @return the word count (64bits words) of the block on which this
 * FED's PAYLOAD (not including Slink headers) spans. Used in
 * conjuction with ...block_pointer and ...block_size, this functions
 * allows the user to handle easyly the payload of a fragment.  returns -1 on error*/
int fedkit_frag_get_FED_block_size (struct fedkit_fragment * fragment, int FED_index, int index)
{
    int ret = -1;
    int remaining_size;
    int data_block_size;
    int block_number = fedkit_frag_get_FED_block_number (fragment, FED_index);
    if (index >= 0 && index < block_number) {
        /* Now we can expect to be safe with this block (non-null, etc) */
         data_block_size = (fragment->block_size - fragment->header_size) / 8;
        /* Full size of payload */
        remaining_size = fragment->FED_size[FED_index] - fragment->receiver->frag_header_size 
            - fragment->receiver->frag_trailer_size;
        if (block_number == 1) { /* We only deal with a single block */
            ret = remaining_size;
        } else {
            /* second term in parenthesis is slink header size */
            int first_payload = data_block_size - (fragment->FED_first_payload_offset[FED_index] - fragment->header_size / 8);
            if (index == 0) { /* we deal with the first block */
                ret = first_payload;
            } else if (index < block_number - 1){ /* we deal with a middle block (not 1st, not last) */
                ret = data_block_size;
            } else { /* We deal with the last block = everything - first block + middle blocks */
                remaining_size -= first_payload + (block_number - 2) * data_block_size;
                ret = remaining_size;
            }
        }
        
    }
    return ret;
}

/**
 * @function fedkit_frag_get_FED_trigger
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return trigger number for FED or neagtive error number
 */
int fedkit_frag_get_FED_trigger (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_word (fragment, FED_index, 0); 
	if (words == NULL) return -FK_error;
	return (words[_FKH_WORD_LV1_id] >> _FKH_SHIFT_LV1_id) & _FKH_MASK_LV1_id;
}

/**
 * @function fedkit_frag_get_FED_wc
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return word count for FED, header and trailer included or neagtive error number
 */
int fedkit_frag_get_FED_wc (struct fedkit_fragment * fragment, int FED_index)
{
	if (fragment->FED_number == FEDKIT_FRAGMENT_UNANALYSED) {
		_fedkit_frag_analyse (fragment);
	}
	if (fragment->FED_number < 0)
		return fragment->FED_number;
	if (FED_index >= fragment->FED_number || FED_index < 0)
		return -FK_error;
	return fragment->FED_size[FED_index];
}

/**
 * @function fedkit_frag_get_FED_eventID
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return event ID for FED or neagtive error number
 */
int fedkit_frag_get_FED_eventID (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	return -1;
	
	words = (uint32_t*)fedkit_frag_get_FED_word (fragment, FED_index, 0);
	if (words == NULL) return -FK_error;
	return (words[_FKH_WORD_LV1_id] >> _FKH_SHIFT_LV1_id) & _FKH_MASK_LV1_id;
}

/**
 * @function fedkit_set_header_size sets the size of the header (in 64 bits words) in order
 * to be used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @param header_size header size in 64 bits words
 * @return FK_OK in case of success, FK_error otherwise. (usually if receiver was NULL)
 * this function does not do any checks (besides NULL pointer or out of range) as it's quite harmless.
 */
int fedkit_set_FED_header_size (struct fedkit_receiver * receiver, int header_size)
{
	if (receiver == NULL) {
		return FK_error;
	} else if ((header_size < 0) || (header_size >= receiver->block_size - receiver->frag_trailer_size)) {
		return FK_size_too_big;
	} else {
		receiver->frag_header_size = header_size;
	}
	return FK_OK;
}

/**
 * @function fedkit_get_header_size gets the size of the header (in 64 bits words)
 * used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @return header size in case of success, -FK_error otherwise. (usually if receiver was NULL)
 */
int fedkit_get_FED_header_size (struct fedkit_receiver * receiver)
{
	if (receiver == NULL) {
		return -FK_error;
	}
	return receiver->frag_header_size;
}
 
/**
 * @function fedkit_set_header_size sets the size of the header (in 64 bits words) in order
 * to be used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @param trailer_size header size in 64 bits words
 * @return FK_OK in case of success, FK_error otherwise. (usually if receiver was NULL)
 * this function does not do any checks (besides NULL pointer or out of range) as it's quite harmless.
 */
int fedkit_set_FED_trailer_size (struct fedkit_receiver * receiver, int trailer_size)
{
	if (receiver == NULL) {
		return FK_error;
	} else if ((trailer_size < 0) || (trailer_size >= receiver->block_size - receiver->frag_header_size)) {
		return FK_size_too_big;
	} else {
		receiver->frag_trailer_size = trailer_size;
	}
	return FK_OK;
}

/**
 * @function fedkit_get_header_size gets the size of the header (in 64 bits words)
 * used by the analysis functions
 * @param receiver pointer to the receiver strucuture
 * @return header size in case of success, -FK_error otherwise. (usually if receiver was NULL)
 */
int fedkit_get_FED_trailer_size (struct fedkit_receiver * receiver)
{
	if (receiver == NULL) {
		return -FK_error;
	}
	return receiver->frag_trailer_size;
}

/**
 * @function fedkit_frag_get_FED_header 
 * @param fragment pointer to the fragment
 * @return -FK_error in case of NULL pointer, fedkit_get_FED_header_size (fragment->receiver)
 * otherwise.
 */
int  fedkit_frag_get_FED_header_size (struct fedkit_fragment * fragment)
{
	if (fragment == NULL) {
		return -FK_error;
	} else {
		return (fedkit_get_FED_header_size (fragment->receiver));
	}
}

/**
 * @function fedkit_frag_get_FED_trailer 
 * @param fragment pointer to the fragment
 * @return -FK_error in case of NULL pointer, fedkit_get_FED_trailer_size (fragment->receiver)
 * otherwise.
 */
int  fedkit_frag_get_FED_trailer_size (struct fedkit_fragment * fragment)
{
	if (fragment == NULL) {
		return -FK_error;
	} else {
		return (fedkit_get_FED_trailer_size (fragment->receiver));
	}
}

/**
 *	@function fedkit_frag_get_FED_BX_id
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return bunch crossin number for FED or neagtive error number
 */
int fedkit_frag_get_FED_BX_id (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_word (fragment, FED_index, 0); 
	if (words == NULL) return -FK_error;
	return (words[_FKH_WORD_BX_id] >> _FKH_SHIFT_BX_id) & _FKH_MASK_BX_id;
}

/**
 *	@function fedkit_frag_get_FED_Source_id
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return source id for FED or neagtive error number
 */
int fedkit_frag_get_FED_Source_id (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_word (fragment, FED_index, 0); 
	if (words == NULL) return -FK_error;
	return (words[_FKH_WORD_Source_id] >> _FKH_SHIFT_Source_id) & _FKH_MASK_Source_id;
}

/**
 *	@function fedkit_frag_get_FED_Evt_lgth
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_Evt_lgth (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_trailer (fragment, FED_index); 
	if (words == NULL) return -FK_error;
	return (words[_FKT_WORD_Evt_lgth] >> _FKT_SHIFT_Evt_lgth) & _FKT_MASK_Evt_lgth;
}

/**
 *	@function fedkit_frag_get_FED_Evt_stat
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_Evt_stat (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_trailer (fragment, FED_index); 
	if (words == NULL) return -FK_error;
	return (words[_FKT_WORD_Evt_stat] >> _FKT_SHIFT_Evt_stat) & _FKT_MASK_Evt_stat;
}

/**
 *	@function fedkit_frag_get_FED_CRC
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_CRC (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_trailer (fragment, FED_index); 
	if (words == NULL) return -FK_error;
	return (words[_FKT_WORD_CRC] >> _FKT_SHIFT_CRC) & _FKT_MASK_CRC;
}

/**
 *	@function fedkit_frag_get_FED_TTS
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_TTS (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_trailer (fragment, FED_index); 
	if (words == NULL) return -FK_error;
	return (words[_FKT_WORD_TTS] >> _FKT_SHIFT_TTS) & _FKT_MASK_TTS;
}

/**
 *	@function fedkit_frag_get_FED_CRC
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_LV1_id (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_word (fragment, FED_index, 0); 
	if (words == NULL) return -FK_error;
	return (words[_FKH_WORD_LV1_id] >> _FKH_SHIFT_LV1_id) & _FKH_MASK_LV1_id;
}


/**
 *	@function fedkit_frag_get_FED_FOV
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_FOV (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_word (fragment, FED_index, 0); 
	if (words == NULL) return -FK_error;
	return (words[_FKH_WORD_FOV] >> _FKH_SHIFT_FOV) & _FKH_MASK_FOV;
}

/**
 *	@function fedkit_frag_get_FED_Evt_type
 * @param fragment pointer to the fragment structure
 * @param FED_index index of the FED
 * @return evt_length field for FED or neagtive error number
 */
int fedkit_frag_get_FED_Evt_type (struct fedkit_fragment * fragment, int FED_index)
{
	uint32_t *words;
	
	words = (uint32_t*)fedkit_frag_get_FED_word (fragment, FED_index, 0); 
	if (words == NULL) return -FK_error;
	return (words[_FKH_WORD_Evt_type] >> _FKH_SHIFT_Evt_type) & _FKH_MASK_Evt_type;
}


/**
 * @function fedkit_frag_get_FED CRC_error
 * @param fragment pointer to the fragment structure
 * @return true in case of CRC error reported by hardware, false instead
 */
int fedkit_frag_get_FEDCRC_error (struct fedkit_fragment * fragment)
{
    return fragment->FEDCRC_error;
}

/**
 * @function fedkit_frag_get_CRC_error
 * @param fragment pointer to the fragment structure
 * @return true in case of CRC error reported by hardware, false instead
 */
int fedkit_frag_get_CRC_error (struct fedkit_fragment * fragment)
{
    return fragment->CRC_error;
}

/**
 * @function fedkit_frag_get_truncated
 * @param fragment pointer to the fragment structure
 * @return true in case of truncation reported by hardware, false instead
 */
int fedkit_frag_get_truncated (struct fedkit_fragment * fragment)
{
    return fragment->truncated;
}
