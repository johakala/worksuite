/**
 * @file fedkit-file.c (in driver section)
 * kernel side driver of the fedkit receiver driver 
 * the kernel side drivers has the following functions
 * at installation time, it has to detect all the receiver boards present
 *
 * first function is to reserve a board (identified by ID) for the calling 
 * file descriptor. Base address for the board is delivered in the reply
 *
 * second call is used to register the address of the data fifo addresses 
 * to the interrupt service routine
 *
 * final one is to release the adapter and to free up everything
 * 
 */ 
/* fedkit documentation is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-file.c,v 1.3 2007/10/24 17:36:42 cano Exp $
*/
static char *rcsid = "@(#) $Id: fedkit-file.c,v 1.3 2007/10/24 17:36:42 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid);
#endif

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/pci.h>
#include <linux/types.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <linux/delay.h>

#include "../include/fedkit-private.h"


/* ---------------------------------------------------------------------- */
/* module parameters */
/* ---------------------------------------------------------------------- */

#ifdef I2ODEBUG
unsigned kfedkit_debuglevel = 0;
module_param(kfedkit_debuglevel, uint, 
	     S_IRUGO | S_IWUSR  // allow this parameter to be changed by 
	                        // writing to /sys/module/fedkit/<param_name>
	     ); 
MODULE_PARM_DESC(kfedkit_debuglevel, "Enable debug messages (1) or not (0)");

/* ----- */

unsigned kfedkit_last_interrupt_timeout = 1000;

module_param(kfedkit_last_interrupt_timeout, uint, 
	     S_IRUGO | S_IWUSR  // allow this parameter to be changed by 
	                        // writing to /sys/module/fedkit/<param_name>
	     ); 

MODULE_PARM_DESC(kfedkit_last_interrupt_timeout, "Approximate maximum waiting time for last interrupt when closing a receiver device in milliseconds (0 for infinite)");

#endif
//----------------------------------------------------------------------
// PCI ids this driver can be used with 
//----------------------------------------------------------------------

// see e.g. http://www.linuxjournal.com/node/5604/print
static struct pci_device_id kfedkit_pci_table[] = {
  { _FK_VENDOR, _FK_DEVICE, PCI_ANY_ID, PCI_ANY_ID },
  {0}     // end of table
};

MODULE_DEVICE_TABLE(pci, kfedkit_pci_table);

/* ---------------------------------------------------------------------- */
/* Driver's local variables */
#ifdef UNLOCKED
struct mutex  kfedkit_prev_dbuff_sem; /* for use in fedkit-Dbuff.c */
#else
struct semaphore kfedkit_prev_dbuff_sem; /* for use in fedkit-Dbuff.c */
#endif

/* file operations for the fedkit device driver */

/* forward declarations */

int kfedkit_open (struct inode *, struct file *);
#ifdef UNLOCKED
long kfedkit_ioctl(struct file *file_p,unsigned int cmd, unsigned long arg);
#else
int kfedkit_ioctl (struct inode *, struct file *, unsigned int, unsigned long);
#endif
int kfedkit_mmap (struct file *, struct vm_area_struct *);
int kfedkit_release (struct inode *, struct file *);

// unsigned kfedkit_usage_count = 0;

extern void kfedkit_init_procfs(void);
extern void kfedkit_release_procfs(void);

/** list of operations provided by this driver */
struct file_operations kfedkit_fops = {
#ifdef UNLOCKED
	unlocked_ioctl: kfedkit_ioctl,
#else
        ioctl : kfedkit_ioctl,
#endif
        mmap : kfedkit_mmap,
        open : kfedkit_open,

	/** called on file descriptor close (not necessarily on 
	    every close though, e.g. if processes
	    have forked).

	    According to http://stackoverflow.com/questions/4438146/where-does-linux-kernel-do-process-and-tcp-connections-cleanup-after-process-dies
	    it seems that this is also called when e.g. a process
	    dies unexpectedly (because all files are released
	    by the kernel).
	*/
        release : kfedkit_release,
};

/** list of all fedkit receivers currently in use */
struct kfedkit_receiver_t  *kfedkit_receivers = NULL;

/** semaphore to get hold of when modifying the LIST
    of receivers */
#ifdef UNLOCKED
struct mutex kfedkit_receivers_lock;
extern struct mutex kfedkit_senders_lock;
#else
struct semaphore kfedkit_receivers_lock;
extern struct semaphore kfedkit_senders_lock;
#endif

/* ---------------------------------------------------------------------- */

/** called after loading the module */
int __init init_module (void)
{
    printk(KERN_INFO "Loading fedkit version $Id: fedkit-file.c,v 1.3 2007/10/24 17:36:42 cano Exp $\n");
    printk(KERN_INFO "Compiled "__DATE__ " " __TIME__ "\n" );
#ifdef UNLOCKED
    mutex_init (&kfedkit_receivers_lock)/* = MUTEX*/;
    mutex_init (&kfedkit_senders_lock)/* = MUTEX*/;
    mutex_init (&kfedkit_prev_dbuff_sem) /* = MUTEX*/;
#else
    init_MUTEX (&kfedkit_receivers_lock)/* = MUTEX*/;
    init_MUTEX (&kfedkit_senders_lock)/* = MUTEX*/;
    init_MUTEX (&kfedkit_prev_dbuff_sem) /* = MUTEX*/;
#endif

    /* register a character device */
    if (register_chrdev (FEDKIT_MAJOR, "fedkit", &kfedkit_fops)) {
        printk (KERN_ERR "Failed to register fedkit in init_module\n");
        return -EIO;
    }

    kfedkit_init_procfs();

    return 0;
}

/* ---------------------------------------------------------------------- */

void kfedkit_unregister_interrupt (struct kfedkit_receiver_t *receiver);
void kfedkit_receiver_free_dma_memory(struct kfedkit_receiver_t * receiver);

/** called before unloading the module */
void __exit cleanup_module (void)
{
  struct kfedkit_receiver_t *receiver, *next_receiver;
  // clean up stuff left here before
#ifdef UNLOCKED
  mutex_lock (&kfedkit_receivers_lock);
#else
  down (&kfedkit_receivers_lock);
#endif

  /* find the receiver struct */
  for (receiver = kfedkit_receivers; receiver != NULL; receiver = next_receiver) 
    {
      if (receiver->inUse)
	printk(KERN_ERR "kfedkit: warning: receiver %d is still in use on module unload\n", receiver->index);
      
#ifdef UNLOCKED
	mutex_lock (&receiver->lock); 
#else
	down (&receiver->lock); 
#endif
        /* did we get the parameters yet ? */

      if (receiver->map0 != NULL)
	// disable interrupts
        receiver->map0[_FK_CSR_OFFSET / 4] &= ~_FK_CSR_ALL_INTS_ENABLE;


      // must unregister the interrupt here the latest
      // (even if the fedkit itself should not generate
      // any interrupts any more there might be other
      // devices on the same IRQ and our routine is 
      // still registered with the kernel)
      //
      // if we don't do this, the kernel crashes with
      // 'Unable to handle kernel paging request at ...'
      kfedkit_unregister_interrupt (receiver);
      
      // free existing memory
      kfedkit_receiver_free_dma_memory(receiver);
      
      receiver->block_number = 0;
      receiver->block_size = 0;

      receiver->inUse = 0;
      
      // disable interrupts


      // unregister PCI device
      pci_disable_device(receiver->device);
#ifdef UNLOCKED
      mutex_unlock(&receiver->lock); 
#else
      up(&receiver->lock); 
#endif

      // prepare next iteration
      next_receiver = receiver->next;
      kfree(receiver);
    } // loop over receivers


    /* Extra safety : collect semaphores */
    // down (&kfedkit_receivers_lock); // already done above
#ifdef UNLOCKED
    mutex_lock (&kfedkit_senders_lock);
#else
    down (&kfedkit_senders_lock);
#endif
    
    kfedkit_release_procfs();
    unregister_chrdev (FEDKIT_MAJOR, "fedkit");
}

/* ---------------------------------------------------------------------- */

/** kfedkit_nail_pages: according to the LDD book, PG_reserved
    will prevent the memory management system from working with
    such a page at all. */
void kfedkit_nail_pages (void *kernel_address, int size) 
{
    /* we will nail all the pages */
    struct page * page;
    for (page = virt_to_page(kernel_address); 
         page <= virt_to_page(kernel_address + size -1); 
         page++) 
        /*mem_map_reserve(page); */
        set_bit(PG_reserved, &((page)->flags));
}

/* ---------------------------------------------------------------------- */

/** kfedkit_unnail_pages. 
    @param size is in bytes ?
 */
void kfedkit_unnail_pages (void *kernel_address, int size) 
{
    /* we will nail all the pages */
    struct page * page;
    for (page = virt_to_page(kernel_address); 
         page <= virt_to_page(kernel_address + size -1); 
         page++) 
        /*mem_map_unreserve(page); */
        clear_bit(PG_reserved, &((page)->flags));
}

/* ---------------------------------------------------------------------- */

/* register receiver : check of the receiver is allocated, if not,
   look for device, allocate, and fill structures with relevant data */
int kfedkit_register_receiver(int index, struct inode *inode_p, struct file * file_p)
{
    struct kfedkit_receiver_t *receiver;
    struct kfedkit_board_t *board;
    int count;

    idprintf ("In kfedkit_register_receiver\n");

    /* critical section */
#ifdef UNLOCKED
  mutex_lock (&kfedkit_receivers_lock);
#else
  down (&kfedkit_receivers_lock);
#endif

    /* find the receiver in the list */
    idprintf ("In kfedkit_register_receiver\n");

    /* find the receiver struct */
    for (receiver = kfedkit_receivers; receiver != NULL; receiver=receiver->next) {
        
        if (receiver->index == index) break;
    }

    idprintf ("In kfedkit_register_receiver\n");

    if (receiver != NULL && receiver->inUse) {
        /* receiver was already allocated, sorry (EBUSY) */
#ifdef UNLOCKED
    mutex_unlock (&kfedkit_receivers_lock);
#else
    up (&kfedkit_receivers_lock);
#endif
        return -EBUSY;
    }
    
    idprintf ("In kfedkit_register_receiver\n");
    board = (struct kfedkit_board_t *) kmalloc (sizeof (struct kfedkit_board_t), GFP_KERNEL);
    if (board == NULL) {
        /* could not allocate memory */
#ifdef UNLOCKED
    mutex_unlock (&kfedkit_receivers_lock);
#else
    up (&kfedkit_receivers_lock);
#endif
        return -ENOMEM;
    }
    
    idprintf ("In kfedkit_register_receiver\n");
    board->type = receiver_board;
    board->dbuffs = NULL;

    if (receiver != NULL)
      {
	printk(KERN_INFO "fedkit: receiver already allocated\n");

	// for debugging only
	
	// FOR SECOND OPENING: GETTING AT LEAST UP TO HERE
	// return  -EBUSY;

	// receiver already allocated, clean up things
	// we left from previous receiver 
#ifdef UNLOCKED
	mutex_lock (&receiver->lock); 
#else
	down (&receiver->lock); 
#endif

	// new version: leave the interrupt handler
	// and decide there that we're not called
	// printk(KERN_INFO "unregistering interrupt\n");
	// kfedkit_unregister_interrupt (receiver);

	// free existing memory

	kfedkit_receiver_free_dma_memory(receiver);

	receiver->block_number = 0;
	receiver->block_size = 0;

	receiver->inUse = 1;
	board->receiver = receiver;

#ifdef UNLOCKED
	mutex_unlock(&receiver->lock); 
#else
	up(&receiver->lock); 
#endif
	
	// got at least until here
	// return  -EBUSY;
      }
    else
      {
	// printk(KERN_INFO "receiver not yet allocated\n");
	// allocate memory for the receiver structure
	receiver = (struct kfedkit_receiver_t *) kmalloc (sizeof (struct kfedkit_receiver_t), GFP_KERNEL);
	if (receiver == NULL)
	  {
	    /* could not allocate memory */
#ifdef UNLOCKED
    mutex_unlock (&kfedkit_receivers_lock);
#else
    up (&kfedkit_receivers_lock);
#endif
	    kfree(board);
	    return -ENOMEM;
	  }

	board->receiver = receiver;

	// initialize the receiver structure
	receiver->num_interrupts = 0;
	receiver->num_interrupts_with_receiver_inactive = 0;
	receiver->num_free_block_fifo_entries_written = 0;

	/* The device is not allocated. Let's find it (i.e. the PCI device) */
	receiver->device = NULL;
	idprintf ("In kfedkit_register_receiver\n");
	count = 0;
	idprintf ("In kfedkit_register_receiver\n");

	/* get a handle to the physical device (specified by 'index') by searching for devices
	   by vendor and device id. */
#ifdef UNLOCKED
	while (NULL != (receiver->device = pci_get_device(_FK_VENDOR, _FK_DEVICE, receiver->device)))
#else
	while (NULL != (receiver->device = pci_find_device(_FK_VENDOR, _FK_DEVICE, receiver->device)))
#endif
	  if (count++ == index)
            break;

	idprintf ("In kfedkit_register_receiver\n");
	if (receiver->device == NULL) 
	  {
	    /* the required device could not be found */
	    kfree (receiver); // TODO: this should not have been done before
	                      //       receiver was a pointer ?!
	    // TODO: shoudn't we also kfree board ?
#ifdef UNLOCKED
    mutex_unlock (&kfedkit_receivers_lock);
#else
    up (&kfedkit_receivers_lock);
#endif
	    return -ENODEV;
	  }

	/* ---------------------------------------- */
	/* 'enable' this device (by doing this,     */
	/* lspci will also print by which kernel    */
	/* module this is used and we should get    */
	/* the interrupts properly (and not from    */
	/* some other device sharing the same       */
	/* interrupt)                               */
	/* ---------------------------------------- */

	/* note that this is only called once per device
	   in the lifetime of the driver (see above, 
	   the allocated receiver struct is re-used) */
	if (pci_enable_device(receiver->device) != 0)
        {
	  // failed to enable the device, clean up and return
	  printk(KERN_INFO "call to pci_enable_device failed\n");

	  /* the required device could not be found */
	  kfree(receiver); // TODO: this should not have been done before
	                   //       receiver was a pointer ?!
	  // TODO: shoudn't we also kfree board ?
#ifdef UNLOCKED
    mutex_unlock (&kfedkit_receivers_lock);
#else
    up (&kfedkit_receivers_lock);
#endif
	  return -ENODEV;
        }

	/* ---------------------------------------- */
	/* set 32 bit mask for DMA */
	/* ---------------------------------------- */
	{
#ifdef UNLOCKED
	  int res = pci_set_dma_mask(receiver->device,DMA_BIT_MASK(32));
#else
	  int res = pci_set_dma_mask(receiver->device,DMA_32BIT_MASK);
#endif
	  if (res != 0)
	    {
	      printk(KERN_ERR "failed to set 32bit DMA mask for receiver\n");
	      // TODO: shouldn't we also kfree board ?

	      pci_disable_device(receiver->device);

	      kfree (receiver);
#ifdef UNLOCKED
    mutex_unlock (&kfedkit_receivers_lock);
#else
    up (&kfedkit_receivers_lock);
#endif
	      return -ENODEV;
	    }
	  //      else
	  //	printk(KERN_DEBUG "successfully set 32 bit DMA mask for receiver\n");
	}

	/* ---------------------------------------- */

	/* everything seems to be in order. Let's get out of the critical section */
	idprintf ("In kfedkit_register_receiver\n");
	receiver->index = index;
	receiver->next = kfedkit_receivers;
	kfedkit_receivers = receiver;
	/*up (&kfedkit_receivers_lock);*/

	/* now we can fill the blanks in the strucuture, and return a success */
	idprintf ("In kfedkit_register_receiver\n");
	receiver->block_fifo = NULL;
	receiver->block_fifo_dma_addr = 0;
	receiver->data_fifo = NULL;
	receiver->blocks = NULL;
	receiver->blocks_dma_addr = 0;
	receiver->wc_fifo = NULL;
	receiver->wc_fifo_dma_addr = 0;

	receiver->block_number = 0;
	receiver->block_size = 0;


	receiver->interrupt_registered = 0;

	idprintf ("In kfedkit_register_receiver\n");
	pci_read_config_dword (receiver->device, 0x10, &(receiver->base_address0));
	pci_read_config_dword (receiver->device, 0x14, &(receiver->base_address1));
	pci_read_config_dword (receiver->device, 0x18, &(receiver->base_address2));
	pci_read_config_dword (receiver->device, 0x48, &(receiver->FPGA_version));
	idprintf ("In kfedkit_register_receiver\n");

	/* only base address 0 needs to be mapped in the kernel */
	if (receiver->base_address0!= 0) 
	  receiver->map0 = ioremap_nocache (receiver->base_address0, _FK_BAR0_range);

	/*if (receiver->base_address1!= 0) 
	  receiver->map1 = ioremap_nocache (receiver->base_address1, _FK_BAR1_range);*/
	receiver->map1 = NULL;
	receiver->map2 = NULL;
	idprintf ("In kfedkit_register_receiver\n");
	if (receiver->map0 == NULL/* || receiver->map1 == NULL*/) {
	  /* we could not map in kernel one of the memory zones... Fishy */
	  idprintf ("In kfedkit_register_receiver : could not map in kernel one of the base addresses\n");
	  idprintf ("BARS : 0x%08x/0x%08x maps : %p/%p\n", receiver->base_address0, receiver->base_address1,
		    receiver->map0, receiver->map1
		    );
	  if (receiver->map0 != NULL)
            iounmap (receiver->map0);
	  if (receiver->map1 != NULL)
            iounmap (receiver->map1);
	  /* unplug the board from the general list (we still hold the lock) and free structure */
	  // TODO: not necessary any more ?
	  kfedkit_receivers = receiver->next;        

	  pci_disable_device(receiver->device);
	  
	  kfree (board);
#ifdef UNLOCKED
    mutex_unlock (&kfedkit_receivers_lock);
#else
    up (&kfedkit_receivers_lock);
#endif
	  return -EIO;
	}

	receiver->inUse = 1;
#ifdef UNLOCKED
	mutex_init (&(receiver->lock))/* = MUTEX*/;
#else
	init_MUTEX (&(receiver->lock))/* = MUTEX*/;
#endif

      }
    

    // receiver = &board->receiver;

    /* ---------------------------------------- */

    /* keep a handy pointer to the structure */
    file_p->private_data = board;
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
    MOD_INC_USE_COUNT;
#endif

#ifdef UNLOCKED
    mutex_unlock (&kfedkit_receivers_lock);
#else
    up (&kfedkit_receivers_lock);
#endif
    idprintf ("In kfedkit_register_receiver\n");
    return 0;
}

/* ---------------------------------------------------------------------- */

extern int kfedkit_register_sender(int index, struct inode *inode_p, struct file * file_p);


/* kfedkit_open. Just handles board reservation with kfedkit_close */ 
int kfedkit_open (struct inode *inode_p, struct file * file_p)
{
    int retval = -EFAULT;

    /* First let's find out what is required */
    if (MINOR (inode_p->i_rdev) < MAX_FEDKIT_SENDER_NUMBER) {
        /* We look for a sender */
        int sender_index = MINOR (inode_p->i_rdev) ;
        idprintf ("Will call kfedkit_register_sender for sender %d\n", sender_index);
        retval = kfedkit_register_sender (sender_index, inode_p, file_p);
    } else {
        /* We look for a receiver */
        int receiver_index = MINOR (inode_p->i_rdev) - MAX_FEDKIT_SENDER_NUMBER;
        idprintf ("Will call kfedkit_register_receiver for receiver %d\n", receiver_index);
        retval = kfedkit_register_receiver(receiver_index, inode_p, file_p);
    }

    if (retval == 0)
      // ignoring the return value of try_module_get(..) for the moment,
      // see http://kernel.org/pub/linux/utils/kernel/module-init-tools/FAQ
      // for reasons when this can fail
      try_module_get(THIS_MODULE);

    return retval;
}

/* ---------------------------------------------------------------------- */

/** 
    called by the receiver interrupt service routine (kfedkit_ISR). 

    @return an address to be written to the FRBKFIFO register, i.e.
            the next block which is available for writing by
            the fedkit card.

	    The return value is a PHYSICAL/BUS address.
*/
void * kfedkit_pop_data (struct kfedkit_receiver_t *receiver)
{
	void *ret;
	int next_read;
	
	idprintf ("In fedkit_pop_data : read = %d, write=%d, ,size=%d, tab=%p\n",
		  receiver->data_fifo->read, 
		  receiver->data_fifo->write, 
		  receiver->data_fifo->size,
		  receiver->data_fifo_tab);

	if (receiver->data_fifo->read == receiver->data_fifo->write) {

                /* no cleared buffers available to be given to
		   the card.
		*/
		return NULL;
	}
	ret = receiver->data_fifo_tab[receiver->data_fifo->read];

	/* increment the counter (signal to the PCI card that we have
           processed this ?) */
	next_read = (receiver->data_fifo->read + 1) % receiver->data_fifo->size;

	receiver->data_fifo->read = next_read;
	receiver->data_fifo->done_read++;
	return ret;
}

/* ---------------------------------------------------------------------- */

/** interrupt service routine for a fedkit receiver card 

   @param irq is the interrupt number
   @param receiver_p is user-specific data (passed to the kernel
    when registering this function), we use a pointer to a kfedkit_receiver_t
    struct.
*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
void kfedkit_ISR (int irq, void *receiver_p, struct pt_regs* regs)
#else
#ifdef UNLOCKED
irqreturn_t kfedkit_ISR(int irq, void *receiver_p)
#else
irqreturn_t kfedkit_ISR (int irq, void *receiver_p, struct pt_regs* regs)
#endif
#endif
{
    int target;
    uint32_t csr;
    struct kfedkit_receiver_t *receiver = (struct kfedkit_receiver_t *) receiver_p;

    /* find out which interrupt was triggered (half empty or empty ?) */
    csr = receiver->map0[_FK_CSR_OFFSET / 4];
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
    if (!(csr & (_FK_CSR_EMPTY_INT_ENABLE | _FK_CSR_HALF_EMPTY_INT_ENABLE))) {

        // TODO: should we also check the actual interrupt status,
        //       not only whether it's enabled or not ?!
  
        /* it's not the card handled by this driver which generated an IRQ 
           (note that we might share the interrupt with other devices) */
        return IRQ_NONE;
    }
#endif

    ++(receiver->num_interrupts);


    if (! receiver->inUse)
      {
	++(receiver->num_interrupts_with_receiver_inactive);

	// disable the interrupts
	if (receiver->map0 != NULL)
	  {
	    printk (KERN_CRIT "fedkit: got interrupt while not in use, disabling interrupts\n");
	    receiver->map0[_FK_CSR_OFFSET / 4] &= ~_FK_CSR_ALL_INTS_ENABLE;
	  }
	if (receiver->num_interrupts_with_receiver_inactive % 10000 == 0)
	  printk(KERN_CRIT "kfedkit: got %d interrupts while receiver is not open\n", receiver->num_interrupts_with_receiver_inactive);
	// causes system to hang if too many of these messages are printed
	//printk(KERN_CRIT "kfedkit: got interrupt while receiver is not open\n");
	// SHOULD NOT HAPPEN !!
	return IRQ_HANDLED;      
      }

    // protection
    if (receiver->data_fifo == NULL)
      {
	printk(KERN_INFO "fedkit: receiver->data_fifo is NULL in ISR\n");
	return IRQ_HANDLED;
      }

    if ((csr & _FK_CSR_EMPTY_INT_STATUS) && (csr & _FK_CSR_EMPTY_INT_ENABLE)) {
        idprintf ("In fedkit interrupt : empty\n");
        target = _FK_free_data_fifo_depth;
    } else if ((csr & _FK_CSR_HALF_EMPTY_INT_STATUS) && (csr & _FK_CSR_HALF_EMPTY_INT_ENABLE)) {
        idprintf ("In fedkit interrupt : half empty\n");
        target = _FK_free_data_fifo_depth / 2;
    } else {
        target = 0;
    }
    idprintf ("In fedkit interrupt, target = %d\n", target);

    for ( ; target > 0; target--) 
    {
      // get the PHYSICAL/BUS address of the block which should
      // be filled next
      void * data_block = kfedkit_pop_data (receiver);
      if (data_block != NULL) 
	{
	  // send the address of another free data block to
	  // the card's 'FRee BlocK fifo' (which looks to
	  // us like a single register but is a FIFO in
	  // the firmware)
	  //
	  // data_block originates from a call to pci_alloc_consistent(..)
	  // which is aware of the fact that the fedkit card is 
	  // 32 bit only (i.e. can only do DMA in the first 4 GByte of
	  // physical RAM) so we never shold get any of the bits
	  // 32..63 nonzero here
#pragma GCC diagnostic ignored "-Wpointer-to-int-cast"
	  receiver->map0[_FK_FRBKFIFO_OFFSET/4] = (uint32_t)data_block;
#pragma GCC diagnostic pop
	  
	  // TODO: should't we obtain the receiver's sempaphore here ?
	  ++receiver->num_free_block_fifo_entries_written;

        } 
      else
        {
	  // no free buffers available to be given to the fedkit card
	  target = -1;
	  break;
        }
    }
    idprintf ("In fedkit interrupt, target = %d\n", target);
    if (target <= -1) 
    { 
      /* we are unable to feed the FIFO, i.e. we don't have a free
	 buffer to give to the card for writing the next fragment.

	 Disable interrupts (because otherwise we'll just get
         the next interrupt after we leave this routine) 

	 TODO: could we actually increase an 'buffer underrun'
	       counter here ?! Or should we do this better
               in the calling function ?
      */
      receiver->map0[_FK_CSR_OFFSET / 4] &= ~_FK_CSR_ALL_INTS_ENABLE;
      receiver->data_fifo->int_disabled = 1;
    }
    #if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
    return IRQ_HANDLED;
    #endif
    
}

/* ---------------------------------------------------------------------- */

/** @function kfedkit_register_interrupt
    @param receiver pointer to the receiver
    registers interrupt for this receiver */
int kfedkit_register_interrupt(struct kfedkit_receiver_t *receiver)
{
  if (receiver->interrupt_registered == -1)
    // already registered, don't do anything 
    return 0;

    snprintf (receiver->device_name, FEDKIT_DEVNAME_SIZE, "fedkit_%d", receiver->index);
    receiver->device_name [FEDKIT_DEVNAME_SIZE - 1] = '\0';


    if (request_irq (receiver->device->irq, 
		     kfedkit_ISR,  /* the interrupt service routine */
		     _FK_IRQ_SHARED_FLAG,  /* capable of sharing IRQs with other devices */ 
		     "fedkit_receiver" /* receiver->device_name*/, 
		     receiver /* IRQ service routine user data */
		     )) {
        /* irq registering failed */
        return -1;
    } else {
        receiver->interrupt_registered = -1;
        return 0;
    }
}

/* ---------------------------------------------------------------------- */

/** don't forget to disable the interrupts on the fedkit card
    BEFORE calling this method */
void kfedkit_unregister_interrupt (struct kfedkit_receiver_t *receiver)
{
    if (receiver->interrupt_registered)
      {
        free_irq (receiver->device->irq, receiver);
	receiver->interrupt_registered = 0;
      }
}

/* ---------------------------------------------------------------------- */

void kfedkit_receiver_free_block_fifo(struct kfedkit_receiver_t *receiver, int unnail)

{
  // make sure the interrupt routine does not run between freeing the memory
  // and setting the pointer to data to NULL (if the interrupt happens
  // between the two, the ISR might give the card a pointer to already freed
  // memory)
  //
  // see also http://www.linuxjournal.com/article/5833
#ifdef UNLOCKED
  static DEFINE_SPINLOCK(lock);
#else
  spinlock_t lock = SPIN_LOCK_UNLOCKED;
#endif
  unsigned long flags;
  spin_lock_irqsave(&lock, flags);

  if (receiver->block_fifo != NULL) 
    { 
      unsigned block_fifo_size = (receiver->block_number + 1) * sizeof (void *) + sizeof (struct _FK_data_fifo);

      if (unnail)
	kfedkit_unnail_pages (receiver->block_fifo, block_fifo_size);

      edprintf ("About to free memory %p\n", receiver->block_fifo); 
      /* bigphysarea_free_pages (receiver->block_fifo); */
      // free_pages((unsigned long)receiver->block_fifo, get_order(block_fifo_size));
      pci_free_consistent(receiver->device, block_fifo_size, receiver->block_fifo, receiver->block_fifo_dma_addr);

      receiver->block_fifo = NULL; /* make sure we don't free more than once */
      receiver->block_fifo_dma_addr = 0;

      receiver->data_fifo = NULL;  /* this points into the block allocated for block_fifo,
				      so we should not access it any more either. */
    
  }

  spin_unlock_irqrestore(&lock, flags);

}

/* ---------------------------------------------------------------------- */

void kfedkit_receiver_free_dma_memory(struct kfedkit_receiver_t * receiver)
{
  // make sure the interrupt routine does not run between freeing the memory
  // and setting the pointer to data to NULL (if the interrupt happens
  // between the two, the ISR might give the card a pointer to already freed
  // memory)
  //
  // see also http://www.linuxjournal.com/article/5833
#ifdef UNLOCKED
  static DEFINE_SPINLOCK(lock);
#else
  spinlock_t lock = SPIN_LOCK_UNLOCKED;
#endif
  unsigned long flags;
  spin_lock_irqsave(&lock, flags);

  //--------------------

  /* deallocate stuff of the receiver */
  // int block_fifo_size = (receiver->block_number + 1) * sizeof (void *) + sizeof (struct _FK_data_fifo);
  edprintf ("C\n");
  
  kfedkit_receiver_free_block_fifo(receiver, 1);
  
  edprintf ("D\n");

  //----------------------------------------

  if (receiver->blocks != NULL) 
  {
    kfedkit_unnail_pages (receiver->blocks, receiver->block_size * receiver->block_number);
    edprintf ("About to free memory %p\n", receiver->blocks);
    /* bigphysarea_free_pages (receiver->blocks); */
    /* free_pages((unsigned long)receiver->blocks, get_order(receiver->block_size * receiver->block_number)); */
    pci_free_consistent(receiver->device, receiver->block_size * receiver->block_number, receiver->blocks,
			receiver->blocks_dma_addr);
    
    receiver->blocks = NULL; /* make sure we don't free more than once */
    receiver->blocks_dma_addr = 0;
  }


  //----------------------------------------
  edprintf ("E\n");

  if (receiver->wc_fifo != NULL) 
  {

    kfedkit_unnail_pages (receiver->wc_fifo, sizeof (struct _FK_wc_fifo));
    edprintf ("About to free memory %p\n", receiver->wc_fifo);
    /* bigphysarea_free_pages (receiver->wc_fifo); */
    /* free_pages((unsigned long)receiver->wc_fifo, get_order(sizeof (struct _FK_wc_fifo))); */
    pci_free_consistent(receiver->device, sizeof (struct _FK_wc_fifo), 
			receiver->wc_fifo, receiver->wc_fifo_dma_addr);
    
    receiver->wc_fifo = NULL; /* make sure we don't free more than once */
    receiver->wc_fifo_dma_addr = 0;
  }

  //----------------------------------------

  /* unmap the receiver's base addresses */
  edprintf ("F\n");
//   if (receiver->map0 != NULL) {
//     iounmap (receiver->map0);
//     receiver->map0 = NULL;
//   }

  //----------------------------------------
  spin_unlock_irqrestore(&lock, flags);
}


/* ---------------------------------------------------------------------- */

/** called on file descriptor close for a receiver board. */
void kfedkit_unregister_receiver(struct kfedkit_board_t *board)
{
  struct kfedkit_receiver_t *receiver;
  //  struct kfedkit_receiver_t **pp_receiver;

    edprintf ("A\n");
    if (board->type != receiver_board) 
        printk (KERN_ERR "Something wrong going on in kfedkit_unregister_receiver: not a receiver\n");
    // receiver = &board->receiver;
    receiver = board->receiver;

    /* -------------------- */

    /* lock the semaphore (note that this doesn't disable interrupts) */
#ifdef UNLOCKED
	mutex_lock (&receiver->lock); 
#else
	down (&receiver->lock); 
#endif
    edprintf ("B\n");

    /* first disable interrupts with the card (not sure what
       happens when an interrupt is pending while we unregsiter
       the interrupt ? -- it actually should not be pending
       but handled if it arrives here) */
    // receiver->map0[_FK_CSR_OFFSET/4] &= ~(_FK_CSR_HALF_EMPTY_INT_ENABLE | _FK_CSR_EMPTY_INT_ENABLE);

    // just to be sure: send a soft reset
    // receiver->map0[_FK_CSR_OFFSET/4] |= _FK_CSR_SOFT_RESET;
    // printk(KERN_INFO "fedkit: receiver CSR after reset: 0x%x\n",receiver->map0[_FK_CSR_OFFSET/4]);

    // wait for the last interrupt to happen: this indicates that the card
    // is done writing data (i.e. there are no pending DMA transfers)
    //
    // only then disable the interrupts

    // signal to the ISR that this receiver should be closed
    // (it will disable interrupts then)
    receiver->inUse = 0;
 
    printk(KERN_INFO "fedkit: waiting for ISR to get the last interrupt, enabled = %d\n", receiver->map0[_FK_CSR_OFFSET / 4] & _FK_CSR_ALL_INTS_ENABLE);

    {
      unsigned delay_counter = 0;
      unsigned timeout_reached = 0;
      while ((receiver->map0[_FK_CSR_OFFSET / 4] & _FK_CSR_ALL_INTS_ENABLE) != 0)
      {
	mdelay(1);
	++delay_counter;

	if (kfedkit_last_interrupt_timeout != 0 && 
	    delay_counter >= kfedkit_last_interrupt_timeout)
	  {
	    timeout_reached = 1;
	    break;
	  }

	if (delay_counter % 1000 == 0)
	  printk(KERN_INFO "fedkit: waited approx %u seconds\n", (delay_counter + 500) / 1000);
      }

      if (timeout_reached)
	printk(KERN_INFO "fedkit: timeout reached while waiting for last interrupt\n");
      else
	printk(KERN_INFO "fedkit: got last interrupt, interrutps are disabled now, waited approx. %u msec\n", delay_counter);
    }



    //--------------------
    // note that we do NOT unregister the PCI device here
    // because we leave the interrupt routine registered..

#ifdef UNLOCKED
    mutex_unlock(&receiver->lock);
#else
    up(&receiver->lock);
#endif

    // TODO: for debugging: do this only when we start a new receiver
    // kfedkit_unregister_interrupt (receiver);
    // kfedkit_receiver_free_dma_memory(receiver);

    /* remove the receiver from the receiver list */
    //    down (&kfedkit_receivers_lock);

    // TODO: for debugging: leave this in for the moment
//    edprintf ("G\n");
//    for (pp_receiver = &kfedkit_receivers;*pp_receiver != NULL;) {
//        if (*pp_receiver == receiver) 
//	  // TODO: we should also free the memory of receiver somewhere ?!
//	  *pp_receiver = (*pp_receiver)->next;
//        else 
//	  pp_receiver = &(*pp_receiver)->next;
//    }
//    edprintf ("H\n");
//    up (&kfedkit_receivers_lock);
    /* free the board structure */

    edprintf ("I\n");
    board->receiver = NULL; // just to be sure...
    kfree (board);
    edprintf ("J\n");
}

/* ---------------------------------------------------------------------- */

/** fedkit_ioctl_receiver : various control specific to the receiver
        board(mainly allcate/delloc bigphys memory */
#ifdef UNLOCKED
long kfedkit_ioctl_receiver(struct file *file_p,unsigned int cmd, unsigned long arg)
#else
int kfedkit_ioctl_receiver (struct inode * inode_p, struct file *file_p, 
                   unsigned int cmd, unsigned long arg)
#endif
{
    // spinlock_t lock = SPIN_LOCK_UNLOCKED;
    // unsigned long flags = 0;

    struct kfedkit_board_t *board = file_p->private_data;
    struct kfedkit_receiver_t *receiver= NULL;
    if (board->type != receiver_board) return -EFAULT;
    // receiver = &board->receiver;
    receiver = board->receiver;



    switch (cmd) {

    /* ---------------------------------------- */

    case FEDKIT_SETGET_RECEIVER_PARAMS:
    {
        struct _fedkit_receiver_parameters params;
        /* lock the file */
#ifdef UNLOCKED
	mutex_lock (&receiver->lock); 
#else
	down (&receiver->lock); 
#endif
        /* if the memories were already allocated, this change is not allowed anymore */
        if (receiver->block_fifo != NULL) {
#ifdef UNLOCKED
            mutex_unlock (&receiver->lock);
#else
            up (&receiver->lock);
#endif
            return -EPERM;
        }
        /* get the parameters */
        if (copy_from_user(&params, (void *)arg, sizeof (struct _fedkit_receiver_parameters))) {
#ifdef UNLOCKED
            mutex_unlock (&receiver->lock);
#else
            up (&receiver->lock);
#endif
            return -EFAULT;
        }

        /* Check the parameters and take note */
        if (params.block_number <=0 || params.block_size <= 0) {
#ifdef UNLOCKED
            mutex_unlock (&receiver->lock);
#else
            up (&receiver->lock);
#endif
            return -EINVAL;
        }
        receiver->block_number = params.block_number;
        receiver->block_size = params.block_size;
        receiver->do_allocate_blocks = params.do_allocate_blocks;
        /* no need to send back the parameters */
#ifdef UNLOCKED
            mutex_unlock (&receiver->lock);
#else
            up (&receiver->lock);
#endif
        return 0;
        break;
    }
 
    /* ---------------------------------------- */
    /* allocate memory */
    /* ---------------------------------------- */ 

    // TODO: review locking here (around memory allocation)
    case FEDKIT_GET_RECEIVER_MEMBLOCKS:
    {
        struct _fedkit_memory_blocks mem_blocks;
        int block_fifo_size;
        /* lock the file */
#ifdef UNLOCKED
	mutex_lock (&receiver->lock); 
#else
	down (&receiver->lock); 
#endif
        /* did we get the parameters yet ? */
        if (receiver->block_number == 0) {
#ifdef UNLOCKED
            mutex_unlock (&receiver->lock);
#else
            up (&receiver->lock);
#endif
            return -EPERM;
        }
        /* did we already allocate? */
        if (receiver->block_fifo != NULL) {
#ifdef UNLOCKED
            mutex_unlock (&receiver->lock);
#else
            up (&receiver->lock);
#endif
            return -EPERM;
        }
	/* -------------------- */
        /* allocate the memory buffers */
	/* -------------------- */
	// TODO: sizeof(void *) is different on 32 and 64 bit !
        block_fifo_size = (receiver->block_number + 1) * sizeof (void *) + sizeof (struct _FK_data_fifo);

        /* receiver->block_fifo = bigphysarea_alloc_pages ((block_fifo_size - 1) / PAGE_SIZE + 1, 0, GFP_KERNEL); */
	/* receiver->block_fifo = (void *) __get_free_pages(GFP_KERNEL, get_order(block_fifo_size)); */
	receiver->block_fifo = pci_alloc_consistent(receiver->device, block_fifo_size, &(receiver->block_fifo_dma_addr));

        /* receiver->wc_fifo = bigphysarea_alloc_pages ((sizeof (struct _FK_wc_fifo) - 1) / PAGE_SIZE + 1, 0, GFP_KERNEL); */
	// receiver->wc_fifo = (void *) __get_free_pages(GFP_KERNEL, get_order(sizeof (struct _FK_wc_fifo)));
	receiver->wc_fifo = pci_alloc_consistent(receiver->device, sizeof (struct _FK_wc_fifo), &(receiver->wc_fifo_dma_addr));

        if (receiver->do_allocate_blocks) {
            /* allocate memory to hold the requested number of buffers times the size per buffer */
            receiver->blocks = 
	      /* (void*) bigphysarea_alloc_pages ((receiver->block_size * receiver->block_number - 1) / PAGE_SIZE + 1, 0, GFP_KERNEL); */
	      /* (void *) __get_free_pages(GFP_KERNEL, get_order(receiver->block_size * receiver->block_number)); */
	      pci_alloc_consistent(receiver->device, receiver->block_size * receiver->block_number, &(receiver->blocks_dma_addr));

#ifdef FEDKIT_PAINT_BIGPHYS
            {
                int i;
                uint32_t * block = (uint32_t *)receiver->blocks;
                for (i = 0; i < (receiver->block_size * receiver->block_number) ; i++) {
                    /* convert from kernel logical address to bus address.
                       Use of virt_to_bus should be avoided according to the LDD book */
		    /* TODO: this is a place where consecutiveness of the allocated memory is assumed
                       
		       IS THIS USED FOR DEBUGGING / PUTTING PATTERNS IN MEMORY ?
                       NORMALLY NOT COMPILED -- FEDKIT_PAINT_BIGPHYS SEEMS NOT TO 
		       BE DEFINED BY DEFAULT
		    */
                    block[i] = virt_to_bus (&(block[i]));
                }
            }
#endif /* def FEDKIT_PAINT_BIGPHYS */
        } else {
	  // TODO: shouldn't we free these pointers if not NULL ?
            receiver->blocks = NULL;
	    receiver->blocks_dma_addr = 0;
        }
        
        /* check if anything went wrong */
        if (receiver->block_fifo == NULL || receiver->wc_fifo == NULL 
            || (receiver->do_allocate_blocks && receiver->blocks == NULL)) {

	        edprintf ("Could not allocate everything needed: block_fifo=%p (size=%d) wc_fifo=%p (size=%lu) do_allocate_blocks=%d blocks=%p (size=%d)\n",
			  receiver->block_fifo, block_fifo_size,
			  receiver->wc_fifo,    sizeof (struct _FK_wc_fifo),
			  receiver->do_allocate_blocks,
			  receiver->blocks,     receiver->block_size * receiver->block_number
			  ); 

		kfedkit_receiver_free_block_fifo(receiver, 0);

                if (receiver->blocks != NULL) { 

		    edprintf ("About to free memory %p\n", receiver->blocks); 
		    /* bigphysarea_free_pages (receiver->blocks); */
		    /* free_pages((unsigned long)receiver->blocks, get_order(receiver->block_size * receiver->block_number)); */
		    pci_free_consistent(receiver->device, receiver->block_size * receiver->block_number, 
					receiver->blocks, receiver->blocks_dma_addr);
		    receiver->blocks = NULL; /* make sure we don't free more than once */
		    receiver->blocks_dma_addr = 0;
		}
                if (receiver->wc_fifo != NULL) {

		    edprintf ("About to free memory %p\n", receiver->wc_fifo); 
		    /* bigphysarea_free_pages (receiver->wc_fifo); */
		    /* free_pages((unsigned long)receiver->wc_fifo,get_order(sizeof (struct _FK_wc_fifo))); */
		    pci_free_consistent(receiver->device, sizeof (struct _FK_wc_fifo),
					receiver->wc_fifo, receiver->wc_fifo_dma_addr);
		    receiver->wc_fifo = NULL; /* make sure we don't free more than once */
		    receiver->wc_fifo_dma_addr = 0;
		}
#ifdef UNLOCKED
    mutex_unlock(&receiver->lock);
#else
    up(&receiver->lock);
#endif
                return -ENOMEM;
        }
        /* do the pages nailing */
        kfedkit_nail_pages (receiver->block_fifo, block_fifo_size);
        kfedkit_nail_pages (receiver->wc_fifo, sizeof (struct _FK_wc_fifo));
        if (receiver->do_allocate_blocks) 
            kfedkit_nail_pages (receiver->blocks, receiver->block_size * receiver->block_number);
        /* prepare the data_fifo structure */
        receiver->data_fifo = (struct _FK_data_fifo *) ((void *)receiver->block_fifo + 
                                                        (receiver->block_number + 1) * sizeof (void *));
        receiver->data_fifo_tab = receiver->block_fifo;
        receiver->data_fifo->read = 0;
        receiver->data_fifo->write = 0;
        receiver->data_fifo->done_read = 0;
        receiver->data_fifo->done_write = 0;
        receiver->data_fifo->size = (receiver->block_number + 1);

        /* buildup reply */
        mem_blocks.free_blocks_fifo_kernel = receiver->block_fifo;

	// TODO: we should use receiver->block_fifo_dma_addr here
        mem_blocks.free_blocks_fifo_physical = (void *)virt_to_bus (receiver->block_fifo);
        mem_blocks.wc_fifo_kernel = receiver->wc_fifo;

	// TODO: we should use receiver->wc_fifo_dma_addr here
        mem_blocks.wc_fifo_physical = (void *)virt_to_bus (receiver->wc_fifo);
        if (receiver->do_allocate_blocks) {
            mem_blocks.data_blocks_kernel = receiver->blocks;

	    /* convert from kernel logical address to bus address (which can
               be given to the PCI card) */
	    // TODO: use blocks_dma_addr here instead
            mem_blocks.data_blocks_physical = (void *)virt_to_bus (receiver->blocks);

        } else {
            mem_blocks.data_blocks_kernel = NULL;
            mem_blocks.data_blocks_physical = NULL;
        }
        /* register the interrupt */
        if (kfedkit_register_interrupt(receiver)) {
            /* de allocate all the crap */
            kfedkit_receiver_free_block_fifo(receiver, 1);

            if (receiver->blocks != NULL) {

                kfedkit_unnail_pages (receiver->blocks, receiver->block_size * receiver->block_number);
                edprintf ("About to free memory %p\n", receiver->blocks);
		/* bigphysarea_free_pages (receiver->blocks); */
		/* free_pages((unsigned long)receiver->blocks, get_order(receiver->block_size * receiver->block_number)); */
		pci_free_consistent(receiver->device, receiver->block_size * receiver->block_number,
				    receiver->blocks, receiver->blocks_dma_addr);
		receiver->blocks = NULL; /* make sure we don't free more than once */
		receiver->blocks_dma_addr = 0;

            }
            if (receiver->wc_fifo != NULL) {

                kfedkit_unnail_pages (receiver->wc_fifo, sizeof (struct _FK_wc_fifo));
                edprintf ("About to free memory %p\n", receiver->wc_fifo);
		/* bigphysarea_free_pages (receiver->wc_fifo); */
		/* free_pages((unsigned long)receiver->wc_fifo, get_order(sizeof (struct _FK_wc_fifo))); */
		pci_free_consistent(receiver->device, sizeof (struct _FK_wc_fifo),
				    receiver->wc_fifo, receiver->wc_fifo_dma_addr);
		receiver->wc_fifo = NULL; /* make sure we don't free more than once */
		receiver->wc_fifo_dma_addr = 0;
            }
#ifdef UNLOCKED
            mutex_unlock (&receiver->lock);
#else
            up (&receiver->lock);
#endif
            return -EFAULT;
        }
        /* send reply */
        if (copy_to_user ((void *)arg, &mem_blocks, sizeof (struct _fedkit_memory_blocks))) {
            /* de allocate all the crap */
	    kfedkit_receiver_free_block_fifo(receiver, 1);

            if (receiver->blocks != NULL) {

                kfedkit_unnail_pages (receiver->blocks, receiver->block_size * receiver->block_number);
                /* bigphysarea_free_pages (receiver->blocks); */
		/* free_pages((unsigned long)receiver->blocks, get_order(receiver->block_size * receiver->block_number)); */
		pci_free_consistent(receiver->device, receiver->block_size * receiver->block_number,
				    receiver->blocks, receiver->blocks_dma_addr);
		receiver->blocks = NULL; /* make sure we don't free more than once */
		receiver->blocks_dma_addr = 0;
            }
            if (receiver->wc_fifo != NULL) {

                kfedkit_unnail_pages (receiver->wc_fifo, sizeof (struct _FK_wc_fifo));
                /* bigphysarea_free_pages (receiver->wc_fifo); */
		/* free_pages((unsigned long)receiver->wc_fifo, get_order(sizeof (struct _FK_wc_fifo))); */
		pci_free_consistent(receiver->device, sizeof (struct _FK_wc_fifo),
				    receiver->wc_fifo, receiver->wc_fifo_dma_addr);
		receiver->wc_fifo = NULL; /* make sure we don't free more than once */
		receiver->wc_fifo_dma_addr = 0;
            }
            kfedkit_unregister_interrupt (receiver);
#ifdef UNLOCKED
            mutex_unlock (&receiver->lock);
#else
            up (&receiver->lock);
#endif
            return -EFAULT;            
        }
        /* complete */
#ifdef UNLOCKED
            mutex_unlock (&receiver->lock);
#else
            up (&receiver->lock);
#endif
        return 0;
        break;
    }

    /* ---------------------------------------- */
    default:
        return -EINVAL;
    }
    return -EFAULT;
}

/* ---------------------------------------------------------------------- */
#ifdef UNLOCKED
extern long kfedkit_ioctl_sender(struct file *file_p,unsigned int cmd, unsigned long arg);
long kfedkit_ioctl(struct file *file_p,unsigned int cmd, unsigned long arg)
#else
extern int kfedkit_ioctl_sender (struct inode * inode_p, struct file *file_p, 
				 unsigned int cmd, unsigned long arg);

/** fedkit_ioctl : various controls for sender and reciever */
int kfedkit_ioctl (struct inode * inode_p, struct file *file_p, 
                   unsigned int cmd, unsigned long arg)
#endif
{
    struct kfedkit_board_t *board = file_p->private_data;
    switch (cmd) {
    /* ioctl functions common to receiver and sender */

    case FEDKIT_GET_BOARD_ADDRESSES:
    {
        struct _fedkit_board_addresses reply = {0, 0, 0, 0};
        switch (board->type) {
        case sender_board:
            reply.base_address_0 = board->sender.base_address0;
            reply.base_address_1 = board->sender.base_address1;
            reply.base_address_2 = board->sender.base_address2;
            reply.FPGA_version = board->sender.FPGA_version;
            break;
        case receiver_board:
            reply.base_address_0 = board->receiver->base_address0;
            reply.base_address_1 = board->receiver->base_address1;
            reply.base_address_2 = board->receiver->base_address2;
            reply.FPGA_version = board->receiver->FPGA_version;
            break;
        default:
            return -EFAULT;
        }
        /* copy the reply to user space */
        if (copy_to_user((void *)arg, &reply, sizeof (struct _fedkit_board_addresses)))
            return -EFAULT;
        return 0;
        break;
    }
    case FEDKIT_ALLOC_DBUFF:
    {
        /* arg is a pointer (in user space) to a _fedkit_dbuff structure.
	   cmd is ignored. */
#ifdef UNLOCKED
      	return kfedkit_ioctl_alloc_dbuff(file_p, cmd, arg);
#else
      	return kfedkit_ioctl_alloc_dbuff(inode_p, file_p, cmd, arg);
#endif
	break;
    }
    case FEDKIT_FREE_DBUFF:
    {
#ifdef UNLOCKED 
	return kfedkit_ioctl_free_dbuff(file_p, cmd, arg);
#else
	return kfedkit_ioctl_free_dbuff(inode_p, file_p, cmd, arg);
#endif
	break;
    }
	    
    default:
    {
        switch (board->type) {

        /* sender-specific functions */
        case sender_board:
#ifdef UNLOCKED
            return kfedkit_ioctl_sender (file_p, cmd, arg);
#else
            return kfedkit_ioctl_sender (inode_p, file_p, cmd, arg);
#endif

        /* receiver-specific functions */
        case receiver_board:
#ifdef UNLOCKED
            return kfedkit_ioctl_receiver (file_p, cmd, arg);
#else
            return kfedkit_ioctl_receiver (inode_p, file_p, cmd, arg);
#endif

        default:
            return -EFAULT;
        }
    }
    }
    return -EFAULT;
}

/* ---------------------------------------------------------------------- */

/** fedkit_mmap : has to handle both bigphys areas and DMA
        buffers. Should be fine with physical address.

    This method is called when the program in user space calls
    mmap(2).
*/
int kfedkit_mmap (struct file *file_p, struct vm_area_struct *vma)
{
    /* there could (should) be a different system call for mmaping the dbuffs, and the boards,
     * but the lack of checks make them identical */
    unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
#ifndef RED_HAT_LINUX_KERNEL
#define RED_HAT_LINUX_KERNEL 0
#endif

#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,10) 

    // should use io_remap_page_range instead according to LDD3, page 425
    // (but seems only to be different on SPARC)
    //
    // see also LDD3, page 426
    if (remap_pfn_range (vma,                         // vma 
			 vma->vm_start,                      // virt_addr: USER VIRTUAL ADDRESS 
			 vma->vm_pgoff,               // pfn (page frame number), PHYSICAL ADDRESS, see LDD3, page 425
                         vma->vm_end - vma->vm_start, // size
			 vma->vm_page_prot            // prot (protection ?)
			 ) ) {

#elif LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)

    // see LDD3, page 421 -- does this do the same as what we are trying to achieve
    // with kfedkit_nail_pages(..) ?
      // vma->vm_flags |= VM_RESERVED; // seems however not to cure the crash on SLC4...

    // looking at include/asm-i386/pgtable.h of the 2.6.9-89.0.20.EL.cern-smp-i686
    // kernel sources, one sees that io_remap_page_range(..) (described in LDD3) is #define'd to be
    // exactly the same as remap_page_range(..)
    if (remap_page_range (vma,                        // vma
			  vma->vm_start,              // from
			  offset,                     // to
                          vma->vm_end - vma->vm_start, // size
			  vma->vm_page_prot           // prot
			  )) {

#elif LINUX_VERSION_CODE > KERNEL_VERSION(2,4,18) && RED_HAT_LINUX_KERNEL

    if (remap_page_range (vma, vma->vm_start, offset, 
                          vma->vm_end - vma->vm_start, vma->vm_page_prot)) {

#else

    if (remap_page_range (vma->vm_start, offset, 
                              vma->vm_end - vma->vm_start, vma->vm_page_prot)) {

#endif
        /* a problem occurred */
        eprintf ("kfedkit_mmap: fail to remap start=0x%08lx, end=0x%08lx, ofset = 0x%08lx\n", 
                 vma->vm_start, vma->vm_end, offset);
        return -EAGAIN;
    }
    idprintf ("In kfedkir_mmap : mapped successfully vma with :\n");
    idprintf ("*start  = 0x%08x\n", (int)vma->vm_start);
    idprintf ("*offset = 0x%08x\n", (int)offset);
    idprintf ("*end =    0x%08x\n", (int)vma->vm_end);
    idprintf ("*(size =  0x%08x)\n",(int)(vma->vm_end-vma->vm_start));
    return (0);   /* happy */
}

/* ---------------------------------------------------------------------- */

extern void kfedkit_unregister_sender (struct kfedkit_board_t *board);

/** fedkit release : close the file and deallocate everything */
int kfedkit_release (struct inode *inode_p, struct file *file_p)
{
    struct kfedkit_board_t *board = file_p->private_data;
    edprintf ("kfedkit_release: board->type = %d\n", (int)board->type);
    /* Free all the dbuffs attached to the board. This is safe as the kernel only
     * calls the release function on a file desciptor when the last of open file descriptor
     * AND memory maps for it is closed. All the dbuffs are hence not mapped anymore */

    // TODO: DEBUG: commented out for debugging purposes
    //    kfedkit_dbuffs_free (board->dbuffs);
    switch (board->type) {
    case receiver_board:
    {
        edprintf ("kfedkit_release: receiver_board\n");
        kfedkit_unregister_receiver(board);
        break;
    }
    case sender_board:
    {
        edprintf ("kfedkit_release: sender_board\n");
        kfedkit_unregister_sender(board);
        break;
    }
    default:
        edprintf ("kfedkit_release: default\n");
        return -EFAULT;
    };
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
    MOD_DEC_USE_COUNT;
#endif
    module_put(THIS_MODULE);
    return 0;
}


MODULE_LICENSE("GPL");

/* ---------------------------------------------------------------------- */
