def addNode(parentNode, nodeName, attributes = dict()):
    doc = parentNode.ownerDocument

    # TODO: should add proper handling of namespace...
    el = doc.createElement(nodeName)

    for name, value in attributes.iteritems():
        el.setAttribute(name, str(value))

    parentNode.appendChild(el)

    return el

#----------------------------------------------------------------------

def addTextChild(parentNode, textContent):

    doc = parentNode.ownerDocument

    # TODO: should add proper handling of namespace...
    el = doc.createTextNode(textContent)

    parentNode.appendChild(el)

    return el

#----------------------------------------------------------------------

def toXML(doc):
    """ returns a pretty printed string based on the xml document"""
    import StringIO
    import xml.dom.ext

    # create a stringstream buffer
    buf = StringIO.StringIO()
    xml.dom.ext.PrettyPrint(doc,buf)

    return buf.getvalue()

#----------------------------------------------------------------------

def singleNode(ctxt, xpathExpr):
    """ insists that there is exactly a single node of this type """

    import xml.xpath
    nodes = xml.xpath.Evaluate(xpathExpr, ctxt.node, ctxt)

    if len(nodes) != 1:
        raise Exception("not exactly one (but %d) nodes found for xpath expression '%s'" % (len(nodes), xpathExpr))

    return nodes[0]

#----------------------------------------------------------------------

def singleChildNodeByName(parentNode, name):
    """ returns the direct child with the given name (or throws an
    exception if there is not exactly one such child """

    retval = [ node for node in parentNode.childNodes if node.nodeName == name ]

    if len(retval) != 1:
        raise Exception("not exactly one child (but %d) with name '%s' found" % (len(retval), name))

    return retval[0]

#----------------------------------------------------------------------

def textContent(parentNode):
    """ loops over all (direct) children and concatenates
    the text content of all text nodes """

    children = [ node for node in parentNode.childNodes if node.nodeType == node.TEXT_NODE ]

    return "".join(child.nodeValue for child in children)
