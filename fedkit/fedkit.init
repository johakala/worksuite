#!/bin/bash
#
# $Header: /afs/cern.ch/project/cvs/reps/tridas/TriDAS/daq/fedkit/fedkit.init,v 1.1 2007/10/26 16:25:46 cano Exp $
#
# init file for fedkit driver
# copy this file to /etc/init.d as fedkit
#
# fedkit        This shell script takes care of loading and unloading the fedkit driver
#		and to create the device files for dynamic major number
#		dynamically).
#
# chkconfig: 345 80 20
#
# description:  fedkit is the software for driveing the development 
#		board for FEDs
#

# Source function library.
. /etc/rc.d/init.d/functions

device="fedkit"


load_driver() {
  group="root"
  mode="777"
  2> /dev/null
  
  unload_driver
  /sbin/modprobe $device || return 1
  /sbin/lsmod | grep -q $device || return 1

  major=`cat /proc/devices | awk "\\$2==\"$device\" {print \\$1}"`
  
  rm -fr /dev/${device}*
  for i in `seq 0 7`; do 
      mknod /dev/fedkit_sender$i c $major $i
      mknod /dev/fedkit_receiver$i c $major $(( $i + 32 ))
  done
  chown root.$group   /dev/${device}*
  chmod $mode         /dev/${device}*
  ls /dev/${device}* 2> /dev/null  | grep -q $device || return 1

  return 0
}

checkDriver () {
  /sbin/lsmod | grep -q $device || return 1
  ls /dev/${device}* 2> /dev/null  | grep -q $device || return 1
  return 0
}


unload_driver() {
  /sbin/modprobe -r $device 2> /dev/null
  rm -f /dev/${device}*

  /sbin/lsmod | grep -q $device && return 1
  ls /dev/${device}* 2> /dev/null  | grep -q $device && return 1

  return 0
}

case "$1" in
    start)
	echo -n "Loading fedkit driver..."
	load_driver
	RETVAL=$?
	[ $RETVAL -eq 0 ] && echo_success
	[ $RETVAL -ne 0 ] && echo_failure

	echo
	;;
    stop)
	echo -n "Unloading fedkit driver..."
	unload_driver
	RETVAL=$?
	[ $RETVAL -eq 0 ] && echo_success
	[ $RETVAL -ne 0 ] && echo_failure
	echo
	;;
    restart)
	$0 stop
	$0 start
	;;
    status)
	echo -n "Check if fedkit driver is loaded: "
	checkDriver
	RETVAL=$?
	[ $RETVAL -eq 0 ] && echo_success
	[ $RETVAL -ne 0 ] && echo_failure
	echo
	;;
    *)
	echo "Usage: fedkit {start|stop|restart|status}"
	exit 1
esac

exit 0
