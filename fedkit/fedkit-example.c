/* fedkit-example.c */
/* very simple fedkit example. */
/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/* to compile the program, from directory itools, execute the following command : */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-example.c,v 1.1 2007/10/09 08:19:00 cano Exp $
*/
static char *rcsid_fedkit_example_c = "@(#) $Id: fedkit-example.c,v 1.1 2007/10/09 08:19:00 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ == 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_fedkit_example_c);
#endif

/* 
gcc packages/fedkit/fedkit-example.c -I packages/fedkit/include/ \
		-I packages/xdaq-shell/include -I packages/i2o/include \
		-D LITTLE_ENDIAN__ packages/fedkit/libfedkit.a \
		packages/xdaq-shell/libxdaq-shell.a 
*/


#include <fedkit/fedkit.h>
#include <fedkit/fedkit-sender.h>
#include <stdlib.h> /* for exit() */

struct fedkit_receiver * receiver = NULL;
struct fedkit_sender * sender = NULL;
struct fedkit_fragment * frag = NULL;
	

int main (void)
{
	/* fedkit sender card initialisation */
	sender = fedkit_sender_open (0, NULL);
	if (sender == NULL) {
		exit (1);
	}

	/* receiver card initialisation. This will
	   set some default values (which could
	   be modified before calling fedkit_start(..).

	   The default block size is FK_DEFAULT_BLOCK_SIZE, 
	   the default number blocks in the receiving
	   buffer is FK_DEFAULT_BLOCK_NUMBER and the
	   default header size is FK_DELAULT_HEADER_SIZE. */
	receiver = fedkit_open (0, NULL);
	if (receiver == NULL) {
		fedkit_sender_close (sender);
		exit (1);
	}

	/* start receiving fragments */
	fedkit_start (receiver);
	
	/* send event (fedkit sender board). Seems like
	   this function sets a flag on the PCI card
	   to make the firmware enerate a (random) fragment. */
	fedkit_send_generated (sender, 1000, 1, 1);
	
	/* receive the event */
	frag = fedkit_frag_get (receiver, NULL);
	
	/* if an event was received, recycle the blocks */
	if (frag != NULL) 
		fedkit_frag_release (frag);
		
	/* final cleanup */
	fedkit_close (receiver);
	fedkit_sender_close (sender);
        return 0;
}
