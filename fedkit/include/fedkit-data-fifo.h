/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-data-fifo.h,v 1.1 2007/10/09 08:19:01 cano Exp $
*/
#ifndef _FEDKIT_DATA_FIFO_H_
#define _FEDKIT_DATA_FIFO_H_

static char *rcsid_data_fifo = "@(#) $Id: fedkit-data-fifo.h,v 1.1 2007/10/09 08:19:01 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_data_fifo);
#endif
#include "fedkit.h"
#ifndef NO_FEDKIT_PTHREAD
#ifndef __KERNEL__
#include <pthread.h>
#endif /* ndef KERNEL */
#endif /* def FEDKIT_PTHREAD */
#include <time.h>

#ifdef DEBUG_DATA_POINTERS
inline void _fedkit_pointer_ubercheck (struct fedkit_receiver * rec, char * where)
{
	/* in this case, we are supposing that the events are only one
	block big (it's the case with the daq column test setup)
	first we look if the hardware FIFO is empty.
	if so, then we can assume that the all blocks are either in the wc FIFO or in the 
	FREE blocks FIFO.
	we should have the same count in the wc+fere blocks FIFO and in the internal FIFO.
	We can also cross check with the in and out counts from the in an out functions*/ 
	int count_1 = -1; /* count of the pending word counts + size of the data FIFO (which should 0) */
	int count_2 = -1; /* count of the blocks in the internal FIFO */
	int count_3 = -1; /* pointers in -out */
	U32 * first_block_in_board = NULL;
	U32 * last_block_in_board = NULL;
	U32 * last_block_in_FIFO = NULL;
	int previous_write_in_FIFO;
	int fifos_involved = 0;
	int wc_fifo_size;
	int data_fifo_size;
	struct timespec decamicrosecond = {0, 10000};
	
	struct _fedkit_data_block *db;
	
	if (rec == NULL) {
		iprintf ("In _fedkit_pointer_ubercheck : NULL receiver\n");
		return;
	} else if (!(rec->started)) {
		iprintf ("In _fedkit_pointer_ubercheck : receiver not started\n");
		return;
	}

	#ifndef NO_FEDKIT_PTHREAD
	pthread_mutex_lock (&(rec->global_mutex));
	#endif
	
	/* let's follow the chain of the blocks in board */
	count_2=0;
	db = rec->data_blocks_in_board;
	if (db != NULL) {
		first_block_in_board = db->bus_address;
	}
	while (db != NULL) {
		last_block_in_board = db->bus_address;
		db=db->next;
		count_2 ++;
	}
	
	if ((rec->map[_FK_CSR_OFFSET/4] & 0x1) && (rec->wc_fifo != NULL) && (rec->wc_fifo != MAP_FAILED)) {/* if data block FIFO empty in hardware */
		data_fifo_size = rec->data_fifo->write - rec->data_fifo->read % rec->data_fifo->size;
		if (data_fifo_size < 0) data_fifo_size += rec->data_fifo->size;
		count_1 = data_fifo_size;
		wc_fifo_size = rec->wc_fifo->write - rec->wc_fifo->read % (2 *_FK_wc_fifo_depth);
		if (wc_fifo_size < 0) wc_fifo_size += (2 *_FK_wc_fifo_depth);
		if (wc_fifo_size > _FK_wc_fifo_depth) {
			iprintf ("Error : wc_fifo_size= %d>_FK_wc_fifo_depth=%d\n in %s", 
				wc_fifo_size, _FK_wc_fifo_depth, where);
			goto _FKPU_failure;
		}
		count_1 += wc_fifo_size;
	}

	/* get last pointer in FIFO */
	previous_write_in_FIFO = rec->data_fifo->write - 1;
	if (previous_write_in_FIFO < 0) previous_write_in_FIFO += rec->data_fifo->size;
	last_block_in_FIFO = (U32*) rec->data_fifo_tab[previous_write_in_FIFO];
	
	/* the last pointer in the FIFO should also be the last pointer in */
	if (last_block_in_FIFO != (U32*)rec->last_pointer_in) {
		iprintf ("Error : last_block_in_FIFO= 0x%08x != rec->last_pointer_in= 0x%08x in %s\n", 
			(int)last_block_in_FIFO, rec->last_pointer_in, where);
		goto _FKPU_failure;
	}
	
	/* this count should match as well */
	count_3 = rec->pointer_in_count - rec->pointer_out_count;

	/* the two counts should be the same and match pointers in - pointers out */
	if (count_1 != -1 && count_2 != -1 && count_1 != count_2) {
		goto _FKPU_recount_1;
	}
	if (count_1 != -1 && count_3 != -1 && count_1 != count_3) {
		goto _FKPU_recount_1;
	}
	if (count_2 != -1 && count_3 != -1 && count_2 != count_3) {
		iprintf ("count_2 (blocks in board)=%d != count_3 (pointer in - out)=%d in %s\n", count_2, count_3, where);
		goto _FKPU_failure;
	}
_FKPU_recount_1:
	nanosleep(&decamicrosecond, NULL);
	if ((rec->map[_FK_CSR_OFFSET/4] & 0x1) && (rec->wc_fifo != NULL) && (rec->wc_fifo != MAP_FAILED)) {/* if data block FIFO empty in hardware */
		data_fifo_size = rec->data_fifo->write - rec->data_fifo->read % rec->data_fifo->size;
		if (data_fifo_size < 0) data_fifo_size += rec->data_fifo->size;
		count_1 = data_fifo_size;
		wc_fifo_size = rec->wc_fifo->write - rec->wc_fifo->read % (2 *_FK_wc_fifo_depth);
		if (wc_fifo_size < 0) wc_fifo_size += (2 *_FK_wc_fifo_depth);
		if (wc_fifo_size > _FK_wc_fifo_depth) {
			iprintf ("Error : wc_fifo_size= %d>_FK_wc_fifo_depth=%d\n in %s", 
				wc_fifo_size, _FK_wc_fifo_depth, where);
			goto _FKPU_failure;
		}
		count_1 += wc_fifo_size;
	}
	if (count_1 != -1 && count_2 != -1 && count_1 != count_2) {
		iprintf ("count_1 (fifo sizes)=%d != count_2 (blocks in board)=%d in %s\n", count_1, count_2, where);
		fifos_involved = 1;
		goto _FKPU_failure;
	}
	if (count_1 != -1 && count_3 != -1 && count_1 != count_3) {
		iprintf ("count_1 (fifo sizes)=%d != count_3 (pointer in - out)=%d in %s\n", count_1, count_3, where);
		fifos_involved = 1;
		goto _FKPU_failure;
	}
	#ifndef NO_FEDKIT_PTHREAD
	pthread_mutex_unlock (&(rec->global_mutex));
	#endif
	return;
_FKPU_failure: 
	iprintf ("FEDKIT receiver dump :\n");
	iprintf ("csr = 0x%08x\n", rec->map[_FK_CSR_OFFSET/4]);
	iprintf ("count_1 = %d, count_2 = %d, count_3 = %d\n", count_1, count_2, count_3);
	iprintf ("in m/l/M %08x/%08x/%08x out l %08x/ count in%d/out%d\n",
		rec->min_pointer_in, rec->last_pointer_in, rec->max_pointer_in,
		rec->last_pointer_out, rec->pointer_in_count, rec->pointer_out_count);
	iprintf ("incount = %x, out_count = %x\n",
		rec->pointer_in_count, rec->pointer_out_count);
	fedkit_dump_info (rec);
	rec->map[0x10/4] = 0xdeadbeef;
	/* if less than 30 blocks, dump for everybody! */
	if (fifos_involved && count_1 < 30) {
		int i;
		iprintf ("data block fifo size= 0x%x\n", data_fifo_size);
		iprintf ("data block fifo dump (from last write and downwards until last read):\n");
		if (rec->data_fifo->write != rec->data_fifo->read) { /* if FIFO not empty */
			i = rec->data_fifo->write;
			do {
				if (--i<0) i+=rec->data_fifo->size;
				iprintf ("data_fifo[%03d] = 0x%08x\n", i, (int)rec->data_fifo_tab[i]);
			} while (i!= rec->data_fifo->read);
			iprintf ("\n");
		} else { 
			iprintf ("FIFO empty\n\n");
		}
		iprintf ("wc_fifo size = 0x%x\n",wc_fifo_size);
		iprintf ("blocks in board dump:\n");
		db = rec->data_blocks_in_board;
		i = 0;
		while (db != NULL) {
			iprintf ("data_blocks_in_board(0x%x) : 0x%08x\n",i++,(int)db->bus_address);
			db=db->next;
			count_2 ++;
		}

	}
	iprintf ("\n");
	#ifndef NO_FEDKIT_PTHREAD
	pthread_mutex_unlock (&(rec->global_mutex));
	#endif
	return;
}
#endif

/**
 * @fucntion _fedkit_enable_FIFO_interrupts
 * @param rec : the fedkit receiver strucuture
 */
inline void _fedkit_enable_FIFO_interrupts (struct fedkit_receiver * rec)
{
    if (rec->data_fifo->int_disabled) {
        int avail_pointers = rec->data_fifo->write - rec->data_fifo->read;
        if (avail_pointers < 0) avail_pointers+=rec->data_fifo->size;
        if (avail_pointers >= _FK_free_data_fifo_depth) {
            rec->data_fifo->int_disabled = 0;
            rec->map[_FK_CSR_OFFSET/4] |= _FK_CSR_HALF_EMPTY_INT_ENABLE | _FK_CSR_EMPTY_INT_ENABLE;
        }
    }
}

#ifdef FEDKIT_NO_INTERRUPT
/**
 * @function _fedkit_noint_pop_data_block
 * @params receiver : pointer to the receiver
 * @returns pointer to a free block, if available in the shared data FIFO.
 * Warning, this function is incompatible with the interrupt running!!
 *
 * TODO: ensure that this actually returns a physical address
 */
inline void * _fedkit_noint_pop_data (struct fedkit_receiver *receiver)
{
    void *ret;
    int next_read;
    if (receiver->data_fifo->read == receiver->data_fifo->write) {
        return NULL;
    }
    ret = receiver->data_fifo_tab[receiver->data_fifo->read];
    next_read = (receiver->data_fifo->read + 1) % receiver->data_fifo->size;
    receiver->data_fifo->read = next_read;
    receiver->data_fifo->done_read++;
    return ret;
}

/**
 * @function _fedkit_noint_handle_blocks
 * @params receiver : pointer to the receiver
 */
inline void _fedkit_noint_handle_blocks (struct fedkit_receiver *receiver)
{
    U32 csr = receiver->map[_FK_CSR_OFFSET/4];
    int target = 0;
    if (csr & _FK_CSR_EMPTY_INT_STATUS) {
        target = _FK_free_data_fifo_depth;
    } else if (csr & _FK_CSR_HALF_EMPTY_INT_STATUS) {
        target = _FK_free_data_fifo_depth/2;
    } else return;
    for (;target > 0;target--) {
        void * data_block = _fedkit_noint_pop_data (receiver);
        if (data_block != NULL) {

	  receiver->map[_FK_FRBKFIFO_OFFSET/4] = (U32)virt_to_bus(data_block);
	  ++receiver->num_free_block_fifo_entries_written;
        } else {
            target = -1;
        }
    }
}
#endif

/**
 * @fucntion _fedkit_disable_FIFO_interrupts
 * @param rec : the fedkit receiver strucuture
 */
inline void _fedkit_disable_FIFO_interrupts (struct fedkit_receiver * rec)
{
	rec->map[_FK_CSR_OFFSET/4] &= ~(_FK_CSR_HALF_EMPTY_INT_ENABLE | _FK_CSR_EMPTY_INT_ENABLE);
	/*rec->map[_FK_CSR_OFFSET/4] = rec->link_enabled;*/
	/*rec->interrupts_enabled = 0;*/
    rec->data_fifo->int_disabled = 1;
}


/**
 * @function _fedkit_fifo_used_slot_count 
 * function calculating the number of available (in) elements in a shared memory FIFO
 * @see _fedkit_feed_shared_fifo
 * 
 */
inline int _fedkit_fifo_used_slot_count (int write, int read, int fifo_depth)
{
	int ret;
	ret= write - read;
	if (ret < 0) {
		ret += fifo_depth;
	} else if (ret >= fifo_depth) { /* this condition should never occur in fact */
		ret -= fifo_depth;
	}
	return ret;
}

/**
 * @function _fedkit_fifo_free_slot_count
 * function calculating the number of elements that can still be pushed in the FIFO
 * @see _fedkit_feed_shared_fifo
 */
inline int _fedkit_fifo_free_slot_count (int write, int read, int fifo_depth)
{
	int ret;
	idprintf ("Free fifo slot count : wr=%d, read=%d, depth=%d\n", write,
		read, fifo_depth);
	ret= read - write - 1;
	if (ret < 0) {
		ret += fifo_depth;
	} else if (ret >= fifo_depth) { /* this condition should never occur in fact */
		ret -= fifo_depth;
	}
	idprintf ("result = %d\n", ret);
	return ret;
}


/**
 * @function _fedkit_feed_shared_fifo
 * @param data_address : the PCI (BUS/PHYSICAL) ADDRESS of the data block (where the board should do the DMA)
 *
 * @return 0 on success, non zero if FIFO already full
 *
 * This function picks free data ahd header buffers from the free list and feed 
 * them to the software fifos. The function allocates new headers if needed, but
 * the strategy for that has still to be defined. This function is called regularly. 
 * (on get_fragment by example). The interrupt will disable the interrupts if it fails
 * to provide (hardware_fifo_depth / 2 elements) therfore, this function will re-enable
 * the interrupt in order to get the fifo emptied to the hardware fifos by the interrupt
 * routine.
 *
 * FIFO algorithm
 * If FIFO are empty when write pointer == read pointer.
 * pointer point to the next slot to be used (read or written)
 * so no read if read==write
 * no write if write+1 == read [modulo size]
 * yes, this means we have at least one empty slot in the FIFO, but we can live
 * with this (4 bytes loss)
 */
inline int _fedkit_feed_shared_fifo(struct fedkit_receiver * rec, void * data_address)
{
	int next_write; /* we have to use this intermediate to avoid synchronisation problems */

	// idprintf ("\n");
	/*if (_fedkit_fifo_free_slot_count(rec->data_fifo->write, rec->data_fifo->read, 
			rec->data_fifo->size) >= 1) {*/
	if (((rec->data_fifo->write+1) % rec->data_fifo->size) != rec->data_fifo->read) {

		rec->data_fifo_tab[rec->data_fifo->write] = data_address;

		next_write = (rec->data_fifo->write+1) % rec->data_fifo->size;

		/*if (next_write >= rec->data_fifo->size)
			next_write -= rec->data_fifo->size;*/

		rec->data_fifo->write = next_write;
		rec->data_fifo->done_write++;

		#ifdef DEBUG_DATA_POINTERS
		{
			U32 pointer = (U32)data_address;
			
			rec->map[0x18/4] = pointer;
			
			if (rec->min_pointer_in == 0xFFFFFFFF) {
				rec->min_pointer_in = pointer;
			}
			if (rec->max_pointer_in == 0xFFFFFFFF || rec->max_pointer_in < pointer) {
				rec->max_pointer_in = pointer;
			}
			if (rec->min_pointer_in > pointer) {
				iprintf ("POINTER_DEBUG : have to go to new min pointer in was %08x, now %08x\n", 
					rec->min_pointer_in, pointer);
				rec->min_pointer_in = pointer;
			}
			if (rec->last_pointer_in == 0xFFFFFFFF) {
				rec->last_pointer_in = pointer;
			} else if (rec->last_pointer_in + rec->block_size +(rec->noalloc?4096:0)!= pointer) {
				if ((rec->last_pointer_in == rec->max_pointer_in) && 
					(rec->min_pointer_in == pointer)) {
					/* normal case, happy */
				} else {
					iprintf ("POINTER_DEBUG : unexpected pointer in: %x min=%x, max=%x, last=%x, expected=%x, block=%x\n", 
						pointer, rec->min_pointer_in, rec->max_pointer_in, rec->last_pointer_in, 
						rec->last_pointer_in + rec->block_size+(rec->noalloc?4096:0),
						rec->block_size);
				}
			}
			rec->last_pointer_in = pointer;
			rec->pointer_in_count++;
		}
		#endif
		return 0;
	} else {
		idprintf ("No space in free FIFO!!! Error\n");
		return -1;
	}
}

/**
 * @function _fedkit_free_datablock_descriptor recycles or allocates a data block descriptor
 * @param receiver  pointer to the receiver structure
 * @param descriptor pointer to the descriptor structure
 */
 
void _fedkit_free_datablock_descriptor (struct fedkit_receiver * receiver, struct _fedkit_data_block * blk)
{
	blk->next = receiver->free_data_block_descriptors;
	receiver->free_data_block_descriptors = blk;
}


/**
 * @function _fedkit_free_data_block
 * @param receiver pointer to the receiver
 * @param block : the data block to be freed
 */
inline void _fedkit_free_data_block (struct fedkit_receiver * receiver, struct _fedkit_data_block * block)
{
	int i;
	idprintf ("_fedkit_free_data_block : Freeing block %p\n", block);
	#ifndef NO_FEDKIT_PTHREAD
	pthread_mutex_lock (&(receiver->global_mutex));
	#endif
	if (_fedkit_feed_shared_fifo (receiver , (void *)((size_t)block->bus_address + receiver->header_size))) {
		/* the fifo was full. this should not happen unless we are in no_alloc mode*/
		if (receiver->noalloc==0) {
			edprintf ("Major failure : no space to put back a block in the shared FIFO\n");
			eprintf ("Major failure : no space to put back a block in the shared FIFO\n");
			eprintf ("Block is %p\n", block->bus_address);
			eprintf ("FIFO free slot count : %d\n", _fedkit_fifo_free_slot_count(receiver->data_fifo->write, 
				receiver->data_fifo->read, receiver->data_fifo->size));
			for (i = receiver->data_fifo->read; i != receiver->data_fifo->write; ++i) {
				if (i >= receiver->data_fifo->size) i-=receiver->data_fifo->size;
				eprintf ("FIFO[%d]=%p\n", i, receiver->data_fifo_tab[i]);
			}
			eprintf ("done read= %d, done write = %d\n",receiver->data_fifo->done_read, receiver->data_fifo->done_write);
		}
		#ifndef NO_FEDKIT_PTHREAD
		pthread_mutex_unlock (&(receiver->global_mutex));
		#endif
	} else {
		*(receiver->data_blocks_in_board_insert) = block;
		block->next = NULL;
		receiver->data_blocks_in_board_insert = &(block->next);
		#ifdef DEBUG_DATA_POINTERS
			_fedkit_pointer_ubercheck (receiver, "_fedkit_free_data_block");
		#endif
		#ifndef NO_FEDKIT_PTHREAD
		pthread_mutex_unlock (&(receiver->global_mutex));
		#endif

	}
}

/**
 * @function _fedkit_pop_block_in_board
 * Pops pointers to the next data block from the list of "blocks in board"
 * @param receiver poiner to the receiver
 * @return pointer to the next block or NULL if no block is available
 */
 
inline struct _fedkit_data_block * _fedkit_pop_block_in_board (struct fedkit_receiver * receiver)
{
	struct _fedkit_data_block * ret = NULL;
	#ifndef NO_FEDKIT_PTHREAD
	pthread_mutex_lock (&(receiver->global_mutex));
	#endif
	if (receiver->data_blocks_in_board != NULL) {
		ret = receiver->data_blocks_in_board;
		receiver->data_blocks_in_board = ret->next;
		/* now we must take care of the exhaustion case */
		if (receiver->data_blocks_in_board == NULL) {
			receiver->data_blocks_in_board_insert = &(receiver->data_blocks_in_board);
		}
	}
	#ifndef NO_FEDKIT_PTHREAD
	pthread_mutex_unlock (&(receiver->global_mutex));
	#endif
	idprintf ("_fedkit_pop_block_in_board : poping block %p\n", ret);
	return ret;
}

#endif /* ndef _FEDKIT_DATA_FIFO_H_ */
