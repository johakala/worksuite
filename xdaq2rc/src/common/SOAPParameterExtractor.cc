#include "xdaq/NamespaceURI.h"
#include "xdata/soap/NamespaceURI.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"

#include "xdata/XStr.h"

#include "xdaq2rc/SOAPParameterExtractor.hh"
#include "xcept/tools.h"

xdaq2rc::SOAPParameterExtractor::SOAPParameterExtractor( xdaq::Application* appl )
: appl_(appl)
{

}

std::string
xdaq2rc::SOAPParameterExtractor::extractParameters( xoap::MessageReference msg )
throw( xoap::exception::Exception )
{

  //extract command <ParameterSet> from SOAPMessage
  DOMNode* node  = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();

  if ( bodyList->getLength() != 1 )
  {
    XCEPT_RAISE (xoap::exception::Exception, toolbox::toString("Expected exactly one Element in soap message."));
    // Throw an exceptin indicating that software bug occured.
  }


  DOMNode* commandNode = bodyList->item(0); // this is the state change command node
  std::string commandName = xoap::XMLCh2String (commandNode->getLocalName());

  // The following if statement is only here in order to guarantee compatibility with the old RCMS in which the namespaces are not
  // correct

  if ( commandNode->getChildNodes()->getLength() > 0 )
  {

    try
    {
      serializer_.import( appl_->getApplicationInfoSpace(),commandNode );
    }
    catch (xdata::exception::Exception & xde)
    {
      XCEPT_RETHROW (xoap::exception::Exception, toolbox::toString("Parameter import failed in %s command", commandName.c_str()), xde);
    }
  }

  return commandName;
}
