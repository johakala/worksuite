#include "xcept/tools.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdata/ItemEvent.h"
#include "xoap/domutils.h"
#include "xoap/SOAPMessage.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xdaq2rc/RcmsStateNotifier.h"
#include "xdaq2rc/exception/Exception.h"

#include <sstream>
#include <time.h>


xdaq2rc::RcmsStateNotifier::RcmsStateNotifier
(
  Logger                      &applicationLogger,
  const xdaq::ApplicationDescriptor *applicationDescriptor,
  xdaq::ApplicationContext    *applicationContext
) :
logger_(Logger::getInstance(getLoggerName(applicationLogger))),
applicationDescriptor_(applicationDescriptor),
applicationContext_(applicationContext),
rcmsStateListenerContext_(0)
{
  // Set default values.
  setDefaults();
}


xdaq2rc::RcmsStateNotifier::RcmsStateNotifier
(
  xdaq::Application* const application
) :
logger_(Logger::getInstance(getLoggerName(application->getApplicationLogger()))),
applicationDescriptor_(application->getApplicationDescriptor()),
applicationContext_(application->getApplicationContext()),
rcmsStateListenerContext_(0)
{
  // Set default values.
  setDefaults();

  // Register the InfoSpace variables.
  xdata::InfoSpace* is = application->getApplicationInfoSpace();
  is->fireItemAvailable("rcmsStateListener", getRcmsStateListenerParameter());
  is->fireItemAvailable("foundRcmsStateListener", getFoundRcmsStateListenerParameter());
  findRcmsStateListener();
  subscribeToChangesInRcmsStateListener(is);
}


void xdaq2rc::RcmsStateNotifier::setDefaults()
{
  // Set the default values of the parameters "rcmsStateListener" and
  // "foundRcmsStateListener"
  rcmsStateListener_.bag.classname = "RCMSStateListener";
  rcmsStateListener_.bag.instance  = 0;
  rcmsStateListener_.bag.url  = "";
  rcmsStateListener_.bag.doStartupNotification  = false;
  foundRcmsStateListener_ = false;
}


std::string xdaq2rc::RcmsStateNotifier::getLoggerName
(
  Logger &applicationLogger
)
{
  std::stringstream oss;
  std::string       loggerName;


  oss << applicationLogger.getName() << ".RcmsStateNotifier";
  loggerName = oss.str();

  return loggerName;
}


xdaq2rc::RcmsStateNotifier::~RcmsStateNotifier()
{
  deleteRcmsStateListener();
}


void xdaq2rc::RcmsStateNotifier::deleteRcmsStateListener()
{
  if ( rcmsStateListenerContext_ != 0 )
  {
    delete rcmsStateListenerContext_;
    delete rcmsStateListenerDescriptor_;
    rcmsStateListenerContext_ = 0;
    rcmsStateListenerDescriptor_ = 0;
    foundRcmsStateListener_ = false;
  }
}


void xdaq2rc::RcmsStateNotifier::subscribeToChangesInRcmsStateListener
(
  xdata::InfoSpace *infoSpace
)
{
  appInfoSpace_ = infoSpace;

  try
  {
    infoSpace->addItemChangedListener("rcmsStateListener", this);
  }
  catch(xcept::Exception &e)
  {
    XCEPT_RETHROW(xdaq2rc::exception::InfoSpace,
                  "Failed to subscribe to changes in rcmsStateListener", e);
  }
}


xdata::Bag<xdaq2rc::ClassnameAndInstance>
*xdaq2rc::RcmsStateNotifier::getRcmsStateListenerParameter()
{
  return &rcmsStateListener_;
}


xdata::Boolean
*xdaq2rc::RcmsStateNotifier::getFoundRcmsStateListenerParameter()
{
  return &foundRcmsStateListener_;
}


void xdaq2rc::RcmsStateNotifier::findRcmsStateListener()
{
  deleteRcmsStateListener();

  const std::string url = rcmsStateListener_.bag.url;

  if ( url.empty() )
    retrieveRcmsStateListener();
  else
    createRcmsStateListener(url);
}


void xdaq2rc::RcmsStateNotifier::retrieveRcmsStateListener()
{
  try
  {
    rcmsStateListenerDescriptor_ =
      applicationContext_->
      getDefaultZone()->
      getApplicationDescriptor(rcmsStateListener_.bag.classname,
                               rcmsStateListener_.bag.instance);
  }
  catch(xcept::Exception &e)
  {
    rcmsStateListenerDescriptor_ = 0;

    LOG4CPLUS_WARN(logger_,
                   "Did not find RCMS state listener."
                   << " Classname:" << rcmsStateListener_.bag.classname.toString()
                   << " Instance:"  << rcmsStateListener_.bag.instance.toString());
  }

  // Update debugging parameter "foundRcmsStateListener"
  foundRcmsStateListener_ = rcmsStateListenerDescriptor_ != 0;
}


void xdaq2rc::RcmsStateNotifier::createRcmsStateListener(const std::string& url)
{
  rcmsStateListenerContext_ = new xdaq::ContextDescriptor(url);
  xdaq::ApplicationDescriptor*  rcmsStateListenerDescriptor = new xdaq::ApplicationDescriptorImpl
    (rcmsStateListenerContext_,"RCMSStateListener",0,"");
  rcmsStateListenerDescriptor->setAttribute("path","/services/replycommandreceiver");
  static_cast<xdaq::ApplicationDescriptorImpl*>(rcmsStateListenerDescriptor)->setInstance(0);

  foundRcmsStateListener_ = true;
  rcmsStateListenerDescriptor_ = rcmsStateListenerDescriptor;
}


void xdaq2rc::RcmsStateNotifier::actionPerformed(xdata::Event &event)
{
  xdata::ItemEvent *itemEvent = dynamic_cast<xdata::ItemEvent*>(&event);


  if(itemEvent == 0)
  {
    LOG4CPLUS_ERROR(logger_,
                    "Failed to cast *xdata::Event to *xdata::ItemEvent");

    return;
  }

  // If the parameter "rcmsStateListener" has changed value
  if
    (
      (itemEvent->type() == "ItemChangedEvent") &&
      (itemEvent->itemName() == "rcmsStateListener")
    )
  {
    const std::string url = rcmsStateListener_.bag.url.toString();

    std::stringstream oss;
    oss << "The parameter \"rcmsStateListener\" has changed value.";
    if ( url.empty() )
    {
      oss << " Classname:" << rcmsStateListener_.bag.classname.toString();
      oss << " Instance:"  << rcmsStateListener_.bag.instance.toString();
    }
    else
    {
      oss << " URL:"  << url;
    }
    oss << " doStartupNotification:"  << rcmsStateListener_.bag.doStartupNotification.toString();

    LOG4CPLUS_INFO(logger_, oss.str());

    findRcmsStateListener();

    if ( rcmsStateListener_.bag.doStartupNotification )
    {
      std::string stateName = "Alive";
      try
      {
        stateName = appInfoSpace_->find("stateName")->toString();
      }
      catch( xdata::exception::Exception& )
      {
        // no stateName found, go with default one
      }

      stateChanged(stateName, "startupNotification");
    }
  }
}


void xdaq2rc::RcmsStateNotifier::stateChanged
(
  const std::string &state,
  const std::string &reasonForStateChange
)
{
  // If the RCMS state listener is not present
  // (it is not compulsary that it should be)
  if(rcmsStateListenerDescriptor_ == 0)
  {
    std::stringstream oss;

    oss << "Unable to notify state change.";
    oss << " RCMS state listener has not been found.";
    oss << " Has findRcmsStateListener() been called at least once?";
    oss << " State:" << state;

    LOG4CPLUS_WARN(logger_, oss.str());
  }
  else
  {
    xoap::MessageReference msg;

    // Time stamp is the number of seconds since the Epoch
    ::time_t timeStamp = ::time(0);

    msg = createStateNotificationMsg
      (
        getApplicationUrl(applicationDescriptor_),    // application
        rcmsStateListenerDescriptor_->getClassName(), // class name
        rcmsStateListenerDescriptor_->getInstance(),  // instance
        state,                                        // state
        reasonForStateChange,                         // reasonForStateChange
        true,                                         // asynchronousReply
        0,                                            // timeout
        timeStamp,                                    // timeStamp
        0                                             // transactionID
      );

    try
    {
      xoap::MessageReference reply = applicationContext_->postSOAP(msg,
                                                                   *applicationDescriptor_, *rcmsStateListenerDescriptor_);

      // Check if the reply indicates a fault occurred
      xoap::SOAPBody replyBody =
        reply->getSOAPPart().getEnvelope().getBody();

      if(replyBody.hasFault())
      {
        std::stringstream oss;

        oss << "Received fault reply from ";
        oss << rcmsStateListenerDescriptor_->getClassName();
        oss << rcmsStateListenerDescriptor_->getInstance();
        oss << " : " << replyBody.getFault().getFaultString();

        XCEPT_RAISE(xdaq2rc::exception::BadReply, oss.str());
      }
    }
    catch(xcept::Exception &e)
    {
      std::stringstream oss;
      std::string       s;

      oss << "Failed to send state change notification message to RCMS";
      oss << " state listener.";
      oss << " Listener url: ";
      oss << getApplicationUrl(rcmsStateListenerDescriptor_);
      oss << " State:" << state;
      s = oss.str();

      XCEPT_RETHROW(xdaq2rc::exception::SendFailed, s, e);
    }
  }
}


std::string xdaq2rc::RcmsStateNotifier::getApplicationUrl
(
  const xdaq::ApplicationDescriptor *appDescriptor
)
{
  std::string url;


  url  = appDescriptor->getContextDescriptor()->getURL();
  url += "/";
  url += appDescriptor->getURN();

  return url;
}


xoap::MessageReference
xdaq2rc::RcmsStateNotifier::createStateNotificationMsg
(
  const std::string &sourceURL,
  const std::string &className,
  const uint32_t    &instance,
  const std::string &state,
  const std::string &reasonForStateChange,
  const bool        asynchronousReply,
  const int64_t     timeout,
  const int64_t     timeStamp,
  const int64_t     transactionID
)
{
  std::stringstream oss;


  try
  {
    xoap::MessageReference message = xoap::createMessage();
    xoap::SOAPPart soapPart = message->getSOAPPart();
    xoap::SOAPEnvelope envelope = soapPart.getEnvelope();

    envelope.addNamespaceDeclaration("xsi",
                                     "http://www.w3.org/2001/XMLSchema-instance");
    envelope.addNamespaceDeclaration("xsd",
                                     "http://www.w3.org/2001/XMLSchema");
    envelope.addNamespaceDeclaration("soapenv",
                                     "http://schemas.xmlsoap.org/soap/envelope/");

    xoap::SOAPBody body = envelope.getBody();

    xoap::SOAPName sendNotificationReplyName =
      envelope.createName("sendNotificationReply", "ns1",
                          "http://replycommandreceiver.ws.fm.rcms");
    xoap::SOAPBodyElement sendNotificationReplyElement =
      body.addBodyElement(sendNotificationReplyName);
    xoap::SOAPName encodingStyleName = envelope.createName("encodingStyle",
                                                           "soapenv", "http://schemas.xmlsoap.org/soap/envelope/");

    sendNotificationReplyElement.addAttribute(encodingStyleName,
                                              "http://schemas.xmlsoap.org/soap/encoding/");

    xoap::SOAPName notReplyName = envelope.createName("notReply", "", "");
    xoap::SOAPElement notReplyElement =
      sendNotificationReplyElement.addChildElement(notReplyName);

    xoap::SOAPName hrefName = envelope.createName("href", "", "");
    notReplyElement.addAttribute(hrefName, "#id0");

    xoap::SOAPName multiRefName = envelope.createName("multiRef", "", "");

    xoap::SOAPBodyElement multiRefId0Element =
      body.addBodyElement(multiRefName);
    multiRefId0Element.addAttribute(encodingStyleName,
                                    "http://schemas.xmlsoap.org/soap/encoding/");

    xoap::SOAPName idName = envelope.createName("id", "", "");
    xoap::SOAPName rootName = envelope.createName("root", "soapenc",
                                                  "http://schemas.xmlsoap.org/soap/encoding/");
    xoap::SOAPName typeName = envelope.createName("type", "xsi",
                                                  "http://www.w3.org/2001/XMLSchema-instance");


    // href #id0 refers to notReply

    multiRefId0Element.addAttribute(idName, "id0");
    multiRefId0Element.addAttribute(rootName, "0");
    multiRefId0Element.addAttribute(typeName, "ns2:NotificationReplyBean");
    multiRefId0Element.addNamespaceDeclaration("soapenc",
                                               "http://schemas.xmlsoap.org/soap/encoding/");
    multiRefId0Element.addNamespaceDeclaration("ns2",
                                               "urn:ReplyCommandReceiver");

    xoap::SOAPName asynchronousReplyName =
      envelope.createName("asynchronousReply","","");

    xoap::SOAPElement asynchronousReplyElement =
      multiRefId0Element.addChildElement(asynchronousReplyName);

    asynchronousReplyElement.addAttribute(hrefName, "#id1");

    xoap::SOAPName sourceURLName = envelope.createName("sourceURL","","");
    xoap::SOAPElement sourceURLElement =
      multiRefId0Element.addChildElement(sourceURLName);
    sourceURLElement.addAttribute(typeName, "soapenc:string");
    sourceURLElement.addTextNode(sourceURL);

    xoap::SOAPName stateNameName = envelope.createName("stateName","","");
    xoap::SOAPElement stateNameElement =
      multiRefId0Element.addChildElement(stateNameName);
    stateNameElement.addAttribute(typeName, "soapenc:string");
    stateNameElement.addTextNode(state);

    xoap::SOAPName userFieldName = envelope.createName("userField","","");
    xoap::SOAPElement userFieldElement =
      multiRefId0Element.addChildElement(userFieldName);
    userFieldElement.addAttribute(typeName, "soapenc:string");
    userFieldElement.addTextNode(reasonForStateChange);

    xoap::SOAPName timeOutName = envelope.createName("timeOut", "", "");
    xoap::SOAPElement timeOutElement =
      multiRefId0Element.addChildElement(timeOutName);
    timeOutElement.addAttribute(hrefName, "#id2");

    xoap::SOAPName timeStampName = envelope.createName("timeStamp", "", "");
    xoap::SOAPElement timeStampElement =
      multiRefId0Element.addChildElement(timeStampName);
    timeStampElement.addAttribute(hrefName, "#id3");

    xoap::SOAPName transactionIDName = envelope.createName("transactionID",
                                                           "", "");
    xoap::SOAPElement transactionIDElement =
      multiRefId0Element.addChildElement(transactionIDName);
    transactionIDElement.addAttribute(hrefName, "#id4");


    // href #id1 refers to asynchronousReply

    xoap::SOAPBodyElement multiRefId1Element =
      body.addBodyElement(multiRefName);
    multiRefId1Element.addAttribute(encodingStyleName,
                                    "http://schemas.xmlsoap.org/soap/encoding/");
    multiRefId1Element.addAttribute(idName, "id1");
    multiRefId1Element.addAttribute(rootName, "0");
    multiRefId1Element.addAttribute(typeName, "xsd:boolean");
    multiRefId1Element.addNamespaceDeclaration("soapenc",
                                               "http://schemas.xmlsoap.org/soap/encoding/");
    if(asynchronousReply)
    {
      multiRefId1Element.addTextNode("true");
    }
    else
    {
      multiRefId1Element.addTextNode("false");
    }


    // href #id2 refers to timeOut

    xoap::SOAPBodyElement multiRefId2Element =
      body.addBodyElement(multiRefName);
    multiRefId2Element.addAttribute(encodingStyleName,
                                    "http://schemas.xmlsoap.org/soap/encoding/");
    multiRefId2Element.addAttribute(idName, "id2");
    multiRefId2Element.addAttribute(rootName, "0");
    multiRefId2Element.addAttribute(typeName, "xsd:long");
    multiRefId2Element.addNamespaceDeclaration("soapenc",
                                               "http://schemas.xmlsoap.org/soap/encoding/");
    oss.str("");
    oss << timeout;
    multiRefId2Element.addTextNode(oss.str());


    // href #id3 refers to timeStamp

    xoap::SOAPBodyElement multiRefId3Element =
      body.addBodyElement(multiRefName);
    multiRefId3Element.addAttribute(encodingStyleName,
                                    "http://schemas.xmlsoap.org/soap/encoding/");
    multiRefId3Element.addAttribute(idName, "id3");
    multiRefId3Element.addAttribute(rootName, "0");
    multiRefId3Element.addAttribute(typeName, "xsd:long");
    multiRefId3Element.addNamespaceDeclaration("soapenc",
                                               "http://schemas.xmlsoap.org/soap/encoding/");
    oss.str("");
    oss << timeStamp;
    multiRefId3Element.addTextNode(oss.str());


    // href #id4 refers to transactionID

    xoap::SOAPBodyElement multiRefId4Element =
      body.addBodyElement(multiRefName);
    multiRefId4Element.addAttribute(encodingStyleName,
                                    "http://schemas.xmlsoap.org/soap/encoding/");
    multiRefId4Element.addAttribute(idName, "id4");
    multiRefId4Element.addAttribute(rootName, "0");
    multiRefId4Element.addAttribute(typeName, "xsd:long");
    multiRefId4Element.addNamespaceDeclaration("soapenc",
                                               "http://schemas.xmlsoap.org/soap/encoding/");
    oss.str("");
    oss << transactionID;
    multiRefId4Element.addTextNode(oss.str());

    // Add SOAPAction header to be compatible with Axis
    std::stringstream soapActionValue;
    soapActionValue << "urn:xdaq-application:class=";
    soapActionValue << className;
    soapActionValue << ",instance=";
    soapActionValue << instance;
    message->getMimeHeaders()->setHeader("SOAPAction",
                                         soapActionValue.str());

    return message;
  }
  catch(xcept::Exception &e)
  {
    std::stringstream oss;
    oss << "Failed to create state notification message.";
    oss << " State:" << state;

    XCEPT_RETHROW(xdaq2rc::exception::soap,
                  oss.str(), e);
  }
  catch(...)
  {
    std::stringstream oss;
    oss << "Failed to create state notification message.";
    oss << " State:" << state;

    XCEPT_RAISE(xdaq2rc::exception::soap,
                oss.str());
  }
}
