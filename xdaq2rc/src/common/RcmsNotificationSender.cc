#include "xcept/tools.h"
#include "xdata/ItemEvent.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xoap/domutils.h"
#include "xoap/SOAPMessage.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xdaq2rc/RcmsNotificationSender.h"
#include "xdaq2rc/exception/Exception.h"

#include <sstream>
#include <time.h>


xdaq2rc::RcmsNotificationSender::RcmsNotificationSender
(
  Logger                      &applicationLogger,
  const xdaq::ApplicationDescriptor *applicationDescriptor,
  xdaq::ApplicationContext    *applicationContext
) :
logger_(Logger::getInstance(getLoggerName(applicationLogger))),
applicationDescriptor_(applicationDescriptor),
applicationContext_(applicationContext)
{
  // Set the default values of the parameters
  rcmsNotificationReceiver_.bag.classname = "RCMSNotificationReceiver";
  rcmsNotificationReceiver_.bag.instance  = 0;
  rcmsNotificationReceiver_.bag.hltsgURI  = "";
  foundRcmsNotificationReceiver_ = false;
}


std::string xdaq2rc::RcmsNotificationSender::getLoggerName
(
  Logger &applicationLogger
)
{
  std::stringstream oss;
  std::string       loggerName;

  oss << applicationLogger.getName() << ".RcmsNotificationSender";
  loggerName = oss.str();

  return loggerName;
}


xdaq2rc::RcmsNotificationSender::~RcmsNotificationSender()
{
}


void xdaq2rc::RcmsNotificationSender::subscribeToChangesInRcmsNotificationReceiver
(
  xdata::InfoSpace *infoSpace
)
{
  appInfoSpace_ = infoSpace;

  try
  {
    infoSpace->addItemChangedListener("rcmsNotificationReceiver", this);
  }
  catch(xcept::Exception &e)
  {
    XCEPT_RETHROW(xdaq2rc::exception::InfoSpace,
                  "Failed to subscribe to changes in rcmsNotificationReceiver", e);
  }
}


xdata::Bag<xdaq2rc::RcmsNotifierInstance>
*xdaq2rc::RcmsNotificationSender::getRcmsNotificationReceiverParameter()
{
  return &rcmsNotificationReceiver_;
}


xdata::Boolean
*xdaq2rc::RcmsNotificationSender::getFoundRcmsNotificationReceiverParameter()
{
  return &foundRcmsNotificationReceiver_;
}


void xdaq2rc::RcmsNotificationSender::findRcmsNotificationReceiver()
{
  try
  {
    rcmsNotificationReceiverDescriptor_ =
      applicationContext_->
      getDefaultZone()->
      getApplicationDescriptor(rcmsNotificationReceiver_.bag.classname,
                               rcmsNotificationReceiver_.bag.instance);
  }
  catch(xcept::Exception &e)
  {
    rcmsNotificationReceiverDescriptor_ = 0;

    LOG4CPLUS_WARN(logger_,
                   "Did not find RCMS notification receiver."
                   << " Classname:" << rcmsNotificationReceiver_.bag.classname.toString()
                   << " Instance:"  << rcmsNotificationReceiver_.bag.instance.toString());
  }

  // Update debugging parameter "foundRcmsNotificationReceiver"
  foundRcmsNotificationReceiver_ = rcmsNotificationReceiverDescriptor_ != 0;
}


void xdaq2rc::RcmsNotificationSender::actionPerformed(xdata::Event &event)
{
  xdata::ItemEvent *itemEvent = dynamic_cast<xdata::ItemEvent*>(&event);


  if(itemEvent == 0)
  {
    LOG4CPLUS_ERROR(logger_,
                    "Failed to cast *xdata::Event to *xdata::ItemEvent");

    return;
  }

  // If the parameter "rcmsNotificationReceiver" has changed value
  if
    (
      (itemEvent->type() == "ItemChangedEvent")      &&
      (itemEvent->itemName() == "rcmsNotificationReceiver")
    )
  {
    std::stringstream oss;

    oss << "The parameter \"rcmsNotificationReceiver\" has changed value.";
    oss << " Classname:" << rcmsNotificationReceiver_.bag.classname.toString();
    oss << " Instance:"  << rcmsNotificationReceiver_.bag.instance.toString();
    oss << " hltsgURI:"  << rcmsNotificationReceiver_.bag.hltsgURI.toString();

    LOG4CPLUS_INFO(logger_, oss.str());

    findRcmsNotificationReceiver();

  }
}


void xdaq2rc::RcmsNotificationSender::sendData
(
  xdata::Table& table,
  const std::string& name
)
{
  // If the RCMS notification receiver is not present
  // (it is not compulsary that it should be)
  if(rcmsNotificationReceiverDescriptor_ == 0)
  {
    std::stringstream oss;

    oss << "Unable to send data for " << name << ".";
    oss << " RCMS notification receiver has not been found.";
    oss << " Has findRcmsNotificationReceiver() been called at least once?";

    LOG4CPLUS_WARN(logger_, oss.str());
  }
  else
  {
    xoap::MessageReference msg;

    // Time stamp is the number of seconds since the Epoch
    ::time_t timeStamp = ::time(0);

    try
    {
      msg = createNotificationMsg(
        getApplicationUrl(applicationDescriptor_),           // application
        table,                                               // data
        name,                                                // name
        timeStamp                                            // timeStamp
      );
    }
    catch(xcept::Exception &e)
    {
      std::stringstream oss;

      oss << "Failed to create data notification message for ";
      oss << name << ".";

      XCEPT_RETHROW(xdaq2rc::exception::soap, oss.str(), e);
    }

    try
    {
      xoap::MessageReference reply = applicationContext_->postSOAP(msg,
                                                                   *applicationDescriptor_, *rcmsNotificationReceiverDescriptor_);

      if (! reply.isNull() )
      {
        // Check if the reply indicates a fault occurred
        xoap::SOAPBody replyBody =
          reply->getSOAPPart().getEnvelope().getBody();

        if(replyBody.hasFault())
        {
          std::stringstream oss;
          oss << "Received fault reply from ";
          oss << rcmsNotificationReceiverDescriptor_->getClassName();
          oss << rcmsNotificationReceiverDescriptor_->getInstance();
          oss << " : " << replyBody.getFault().getFaultString();

          XCEPT_RAISE(xdaq2rc::exception::BadReply, oss.str());
        }
      }
      else
      {
        std::stringstream oss;
        oss << "Received no reply from ";
        oss << rcmsNotificationReceiverDescriptor_->getClassName();
        oss << rcmsNotificationReceiverDescriptor_->getInstance();

        XCEPT_RAISE(xdaq2rc::exception::SendFailed, oss.str());
      }
    }
    catch(xcept::Exception &e)
    {
      std::stringstream oss;

      oss << "Failed to send data notification message to RCMS";
      oss << " notification receiver.";
      oss << " Listener url: ";
      oss << getApplicationUrl(rcmsNotificationReceiverDescriptor_).toString();

      XCEPT_RETHROW(xdaq2rc::exception::SendFailed, oss.str(), e);
    }
  }
}


toolbox::net::URL xdaq2rc::RcmsNotificationSender::getApplicationUrl
(
  const xdaq::ApplicationDescriptor *appDescriptor
)
{
  std::string url;

  url  = appDescriptor->getContextDescriptor()->getURL();
  url += "/";
  url += appDescriptor->getURN();

  return toolbox::net::URL(url);
}


xoap::MessageReference
xdaq2rc::RcmsNotificationSender::createNotificationMsg
(
  const toolbox::net::URL  &sourceURL,
  xdata::Table             &table,
  const std::string        &name,
  const int64_t            timeStamp
)
{
  const std::string namespacePrefix = "xmas";
  const std::string namespaceUri = "http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10";

  const std::string sensorNamespacePrefix = "xmas-sensor";
  const std::string sensorNamespaceUri = "http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-sensor-10";

  xoap::MessageReference msg = xoap::createMessage();
  xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();

  xoap::SOAPName responseName = envelope.createName( "report", namespacePrefix, namespaceUri);
  (void) envelope.getBody().addBodyElement ( responseName );
  xoap::SOAPName reportName ("report", namespacePrefix, namespaceUri);
  xoap::SOAPElement reportElement = envelope.getBody().getChildElements(reportName)[0];
  reportElement.addNamespaceDeclaration (sensorNamespacePrefix, sensorNamespaceUri);

  xoap::SOAPName sampleName =
    envelope.createName( "sample", namespacePrefix, namespaceUri );
  xoap::SOAPElement sampleElement = reportElement.addChildElement( sampleName );

  xoap::SOAPName flashListName = envelope.createName( "flashlist", "", "" );
  sampleElement.addAttribute( flashListName, name );

  xoap::SOAPName tagName = envelope.createName( "tag", "", "");
  sampleElement.addAttribute( tagName, "tag" );

  xoap::MimeHeaders* headers = msg->getMimeHeaders();
  headers->removeHeader( "x-xdaq-tags" );
  headers->addHeader( "x-xdaq-tags", "tag" );
  tagName = envelope.createName( "originator", "", "");
  sampleElement.addAttribute( tagName, sourceURL.toString() );
  headers->setHeader("SOAPAction", rcmsNotificationReceiver_.bag.hltsgURI.toString());
  headers->setHeader("Content-Location", sourceURL.toString() );

  xdata::exdr::Serializer serializer;
  xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;

  serializer.exportAll( &table, &outBuffer );

  xoap::AttachmentPart * attachment =
    msg->createAttachmentPart(
      outBuffer.getBuffer(),
      outBuffer.tellp(),
      "application/x-xdata+exdr"
    );
  attachment->setContentEncoding( "binary" );
  tagName = envelope.createName( "tag", "", "" );
  sampleElement.addAttribute( tagName, "tag" );

  std::stringstream contentId;
  contentId << "<" << name << "@" << sourceURL.getHost() << ">";
  attachment->setContentId( contentId.str() );
  //attachment->setContentLocation( sourceURL.toString() );

  std::stringstream disposition;
  disposition << "attachment; filename=" << name << ".exdr; creation-date="
    << "\"" << timeStamp << "\"";
  attachment->addMimeHeader( "Content-Disposition", disposition.str() );
  msg->addAttachmentPart( attachment );

  return msg;
}

