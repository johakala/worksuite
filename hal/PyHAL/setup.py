#!/usr/bin/python
import setuptools
from distutils.core import setup, Extension

from Cython.Build import cythonize

DAQHOME="../.."

ext_modules = [ 
    Extension( "hal",

               sources = ["hal.pyx"],

               include_dirs=[DAQHOME + '/hal',
                             DAQHOME + '/hal/busAdapter/pci/include',
                             DAQHOME + '/hal/generic/include',
                             DAQHOME + '/xpci/include',
                             DAQHOME + '/xpci/drv/include',
                             '/opt/xdaq/include'],

               library_dirs=[DAQHOME + '/hal/generic/lib/linux/x86_64_centos7',
                             DAQHOME + '/hal/busAdapter/pci/lib/linux/x86_64_centos7',
                             DAQHOME + '/xpci/lib/linux/x86_64_centos7',
                             '/opt/xdaq/lib'],

               libraries=['xcept',
                          'toolbox',
                          'config',
                          'log4cplus',
                          'asyncresolv',
                          'uuid',
                          'GenericHAL',
                          'PCILinuxBusAdapter',
                          'xerces-c',
                          'xpci',
                      ]
           )
]   

setup( name = "hal",
       version = '0.1',
       author = 'Christoph Schwick',
       description = 'Python bindings to the CMS HAL software for hardware access',
       ext_modules=cythonize(ext_modules))
