# distutils: language = c++

from libc.stdint cimport uint32_t, uint64_t
from libcpp.vector cimport vector
from libcpp cimport bool
from libcpp.string cimport string
from cpython cimport array

cimport decl



# A global variable for the module which holds the instance of the BusAdapter.
# The user does not care about the BusAdapter (in general...). 
cdef decl.PCILinuxBusAdapter PCI_BA



# Defintions of the various Enums used int the HAL

cdef extern from "hal/HardwareDeviceInterface.hh" namespace "HAL":
    cdef enum HalVerifyOption:
         HAL_NO_VERIFY,
         HAL_DO_VERIFY

cdef extern from "hal/HardwareDeviceInterface.hh" namespace "HAL":
    cdef enum HalPollMethod:
        HAL_POLL_UNTIL_EQUAL,
        HAL_POLL_UNTIL_DIFFERENT

cdef extern from "hal/HardwareDeviceInterface.hh" namespace "HAL":
    cdef enum HalAddressIncrement:
        HAL_DO_INCREMENT,
        HAL_NO_INCREMENT


# Defintion of the PCIDevice with all the relevant functions which will
# be exposed to the python object.
# Results will be returned via the return value. This is more python style
# than returning the value in a pssed reference as in the C++ HAL API.

cdef class PCIDevice:
    cdef decl.PCIDevice  *pciDevice
    cdef decl.PCIAddressTableASCIIReader *treader
    cdef decl.PCIAddressTable *atable

    def __cinit__( self, 
                  string addressTablePath,
                  uint32_t vendorID,
                  uint32_t deviceID,
                  uint32_t index,
                  bool swapFlag = False):
        self.treader =  new decl.PCIAddressTableASCIIReader( addressTablePath )
        self.atable = new decl.PCIAddressTable( "Address Table", self.treader[0] )
        self.pciDevice = new decl.PCIDevice( self.atable[0],
                                             PCI_BA,
                                             vendorID,
                                             deviceID,
                                             index,
                                             swapFlag )
# The following functions should not be exposed. In the HAL they
# were introduced to accelerate the JAL firmware writing software
# which does a lot of config space accesses and which sufferes from
# many item lookups. The function is not meant to be used by users.
# 
#     def configWrite( self, 
#                      uint32_t address, 
#                      uint32_t data ):
#         self.pciDevice.configWrite( address,
#                                     data )
#     def configWrite64( self, 
#                       uint64_t address, 
#                        uint64_t data ):
#         self.pciDevice.configWrite64( address,
#                                       data )
# 
#     def configRead( self, 
#                     uint32_t address ) :
#         cdef uint32_t value[1] 
#         self.pciDevice.configRead( address, 
#                                    value )
#         return value[0]
#                 
#     def configRead64( self, 
#                       uint64_t address ) :
#         cdef uint64_t value[1] 
#         self.pciDevice.configRead64( address, 
#                                      value )
#         return value[0]
                        
    def write( self,
               string item,
               uint32_t data,
               HalVerifyOption verifyFlag = HAL_NO_VERIFY,
               uint32_t offset = 0 ) :
        self.pciDevice.write( item,
                              data,
                              verifyFlag,
                              offset )

    def write64( self,
                 string item,
                 uint64_t data,
                 HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                 uint64_t offset = 0 ) :
        self.pciDevice.write64( item,
                                data,
                                verifyFlag,
                                offset )


    def read( self, 
              string item,
              uint32_t offset = 0 ) :
        cdef uint32_t value[1]
        self.pciDevice.read( item,
                             value,
                             offset )
        return value[0]

    def read64( self, 
                string item,
                uint64_t offset = 0 ) :
        cdef uint64_t value[1]
        self.pciDevice.read64( item,
                               value,
                               offset )
        return value[0]

    def pollItem( self,
                  string item,
                  uint32_t referenceValue,
                  uint32_t timeout,
                  HalPollMethod pollMethod = HAL_POLL_UNTIL_EQUAL,
                  uint32_t offset = 0 ) :
        cdef uint32_t result[1]
        self.pciDevice.pollItem( item,
                                 referenceValue,
                                 timeout,
                                 result,
                                 pollMethod,
                                 offset )
        return result[0]

    def pollItem64( self,
                    string item,
                    uint64_t referenceValue,
                    uint64_t timeout,
                    HalPollMethod pollMethod = HAL_POLL_UNTIL_EQUAL,
                    uint64_t offset = 0 ) :
        cdef uint64_t result[1]
        self.pciDevice.pollItem64( item,
                                   referenceValue,
                                   timeout,
                                   result,
                                   pollMethod,
                                   offset )
        return result[0]

    def writeBlock( self,
                    string item,
                    array.array buffer,
                    HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                    HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                    uint32_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.pciDevice.writeBlock( item,
                                   length,
                                   buffer.data.as_chars,
                                   verifyFlag,
                                   addressBehaviour,
                                   offset )
        
    def writeBlock64( self,
                      string item,
                      array.array buffer,
                      HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                      HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                      uint64_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.pciDevice.writeBlock64( item,
                                     length,
                                     buffer.data.as_chars,
                                     verifyFlag,
                                     addressBehaviour,
                                     offset )
        
    def readBlock( self,
                   string item,
                   array.array buffer,
                   HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                   uint32_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        self.pciDevice.readBlock( item,
                                  length,
                                  buffer.data.as_chars,
                                  addressBehaviour,
                                  offset )
        
    def readBlock64( self,
                     string item,
                     array.array buffer,
                     HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT,
                     uint64_t offset = 0) :

        length = len(buffer)*buffer.itemsize
        print("len is ", length)
        self.pciDevice.readBlock64( item,
                                    length,
                                    buffer.data.as_chars,
                                    addressBehaviour,
                                    offset )

    def setBit( self,
                string item,
                HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                uint32_t offset = 0 ):
        self.pciDevice.setBit( item, verifyFlag, offset )

    def setBit64( self,
                  string item,
                  HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                  uint64_t offset = 0 ):
        self.pciDevice.setBit64( item, verifyFlag, offset )

    def resetBit( self,
                  string item,
                  HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                  uint32_t offset = 0 ):
        self.pciDevice.resetBit( item, verifyFlag, offset )

    def resetBit64( self,
                    string item,
                    HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                    uint64_t offset = 0 ):
        self.pciDevice.resetBit64( item, verifyFlag, offset )

    def isSet( self,
               string item,               
               uint32_t offset = 0 ):
        return self.pciDevice.isSet( item, offset )

    def isSet64( self,
                 string item,
                 uint64_t offset = 0 ):
        return self.pciDevice.isSet64( item, offset )

    def writePulse( self,
                   string item,
                   uint32_t offset = 0):
        self.pciDevice.writePulse( item, offset )

    def writePulse64( self,
                     string item,
                     uint64_t offset = 0):
        self.pciDevice.writePulse64( item, offset )

    def readPulse( self,
                   string item,
                   uint32_t offset = 0):
        self.pciDevice.readPulse( item, offset )

    def readPulse64( self,
                     string item,
                     uint64_t offset = 0):
        self.pciDevice.readPulse64( item, offset )

    def unmaskedRead( self,
                      string item,
                      uint32_t offset = 0 ) :
        cdef uint32_t value[1]
        self.pciDevice.unmaskedRead( item, value, offset )
        return value[0]

    def unmaskedRead64( self,
                        string item,
                        uint64_t offset = 0 ) :
        cdef uint64_t value[1]
        self.pciDevice.unmaskedRead64( item, value, offset )
        return value[0]

    def unmaskedWrite( self,
                       string item,
                       uint32_t data,
                       HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                       uint32_t offset = 0 ):
        self.pciDevice.unmaskedWrite( item, data, verifyFlag, offset )
        
    def unmaskedWrite64( self,
                         string item,
                         uint64_t data,
                         HalVerifyOption verifyFlag = HAL_NO_VERIFY,
                         uint64_t offset = 0 ):
        self.pciDevice.unmaskedWrite64( item, data, verifyFlag, offset )
        
    def  check( self,
                string item,
                uint32_t expectation,
                string faultMessage = "",
                uint32_t offset = 0 ) :
        res = self.pciDevice.check( item, expectation, faultMessage, offset )
        return res

    def  check64( self,
                  string item,
                  uint64_t expectation,
                  string faultMessage = "",
                  uint64_t offset = 0 ) :
        res = self.pciDevice.check64( item, expectation, faultMessage, offset )
        return res



# The busadapter class may be used by the user but this module holds a global BusAdapter instance
# which is used for the PCIDevices defined in a programme. 

cdef class PCILinuxBusAdapter:
    cdef decl.PCILinuxBusAdapter pciba
    
    cdef findDeviceByVendor( self, 
                             uint32_t vendorID, 
                             uint32_t deviceID,
                             uint32_t index,
                             const decl.PCIAddressTable& pciAddressTable,
                             decl.PCIDeviceIdentifier** deviceIdentifierPtr,
                             vector[uint32_t]& barRegisters,
                             bool swapFlag ):
        self.pciba.findDeviceByVendor( vendorID, deviceID, index, pciAddressTable, deviceIdentifierPtr, barRegisters, swapFlag)
