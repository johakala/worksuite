********************************************************************************************************************
* FRL address map
*****************************************************************************************************************
*  item                                 accessmode	bar	address	                    mask	read	write	description
*****************************************************************************************************************
*
*

CFG_VENDOR_ID                     	configuration		00000000	        0000FFFF	1	0	   This register corresponds to the Vendor ID of the board (function 2 of the card).
*															   Value: ECD6
*															
Device_ID                         	configuration		00000000	        FFFF0000	1	0		This register correspond to the Device ID of the FRL logic element (function 2 of the card)
*																Value: FF01
*															
Memory                            	configuration		00000004	        00000001	1	1		This bit when set by the BIOS able card to be accessed in memory space.
*															
Bus                               	configuration		00000004	        00000002	1	1		This bit when set by the BIOS able the card to do master access on PCI bus
*															
Base                              	configuration		00000010	        FFFFFFFF	1	1		This register is write by the BIOS to indicate at which address the card responds to a PCI access.
*															
JTAG_ctrl                         	configuration		00000040	        0007FFFF	1	1			This register controls the JTAG :
*																-bit 0: Sets this bit to ‘1’ to control the JTAG bus via software. This bit should bit reset to ‘0’ if you want to plug the JTAG controller in the connector on the back of the board. The PCI configuration should be read before the reload to avoid rebooting the PC. A soft-reset should be executed inside the bridge and the PCI configuration rewrite.
*																-bit 1: writes this bit to ‘1’ will reload the bridge FPAG with the firmware inside the EEPROM. The PCI configuration should be read before the FPGA reload to avoid rebooting the PC. The the PCI configuration can be written back.
*																-bit 2: writes this bit to ‘1’ will reload the main FRL FPGA with the firmware inside the flash memory. The design to be uploading is specified by the bit 18..16 (see below). (The FEROL and FRL PCI onfiguration should read first and write back after the FPGA reloaded.
*															             You can monitor the bit x of the command offset 0x...... to check if the FPGA reload is finished or not.
*																-bit18..16: these 3 bits are used to specify the design we want to upload inside the main FRL FPGA. Four different designs are available (actually only two designs exist 0 and 1).
*															
firmware                          	configuration		00000048	        FFFFFFE0	1	0			This register will be increment to the 16 lower bits at each design compilation. The upper 16-bit corresponds to the design number.
*															
Geo_add                           	configuration		00000050	        0000001F	1	0			This register returns the geographical address of the board. It is useful to know the slot position of the board inside the CompactPCI backplane.
*															
SNa                               	configuration		0000005C	        FFFFFFFF	1	0			This register returns the lower 32-bit of the board serial number.
*															
SNb                               	configuration		00000060	        FFFFFFFF	1	0			This register returns the bit 63 to 32 of the board serial number.
*															
SNc                               	configuration		00000064	        00000FFF	1	0			This register returns the upper 12-bit of the board serial number.
*															  
softwareReset                     	memory       	0	00000000	        00020000	1	1	      Reset the internal PCI bus ( this reset is valid during 82 uS after the PCI command)
*															
FPGA_reload_done                  	memory       	0	00000000	        00000003	1	0	        b0:  specify when '1' that the FRL FPGA realod is ongoing, '0' the FRL FPGA realod is done.
*															        b1:  specify when '1' that the FEROL FPAG realod is ongoing, '0' , the FEROL FPGA realod is done.
*															        The you launch the FPGA realod (for both FPGA) you should wait 1uS before you mopnitor these two bits. 
*															
JTAG_TDE                          	memory       	0	00008090	        FFFFFFFF	1	1		Write to this register the bit available to be shifted in a JTAG access. A bit is care for the shift when it is set to ‘1’. The bit available should start from the lower bit (bit 0 ) without hole. As soon as a bit is ‘0’ the bits upper will be ignored. This register should not be reloaded at each time if it doesn’t change.
*															
JTAG_TDI                          	memory       	0	00008094	        FFFFFFFF	1	1		This register loads TDI bits to be shifted in a JTAG access. A write access to this register will start a JTAG shift access. Other JTAG register should load before. 
*															
JTAG_TMS                          	memory       	0	00008098	        FFFFFFFF	1	1		This register loads the TMS bits to be shifted during a JTAG access. This register should not be reloaded at each time if it doesn’t change.
*															
JTAG_TDO                          	memory       	0	0000809C	        FFFFFFFF	1	0		This register return the TDO bit shifted during a JTAG access. When a JTAG access is initiated by JTAG_TDI function (offset 0x8094). The access to JTAG_TDO will generate a PCI retry if the software try to read it and the JTAG access is not finished.
*															
Memory                            	memory       	0	00200000	        FFFFFFFF	1	1		This offsets access the memory (shared by both FPGA) to read and write histogram, event descriptors…(see 5.6 for the memory map)
*																This access to memory is to offset 0x3FFFFF
*															
Descriptors_Stream0               	memory       	0	00700000	        77FFFFFF	1	1		This accesses the memory to read and write the descriptors of the Stream0 (2 64-bit words per event; see FRL page for details).
*															        You are allowed to write up to the offset 0x7FFFFF.
*															
Descriptors_Stream1               	memory       	0	00780000	        7FFFFFFF	1	1		This accesses the memory to read and write the descriptors of the Stream1 (2 64-bit words per event; see FRL page for details).
*															        You are allowed to write up to the offset 0xFFFFFF.