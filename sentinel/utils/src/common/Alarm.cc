// $Id: Alarm.cc,v 1.4 2008/11/13 15:10:24 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "sentinel/utils/Alarm.h"

sentinel::utils::Alarm::Alarm(const std::string & severity, xcept::Exception & e, xdaq::Application * owner)
{
	e_ = e;
	owner->qualifyException(severity,e_);
	owner_ = owner;
	e_.setProperty("args", "alarm");
}

sentinel::utils::Alarm::~Alarm()
{
}

void sentinel::utils::Alarm::setValue(const xdata::Serializable & s)  throw (xdata::exception::Exception)
{
}

int sentinel::utils::Alarm::equals(const xdata::Serializable & s) const
{
	return 0;
}

xdaq::Application *  sentinel::utils::Alarm::getOwnerApplication()
{
        return owner_;
}



std::string sentinel::utils::Alarm::type() const
{
	return "alarm";
}

std::string sentinel::utils::Alarm::toString () const throw (xdata::exception::Exception)
{
	return "";
}

void  sentinel::utils::Alarm::fromString (const std::string& value) throw (xdata::exception::Exception)
{
}


xcept::Exception & sentinel::utils::Alarm::getException()
{
	return e_;
}

