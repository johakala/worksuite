// $Id: NewsEvent.cc,v 1.2 2008/07/18 15:27:30 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "sentinel/utils/NewsEvent.h"

sentinel::utils::NewsEvent::NewsEvent(void* originator)
	: xdata::Event ("urn:sentinel-event:news", originator)
{

}

sentinel::utils::NewsEvent::NewsEvent(toolbox::Properties& article, void* originator)
	: xdata::Event ("urn:sentinel-event:news", originator), article_(article)
{
	
}
	
toolbox::Properties& sentinel::utils::NewsEvent::getArticle()
{
	return article_;
}
