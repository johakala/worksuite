// $Id: Application.cc,v 1.22 2008/11/27 14:40:16 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                  *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/Runtime.h"
#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 
#include "xdaq/ApplicationDescriptorImpl.h"

#include "xoap/domutils.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"
#include "xoap/Event.h"
#include <xercesc/util/XMLURL.hpp>
#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"

 
#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "sentinel/Sentinel.h"
#include "sentinel/spotlightocci/Application.h"
#include "sentinel/spotlightocci/ExceptionEvent.h"
#include "sentinel/spotlightocci/RevokeEvent.h"


#include "xdata/exdr/Serializer.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/Table.h"

#include "xdaq/XceptSerializer.h"

XDAQ_INSTANTIATOR_IMPL(sentinel::spotlightocci::Application);

sentinel::spotlightocci::Application::Application(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this),
	dispatcher_("urn:xdaq-workloop:spotlight", "waiting", 0.8),
	repository_(0)
{	
	s->getDescriptor()->setAttribute("icon", "/sentinel/spotlightocci/images/spotlight-occi-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/sentinel/spotlightocci/images/spotlight-occi-icon.png");
	s->getDescriptor()->setAttribute("service", "sentinelspotlight2g");

	subscribeGroup_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeGroup", &subscribeGroup_);

	jelFileName_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("jelFileName", &jelFileName_);

	// Activates work loop for dashboard asynchronous operations (SOAP messages)
	dispatcher_.addActionListener(this);
	(void) toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:spotlight", "waiting")->activate();


	// Set Oracle connection details
	databaseUser_ = "";
	databasePassword_ = "";
	databaseTnsName_ = "";
	oclFileName_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("oclFileName", &oclFileName_);
	this->getApplicationInfoSpace()->fireItemAvailable("databaseUser", &databaseUser_);
	this->getApplicationInfoSpace()->fireItemAvailable("databasePassword", &databasePassword_);
	this->getApplicationInfoSpace()->fireItemAvailable("databaseTnsName", &databaseTnsName_);

	scatterReadNum_ = 100;
	this->getApplicationInfoSpace()->fireItemAvailable("scatterReadNum", &scatterReadNum_);

	// Which flashlist tags to retrieve from the ws-eventing
	topic_ = "sentinel";
	this->getApplicationInfoSpace()->fireItemAvailable("topic", &topic_);

	scanPeriod_ = "PT10S";
	this->getApplicationInfoSpace()->fireItemAvailable("scanPeriod",&scanPeriod_);
	subscribeExpiration_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeExpiration",&subscribeExpiration_);
	dataInputSubscription_ = true;
	this->getApplicationInfoSpace()->fireItemAvailable("dataInputSubscription", &dataInputSubscription_);

	// bind SOAP interface for incoming 'notify' messages containing exceptions
	//
	b2in::nub::bind(this, &sentinel::spotlightocci::Application::onMessage );

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this,  &sentinel::spotlightocci::Application::Default, "Default");
	xgi::bind(this, &sentinel::spotlightocci::Application::statisticsTabPage, "statisticsTabPage");

	xgi::bind(this, &sentinel::spotlightocci::Application::rearm, "rearm");

	// Retrieves a complete exception. View takes two parameter
	// 'uniqueid' is the uuid of the exception to be retrieved
	// 'format' indicates the result format. The default is 'html', also 'xml' is supported.
	//
	xgi::bind(this, &sentinel::spotlightocci::Application::view, "view");

	// Clear all exceptions, cleanup and close files
	xgi::bind(this, &sentinel::spotlightocci::Application::reset, "reset");

	// DB operation
	//xgi::bind(this, &sentinel::spotlightocci::Application::files, "files");

	// Generate a test exception
	xgi::bind(this, &sentinel::spotlightocci::Application::generate, "generate");
	xgi::bind(this, &sentinel::spotlightocci::Application::inject, "inject");

	// List takes two parameters: 
	// 'timeBack' indicates how much bach in the the retrieve list should go
	// 'maxResults' indicates the maximum number of exceptions to be retrieved
	//
	xgi::bind(this, &sentinel::spotlightocci::Application::catalog, "catalog");

	// lastStoredEvents?start=<LAST STORE TIME>
	// retrieve all exceptions using storeTime field between <LAST STORE TIME> and now.
	// Return also the newest exception storeTime in the result set.
	// If start=EMPTY, return the newest exception in the database
	//
	xgi::bind(this, &sentinel::spotlightocci::Application::lastStoredEvents, "lastStoredEvents");
	xgi::bind(this, &sentinel::spotlightocci::Application::events, "events");
	xgi::bind(this, &sentinel::spotlightocci::Application::query, "query");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	exceptionsLostCounter_ = 0;
}

sentinel::spotlightocci::Application::~Application()
{

}

void sentinel::spotlightocci::Application::inject(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
                                
        try                     
        {
		std::string identifier = "toolbox::exception::Test";
		std::string notifier   = "http://lxcmd113.cern.ch:9920/urn:xdaq-application:lid=10";
		std::string severity   = "error";
		std::string className   = "xmas::sensor2g::Application";
		std::string tag   = "";
		std::string instance   = "";
		std::string unamespace   = "urn:jobcontrol:jid";
		std::string uvalue   = "34";
		std::string group =  this->getApplicationDescriptor()->getAttribute("group");

                cgicc::Cgicc cgi(in);
                cgicc::const_form_iterator fi = cgi.getElement("identifier");
                if (fi != cgi.getElements().end())
                {               
                        identifier = (*fi).getValue();
                }               
                                
                fi = cgi.getElement("notifier");                
                if (fi != cgi.getElements().end())
                {               
                        notifier = (*fi).getValue();
                }


                fi = cgi.getElement("severity");
                if (fi != cgi.getElements().end())
                {
                        severity = (*fi).getValue();
                }

                fi = cgi.getElement("class");
                if (fi != cgi.getElements().end())
                {
                        className = (*fi).getValue();
                }

                fi = cgi.getElement("tag");
                if (fi != cgi.getElements().end())
                {
                        tag = (*fi).getValue();
                }

                fi = cgi.getElement("instance");
                if (fi != cgi.getElements().end())
                {
                        instance = (*fi).getValue();
                }
                fi = cgi.getElement("unamespace");
                if (fi != cgi.getElements().end())
                {
                        unamespace = (*fi).getValue();
                }
                fi = cgi.getElement("uvalue");
                if (fi != cgi.getElements().end())
                {
                        uvalue = (*fi).getValue();
                }
		
  		xcept::Exception exception1 ( "toolbox::exception::TestLowLevel", "low level",  __FILE__, __LINE__, __FUNCTION__);
                xcept::Exception exception2(  "xoap::exception::TestMidLevel", "mid level",  __FILE__, __LINE__, __FUNCTION__, exception1);
                xcept::Exception exception3(  identifier, "this is an exception", __FILE__, __LINE__, __FUNCTION__, exception2);
                xcept::Exception exception4(  identifier, "this is an exception", __FILE__, __LINE__, __FUNCTION__, exception3);
                xcept::Exception exception5(  identifier, "this is an exception", __FILE__, __LINE__, __FUNCTION__, exception4);
                xcept::Exception exception6(  identifier, "this is an exception", __FILE__, __LINE__, __FUNCTION__, exception5);
                xcept::Exception exception7(  identifier, "this is an exception", __FILE__, __LINE__, __FUNCTION__, exception6);

                exception7.setProperty("notifier",notifier);
                exception7.setProperty("severity",severity);
                exception7.setProperty("urn:xdaq-application:class",className);
                exception7.setProperty("tag",tag);
                exception7.setProperty("urn:xdaq-application:instance",instance);
                exception7.setProperty(unamespace,uvalue);
		exception7.setProperty("urn:xdaq-application:group",group);

                repository_->store(exception7);

        }
	catch (sentinel::spotlightocci::exception::FailedToStore &se)
	{

                std::stringstream msg;
                msg << "Failed to process 'view' request, error: " << se.what();
                XCEPT_RETHROW (xgi::exception::Exception, msg.str(),se);
	}
        catch (std::exception &se)
        {
                std::stringstream msg;
                msg << "Failed to process 'view' request, error: " << se.what();
                XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}
}


void sentinel::spotlightocci::Application::generate(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	static int count = 0;

	if (repository_ == 0)
	{
		XCEPT_RAISE (xgi::exception::Exception, "Failed to generate test exceptions, repository is null");
	}

	try
	{
		for (size_t j = 0; j < 2; ++j)
		{
			toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
			for (size_t i = 0; i < 2000; ++i)
			{
				++count;

				std::stringstream msg;
				msg << "This is exception number " << count;

				xcept::Exception exception1 ( "toolbox::exception::TestLowLevel", "low level",  __FILE__, __LINE__, __FUNCTION__);
				xcept::Exception exception2(  "xoap::exception::TestMidLevel", "mid level",  __FILE__, __LINE__, __FUNCTION__, exception1);
				xcept::Exception exception3(  "xdaq::exception::TestHighLevel", msg.str(), __FILE__, __LINE__, __FUNCTION__, exception2); 
				exception3.setProperty("notifier","http://lxcmd113.cern.ch:9920/urn:xdaq-application:lid=10");	
				exception3.setProperty("severity","error");
				exception3.setProperty("urn:xdaq-application:class","xmas::sensor2g::Application");
				exception3.setProperty("tag","fu");
				exception3.setProperty("urn:xdaq-application:instance","34");

				repository_->store(exception3);


				/*xcept::Exception exception4 ( "toolbox::exception::TestHighevel", "high level",  __FILE__, __LINE__, __FUNCTION__);
				exception4.setProperty("urn:tracker-fed:instance","56");
				exception4.setProperty("urn:jobcontrol:jid","5600");
                                xcept::Exception exception5(  "xoap::exception::TestMidLevel", "mid level",  __FILE__, __LINE__, __FUNCTION__, exception4);
                                exception5.setProperty("notifier","http://lxcmd113.cern.ch:9920/urn:xdaq-application:lid=10");
                                exception5.setProperty("severity","alarm");
                                exception5.setProperty("class","xmas::las2g::Application");
				exception5.setProperty("tag","");
				exception5.setProperty("urn:xdaq-application:instance","56");
				exception5.setProperty("urn:tracker-fed:instance","56");
				exception5.setProperty("urn:jobcontrol:jid","5600");

                                repository_->store(exception5);*/

				//::sleep(2);
			}

			std::cout << std::endl;
			toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();

			std::cout << "Total: " << ((double)(stop-start)) << " sec" << std::endl;
			std::cout << "Rate: " << 1/(((double)(stop-start))/2000.0) << " Hz" << std::endl;
			
		}

		std::cout << "Finished generate." << std::endl;
	}
	catch (sentinel::spotlightocci::exception::Exception& e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		XCEPT_RETHROW (xgi::exception::Exception, "Test failed", e);
	}
}

//
// Infoapace listener
//
void sentinel::spotlightocci::Application::actionPerformed( xdata::Event& event) 
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		if ( oclFileName_.toString() != "" ) // for backward compatibility if paramater not specified
		{
			// makes use of the credential as specified in the OCL file for the current default zone (-z zonename or XDAQ_ZONE or default)
			this->loadOCL(oclFileName_.toString());
		}

		b2inEventingProxy_ = new b2in::utils::ServiceProxy(this,"b2in-eventing",subscribeGroup_.toString(),this);

		try
		{
			repository_ = new sentinel::spotlightocci::Repository(this,
				databaseUser_.toString(), databasePassword_.toString(), databaseTnsName_.toString(), scatterReadNum_); // open for current write and read usage
		}
		catch( sentinel::spotlightocci::exception::Exception & e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			return;
		}

		try
		{
			if (!toolbox::task::getTimerFactory()->hasTimer("spotlight"))
			{
				(void) toolbox::task::getTimerFactory()->createTimer("spotlight");
			}
			toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer("spotlight");
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();

			// submit scan task
			if ( dataInputSubscription_ )
			{
				toolbox::TimeInterval interval2;
				interval2.fromString(scanPeriod_);
				LOG4CPLUS_INFO (this->getApplicationLogger(), "Schedule discovery timer for " << interval2.toString());
				timer->scheduleAtFixedRate( start, this, interval2, 0, "discovery-staging" );
			}
		}
		catch (toolbox::task::exception::InvalidListener& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::InvalidSubmission& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::NotActive& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::exception::Exception& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		//
		if ( jelFileName_.toString() != "" ) // for backward compatibility if paramater not specified
		{
			this->loadJEL(jelFileName_.toString());
		}

	}
	else
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}
void sentinel::spotlightocci::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist)
        throw (b2in::nub::exception::Exception)
{
	std::string name = plist.getProperty("urn:sentinel-event:name");
	if( name != "revoke" && name != "notify" )
	{
		// unexpected message
		if(msg != 0) msg->release();
		return;
	}

	//TODO error handling
 	xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) msg->getDataLocation(), msg->getDataSize());         
	xcept::Exception* newException = new xcept::Exception();
	xdaq::XceptSerializer::importFrom (&inBuffer, *newException);

	toolbox::Event * event;
	if ( name == "revoke")
	{
		event = new  sentinel::spotlightocci::RevokeEvent(newException,"");
	}
	else /* if (name == "notify" ) */
	{
		event = new  sentinel::spotlightocci::ExceptionEvent(newException,"");
	}
	
	 // process exception in a separate thread, to release the HTTP emebedded server
	
	if(msg != 0)  msg->release();

	toolbox::task::EventReference e(event);
	try
	{
        	dispatcher_.fireEvent (e);
	}
	catch(toolbox::task::exception::Overflow & e )
	{
        	exceptionsLostCounter_++;
        	std::stringstream msg;
        	msg << "Exceptions lost: " << exceptionsLostCounter_  << ", " << e.what() ;
        	LOG4CPLUS_FATAL  (this->getApplicationLogger(), msg.str());
       	
	}
	catch(toolbox::task::exception::OverThreshold & e )
	{
        	// message is lost, keep counting
        	exceptionsLostCounter_++;
	}
	catch(toolbox::task::exception::InternalError & e )
	{
        	// dead band ignore
        	exceptionsLostCounter_++;
        	std::stringstream msg;
        	msg << "Exceptions lost: " << exceptionsLostCounter_  << ", " << e.what() ;
        	LOG4CPLUS_FATAL  (this->getApplicationLogger(), msg.str());
	}                               
}

void sentinel::spotlightocci::Application::asynchronousExceptionNotification(xcept::Exception& e)
{
	//TODO
}

void sentinel::spotlightocci::Application::refreshSubscriptionsToEventing()
	throw (sentinel::exception::Exception)
{
        if (subscriptions_.empty())
        {
                LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Prepare subscription records");
                const xdaq::Network * network = 0;
                std::string networkName =  this->getApplicationDescriptor()->getAttribute("network");
                try
                {
                        network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
                }
                catch (xdaq::exception::NoNetwork& nn)
                {
                        std::stringstream msg;
                        msg << "Failed to access b2in network " << networkName << ", not configured";
                        XCEPT_RETHROW (sentinel::exception::Exception, msg.str(), nn);
                }
                pt::Address::Reference localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());


                std::set<std::string> topics = toolbox::parseTokenSet ( topic_.toString(), "," );
                std::set<std::string>::iterator i;
                for (i = topics.begin(); i != topics.end(); ++i)
                {
                        if ( subscriptions_.find(*i) == subscriptions_.end())
                        {
                                xdata::Properties plist;
                                toolbox::net::UUID identifier;
                                plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching

                                plist.setProperty("urn:b2in-eventing:action", "subscribe");
                                plist.setProperty("urn:b2in-eventing:id", identifier.toString());
                                plist.setProperty("urn:b2in-eventing:topic", (*i)); // Subscribe to collected data
                                plist.setProperty("urn:b2in-eventing:subscriberurl", localAddress->toString());
                                plist.setProperty("urn:b2in-eventing:subscriberservice", "sentinelspotlight2g");
                                plist.setProperty("urn:b2in-eventing:expires", subscribeExpiration_.toString("xs:duration"));
                                subscriptions_[(*i)] = plist;
                        }
                }
        }

        LOG4CPLUS_DEBUG (this->getApplicationLogger(), "subscribe/Renew");
        b2in::utils::MessengerCache * messengerCache = 0;
        try
        {
                messengerCache = b2inEventingProxy_->getMessengerCache();
        }
        catch(b2in::utils::exception::Exception & e)
        {
                        XCEPT_RETHROW (sentinel::exception::Exception, "cannot access messenger cache", e);
        }

	// This lock is available all the time excepted when an asynchronous send will fail and a cache invalidate is performed
        // Therefore there is no loss of efficiency.
        //

        std::list<std::string> destinations = messengerCache->getDestinations();

        for (std::list<std::string>::iterator j = destinations.begin(); j != destinations.end(); j++ )
        {
                //
                // Subscribe to OR Renew all existing subscriptions
                //
                std::map<std::string, xdata::Properties>::iterator i;
                for ( i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
                {
                        try
                        {
                                // plist is already prepared for a subscribe/resubscribe message
                                //
                                LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Subscribe to topic " << (*i).first << " on url " << (*j));
                                messengerCache->send((*j),0,(*i).second);
                        }
                        catch (b2in::nub::exception::InternalError & e)
                        {
                                std::stringstream msg;
                                msg << "failed to send subscription for topic (internal error) " << (*i).first << " to url " << (*j);

                                LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());

                                XCEPT_DECLARE_NESTED(sentinel::exception::Exception, ex , msg.str(), e);
                                this->notifyQualified("fatal",ex);
                                return;
                        }
                        catch ( b2in::nub::exception::QueueFull & e )
                        {
                                std::stringstream msg;
                                msg << "failed to send subscription for topic (queue full) " << (*i).first << " to url " << (*j);

                                LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());

                                XCEPT_DECLARE_NESTED(sentinel::exception::Exception, ex , msg.str(), e);
                                this->notifyQualified("error",ex);
                                return;
                        }
                        catch ( b2in::nub::exception::OverThreshold & e)
                        {
                                // ignore just count to avoid verbosity                                
                                std::stringstream msg;
                                msg << "failed to send subscription for topic (over threshold) " << (*i).first << " to url " << (*j);
                                LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
                                return;
                        }
                }
                LOG4CPLUS_DEBUG(this->getApplicationLogger(), "done with subscription");
        }
}

void sentinel::spotlightocci::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	std::string name =  e.getTimerTask()->name;

	if (name == "discovery-staging")
	{
		try
		{
			b2inEventingProxy_->scan();
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}


		try
		{
			this->refreshSubscriptionsToEventing();
		}
		catch (sentinel::exception::Exception& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

	}
}

//
// XGI SUpport
//
void sentinel::spotlightocci::Application::view(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	if (!repository_)
	{
		// If we are in maintenance phase, raise an exception
		XCEPT_RAISE (xgi::exception::Exception, "Repository temporarily unavailable.");
	}
	
	std::string uniqueid;
	std::string datetime = "";
	std::string format = "json"; // by default output json format
	std::string dbname = ""; // by default operate on the main database
	
	try
        {
                cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("uniqueid");
		if (fi != cgi.getElements().end())
		{
			uniqueid = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE (xgi::exception::Exception, "Missing 'uniqueid' parameter");
		}

		fi = cgi.getElement("datetime");		
		if (fi != cgi.getElements().end())
                {
                        datetime = (*fi).getValue();
                }

		
		fi = cgi.getElement("fmt");		
		if (fi != cgi.getElements().end())
		{
			format = (*fi).getValue();
		}
		
		fi = cgi.getElement("dbname");		
		if (fi != cgi.getElements().end())
		{
			dbname = (*fi).getValue();
		}
	}
	catch (toolbox::exception::Exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW(xgi::exception::Exception, "invalid time",e);

	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'view' request, error: " << se.what();
		XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}
		
	if (format == "json")
	{		
		try
		{
			repository_->retrieve(uniqueid, datetime, format, out);
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
		}
		catch (sentinel::spotlightocci::exception::NotFound &e)
		{
			std::stringstream msg;
			msg << "Failed to retrieve exception '" << uniqueid << "'";
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
			XCEPT_RETHROW (xgi::exception::Exception, msg.str(), e);
		}
	}	
}

void sentinel::spotlightocci::Application::catalog(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::string format = "json"; // by default output json format
	std::string dbname = ""; // by default, operate on the main database

	// By default, retrieve errors from the last 1 hour
	toolbox::TimeInterval window (3600,0);
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday() - window;
	toolbox::TimeVal end = toolbox::TimeVal::gettimeofday();

	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi;
		
		fi = cgi.getElement("start");
		if (fi != cgi.getElements().end())
		{
			start.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		fi = cgi.getElement("end");
		if (fi != cgi.getElements().end())
		{
			end.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		fi = cgi.getElement("fmt");		
		if (fi != cgi.getElements().end())
		{
			format = (*fi).getValue();
		}
		
	}
	catch (toolbox::exception::Exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW(xgi::exception::Exception, "invalid time",e);

	}
	catch (const std::exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
	
	try
	{
		repository_->catalog (out, start, end, format);		
	}
	catch (sentinel::spotlightocci::exception::NotFound& e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to retrieve data from repository", e);
	}
}

void sentinel::spotlightocci::Application::lastStoredEvents(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{

	std::string format = "json"; // by default output json format
	std::string dbname = ""; // by default, operate on the main database

	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();

	out->getHTTPResponseHeader().addHeader("Expires", "0");
	out->getHTTPResponseHeader().addHeader("Cache-Control",
					 "no-store, no-cache, must-revalidate, max-age=0");
	out->getHTTPResponseHeader().addHeader("Cache-Control",
					 "post-check=0, pre-check=0");
	out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");

	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi;
		
		fi = cgi.getElement("start");
		if (fi != cgi.getElements().end())
		{
			start.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		fi = cgi.getElement("fmt");		
		if (fi != cgi.getElements().end())
		{
			format = (*fi).getValue();
		}
		
	}
	catch (toolbox::exception::Exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW(xgi::exception::Exception, "invalid time",e);

	}
	catch (const std::exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
	
	try
	{
		// This function will return based on storeTime everything that is younger than 'start'
		// If there is nothing, return an empty data set, but always return the youngest store time
		//
		repository_->lastStoredEvents (out, start, format);		
	}
	catch (sentinel::spotlightocci::exception::NotFound& e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to retrieve data from repository", e);
	}
}
//--
void sentinel::spotlightocci::Application::events(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::string format = "json"; // by default output json format
	std::string dbname = ""; // by default, operate on the main database

	// By default, retrieve errors from the last 1 hour
	toolbox::TimeInterval window (3600,0);
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday() - window;
	toolbox::TimeVal end = toolbox::TimeVal::gettimeofday();

	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi;
		
		fi = cgi.getElement("start");
		if (fi != cgi.getElements().end())
		{
			start.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		fi = cgi.getElement("end");
		if (fi != cgi.getElements().end())
		{
			end.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		fi = cgi.getElement("fmt");		
		if (fi != cgi.getElements().end())
		{
			format = (*fi).getValue();
		}
		
	}
	catch (toolbox::exception::Exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW(xgi::exception::Exception, "invalid time",e);

	}
	catch (const std::exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
	
	try
	{
		repository_->events (out, start, end, format);		
	}
	catch (sentinel::spotlightocci::exception::NotFound& e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to retrieve data from repository", e);
	}
}

void sentinel::spotlightocci::Application::query(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::cout << "Application::query" << std::endl;
        std::string format = "json"; // by default output json format
        std::string dbname = ""; // by default, operate on the main database
	std::string statement = "";

        try
        {
                cgicc::Cgicc cgi(in);
                cgicc::const_form_iterator fi;

                fi = cgi.getElement("statement");
                if (fi != cgi.getElements().end())
                {
                	format = cgicc::form_urldecode((*fi).getValue()); // need to decode for coldspot

			LOG4CPLUS_DEBUG(this->getApplicationLogger(),"query is:" << format);
			//std::cout << "Statement received: " << format << std::endl;
		}
        }
        catch (toolbox::exception::Exception & e)
        {
                out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW(xgi::exception::Exception, "invalid time",e);

        }
        catch (const std::exception & e)
        {
                out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RAISE(xgi::exception::Exception, e.what());
        }

        try
        {
                repository_->query (out, format);
        }
        catch (sentinel::spotlightocci::exception::NotFound& e)
        {
                out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW (xgi::exception::Exception, "Failed to retrieve data from repository", e);
        }
}


//--
void sentinel::spotlightocci::Application::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	this->mainPage(in,out);
}



/*
void sentinel::spotlightocci::Application::files(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	std::string fmt = "json"; // default
	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("fmt");
		if (fi != cgi.getElements().end())
		{
			fmt = (*fi).getValue();
		}
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'databases' request, error: " << se.what();
		XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}
		
	std::vector<std::string> files = repository_->getFiles();
	std::vector<std::string>::iterator i = files.begin();
	*out << "[";
	do
	{
		*out << "\"" << (*i) << "\"";
		++i;
		if (i != files.end())
		{
			*out << ",";
		}
	} while (i != files.end());
	*out << "]";
}
*/


void sentinel::spotlightocci::Application::rearm(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	std::string exception = ""; // default
	std::vector<cgicc::FormEntry> exceptions; // list of exceptions to be rearmed
	std::string user = "unknown";
	 
	try
	{
		cgicc::Cgicc cgi(in);
		
		const cgicc::CgiEnvironment& env = cgi.getEnvironment();
		user = env.getRemoteUser();
				
		cgi.getElement("exception", exceptions);
				
		if(exceptions.empty()) 
		{
		     XCEPT_RAISE (xgi::exception::Exception,"Missing exception(uuid) for rearm");
		}
		
		/* cgicc::const_form_iterator fi = cgi.getElement("exception");
		if (fi != cgi.getElements().end())
		{
			exception = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE (xgi::exception::Exception,"Missing exception(uuid) for rearm");
		}
		*/
		
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process rearm request, error: " << se.what();
		XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}

	try
	{
		if (repository_)
		{
			for(std::string::size_type i = 0; i < exceptions.size(); ++i) 
			{
				exception = exceptions[i].getValue();
				repository_->rearm(exception, user); 
			}			
		}
		else
		{
			XCEPT_RAISE (xgi::exception::Exception,"Failed to rearm exception, repository not initialized");
		}
	}
	catch (sentinel::spotlightocci::exception::FailedToStore& st)
	{
		std::stringstream msg;
		msg << "Failed to rearm exception '" << exception << "'";
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(),st);
	}
}

void sentinel::spotlightocci::Application::reset(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	LOG4CPLUS_INFO (this->getApplicationLogger(), "Performing repository reset");
	repository_->reset();
	LOG4CPLUS_INFO (this->getApplicationLogger(), "Reset finished");	

	this->Default(in, out);
}

// Load JEL
void sentinel::spotlightocci::Application::loadOCL(const std::string & fname)
{

	std::vector < std::string > files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(fname);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand OCL filename from '" << fname << "', ";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		return;
	}

	std::string zone = this->getApplicationContext()->getDefaultZoneName();
 	try
        {
		//LOG4CPLUS_INFO (this->getApplicationLogger(), "Load OCL from '" << files[0]  << "'");
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML( files[0] );
		DOMNodeList* list = doc->getElementsByTagNameNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2016/oraclecl"),xoap::XStr("XaasZone") );

		std::string defautZone = this->getApplicationContext()->getDefaultZoneName();

		for (XMLSize_t j = 0; j < list->getLength(); j++)
		{
			DOMNode* zoneNode = list->item(j);
			std::string zoneName  = xoap::getNodeAttribute (zoneNode, "zone");   // new network name

			if (defautZone == zoneName )
			{
					DOMNodeList* credentials = zoneNode->getChildNodes();
					for (unsigned int j = 0; j < credentials->getLength(); j++ )
					{
							DOMNode* c = credentials->item(j);
							if  (( c->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(c->getLocalName()) == "databaseUser" ))
							{
								databaseUser_ = xoap::XMLCh2String(c->getFirstChild()->getNodeValue());

							}
							else if  (( c->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(c->getLocalName()) == "databasePassword" ))
							{
								databasePassword_ = xoap::XMLCh2String(c->getFirstChild()->getNodeValue());

							}
							else if  (( c->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(c->getLocalName()) == "databaseTnsName" ))
							{
								databaseTnsName_ = xoap::XMLCh2String(c->getFirstChild()->getNodeValue());

							}
					}
					//std::cout << "found zone: " << zoneName << " in OCL"  << std::endl;
					//std::cout << "databaseUser_: " << databaseUser_.toString()  << std::endl;
					//std::cout << "databasePassword_: " << databasePassword_.toString()  << std::endl;
					//std::cout << "databaseTnsName_: " << databaseTnsName_.toString()  << std::endl;
					break;
			}


		}
		doc->release();
		//LOG4CPLUS_INFO (this->getApplicationLogger(), "Loaded OCL from '" << fname << "'");
	}
	catch (xoap::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to load OCL file from '" << fname << "'";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));

	}

}


// Load JEL
void sentinel::spotlightocci::Application::loadJEL(const std::string & fname)
{
	std::vector < std::string > files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(fname);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand filter filename from '" << fname << "', ";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		return;
	}

 	try
        {
		LOG4CPLUS_INFO (this->getApplicationLogger(), "Load JEL from '" << files[0]  << "'");
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML( files[0] );
		DOMNodeList* list = doc->getElementsByTagNameNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2010/spotlight"),xoap::XStr("rule") );

		for (XMLSize_t j = 0; j < list->getLength(); j++)
		{
			//std::cout << "set rule:" << std::endl;
			DOMNode* node = list->item(j);
			DOMNamedNodeMap* attributes = node->getAttributes();
			xdata::Properties p;
			for (unsigned int i = 0; i < attributes->getLength(); i++ )
			{
				std::string valueStr  = xoap::XMLCh2String( attributes->item(i)->getNodeValue() );
				std::string nameStr  = xoap::XMLCh2String( attributes->item(i)->getNodeName() );
				p.setProperty(nameStr,valueStr);
				//std::cout << "attribute" << nameStr << " regexp " << valueStr << std::endl;
			}
			jel_.push_back(p);
		}
		doc->release();
		LOG4CPLUS_INFO (this->getApplicationLogger(), "Loaded JEL from '" << fname << "'");
	}
	catch (xoap::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to load configuration file from '" << fname << "'";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		
	}

}

// apply Junk Exception List
bool sentinel::spotlightocci::Application::applyJEL( xcept::Exception & e)
{
	
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "check JEL rules");
	size_t labelCounter = 0;
	std::list<xdata::Properties>::iterator i;
	std::string name = e.name();
        for (i = jel_.begin(); i != jel_.end(); ++i)
        { 
		std::stringstream label;
		label << "r" << labelCounter;	
	
		if ( this->match(e,(*i)) )
		{
			// junk exception counter
			if ( jec_.find(name) != jec_.end() )
			{
				if ( jec_[name].find(label.str()) != jec_[name].end())
				{
					jec_[name][label.str()] += 1;
				}
				else
				{
					jec_[name][label.str()] = 1;
				}
			}
			else
			{
				jec_[name][label.str()] = 1;

			}
			return true;
		}
		labelCounter++;
	}

	return false;
}

std::string sentinel::spotlightocci::Application::mapToAttributeName(const std::string & name)
{
	if ((name  == "class") || ( name == "instance") || ( name == "lid") || ( name == "context") 
		|| ( name == "group") || ( name == "service") || ( name == "zone") )
        {
		std::string v = "urn:xdaq-application:";
		v += name;
                return  v;
        }
	return name;
}

bool sentinel::spotlightocci::Application::match 
(
	xcept::Exception & e,
        std::map<std::string, std::string>& filter
)
{
        // Algorithm requires that all filter expressions match!
        //

	//std::cout << "apply rule" << std::endl;
        std::map<std::string, std::string>::iterator i;
        for (i = filter.begin(); i != filter.end(); ++i)
        {       
                try
                {
			//--

			std::string name = this->mapToAttributeName((*i).first);
			//--
			// extract property from exception
			std::string value = e.getProperty(name);
			//std::cout << "check attribute:" << name << " regex:" << (*i).second << " on value: " << value;
                        if ( ! toolbox::regx_match_nocase(value, (*i).second ) )
                        {
				//std::cout << " -> does not match" << std::endl;
                                return false;
                        }
			
                }
                catch (xdata::exception::Exception& e)
                {
                        // doesn't match the column name
                        return false;
                }
        }
	//std::cout << " -> it matches!" << std::endl;
        return true;
}       

void sentinel::spotlightocci::Application::actionPerformed( toolbox::Event& event) 
{
 /* xcept::Exception* newException = new xcept::Exception();
  sentinel::utils::Serializer::importFrom (command, *newException);
  toolbox::task::EventReference e(new sentinel::spotlight2g::ExceptionEvent(newException,""));
*/

	if (event.type() == "urn:sentinel-spotlight:ExceptionEvent") 
	{
		// Synchronous store operation
		//
		sentinel::spotlightocci::ExceptionEvent& e = dynamic_cast<sentinel::spotlightocci::ExceptionEvent&>(event);

		// Apply antispam filter
		if ( jel_.size() != 0  && this->applyJEL(*(e.getException()) ))
		{
			// discard exception 
			//std::cout << "Discard junk exception" << std::endl;
			return;
		}	
		
		// If the DB has not yet been opened or created, open it
		try
		{
			if (repository_)
			{
				repository_->store(*(e.getException())); // After use, event and exception go away automatically
			}
			else
			{
				// Repository not initialized
				LOG4CPLUS_FATAL (this->getApplicationLogger(), "Failed to store excpetion, repository not initialized");
			}
		}
		catch (sentinel::spotlightocci::exception::FailedToStore& st)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(st));
		}
	}
	else if (event.type() == "urn:sentinel-spotlight:RevokeEvent") 
	{
		// Synchronous store operation
		//
		sentinel::spotlightocci::RevokeEvent& e = dynamic_cast<sentinel::spotlightocci::RevokeEvent&>(event);
		// If the DB has not yet been opened or created, open it
		try
		{
			if (repository_)
			{
				repository_->revoke(*(e.getException())); // After use, event and exception go away automatically
			}
			else
			{
				// Repository not initialized
				LOG4CPLUS_FATAL (this->getApplicationLogger(), "Failed to revoke exception, repository not initialized");
			}
		}
		catch (sentinel::spotlightocci::exception::FailedToStore& st)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(st));
		}

	}
	
}


void sentinel::spotlightocci::Application::mainPage (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	// Begin of tabs
        *out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
  
  	// Tab 1      
        *out << "<div class=\"xdaq-tab\" title=\"Hotspot\">" << std::endl;
        this->HotspotTabPage(out);
        *out << "</div>";
        
        // Tab 2
        *out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;

        // Make statistics an optional callback
        //this->StatisticsTabPage(out);

        std::stringstream baseurl;
		baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();
        *out << "<button id=\"loadStatistics\" data-url=\"" << baseurl.str() << "/statisticsTabPage\">Load Statistics</button> Warning! This may take a while <img src=\"/hyperdaq/images/framework/xdaq-ajax-loader.gif\" class=\"xdaq-ajax-loader\" />";
        *out << "<br /><br /><hr /><br />" << std::endl;
        *out << "<div id=\"statisticsContent\"></div>" << std::endl;
		*out << "<script type=\"text/javascript\" src=\"/sentinel/spotlightocci/html/js/xdaq-sentinel-spotlightocci.js\"></script>" 					<< std::endl;

        *out << "</div>";

        // Tab 3
        *out << "<div class=\"xdaq-tab\" title=\"JEL\">" << std::endl;
        this->JELTabPage(out);
        *out << "</div>";

        *out << "</div>"; // end of tab pane
}

void sentinel::spotlightocci::Application::statisticsTabPage (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	if (repository_ == 0)
	{
		// Instead of raising, display an error message in framework - This is only triggered by a web page access anyway
		//XCEPT_RAISE(xgi::exception::Exception, "Repository unavailable.");
		XCEPT_DECLARE(xgi::exception::Exception, e, "Repository unavailable");
		*out << xcept::htmlformat_exception_history(e) << std::endl;
		return;
	}
	/*! Output summary page with the following information
	 
	 State of database file (opened/closed)
	 Path and filename to database
	 Last exception stored
	 Number of exceptions stored in file
	 average time to store an exception
	 theoretical average rate to store exceptions
	 average time to retrieve catalog
	 theoretical averate rate to retrieve catalog
	 average time to retrieve a single exception by uuid
	 theoretical averate rate to retrieve single exceptions by uuid
	 */

	*out << cgicc::table().set("class", "xdaq-table") << std::endl;

	// Output the header line with the column names
	//
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Latest Exception") << std::endl;
	*out << cgicc::th("Oldest Exception") << std::endl;
	*out << cgicc::th("# Exceptions") << std::endl;
	*out << cgicc::th("Size") << std::endl;
	*out << cgicc::th("Read Exception Time") << std::endl;
	*out << cgicc::th("Read Catalog Time") << std::endl;
	*out << cgicc::th("Write Exception Tme") << std::endl;
	*out << cgicc::th("Read Exception Rate") << std::endl;
	*out << cgicc::th("Read Catalog Rate") << std::endl;
	*out << cgicc::th("Write Exception Rate") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	std::list < toolbox::Properties > plist = repository_->getOpenArchivesInfo();
	for (std::list<toolbox::Properties>::iterator i = plist.begin(); i != plist.end(); i++)
	{
		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*i).getProperty("latest")) << std::endl;
		*out << cgicc::td((*i).getProperty("oldest")) << std::endl;
		*out << cgicc::td((*i).getProperty("count")) << std::endl;
		*out << cgicc::td((*i).getProperty("size")) << std::endl;
		*out << cgicc::td((*i).getProperty("readexceptiontime")) << std::endl;
		*out << cgicc::td((*i).getProperty("readcatalogtime")) << std::endl;
		*out << cgicc::td((*i).getProperty("writeexceptiontime")) << std::endl;
		*out << cgicc::td((*i).getProperty("readexceptionrate")) << std::endl;
		*out << cgicc::td((*i).getProperty("readcatalograte")) << std::endl;
		*out << cgicc::td((*i).getProperty("writeexceptionrate")) << std::endl;
		*out << cgicc::tr() << std::endl;

	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << cgicc::br() << std::endl;

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}

void sentinel::spotlightocci::Application::JELTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;

	// Output the header line with the column names
	//
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Name") << std::endl;
	*out << cgicc::th("Matching Rules") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	for (std::map<std::string, std::map<std::string, size_t> >::iterator i = jec_.begin(); i != jec_.end(); i++)
	{
		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*i).first) << std::endl;
		std::stringstream counters;
		for (std::map<std::string, size_t>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			counters << (*j).first << "(" << (*j).second << ") ";
		}
		*out << cgicc::td(counters.str()) << std::endl;
		*out << cgicc::tr() << std::endl;

	}
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

}

void sentinel::spotlightocci::Application::HotspotTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string zone = this->getApplicationContext()->getDefaultZoneName();

	std::stringstream to;
	to << this->getApplicationDescriptor()->getContextDescriptor()->getURL();
	//std::string url = toolbox::escape(to.str());
	std::string url = cgicc::form_urlencode(to.str());

	std::string model = cgicc::form_urlencode("https://xdaq.web.cern.ch/xdaq/setup/13/" + zone  + "/defaultmodel.xml");
//      *out << "<a href=\"https://xdaq.web.cern.ch/xdaq/xmas/13/hotspot/hotspot.swf?confPath=" << url << "&autoStart=true\" target=\"_blank\">Hotspot Flex Interface Test</a>";
	*out << "<a href=\"https://xdaq.web.cern.ch/xdaq/xmas/13/hotspot/hotspot.swf?spotlighturl=" << url << "&sysmodelurl=" << model << "\" target=\"_blank\"><button><br/><img src=\"/sentinel/images/hotspot-icon.png\" style=\"height: 80px;\"><br />Hotspot Flex Interface<br/><br/></button></a>";
	*out << " <a href=\"https://xdaq.web.cern.ch/xdaq/xmas/13/coldspot/coldspot.swf?spotlighturl=" << url << "&sysmodelurl=" << model << "\" target=\"_blank\"><button><br/><img src=\"/sentinel/images/coldspot-icon.png\" style=\"height: 80px;\"><br />Coldspot Flex Interface<br/><br/></button></a>";
}
