// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "sentinel/spotlightocci/RevokeEvent.h"
#include <string>
#include <sstream>
			
sentinel::spotlightocci::RevokeEvent::RevokeEvent( xcept::Exception* ex , const std::string & name):
	  toolbox::Event("urn:sentinel-spotlight:RevokeEvent", 0), exception_(ex), name_(name)
{
	//	std::cout << "CTOR of xoap::Event: " << name_ << std::endl;
}

sentinel::spotlightocci::RevokeEvent::~RevokeEvent()
{
	delete exception_;
	//std::cout << "DTOR of xoap::Event: " << name_ << std::endl;
}


std::string sentinel::spotlightocci::RevokeEvent::name()
{
	return name_;
}

xcept::Exception* sentinel::spotlightocci::RevokeEvent::getException()
{
	return exception_;
}
