function xdaqWindowPostLoad()
{
	$("#loadStatistics").on("click", function() {
		var url = $(this).attr("data-url");
		
		$(".xdaq-ajax-loader").addClass("xdaq-ajax-loading");
		
		var options = {
			url: url,
			type: "GET",
			error: function (xhr, textStatus, errorThrown) {
				$(".xdaq-ajax-loader").removeClass("xdaq-ajax-loading");
				console.error(xhr.status);
				console.error(errorThrown);
			}
		};
		
		xdaqAJAX(options, function (data, textStatus, xhr) {		
			$("#statisticsContent").html(xhr.responseText);
			$(".xdaq-ajax-loader").removeClass("xdaq-ajax-loading");
		});
	});
}