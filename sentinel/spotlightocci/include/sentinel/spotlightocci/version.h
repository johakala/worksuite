// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _sentinel_spotlightocci_version_h_
#define _sentinel_spotlightocci_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define SENTINELSPOTLIGHTOCCI_VERSION_MAJOR 3
#define SENTINELSPOTLIGHTOCCI_VERSION_MINOR 4
#define SENTINELSPOTLIGHTOCCI_VERSION_PATCH 0
// If any previous versions available E.g. #define SENTINELSPOTLIGHTOCCI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define SENTINELSPOTLIGHTOCCI_PREVIOUS_VERSIONS "3.0.3,3.0.4,3.0.5,3.1.0,3.1.1,3.2.0,3.3.0,3.3.1,3.3.2"


//
// Template macros
//
#define SENTINELSPOTLIGHTOCCI_VERSION_CODE PACKAGE_VERSION_CODE(SENTINELSPOTLIGHTOCCI_VERSION_MAJOR,SENTINELSPOTLIGHTOCCI_VERSION_MINOR,SENTINELSPOTLIGHTOCCI_VERSION_PATCH)
#ifndef SENTINELSPOTLIGHTOCCI_PREVIOUS_VERSIONS
#define SENTINELSPOTLIGHTOCCI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(SENTINELSPOTLIGHTOCCI_VERSION_MAJOR,SENTINELSPOTLIGHTOCCI_VERSION_MINOR,SENTINELSPOTLIGHTOCCI_VERSION_PATCH)
#else 
#define SENTINELSPOTLIGHTOCCI_FULL_VERSION_LIST  SENTINELSPOTLIGHTOCCI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(SENTINELSPOTLIGHTOCCI_VERSION_MAJOR,SENTINELSPOTLIGHTOCCI_VERSION_MINOR,SENTINELSPOTLIGHTOCCI_VERSION_PATCH)
#endif 

namespace sentinelspotlightocci
{
	const std::string package  =  "sentinelspotlightocci";
	const std::string versions =  SENTINELSPOTLIGHTOCCI_FULL_VERSION_LIST;
	const std::string summary = "Server for exceptions and alarms";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
