// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/Runtime.h"
#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 
#include "xdaq/ApplicationDescriptorImpl.h"

#include "xoap/domutils.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"
#include "xoap/Event.h"
#include <xercesc/util/XMLURL.hpp>
#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "sentinel/Sentinel.h"
#include "sentinel/arc/Application.h"

#include "xdata/exdr/Serializer.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/Table.h"

#include "xdaq/XceptSerializer.h"

#include "sentinel/arc/utils/Node.h"

#include "sentinel/utils/Alarm.h"

#include "sentinel/arc/exception/Exception.h"

XDAQ_INSTANTIATOR_IMPL (sentinel::arc::Application);

sentinel::arc::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
	: xdaq::Application(s) //, repositoryLock_(toolbox::BSem::FULL, true)
{
	s->getDescriptor()->setAttribute("icon", "/sentinel/arc/images/sentinel-arc-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/sentinel/arc/images/sentinel-arc-icon.png");
	//this->getApplicationDescriptor()->setAttribute("service", "sentinelspotlight2g");
	s->getDescriptor()->setAttribute("service", "sentinelarc");

	this->modelURL_ = "";
	this->severitiesURL_ = "";
	this->scatterReadNum_ = 100;

	maxInitExceptionNum_ = 5;
	flashbackInitWindow_ = 60; // default is 24h

	/* this->adminMode_ = false; */
	this->oclFileName_ = "";
	this->expireInterval_ = "PT1H";

	this->getApplicationInfoSpace()->fireItemAvailable("oclFileName", &oclFileName_);
	this->getApplicationInfoSpace()->fireItemAvailable("modelURL", &modelURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("severitiesURL", &severitiesURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("scatterReadNum", &scatterReadNum_);
	this->getApplicationInfoSpace()->fireItemAvailable("databases", &databases_);
	this->getApplicationInfoSpace()->fireItemAvailable("expireInterval", &expireInterval_);
	this->getApplicationInfoSpace()->fireItemAvailable("maxInitExceptionNum", &maxInitExceptionNum_);
	this->getApplicationInfoSpace()->fireItemAvailable("flashbackInitWindow", &flashbackInitWindow_);

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	/*
	if (adminMode_)
	{
		hyperdaq_ = new sentinel::arc::DevHyperDAQ(this);
	}
	else
	{
		hyperdaq_ = new sentinel::arc::HyperDAQ(this);
	}
	*/

	//
	// simple restful replace old style hyperdaq pages, supports hotspot 5
	//
	//RESTful
	xgi::bind(this, &sentinel::arc::Application::getNode, "getNode");
	xgi::bind(this, &sentinel::arc::Application::getException, "getException");
	xgi::bind(this, &sentinel::arc::Application::getExceptionList, "getExceptionList");
	xgi::bind(this, &sentinel::arc::Application::getModelTree, "getModelTree");
	xgi::bind(this, &sentinel::arc::Application::accept, "accept");
	xgi::bind(this, &sentinel::arc::Application::clear, "clear");
}

sentinel::arc::Application::~Application ()
{

}

//
// Infoapace listener
//
void sentinel::arc::Application::actionPerformed (xdata::Event& event)
{

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		if ( oclFileName_.toString() != "" ) // for backward compatibility if paramater not specified
		{
			// makes use of the credential as specified in the OCL file for the current default zone (-z zonename or XDAQ_ZONE or default)
			this->loadOCL(oclFileName_.toString());
		}

		toolbox::TimeVal startTime = toolbox::TimeVal::gettimeofday();
		std::list<std::string> alarmStoreNames;
		for (xdata::Vector<xdata::Bag<DatabaseInfo> >::iterator i = databases_.begin(); i != databases_.end(); i++)
		{
			try
			{
				sentinel::arc::utils::AlarmStore* rep = new sentinel::arc::utils::AlarmStore(this, (*i).bag.databaseUser_.toString(), (*i).bag.databasePassword_.toString(), (*i).bag.databaseTnsName_.toString(), false, scatterReadNum_,
						maxInitExceptionNum_, flashbackInitWindow_);
				AlarmStoreDescriptor ad(rep, startTime);
				alarmStores_[rep->getName()] = ad;

				//shelfRuleCache_[rep->getName()] = new sentinel::arc::utils::PolicyCache();
				//clearRuleCache_[rep->getName()] = new sentinel::arc::utils::PolicyCache();
				//debounceRuleCache_[rep->getName()] = new sentinel::arc::utils::PolicyCache();
				alarmStoreNames.push_back(rep->getName());

				LOG4CPLUS_INFO(this->getApplicationLogger(), "Accessing DB : " << rep->getName());
				//std::cout << "DB Name = " << rep->getName() << std::endl;
			}
			catch (sentinel::arc::exception::Exception & e)
			{
				LOG4CPLUS_ERROR(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
				std::stringstream ss;
				ss << "Failed to create alarm store for database : user = " << (*i).bag.databaseUser_.toString() << ", tns = " << (*i).bag.databaseTnsName_.toString();
				XCEPT_DECLARE_NESTED(sentinel::arc::exception::Exception, ex, ss.str(), e);
				this->notifyQualified("error", ex);
				continue;
			}
		}

		// load models
		try
		{
			model_ = new sentinel::arc::utils::Model(modelURL_.toString(), severitiesURL_.toString(), alarmStoreNames);

			/*
			 toolbox::Properties e1;
			 e1.setProperty("label", "BPIX");
			 e1.setProperty("group", "unit_FRLCrate,fb_00,ttcp_BPIX");
			 e1.setProperty("severity", "critical");

			 toolbox::Properties e2;
			 e2.setProperty("label", "LTC_TOTEM");
			 e2.setProperty("group", "unit_FRLCrate,fb_01,ttcp_LTC_TOTEM");
			 e2.setProperty("severity", "fatal");

			 toolbox::Properties e3;
			 e3.setProperty("label", "TOB");
			 e3.setProperty("group", "unit_FRLCrate,fb_01,ttcp_TOB");
			 e3.setProperty("severity", "error");

			 toolbox::Properties e4;
			 e4.setProperty("label", "TIBTID");
			 e4.setProperty("group", "unit_FRLCrate,fb_01,ttcp_TIBTID");
			 e4.setProperty("severity", "warn");

			 model_->fire(e1);
			 model_->fire(e2);
			 model_->fire(e3);
			 model_->fire(e4);
			 */
			//model_->printTree();
		}
		catch (sentinel::arc::utils::exception::Exception & e)
		{
			std::cout << xcept::stdformat_exception_history(e) << std::endl;
		}

		/*
		 for (int i = 0; i < 100000; i++)
		 {
		 XCEPT_DECLARE(sentinel::arc::exception::Exception, e, "test");

		 for (std::map<std::string, AlarmStoreDescriptor>::iterator i = alarmStores_.begin(); i != alarmStores_.end(); i++)
		 {
		 (*i).second.first->fire(e);
		 }

		 }

		 ::exit(0);
		 */
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Starting load...");
		this->initialize(startTime);

		/*
		for (std::map<std::string, sentinel::arc::utils::PolicyCache*>::iterator i = shelfRuleCache_.begin(); i != shelfRuleCache_.end(); i++)
		{
			(*i).second->purge(startTime);
		}
		for (std::map<std::string, sentinel::arc::utils::PolicyCache*>::iterator i = clearRuleCache_.begin(); i != clearRuleCache_.end(); i++)
		{
			(*i).second->purge(startTime);
		}
		for (std::map<std::string, sentinel::arc::utils::PolicyCache*>::iterator i = debounceRuleCache_.begin(); i != debounceRuleCache_.end(); i++)
		{
			(*i).second->purge(startTime);
		}
		*/
		//shelfRuleCache_.purge(startTime);
		//clearRuleCache_.purge(startTime);
		//debounceRuleCache_.purge(startTime);

		std::cout << "Load complete" << std::endl;

		// start timers
		try
		{
			if (!toolbox::task::getTimerFactory()->hasTimer("sentinelarc"))
			{
				(void) toolbox::task::getTimerFactory()->createTimer("sentinelarc");
			}
			toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer("sentinelarc");
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();

			// submit scan task
			toolbox::TimeInterval dbInterval;
			dbInterval.fromString("PT2S");
			toolbox::TimeInterval shelfInterval;
			shelfInterval.fromString("PT10S");
			toolbox::TimeInterval repackInterval;
			repackInterval.fromString("PT1H");
			//repackInterval.fromString("PT15S");
			toolbox::TimeInterval garbageInterval;
			garbageInterval.fromString("PT10S");
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Schedule arc timer for " << dbInterval.toString());
			timer->scheduleAtFixedRate(start, this, dbInterval, 0, "arc-scanning");
			timer->scheduleAtFixedRate(start, this, shelfInterval, 0, "arc-shelf-maintenance");
			timer->scheduleAtFixedRate(start, this, repackInterval, 0, "arc-repack-maintenance");
			timer->scheduleAtFixedRate(start, this, garbageInterval, 0, "arc-garbage-maintenance");

		}
		catch (toolbox::task::exception::InvalidListener& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::InvalidSubmission& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::NotActive& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

		// load shelving rules
		//shelfRulesCache_.loadShelfRules("/nfshome0/aforrest/daq/sentinel/arc/xml/testshelfrules.xml");
	}
	else
	{
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}

	//interface_ = new sentinel::arc::Interface(this);

	//hyperdaq_->enable();
	//LOG4CPLUS_INFO(this->getApplicationLogger(), "HyperDAQ access enabled");
}

void sentinel::arc::Application::loadOCL(const std::string & fname)
{
	databases_.clear();

	std::vector < std::string > files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(fname);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand OCL filename from '" << fname << "', ";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		return;
	}

	std::string zone = this->getApplicationContext()->getDefaultZoneName();
	try
	{
		//LOG4CPLUS_INFO (this->getApplicationLogger(), "Load OCL from '" << files[0]  << "'");
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML( files[0] );
		DOMNodeList* list = doc->getElementsByTagNameNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2016/oraclecl"),xoap::XStr("XaasZone") );

		std::string defautZone = this->getApplicationContext()->getDefaultZoneName();

		for (XMLSize_t j = 0; j < list->getLength(); j++)
		{
			DOMNode* zoneNode = list->item(j);
			std::string zoneName  = xoap::getNodeAttribute (zoneNode, "zone");   // new network name

			if (defautZone == zoneName )
			{
				xdata::Bag<DatabaseInfo> dbinfo;
				DOMNodeList* credentials = zoneNode->getChildNodes();
				for (unsigned int j = 0; j < credentials->getLength(); j++ )
				{
					DOMNode* c = credentials->item(j);
					if (( c->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(c->getLocalName()) == "databaseUser" ))
					{
						dbinfo.bag.databaseUser_ = xoap::XMLCh2String(c->getFirstChild()->getNodeValue());
					}
					else if (( c->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(c->getLocalName()) == "databasePassword" ))
					{
						dbinfo.bag.databasePassword_ = xoap::XMLCh2String(c->getFirstChild()->getNodeValue());
					}
					else if (( c->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(c->getLocalName()) == "databaseTnsName" ))
					{
						dbinfo.bag.databaseTnsName_ = xoap::XMLCh2String(c->getFirstChild()->getNodeValue());
					}
				}
				databases_.push_back(dbinfo);
				//std::cout << "found zone: " << zoneName << " in OCL"  << std::endl;
				//std::cout << "databaseUser_: " << databaseUser_.toString()  << std::endl;
				//std::cout << "databasePassword_: " << databasePassword_.toString()  << std::endl;
				//std::cout << "databaseTnsName_: " << databaseTnsName_.toString()  << std::endl;
				break;
			}


		}
		doc->release();
		//LOG4CPLUS_INFO (this->getApplicationLogger(), "Loaded OCL from '" << fname << "'");
	}
	catch (xoap::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to load OCL file from '" << fname << "'";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
}

void sentinel::arc::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string name = e.getTimerTask()->name;
	//std::cout << "event name:" << name << std::endl;

	if (name == "arc-scanning")
	{
		//repositoryLock_.take();
		//sentinel::arc::utils::AlarmStore::hasInsertedfirstRow(false);

		for (std::map<std::string, AlarmStoreDescriptor>::iterator i = alarmStores_.begin(); i != alarmStores_.end(); i++)
		{
			toolbox::TimeVal start = (*i).second.second;
			toolbox::TimeVal newTime = (*i).second.first->lastStoredEvents(this, start);
			(*i).second.second = newTime;
		}

		//repositoryLock_.give();
	}
	else if (name == "arc-shelf-maintenance")
	{
		model_->checkShelfExpired();
	}
	else if (name == "arc-repack-maintenance")
	{
		//std::cout << "Re-Pack maintenance TODO" << std::endl;
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Re-Pack maintenance ");
		try
		{
			model_->repack();
		}
		catch(...)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(),  "error in repacking: ");
		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),  "done repack: " << name);
	}
	else if (name == "arc-garbage-maintenance")
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Garbage maintenance");
			try
			{
			model_->garbageCollection( this->maxInitExceptionNum_, this->expireInterval_);
			}
			catch(...)
			{
				LOG4CPLUS_ERROR(this->getApplicationLogger(),  "error in garbage: " << name);
			}
			LOG4CPLUS_DEBUG(this->getApplicationLogger(),  "done garbage: " << name);
		}
}

void sentinel::arc::Application::lastStoredEventsCallback (sentinel::arc::utils::AlarmStore* alarmStore, std::vector<toolbox::Properties>& events)
{
	//std::map<std::string, size_t> severities = model_->getSeverities ();
	for (std::vector<toolbox::Properties>::iterator i = events.begin(); i != events.end(); i++)
	{
		//std::string severity = toolbox::tolower((*i).getProperty(sentinel::arc::utils::SEVERITY_ATTR));
		model_->processEvent(alarmStore, *i);

		/*
		if ( severities.find(severity) != severities.end())
		{
			model_->processEvent(alarmStore, *i);
		}
		else
		{
			std::cout << "discard exception" << std::endl;
		}
		*/
	}
}

void sentinel::arc::Application::initialize (toolbox::TimeVal initTime)
{
	for (std::map<std::string, AlarmStoreDescriptor>::iterator i = alarmStores_.begin(); i != alarmStores_.end(); i++)
	{
		(*i).second.first->initialize(this, initTime);
	}
}

sentinel::arc::utils::AlarmStore* sentinel::arc::Application::getAlarmStore (std::string& key)
{
	std::map<std::string, AlarmStoreDescriptor>::iterator i = alarmStores_.find(key);
	if (i == alarmStores_.end())
	{
		return 0;
	}
	return (*i).second.first;
}

std::list<std::string> sentinel::arc::Application::getAlarmStoreNames()
{
	std::list<std::string> list;
	for (std::map<std::string, AlarmStoreDescriptor>::iterator i = alarmStores_.begin(); i != alarmStores_.end(); i++)
	{
		list.push_back((*i).first);
	}
	return list;
}

//RESTful
void sentinel::arc::Application::getNode (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	int nodeID = 1; // default root

	if (in != 0)
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("id");
		if (fi != cgi.getElements().end())
		{
			nodeID = (*fi).getIntegerValue();
		}

	}

	json_t *  document = this->model_->getNodeInfo(nodeID);


	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=60");
	char* dump = json_dumps(document, JSON_COMPACT);
	*out << dump;
	free(dump);
	json_decref(document);
}

void sentinel::arc::Application::getException (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string exceptionID;

	if (in != 0)
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("id");
		if (fi != cgi.getElements().end())
		{
			exceptionID = fi->getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "Missing exception id!");
		}
	}

	toolbox::Properties exTB = this->model_->getException(exceptionID);

	//this->repositoryLock_.take();

	std::string dbID = exTB.getProperty("sentinel-arc-db");
	sentinel::arc::utils::AlarmStore* alarmStore = this->getAlarmStore(dbID);
	if (alarmStore == 0)
	{
		//this->repositoryLock_.give();
		return;
	}

	std::string blobStr;
	alarmStore->readBlob(exceptionID, blobStr);
	//this->repositoryLock_.give();

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=60");
	*out << blobStr;
}

void sentinel::arc::Application::getExceptionList (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	int nodeID = 1; // default root
	std::set<std::string> states;
	if (in != 0)
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("nodeid");
		if (fi != cgi.getElements().end())
		{
			nodeID = (*fi).getIntegerValue();
		}
		cgicc::const_form_iterator si = cgi.getElement("state");
		if (si != cgi.getElements().end())
		{
			states = toolbox::parseTokenSet((*si).getValue(), "|");
		}
	}

	json_t*  document = this->model_->getExceptionList(nodeID, states);


	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=2");
	char* dump = json_dumps(document, JSON_COMPACT);
	*out << dump;
	free(dump);
	json_decref(document);

}



void sentinel::arc::Application::getModelTree (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	json_t* document = this->model_->getModelTree();

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=2");

	char* dump = json_dumps(document, JSON_COMPACT);
	*out << dump;
	free(dump);
	json_decref(document);
}



void sentinel::arc::Application::clear (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	try
	{
		std::string uuid = "";
		std::string identifier = "sentinel::arc::exception::Test";
		std::string notifier = "http://srv-c2d06-17.cms:1972/urn:xdaq-application:lid=3";
		std::string severity = "error";
		std::string className = "sentinel::arc::Interface";
		std::string tag = "";
		std::string instance = "";
		std::string unamespace = "urn:sentinelarc:test";
		std::string uvalue = "69";
		std::string group = this->getApplicationDescriptor()->getAttribute("group");
		std::string user = "";

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("uuid");
		if (fi != cgi.getElements().end())
		{
			uuid = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "missing uuid parameter");
		}

		fi = cgi.getElement("user");
		if (fi != cgi.getElements().end())
		{
			user = (*fi).getValue();
		}
		else
		{
			//XCEPT_RAISE(xgi::exception::Exception, "missing user parameter");
		}

		std::string authUser = in->getenv("REMOTE_USER");
		if (authUser == "")
		{
			authUser = "Unknown";
		}

		//std::cout << "HYPERDAQ clearing " << uuid << std::endl;
		//this->repositoryLock_.take();
		//
		if (user != "")
		{
			this->alarmStores_[user].first->clear(uuid, authUser);
		}
		else
		{
			std::map<std::string, std::pair<sentinel::arc::utils::AlarmStore*, toolbox::TimeVal> >::iterator i;
			for (i = this->alarmStores_.begin(); i != this->alarmStores_.end(); i++)
			{
				if ((*i).second.first->hasException(uuid))
				{
					(*i).second.first->clear(uuid, authUser);
					break;
				}
			}
		}
		//std::cout << "HYPERDAQ revoke complete" << std::endl;
		//this->repositoryLock_.give();

	}
	catch (sentinel::arc::utils::exception::FailedToStore &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (sentinel::arc::utils::exception::ConstraintViolated &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE(xgi::exception::Exception, msg.str());
	}
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=60");
}

void sentinel::arc::Application::accept (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	try
	{
		std::string uuid = "";
		std::string user = "";

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("uuid");
		if (fi != cgi.getElements().end())
		{
			uuid = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "missing uuid parameter");
		}

		fi = cgi.getElement("user");
		if (fi != cgi.getElements().end())
		{
			user = (*fi).getValue();
		}
		else
		{
			//XCEPT_RAISE(xgi::exception::Exception, "missing user parameter");
		}
		//std::cout << "HYPERDAQ accepting " << uuid << std::endl;
		//this->repositoryLock_.take();
		if (user != "")
		{
			this->alarmStores_[user].first->accept(uuid, "Unknwon");
		}
		else
		{
			std::map<std::string, std::pair<sentinel::arc::utils::AlarmStore*, toolbox::TimeVal> >::iterator i;
			for (i = this->alarmStores_.begin(); i != this->alarmStores_.end(); i++)
			{
				if ((*i).second.first->hasException(uuid))
				{
					(*i).second.first->accept(uuid, "Unknwon");
					break;
				}
			}
		}
        ///this->repositoryLock_.give();
		//std::cout << "HYPERDAQ accept complete" << std::endl;

	}
	catch (sentinel::arc::utils::exception::FailedToStore &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (sentinel::arc::utils::exception::ConstraintViolated &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE(xgi::exception::Exception, msg.str());
	}
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=60");
}

