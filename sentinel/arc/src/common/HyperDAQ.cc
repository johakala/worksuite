// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: A. Forrest, L. Orsini and D. Simelevicius                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "sentinel/arc/HyperDAQ.h"
#include "sentinel/arc/Application.h"

#include "sentinel/arc/exception/Exception.h"

#include "xcept/tools.h"

#include "xdata/InfoSpaceFactory.h"

#include "sentinel/utils/Alarm.h"

#include <string.h>

sentinel::arc::HyperDAQ::HyperDAQ (sentinel::arc::Application * app)
	: xgi::framework::UIManager(app), application_(app)
{
	xgi::framework::deferredbind(application_, this, &sentinel::arc::HyperDAQ::loading, "Default");
}

void sentinel::arc::HyperDAQ::enable ()
{
	xgi::framework::deferredbind(application_, this, &sentinel::arc::HyperDAQ::Default, "Default");

	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::modelTree, "modelTree");

	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::shelfRulesTable, "shelfRulesTable");
	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::clearRulesTable, "clearRulesTable");
	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::debounceRulesTable, "debounceRulesTable");

	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::exceptionsTable, "exceptionsTable");

	xgi::framework::deferredbind(application_, this, &sentinel::arc::HyperDAQ::viewExceptionTable, "viewExceptionTable");

	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::printAllExceptions, "printAllExceptions");

	//RESTful
	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::getNode, "getNode");
	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::getException, "getException");
	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::getExceptionList, "getExceptionList");
	xgi::deferredbind(application_, this, &sentinel::arc::HyperDAQ::getModelTree, "getModelTree");
}

void sentinel::arc::HyperDAQ::loading (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<link href=\"/sentinel/arc/html/css/xdaq-sentinel-arc.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "<div class=\"sentinel-arc-loading-screen\"><img src=\"/hyperdaq/images/framework/xdaq-ajax-loader.gif\" /><br/>Loading...</div>" << std::endl;
	*out << "<script>setInterval(function(){location.reload();}, 3000);</script>" << std::endl;
}

void sentinel::arc::HyperDAQ::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<link href=\"/sentinel/arc/html/css/xdaq-sentinel-arc.css\" rel=\"stylesheet\" />" << std::endl;

	std::stringstream urlBase;
	urlBase << application_->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << application_->getApplicationDescriptor()->getURN() << "/";

	*out << "<div class=\"xdaq-tab-wrapper\" id=\"sentinel-arc-wrapper\" data-url=\"" << urlBase.str() << "\">" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"Model\" id=\"model-tab\">" << std::endl;

	*out << "<button id=\"tree-toggle\" onclick=\"toggleGetTree()\">Auto Refresh</button><br /><br />          " << std::endl;

	this->modelTabPage(in, out);
	*out << "</div>";

	/*
	 *out << "<div class=\"xdaq-tab\" title=\"Exceptions\" id=\"exceptions-tab\">" << std::endl;
	 this->exceptionsTabPage(out);
	 *out << "</div>";
	 */

	*out << "<div class=\"xdaq-tab\" title=\"Shelf Rules\">" << std::endl;
	this->shelfRulesTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Auto-Clear Rules\">" << std::endl;
	this->clearRulesTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"De-Bounce Rules\">" << std::endl;
	this->debounceRulesTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->statisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";

	*out << "	<script type=\"text/javascript\" src=\"/sentinel/arc/html/js/xdaq-sentinel-arc.js\"></script>" << std::endl;

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html; charset=utf-8");
}

void sentinel::arc::HyperDAQ::modelTabPage (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{

	this->modelTree(in, out);

	std::stringstream urlBase;
	urlBase << application_->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << application_->getApplicationDescriptor()->getURN() << "/";

	*out << "<br/>";
	*out << "<button disabled=\"disabled\" id=\"xdaq-sentinel-arc-exception-action-accept\" href=\"" << urlBase.str() << "accept?uuid=\" class=\"xdaq-sentinel-arc-exception-action\">Accept</button> | ";
	*out << "<button disabled=\"disabled\" id=\"xdaq-sentinel-arc-exception-action-clear\" href=\"" << urlBase.str() << "clear?uuid=\" class=\"xdaq-sentinel-arc-exception-action\">Clear</button> | ";
	*out << "<button disabled=\"disabled\" id=\"xdaq-sentinel-arc-exception-action-repack\" href=\"" << urlBase.str() << "repack?uuid=\" class=\"xdaq-sentinel-arc-exception-action\">Re-Pack</button><div id=\"sentinel-arc-exception-list-type\"></div>";

	*out << "<br/><br/> <div id=\"xdaq-sentinel-arc-exceptions-table-wrapper\">          " << std::endl;
	/*
	 *out << "<table id=\"xdaq-sentinel-arc-exceptions-table\" class=\"xdaq-table\">          " << std::endl;
	 *out << "    <thead id=\"xdaq-sentinel-arc-exceptions-table-thead\">                                                 " << std::endl;
	 *out << "        <tr>                                                " << std::endl;
	 // *out << "            <th style=\"min-width: 100px;\"></th>      " << std::endl;
	 *out << "            <th style=\"min-width: 100px;\">Severity</th>      " << std::endl;
	 *out << "            <th style=\"min-width: 100px;\">Identifier</th>      " << std::endl;
	 *out << "            <th style=\"min-width: 100px;\">Message</th>      " << std::endl;
	 *out << "            <th style=\"min-width: 100px;\" class=\"xdaq-sortable\">Date/Time</th>      " << std::endl;
	 *out << "            <th style=\"min-width: 100px;\">Notifier</th>      " << std::endl;
	 *out << "            <th style=\"min-width: 100px;\">UUID</th>      " << std::endl;
	 *out << "        </tr>                                               " << std::endl;
	 *out << "    </thead>                                                " << std::endl;

	 *out << "    <tbody id=\"xdaq-sentinel-arc-exceptions-table-tbody\">                                                 " << std::endl;

	 //this->exceptionsTable(0, out);

	 *out << "    </tbody>                                                " << std::endl;
	 *out << "</table>                                                  " << std::endl;
	 */
	*out << "</div>                                                 " << std::endl;

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}

// The table of exceptions per list
void sentinel::arc::HyperDAQ::exceptionsTable (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	int nodeID = 1; // default root

	std::string listName;
	std::string severity = "";

	if (in != 0)
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("node");
		if (fi != cgi.getElements().end())
		{
			nodeID = (*fi).getIntegerValue();
		}

		fi = cgi.getElement("list");
		if (fi != cgi.getElements().end())
		{
			listName = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "Failed to extract list from HTTP request");
		}

		fi = cgi.getElement("severity");
		if (fi != cgi.getElements().end())
		{
			severity = (*fi).getValue();
		}
	}

	sentinel::arc::utils::Node* node = application_->model_->getNode(nodeID);

	////////////
	std::stringstream urlBase;
	urlBase << application_->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << application_->getApplicationDescriptor()->getURN() << "/";

	std::map < std::string, toolbox::Properties > exceptions = application_->model_->getExceptions();
	std::vector < std::string > lists;

	if (listName == "shelved")
	{
		lists.push_back("shelvedUnaccepted");
		lists.push_back("shelvedAccepted");
	}
	else
	{
		lists.push_back(listName);
	}

	std::map<size_t, std::string, std::greater<size_t> > severities = application_->model_->getSeveritiesOrdered();

	std::map < std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus > ids = application_->model_->getRootNode()->getExceptions();

	*out << "<table id=\"xdaq-sentinel-arc-exceptions-table\" class=\"xdaq-table\">          " << std::endl;
	*out << "    <thead id=\"xdaq-sentinel-arc-exceptions-table-thead\">                                                 " << std::endl;
	*out << "        <tr>                                                " << std::endl;
	//*out << "            <th style=\"min-width: 100px;\"></th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Severity</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Identifier</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Message</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\" class=\"xdaq-sortable\">Date/Time</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Notifier</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">UUID</th>      " << std::endl;
	*out << "        </tr>                                               " << std::endl;
	*out << "    </thead>                                                " << std::endl;

	*out << "    <tbody id=\"xdaq-sentinel-arc-exceptions-table-tbody\">                                                 " << std::endl;

	for (std::vector<std::string>::iterator li = lists.begin(); li != lists.end(); li++)
	{
		// for each severity
		for (std::map<size_t, std::string, std::greater<size_t> >::iterator sev = severities.begin(); sev != severities.end(); sev++)
		{

			if (severity == "" || severity == (*sev).second)
			{
				std::set < std::string > s = node->getExceptions((*li), (*sev).second);

				// for all exceptions in the list of exceptions from list li of severity sev
				for (std::set<std::string>::iterator exs = s.begin(); exs != s.end(); exs++)
				{

					toolbox::Properties& exception = exceptions[*exs];

					*out << "        <tr data-uuid=\"" << exception.getProperty("exception") << "\">            " << std::endl;

					std::string uuid = exception.getProperty("exception");
					*out << "            <td data-uuid=\"" << uuid << "\" class=\"sentinel-arc-exception-viewer-link exception-" << exception.getProperty("severity") << "\" style=\"min-width: 100px;\">" << exception.getProperty("severity") << "</td>      " << std::endl;

					*out << "            <td style=\"min-width: 100px;\">" << exception.getProperty("identifier") << "</td>      " << std::endl;

					*out << "            <td style=\"min-width: 100px;\">" << this->htmlencode(cgicc::form_urldecode(exception.getProperty("message"))) << "</td>      " << std::endl;

					*out << "            <td style=\"min-width: 100px;\">" << exception.getProperty("dateTime") << "</td>      " << std::endl;
					*out << "            <td style=\"min-width: 100px;\">" << exception.getProperty("notifier") << "</td>      " << std::endl;
					*out << "            <td style=\"min-width: 100px;\">" << uuid << "</td>   " << std::endl;
					*out << "        </tr>     " << std::endl;

				}
			}
		}
	}

	*out << "</tbody>" << std::endl;
	*out << "</table>" << std::endl;

	out->getHTTPResponseHeader().addHeader("Content-Type", "application/xml");
}

// The D0 style tree table view
void sentinel::arc::HyperDAQ::modelTree (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<table id=\"xdaq-sentinel-arc-tree\" class=\"xdaq-table-tree\">          " << std::endl;

	*out << "    <thead>                                                 " << std::endl;
	*out << "        <tr>                                                " << std::endl;
	*out << "            <th style=\"min-width: 200px;\"></th>        " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">New</th>      " << std::endl;

	std::map<size_t, std::string, std::greater<size_t> > severities = application_->model_->getSeveritiesOrdered();
	for (std::map<size_t, std::string, std::greater<size_t> >::iterator sev = severities.begin(); sev != severities.end(); sev++)
	{
		std::string title = (*sev).second;
		std::transform(title.begin(), title.begin() + 1, title.begin(), ::toupper);
		*out << "            <th style=\"min-width: 100px;\">" << title << "</th>      " << std::endl;
	}

	*out << "            <th style=\"min-width: 100px;\">Unknown</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Shelved</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Cleared</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Solved</th>      " << std::endl;
	*out << "        </tr>                                               " << std::endl;
	*out << "    </thead>                                                " << std::endl;
	*out << "    <tbody>                                                 " << std::endl;

	sentinel::arc::utils::Node * n = application_->model_->getRootNode();
	this->modelTreeRow(out, n, 0);

	*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                   " << std::endl;
}

void sentinel::arc::HyperDAQ::modelTreeRow (xgi::Output * out, sentinel::arc::utils::Node * n, size_t depth) throw (xgi::exception::Exception)
{
	std::list<sentinel::arc::utils::Node *>& children = n->getChildren();

	std::stringstream treeparent;
	std::stringstream rowClasses;
	if (n->getParent() != 0)
	{
		//treeparent << " data-treeparent=\"" << n->getParent()->nodeID_ << "\"";
		treeparent << " data-treeparent=\"" << n->getParent()->nodeID_ << "\"";

		//rowClasses << "xdaq-table-tree-hidden";
		rowClasses << "xdaq-table-tree-toggle-on";
		if (children.size() > 0)
		{
			//rowClasses << " xdaq-table-tree-not-expanded";
			rowClasses << " xdaq-table-tree-expanded";
		}
	}
	else
	{
		//rowClasses << "xdaq-table-tree-not-expanded xdaq-table-tree-toggle-on";
		rowClasses << "xdaq-table-tree-expanded xdaq-table-tree-toggle-on";
	}

	//*out << "        <tr data-treeid=\"" << n->nodeID_ << "\" data-tree-depth=\"" << depth << "\"" << treeparent.str() << " class=\"" << rowClasses.str() << "\">            " << std::endl;
	*out << "        <tr data-treeid=\"" << n->nodeID_ << "\" " << treeparent.str() << ">            " << std::endl;

	std::string classes = "";
	if (n->getTotalRaisedUnacceptedExceptionsCount() > 0)
	{
		classes = "xdaq-color-red";
	}

	std::stringstream exclass;
	if (n->getTotalRaisedUnacceptedExceptionsCount() > 0)
	{
		//std::cout << "L = " << n->getHighestSeverityValue() << std::endl;
		exclass << "exception-" << application_->model_->getSeverityMeaning(n->getHighestSeverityValue());
	}

	size_t padding = 25 + (13 * depth);
	size_t backgroundPosition = 10 + (15 * depth);

	//label
	*out << "            <td class=\"" << classes << "\" style=\"padding-left: " << padding << "px; background-position: " << backgroundPosition << "px 50%;\">" << n->label_ << "</td>                                      " << std::endl;

	// raisedUnaccepted
	*out << "            <td data-list=\"raisedUnaccepted\" data-nodeid=\"" << n->nodeID_ << "\" class=\"sentinel-arc-td-link blink " << exclass.str() << "\">" << n->getTotalRaisedUnacceptedExceptionsCount() << "</td>                                    " << std::endl;

	size_t count = 0;
	std::map<size_t, std::string, std::greater<size_t> > severities = application_->model_->getSeveritiesOrdered();
	for (std::map<size_t, std::string, std::greater<size_t> >::iterator sev = severities.begin(); sev != severities.end(); sev++)
	{
		std::stringstream exclass2;
		count = n->getRaisedAcceptedExceptionsCount((*sev).second);
		if (count > 0)
		{
			//std::cout << "L = " << n->getHighestSeverityValue() << std::endl;
			exclass2 << "exception-" << (*sev).second;
		}
		*out << "            <td data-list=\"raisedAccepted\" data-severity=\"" << (*sev).second << "\" data-nodeid=\"" << n->nodeID_ << "\" class=\"sentinel-arc-td-link " << exclass2.str() << "\">" << count << "</td>                                    " << std::endl;
	}

	std::stringstream exclass2;
	count = n->getRaisedAcceptedExceptionsCount("unknown");
	if (count > 0)
	{
		//std::cout << "L = " << n->getHighestSeverityValue() << std::endl;
		exclass2 << "exception-unknown";
	}
	*out << "            <td data-list=\"raisedAccepted\" data-severity=\"unknown\" data-nodeid=\"" << n->nodeID_ << "\" class=\"sentinel-arc-td-link " << exclass2.str() << "\">" << count << "</td>    " << std::endl;

	count = (n->getTotalShelvedUnacceptedExceptionsCount() + n->getTotalShelvedAcceptedExceptionsCount());
	std::string displayClass;
	if (count > 0)
	{
		displayClass = " exception-shelved-unaccepted";
	}
	*out << "            <td data-list=\"shelved\" data-nodeid=\"" << n->nodeID_ << "\" class=\"sentinel-arc-td-link" << displayClass << "\">" << count << "</td>                                    " << std::endl;

	count = n->getTotalClearedUnacceptedExceptionsCount();
	displayClass = "";
	if (count > 0)
	{
		displayClass = " exception-cleared-unaccepted";
	}
	*out << "            <td data-list=\"clearedUnaccepted\" data-nodeid=\"" << n->nodeID_ << "\" class=\"sentinel-arc-td-link" << displayClass << "\">" << count << "</td>                                    " << std::endl;

	count = n->getTotalClearedAcceptedExceptionsCount();
	displayClass = "";
	if (count > 0)
	{
		displayClass = " exception-cleared-accepted";
	}
	*out << "            <td data-list=\"clearedAccepted\" data-nodeid=\"" << n->nodeID_ << "\" class=\"sentinel-arc-td-link" << displayClass << "\">" << count << "</td>                                    " << std::endl;

	*out << "        </tr>                                               " << std::endl;

	for (std::list<sentinel::arc::utils::Node *>::iterator i = children.begin(); i != children.end(); i++)
	{
		this->modelTreeRow(out, (*i), depth + 1);
	}
}

// The individual exception view
void sentinel::arc::HyperDAQ::viewExceptionTable (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<link href=\"/sentinel/arc/html/css/xdaq-sentinel-arc.css\" rel=\"stylesheet\" />" << std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/sentinel/arc/html/js/xdaq-sentinel-arc-exception-viewer.js\"></script>" << std::endl;
	//*out << "	<script type=\"text/javascript\" src=\"/sentinel/arc/html/js/xdaq-sentinel-arc.js\"></script>" << std::endl;

	*out << "<div class=\"xdaq-tab-wrapper\" id=\"sentinel-arc-exception-tab-wrapper\">" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"Properties\" id=\"exception-properties-tab\">" << std::endl;

	std::string exceptionUUID;

	cgicc::Cgicc cgi(in);
	cgicc::const_form_iterator fi = cgi.getElement("uuid");
	if (fi != cgi.getElements().end())
	{
		exceptionUUID = (*fi).getValue();
	}

	toolbox::Properties exTB = application_->model_->getException(exceptionUUID);
	std::string dbID = exTB.getProperty("sentinel-arc-db");
	sentinel::arc::utils::AlarmStore* alarmStore = application_->getAlarmStore(dbID);
	if (alarmStore == 0)
	{
		return;
	}
	std::string blobStr;
	alarmStore->readBlob(exceptionUUID, blobStr);

	json_t *root;
	json_error_t error;

	root = json_loads(blobStr.c_str(), 0, &error);
	if (!root)
	{
		std::cout << "Failed to parse JSON" << std::endl;
	}

	//std::cout << "EXCEPTION BLOB" << std::endl << blobStr << std::endl;
	//return;

	*out << "<table id=\"sentinel-arc-exception-viewer-table-wrapper\">          " << std::endl;
	*out << "<tbody>          " << std::endl;
	*out << "<tr>          " << std::endl;
	*out << "<td style=\"vertical-align:top;\" id=\"xdaq-sentinel-arc-exception-viewer-td-tree\">          " << std::endl;

	std::stringstream htmlString;
	std::stringstream jsonString;
	jsonString << blobStr;

	//////////

	*out << "<table id=\"xdaq-sentinel-arc-exception-viewer-tree\" class=\"xdaq-table-tree\" data-expand=\"true\" data-allow-refresh=\"false\">          " << std::endl;
	*out << "    <tbody>                                                " << std::endl;

	json_t * originatedBy;
	json_t * label;
	json_t * data;
	originatedBy = json_object_get(root, "children");
	label = json_object_get(root, "label");

	size_t treeDepth = 0;

	const char *nodeLabel;
	nodeLabel = json_string_value(label);
	std::cout << "originatedby : " << nodeLabel << std::endl;
	*out << "<tr class=\"xdaq-table-tree-noclick\" data-treeid=\"" << treeDepth << "\" data-tree-depth=\"" << treeDepth << "\" data-label=\"" << nodeLabel << "\"><td>" << nodeLabel << "</td></tr>" << std::endl;
	treeDepth++;

	while (originatedBy)
	{
		if (!json_is_array(originatedBy))
		{
			std::cout << "originatedBy is not an array" << std::endl;
			json_decref(root);
			return;
		}
		data = json_array_get(originatedBy, 0);
		if (data)
		{
			originatedBy = json_object_get(data, "children");
			label = json_object_get(data, "label");
		}
		else
		{
			//error
		}

		nodeLabel = json_string_value(label);
		std::cout << "originatedby : " << nodeLabel << std::endl;
		*out << "<tr class=\"xdaq-table-tree-noclick\" data-label=\"" << nodeLabel << "\" data-treeid=\"" << treeDepth << "\" data-tree-depth=\"" << treeDepth << "\" data-treeparent=\"" << (treeDepth - 1) << "\"><td>" << nodeLabel << "</td></tr>" << std::endl;

		treeDepth++;
	}

	*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;

	//////////
	*out << "</td><td style=\"vertical-align:top;\" id=\"xdaq-sentinel-arc-exception-viewer-td-table\">          " << std::endl;
	//////////

	*out << "<table id=\"xdaq-sentinel-arc-exceptions-properties\" class=\"xdaq-table\">          " << std::endl;
	*out << "    <thead>                                                 " << std::endl;
	*out << "        <tr>                                                " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Name</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Value</th>      " << std::endl;
	*out << "        </tr>                                               " << std::endl;
	*out << "    </thead>                                                " << std::endl;

	originatedBy = json_object_get(root, "children");
	label = json_object_get(root, "label");

	json_t * properties;
	json_t * key;
	json_t * value;
	properties = json_object_get(root, "properties");
	treeDepth = 0;

	*out << "    <tbody id=\"sentinel-arc-exception-view-" << "" << treeDepth << "\">                                                " << std::endl;
	for (size_t i = 0; i < json_array_size(properties); i++)
	{
		data = json_array_get(properties, i);
		key = json_object_get(data, "name");
		value = json_object_get(data, "value");

		std::string v = this->htmlencode(cgicc::form_urldecode(json_string_value(value)));

		*out << "<tr><td>" << json_string_value(key) << "</td><td>" << v << "</td></tr>";
	}
	*out << "    </tbody >                                                " << std::endl;

	while (originatedBy)
	{
		if (!json_is_array(originatedBy))
		{
			std::cout << "originatedBy is not an array" << std::endl;
			json_decref(root);
			return;
		}
		data = json_array_get(originatedBy, 0);
		if (data)
		{
			originatedBy = json_object_get(data, "children");
			label = json_object_get(data, "label");
		}

		properties = json_object_get(data, "properties");

		treeDepth++;

		*out << "    <tbody class=\"xdaq-sentinel-arc-exception-viewer-hidden\" id=\"sentinel-arc-exception-view-" << "" << treeDepth << "\">                                                " << std::endl;
		for (size_t i = 0; i < json_array_size(properties); i++)
		{
			data = json_array_get(properties, i);
			key = json_object_get(data, "name");
			value = json_object_get(data, "value");

			std::string v = this->htmlencode(cgicc::form_urldecode(json_string_value(value)));

			*out << "<tr><td>" << json_string_value(key) << "</td><td>" << v << "</td></tr>";
		}
		*out << "    </tbody >                                                " << std::endl;
	}

	*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;

	//////////
	*out << "</td>          " << std::endl;
	*out << "</tr>          " << std::endl;
	*out << "</tbody>          " << std::endl;
	*out << "</table>         " << std::endl;
	json_decref(root);

	*out << "</div>" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"JSON\" id=\"exception-json-tab\">" << std::endl;
	*out << "<textarea class=\"sentinel-arc-output-textarea\">" << blobStr << "</textarea>";
	*out << "</div>";

	*out << "</div>" << std::endl;

	//out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}

/* Test functions */
void sentinel::arc::HyperDAQ::firealarm (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
	std::stringstream msg;
	msg << "Test qualified  alarm ";
	XCEPT_DECLARE(sentinel::arc::exception::Exception, ex, msg.str());
	sentinel::utils::Alarm * alarm = new sentinel::utils::Alarm("warning", ex, application_);
	is->fireItemAvailable("test-alarm", alarm);
}
void sentinel::arc::HyperDAQ::revokealarm (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	xdata::InfoSpace* is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
// immediately revoke  previous
	sentinel::utils::Alarm * ralarm = dynamic_cast<sentinel::utils::Alarm*>(is->find("test-alarm"));
	is->fireItemRevoked("test-alarm", this);
	delete ralarm;
}

void sentinel::arc::HyperDAQ::statisticsTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{

}

void sentinel::arc::HyperDAQ::shelfRulesTable (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::stringstream urlBase;
	urlBase << application_->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << application_->getApplicationDescriptor()->getURN() << "/";

	std::map<std::string, sentinel::arc::utils::PolicyCache*> cache = application_->model_->getShelfRuleCache();
	std::map<std::string, sentinel::arc::utils::PolicyCache*>::iterator j = cache.begin();
	for (; j != cache.end(); j++)
	{

		*out << "<table id=\"xdaq-sentinel-arc-shelf-rules\" class=\"xdaq-table\">          " << std::endl;
		*out << "<caption>Shelf Rules - " << (*j).first << "</caption>          " << std::endl;
		*out << "    <thead>                                                 " << std::endl;
		*out << "        <tr>                                                " << std::endl;
		*out << "            <th style=\"min-width: 100px;\"></th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">ID</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">start</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">expiry</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">exception</th>      " << std::endl;

		*out << "            <th style=\"min-width: 100px;\">identifier</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">notifier</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">severity</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">class</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">tag</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">instance</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">message</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">schema</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">version</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">module</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">line</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">func</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">lid</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">context</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">groups</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">service</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">zone</th>      " << std::endl;
		*out << "        </tr>                                               " << std::endl;
		*out << "    </thead>                                                " << std::endl;
		*out << "    <tbody>                                                 " << std::endl;

		std::map < std::string, xdata::Properties > rules = (*j).second->getRules();
		for (std::map<std::string, xdata::Properties>::iterator i = rules.begin(); i != rules.end(); i++)
		{
			/*
			std::map<std::string, std::string, std::less<std::string> > props = (*i).second.getProperties();
			for (std::map<std::string, std::string, std::less<std::string> >::iterator j = props.begin(); j != props.end(); j++)
			{
				std::cout << "PROPERTY : " << (*j).first << " = " << (*j).second << std::endl;
			}
			*/

			*out << "        <tr>            " << std::endl;

			*out << "<td><button href=\"" << urlBase.str() << "ruleoff?type=shelf&uuid=" << (*i).first << "\" class=\"xdaq-sentinel-arc-action-url\">Remove</button></td>";

			*out << "            <td style=\"min-width: 100px;\">" << (*i).first << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("start") << "</td>      " << std::endl;

			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("expiry") << "</td>      " << std::endl;

			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("exception") << "</td>      " << std::endl;

			///

			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("identifier") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("notifier") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("severity") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("class") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("tag") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("instance") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("message") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("schema") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("version") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("module") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("line") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("func") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("lid") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("context") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("groups") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("service") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("zone") << "</td>      " << std::endl;
			*out << "        </tr>            " << std::endl;
		}

		*out << "    </tbody>                                                " << std::endl;
		*out << "</table> <br/>                                                   " << std::endl;
	}

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}
void sentinel::arc::HyperDAQ::clearRulesTable (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::stringstream urlBase;
	urlBase << application_->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << application_->getApplicationDescriptor()->getURN() << "/";

	std::map<std::string, sentinel::arc::utils::PolicyCache*> cache = application_->model_->getClearRuleCache();
	std::map<std::string, sentinel::arc::utils::PolicyCache*>::iterator j = cache.begin();
	for (; j != cache.end(); j++)
	{

		*out << "<table id=\"xdaq-sentinel-arc-clear-rules\" class=\"xdaq-table\">          " << std::endl;
		*out << "<caption>Clear Rules - " << (*j).first << "</caption>          " << std::endl;
		*out << "    <thead>                                                 " << std::endl;
		*out << "        <tr>                                                " << std::endl;
		*out << "            <th style=\"min-width: 100px;\"></th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">ID</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">start</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">expiry</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">exception</th>      " << std::endl;

		*out << "            <th style=\"min-width: 100px;\">identifier</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">notifier</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">severity</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">class</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">tag</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">instance</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">message</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">schema</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">version</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">module</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">line</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">func</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">lid</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">context</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">groups</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">service</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">zone</th>      " << std::endl;
		*out << "        </tr>                                               " << std::endl;
		*out << "    </thead>                                                " << std::endl;
		*out << "    <tbody>                                                 " << std::endl;

		std::map < std::string, xdata::Properties > rules = (*j).second->getRules();
		for (std::map<std::string, xdata::Properties>::iterator i = rules.begin(); i != rules.end(); i++)
		{
			*out << "        <tr>            " << std::endl;

			*out << "<td><button href=\"" << urlBase.str() << "ruleoff?type=clear&uuid=" << (*i).first << "\" class=\"xdaq-sentinel-arc-action-url\">Remove</button></td>";

			*out << "            <td style=\"min-width: 100px;\">" << (*i).first << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("start") << "</td>      " << std::endl;

			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("expiry") << "</td>      " << std::endl;

			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("exception") << "</td>      " << std::endl;

			///

			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("identifier") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("notifier") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("severity") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("class") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("tag") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("instance") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("message") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("schema") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("version") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("module") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("line") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("func") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("lid") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("context") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("groups") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("service") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("zone") << "</td>      " << std::endl;
			*out << "        </tr>            " << std::endl;
		}

		*out << "    </tbody>                                                " << std::endl;
		*out << "</table><br/>                                                    " << std::endl;
	}

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}
void sentinel::arc::HyperDAQ::debounceRulesTable (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::stringstream urlBase;
	urlBase << application_->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << application_->getApplicationDescriptor()->getURN() << "/";

	std::map<std::string, sentinel::arc::utils::PolicyCache*> cache = application_->model_->getDebounceRuleCache();
	std::map<std::string, sentinel::arc::utils::PolicyCache*>::iterator j = cache.begin();
	for (; j != cache.end(); j++)
	{

		*out << "<table id=\"xdaq-sentinel-arc-debounce-rules\" class=\"xdaq-table\">          " << std::endl;
		*out << "<caption>De-Bounce Rules - " << (*j).first << "</caption>          " << std::endl;
		*out << "    <thead>                                                 " << std::endl;
		*out << "        <tr>                                                " << std::endl;
		*out << "            <th style=\"min-width: 100px;\"></th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">ID</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">start</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">expiry</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">debounce</th>      " << std::endl;

		*out << "            <th style=\"min-width: 100px;\">identifier</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">notifier</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">severity</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">class</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">tag</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">instance</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">message</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">schema</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">version</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">module</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">line</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">func</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">lid</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">context</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">groups</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">service</th>      " << std::endl;
		*out << "            <th style=\"min-width: 100px;\">zone</th>      " << std::endl;

		*out << "        </tr>                                               " << std::endl;
		*out << "    </thead>                                                " << std::endl;
		*out << "    <tbody>                                                 " << std::endl;

		std::map < std::string, xdata::Properties > rules = (*j).second->getRules();
		for (std::map<std::string, xdata::Properties>::iterator i = rules.begin(); i != rules.end(); i++)
		{
			*out << "        <tr>            " << std::endl;

			*out << "<td><button href=\"" << urlBase.str() << "ruleoff?type=debounce&uuid=" << (*i).first << "\" class=\"xdaq-sentinel-arc-action-url\">Remove</button></td>";

			*out << "            <td style=\"min-width: 100px;\">" << (*i).first << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("start") << "</td>      " << std::endl;

			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("expiry") << "</td>      " << std::endl;

			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("debounce") << "</td>      " << std::endl;

			///

			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("identifier") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("notifier") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("severity") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("class") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("tag") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("instance") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("message") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("schema") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("version") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("module") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("line") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("func") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("lid") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("context") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("groups") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("service") << "</td>      " << std::endl;
			*out << "            <td style=\"min-width: 100px;\">" << (*i).second.getProperty("zone") << "</td>      " << std::endl;
			*out << "        </tr>            " << std::endl;
		}

		*out << "    </tbody>                                                " << std::endl;
		*out << "</table>                                                    " << std::endl;
	}

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}

/*
 args
 expiry
 user

 identifier
 notifier
 severity
 class
 tag
 instance
 unamespace
 uvalue
 */

void sentinel::arc::HyperDAQ::shelfRulesTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<button id=\"srules-toggle\" onclick=\"toggleGetSRules()\">Auto Refresh</button><br />          " << std::endl;

	*out << "<table id=\"srule-maker\" class=\"xdaq-table\">          " << std::endl;
	*out << "	<caption>New Rule</caption>          " << std::endl;
	*out << "    <tbody>                                                 " << std::endl;
	//*out << "<tr><td style=\"min-width: 100px;\">DB</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-user\" value=\"int9r_lb-CMS_LUMI_ERR_LOG\"/></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">DB Scope</td><td><select id=\"srule-maker-user\">";
	*out << "<option value=\"global\">Global</option>" << std::endl;
	std::list < std::string > stores = application_->getAlarmStoreNames();
	for (std::list<std::string>::iterator i = stores.begin(); i != stores.end(); i++)
	{
		*out << "<option value=\"" << (*i) << "\">" << (*i) << "</option>" << std::endl;
	}
	*out << "</select></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">expiry</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-expiry\" \"PT30S\"/></td></tr>      " << std::endl;

	*out << "<tr><td colspan=\"2\">REGEX</td></tr>      " << std::endl;

	*out << "<tr><td style=\"min-width: 100px;\">exceptionUUID</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-args\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">identifier</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-identifier\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">notifier</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-notifier\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">severity</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-severity\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">class</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-class\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">tag</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-tag\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">instance</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-instance\" /></td></tr>      " << std::endl;

	*out << "<tr><td style=\"min-width: 100px;\">message</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-message\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">schema</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-schema\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">version</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-version\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">module</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-module\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">line</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-line\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">func</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-func\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">lid</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-lid\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">context</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-context\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">groups</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-groups\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">service</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-service\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">zone</td><td><input type=\"text\" style=\"width:200px\" id=\"srule-maker-zone\" /></td></tr>      " << std::endl;

	*out << "<tr><td colspan=\"2\"><input type=\"button\" data-type=\"shelfon\" data-prefix=\"s\" value=\"Submit\" class=\"rule-maker-submit xdaq-submit-button\"/></td></tr>      " << std::endl;

	*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;

	*out << "<div id=\"shelfRulesTableWrapper\">" << std::endl;
	this->shelfRulesTable(0, out);
	*out << "</div>" << std::endl;
}
void sentinel::arc::HyperDAQ::clearRulesTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<button id=\"crules-toggle\" onclick=\"toggleGetCRules()\">Auto Refresh</button><br />          " << std::endl;

	*out << "<table id=\"crule-maker\" class=\"xdaq-table\">          " << std::endl;
	*out << "	<caption>New Rule</caption>          " << std::endl;
	*out << "    <tbody>                                                 " << std::endl;
	//*out << "<tr><td style=\"min-width: 100px;\">DB</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-user\" value=\"int9r_lb-CMS_LUMI_ERR_LOG\"/></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">DB Scope</td><td><select id=\"crule-maker-user\">";
	*out << "<option value=\"global\">Global</option>" << std::endl;
	std::list < std::string > stores = application_->getAlarmStoreNames();
	for (std::list<std::string>::iterator i = stores.begin(); i != stores.end(); i++)
	{
		*out << "<option value=\"" << (*i) << "\">" << (*i) << "</option>" << std::endl;
	}
	*out << "</select></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">expiry</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-expiry\" \"PT30S\"/></td></tr>      " << std::endl;
	//*out << "<tr><td style=\"min-width: 100px;\">args</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-args\" /></td></tr>      " << std::endl;
	*out << "<input type=\"hidden\" id=\"crule-maker-args\" />      " << std::endl;

	*out << "<tr><td colspan=\"2\">REGEX</td></tr>      " << std::endl;

	*out << "<tr><td style=\"min-width: 100px;\">identifier</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-identifier\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">notifier</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-notifier\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">severity</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-severity\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">class</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-class\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">tag</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-tag\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">instance</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-instance\" /></td></tr>      " << std::endl;

	*out << "<tr><td style=\"min-width: 100px;\">message</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-message\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">schema</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-schema\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">version</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-version\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">module</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-module\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">line</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-line\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">func</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-func\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">lid</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-lid\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">context</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-context\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">groups</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-groups\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">service</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-service\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">zone</td><td><input type=\"text\" style=\"width:200px\" id=\"crule-maker-zone\" /></td></tr>      " << std::endl;

	*out << "<tr><td colspan=\"2\"><input type=\"button\" data-type=\"clearon\" data-prefix=\"c\" value=\"Submit\" class=\"rule-maker-submit xdaq-submit-button\"/></td></tr>      " << std::endl;

	*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;

	*out << "<div id=\"clearRulesTableWrapper\">" << std::endl;
	this->clearRulesTable(0, out);
	*out << "</div>" << std::endl;
}
void sentinel::arc::HyperDAQ::debounceRulesTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<button id=\"drules-toggle\" onclick=\"toggleGetDRules()\">Auto Refresh</button><br />          " << std::endl;

	*out << "<table id=\"drule-maker\" class=\"xdaq-table\">          " << std::endl;
	*out << "	<caption>New Rule</caption>          " << std::endl;
	*out << "    <tbody>                                                 " << std::endl;

	//*out << "<tr><td style=\"min-width: 100px;\">DB</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-user\" value=\"int9r_lb-CMS_LUMI_ERR_LOG\"/></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">DB Scope</td><td><select id=\"drule-maker-user\">";
	*out << "<option value=\"global\">Global</option>" << std::endl;
	std::list < std::string > stores = application_->getAlarmStoreNames();
	for (std::list<std::string>::iterator i = stores.begin(); i != stores.end(); i++)
	{
		*out << "<option value=\"" << (*i) << "\">" << (*i) << "</option>" << std::endl;
	}
	*out << "</select></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">expiry</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-expiry\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">bounce time</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-args\" value=\"PT30S\" /></td></tr>      " << std::endl;

	*out << "<tr><td colspan=\"2\">REGEX</td></tr>      " << std::endl;

	*out << "<tr><td style=\"min-width: 100px;\">identifier</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-identifier\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">notifier</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-notifier\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">severity</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-severity\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">class</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-class\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">tag</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-tag\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">instance</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-instance\" /></td></tr>      " << std::endl;

	*out << "<tr><td style=\"min-width: 100px;\">message</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-message\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">schema</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-schema\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">version</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-version\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">module</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-module\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">line</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-line\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">func</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-func\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">lid</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-lid\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">context</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-context\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">groups</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-groups\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">service</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-service\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">zone</td><td><input type=\"text\" style=\"width:200px\" id=\"drule-maker-zone\" /></td></tr>      " << std::endl;

	*out << "<tr><td colspan=\"2\"><input type=\"button\" data-type=\"debounceon\" data-prefix=\"d\" value=\"Submit\" class=\"rule-maker-submit xdaq-submit-button\"/></td></tr>      " << std::endl;

	*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;

	*out << "<div id=\"debounceRulesTableWrapper\">" << std::endl;
	this->debounceRulesTable(0, out);
	*out << "</div>" << std::endl;
}

void sentinel::arc::HyperDAQ::printAllExceptions (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::cout << std::endl << "Printing all exceptions" << std::endl;
	std::map < std::string, toolbox::Properties > exceptions = application_->model_->getExceptions();
	for (std::map<std::string, toolbox::Properties>::iterator i = exceptions.begin(); i != exceptions.end(); i++)
	{
		std::cout << (*i).second.getProperty("exception") << std::endl;
	}
	std::cout << "END" << std::endl << std::endl;
}

std::string sentinel::arc::HyperDAQ::htmlencode (const std::string& src)
{
	size_t len = src.length();
	std::stringstream dst;
	for (size_t i = 0; i < len; i++)
	{
		char ch = src[i];
		switch (ch)
		{
			case '&':
				dst << "&amp;";
				break;
			case '\'':
				dst << "&apos;";
				break;
			case '"':
				dst << "&quot;";
				break;
			case '<':
				dst << "&lt;";
				break;
			case '>':
				dst << "&gt;";
				break;
			default:
				dst << ch;
				break;
		}
	}
	return dst.str();
}

//RESTful
void sentinel::arc::HyperDAQ::getNode (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	int nodeID = 1; // default root

	if (in != 0)
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("id");
		if (fi != cgi.getElements().end())
		{
			nodeID = (*fi).getIntegerValue();
		}

	}

	application_->model_->lock();

	sentinel::arc::utils::Node* node = application_->model_->getNode(nodeID);

	// Output in JSON format
	/*
	 "id": 56,
	"label": "root",
	"parent": 53,
	"children": [34, 35, 36],
	"raisedUnacceptedExceptions" :

	"raisedAcceptedExceptions" :
	"shelvedUnacceptedExceptions" : {
		"count" : 56,
		"exceptions" : [
			{
				"ids" : [uuid1,"uuuid2", "uuid3"],
				"count" : 2,
				"level": 100,
				"severity" : "warning"
			}
			,
			{
				"ids" : ["uuid1", "uuid2"],
				"count" : 2,
				"level": 200,
				"severity" : "error"
			}
			]
	}
	"shelvedAcceptedExceptions" :

	"clearedUnacceptedExceptions" :

	"clearedAcceptedExceptions" :
	 */


	json_t* document = json_object();
	//id
	json_object_set_new(document, "id", json_integer(nodeID));
	//label
	json_object_set_new(document, "label", json_string(node->label_.c_str()));
	//parent
	sentinel::arc::utils::Node* parent = node->getParent();
	if (parent != 0)
	{
		json_object_set_new(document, "parent", json_integer(parent->nodeID_));
	}
	else
	{
		json_object_set_new(document, "parent", json_integer(0));
	}
	//children
	std::list<sentinel::arc::utils::Node*>& children = node->getChildren();
	json_t* jsonvector = json_array();
	for (std::list<sentinel::arc::utils::Node*>::iterator i = children.begin(); i != children.end(); i++)
	{
		json_t* val = json_integer((*i)->nodeID_);
		json_array_append_new(jsonvector, val);
	}
	json_object_set_new(document, "children", jsonvector);

	int totalCount = 0;
	json_t* exceptions;
	//raisedUnacceptedExceptions
	exceptions = this->getNode (totalCount, node->raisedUnacceptedExceptions_);
	json_object_set_new(document, "raisedUnacceptedExceptions", exceptions);

	//raisedAcceptedExceptions
	exceptions = this->getNode (totalCount, node->raisedAcceptedExceptions_);
	json_object_set_new(document, "raisedAcceptedExceptions", exceptions);

	//clearedUnacceptedExceptions
	exceptions = this->getNode (totalCount, node->clearedUnacceptedExceptions_);
	json_object_set_new(document, "clearedUnacceptedExceptions", exceptions);

	//clearedAcceptedExceptions
	exceptions = this->getNode (totalCount, node->clearedAcceptedExceptions_);
	json_object_set_new(document, "clearedAcceptedExceptions", exceptions);

	json_object_set_new(document, "count", json_integer(totalCount));

	application_->model_->unlock();

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=60");
	char* dump = json_dumps(document, JSON_COMPACT);
	*out << dump;
	free(dump);
	json_decref(document);
}

void sentinel::arc::HyperDAQ::getException (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string exceptionID;

	if (in != 0)
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("id");
		if (fi != cgi.getElements().end())
		{
			exceptionID = fi->getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "Missing exception id!");
		}
	}

	application_->model_->lock();

	toolbox::Properties exTB = application_->model_->getException(exceptionID);

	application_->model_->unlock();

	application_->repositoryLock_.take();

	std::string dbID = exTB.getProperty("sentinel-arc-db");
	sentinel::arc::utils::AlarmStore* alarmStore = application_->getAlarmStore(dbID);
	if (alarmStore == 0)
	{
		application_->repositoryLock_.give();
		return;
	}

	std::string blobStr;
	alarmStore->readBlob(exceptionID, blobStr);
	application_->repositoryLock_.give();

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=60");
	*out << blobStr;
}

void sentinel::arc::HyperDAQ::getExceptionList (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	int nodeID = 1; // default root
	std::set<std::string> states;
	if (in != 0)
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("nodeid");
		if (fi != cgi.getElements().end())
		{
			nodeID = (*fi).getIntegerValue();
		}
		cgicc::const_form_iterator si = cgi.getElement("state");
		if (si != cgi.getElements().end())
		{
			states = toolbox::parseTokenSet((*si).getValue(), "|");
		}
	}

	json_t* document = json_object();
	json_t* jsonvector = json_array();
	json_object_set_new(document, "exceptions", jsonvector);

	application_->model_->lock();

	sentinel::arc::utils::Node* node = application_->model_->getNode(nodeID);

	for (std::set<std::string>::iterator i = states.begin(); i != states.end(); i++)
	{
		if ( (*i) == "RaisedUnaccepted" )
			this->getExceptionList(jsonvector, node->raisedUnacceptedExceptions_, *i);
		if ( (*i) == "RaisedAccepted" )
			this->getExceptionList(jsonvector, node->raisedAcceptedExceptions_, *i);
		if ( (*i) == "ClearedUnaccepted" )
			this->getExceptionList(jsonvector, node->clearedUnacceptedExceptions_, *i);
		if ( (*i) == "ClearedAccepted" )
			this->getExceptionList(jsonvector, node->clearedAcceptedExceptions_, *i);
	}
	application_->model_->unlock();

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=2");
	char* dump = json_dumps(document, JSON_COMPACT);
	*out << dump;
	free(dump);
	json_decref(document);

}

json_t * sentinel::arc::HyperDAQ::getNode (int & totalCount, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap)
{
	json_t* stateObject = json_object();
	json_t* exceptions = json_array();
	json_object_set_new(stateObject, "exceptions", exceptions);
	int stateCount = 0;

	application_->model_->lock();

	for (sentinel::arc::utils::Node::ExceptionMap::iterator i = exceptionsMap.begin(); i != exceptionsMap.end(); i++)
	{
		json_t* severityitem = json_object();
		int count = (*i).second.size();
		stateCount += count;
		json_object_set_new(severityitem, "count", json_integer(count));
		json_object_set_new(severityitem, "severity", json_string((*i).first.c_str()));
		json_object_set_new(severityitem, "level", json_integer(application_->model_->getSeverityLevel((*i).first)));

		json_t* ids = json_array();
		json_object_set_new(severityitem, "ids", ids);
		for(std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			json_t* val = json_string(j->c_str());
			json_array_append_new(ids, val);
		}
		json_array_append_new(exceptions, severityitem);
	}

	application_->model_->unlock();

	json_object_set_new(stateObject, "count", json_integer(stateCount));
	totalCount += stateCount;
	return stateObject;
}

void sentinel::arc::HyperDAQ::getExceptionList (json_t * jsonvector, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap, const std::string & exceptionState)
{
	application_->model_->lock();

	for (sentinel::arc::utils::Node::ExceptionMap::iterator i = exceptionsMap.begin(); i != exceptionsMap.end(); i++)
	{
		for(std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			json_t* exceptionitem = json_object();
			std::map < std::string, toolbox::Properties > exceptions = application_->model_->getExceptions();
			std::vector<std::string> names = exceptions[*j].propertyNames();

			for(std::vector<std::string>::iterator k = names.begin(); k != names.end(); k++)
			{
				std::string value = exceptions[*j].getProperty(*k);
				json_object_set_new(exceptionitem, (*k).c_str(), json_string(value.c_str()));
				if ((*k) == "severity" )
				{
					size_t severitylevel = application_->model_->getSeverityLevel(toolbox::tolower(value));
					json_object_set_new(exceptionitem, "severitylevel", json_integer(severitylevel));
				}
			}
			json_object_set_new(exceptionitem, "exceptionState", json_string(exceptionState.c_str()));

			json_array_append_new(jsonvector, exceptionitem);
		}
	}

	application_->model_->unlock();
}
void sentinel::arc::HyperDAQ::getModelTree (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	application_->model_->lock();
	json_t* root = this->getModelTree(application_->model_->getRootNode());
	application_->model_->unlock();
	json_t* document = json_array();
	json_array_append_new(document, root);
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=2");

	char* dump = json_dumps(document, JSON_COMPACT);
	*out << dump;
	free(dump);
	json_decref(document);
}

json_t * sentinel::arc::HyperDAQ::getModelTree (sentinel::arc::utils::Node * node)
{
	json_t* parent = json_object();

	json_object_set_new(parent, "id", json_string(toolbox::toString("%d", node->nodeID_).c_str()));
	json_object_set_new(parent, "key", json_string(toolbox::toString("%d", node->nodeID_).c_str()));
	json_object_set_new(parent, "title", json_string(node->label_.c_str()));
	json_object_set_new(parent, "lastupdate", json_string(node->lastUpdate_.toString("", toolbox::TimeVal::gmt).c_str()));
	size_t conspicuity = node->getHighestSeverityValue();
	json_object_set_new(parent, "conspicuity", json_string(application_->model_->getSeverityMeaning(conspicuity).c_str()));

	// add all avaiable counters
	json_object_set_new(parent, "raisedunaccepted", json_string(toolbox::toString("%d", node->getTotalRaisedUnacceptedExceptionsCount()).c_str()));
	json_object_set_new(parent, "raisedaccepted", json_string(toolbox::toString("%d", node->getTotalRaisedAcceptedExceptionsCount()).c_str()));
	json_object_set_new(parent, "clearedunaccepted", json_string(toolbox::toString("%d", node->getTotalClearedUnacceptedExceptionsCount()).c_str()));
	json_object_set_new(parent, "clearedaccepted", json_string(toolbox::toString("%d", node->getTotalClearedAcceptedExceptionsCount()).c_str()));
	//


	json_t* tags = json_array();
	json_object_set_new(parent, "tags", tags);
	int count = node->getTotalRaisedUnacceptedExceptionsCount() + node->getTotalRaisedAcceptedExceptionsCount();
	json_array_append_new(tags, json_string(toolbox::toString("%d", count).c_str()));

	std::list<sentinel::arc::utils::Node *>& children = node->getChildren();

	json_t* nodes = json_array();
	json_object_set_new(parent, "children", nodes);
	for (std::list<sentinel::arc::utils::Node *>::iterator i = children.begin(); i != children.end(); i++)
	{
		json_t* child = this->getModelTree(*i);
		json_array_append_new(nodes, child);
	}

	if (children.size() > 0)
	{
		json_object_set_new(parent, "folder", json_boolean(true));
	}

	return parent;
}




