// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "sentinel/arc/Interface.h"
#include "sentinel/arc/Application.h"

sentinel::arc::Interface::Interface (sentinel::arc::Application * app)
	: application_(app)
{
	xgi::deferredbind(app, this, &sentinel::arc::Interface::getExceptions, "getExceptions");
	xgi::deferredbind(app, this, &sentinel::arc::Interface::getNodeSummary, "getNodeSummary");
	xgi::deferredbind(app, this, &sentinel::arc::Interface::getNodeExceptionList, "getNodeExceptionList");

	xgi::deferredbind(app, this, &sentinel::arc::Interface::getNodePath, "getNodePath");

	xgi::deferredbind(app, this, &sentinel::arc::Interface::accept, "accept");
	xgi::deferredbind(app, this, &sentinel::arc::Interface::clear, "clear");

	xgi::deferredbind(app, this, &sentinel::arc::Interface::repack, "repack");

	xgi::deferredbind(app, this, &sentinel::arc::Interface::ruleon, "ruleon");
	xgi::deferredbind(app, this, &sentinel::arc::Interface::ruleoff, "ruleoff");
}

void sentinel::arc::Interface::getNodePath (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");

	int nodeID = 1; // default root

	if (in != 0)
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("node");
		if (fi != cgi.getElements().end())
		{
			nodeID = (*fi).getIntegerValue();
		}
	}

	sentinel::arc::utils::Node* node = application_->model_->getNode(nodeID);

	std::string path = "";
	sentinel::arc::utils::Node* parent = node;

	*out << "{" << std::endl;
	*out << "\"path\": [" << std::endl;

	do
	{
		*out << "{" << std::endl;

		*out << "\"id\": " << parent->nodeID_ << "," << std::endl;
		*out << "\"label\": \"" << parent->label_ << "\"" << std::endl;

		parent = parent->getParent();

		*out << "}" << std::endl;
	} while (parent != 0);

	*out << "]" << std::endl;
	*out << "}" << std::endl;
}

void sentinel::arc::Interface::getExceptionBlob (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
	std::string exceptionUUID;

	cgicc::Cgicc cgi(in);
	cgicc::const_form_iterator fi = cgi.getElement("uuid");
	if (fi != cgi.getElements().end())
	{
		exceptionUUID = (*fi).getValue();
	}

	toolbox::Properties exTB = application_->model_->getException(exceptionUUID);
	std::string dbID = exTB.getProperty("sentinel-arc-db");
	sentinel::arc::utils::AlarmStore* alarmStore = application_->getAlarmStore(dbID);
	if (alarmStore == 0)
	{
		return;
	}
	std::string blobStr;
	alarmStore->readBlob(exceptionUUID, blobStr);

	*out << blobStr;
}

void sentinel::arc::Interface::getExceptions (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");

	std::map < std::string, toolbox::Properties > exceptions = application_->model_->getExceptions();

	*out << "{" << std::endl;
	*out << "\"exceptions\": [" << std::endl;
	std::map<std::string, toolbox::Properties>::iterator i = exceptions.begin();
	while (i != exceptions.end())
	{
		*out << "{" << std::endl;

		*out << "\"uuid\": \"" << (*i).second.getProperty("exception") << "\"," << std::endl;
		*out << "\"severity\": \"" << (*i).second.getProperty("severity") << "\"" << std::endl;

		*out << "}" << std::endl;

		i++;
		if (i != exceptions.end()) *out << "," << std::endl;
	}

	*out << "]" << std::endl;
	*out << "}" << std::endl;
}

void sentinel::arc::Interface::getNodeSummary (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");

	cgicc::Cgicc cgi(in);
	size_t nodeID = this->getNodeID(cgi);

	sentinel::arc::utils::Node* node = application_->model_->getNode(nodeID);
	if (node == 0)
	{
		*out << "FAIL" << std::endl;
		return;
	}

	*out << "{" << std::endl;

	*out << "\"path\": [" << std::endl;
	sentinel::arc::utils::Node* parent = node;
	do
	{
		*out << "{" << std::endl;

		*out << "\"id\": " << parent->nodeID_ << "," << std::endl;
		*out << "\"label\": \"" << parent->label_ << "\"" << std::endl;

		*out << "}" << std::endl;

		parent = parent->getParent();
		if (parent != 0) *out << "," << std::endl;
	} while (parent != 0);
	*out << "]," << std::endl;

	*out << "\"severities\": [" << std::endl;
	std::map<size_t, std::string, std::greater<size_t> > sev = application_->model_->getSeveritiesOrdered();
	std::map<size_t, std::string, std::greater<size_t> >::iterator i = sev.begin();
	while (i != sev.end())
	{
		*out << "{" << std::endl;

		*out << "\"label\": \"" << (*i).second << "\"," << std::endl;
		*out << "\"value\": " << (*i).first << "" << std::endl;

		*out << "}" << std::endl;

		i++;
		if (i != sev.end()) *out << "," << std::endl;
	}
	*out << "]," << std::endl;

	*out << "\"children\": [" << std::endl;
	std::list<sentinel::arc::utils::Node *>& children = node->getChildren();
	std::list<sentinel::arc::utils::Node *>::iterator c = children.begin();
	while (c != children.end())
	{
		this->nodeToJSON(out, *c);

		c++;
		if (c != children.end()) *out << "," << std::endl;
	}
	*out << "]," << std::endl;

	*out << "\"node\": " << std::endl;
	this->nodeToJSON(out, node);

	*out << "}" << std::endl;

	/*
	 {
	 "path": [{"id":0, "label":"CMS"},{"id":1, "label":"ECAL"},{}],
	 "severities": [{"label":"FATAL", "value":4000},{"label":"ERROR", "value":2000},{}],
	 "children":[{"id": 1, "label":"ECAL","new":0, "accepted":[{"FATAL":0},{"ERROR":0},{}],"shelved":0,"cleared":0,"solved":0},{}]
	 }
	 */
}
void sentinel::arc::Interface::nodeToJSON (xgi::Output * out, sentinel::arc::utils::Node * node)
{

	*out << "{" << std::endl;

	*out << "\"id\": " << node->nodeID_ << "," << std::endl;
	*out << "\"label\": \"" << node->label_ << "\"," << std::endl;
	*out << "\"raisedUnacceptedHighestSeverity\": \"" << node->getHighestRaisedUnacceptedSeverityValue() << "\"," << std::endl;
	*out << "\"raisedAcceptedHighestSeverity\": \"" << node->getHighestRaisedAcceptedSeverityValue() << "\"," << std::endl;
	*out << "\"raisedUnaccepted\": " << node->getTotalRaisedUnacceptedExceptionsCount() << "," << std::endl;
	*out << "\"shelvedUnaccepted\": " << node->getTotalShelvedUnacceptedExceptionsCount() << "," << std::endl;
	*out << "\"shelvedAccepted\": " << node->getTotalShelvedAcceptedExceptionsCount() << "," << std::endl;
	*out << "\"clearedUnaccepted\": " << node->getTotalClearedUnacceptedExceptionsCount() << "," << std::endl;
	*out << "\"clearedAccepted\": " << node->getTotalClearedAcceptedExceptionsCount() << "," << std::endl;

	*out << "\"raisedAccepted\": {" << std::endl;
	std::map < std::string, size_t > sev = application_->model_->getSeverities();
	std::map<std::string, size_t>::iterator j = sev.begin();
	while (j != sev.end())
	{
		//*out << "{" << std::endl;

		*out << "\"" << (*j).first << "\": " << node->getRaisedAcceptedExceptionsCount((*j).first) << "" << std::endl;

		//*out << "}" << std::endl;

		j++;
		if (j != sev.end()) *out << "," << std::endl;
	}

	*out << "}" << std::endl;
	*out << "}" << std::endl;
}

size_t sentinel::arc::Interface::getNodeID (cgicc::Cgicc & cgi) throw (xgi::exception::Exception)
{
	int nodeID = 0; // number of results per page

	cgicc::const_form_iterator fi = cgi.getElement("node");
	if (fi != cgi.getElements().end())
	{
		nodeID = (*fi).getIntegerValue();
	}
	else
	{
		XCEPT_RAISE(xgi::exception::Exception, "Failed to extract node id from HTTP request");
	}

	return (size_t) nodeID;
}

void sentinel::arc::Interface::getNodeExceptionList (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);
	size_t nodeID = this->getNodeID(cgi);

	std::string listName;
	std::string severity = "";

	cgicc::const_form_iterator fi = cgi.getElement("list");
	if (fi != cgi.getElements().end())
	{
		listName = (*fi).getValue();
	}
	else
	{
		XCEPT_RAISE(xgi::exception::Exception, "Failed to extract node id from HTTP request");
	}

	fi = cgi.getElement("severity");
	if (fi != cgi.getElements().end())
	{
		severity = (*fi).getValue();
	}

	sentinel::arc::utils::Node* node = application_->model_->getNode(nodeID);

	std::map < std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus > exceptionDescriptors;

	if (listName == "shelved")
	{
		exceptionDescriptors = node->getExceptions("shelvedUnaccepted");
		std::map < std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus > exceptionsB = node->getExceptions("shelvedAccepted");
		exceptionDescriptors.insert(exceptionsB.begin(), exceptionsB.end());
	}
	else
	{
		exceptionDescriptors = node->getExceptions(listName);
	}

	*out << "{" << std::endl;
	*out << "\"exceptions\": [" << std::endl;

	std::map < std::string, toolbox::Properties > exceptions = application_->model_->getExceptions();
	std::map<std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus>::iterator i = exceptionDescriptors.begin();
	while (i != exceptionDescriptors.end())
	{
		toolbox::Properties& ex = exceptions[(*i).first];

		std::string sev = ex.getProperty("severity");
		if (severity == "" || severity == sev)
		{
			*out << "{" << std::endl;

			*out << "\"identifier\": \"" << ex.getProperty("identifier") << "\"," << std::endl;
			*out << "\"message\": \"" << ex.getProperty("message") << "\"," << std::endl;
			*out << "\"uuid\": \"" << ex.getProperty("exception") << "\"," << std::endl;
			*out << "\"dateTime\": \"" << ex.getProperty("dateTime") << "\"," << std::endl;
			*out << "\"severity\": \"" << sev << "\"" << std::endl;

			*out << "}" << std::endl;

			i++;
			if (i != exceptionDescriptors.end()) *out << "," << std::endl;
		}
		else
		{
			i++;
		}
	}

	*out << "]" << std::endl;
	*out << "}" << std::endl;
}

void sentinel::arc::Interface::clear (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	try
	{
		std::string uuid = "";
		std::string identifier = "sentinel::arc::exception::Test";
		std::string notifier = "http://srv-c2d06-17.cms:1972/urn:xdaq-application:lid=3";
		std::string severity = "error";
		std::string className = "sentinel::arc::Interface";
		std::string tag = "";
		std::string instance = "";
		std::string unamespace = "urn:sentinelarc:test";
		std::string uvalue = "69";
		std::string group = application_->getApplicationDescriptor()->getAttribute("group");
		std::string user = "";

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("uuid");
		if (fi != cgi.getElements().end())
		{
			uuid = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "missing uuid parameter");
		}

		fi = cgi.getElement("user");
		if (fi != cgi.getElements().end())
		{
			user = (*fi).getValue();
		}
		else
		{
			//XCEPT_RAISE(xgi::exception::Exception, "missing user parameter");
		}

		std::string authUser = in->getenv("REMOTE_USER");
		if (authUser == "")
		{
			authUser = "Unknown";
		}

		//std::cout << "HYPERDAQ clearing " << uuid << std::endl;
		application_->repositoryLock_.take();
		//
		if (user != "")
		{
			application_->alarmStores_[user].first->clear(uuid, authUser);
		}
		else
		{
			std::map<std::string, std::pair<sentinel::arc::utils::AlarmStore*, toolbox::TimeVal> >::iterator i;
			for (i = application_->alarmStores_.begin(); i != application_->alarmStores_.end(); i++)
			{
				if ((*i).second.first->hasException(uuid))
				{
					(*i).second.first->clear(uuid, authUser);
					break;
				}
			}
		}
		//std::cout << "HYPERDAQ revoke complete" << std::endl;
		application_->repositoryLock_.give();

	}
	catch (sentinel::arc::utils::exception::FailedToStore &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (sentinel::arc::utils::exception::ConstraintViolated &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE(xgi::exception::Exception, msg.str());
	}
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=60");
}
void sentinel::arc::Interface::accept (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	try
	{
		std::string uuid = "";
		std::string user = "";

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("uuid");
		if (fi != cgi.getElements().end())
		{
			uuid = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "missing uuid parameter");
		}

		fi = cgi.getElement("user");
		if (fi != cgi.getElements().end())
		{
			user = (*fi).getValue();
		}
		else
		{
			//XCEPT_RAISE(xgi::exception::Exception, "missing user parameter");
		}
		//std::cout << "HYPERDAQ accepting " << uuid << std::endl;
		application_->repositoryLock_.take();
		if (user != "")
		{
			application_->alarmStores_[user].first->accept(uuid, "Unknwon");
		}
		else
		{
			std::map<std::string, std::pair<sentinel::arc::utils::AlarmStore*, toolbox::TimeVal> >::iterator i;
			for (i = application_->alarmStores_.begin(); i != application_->alarmStores_.end(); i++)
			{
				if ((*i).second.first->hasException(uuid))
				{
					(*i).second.first->accept(uuid, "Unknwon");
					break;
				}
			}
		}
        application_->repositoryLock_.give();
		//std::cout << "HYPERDAQ accept complete" << std::endl;

	}
	catch (sentinel::arc::utils::exception::FailedToStore &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (sentinel::arc::utils::exception::ConstraintViolated &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE(xgi::exception::Exception, msg.str());
	}
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "max-age=60");
}

void sentinel::arc::Interface::ruleon (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	try
	{
		std::string user = "";
		std::string type = "";

		toolbox::Properties rule;

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("identifier");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("identifier", (*fi).getValue());
		}

		fi = cgi.getElement("notifier");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("notifier", (*fi).getValue());
		}

		fi = cgi.getElement("severity");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("severity", (*fi).getValue());
		}

		fi = cgi.getElement("class");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("class", (*fi).getValue());
		}

		fi = cgi.getElement("tag");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("tag", (*fi).getValue());
		}

		fi = cgi.getElement("instance");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("instance", (*fi).getValue());
		}
		fi = cgi.getElement("unamespace");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("unamespace", (*fi).getValue());
		}
		fi = cgi.getElement("uvalue");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("uvalue", (*fi).getValue());
		}
		fi = cgi.getElement("args");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("args", (*fi).getValue());
		}
		fi = cgi.getElement("expiry");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("expiry", (*fi).getValue());
		}
		else
		{
			rule.setProperty("expiry", "PT1H");
		}
		fi = cgi.getElement("user");
		if (fi != cgi.getElements().end())
		{
			user = (*fi).getValue();
			if (user == "")
			{
				XCEPT_RAISE(xgi::exception::Exception, "user parameter not valid");
			}
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "missing user parameter");
		}
		fi = cgi.getElement("type");
		if (fi != cgi.getElements().end())
		{
			type = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "missing type parameter");
		}


		fi = cgi.getElement("message");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("message", (*fi).getValue());
		}
		fi = cgi.getElement("schema");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("qualifiedErrorSchemaURI", (*fi).getValue());
		}
		fi = cgi.getElement("version");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("version", (*fi).getValue());
		}
		fi = cgi.getElement("module");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("module", (*fi).getValue());
		}
		fi = cgi.getElement("line");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("line", (*fi).getValue());
		}
		fi = cgi.getElement("func");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("func", (*fi).getValue());
		}
		fi = cgi.getElement("lid");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("lid", (*fi).getValue());
		}
		fi = cgi.getElement("context");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("context", (*fi).getValue());
		}
		fi = cgi.getElement("groups");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("groups", (*fi).getValue());
		}
		fi = cgi.getElement("service");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("service", (*fi).getValue());
		}
		fi = cgi.getElement("zone");
		if (fi != cgi.getElements().end())
		{
			rule.setProperty("zone", (*fi).getValue());
		}

		toolbox::TimeVal endTime;

		if (rule.getProperty("expiry") == "")
		{
			endTime = toolbox::TimeVal::zero();
		}
		else
		{
			// convert expiry
			toolbox::TimeInterval intervalT;

			intervalT.fromString(rule.getProperty("expiry"));

			if ((double) intervalT == 0.0)
			{
				endTime = toolbox::TimeVal::zero();
			}
			else
			{
				endTime = toolbox::TimeVal::gettimeofday();
				endTime = endTime + intervalT;
			}
		}

		rule.setProperty("expiry", endTime.toString(toolbox::TimeVal::gmt));

		std::string authUser = in->getenv("REMOTE_USER");
		if (authUser == "")
		{
			authUser = "Unknown";
		}

		//std::vector<std::string> propertyNames();

		std::vector<std::string> propes = rule.propertyNames();
		for (std::vector<std::string>::iterator j = propes.begin(); j != propes.end(); j++)
		{
			//std::cout << "HYPERDAQ RULE : " << (*j) << " = " << rule.getProperty((*j)) << std::endl;
		}

		//std::cout << "user is " << authUser << std::endl;

		//std::cout << "HYPERDAQ adding rule " << type << ", expires " << rule.getProperty("expiry") << std::endl;

		if (user == "global")
		{
			std::list < std::string > stores = application_->getAlarmStoreNames();
			for (std::list<std::string>::iterator i = stores.begin(); i != stores.end(); i++)
			{
				application_->alarmStores_[(*i)].first->ruleon(rule, type, authUser);
			}
		}
		else
		{
			application_->alarmStores_[user].first->ruleon(rule, type, authUser);
		}
		//std::cout << "HYPERDAQ rule added" << std::endl;

	}
	catch (sentinel::arc::utils::exception::FailedToStore &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (sentinel::arc::utils::exception::ConstraintViolated &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE(xgi::exception::Exception, msg.str());
	}
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}
void sentinel::arc::Interface::ruleoff (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	try
	{
		std::string user = "";
		std::string type = "";
		std::string uuid = "";

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("uuid");
		if (fi != cgi.getElements().end())
		{
			uuid = (*fi).getValue();
		}

		fi = cgi.getElement("user");
		if (fi != cgi.getElements().end())
		{
			user = (*fi).getValue();
		}
		else
		{
			//XCEPT_RAISE(xgi::exception::Exception, "missing user parameter");
		}
		fi = cgi.getElement("type");
		if (fi != cgi.getElements().end())
		{
			type = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception, "missing type parameter");
		}

		//std::cout << "HYPERDAQ removing rule " << type << std::endl;

		std::string authUser = in->getenv("REMOTE_USER");
		if (authUser == "")
		{
			authUser = "Unknown";
		}

		if (user != "")
		{
			application_->alarmStores_[user].first->ruleoff(uuid, type, authUser);
		}
		else
		{
			std::string offName = type + "off";
			std::string onName = type + "on";

			std::map<std::string, std::pair<sentinel::arc::utils::AlarmStore*, toolbox::TimeVal> >::iterator i;
			for (i = application_->alarmStores_.begin(); i != application_->alarmStores_.end(); i++)
			{
				if ((*i).second.first->ruleOnExists(uuid, onName))
				{
					(*i).second.first->ruleoff(uuid, offName, authUser);
					break;
				}
			}
			if (i == application_->alarmStores_.end())
			{
				//std::cout << "failed to find rule" << std::endl;
			}
		}
		//std::cout << "HYPERDAQ rule removed" << std::endl;

	}
	catch (sentinel::arc::utils::exception::FailedToStore &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (sentinel::arc::utils::exception::ConstraintViolated &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE(xgi::exception::Exception, msg.str());
	}
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}
void sentinel::arc::Interface::repack (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	try
	{
		std::string uuid = "";

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("uuid");
		if (fi != cgi.getElements().end())
		{
			uuid = (*fi).getValue();
		}
		else
		{
			//XCEPT_RAISE(xgi::exception::Exception, "missing user parameter");
		}

		//std::cout << "HYPERDAQ re-pack uuid " << uuid << std::endl;
		toolbox::Properties ex = application_->model_->getException(uuid);
		application_->model_->repack(ex);
		//std::cout << "HYPERDAQ re-pack complete" << std::endl;

	}
	catch (sentinel::arc::utils::exception::FailedToStore &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (sentinel::arc::utils::exception::ConstraintViolated &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE(xgi::exception::Exception, msg.str());
	}
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}
