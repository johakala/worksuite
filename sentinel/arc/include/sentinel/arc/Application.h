// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_Application_h_
#define _sentinel_arc_Application_h_

#include <string>
#include <map>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/ApplicationContext.h"

#include "toolbox/ActionListener.h"
#include "toolbox/task/TimerListener.h"

#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"
#include "xdata/Properties.h"

#include "b2in/nub/Method.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"
#include "xgi/Utils.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"

#include "xplore/utils/DescriptorsCache.h"

/*
#include "sentinel/arc/AbstractHyperDAQ.h"
#include "sentinel/arc/DevHyperDAQ.h"
#include "sentinel/arc/HyperDAQ.h"

#include "sentinel/arc/Interface.h"
*/
#include "sentinel/exception/Exception.h"

#include "sentinel/arc/utils/Model.h"
#include "sentinel/arc/utils/AlarmStore.h"
#include "sentinel/arc/utils/AlarmStoreListener.h"
#include "sentinel/arc/utils/PolicyCache.h"
#include "jansson.h"

namespace sentinel
{
	namespace arc
	{
		class Application : public xdaq::Application, public xdata::ActionListener, public toolbox::task::TimerListener, public sentinel::arc::utils::AlarmStoreListener
		{
				//friend class sentinel::arc::HyperDAQ;
				//friend class sentinel::arc::DevHyperDAQ;
				//friend class sentinel::arc::Interface;
			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);

				~Application ();

				void actionPerformed (xdata::Event& e);

				void timeExpired (toolbox::task::TimerEvent& e);

				void lastStoredEventsCallback (sentinel::arc::utils::AlarmStore* alarmStore, std::vector<toolbox::Properties>& events);

				void initialize (toolbox::TimeVal initTime);

				sentinel::arc::utils::AlarmStore* getAlarmStore(std::string& key);

				std::list<std::string> getAlarmStoreNames();


				// restful
				void getNode (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void getException (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void getExceptionList (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void getModelTree (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void accept (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void clear (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

			protected:
				void getExceptionList (json_t * jsonvector, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap, const std::string & exceptionState);
				json_t * getNode (int & totalCount, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap);
				json_t * buildModelTree ( sentinel::arc::utils::Node * node);


			protected:
				void loadOCL(const std::string & fname);

			private:

				//sentinel::arc::AbstractHyperDAQ * hyperdaq_;
				//sentinel::arc::Interface * interface_;

				typedef std::pair<sentinel::arc::utils::AlarmStore*, toolbox::TimeVal> AlarmStoreDescriptor;

				std::map<std::string, AlarmStoreDescriptor> alarmStores_;

				//toolbox::BSem repositoryLock_; // used for db management operations

				sentinel::arc::utils::Model * model_;

				// Connection details for Oracle database
				class DatabaseInfo
				{
					public:

						void registerFields (xdata::Bag<DatabaseInfo>* bag)
						{
							bag->addField("databaseUser", &databaseUser_);
							bag->addField("databasePassword", &databasePassword_);
							bag->addField("databaseTnsName", &databaseTnsName_);
						}

						xdata::String databaseUser_;
						xdata::String databasePassword_;
						xdata::String databaseTnsName_;
				};

				xdata::Vector<xdata::Bag<DatabaseInfo> > databases_;

				xdata::UnsignedInteger scatterReadNum_;
				xdata::UnsignedInteger maxInitExceptionNum_;
				xdata::UnsignedInteger flashbackInitWindow_;

				xdata::String modelURL_;
				xdata::String severitiesURL_;
				xdata::Boolean adminMode_;
				xdata::String oclFileName_;
				xdata::String expireInterval_;
		};
	}
}
#endif
