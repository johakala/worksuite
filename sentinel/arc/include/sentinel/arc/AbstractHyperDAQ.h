// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_AbstractHyperDAQ_h_
#define _sentinel_arc_AbstractHyperDAQ_h_

namespace sentinel
{
	namespace arc
	{
		class AbstractHyperDAQ
		{
			public:
				virtual void enable() = 0;
		};
	}
}
#endif
