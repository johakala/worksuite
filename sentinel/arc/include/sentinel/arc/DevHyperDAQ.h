// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_DevHyperDAQ_h_
#define _sentinel_arc_DevHyperDAQ_h_

#include "xgi/framework/UIManager.h"
#include "sentinel/arc/HyperDAQ.h"
#include "sentinel/arc/utils/Node.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"
#include "xgi/Utils.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"

namespace sentinel
{
	namespace arc
	{
		class Application;

		class DevHyperDAQ : public HyperDAQ
		{
			public:

				DevHyperDAQ (sentinel::arc::Application * app);

				void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				void fire (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				void exceptionMakerTab (xgi::Output * out) throw (xgi::exception::Exception);
		};
	}
}
#endif
