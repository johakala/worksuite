// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_utils_AlarmStore_h_
#define _sentinel_arc_utils_AlarmStore_h_

#include "sentinel/arc/utils/AlarmStoreListener.h"

#include "xdaq/Application.h"

#include <occi.h>

#include "xgi/Method.h"

#include "sentinel/arc/utils/exception/ConstraintViolated.h"
#include "sentinel/arc/utils/exception/FailedToArchive.h"
#include "sentinel/arc/utils/exception/FailedToClose.h"
#include "sentinel/arc/utils/exception/FailedToOpen.h"
#include "sentinel/arc/utils/exception/FailedToRead.h"
#include "sentinel/arc/utils/exception/FailedToRemove.h"
#include "sentinel/arc/utils/exception/FailedToStore.h"
#include "sentinel/arc/utils/exception/NotFound.h"

#include "toolbox/Properties.h"
#include "toolbox/TimeVal.h"
#include "toolbox/BSem.h"
#include "toolbox/net/UUID.h"

namespace sentinel
{
	namespace arc
	{
		namespace utils
		{

			static const std::string EVENT_NULL_STR = "null";

			static const std::string EVENT_FIRE_STR = "fire";
			static const std::string EVENT_ACCEPT_STR = "accept";
			static const std::string EVENT_REVOKE_STR = "revoke";
			static const std::string EVENT_CLEAR_STR = "clear";
			/////
			static const std::string EVENT_CLEARON_STR = "clearon";
			static const std::string EVENT_CLEAROFF_STR = "clearoff";
			static const std::string EVENT_SHELFON_STR = "shelfon";
			static const std::string EVENT_SHELFOFF_STR = "shelfoff";
			static const std::string EVENT_DEBOUNCEON_STR = "debounceon";
			static const std::string EVENT_DEBOUNCEOFF_STR = "debounceoff";

			class AlarmStore
			{
				public:

					AlarmStore (xdaq::Application * application, const std::string& username, const std::string& password, const std::string& tnsname, bool readOnly, unsigned int scatterReadNum,
							unsigned int maxInitExceptionNum, unsigned int flashbackInitWindow) throw (sentinel::arc::utils::exception::FailedToOpen);
					~AlarmStore ();

					std::string getName();
					toolbox::TimeVal lastStoredEvents (sentinel::arc::utils::AlarmStoreListener * listener, toolbox::TimeVal & since) throw (sentinel::arc::utils::exception::NotFound);
					// initialization
					toolbox::TimeVal initialize (sentinel::arc::utils::AlarmStoreListener * listener, toolbox::TimeVal & since) throw (sentinel::arc::utils::exception::NotFound);
					//static void hasInsertedfirstRow (bool b);
					void readBlob (const std::string& zKey, std::string& pzBlob) throw (sentinel::arc::utils::exception::FailedToRead);
					/*! Check if database contain the exception for uuid
										 */
					bool hasException (const std::string& uuid);

					void accept (const std::string & exception, const std::string & source) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);
					void clear (const std::string & exception, const std::string & source) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);

				private:
					/*! Store an exception object
					 This function is thread safe (read/write lock)
					 */
					void fire (xcept::Exception& ex) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);
					//void fireNew (xcept::Exception& ex) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);
					void revoke (xcept::Exception& ex) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);


					void ruleon (toolbox::Properties& rule, const std::string& ruleType, const std::string & source) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);
					void ruleoff (const std::string & ruleUUID, const std::string & ruleType, const std::string & source) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);

					/*! Store an exception, all values are in the properties object
					 The function does not store a chained exception (blob)
					 */
					void storeNewException (toolbox::Properties& properties, const std::string & blob) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);
					void storeNewCatalogEntry (toolbox::Properties& properties) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);
					void storeNewXDAQEntry (const std::string& uniqueid, toolbox::Properties& properties) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);

					void storeNewEvent (const std::string & type, toolbox::Properties& properties) throw (sentinel::arc::utils::exception::FailedToStore, sentinel::arc::utils::exception::ConstraintViolated);



					std::pair<std::string, std::string> getLastEvent (const std::string& uuid, const std::string& time) throw (sentinel::arc::utils::exception::Exception); // returns fire, clear or null

					bool ruleOnExists (const std::string& ruleUUID, const std::string& ruleType) throw (sentinel::arc::utils::exception::Exception);

					/*! Retrieve a single exception by uuid in the format it has been stored.
					 The name of the format is returned in \param format.
					 */
					//void retrieve (const std::string& uuid, const std::string& format, xgi::Output* out) throw (sentinel::arc::utils::exception::NotFound);

					/*! Return a list of stored exceptions in the \param ex set that match the search query
					 This function is thread safe (read lock)*/
					void catalog (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal & end, const std::string& format) throw (sentinel::arc::utils::exception::NotFound);

					/*! Return a list of stored exceptions in the \param ex set that match the search query , order desc by dateTime
					 */
					void catalog (toolbox::TimeVal & age, int (*callback) (void*, int, char**, char**), void * context) throw (sentinel::arc::utils::exception::FailedToArchive);

					void events (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal & end, const std::string& format) throw (sentinel::arc::utils::exception::NotFound);


					void remove (toolbox::TimeVal & age) throw (sentinel::arc::utils::exception::FailedToRemove);

					/*! Retrieve the time at which the last exception has been stored from the database */
					toolbox::TimeVal getLatestStoreTime ();

					/*! Retrieve the time of the most recent event in the database */
					toolbox::TimeVal getLatestEventTime () throw (sentinel::arc::utils::exception::Exception);

					/*! Retrieve the time of the most recent exception in the database */
					toolbox::TimeVal getLatestExceptionTime () throw (sentinel::arc::utils::exception::Exception);

					/*! Retrieve the time of the oldest exception in the database */
					toolbox::TimeVal getOldestExceptionTime ();

					/*! Retrieve the number of exceptions stored */
					std::string getNumberOfExceptions () throw (sentinel::arc::utils::exception::Exception);

					size_t getSize ();

					/*! Average time to store a single exception */
					double getAverageTimeToStore ();

					/*! Average time to retrieve a catalog */
					double getAverageTimeToRetrieveCatalog ();

					/*! Average time to retrieve a single exception by uuid */
					double getAverageTimeToRetrieveException ();

					void writeBlob (const std::string & zKey, const std::string & zBlob) throw (sentinel::arc::utils::exception::FailedToStore);


					void query (xgi::Output* out, const std::string& query) throw (sentinel::arc::utils::exception::Exception);

					void lock ();

					void unlock ();

					// Vacuum the database, other operations TBD
					//
					void maintenance () throw (sentinel::arc::utils::exception::Exception);

					std::list<toolbox::Properties> getOpenArchivesInfo ();



				private:
					bool inCatalog (const std::string& zKey) throw (sentinel::arc::utils::exception::Exception);

					toolbox::TimeVal outputCatalog (sentinel::arc::utils::AlarmStoreListener * listener, oracle::occi::ResultSet* occiResult) throw (sentinel::arc::utils::exception::NotFound);

					// Use for coldspot query
					//toolbox::TimeVal outputCatalogJSONExtended (xgi::Output* out, oracle::occi::ResultSet* occiResult) throw (sentinel::arc::utils::exception::NotFound);

					void prepareDatabase (bool readOnly) throw (sentinel::arc::utils::exception::FailedToOpen, sentinel::arc::utils::exception::Exception);

					std::string escape (const std::string& s);

					toolbox::BSem mutex_; // used for db management operations

					double averageTimeToStore_;
					double averageTimeToRetrieveCatalog_;
					double averageTimeToRetrieveException_;
					toolbox::TimeVal lastExceptionTime_;
					toolbox::TimeVal maintenanceSinceTime_;

					unsigned int scatterReadNum_;
					unsigned int maxInitExceptionNum_; // maximum number of last occurred exception to be loaded in the model
					unsigned int  flashbackInitWindow_; // time window in seconds of last occurred exception

					oracle::occi::Environment* environment_;
					oracle::occi::Connection* connection_;

					oracle::occi::Statement* insertCatalogStmt_;
					oracle::occi::Statement* insertExceptionStmt_;
					oracle::occi::Statement* insertApplicationStmt_;
					oracle::occi::Statement* insertEventStmt_;

					/// NEW STATEMENTS

					oracle::occi::Statement* updateEventStmt_;

					oracle::occi::Statement* openRulesStmt_;
					oracle::occi::Statement* openExceptionsStmt_;

					xdaq::Application * application_;

					std::string name_;
			};
		}
	}
}
#endif
