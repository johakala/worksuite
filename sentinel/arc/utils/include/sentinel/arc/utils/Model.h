// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_utils_Model_h_
#define _sentinel_arc_utils_Model_h_

#include "sentinel/arc/utils/AbstractModel.h"
#include "sentinel/arc/utils/exception/Exception.h"
#include "sentinel/arc/utils/Node.h"

#include <xercesc/dom/DOM.hpp>

#include "toolbox/Properties.h"
#include "toolbox/BSem.h"
#include "sentinel/arc/utils/PolicyCache.h"

#include "sentinel/arc/utils/AlarmStore.h"
#include <queue>
#include "jansson.h"

XERCES_CPP_NAMESPACE_USE

namespace sentinel
{
	namespace arc
	{
		namespace utils
		{
			class Model : public sentinel::arc::utils::AbstractModel
			{
				public:

					Model (std::string modelURL, std::string severitiesURL, std::list<std::string>& alarmStoreNames) throw (sentinel::arc::utils::exception::Exception);



					void resetModel ();

					void repack ();

					void repack (toolbox::Properties & e);

					void fire (toolbox::Properties & e);

					void revoke (toolbox::Properties & e);

					void shelve (toolbox::Properties & e);

					void clear (toolbox::Properties & e);

					void accept (toolbox::Properties & e);

					void processEvent (sentinel::arc::utils::AlarmStore* alarmStore, toolbox::Properties & e);

					void purgeRuleCaches (toolbox::TimeVal& startTime);

					bool hasException (const std::string uuid);
					toolbox::Properties getException (const std::string uuid);
					std::map<std::string, toolbox::Properties> getExceptions ();

					size_t getSeverityLevel (std::string severity);

					std::string getSeverityMeaning (size_t severity);

					size_t getUniqueID ();

					std::map<std::string, size_t> getSeverities ();
					std::map<size_t, std::string, std::greater<size_t> > getSeveritiesOrdered ();



					//void setOtherNode (sentinel::arc::utils::Node* node);


					void printTree ();


					void checkShelfExpired ();


					void garbageCollection(unsigned int maxExceptionNum, const std::string & expireInterval);
					void garbage (const std::string & uuid);

					json_t* getExceptionList(int nodeid, std::set<std::string> & states);
					json_t * getModelTree ();
					json_t * getNodeInfo (size_t id);




				private:
					void erase (const std::string & uuid);
					void insert (toolbox::Properties & e);


					 // unsafe so make private for the moment
					std::map<std::string, sentinel::arc::utils::PolicyCache*>& getShelfRuleCache();
					std::map<std::string, sentinel::arc::utils::PolicyCache*>& getClearRuleCache();
					std::map<std::string, sentinel::arc::utils::PolicyCache*>& getDebounceRuleCache();

					void lock();
					void unlock();
					void printTree (sentinel::arc::utils::Node* node, size_t depth);
					void releaseTree (sentinel::arc::utils::Node* node);
					sentinel::arc::utils::Node * createTree (DOMNode * n, sentinel::arc::utils::Node * father);
					json_t * buildModelTree (sentinel::arc::utils::Node * node);
					void getExceptionList (json_t * jsonvector, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap, const std::string & exceptionState);
					json_t * getNode (int & totalCount, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap);

					Node* getRootNode ();
					Node* getNode (size_t id);
					Node* getOtherNode ();
					void addGuard (sentinel::arc::utils::Node * node);
									void indexNode (sentinel::arc::utils::Node * node);



					xdata::Properties buildRule (toolbox::Properties& event);

					std::map<std::string, sentinel::arc::utils::PolicyCache*> shelfRuleCache_;
					std::map<std::string, sentinel::arc::utils::PolicyCache*> clearRuleCache_;
					std::map<std::string, sentinel::arc::utils::PolicyCache*> debounceRuleCache_;

					toolbox::BSem mutex_; // used for db management operations

					std::string modelURL_;
					std::string severitiesURL_;

					std::map<std::string, size_t> severities_;
					std::map<size_t, std::string, std::greater<size_t> > severitiesOrdered_;

					//std::map<std::string, toolbox::Properties> exceptions_;
					std::map<std::string, toolbox::Properties> exceptions_;
					std::multimap<time_t,std::string> timeline_;


					std::queue<std::string> bufferedExceptions_; // fired exception uuid are put into a FIFO for garbage collection

					std::vector<sentinel::arc::utils::Node *> guards_;
					std::map<size_t, sentinel::arc::utils::Node *> nodes_;

					sentinel::arc::utils::Node * otherNode_;
					sentinel::arc::utils::Node * rootNode_;

					size_t uniqueID_;

			};
		}
	}
}
#endif
