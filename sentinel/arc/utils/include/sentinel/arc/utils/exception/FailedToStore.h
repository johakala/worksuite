// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_utils_exception_FailedToStore_h_
#define _sentinel_arc_utils_exception_FailedToStore_h_

#include "sentinel/arc/utils/exception/Exception.h"

namespace sentinel
{
	namespace arc
	{
		namespace utils
		{
			namespace exception
			{
				class FailedToStore : public sentinel::arc::utils::exception::Exception
				{
					public:
						FailedToStore (std::string name, std::string message, std::string module, int line, std::string function)
							: sentinel::arc::utils::exception::Exception(name, message, module, line, function)
						{
						}

						FailedToStore (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
							: sentinel::arc::utils::exception::Exception(name, message, module, line, function, e)
						{
						}

				};
			}
		}
	}
}

#endif
