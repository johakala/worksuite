// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_utils_version_h_
#define _sentinel_arc_utils_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define SENTINELARCUTILS_VERSION_MAJOR 2
#define SENTINELARCUTILS_VERSION_MINOR 0
#define SENTINELARCUTILS_VERSION_PATCH 0
// If any previous versions available E.g. #define SENTINELARCUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef SENTINELARCUTILS_PREVIOUS_VERSIONS

//
// Template macros
//
#define SENTINELARCUTILS_VERSION_CODE PACKAGE_VERSION_CODE(SENTINELARCUTILS_VERSION_MAJOR,SENTINELARCUTILS_VERSION_MINOR,SENTINELARCUTILS_VERSION_PATCH)
#ifndef SENTINELARCUTILS_PREVIOUS_VERSIONS
#define SENTINELARCUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(SENTINELARCUTILS_VERSION_MAJOR,SENTINELARCUTILS_VERSION_MINOR,SENTINELARCUTILS_VERSION_PATCH)
#else 
#define SENTINELARCUTILS_FULL_VERSION_LIST  SENTINELARCUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(SENTINELARCUTILS_VERSION_MAJOR,SENTINELARCUTILS_VERSION_MINOR,SENTINELARCUTILS_VERSION_PATCH)
#endif 

namespace sentinelarcutils
{
	const std::string package = "sentinelarcutils";
	const std::string versions = SENTINELARCUTILS_FULL_VERSION_LIST;
	const std::string summary = "Utilities for book-keeping and filtering of alarms and exceptions";
	const std::string description = "";
	const std::string authors = "Andrew Forrest, Luciano Orsini";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
