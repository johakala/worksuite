// $Id: $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "sentinel/arc/utils/PolicyCache.h"

#include <sstream>

#include "toolbox/Runtime.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"

#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"
#include "xoap/domutils.h"

#include "xcept/tools.h"

#include "toolbox/net/UUID.h"

sentinel::arc::utils::PolicyCache::PolicyCache ()
	: mutex_(toolbox::BSem::FULL, false)
{

}

// this doesnt care which rule applied, it just returns true/false if any rule applied
bool sentinel::arc::utils::PolicyCache::applyRules (toolbox::Properties& e, toolbox::TimeVal& current)
{
	std::string tmp;
	return this->applyRules(e, tmp, current);
}

// uuid is a return value of the rule that matched first for a given event
bool sentinel::arc::utils::PolicyCache::applyRules (toolbox::Properties& e, std::string& uuid, toolbox::TimeVal& current)
{
	std::set < std::string > expired;
	mutex_.take();
	//LOG4CPLUS_DEBUG(this->getApplicationLogger(), "check shelf rules");
	std::map<std::string, xdata::Properties>::iterator i;
	for (i = rules_.begin(); i != rules_.end(); ++i)
	{
		if (!this->ruleHasExipred((*i).second.getProperty("expiry"), current))
		{
			if (this->matchRule(e, (*i).second))
			{
				//std::cout << "Shelf rule matched" << std::endl;
				uuid = (*i).first;
				mutex_.give();
				return true;
			}
			else
			{
				//std::cout << "Rule has not expired, didnt match" << std::endl;
			}
		}
		else
		{
			//std::cout << "Rule has expired, adding to list" << std::endl;
			expired.insert((*i).first);
		}
	}

	for (std::set<std::string>::iterator i = expired.begin(); i != expired.end(); i++)
	{
		//std::cout << "removing rule" << std::endl;
		rules_.erase(*i);
	}

	mutex_.give();
	return false;
}

/*
 // PT0S means permanent rule
 bool sentinel::arc::utils::PolicyCache::ruleHasExipred (const std::string & start, const std::string & period)
 {
 if (period == "PT0S")
 {
 return false;
 }

 toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();

 toolbox::TimeVal startTime;
 startTime.fromString(start, "", toolbox::TimeVal::gmt);

 toolbox::TimeInterval interval;
 interval.fromString(period);

 if ((startTime + interval) < now)
 {
 //std::cout << "shelf rule has expired" << std::endl;
 return true;
 }
 //std::cout << "shelf rule is still active" << std::endl;
 return false;
 }
 */
// PT0S means permanent rule
bool sentinel::arc::utils::PolicyCache::ruleHasExipred (const std::string & end, toolbox::TimeVal& current)
{
	toolbox::TimeVal expiryT;
	expiryT.fromString(end, "", toolbox::TimeVal::gmt);

	if (expiryT == toolbox::TimeVal::zero())
	{
		return false;
	}

	if (expiryT < current)
	{
		//std::cout << "shelf rule has expired" << std::endl;
		return true;
	}
	//std::cout << "shelf rule is still active" << std::endl;
	return false;
}

bool sentinel::arc::utils::PolicyCache::matchRule (toolbox::Properties & e, std::map<std::string, std::string>& filter)
{
//std::cout << "apply rule" << std::endl;
	bool ret = false;
	std::map<std::string, std::string>::iterator i;
	for (i = filter.begin(); i != filter.end(); ++i)
	{
		try
		{
			//--

			std::string name = (*i).first;

			// we dont want to regex against the following fields
			if ((name == "start") || (name == "expiry") || (name == "debounce"))
			{
				continue;
			}

			/*
			 if ((name == "class") || (name == "instance") || (name == "lid") || (name == "context") || (name == "group") || (name == "service") || (name == "zone"))
			 {
			 std::string v = "urn:xdaq-application:";
			 v += name;
			 //name = v;
			 }
			 */

			// extract property from exception
			std::string value = e.getProperty(name);
			//std::cout << "check attribute:" << name << " regex:" << (*i).second << " on value: " << value << std::endl;
			if (!toolbox::regx_match_nocase(value, (*i).second))
			{
				//std::cout << " -> does not match" << std::endl;
				return false;
			}
			else
			{
				ret = true;
			}

		}
		catch (xdata::exception::Exception& e)
		{
			// doesn't match the column name
			return false;
		}
	}
//std::cout << " -> it matches!" << std::endl;
	return ret;
}

void sentinel::arc::utils::PolicyCache::loadRules (std::string fname)
{
	mutex_.take();
	std::vector < std::string > files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(fname);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand filter filename from '" << fname << "', ";
		std::cout << msg.str() << std::endl;
		mutex_.give();
		//LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		return;
	}

	try
	{
		//LOG4CPLUS_INFO(this->getApplicationLogger(), "Load shelf rules from '" << files[0] << "'");
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(files[0]);
		DOMNodeList* list = doc->getElementsByTagNameNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2014/sentinelarcshelfrules"), xoap::XStr("rule"));

		for (XMLSize_t j = 0; j < list->getLength(); j++)
		{
			//std::cout << "set rule:" << std::endl;
			DOMNode* node = list->item(j);
			DOMNamedNodeMap* attributes = node->getAttributes();
			xdata::Properties p;
			for (unsigned int i = 0; i < attributes->getLength(); i++)
			{
				std::string valueStr = xoap::XMLCh2String(attributes->item(i)->getNodeValue());
				std::string nameStr = xoap::XMLCh2String(attributes->item(i)->getNodeName());
				p.setProperty(nameStr, valueStr);
				//std::cout << "attribute" << nameStr << " regexp " << valueStr << std::endl;
			}

			// add start time, this should be set by the user or use current time
			if (!p.hasProperty("start"))
			{
				toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
				p.setProperty("start", now.toString(toolbox::TimeVal::gmt));
			}

			toolbox::net::UUID uuid;
			rules_[uuid.toString()] = p;
		}
		doc->release();
		//LOG4CPLUS_INFO(this->getApplicationLogger(), "Loaded shelf rule from '" << fname << "'");
	}
	catch (xoap::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to load shelf rules file from '" << fname << "'";
		//LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		std::cout << msg.str() << " : " << xcept::stdformat_exception_history(e) << std::endl;

	}
	mutex_.give();
}

void sentinel::arc::utils::PolicyCache::addRule (std::string& uuid, xdata::Properties& e)
{
	mutex_.take();
	//std::cout << "adding new rule" << std::endl;
	rules_[uuid] = e;
	mutex_.give();
}

void sentinel::arc::utils::PolicyCache::removeRule (std::string& uuid)
{
	mutex_.take();
	//std::cout << "removing rule" << std::endl;
	rules_.erase(uuid);
	mutex_.give();
}

std::map<std::string, xdata::Properties> sentinel::arc::utils::PolicyCache::getRules ()
{
	mutex_.take();
	std::map < std::string, xdata::Properties > ret = rules_;
	mutex_.give();

	return ret;
}

xdata::Properties sentinel::arc::utils::PolicyCache::getRule (const std::string& uuid)
{
	mutex_.take();
	xdata::Properties p = rules_[uuid];
	mutex_.give();

	return p;
}

void sentinel::arc::utils::PolicyCache::purge (toolbox::TimeVal& current)
{
	std::set < std::string > expired;
	mutex_.take();

	std::map<std::string, xdata::Properties>::iterator i;
	for (i = rules_.begin(); i != rules_.end(); ++i)
	{

		if (this->ruleHasExipred((*i).second.getProperty("expiry"), current))
		{
			expired.insert((*i).first);
		}
	}

	for (std::set<std::string>::iterator i = expired.begin(); i != expired.end(); i++)
	{
		//std::cout << "purging rule" << std::endl;
		rules_.erase(*i);
	}

	mutex_.give();
}
