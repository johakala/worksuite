// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "sentinel/spotlight2g/ExceptionEvent.h"
#include <string>
#include <sstream>
			
sentinel::spotlight2g::ExceptionEvent::ExceptionEvent( xcept::Exception* ex , const std::string & name):
	  toolbox::Event("urn:sentinel-spotlight:ExceptionEvent", 0), exception_(ex), name_(name)
{
}

sentinel::spotlight2g::ExceptionEvent::~ExceptionEvent()
{
	delete exception_;
}


std::string sentinel::spotlight2g::ExceptionEvent::name()
{
	return name_;
}

xcept::Exception* sentinel::spotlight2g::ExceptionEvent::getException()
{
	return exception_;
}
