// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                  *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/Runtime.h"
#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 
#include "xdaq/ApplicationDescriptorImpl.h"

#include "xoap/domutils.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xgi/framework/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"
#include "xoap/Event.h"
#include <xercesc/util/XMLURL.hpp>
#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"

 
#include "xgi/Table.h" 
#include "xcept/tools.h"

#include "sentinel/Sentinel.h"
#include "sentinel/spotlight2g/Application.h"
#include "sentinel/spotlight2g/ExceptionEvent.h"
#include "sentinel/spotlight2g/RevokeEvent.h"


#include "xdata/exdr/Serializer.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/Table.h"

#include "xdaq/XceptSerializer.h"

XDAQ_INSTANTIATOR_IMPL(sentinel::spotlight2g::Application);

sentinel::spotlight2g::Application::Application(xdaq::ApplicationStub* s)  throw (xdaq::exception::Exception) 
	: xdaq::Application(s), xgi::framework::UIManager(this),
  	dispatcher_("urn:xdaq-workloop:spotlight","waiting", 0.8),
	repository_(0)
{	
	s->getDescriptor()->setAttribute("icon", "/sentinel/spotlight2g/images/spotlight2g-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/sentinel/spotlight2g/images/spotlight2g-icon.png");
	s->getDescriptor()->setAttribute("service", "sentinelspotlight2g");

	subscribeGroup_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeGroup", &subscribeGroup_);
	
	exceptionsTimeWindow_ = "PT60M"; // Keep one hour per file
	maintenanceInterval_ = "PT30M"; // Archive every 30 minutes
	archiveWindow_ = "PT60M"; // new archive file name date-hour  
	jelFileName_ = "";
	
	this->getApplicationInfoSpace()->fireItemAvailable("exceptionsTimeWindow", &exceptionsTimeWindow_);
	this->getApplicationInfoSpace()->fireItemAvailable("maintenanceInterval", &maintenanceInterval_);
	this->getApplicationInfoSpace()->fireItemAvailable("archiveWindow", &archiveWindow_);
	this->getApplicationInfoSpace()->fireItemAvailable("jelFileName", &jelFileName_);

   // Activates work loop for dashboard asynchronous operations (SOAP messages)
        dispatcher_.addActionListener(this);
        (void) toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:spotlight", "waiting")->activate();
	
	repositoryPath_ = "/tmp";
	this->getApplicationInfoSpace()->fireItemAvailable("repositoryPath",&repositoryPath_);	


        // Which flashlist tags to retrieve from the ws-eventing
        topic_ = "sentinel";
        this->getApplicationInfoSpace()->fireItemAvailable("topic", &topic_);

        scanPeriod_ = "PT10S";
        this->getApplicationInfoSpace()->fireItemAvailable("scanPeriod",&scanPeriod_);
        subscribeExpiration_ = "PT30S";
        this->getApplicationInfoSpace()->fireItemAvailable("subscribeExpiration",&subscribeExpiration_);

	 // bind SOAP interface for incoming 'notify' messages containing exceptions
	 //
  	b2in::nub::bind(this, &sentinel::spotlight2g::Application::onMessage );

	
	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this,  &sentinel::spotlight2g::Application::Default, "Default");
	xgi::bind(this, &sentinel::spotlight2g::Application::rearm, "rearm");

	// Retrieves a complete exception. View takes two parameter
	// 'uniqueid' is the uuid of the exception to be retrieved
	// 'format' indicates the result format. The default is 'html', also 'xml' is supported.
	//
	xgi::bind(this, &sentinel::spotlight2g::Application::view, "view");
	
	// Clear all exceptions, cleanup and close files
	xgi::bind(this, &sentinel::spotlight2g::Application::reset, "reset");
	
	// DB operation
	xgi::bind(this, &sentinel::spotlight2g::Application::files, "files");
			
	// Generate a test exception
	xgi::bind(this, &sentinel::spotlight2g::Application::generate, "generate");
	xgi::bind(this, &sentinel::spotlight2g::Application::inject, "inject");
	
	// List takes two parameters: 
	// 'timeBack' indicates how much bach in the the retrieve list should go
	// 'maxResults' indicates the maximum number of exceptions to be retrieved
	//
	xgi::bind(this, &sentinel::spotlight2g::Application::catalog, "catalog");
	
	// lastStoredEvents?start=<LAST STORE TIME>
	// retrieve all exceptions using storeTime field between <LAST STORE TIME> and now.
	// Return also the newest exception storeTime in the result set.
	// If start=EMPTY, return the newest exception in the database
	//
	xgi::bind(this, &sentinel::spotlight2g::Application::lastStoredEvents, "lastStoredEvents");
	xgi::bind(this, &sentinel::spotlight2g::Application::events, "events");
	
	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	
	exceptionsLostCounter_ = 0;
}

sentinel::spotlight2g::Application::~Application()
{

}

void sentinel::spotlight2g::Application::inject(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
                                
        try                     
        {
		std::string identifier = "toolbox::exception::Test";
		std::string notifier   = "http://lxcmd113.cern.ch:9920/urn:xdaq-application:lid=10";
		std::string severity   = "error";
		std::string className   = "xmas::sensor2g::Application";
		std::string tag   = "";
		std::string instance   = "";
		std::string unamespace   = "urn:jobcontrol:jid";
		std::string uvalue   = "34";
		std::string group =  this->getApplicationDescriptor()->getAttribute("group");

                cgicc::Cgicc cgi(in);
                cgicc::const_form_iterator fi = cgi.getElement("identifier");
                if (fi != cgi.getElements().end())
                {               
                        identifier = (*fi).getValue();
                }               
                                
                fi = cgi.getElement("notifier");                
                if (fi != cgi.getElements().end())
                {               
                        notifier = (*fi).getValue();
                }


                fi = cgi.getElement("severity");
                if (fi != cgi.getElements().end())
                {
                        severity = (*fi).getValue();
                }

                fi = cgi.getElement("class");
                if (fi != cgi.getElements().end())
                {
                        className = (*fi).getValue();
                }

                fi = cgi.getElement("tag");
                if (fi != cgi.getElements().end())
                {
                        tag = (*fi).getValue();
                }

                fi = cgi.getElement("instance");
                if (fi != cgi.getElements().end())
                {
                        instance = (*fi).getValue();
                }
                fi = cgi.getElement("unamespace");
                if (fi != cgi.getElements().end())
                {
                        unamespace = (*fi).getValue();
                }
                fi = cgi.getElement("uvalue");
                if (fi != cgi.getElements().end())
                {
                        uvalue = (*fi).getValue();
                }
		
		fi = cgi.getElement("group");
                if (fi != cgi.getElements().end())
                {
                        group = (*fi).getValue();
                }

  		xcept::Exception exception1 ( "toolbox::exception::TestLowLevel", "low level",  __FILE__, __LINE__, __FUNCTION__);
                xcept::Exception exception2(  "xoap::exception::TestMidLevel", "mid level",  __FILE__, __LINE__, __FUNCTION__, exception1);
                xcept::Exception exception3(  identifier, "this is an exception", __FILE__, __LINE__, __FUNCTION__, exception2);

                exception3.setProperty("notifier",notifier);
                exception3.setProperty("severity",severity);
                exception3.setProperty("urn:xdaq-application:class",className);
                exception3.setProperty("tag",tag);
                exception3.setProperty("urn:xdaq-application:instance",instance);
                exception3.setProperty(unamespace,uvalue);
		exception3.setProperty("urn:xdaq-application:group",group);

                repository_->store(exception3);

        }
        catch (std::exception &se)
        {
                std::stringstream msg;
                msg << "Failed to process 'view' request, error: " << se.what();
                XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}
}


void sentinel::spotlight2g::Application::generate(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	static int count = 0;

	if (repository_ == 0)
	{
		XCEPT_RAISE (xgi::exception::Exception, "Failed to generate test exceptions, repository is null");
	}

	try
	{
		for (size_t j = 0; j < 100; ++j)
		{
			//toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
			for (size_t i = 0; i < 1000; ++i)
			{
				++count;

				std::stringstream msg;
				msg << "This is exception number " << count;

				xcept::Exception exception1 ( "toolbox::exception::TestLowLevel", "low level",  __FILE__, __LINE__, __FUNCTION__);
				xcept::Exception exception2(  "xoap::exception::TestMidLevel", "mid level",  __FILE__, __LINE__, __FUNCTION__, exception1);
				xcept::Exception exception3(  "xdaq::exception::TestHighLevel", msg.str(), __FILE__, __LINE__, __FUNCTION__, exception2); 
				exception3.setProperty("notifier","http://lxcmd113.cern.ch:9920/urn:xdaq-application:lid=10");	
				exception3.setProperty("severity","error");
				exception3.setProperty("urn:xdaq-application:class","xmas::sensor2g::Application");
				exception3.setProperty("tag","fu");
				exception3.setProperty("urn:xdaq-application:instance","34");

				repository_->store(exception3);


				xcept::Exception exception4 ( "toolbox::exception::TestHighevel", "high level",  __FILE__, __LINE__, __FUNCTION__);
				exception4.setProperty("urn:tracker-fed:instance","56");
				exception4.setProperty("urn:jobcontrol:jid","5600");
                                xcept::Exception exception5(  "xoap::exception::TestMidLevel", "mid level",  __FILE__, __LINE__, __FUNCTION__, exception4);
                                exception5.setProperty("notifier","http://lxcmd113.cern.ch:9920/urn:xdaq-application:lid=10");
                                exception5.setProperty("severity","alarm");
                                exception5.setProperty("class","xmas::las2g::Application");
				exception5.setProperty("tag","");
				exception5.setProperty("urn:xdaq-application:instance","56");
				exception5.setProperty("urn:tracker-fed:instance","56");
				exception5.setProperty("urn:jobcontrol:jid","5600");

                                repository_->store(exception5);

				::sleep(2);
			}

			//std::cout << std::endl;
			//toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();

			//std::cout << "Total: " << ((double)(stop-start)) << " sec" << std::endl;
			//std::cout << "Rate: " << 1/(((double)(stop-start))/1000.0) << " Hz" << std::endl;
			
		}
	}
	catch (sentinel::spotlight2g::exception::Exception& e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		XCEPT_RETHROW (xgi::exception::Exception, "Test failed", e);
	}
}

//
// Infoapace listener
//
void sentinel::spotlight2g::Application::actionPerformed( xdata::Event& event) 
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{		
		 b2inEventingProxy_ = new b2in::utils::ServiceProxy(this,"b2in-eventing",subscribeGroup_.toString(),this);

		try
		{
			toolbox::TimeInterval archiveWindow;
                        archiveWindow.fromString(archiveWindow_);

			repository_ = new sentinel::spotlight2g::Repository(this,repositoryPath_.toString(),archiveWindow); // open for current write and read usage
		}
		catch( sentinel::spotlight2g::exception::Exception & e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			return;
		}
		
		try
		{
			if (!toolbox::task::getTimerFactory()->hasTimer("spotlight"))
			{
				(void) toolbox::task::getTimerFactory()->createTimer("spotlight");
			} 
			toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer("spotlight");
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();

			// submit archive task
			toolbox::TimeInterval interval;
			interval.fromString(maintenanceInterval_);
			
			LOG4CPLUS_INFO (this->getApplicationLogger(), "Schedule immediate archivation at startup");
			timer->schedule (this, start, 0, "archive-staging-now") ;
			
			LOG4CPLUS_INFO (this->getApplicationLogger(), "Schedule spotlight archivation timer for " << interval.toString());
			timer->scheduleAtFixedRate( start, this, interval, 0, "archive-staging" );

     			// submit scan task
                	toolbox::TimeInterval interval2;
                	interval2.fromString(scanPeriod_);
			LOG4CPLUS_INFO (this->getApplicationLogger(), "Schedule discovery timer for " << interval2.toString());
                	timer->scheduleAtFixedRate( start, this, interval2, 0, "discovery-staging" );

		}
		catch (toolbox::task::exception::InvalidListener& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::InvalidSubmission& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::NotActive& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		catch (toolbox::exception::Exception& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
		//
		if ( jelFileName_.toString() != "" ) // for backward compatibility if paramater not specified
		{
			this->loadJEL(jelFileName_.toString());
		}
	}
	else
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}
void sentinel::spotlight2g::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist)
        throw (b2in::nub::exception::Exception)
{
	std::string name = plist.getProperty("urn:sentinel-event:name");
	if( name != "revoke" && name != "notify" )
	{
		// unexpected message
		if(msg != 0) msg->release();
		return;
	}

	//TODO error handling
 	xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) msg->getDataLocation(), msg->getDataSize());         
	xcept::Exception* newException = new xcept::Exception();
	xdaq::XceptSerializer::importFrom (&inBuffer, *newException);

	toolbox::Event * event;
	if ( name == "revoke")
	{
		event = new  sentinel::spotlight2g::RevokeEvent(newException,"");
	}
	else /* if (name == "notify" ) */
	{
		event = new  sentinel::spotlight2g::ExceptionEvent(newException,"");
	}
	
	 // process exception in a separate thread, to release the HTTP emebedded server
	
	if(msg != 0)  msg->release();

	toolbox::task::EventReference e(event);
	try
	{
        	dispatcher_.fireEvent (e);
	}
	catch(toolbox::task::exception::Overflow & e )
	{
        	exceptionsLostCounter_++;
        	std::stringstream msg;
        	msg << "Exceptions lost: " << exceptionsLostCounter_  << ", " << e.what() ;
        	LOG4CPLUS_FATAL  (this->getApplicationLogger(), msg.str());
       	
	}
	catch(toolbox::task::exception::OverThreshold & e )
	{
        	// message is lost, keep counting
        	exceptionsLostCounter_++;
	}
	catch(toolbox::task::exception::InternalError & e )
	{
        	// dead band ignore
        	exceptionsLostCounter_++;
        	std::stringstream msg;
        	msg << "Exceptions lost: " << exceptionsLostCounter_  << ", " << e.what() ;
        	LOG4CPLUS_FATAL  (this->getApplicationLogger(), msg.str());
	}                               
}

void sentinel::spotlight2g::Application::asynchronousExceptionNotification(xcept::Exception& e)
{
	//TODO
}

void sentinel::spotlight2g::Application::refreshSubscriptionsToEventing()
	throw (sentinel::exception::Exception)
{
        if (subscriptions_.empty())
        {
                LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Prepare subscription records");
                const xdaq::Network * network = 0;
                std::string networkName =  this->getApplicationDescriptor()->getAttribute("network");
                try
                {
                        network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
                }
                catch (xdaq::exception::NoNetwork& nn)
                {
                        std::stringstream msg;
                        msg << "Failed to access b2in network " << networkName << ", not configured";
                        XCEPT_RETHROW (sentinel::exception::Exception, msg.str(), nn);
                }
                pt::Address::Reference localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());


                std::set<std::string> topics = toolbox::parseTokenSet ( topic_.toString(), "," );
                std::set<std::string>::iterator i;
                for (i = topics.begin(); i != topics.end(); ++i)
                {
                        if ( subscriptions_.find(*i) == subscriptions_.end())
                        {
                                xdata::Properties plist;
                                toolbox::net::UUID identifier;
                                plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching

                                plist.setProperty("urn:b2in-eventing:action", "subscribe");
                                plist.setProperty("urn:b2in-eventing:id", identifier.toString());
                                plist.setProperty("urn:b2in-eventing:topic", (*i)); // Subscribe to collected data
                                plist.setProperty("urn:b2in-eventing:subscriberurl", localAddress->toString());
                                plist.setProperty("urn:b2in-eventing:subscriberservice", "sentinelspotlight2g");
                                plist.setProperty("urn:b2in-eventing:expires", subscribeExpiration_.toString("xs:duration"));
                                subscriptions_[(*i)] = plist;
                        }
                }
        }

        LOG4CPLUS_DEBUG (this->getApplicationLogger(), "subscribe/Renew");
        b2in::utils::MessengerCache * messengerCache = 0;
        try
        {
                messengerCache = b2inEventingProxy_->getMessengerCache();
        }
        catch(b2in::utils::exception::Exception & e)
        {
                        XCEPT_RETHROW (sentinel::exception::Exception, "cannot access messenger cache", e);
        }

	// This lock is available all the time excepted when an asynchronous send will fail and a cache invalidate is performed
        // Therefore there is no loss of efficiency.
        //

        std::list<std::string> destinations = messengerCache->getDestinations();

        for (std::list<std::string>::iterator j = destinations.begin(); j != destinations.end(); j++ )
        {
                //
                // Subscribe to OR Renew all existing subscriptions
                //
                std::map<std::string, xdata::Properties>::iterator i;
                for ( i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
                {
                        try
                        {
                                // plist is already prepared for a subscribe/resubscribe message
                                //
                                LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Subscribe to topic " << (*i).first << " on url " << (*j));
                                messengerCache->send((*j),0,(*i).second);
                        }
                        catch (b2in::nub::exception::InternalError & e)
                        {
                                std::stringstream msg;
                                msg << "failed to send subscription for topic (internal error) " << (*i).first << " to url " << (*j);

                                LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());

                                XCEPT_DECLARE_NESTED(sentinel::exception::Exception, ex , msg.str(), e);
                                this->notifyQualified("fatal",ex);
                                return;
                        }
                        catch ( b2in::nub::exception::QueueFull & e )
                        {
                                std::stringstream msg;
                                msg << "failed to send subscription for topic (queue full) " << (*i).first << " to url " << (*j);

                                LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());

                                XCEPT_DECLARE_NESTED(sentinel::exception::Exception, ex , msg.str(), e);
                                this->notifyQualified("error",ex);
                                return;
                        }
                        catch ( b2in::nub::exception::OverThreshold & e)
                        {
                                // ignore just count to avoid verbosity                                
                                std::stringstream msg;
                                msg << "failed to send subscription for topic (over threshold) " << (*i).first << " to url " << (*j);
                                LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
                                return;
                        }
                }
                LOG4CPLUS_DEBUG(this->getApplicationLogger(), "done with subscription");
        }
}

void sentinel::spotlight2g::Application::timeExpired(toolbox::task::TimerEvent& e)
{
        std::string name =  e.getTimerTask()->name;

	if (name == "discovery-staging")
	{
		try
        	{
                	b2inEventingProxy_->scan();
        	}
        	catch (b2in::utils::exception::Exception& e)
        	{
                	LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
        	}
	
	
        	try
        	{
                	this->refreshSubscriptionsToEventing();
        	}
        	catch (sentinel::exception::Exception& e)
        	{
                	LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
        	}
	
	}
        else if ( (name == "archive-staging-now") || (name == "archive-staging") )
	{
        // archive event
		// If we are in maintenance phase and the repository_ is NULL, just return
		if (repository_)
		{
			try
			{
				LOG4CPLUS_INFO (this->getApplicationLogger(), "Time for archiving");
				toolbox::TimeInterval window;
				window.fromString(exceptionsTimeWindow_);
				repository_->archive(window);
			}
			catch(sentinel::spotlight2g::exception::FailedToArchive & e)
			{
				LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			}
		}
	}
}

//
// XGI SUpport
//
void sentinel::spotlight2g::Application::view(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	if (!repository_)
	{
		// If we are in maintenance phase, raise an exception
		XCEPT_RAISE (xgi::exception::Exception, "Repository temporarily unavailable.");
	}
	
	std::string uniqueid;
	std::string datetime = "";
	std::string format = "json"; // by default output json format
	std::string dbname = ""; // by default operate on the main database
	
	try
        {
                cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("uniqueid");
		if (fi != cgi.getElements().end())
		{
			uniqueid = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE (xgi::exception::Exception, "Missing 'uniqueid' parameter");
		}

		fi = cgi.getElement("datetime");		
		if (fi != cgi.getElements().end())
                {
                        datetime = (*fi).getValue();
                }

		
		fi = cgi.getElement("fmt");		
		if (fi != cgi.getElements().end())
		{
			format = (*fi).getValue();
		}
		
		fi = cgi.getElement("dbname");		
		if (fi != cgi.getElements().end())
		{
			dbname = (*fi).getValue();
		}
	}
	catch (toolbox::exception::Exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW(xgi::exception::Exception, "invalid time",e);

	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'view' request, error: " << se.what();
		XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}
		
	if (format == "json")
	{		
		try
		{
			repository_->retrieve(uniqueid, datetime, format, out);
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
		}
		catch (sentinel::spotlight2g::exception::NotFound &e)
		{
			std::stringstream msg;
			msg << "Failed to retrieve exception '" << uniqueid << "'";
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
			XCEPT_RETHROW (xgi::exception::Exception, msg.str(), e);
		}
	}	
}

void sentinel::spotlight2g::Application::catalog(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::string format = "json"; // by default output json format
	std::string dbname = ""; // by default, operate on the main database

	// By default, retrieve errors from the last 1 hour
	toolbox::TimeInterval window (3600,0);
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday() - window;
	toolbox::TimeVal end = toolbox::TimeVal::gettimeofday();

	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi;
		
		fi = cgi.getElement("start");
		if (fi != cgi.getElements().end())
		{
			start.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		fi = cgi.getElement("end");
		if (fi != cgi.getElements().end())
		{
			end.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		// By default limit the maximum number of results to output to 1000
		/*
		fi = cgi.getElement("maxResults");
		size_t maxResults = 1000;	
		if (fi != cgi.getElements().end())
		{
			maxResults = (*fi).getIntegerValue();
		}
		*/
		
		fi = cgi.getElement("fmt");		
		if (fi != cgi.getElements().end())
		{
			format = (*fi).getValue();
		}
		
	}
	catch (toolbox::exception::Exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW(xgi::exception::Exception, "invalid time",e);

	}
	catch (const std::exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
	
	try
	{
		repository_->catalog (out, start, end, format);		
	}
	catch (sentinel::spotlight2g::exception::NotFound& e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to retrieve data from repository", e);
	}
}

void sentinel::spotlight2g::Application::lastStoredEvents(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::string format = "json"; // by default output json format
	std::string dbname = ""; // by default, operate on the main database

	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();

	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi;
		
		fi = cgi.getElement("start");
		if (fi != cgi.getElements().end())
		{
			start.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		fi = cgi.getElement("fmt");		
		if (fi != cgi.getElements().end())
		{
			format = (*fi).getValue();
		}
		
	}
	catch (toolbox::exception::Exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW(xgi::exception::Exception, "invalid time",e);

	}
	catch (const std::exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
	
	try
	{
		// This function will return based on storeTime everything that is younger than 'start'
		// If there is nothing, return an empty data set, but always return the youngest store time
		//
		repository_->lastStoredEvents (out, start, format);		
	}
	catch (sentinel::spotlight2g::exception::NotFound& e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to retrieve data from repository", e);
	}
}
//--
void sentinel::spotlight2g::Application::events(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::string format = "json"; // by default output json format
	std::string dbname = ""; // by default, operate on the main database

	// By default, retrieve errors from the last 1 hour
	toolbox::TimeInterval window (3600,0);
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday() - window;
	toolbox::TimeVal end = toolbox::TimeVal::gettimeofday();

	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi;
		
		fi = cgi.getElement("start");
		if (fi != cgi.getElements().end())
		{
			start.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		fi = cgi.getElement("end");
		if (fi != cgi.getElements().end())
		{
			end.fromString((*fi).getValue(),"",toolbox::TimeVal::gmt);
		}
		
		// By default limit the maximum number of results to output to 1000
		/*
		fi = cgi.getElement("maxResults");
		size_t maxResults = 1000;	
		if (fi != cgi.getElements().end())
		{
			maxResults = (*fi).getIntegerValue();
		}
		*/
		
		fi = cgi.getElement("fmt");		
		if (fi != cgi.getElements().end())
		{
			format = (*fi).getValue();
		}
		
	}
	catch (toolbox::exception::Exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RETHROW(xgi::exception::Exception, "invalid time",e);

	}
	catch (const std::exception & e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
                XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
	
	try
	{
		repository_->events (out, start, end, format);		
	}
	catch (sentinel::spotlight2g::exception::NotFound& e)
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to retrieve data from repository", e);
	}
}

//--
void sentinel::spotlight2g::Application::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	this->mainPage(in,out);
}



void sentinel::spotlight2g::Application::files(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	std::string fmt = "json"; // default
	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("fmt");
		if (fi != cgi.getElements().end())
		{
			fmt = (*fi).getValue();
		}
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'databases' request, error: " << se.what();
		XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}
		
	std::vector<std::string> files = repository_->getFiles();
	std::vector<std::string>::iterator i = files.begin();
	*out << "[";
	do
	{
		*out << "\"" << (*i) << "\"";
		++i;
		if (i != files.end())
		{
			*out << ",";
		}
	} while (i != files.end());
	*out << "]";
}


void sentinel::spotlight2g::Application::rearm(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	std::string exception = ""; // default
	std::vector<cgicc::FormEntry> exceptions; // list of exceptions to be rearmed
	std::string user = "unknown";
	 
	try
	{
		cgicc::Cgicc cgi(in);
		
		const cgicc::CgiEnvironment& env = cgi.getEnvironment();
		user = env.getRemoteUser();
				
		cgi.getElement("exception", exceptions);
				
		if(exceptions.empty()) 
		{
		     XCEPT_RAISE (xgi::exception::Exception,"Missing exception(uuid) for rearm");
		}
		
		/* cgicc::const_form_iterator fi = cgi.getElement("exception");
		if (fi != cgi.getElements().end())
		{
			exception = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE (xgi::exception::Exception,"Missing exception(uuid) for rearm");
		}
		*/
		
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process rearm request, error: " << se.what();
		XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}

	try
	{
		if (repository_)
		{
			for(std::string::size_type i = 0; i < exceptions.size(); ++i) 
			{
				exception = exceptions[i].getValue();
				repository_->rearm(exception, user); 
			}			
		}
		else
		{
			XCEPT_RAISE (xgi::exception::Exception,"Failed to rearm exception, repository not initialized");
		}
	}
	catch (sentinel::spotlight2g::exception::FailedToStore& st)
	{
		std::stringstream msg;
		msg << "Failed to rearm exception '" << exception << "'";
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(),st);
	}
}

void sentinel::spotlight2g::Application::reset(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	LOG4CPLUS_INFO (this->getApplicationLogger(), "Performing repository reset");
	repository_->reset();
	LOG4CPLUS_INFO (this->getApplicationLogger(), "Reset finished");	

	this->Default(in, out);
}

// Load JEL
void sentinel::spotlight2g::Application::loadJEL(const std::string & fname)
{
 		try
                {
                        LOG4CPLUS_INFO (this->getApplicationLogger(), "Load JEL from '" << fname << "'");
                        DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML( fname );
			DOMNodeList* list = doc->getElementsByTagNameNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2010/spotlight"),xoap::XStr("rule") );

                	for (XMLSize_t j = 0; j < list->getLength(); j++)
                	{
				//std::cout << "set rule:" << std::endl;
                       		 DOMNode* node = list->item(j);
                      		  DOMNamedNodeMap* attributes = node->getAttributes();
                       		 xdata::Properties p;
                       		 for (unsigned int i = 0; i < attributes->getLength(); i++ )
                       	 	{
                               		 std::string valueStr  = xoap::XMLCh2String( attributes->item(i)->getNodeValue() );
                                	std::string nameStr  = xoap::XMLCh2String( attributes->item(i)->getNodeName() );
                                	p.setProperty(nameStr,valueStr);
					//std::cout << "attribute" << nameStr << " regexp " << valueStr << std::endl;
                        	}
                        	jel_.push_back(p);
                	}
                        doc->release();
                        LOG4CPLUS_INFO (this->getApplicationLogger(), "Loaded JEL from '" << fname << "'");
                }
                catch (xoap::exception::Exception& e)
                {
                        std::stringstream msg;
                        msg << "Failed to load configuration file from '" << fname << "'";
                        LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));

                }


}

// apply Junk Exception List
bool sentinel::spotlight2g::Application::applyJEL( xcept::Exception & e)
{
	
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "check JEL rules");
	size_t labelCounter = 0;
	std::list<xdata::Properties>::iterator i;
	std::string name = e.name();
        for (i = jel_.begin(); i != jel_.end(); ++i)
        { 
		std::stringstream label;
		label << "r" << labelCounter;	
	
		if ( this->match(e,(*i)) )
		{
			// junk exception counter
			if ( jec_.find(name) != jec_.end() )
			{
				if ( jec_[name].find(label.str()) != jec_[name].end())
				{
					jec_[name][label.str()] += 1;
				}
				else
				{
					jec_[name][label.str()] = 1;
				}
			}
			else
			{
				jec_[name][label.str()] = 1;

			}
			return true;
		}
		labelCounter++;
	}

	return false;
}

std::string sentinel::spotlight2g::Application::mapToAttributeName(const std::string & name)
{
	if ((name  == "class") || ( name == "instance") || ( name == "lid") || ( name == "context") 
		|| ( name == "group") || ( name == "service") || ( name == "zone") )
        {
		std::string v = "urn:xdaq-application:";
		v += name;
                return  v;
        }
	return name;
}

bool sentinel::spotlight2g::Application::match 
(
	xcept::Exception & e,
        std::map<std::string, std::string>& filter
)
{
        // Algorithm requires that all filter expressions match!
        //

	//std::cout << "apply rule" << std::endl;
        std::map<std::string, std::string>::iterator i;
        for (i = filter.begin(); i != filter.end(); ++i)
        {       
                try
                {
			//--

			std::string name = this->mapToAttributeName((*i).first);
			//--
			// extract property from exception
			std::string value = e.getProperty(name);
			//std::cout << "check attribute:" << name << " regex:" << (*i).second << " on value: " << value;
                        if ( ! toolbox::regx_match_nocase(value, (*i).second ) )
                        {
				//std::cout << " -> does not match" << std::endl;
                                return false;
                        }
			
                }
                catch (xdata::exception::Exception& e)
                {
                        // doesn't match the column name
                        return false;
                }
        }
	//std::cout << " -> it matches!" << std::endl;
        return true;
}       

void sentinel::spotlight2g::Application::actionPerformed( toolbox::Event& event) 
{
 /* xcept::Exception* newException = new xcept::Exception();
  sentinel::utils::Serializer::importFrom (command, *newException);
  toolbox::task::EventReference e(new sentinel::spotlight2g::ExceptionEvent(newException,""));
*/

	if (event.type() == "urn:sentinel-spotlight:ExceptionEvent") 
	{
		// Synchronous store operation
		//
		sentinel::spotlight2g::ExceptionEvent& e = dynamic_cast<sentinel::spotlight2g::ExceptionEvent&>(event);

		// Apply antispam filter
		if ( jel_.size() != 0  && this->applyJEL(*(e.getException()) ))
		{
			// discard exception 
			//std::cout << "Discard junk exception" << std::endl;
			return;
		}	
		
		// If the DB has not yet been opened or created, open it
		try
		{
			if (repository_)
			{
				repository_->store(*(e.getException())); // After use, event and exception go away automatically
			}
			else
			{
				// Repository not initialized
				LOG4CPLUS_FATAL (this->getApplicationLogger(), "Failed to store excpetion, repository not initialized");
			}
		}
		catch (sentinel::spotlight2g::exception::FailedToStore& st)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(st));
		}
	}
	else if (event.type() == "urn:sentinel-spotlight:RevokeEvent") 
	{
		// Synchronous store operation
		//
		sentinel::spotlight2g::RevokeEvent& e = dynamic_cast<sentinel::spotlight2g::RevokeEvent&>(event);
		// If the DB has not yet been opened or created, open it
		try
		{
			if (repository_)
			{
				repository_->revoke(*(e.getException())); // After use, event and exception go away automatically
			}
			else
			{
				// Repository not initialized
				LOG4CPLUS_FATAL (this->getApplicationLogger(), "Failed to revoke exception, repository not initialized");
			}
		}
		catch (sentinel::spotlight2g::exception::FailedToStore& st)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(st));
		}

	}
	
}


void sentinel::spotlight2g::Application::mainPage
(
	xgi::Input * in, 
	xgi::Output * out
) throw (xgi::exception::Exception)
{
        
	// Begin of tabs
        *out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
  
  	// Tab 1      
        *out << "<div class=\"xdaq-tab\" title=\"Hotspot\">" << std::endl;
        this->HotspotTabPage(out);
        *out << "</div>";
        
        // Tab 2
        *out << "<div class=\"xdaq-tab\"  title=\"Statistics\">" << std::endl;
        this->StatisticsTabPage(out);
        *out << "</div>";
        
        // Tab 3
        *out << "<div class=\"xdaq-tab\"  title=\"JEL\">" << std::endl;
        this->JELTabPage(out);
        *out << "</div>";

        *out << "</div>"; // end of tab pane
}

void sentinel::spotlight2g::Application::JELTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;") << std::endl;

	// Output the header line with the column names
	//
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Name") << std::endl;
	*out << cgicc::th("Matching Rules") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	for (std::map<std::string, std::map<std::string, size_t> >::iterator i = jec_.begin(); i != jec_.end(); i++)
	{
		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*i).first) << std::endl;
		std::stringstream counters;
		for (std::map<std::string, size_t>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			counters << (*j).first << "(" << (*j).second << ") ";
		}
		*out << cgicc::td(counters.str()) << std::endl;
		*out << cgicc::tr() << std::endl;

	}
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

}
void sentinel::spotlight2g::Application::StatisticsTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	if (repository_ == 0)
	{
		// Instead of raising, display an error message in framework - This is only triggered by a web page access anyway
		//XCEPT_RAISE(xgi::exception::Exception, "Repository unavailable.");
		XCEPT_DECLARE(xgi::exception::Exception, e, "Repository unavailable");
		*out << xcept::htmlformat_exception_history(e) << std::endl;
		return;
	}
	/*! Output summary page with the following information
	 
	 State of database file (opened/closed)
	 Path and filename to database
	 Last exception stored
	 Number of exceptions stored in file
	 average time to store an exception
	 theoretical average rate to store exceptions
	 average time to retrieve catalog
	 theoretical averate rate to retrieve catalog
	 average time to retrieve a single exception by uuid
	 theoretical averate rate to retrieve single exceptions by uuid
	 */

	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;") << std::endl;

	// Output the header line with the column names
	//
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("File") << std::endl;
	*out << cgicc::th("Latest Exception") << std::endl;
	*out << cgicc::th("Oldest Exception") << std::endl;
	*out << cgicc::th("#Exceptions") << std::endl;
	*out << cgicc::th("Size") << std::endl;
	*out << cgicc::th("read exception time") << std::endl;
	*out << cgicc::th("read catalog time") << std::endl;
	*out << cgicc::th("write exception time") << std::endl;
	*out << cgicc::th("read exception rate") << std::endl;
	*out << cgicc::th("read catalog rate") << std::endl;
	*out << cgicc::th("write exception rate") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	std::list < toolbox::Properties > plist = repository_->getOpenArchivesInfo();
	for (std::list<toolbox::Properties>::iterator i = plist.begin(); i != plist.end(); i++)
	{
		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*i).getProperty("filename")) << std::endl;
		*out << cgicc::td((*i).getProperty("latest")) << std::endl;
		*out << cgicc::td((*i).getProperty("oldest")) << std::endl;
		*out << cgicc::td((*i).getProperty("count")) << std::endl;
		*out << cgicc::td((*i).getProperty("size")) << std::endl;
		*out << cgicc::td((*i).getProperty("readexceptiontime")) << std::endl;
		*out << cgicc::td((*i).getProperty("readcatalogtime")) << std::endl;
		*out << cgicc::td((*i).getProperty("writeexceptiontime")) << std::endl;
		*out << cgicc::td((*i).getProperty("readexceptionrate")) << std::endl;
		*out << cgicc::td((*i).getProperty("readcatalograte")) << std::endl;
		*out << cgicc::td((*i).getProperty("writeexceptionrate")) << std::endl;
		*out << cgicc::tr() << std::endl;

	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << cgicc::br() << std::endl;
	*out << "Average archive time: " << repository_->getAverageArchiveTime() << " seconds" << std::endl;

	//
	// -- END PAGE CONTENT

}

void sentinel::spotlight2g::Application::HotspotTabPage (xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string zone = this->getApplicationContext()->getDefaultZoneName();

	std::stringstream to;
	to << this->getApplicationDescriptor()->getContextDescriptor()->getURL();

	//std::string url = toolbox::escape(to.str());
	std::string url = cgicc::form_urlencode(to.str());

	std::string model = cgicc::form_urlencode("https://xdaq.web.cern.ch/xdaq/setup/12/defaultmodel.xml");
	//*out << "<a href=\"https://xdaq.web.cern.ch/xdaq/xmas/12/hotspot/hotspot.swf?confPath=" << url << "&autoStart=true\" target=\"_blank\">Hotspot Flex Interface Test</a>";
	*out << "<a href=\"https://xdaq.web.cern.ch/xdaq/xmas/12/hotspot/hotspot.swf?spotlighturl=" << url << "&sysmodelurl=" << model << "\" target=\"_blank\"><button><br/><img src=\"/sentinel/images/hotspot-icon.png\" style=\"height: 80px;\"><br />Hotspot Flex Interface<br/><br/></button></a>";
}
