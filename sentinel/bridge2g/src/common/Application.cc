// $Id: Application.cc,v 1.23 2009/04/06 13:44:55 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "sentinel/bridge2g/Application.h"
#include "sentinel/utils/Serializer.h"
#include "sentinel/utils/Alarm.h"
#include "sentinel/utils/NewsEvent.h"
#include "sentinel/Sentinel.h"

#include "toolbox/stl.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/task/exception/InvalidListener.h"
#include "toolbox/task/exception/NotActive.h"
#include "toolbox/task/exception/InvalidSubmission.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "pt/PeerTransportAgent.h"
#include "pt/SOAPMessenger.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/Event.h"

#include "xcept/tools.h"

#include "xplore/Interface.h"
#include "xplore/DiscoveryEvent.h"
#include "xplore/exception/Exception.h"

#include  "xgi/Input.h"
#include  "xgi/Output.h"

#include "xgi/Utils.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"

#include "b2in/nub/Method.h" 
#include "xgi/framework/Method.h" 

XDAQ_INSTANTIATOR_IMPL(sentinel::bridge2g::Application)

sentinel::bridge2g::Application::Application(xdaq::ApplicationStub * s)  throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this),  dialup_(0)
{
	s->getDescriptor()->setAttribute("icon", "/sentinel/bridge2g/images/sentinel-bridge2g-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/sentinel/bridge2g/images/sentinel-bridge2g-icon.png");
	// override default service name
	//getApplicationDescriptor()->setAttribute("service", "sentinelbridge2g");

	brokerWatchdog_ = "PT30S"; // 30 seconds bys default
	committedPoolSize_ = 0x100000 * 50; // 50 MB
	maxExceptionMessageSize_ = 0x10000; // 64KB
	publishGroup_ = "eventing";
	brokerURL_ = "";
	eventingURL_ = "";

	this->getApplicationInfoSpace()->fireItemAvailable("brokerWatchdog",          &brokerWatchdog_);
	// Broker configuration
	this->getApplicationInfoSpace()->fireItemAvailable("brokerURL",               &brokerURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("publishGroup",            &publishGroup_);
	// Static eventing configuration
	this->getApplicationInfoSpace()->fireItemAvailable("eventingURL",             &eventingURL_);

	// General configuration parameters
	this->getApplicationInfoSpace()->fireItemAvailable("maxExceptionMessageSize", &maxExceptionMessageSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("committedPoolSize",       &committedPoolSize_);

	xgi::framework::deferredbind(this, this,  &sentinel::bridge2g::Application::Default, "Default");

	// SOAP binding
	xoap::bind(this, &sentinel::bridge2g::Application::notify, "notify",  sentinel::NamespaceUri );
	xoap::bind(this, &sentinel::bridge2g::Application::revoke, "revoke",  sentinel::NamespaceUri );

	// B2IN binding
	b2in::nub::bind(this, &sentinel::bridge2g::Application::onMessage );

	// Listen to events indicating the setting of the application's default values
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationContext()->addActionListener(this); // attach to endpoint available events
}

sentinel::bridge2g::Application::~Application()
{
}

void sentinel::bridge2g::Application::actionPerformed(toolbox::Event& e)
{
	if ( e.type() == "xdaq::EndpointAvailableEvent" )
	{
		toolbox::TimeInterval interval;
		try
		{
			interval.fromString(brokerWatchdog_.toString()); 
		}		
		catch (toolbox::exception::Exception& e)
		{
			// Failed to parse the watchdog time string
			LOG4CPLUS_FATAL (this->getApplicationLogger(), "Failed to parse brokerWatchdog property '" << brokerWatchdog_.toString() << "'");
		}

		// If the available endpoint is a b2in endpoint we are ready to
		// discover other b2in endpoints on the network
		//
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
		
		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
		const xdaq::Network* network = ie.getNetwork();
		
		LOG4CPLUS_INFO (this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());
		
		if (network->getName() == networkName)
		{
			dialup_ = new b2in::utils::EventingDialup (this, publishGroup_, brokerURL_, eventingURL_, networkName, interval);
		}
	}
}


void sentinel::bridge2g::Application::actionPerformed (xdata::Event& e) 
{ 
	if ( e.type() == "urn:xdaq-event:setDefaultValues" )
	{
		try 
		{
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			toolbox::net::URN urn("sentinel", "bridge2g");
			pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to create b2in/sensor memory pool for size " << committedPoolSize_.toString();
			LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());

		}
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to process unknown event type '" << e.type() << "'";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), msg.str());
	}
}

void sentinel::bridge2g::Application::publishEvent (const std::string & type, xcept::Exception& e)
	throw (sentinel::bridge2g::exception::Exception)
{
	if ( dialup_ == 0 )
	{
		statistics_.incrementCommunicationLossCounter();
		return;
	}


	toolbox::net::URL at(getApplicationContext()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN());
	
	xdata::Properties plist;
	plist.setProperty("urn:b2in-protocol:service", "b2in-eventing");
	plist.setProperty("urn:b2in-eventing:topic", "sentinel");
	plist.setProperty("urn:b2in-eventing:action", "notify");
	plist.setProperty("urn:sentinel-event:name", type);

	toolbox::mem::Reference* ref = 0;
	try
	{
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxExceptionMessageSize_);
	}
	catch (toolbox::mem::exception::Exception & ex )
	{
		statistics_.incrementMemoryLossCounter();
		XCEPT_RETHROW(sentinel::bridge2g::exception::Exception, "Failed to allocate messaage for monitor report", e);
	}

	try
	{
		xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*)ref->getDataLocation(), maxExceptionMessageSize_);
		xdaq::XceptSerializer::writeTo (e, &outBuffer);
		ref->setDataSize(outBuffer.tellp());
	}
	catch(xdata::exception::Exception & e)
	{
		statistics_.incrementInternalLossCounter();
		ref->release();
		XCEPT_RETHROW(sentinel::bridge2g::exception::Exception, "Failed to serialize exception", e);
	}

	try
	{
		dialup_->send(ref, plist);
	}
	catch (b2in::utils::exception::Exception & e )
	{
		statistics_.getCommunicationLossCounter();
		ref->release();
		XCEPT_RETHROW(sentinel::bridge2g::exception::Exception, "Failed to publish exception to " +  dialup_->getEventingURL(), e);
	}
}


// Hyperdaq

void sentinel::bridge2g::Application::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;
	
	// Tabbed pages
	*out << "<div class=\"xdaq-tab\" title=\"Statistics\"  id=\"tabPage1\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void sentinel::bridge2g::Application::StatisticsTabPage(xgi::Output * out) throw (xgi::exception::Exception)
{
	//Dialup

	*out << cgicc::table().set("class","xdaq-table-vertical");
	*out << cgicc::caption("Eventing Dialup");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td().set("style","min-width:100px;");
	if(dialup_ != 0)
	{
		*out << dialup_->getStateName();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << brokerURL_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getBrokerLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Eventing URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Eventing address";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getEventingURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Exceptions sent
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total outgoing reports";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Exceptions lost
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total reports lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 


	// Statistics

	*out << cgicc::table().set("class","xdaq-table-vertical");
	*out << cgicc::caption("Statistics");
	*out << cgicc::tbody() << std::endl;

	// Fire Counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Fired";
	*out << cgicc::th();
	*out << cgicc::td().set("style","min-width:100px;");
	*out << statistics_.getFireCounter();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Internal Loss Counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Internal loss";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << statistics_.getInternalLossCounter();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Enqueuing Loss Counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Communication loss";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << statistics_.getCommunicationLossCounter();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Out of Memory Loss Counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Out of memory loss";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << statistics_.getMemoryLossCounter();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 
}

void sentinel::bridge2g::Application::onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) 
	throw (b2in::nub::exception::Exception)
{
	std::string action = plist.getProperty("urn:xmasbroker2g:action");
	if ( action == "allocate")
	{
		std::string model = plist.getProperty("urn:xmasbroker2g:model");
		if( model == "eventing"  && dialup_ != 0)
		{
			dialup_->onMessage(msg,plist);
		}
		else
		{
			LOG4CPLUS_INFO(this->getApplicationLogger(), "unknown broker response: " << model);
		}
	}	
	if (msg != 0) msg->release();
}

xoap::MessageReference sentinel::bridge2g::Application::notify (xoap::MessageReference msg) throw(xoap::exception::Exception)
{
	// Extract info for reply
	DOMNodeList* bodyList = msg->getSOAPPart().getEnvelope().getBody().getDOMNode()->getChildNodes();
	std::string namespaceURI = "";
	std::string namespacePrefix = "";
	//std::string commandName = "";
	for (XMLSize_t i = 0; i < bodyList->getLength(); i++) 
	{
		DOMNode* command = bodyList->item(i);
		
		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			namespaceURI = xoap::XMLCh2String (command->getNamespaceURI());
			namespacePrefix = xoap::XMLCh2String (command->getPrefix());
			//commandName = xoap::XMLCh2String (command->getLocalName());
			
			// process exception in a separate thread, to release the HTTP emebedded server
			xcept::Exception ex;
			xdaq::XceptSerializer::importFrom (command, ex);

			try
			{
				this->publishEvent("notify", ex);
			}
			catch(sentinel::bridge2g::exception::Exception & e)
			{
				XCEPT_RETHROW(xoap::exception::Exception, "Failed to forward exception", e);
			}
		}
	}
	
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("notifyResponse", namespacePrefix, namespaceURI);
	b.addBodyElement ( responseName );
	return reply;	
}

xoap::MessageReference sentinel::bridge2g::Application::revoke (xoap::MessageReference msg) throw(xoap::exception::Exception)
{
	// Extract info for reply
	DOMNodeList* bodyList = msg->getSOAPPart().getEnvelope().getBody().getDOMNode()->getChildNodes();
	std::string namespaceURI = "";
	std::string namespacePrefix = "";
	//std::string commandName = "";
	for (XMLSize_t i = 0; i < bodyList->getLength(); i++) 
	{
		DOMNode* command = bodyList->item(i);
		
		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			namespaceURI = xoap::XMLCh2String (command->getNamespaceURI());
			namespacePrefix = xoap::XMLCh2String (command->getPrefix());
			//commandName = xoap::XMLCh2String (command->getLocalName());
			
			// process exception in a separate thread, to release the HTTP emebedded server
			xcept::Exception ex;
			xdaq::XceptSerializer::importFrom (command, ex);

			try
			{
				this->publishEvent("revoke", ex);
			}
			catch(sentinel::bridge2g::exception::Exception & e)
			{
				XCEPT_RETHROW(xoap::exception::Exception, "Failed to forward exception", e);
			}
		}
	}
	
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName("revokeResponse", namespacePrefix, namespaceURI);
	b.addBodyElement ( responseName );
	return reply;	
}

