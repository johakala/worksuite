// $Id: $

/*************************************************************************
 * XDAQ Sentinel Daemon		               								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#include <sstream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "sentinel/sentineld/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"

#include "xoap/DOMParserFactory.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

XDAQ_INSTANTIATOR_IMPL (sentinel::sentineld::Application);

XERCES_CPP_NAMESPACE_USE

sentinel::sentineld::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this)
{
	dialup_ = 0;

	brokerDialupWatchdog_ = "PT30S";
	eventingURL_ = "";

	this->getApplicationInfoSpace()->fireItemAvailable("brokerDialupWatchdog", &brokerDialupWatchdog_);
	this->getApplicationInfoSpace()->fireItemAvailable("eventingURL", &eventingURL_);

	b2in::nub::bind(this, &sentinel::sentineld::Application::onMessage);

	publishGroup_ = "eventing";

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/sentinel/sentineld/images/sentineld-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/sentinel/sentineld/images/sentineld-icon.png");

	this->getApplicationInfoSpace()->fireItemAvailable("publishGroup", &publishGroup_);

	brokerGroup_ = "";
	brokerURL_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("brokerURL", &brokerURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("brokerGroup", &brokerGroup_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &sentinel::sentineld::Application::Default, "Default");
	xgi::bind(this, &sentinel::sentineld::Application::inject, "inject");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);
}

sentinel::sentineld::Application::~Application ()
{

}

void sentinel::sentineld::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	/*
	 std::string timerTaskName = e.getTimerTask()->name;
	 if (timerTaskName == "scan")
	 {

	 }
	 */
}

void sentinel::sentineld::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) throw (b2in::nub::exception::Exception)
{
	//LOG4CPLUS_INFO(this->getApplicationLogger(), "Received message");
	if (plist.getProperty("urn:b2in-eventing:topic") == "sentinel")
	{
		 std::string severity = toolbox::tolower(plist.getProperty("severity"));
		 plist.setProperty("severity", severity);
		// forward message , ne need to free it
		this->publishException(msg, plist);
	}
	else
	{
		std::string action = plist.getProperty("urn:xmasbroker2g:action");
		if (action == "allocate")
		{
			std::string model = plist.getProperty("urn:xmasbroker2g:model");
			if (model == "eventing" && dialup_ != 0)
			{
				dialup_->onMessage(msg, plist);
			}
			else
			{
				LOG4CPLUS_INFO(this->getApplicationLogger(), "unknown broker response: " << model);
			}
		}
		if (msg != 0) msg->release();
	}
}

void sentinel::sentineld::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		//std::cout << "Service name is '" << this->getApplicationDescriptor()->getAttribute("service") << "'" << std::endl;

		/*
		 toolbox::task::Timer * timer = 0;

		 if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:sensord-timer"))
		 {
		 timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:sensord-timer");
		 }
		 else
		 {
		 timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:sensord-timer");
		 }

		 toolbox::TimeInterval interval;

		 int delay = (rand() % 14) + 1;
		 toolbox::TimeVal start(toolbox::TimeVal::gettimeofday().sec() + delay);
		 interval.fromString(heartbeatWatchdog_);

		 timer->scheduleAtFixedRate(start, this, interval, 0, "scan");
		 */
	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(sentinel::sentineld::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

void sentinel::sentineld::Application::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "xdaq::EndpointAvailableEvent")
	{
		toolbox::TimeInterval intervalDialup;
		toolbox::TimeInterval intervalTopics;
		try
		{
			intervalDialup.fromString(brokerDialupWatchdog_.toString());
		}
		catch (toolbox::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to parse the watchdog brokerDialupWatchdog parameter '" << brokerDialupWatchdog_.toString() << "', ";
			XCEPT_DECLARE_NESTED(sentinel::sentineld::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);
		}

		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");

		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(event);
		const xdaq::Network* network = ie.getNetwork();

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());

		if (network->getName() == networkName)
		{

			dialup_ = new b2in::utils::EventingDialup(this, publishGroup_, brokerURL_, eventingURL_, networkName, intervalDialup);
		}
	}
}

void sentinel::sentineld::Application::publishException (toolbox::mem::Reference * msg, xdata::Properties & plist)
{

	std::string name = plist.getProperty("urn:sentinel-exception:identifier");
	counters_[name].incrementPulseCounter();

	if (dialup_ == 0)
	{
		counters_[name].incrementCommunicationLossCounter();
		msg->release();
		return;
	}

	// override properties forward to b2in-eventing 
	plist.setProperty("urn:b2in-protocol:service", "b2in-eventing");
	plist.setProperty("urn:b2in-eventing:action", "notify");

	try
	{
		dialup_->send(msg, plist);
		counters_[name].incrementFireCounter();
		return;
	}
	catch (b2in::utils::exception::Exception & e)
	{
		counters_[name].incrementInternalLossCounter();
		msg->release();
		return;
	}

}

//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void sentinel::sentineld::Application::TabPanel (xgi::Output * out)
{

	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\" id=\"tabPage2\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void sentinel::sentineld::Application::StatisticsTabPage (xgi::Output * out)
{
	//Dialup

	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Eventing Dialup");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");
	if (dialup_ != 0)
	{
		*out << dialup_->getStateName();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerLostCounter();
	}	
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Eventing URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Eventing address";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getEventingURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports sent
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total outgoing reports";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports lost
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total reports lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
}

void sentinel::sentineld::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{

	this->TabPanel(out);

}

void sentinel::sentineld::Application::inject(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	/*
	try
	{
		std::string identifier = "toolbox::exception::Test";
		std::string notifier   = "http://lxcmd113.cern.ch:9920/urn:xdaq-application:lid=10";
		std::string severity   = "error";
		std::string className   = "xmas::sensor2g::Application";
		std::string tag   = "";
		std::string instance   = "";
		std::string unamespace   = "urn:jobcontrol:jid";
		std::string uvalue   = "34";
		std::string group =  this->getApplicationDescriptor()->getAttribute("group");

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("identifier");
		if (fi != cgi.getElements().end())
		{
			identifier = (*fi).getValue();
		}

		fi = cgi.getElement("notifier");
		if (fi != cgi.getElements().end())
		{
			notifier = (*fi).getValue();
		}

		fi = cgi.getElement("severity");
		if (fi != cgi.getElements().end())
		{
			severity = (*fi).getValue();
		}

		fi = cgi.getElement("class");
		if (fi != cgi.getElements().end())
		{
			className = (*fi).getValue();
		}

		fi = cgi.getElement("tag");
		if (fi != cgi.getElements().end())
		{
			tag = (*fi).getValue();
		}

		fi = cgi.getElement("instance");
		if (fi != cgi.getElements().end())
		{
			instance = (*fi).getValue();
		}

		fi = cgi.getElement("unamespace");
		if (fi != cgi.getElements().end())
		{
			unamespace = (*fi).getValue();
		}

		fi = cgi.getElement("uvalue");
		if (fi != cgi.getElements().end())
		{
			uvalue = (*fi).getValue();
		}

		fi = cgi.getElement("group");
                if (fi != cgi.getElements().end())
                {
                        group = (*fi).getValue();
                }


		xcept::Exception exception1 ( "toolbox::exception::TestLowLevel", "low level",  __FILE__, __LINE__, __FUNCTION__);
		xcept::Exception exception2(  "xoap::exception::TestMidLevel", "mid level",  __FILE__, __LINE__, __FUNCTION__, exception1);
		xcept::Exception exception3(  identifier, "this is an exception", __FILE__, __LINE__, __FUNCTION__, exception2);

		//exception3.setProperty("notifier",notifier);
		exception3.setProperty("severity",severity);
		exception3.setProperty("urn:xdaq-application:class",className);
		exception3.setProperty("tag",tag);
		exception3.setProperty("urn:xdaq-application:instance",instance);
		exception3.setProperty(unamespace,uvalue);
		exception3.setProperty("urn:xdaq-application:group",group);

		this->handleException(exception3, 0);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}
	*/
}
