#!/bin/sh
# usage: raise host port identifier severity message tag
# e.g. ./revoke.sh http://srv-c2d06-17.cms:1972/urn:xdaq-application:lid=1000  myAlarm
curl  $1/revoke?identifier=$2
echo ""
