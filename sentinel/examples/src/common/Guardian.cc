// $Id: Guardian.cc,v 1.10 2008/07/18 15:27:24 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "Guardian.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h"

#include "xcept/tools.h"
#include "toolbox/utils.h"
#include "xdaq/exception/ApplicationNotFound.h"
//
// provides factory method for instantion of Guardian  application
//
XDAQ_INSTANTIATOR_IMPL(Guardian)

Guardian::Guardian(xdaq::ApplicationStub * s): xdaq::WebApplication(s)
{	
	sentinel_ = 0;
	// retrieve the Sentinel
	try
	{
		sentinel_ = dynamic_cast<sentinel::Interface*>(getApplicationContext()->getFirstApplication("Sentinel"));
		sentinel_->join(this);
		// add this class as a listener to the sentinel to intercept incoming exceptions
		sentinel_->setListener(this,this);
		// Will handle exception in the group 'guardian' an exception handling context
		sentinel_->attachContext("guardian", this);
	}
	catch(xdaq::exception::ApplicationNotFound & e)
	{
		LOG4CPLUS_WARN(getApplicationLogger(), "no sentinel found, remote notification not enable");
	}
	catch(sentinel::exception::Exception & se)
	{
		LOG4CPLUS_WARN(getApplicationLogger(), se.what());
	}
	//export a Web interface for easy self test command
	xgi::bind(this,&Guardian::selfTest, "selfTest");
}

Guardian::~Guardian()
{
	

}

void  Guardian::selfTest(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	try
	{
		if ( sentinel_ != 0 )
		{
			sentinel_->pushContext("guardian", this);
		}
		else
		{
			XCEPT_RAISE (xgi::exception::Exception, "Failed to remotly notify Exception, no sentinel");
		}
		
		xcept::Exception exception1 ( "xcept::Exception", "error one has occurred",  __FILE__, __LINE__, __FUNCTION__);
		xcept::Exception exception2(  "xcept::Exception", "error two has occurred",  __FILE__, __LINE__, __FUNCTION__, exception1);
		xcept::Exception exception3(  "xcept::Exception", "error three has occured", __FILE__, __LINE__, __FUNCTION__, exception2);	
	
		std::string notifier = toolbox::toString("%s/%s", getApplicationDescriptor()->getContextDescriptor()->getURL().c_str() , getApplicationDescriptor()->getURN().c_str()) ;
		// Fill last exception according to schema
		// Mandatory fields
		exception3.setProperty("notifier", notifier );
		exception3.setProperty("qualifiedErrorSchemaURI", "http://xdaq.web.cern.ch/xdaq/xsd/2005/QualifiedSoftwareErrorRecord-10.xsd");
		exception3.setProperty("dateTime", toolbox::getDateTime() );
		exception3.setProperty("sessionID", "none" );
		exception3.setProperty("severity", "ERROR" );
			
		sentinel_->notify(exception3, this);		
	} 
	catch (sentinel::exception::Exception& e)
	{
		XCEPT_RETHROW (xgi::exception::Exception, "Failed to remotly notify Exception", e);
	}
}

void  Guardian::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
}
		
void Guardian::onException(xcept::Exception& e)
{
	std::cout << "Recived exception:" << std::endl;
	
	std::cout << xcept::stdformat_exception_history(e) << std::endl;
	
	std::cout << std::endl << "Exception details:" << std::endl;
	
	xcept::ExceptionHistory history(e);
	
	while ( history.hasMore() )
	{
		xcept::ExceptionInformation & info = history.getPrevious();
		std::map<std::string, std::string, std::less<std::string> > & properties = info.getProperties();
		for  (std::map<std::string, std::string, std::less<std::string> >::iterator j = properties.begin(); j != properties.end(); j++)
		{
			std::cout << (*j).first << " = " << (*j).second << std::endl;
		}
		
		std::cout << std::endl;		
	}	
}	


