/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: R. Moser and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _sentinel_probe_version_h_
#define _sentinel_probe_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define SENTINELPROBE_VERSION_MAJOR 2
#define SENTINELPROBE_VERSION_MINOR 0
#define SENTINELPROBE_VERSION_PATCH 1
// If any previous versions available E.g. #define SENTINELPROBE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define SENTINELPROBE_PREVIOUS_VERSIONS "2.0.0"


//
// Template macros
//
#define SENTINELPROBE_VERSION_CODE PACKAGE_VERSION_CODE(SENTINELPROBE_VERSION_MAJOR,SENTINELPROBE_VERSION_MINOR,SENTINELPROBE_VERSION_PATCH)
#ifndef SENTINELPROBE_PREVIOUS_VERSIONS
#define SENTINELPROBE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(SENTINELPROBE_VERSION_MAJOR,SENTINELPROBE_VERSION_MINOR,SENTINELPROBE_VERSION_PATCH)
#else 
#define SENTINELPROBE_FULL_VERSION_LIST  SENTINELPROBE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(SENTINELPROBE_VERSION_MAJOR,SENTINELPROBE_VERSION_MINOR,SENTINELPROBE_VERSION_PATCH)
#endif 

namespace sentinelprobe
{
	const std::string package  =  "sentinelprobe";
	const std::string versions =  SENTINELPROBE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ Monitoring and Alarming System probe for sentineld";
	const std::string description = "";
	const std::string authors = "Andy Forrest, Luciano Orsini";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

