// $Id: $

/*************************************************************************
 * XDAQ Sentinel Probe		               								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _sentinel_probe_Application_h_
#define _sentinel_probe_Application_h_

#include <string>
#include <stack>
#include <set>
#include <map>

#include "xdata/Boolean.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Double.h"
#include "xdata/TimeVal.h"
#include "xdata/ActionListener.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/ContextTable.h"
#include "xdaq/ApplicationContext.h" 
#include "xdaq/ApplicationDescriptorImpl.h"

#include "xgi/framework/UIManager.h"

#include "toolbox/ActionListener.h"
#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerFactory.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"

#include "b2in/nub/exception/Exception.h"
#include "b2in/utils/MessengerCache.h"

#include "b2in/utils/EventingDialup.h"

#include "sentinel/probe/exception/Exception.h"

namespace sentinel
{
	namespace probe
	{
		class Application : public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener, public b2in::utils::MessengerCacheListener
		{
			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);

				~Application ();

				/*! Callback to be implemented when discovery service comes back with a xplore::DiscoveryEvent
				 */
				void actionPerformed (toolbox::Event& event);

				/*! Callback to be implemented for receiving default parameter setting
				 */
				void actionPerformed (xdata::Event& e);

				void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void view (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void inject (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				void TabPanel (xgi::Output * out);
				void ExceptionsTabPage (xgi::Output * out);
				void StatisticsTabPage (xgi::Output * out);

				void publishEvent (const std::string & command, xcept::Exception& e) throw (sentinel::probe::exception::Exception);

				bool handleException (xcept::Exception& ex, void* context);

				void asynchronousExceptionNotification (xcept::Exception& ex);

			protected:

				void timeExpired (toolbox::task::TimerEvent& e);

				// Exported variables
				xdata::String watchdog_;       // an interval for a watchdog to deliver exceptions
				xdata::Double committedPoolSize_;
				xdata::UnsignedInteger32 maxExceptionMessageSize_;

				b2in::utils::MessengerCache * sentineldMessengerCache_;

				xdata::String sentineldURL_;	// static eventing address configuration

				//! lock for the alarm, revoke and exception queues and infospaces
				toolbox::BSem repositoryLock_;
				//! infospace for receiving alarms from applications
				xdata::InfoSpace* alarms_;
				//! queue for exceptions to be sent out
				std::list<std::pair<xcept::Exception, size_t> > exceptions_;
				//! queue for revokes to send out
				std::list<xcept::Exception> revokes_;

				xdata::UnsignedInteger64 outgoingLossCounter_;
				xdata::UnsignedInteger64 outgoingCounter_;

				toolbox::mem::Pool* pool_;

				void processPendingEvents ();
		};
	}
}

#endif

