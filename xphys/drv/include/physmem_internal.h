/*
 * physmem_internal.h  in ..
 *
 * FM 23-sep-1999
 *
 * $Id: physmem_internal.h,v 1.4 2005/01/14 10:43:32 xdaq Exp $
 */

#ifndef PHYSMEM_INTERNAL_H
#define PHYSMEM_INTERNAL_H

#undef PRINT_LEVEL
#define PRINT_LEVEL 0


#define PRINTF(x)	if (PRINT_LEVEL>=(x)) printk


#define PHYSMEM_MAJOR	121

/*
 *  1 pahe is 4k bytes
 *  16 k pages : bigphys eror
 *  16 k pages -1 : bigphys OK but susbsequent mmap error
 *  14 k idem
 *  12 k pages : OK ie 48 Mbyte
 */ 

/* #define PHYSMEM_NMB_PAGES (16*1024 - 1) */
/* mod 28 mar 2002 - now bigphysarea of 196608 kB = 49152 pages */
#define PHYSMEM_NMB_PAGES (3*16*1024 - 1)
/* #define PHYSMEM_NMB_PAGES (14*1024) */
/* #define PHYSMEM_SIZE (PHYSMEM_NMB_PAGES * PAGE_SIZE) */

/*
 * ioctl's
 */

struct  physmem_info {
  int bpa_nmb_pages ;
  int bpa_size ;
  int bpa_kern ;
  int bpa_bus ;
  int bpa_phys ;
} ;

#define PHYSMEM_GET_INFO  _IOR ('M',0,struct physmem_info)

typedef struct physmem_dev {
  int bpa_nmb_pages ;
  int bpa_size ;
  int bpa_addr ;
  int bpa_bus ;
  int bpa_phys ;
  int client;	/* to which client is it allocated (0 for none) */
  struct physmem_dev* next; /* pointer to next device. NULL if last */
} physmem_dev_t ;

#define PHYSMEM_CONFIG  _IOW('T',1, unsigned long)
#endif



