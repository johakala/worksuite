// $Id: memphys.c,v 1.1 2004/09/16 10:05:07 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, S. Markan and L. Orsini                        *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

/*
 *
 * memphys is a Linux driver which provides applications with access to
 * a large pool of memory (approaching the total amount of RAM on the
 * system). This pool consists of many chunks, each of which is a
 * contiguous group of pages in physical memory. Chunks are not
 * swapped or relocated by the operating system, so they can be used
 * for DMA. Applications may allocate any number of free chunks for
 * their own use, and can mmap those chunks.
 *
 * More information in readme.html
 */
                                                    
#include <linux/module.h>
#include <linux/config.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/poll.h>
#include <linux/random.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>

#include <asm/uaccess.h>
#include <asm/io.h>

#include <linux/fs.h>
#include <linux/types.h>
#include <linux/fcntl.h>
#include <asm/system.h> 

#include "memphys.h"

// Print debugging messages to the kernel log?
#define DEBUG 0

// This is a hack to work around the fact that set_page_count is not
// defined in the CERN Linux sources.
#ifdef SPC_HACK
#define set_page_count(p,v) atomic_set(&(p)->count, v)
#endif

/* Module parameters --------------------------------------------------- */

// These parameters may be set at compile time or at module load time,
// and otherwise will have default values.

// What device major number to use
#ifndef MEMPHYS_MAJOR
#define MEMPHYS_MAJOR 0
#endif

// Number of chunks to allocate for the pool
#ifndef NUM_CHUNKS
#define NUM_CHUNKS 1
#endif

// Order of each chunk (# pages in a chunk = 2^order)
#ifndef CHUNK_ORDER
#define CHUNK_ORDER 0
#endif

static int memphys_major = MEMPHYS_MAJOR;
static int num_chunks = NUM_CHUNKS;
static int chunk_order = CHUNK_ORDER;

MODULE_PARM(memphys_major,"i");
MODULE_PARM(num_chunks,"i");
MODULE_PARM(chunk_order,"i");

/* end module parameters --------------------------------------------------- */

static struct semaphore sem;	/* for thread safety */

// information maintained by the driver for each chunk.  A chunk is
// considered free iff client == mapcount == 0
struct chunk_info {
  int adr;			/* kernel virtual address */
  int client;			/* to which client is it allocated (0
				   for none) */
  int mapcount;			/* how many VMAs point to it */
};

static struct chunk_info *chunks; /* the pool */

// Each client (file descriptor) who opens the driver is assigned a
// client_id.  next_client_id is the next free one.
static int next_client_id = 1;

// How many chunks are free
static int chunks_available(void) {
  int ret, i;
  ret = 0;
  for (i = 0 ; i < num_chunks; i++) {
    if (chunks[i].client == 0 && chunks[i].mapcount == 0)
      ret++;
  }
  return ret;
}

// Implements the open syscall
int memphys_open(struct inode *inode_p, struct file *file_p) {
  if (DEBUG) printk("<1>memphys: memphys_open() called\n");
  if (down_interruptible(&sem))
    return -ERESTARTSYS;
  if (DEBUG) printk("<1>memphys: memphys_open() got semaphore\n");

  // we need to keep track of which client_id is assigned to which
  // struct file
  file_p->private_data = (void*) next_client_id;

  if (DEBUG) printk("<1>client id is %d\n", (int) file_p->private_data);
  next_client_id++;

  up(&sem);
  return 0;
}

// Implements the close syscall
int memphys_release(struct inode *inode_p, struct file *file_p) {
  int i;
  if (DEBUG) printk("<1>memphys: memphys_release() called\n");
  if (DEBUG) printk("<1>client id is %d\n", (int) file_p->private_data);

  if (down_interruptible(&sem))
    return -ERESTARTSYS;
  if (DEBUG) printk("<1>memphys: memphys_release() got semaphore\n");

  // mark the chunks not used by any client
  for (i = 0; i < num_chunks; i++)
    if (chunks[i].client == (int) file_p->private_data)
      chunks[i].client = 0;

  up(&sem);
  return 0;
}

// Called when a VMA mapping one of the chunks is opened (the first
// time doesn't count)
void memphys_vma_open(struct vm_area_struct *vma) {
  if (DEBUG) printk("<1>memphys: a vma is being mapped\n");

  int chunk = (int) vma->vm_private_data; /* which chunk this vma was mapped to */
  chunks[chunk].mapcount++;

  //  MOD_INC_USE_COUNT would be here if we were using just 2.4...
}

// Called when a VMA mapping one of the chunks is closed
void memphys_vma_close(struct vm_area_struct *vma) {
  if (DEBUG) printk("<1>memphys: a vma is being unmapped\n");
  
  int chunk = (int) vma->vm_private_data; /* which chunk this vma was mapped to */
  chunks[chunk].mapcount--;

  if (DEBUG)
    printk("begins: %d,%d,%d,%d\n",
	   ((char *)(chunks[chunk].adr))[0],
	   ((char *)(chunks[chunk].adr))[1],
	   ((char *)(chunks[chunk].adr))[2],
	   ((char *)(chunks[chunk].adr))[3]);
  //  MOD_DEC_USE_COUNT would be here if we were using just 2.4....
}

static struct vm_operations_struct memphys_vm_ops = {
  open: memphys_vma_open,
  close: memphys_vma_close,
};

// Implements the mmap syscall
int memphys_mmap(struct file *file_p, struct vm_area_struct *vma) {
  if (DEBUG) printk("<1>memphys: memphys_mmap() called w/ p.d. %d\n", (int) file_p->private_data);

  int result, whichchunk, rpr;
  unsigned long vma_len, vma_start, vma_end, vma_offset;
  pgprot_t vma_page_prot;
  int ret = 0;

  if ( (result = down_interruptible(&sem)) )
    {
      return -ERESTARTSYS;
    }
  
  if (DEBUG) printk("<1>memphys: memphys_mmap() got semaphore\n");

  vma_offset = vma->vm_pgoff << PAGE_SHIFT;
  vma_start  =  vma->vm_start;
  vma_end    =  vma->vm_end;
  vma_page_prot = vma->vm_page_prot;

  vma_len = vma_end - vma_start;

  // can only mmap exactly one chunk
  if (vma_len != ((1 << chunk_order) * PAGE_SIZE)) {
    ret = -EINVAL;
    goto out;
  }
  // file offset must be chunk-aligned and not past end of pool
  if (vma_offset % ((1 << chunk_order) * PAGE_SIZE) != 0) {
    ret = -EINVAL;
    goto out;
  }
  whichchunk = vma_offset / ((1 << chunk_order) * PAGE_SIZE);
  if (whichchunk < 0 || whichchunk >= num_chunks) {
    ret = -EINVAL;
    goto out;
  }
  // cannot mmap somebody else's chunk
  if (chunks[whichchunk].client != (int) file_p->private_data) {
    ret = -EINVAL;
    goto out;
  }
  
  // actually perform the page table changes

#ifdef KERNEL_2_6
  rpr = remap_page_range(vma, 
		       vma_start, 
		       virt_to_phys((void*) chunks[whichchunk].adr),
		       PAGE_SIZE << chunk_order, 
		       vma_page_prot) != 0 ? -EAGAIN : 0;
	if (rpr == -EAGAIN) {
		 if (DEBUG) printk("<1>memphys: memphys_mmap() remap_page_range failure %d\n", (int) rpr);
		 up(&sem);
		 return rpr;
	  } 
#else
  rpr = remap_page_range(vma_start, 
			    virt_to_phys((void*) chunks[whichchunk].adr),
			    PAGE_SIZE << chunk_order, 
			 vma_page_prot) != 0 ? -EAGAIN : 0;
	if (rpr == -EAGAIN) {
	   if (DEBUG) printk("<1>memphys: memphys_mmap() remap_page_range failure %d\n", (int) rpr);
	   up(&sem);
	   return rpr;
	 } 
#endif

  vma->vm_private_data = (void*) whichchunk;
  vma->vm_ops = &memphys_vm_ops;

  //  vma->vm_flags |= VM_RESERVED <-- not sure if this ought to be used
  //  MOD_INC_USE_COUNT would be here if we were using just 2.4......

  chunks[whichchunk].mapcount++;

 out:
  up(&sem);
  return ret;
}

// implements ioctl syscall
int memphys_ioctl(struct inode *inode_p, struct file *file_p, unsigned int cmd, unsigned long arg) {
  int i, chunks_found, err;
  struct chunk_addrs tca;
  struct pool_info temp_pi;
  int ret = 0;
  
  if (down_interruptible(&sem))
    return -ERESTARTSYS;
  if (DEBUG) printk("<1>memphys: memphys_ioctl() got semaphore %X\n", cmd);

  // structures to store data received from user
  struct alloc_info alloc_request;
  struct free_info free_request;

  switch(cmd) {
  case MEMPHYS_IOC_ALLOC:
    // allocate chunks

    // safely copy the alloc_info structure from userspace
    if (copy_from_user(&alloc_request, (void*) arg, sizeof(struct alloc_info))) {
      ret = -EFAULT;
      break;
    }
    
    // do we have enough chunks?
    if (chunks_available() < alloc_request.num_chunks) {
      ret = -EFAULT;
      break;
    }

    // can we safely write to userspace?  (On a sidenote, I read
    // something about access_ok not being sufficient in all
    // circumstances...so this code may need something more)
    err = !access_ok(VERIFY_WRITE, (void *)(alloc_request.result),
		     alloc_request.num_chunks * sizeof(struct chunk_addrs));
    if (err) {
      ret = -EFAULT;
      break;
    }

    // actually allocate the chunks
    chunks_found = 0;
    for (i = 0; i < num_chunks; i++) {

      // if this chunk is not being used...
      if (chunks[i].client == 0 && chunks[i].mapcount == 0) {

	// mark it allocated
	chunks[i].client = (int) file_p->private_data;

	// get its addresses
	tca.virt_adr = chunks[i].adr;
	tca.phys_adr = virt_to_phys((void*) tca.virt_adr);
	tca.bus_adr = virt_to_bus((void*) tca.virt_adr);
	tca.index = i;
	// if (DEBUG) printk("<1>alloc: %d %d %lu %lu\n", i, tca.index, i, tca.index);

	// copy this data to the user
	__copy_to_user((void *)(alloc_request.result) + chunks_found * sizeof(struct chunk_addrs),
		       &tca, sizeof(struct chunk_addrs));
	chunks_found++;
	if (chunks_found == alloc_request.num_chunks) break;
      }
    }
    break;
  case MEMPHYS_IOC_FREE:
    // free chunks

    // Copy free_info from userspace to kernel
    if (copy_from_user(&free_request, (void*) arg, FREE_INFO_LEN)) 
	{
      		ret = -EFAULT;
      		break;
    	}

    // can we safely read from userspace?  (On a sidenote, I read
    // something about access_ok not being sufficient in all
    // circumstances...so this code may need something more)
    err = !access_ok(VERIFY_READ, (void *)(free_request.ids),
		     free_request.num_chunks * sizeof(unsigned long));
    if (err) {
      ret = -EFAULT;
      break;
    }

    // actually free the chunks
    for (i = 0; i < free_request.num_chunks; i++) {
      unsigned long chunk;

      // find out which chunk should be freed in this iteration
      __copy_from_user(&chunk, free_request.ids + i, sizeof(unsigned long));

      // check that the chunk the user wants to free is in bounds
      if (chunk >= num_chunks) {
	ret = -EFAULT;
	goto out2;
      }

      // Free the chunk.  Give an error if user tries to free chunks
      // that are mmaped - this is so that clients can't cause silent
      // memory leaks
      if (chunks[chunk].client == (int) file_p->private_data &&
	  chunks[chunk].mapcount == 0) {
	chunks[chunk].client = 0;
      } else {
	ret = -EFAULT;
	goto out2;
      }
    }
    break;
  case MEMPHYS_IOC_POOL_INFO:
    // pool info

    // verify that we can write the pool_info struct to userspace
    err = !access_ok(VERIFY_WRITE, (void *)arg, sizeof(struct pool_info));
    if (err) {
      ret = -EFAULT;
      break;
    }

    // fill the structure
    temp_pi.chunk_order = chunk_order;
    temp_pi.chunks_available = chunks_available();

    // send it to the user
    __copy_to_user((void *) arg, &temp_pi, sizeof(struct pool_info));
    break;
  case MEMPHYS_IOC_CLIENT_ID:
    // return the client_id of the client (this ioctl is mainly for debugging)
    ret = (int) file_p->private_data;
    break;
  default:
    ret = -EINVAL;
  }

 out2:
  up(&sem);
  return ret;
}

struct file_operations memphys_fops = {
  ioctl: memphys_ioctl,
  mmap: memphys_mmap,
  open: memphys_open,
  release: memphys_release,
  owner: THIS_MODULE,
};

// Mark all the pages of the pool reserved so the kernel doesn't move or swap them
static void reserve_pages(void) {
  int i, start, end;
  struct page *page;

  for (i = 0; i < num_chunks; i++) {
    start = chunks[i].adr;
    end = start + (1<<chunk_order)*PAGE_SIZE - 1;

    for (page = virt_to_page(start) ; page <= virt_to_page(end) ; page++) {
      set_bit(PG_reserved, &(page->flags));
      // in 2.4, could have written mem_map_reserve(page) here
    }
  }
}

// Undo reserve_pages
static void unreserve_pages(void) {
  int i, start, end;
  struct page *page;
  
  for (i = 0; i < num_chunks; i++) {
    start = chunks[i].adr;
    end = start + (1<<chunk_order)*PAGE_SIZE - 1;

    for (page = virt_to_page(start) ; page <= virt_to_page(end) ; page++) {
      // This is an ugly hack needed to prevent a memory leak.  It
      // seems that when pages are marked reserved, and a VMA is
      // opened pointing to them, the page usage count is incremented.
      // But when those VMAs are closed, the page usage count is not
      // decremented.  So here we force it down.
      if (page == virt_to_page(start))
	set_page_count(page, 1);
      else
	set_page_count(page, 0);


      clear_bit(PG_reserved, &(page->flags));
      // in 2.4, could have written mem_map_unreserve(page) here
    }
  }
}

// Free the pages we reserved when the module was loaded
static void free_chunks(void) {
  int i;

  for (i = 0; i < num_chunks; i++)
    free_pages(chunks[i].adr, chunk_order);
}

// This function implements the /proc/memphys filesystem entry
int memphys_read_procmem(char *buf, char **start, off_t offset,
			int count, int *eof, void *data) {
  int len = 0;
  if (down_interruptible(&sem))
    return -ERESTARTSYS;
  if (DEBUG) printk("<1>memphys: memphys_read_procmem() got semaphore\n");

  len += sprintf(buf, "Chunks in pool: %d chunks of order %d\n", num_chunks, chunk_order);
  len += sprintf(buf + len, "Chunks available: %d chunks\n", chunks_available());
  *eof = 1;
  up(&sem);
  return len;
}

int init_module(void) {
  int i;

  if (DEBUG) printk("<1>memphys: init_module() called\n");

  // Register the driver
  int result = register_chrdev(memphys_major, "memphys", &memphys_fops);
  if (result < 0) {
    printk(KERN_WARNING "memphys: register_chrdev() failed: attempted to get major %d, returned %d\n", memphys_major, result);
    return -EIO;
  }
  
  // Dynamic assignment of major number
  if (memphys_major == 0) memphys_major = result;

  // Need a place to store the (kernel virtual) addresses of the pages we get
  chunks = vmalloc(num_chunks * sizeof(struct chunk_info));
  if (chunks == 0) {
    printk(KERN_WARNING "memphys: couldn't allocate space to store addresses\n");
    goto alloc_fail_one;
  }

  // Allocate the memory that will make up the pool
  for (i = 0; i < num_chunks; i++) {
    int addr = __get_free_pages(GFP_KERNEL, chunk_order);
    if (addr == 0)
      break;
    chunks[i].adr = addr;
    chunks[i].client = 0;
    chunks[i].mapcount = 0;
  }
  if (i == num_chunks) {
    printk("<1>memphys: successfully allocated %d chunks of order %d\n",
	   num_chunks, chunk_order);
  } else {
    num_chunks = i;
    printk("<1>memphys: could only allocate %d chunks of order %d\n",
	   num_chunks, chunk_order);
    goto alloc_fail_two;
  }

  // Mark pages reserved
  reserve_pages();

  // create /proc/memphys
  create_proc_read_entry("memphys", 0, NULL, memphys_read_procmem, NULL);

  // Initialize semaphore
  sema_init(&sem, 1);

  return 0;			/* success */

  // Undo what was done above in case of failure
 alloc_fail_two:
  free_chunks();
  vfree(chunks);
 alloc_fail_one:
  unregister_chrdev(memphys_major, "memphys");
  return -EIO;
}

// Unload the module by undoing what was done in init_module
void cleanup_module(void) {
  printk("<1>memphys: cleanup_module() called\n");

  remove_proc_entry("memphys", NULL);
  unreserve_pages();
  free_chunks();
  vfree(chunks);
  unregister_chrdev(memphys_major, "memphys");
}
