// $Id: long_test.cpp,v 1.1 2004/09/16 10:05:07 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, S. Markan and L. Orsini                         *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

 /*
 * This is a program to test the memphys driver when it is being used
 * by several processes simultaneously.  It forks off four processes
 * which continually attempt to allocate random amounts of memory, and
 * another one which randomly makes illegal requests.  The processes
 * keep a synchronized count of how many pages they've collectively
 * allocated, so that this can be cross-referenced with the
 * /proc/memphys entry to be sure no memory has gone missing.
 *
 * The synchronization can be disabled by defining the macro NOSYNC
 * (this could possibly provide a more thorough test)
 *
 * This program assumes the module has been loaded and the pool has
 * 1024 chunks of order 3 - this is set in the global variables
 * num_chunks_total and o.
 *
 * Errors should show up as lines to stdout containing the text ERROR.
 * The program runs indefinitely.
 *
 * More information in readme.html
 */

#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "memphys.h"

#define PAGE_SIZE 4096
// #define NOSYNC

// Hard-coded pool info
int num_chunks_total = 1024;
int o = 3;

// We use a semaphore for synchronizing the subprocesses
int sem_id, *shm_ptr;
struct sembuf sem_lock = { 0, -1, 0 };
struct sembuf sem_lock2 = { 0, 1, 0 };

void getSemaphore() {
#ifndef NOSYNC
  if ((semop(sem_id, &sem_lock, 1) == -1)) {
    printf("ERROR %d getting semaphore\n", errno);
    exit(1);
  }
#endif
}
    
void releaseSemaphore() {
#ifndef NOSYNC
  if ((semop(sem_id, &sem_lock2, 1) == -1)) {
    printf("ERROR %d releasing semaphore\n", errno);
    exit(1);
  }
#endif
}

// This function is the entry point after forking off the process
// which makes illegal requests
void errantProcess(int id) {
  printf("Starting errant process %d\n", id);

  int fd = open("/dev/memphys0", O_RDWR);
  if (fd < 0) {
    printf("Couldn't open driver\n");
    exit(0);
  }

  srandom(id);
  
  while (true) {
    printf("Process %c making random erroneous requests\n",
	   'A' + id);

    // just send random free requests

    struct free_info fi;
    fi.num_chunks = random() % (num_chunks_total * 2);
    fi.ids = (unsigned long *) malloc(fi.num_chunks * sizeof(unsigned long));
    for (int i = 0; i < fi.num_chunks; i++) {
      fi.ids[i] = random() % (num_chunks_total * 2);
    }
    ioctl(fd, MEMPHYS_IOC_FREE, &fi);
    free(fi.ids);

    mmap(0, PAGE_SIZE << o,
	 PROT_READ | PROT_WRITE, MAP_SHARED, fd,
	 (random() % (num_chunks_total * 2)) * PAGE_SIZE << o);

    sleep(random() % 2);
  }
}

// This function is the entry point after forking off a process which
// tries to allocate memory
void goodProcess(int id) {
  printf("Starting good process %d\n", id);

  int fd = open("/dev/memphys0", O_RDWR);
  if (fd < 0) {
    printf("Couldn't open driver\n");
    exit(0);
  }

  srandom(id);
  
  while (true) {
    // fill in a struct alloc_info
    struct alloc_info ai;
    ai.num_chunks = random() % num_chunks_total + 1;
    ai.result = (struct chunk_addrs*) malloc(ai.num_chunks * sizeof(struct chunk_addrs));

    // attempt to allocate
    int result = ioctl(fd, MEMPHYS_IOC_ALLOC, &ai);
    if (result != 0) continue;

    // keep track of how much memory the pool should have left
    getSemaphore();
    *shm_ptr -= ai.num_chunks;
    printf("Process %c got %d chunks, so total free should be %d\n",
	   'A' + id, ai.num_chunks, *shm_ptr);
    releaseSemaphore();

    // test the memory

    // write
    for (int chunk = 0; chunk < ai.num_chunks; chunk++) {
      void *adr = mmap(0, PAGE_SIZE << o, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (PAGE_SIZE << o) * ai.result[chunk].index);
      if (adr < 0) {
	printf("ERROR mapping\n");
	exit(1);
      }

      for (unsigned long pos = 0; pos < (PAGE_SIZE << o) / sizeof(unsigned long); pos++) {
	((unsigned long *)adr)[pos] = id + pos + chunk * (PAGE_SIZE << o) / sizeof(unsigned long);
      }
      result = munmap(adr, PAGE_SIZE << o);
      if (result != 0) {
	printf("ERROR unmapping\n");
	exit(1);
      }
    }
    
    // read
    for (int chunk = 0; chunk < ai.num_chunks; chunk++) {
      void *adr = mmap(0, PAGE_SIZE << o, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (PAGE_SIZE << o) * ai.result[chunk].index);
      if (adr < 0) {
	printf("ERROR mapping\n");
	exit(1);
      }

      for (unsigned long pos = 0; pos < (PAGE_SIZE << o) / sizeof(unsigned long); pos++) {
	bool ok = ((unsigned long *)adr)[pos] == id + pos + chunk * (PAGE_SIZE << o) / sizeof(unsigned long);
	if (!ok) {
	  printf("ERROR testing map\n");
	  exit(1);
	}
      }
      result = munmap(adr, PAGE_SIZE << o);
      if (result != 0) {
	printf("ERROR unmapping\n");
	exit(1);
      }
    }

    sleep(random() % 2);

    // Free the memory we allocated
    struct free_info fi;
    fi.num_chunks = ai.num_chunks;
    fi.ids = (unsigned long *) malloc(fi.num_chunks * sizeof(unsigned long));
    for (int i = 0; i < fi.num_chunks; i++) {
      fi.ids[i] = ai.result[i].index;
    }
    int ret = ioctl(fd, MEMPHYS_IOC_FREE, &fi);
    if (ret != 0) {
      printf("ERROR: couldn't free\n");
      exit(1);
    }
    free(fi.ids);
    free(ai.result);

    // Again, update the count of free chunks that should be in the pool
    getSemaphore();
    *shm_ptr += ai.num_chunks;
    printf("Process %c released %d chunks, so total free should be %d\n",
	   'A' + id, ai.num_chunks, *shm_ptr);
    releaseSemaphore();
  }
}

int main() {
  // for System V IPC
  key_t k = ftok(".", 0); // 2nd is an arbitrary integer

  // semaphore
  sem_id = semget(k, 1, IPC_CREAT | 0666);
  if (sem_id == -1) {
    printf("Error %d making semaphore\n", errno);
    exit(1);
  }
  semctl(sem_id, 0, SETVAL, 1);

  // shared memory to keep a count of the number of chunks that should
  // be free
  int shm_id = shmget(k, sizeof(int), IPC_CREAT | 0666);
  if (shm_id == -1) {
    printf("Error %d making shared memory area\n", errno);
    exit(1);
  }
  shm_ptr = (int*) shmat(shm_id, 0, 0);

  *shm_ptr = num_chunks_total;

  // make a few processes
  for (int i = 0; i < 5; i++) {
    int pid = fork();
    if (pid == 0) {
      if (i == 0)
	errantProcess(i);
      else
	goodProcess(i);
      exit(0);
    }
  }

  while(true);

  // release the memory (no, we never get here)
  shmctl(shm_id, IPC_RMID, 0);  
}
