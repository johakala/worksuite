// $Id: memphysTest.cpp,v 1.1 2004/09/16 10:05:07 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, S. Markan and L. Orsini                         *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

/*
 * memphysTest.cpp
 *
 *
 * This file contains the unit tests run by tester.cpp.  Note that the
 * two execlp commands may have to be modified as appropriate to your
 * system.
 *
 * More information in readme.html
 */

#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/wait.h>
#include <iostream>

using namespace std;

#include "memphys.h"
#include "memphysTest.h"

#define PAGE_SIZE 4096

CPPUNIT_TEST_SUITE_REGISTRATION(memphysTest);

void memphysTest::setUp() {
  removeModule();		// start from a clean slate
}

void memphysTest::tearDown() {
}

void memphysTest::insertModule() {
  insertModule(1, 0, true);	// default values
}

/**
 * Insert the module.  If should_succeed is true and the insertion
 * fails, the test fails.
 **/
void memphysTest::insertModule(int num_chunks, int chunk_order, bool should_succeed) {
  int status, pid;

  // Test insertion
  pid = fork();
  if (pid > 0) {
    wait(&status);

    if (should_succeed)
      CPPUNIT_ASSERT(WEXITSTATUS(status) == 0);
  } else {
    char buf1[80];
    char buf2[80];
    sprintf(buf1, "num_chunks=%d", num_chunks);
    sprintf(buf2, "chunk_order=%d", chunk_order);
    execlp("sudo", "sudo", "/sbin/insmod", "memphys.ko", buf1, buf2, NULL);
    CPPUNIT_FAIL("exec() failed");
  }
}

/**
 * Remove the module.
 **/
void memphysTest::removeModule() {
  int status, pid;

  // Test removal
  pid = fork();
  if (pid > 0) {
    wait(&status);

  } else {
    close(1);
    close(2);
    execlp("sudo", "sudo", "/sbin/rmmod", "memphys", NULL);
    CPPUNIT_FAIL("exec() failed");
  }
}

void memphysTest::testInsertAndRemove() {
  // Insert and remove the module with various chunk sizes and numbers
  // of chunks
  for (int n = 1; n <= 1024; n *= 32) {
    for (int o = 0; o <= 3; o++) {
      insertModule(n, o, true);
      removeModule();
    }
  }

  // We don't expect these to succeed, but we should make sure they
  // fail gracefully (i.e. don't crash the kernel)
  insertModule(1, 20, false);
  removeModule();
  insertModule(1 << 29, 0, false);
  removeModule();
}

// Open the device and return a file descriptor
int memphysTest::tryOpen() {
  int fd = open("/dev/memphys0", O_RDWR);
  CPPUNIT_ASSERT(fd >= 0);
  return fd;
}

// Close a file descriptor
void memphysTest::tryClose(int fd) {
  int result = close(fd);
  CPPUNIT_ASSERT(result == 0);
}

/**
 * Fork a process, have it allocate the whole pool, kill it, and check
 * that the chunks are released.
 **/
void memphysTest::testAllocAndKill() {
  int status;

  int n = 1024;
  int o = 3;
  insertModule(n, o, true);

  int pid = fork();
  if (pid > 0) {
    // parent

    sleep(5);
    kill(pid, 9);
    wait(&status);

    // check that memory all free
    int fd = tryOpen();
    numberOfFreeChunksShouldBe(fd, n);
    tryClose(fd);

  } else {
    // child

    int fd = tryOpen();
    numberOfFreeChunksShouldBe(fd, n);

    struct alloc_info ai;
    ai.num_chunks = n;
    CPPUNIT_ASSERT(tryAlloc(fd, &ai) == 0);
    free(ai.result);

    numberOfFreeChunksShouldBe(fd, 0);
    while (true);
  }

  removeModule();
}

/**
 * Make sure the device can be opened
 **/
void memphysTest::testOpenAndKill() {
  int status;

  int n = 1024;
  int o = 3;
  insertModule(n, o, true);

  int pid = fork();
  if (pid > 0) {
    sleep(5);
    kill(pid, 9);
    wait(&status);

    // check that memory all free
    int fd = tryOpen();
    numberOfFreeChunksShouldBe(fd, n);
    tryClose(fd);

  } else {
    int fd = tryOpen();
    numberOfFreeChunksShouldBe(fd, n);
    while (true);
  }

  removeModule();
}

/**
 * Fork a process, have it allocate and mmap the whole pool, kill it,
 * and check that the chunks are released.
 **/
void memphysTest::testMmapAndKill() {
  int status;

  int n = 1024;
  int o = 3;
  insertModule(n, o, true);

  int pid = fork();
  if (pid > 0) {
    // parent
    sleep(5);
    kill(pid, 9);
    wait(&status);

    // check that memory all free
    int fd = tryOpen();
    numberOfFreeChunksShouldBe(fd, n);
    tryClose(fd);

  } else {
    // child

    int fd = tryOpen();
    numberOfFreeChunksShouldBe(fd, n);

    struct alloc_info ai;
    ai.num_chunks = n;
    CPPUNIT_ASSERT(tryAlloc(fd, &ai) == 0);

    for (int chunk = 0; chunk < n; chunk++) {
      void *adr = mmap(0, PAGE_SIZE << o, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (PAGE_SIZE << o) * ai.result[chunk].index);
      CPPUNIT_ASSERT(adr >= 0);
    }

    free(ai.result);

    numberOfFreeChunksShouldBe(fd, 0);
    while (true);
  }

  removeModule();
}

/**
 * Make sure we can open and close the driver and that we can read
 * basic pool information
 **/
void memphysTest::testOpenAndClose() {
  for (int n = 1; n <= 1024; n *= 32) {
    for (int o = 0; o <= 3; o++) {
      insertModule(n, o, true);
      int fd = tryOpen();
      numberOfFreeChunksShouldBe(fd, n);
      chunkOrderShouldBe(fd, o);
      tryClose(fd);
      removeModule();
    }
  }
}

// Helper function - test fails if the chunk order reported by the
// driver is not n
void memphysTest::chunkOrderShouldBe(int fd, int n) {
  struct pool_info pi;
  
  int result = ioctl(fd, MEMPHYS_IOC_POOL_INFO, &pi);
  CPPUNIT_ASSERT(result == 0);

  CPPUNIT_ASSERT(pi.chunk_order == n);
}

// Helper function - return how many free chunks are available
int memphysTest::numFreeChunks(int fd) {
  struct pool_info pi;
  
  int result = ioctl(fd, MEMPHYS_IOC_POOL_INFO, &pi);
  CPPUNIT_ASSERT(result == 0);

  return pi.chunks_available;
}

// Helper function - test fails if the number of free chunks reported
// by the driver is not n
void memphysTest::numberOfFreeChunksShouldBe(int fd, int n) {
  CPPUNIT_ASSERT(numFreeChunks(fd) == n);
}

// Helper function - try an allocation and return the result of the ioctl
int memphysTest::tryAlloc(int fd, struct alloc_info *ai) {
  int result;
  ai->result = (struct chunk_addrs*) malloc(ai->num_chunks * sizeof(struct chunk_addrs));
  CPPUNIT_ASSERT(ai->result > 0);
  return ioctl(fd, MEMPHYS_IOC_ALLOC, ai);
}

// Helper function - try a free and return the result of the ioctl
int memphysTest::tryFree(int fd, struct alloc_info *ai) {
  struct free_info fi;
  fi.num_chunks = ai->num_chunks;
  fi.ids = (unsigned long *) malloc(fi.num_chunks * sizeof(unsigned long));
  for (int i = 0; i < fi.num_chunks; i++) {
    fi.ids[i] = ai->result[i].index;
  }
  int ret = ioctl(fd, MEMPHYS_IOC_FREE, &fi);
  free(fi.ids);
  return ret;
}

// for convenience
void memphysTest::testSimpleAllocation() {
  testSimpleAllocation(false);
}

// for convenience
void memphysTest::testSimpleMmap() {
  testSimpleAllocation(true);
}

/**
 * Try repeatedly allocating and freeing memory
 **/
void memphysTest::testCyclic() {
  int n = 1024;
  int o = 3;
  int times = 10;

  insertModule(n, o, true);
  for (int i = 0; i < 10; i++) {
    printf("%d\n", i);
    testSimpleAllocationAlreadyInserted(true, n, o);
  }
  removeModule();
}

/**
 * Try opening the driver with multiple file descriptors
 **/
void memphysTest::testMultipleOpen() {
  int result;
  int n = 1024;
  int o = 3;
  insertModule(n * 4, o, true);

  int fd = tryOpen();
  int fd2 = tryOpen();

  // Here we test that when we allocate through one file descriptor,
  // the other file descriptor sees the right number of available
  // chunks
  struct alloc_info ai, ai2;
  ai.num_chunks = ai2.num_chunks = n;
  CPPUNIT_ASSERT(tryAlloc(fd, &ai) == 0);
  numberOfFreeChunksShouldBe(fd, n * 3);
  numberOfFreeChunksShouldBe(fd2, n * 3);
  CPPUNIT_ASSERT(tryAlloc(fd2, &ai2) == 0);
  numberOfFreeChunksShouldBe(fd, n * 2);
  numberOfFreeChunksShouldBe(fd2, n * 2);

  // Next we test that when one writes to the memory through one file
  // descriptor, it does not overwrite memory written through the
  // other file descriptor, and that everything written can be read
  // back later.

  // write to all the memory allocated through both file descriptors
  for (int chunk = 0; chunk < n; chunk++) {
    //  for (int chunk = 0; chunk < n; chunk++) {
    void *adr = mmap(0, PAGE_SIZE << o, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (PAGE_SIZE << o) * ai.result[chunk].index);
    CPPUNIT_ASSERT(adr >= 0);
    void *adr2 = mmap(0, PAGE_SIZE << o, PROT_READ | PROT_WRITE, MAP_SHARED, fd2, (PAGE_SIZE << o) * ai2.result[chunk].index);
    CPPUNIT_ASSERT(adr2 >= 0);
    for (unsigned long pos = 0; pos < (PAGE_SIZE << o) / sizeof(unsigned long); pos++) {
      ((unsigned long *)adr)[pos] = pos + chunk * (PAGE_SIZE << o) / sizeof(unsigned long);
      ((unsigned long *)adr2)[pos] = -(pos + chunk * (PAGE_SIZE << o) / sizeof(unsigned long));
    }
    result = munmap(adr, PAGE_SIZE << o);
    CPPUNIT_ASSERT(result == 0);
    result = munmap(adr2, PAGE_SIZE << o);
    CPPUNIT_ASSERT(result == 0);
  }
    
  // read everything written above and make sure it's the same
  for (int chunk = 0; chunk < n; chunk++) {
    void *adr = mmap(0, PAGE_SIZE << o, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (PAGE_SIZE << o) * ai.result[chunk].index);
    CPPUNIT_ASSERT(adr >= 0);
    void *adr2 = mmap(0, PAGE_SIZE << o, PROT_READ | PROT_WRITE, MAP_SHARED, fd2, (PAGE_SIZE << o) * ai2.result[chunk].index);
    CPPUNIT_ASSERT(adr2 >= 0);
    for (unsigned long pos = 0; pos < (PAGE_SIZE << o) / sizeof(unsigned long); pos++) {
      //      cout << "cp: " << chunk << " " << pos << endl;
      CPPUNIT_ASSERT(((unsigned long *)adr)[pos] == pos + chunk * (PAGE_SIZE << o) / sizeof(unsigned long));
      CPPUNIT_ASSERT(((unsigned long *)adr2)[pos] == -(pos + chunk * (PAGE_SIZE << o) / sizeof(unsigned long)));
    }
    result = munmap(adr, PAGE_SIZE << o);
    CPPUNIT_ASSERT(result == 0);
    result = munmap(adr2, PAGE_SIZE << o);
    CPPUNIT_ASSERT(result == 0);
  }

  // Try a couple more allocations and frees
  for (int i = 0; i < 2; i++) {
    testSimpleAllocationAlreadyOpened(true, n * 2, o, fd);
    testSimpleAllocationAlreadyOpened(true, n * 2, o, fd2);
  }

  // Make sure that chunks can't be freed from a different file
  // descriptor from the one that allocated them
  struct alloc_info ai3;
  ai3.num_chunks = n;
  CPPUNIT_ASSERT(tryAlloc(fd, &ai3) == 0);
  int temp = numFreeChunks(fd);
  CPPUNIT_ASSERT(tryFree(fd2, &ai3) != 0);
  CPPUNIT_ASSERT(numFreeChunks(fd2) == temp);
  
  tryClose(fd);
  tryClose(fd2);
  removeModule();
}

/**
 * Fork a process which goes into an infinite loop of allocation and
 * deallocation, and kill it.  Make sure memory is not lost.
 **/
void memphysTest::testCyclicAndKill() {
  int status;

  int n = 1024;
  int o = 3;
  insertModule(n, o, true);

  int pid = fork();
  if (pid > 0) {
    sleep(5);
    kill(pid, 9);
    wait(&status);

    // check that memory all free
    int fd = tryOpen();
    numberOfFreeChunksShouldBe(fd, n);
    tryClose(fd);

  } else {
    while (true)
      testSimpleAllocationAlreadyInserted(true, n, o);
  }

  removeModule();
}

/**
 * This is used by several methods above.  Given the file descriptor
 * for the driver, it allocates, mmaps, unmaps, and frees all the
 * memory in the pool (which should be n chunks of order o).  It
 * checks that the memory was mmapped correctly.  All the mmapping
 * steps are skipped if withmmap is false.
 *
 * Also checks that we can't allocate more memory than the pool has.
 **/
void memphysTest::testSimpleAllocationAlreadyOpened(bool withmmap, int n, int o, int fd) {
  int result;
  struct alloc_info ai;
  struct alloc_info ai2;
  ai.num_chunks = n;
  ai2.num_chunks = 1;
  CPPUNIT_ASSERT(tryAlloc(fd, &ai) == 0);
  
  if (withmmap) {
    for (int chunk = 0; chunk < n; chunk++) {
      void *adr = mmap(0, PAGE_SIZE << o, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (PAGE_SIZE << o) * ai.result[chunk].index);
      CPPUNIT_ASSERT(adr >= 0);
      
      for (unsigned long pos = 0; pos < (PAGE_SIZE << o) / sizeof(unsigned long); pos++) {
	((unsigned long *)adr)[pos] = pos;
      }
      for (unsigned long pos = 0; pos < (PAGE_SIZE << o) / sizeof(unsigned long); pos++) {
	CPPUNIT_ASSERT(((unsigned long *)adr)[pos] == pos);
      }
      
      // Unmap memory
      result = munmap(adr, PAGE_SIZE << o);
      CPPUNIT_ASSERT(result == 0);
    }
  }
	  
  numberOfFreeChunksShouldBe(fd, 0);
  // we should be out of memory now
  CPPUNIT_ASSERT(tryAlloc(fd, &ai2) < 0);
  CPPUNIT_ASSERT(tryFree(fd, &ai) == 0);
  free(ai.result);
      
  numberOfFreeChunksShouldBe(fd, n);

  // shouldn't be able to allocate > n chunks
  ai.num_chunks = n + 1;
  CPPUNIT_ASSERT(tryAlloc(fd, &ai) < 0);
  free(ai.result);
  
  numberOfFreeChunksShouldBe(fd, n);
}


void memphysTest::testSimpleAllocationAlreadyInserted(bool withmmap, int n, int o) {
  int fd = tryOpen();
  testSimpleAllocationAlreadyOpened(withmmap, n, o, fd);
  tryClose(fd);
}

void memphysTest::testSimpleAllocation(bool withmmap) {
  int result;

  for (int n = 1; n <= 1024; n *= 32) {
    for (int o = 0; o <= 3; o++) {
      // Open the device
      insertModule(n, o, true);
      testSimpleAllocationAlreadyInserted(withmmap, n, o);
      removeModule();
    }
  }
}
