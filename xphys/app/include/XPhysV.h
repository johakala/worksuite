//
// Version definition for XPhys XDAQ application module
//
#ifndef ptXPhysV_H
#define ptXPhysV_H

#include "PackageInfo.h"

namespace XPhys {
    const string package  =  "XPhys";
    const string versions =  "2.0.1";
    const string description = "XPhys memory allocation for Linux BigPhys memory patch";
	const string link = "";
    toolbox::PackageInfo getPackageInfo();
    void checkPackageDependencies() throw (toolbox::PackageInfo::VersionException);
    set<string, less<string> > getPackageDependencies();
}

#endif
