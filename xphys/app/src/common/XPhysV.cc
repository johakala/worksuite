#include "toolboxV.h"
#include "xoapV.h"
#include "xdaqV.h"
#include "XPhysV.h"

GETPACKAGEINFO(XPhys)

void XPhys::checkPackageDependencies() throw (toolbox::PackageInfo::VersionException)
{
        CHECKDEPENDENCY(toolbox)
        CHECKDEPENDENCY(xoap)
	CHECKDEPENDENCY(xdaq)
}

set<string, less<string> > xdaq::getPackageDependencies()
{
    set<string, less<string> > dependencies;
    ADDDEPENDENCY(dependencies,toolbox);
    ADDDEPENDENCY(dependencies,xoap);
    ADDDEPENDENCY(dependencies,xdaq);
    return dependencies;
}	
