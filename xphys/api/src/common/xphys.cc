#include "xphys.h"
#include "bget.h"
#include "xdaq.h"
#include <stdio.h>
#include <errno.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdarg.h>
#include <sys/resource.h>
#include <sched.h>
#include <stdlib.h>
#include <asm/page.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/file.h>
#include <sys/fcntl.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <net/if.h>
#include <string.h>
#include <unistd.h>

#include "physmem_internal.h"

#include "xphysException.h"

XPhysBuffer::XPhysBuffer(unsigned long usrAddr, unsigned long busAddr, unsigned long kernelAddr, unsigned long size) 
{
	usrAddr_ = usrAddr;
	physAddr_ = busAddr;
	kernelAddr_ = kernelAddr;
	size_ = size;
}	

void * XPhysBuffer::userAddr() 
{
	return (void*)usrAddr_;
}

void * XPhysBuffer::physAddr() 
{
	return (void*)physAddr_;
}

void * XPhysBuffer::kernelAddr() 
{
	return (void*)kernelAddr_;
}

unsigned long XPhysBuffer::size()
{
	return size_;
}


XPhysAllocator::XPhysAllocator (unsigned long pages) throw (xphysException)
{

   int rv;
   int map_len_bytes = 0;
   char *mb_map ;
   int bpa_nmb_pages ;
   int bpa_bus, bpa_kern ;



   fd_ = open("/dev/physmem0", O_RDWR) ; 
   if (fd_ < 0) 
   {
      throw xphysException("could not open /dev/physmem0 device");
   }


   // Set BigPhys Size 
     if ( pages > 0 ) 
     {
	 if ((rv = ioctl(fd_, PHYSMEM_CONFIG, pages) < 0)) 
	 {
	 	/**  perror("ioctl failed: "); **/		
	 	throw xphysException ("cannot set bigphys size");
	 }
     } else 
     {
	 throw xphysException("bigphys page size not initialized");
     }


     // Query BigPhys configuration 
     struct physmem_info bpinfo;
     bzero(&bpinfo, sizeof(struct physmem_info));

     if ((rv = ioctl(fd_, PHYSMEM_GET_INFO, &bpinfo)) < 0) 
     {
	throw xphysException("cannot retrieve bigphys information");
     }


     bpa_nmb_pages = bpinfo.bpa_nmb_pages ;
     bpa_bus = bpinfo.bpa_bus ;
     bpa_kern = bpinfo.bpa_kern ;

     map_len_bytes = bpa_nmb_pages * PAGE_SIZE;

     mb_map = (char *) mmap(NULL, map_len_bytes,
			    PROT_READ | PROT_WRITE,
			    MAP_SHARED,
			    fd_,
			    (off_t) 0);


     if ((int) mb_map == -1) {
       throw xphysException("failed to mmap bigphys memory"); 
     }

     segmentSize_ =  map_len_bytes;
     baseUserAddress_ = (unsigned long)mb_map;
     baseBusAddress_ = bpa_bus;
     baseKernelAddress_ = bpa_kern;

     bpool((void *)baseUserAddress_, segmentSize_);
}



XPhysAllocator::~XPhysAllocator() throw (xphysException)
{
	// unmap bigphys memory
	int status = munmap((void*)baseUserAddress_,segmentSize_);
	if ( status == -1) {
	       	throw xphysException("failed to unmap bigphys memory"); 
	}

	//close big phys device
	if ( fd_ != 0 )
		close(fd_);

	bclear();

}

Buffer * XPhysAllocator::alloc(unsigned long size) 
{
	void * userAddr = bget(size);	

	if (userAddr == 0) return (Buffer*) 0;

	return new XPhysBuffer((unsigned long)userAddr,
			(unsigned long)	virttophys((unsigned long)userAddr),
		(unsigned long)	virttokernel((unsigned long)userAddr), size);
}

void XPhysAllocator::free (Buffer * buffer) 
{
	if (buffer != 0)
	{
		//printf("Free buffer address: %x usr address %x\n", buffer, buffer->userAddr());
		brel(buffer->userAddr());
		delete buffer;
	}

}

//conversion functions
void * XPhysAllocator::virttophys(unsigned long userAddr)
{
	// check should be done here
	return (void*)((userAddr - baseUserAddress_) + baseBusAddress_); 
}

void * XPhysAllocator::virttokernel(unsigned long userAddr)
{
        // check should be done here
        return (void*)((userAddr - baseUserAddress_) + baseKernelAddress_);
}


unsigned long XPhysAllocator::usrBaseAddr ()
{
	return baseUserAddress_;
}	

unsigned long XPhysAllocator::busBaseAddr ()
{
	return baseBusAddress_;
}	

unsigned long XPhysAllocator::kernelBaseAddr ()
{
	return baseKernelAddress_;
}	

void XPhysAllocator::statistics (unsigned long* curalloc, unsigned long* totfree, unsigned long* maxfree, unsigned long* nallocated)
{
	if ((curalloc || totfree || maxfree || nallocated) == 0) return;

	unsigned long nget, nrel;
	bstats ( (bufsize*) curalloc, (bufsize*) totfree, (bufsize*) maxfree, (long*) &nget, (long*) &nrel);
	*nallocated = nget-nrel;
}


