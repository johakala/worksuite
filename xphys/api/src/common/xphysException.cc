#include "xphysException.h"

xphysException::xphysException() throw ()
{
	msg_ = "unknown";
	name_ = typeid( *this ).name();
}


xphysException::xphysException(string message) throw ()
{
	msg_ = message;
	name_ = typeid( *this ).name();
}

xphysException::~xphysException() throw() 
{

}

string& xphysException::message	()
{
	return msg_;
}

const char* xphysException::what ()
{
	return toString().c_str();
}

string xphysException::name() 
{
	return name_;
}

string xphysException::toString ()
{
	return msg_;
}
