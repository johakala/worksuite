#include <vector>
#include "xphys.h"

int main (int argc, char** argv)
{
	cout << "Allocating 10000 pages of BigPHys memory" << endl;
	XPhysAllocator allocator (10000);
	
	vector<Buffer*> buffers;
	
	cout << "Allocating 1000 x 8192 Bytes" << endl;
	cout << "Filling memory with pattern of 0xFF characters" << endl;
	
	for (int i = 0; i < 1000; i++)
	{
		Buffer* b = allocator.alloc ( 8192 );
		
		memset (b->userAddr(), 0xFF, 8192);
		
		buffers.push_back ( b );
	}
	
	cout << "Comparing all buffers against first one." << endl;
	
	Buffer* first = buffers[0];
	
	for (int i = 1; i < 1000; i++)
	{
		cout << "Comparing buffer " << i << endl;
	
		Buffer* b = buffers[i];
		
		if (memcmp (first->userAddr(), b->userAddr(), 8192) != 0)
		{
			cout << "Error. A buffer is different from the first one." << endl;
		}
	}
	
	for (unsigned int k = 0; k < buffers.size(); k++)
	{
		cout << "Freeing buffer " << k << endl;
		allocator.free (buffers[k]);
	}

	return 0;
	
}
