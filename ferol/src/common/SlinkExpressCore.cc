#include "ferol/SlinkExpressCore.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/ferolConstants.h"
#include <string>
#include <sstream>

ferol::SlinkExpressCore::SlinkExpressCore( HAL::HardwareDeviceInterface *ferol, HAL::AddressTableInterface *addressTable, std::string dataSource )
    : ferolDevice_P( ferol ),
      addressTable_P( addressTable ),
      dataSource_( dataSource )
{
}

HAL::AddressTableInterface &
ferol::SlinkExpressCore::getAddressTableInterface()
{
    return *addressTable_P;
}

void
ferol::SlinkExpressCore::read( std::string item, uint32_t *result, uint32_t linkno )
{
    uint32_t adr = addressTable_P->getGeneralHardwareAddress( item ).getAddress();
    uint32_t val;
    slinkExpressCommand( adr, val, READ, linkno);
    *result = val;
}

void
ferol::SlinkExpressCore::write( std::string item, uint32_t data, uint32_t linkno )
{
    uint32_t adr = addressTable_P->getGeneralHardwareAddress( item ).getAddress();
    slinkExpressCommand( adr, data, WRITE, linkno);     
}

void
ferol::SlinkExpressCore::slinkExpressCommand( uint32_t adr, uint32_t &data, Slink_ReadWrite rw, uint32_t linkno )
{
    uint32_t pollres = 0;
    std::string cmdstr,datstr,polstr;

    if ( (dataSource_ == L10G_SOURCE) || (dataSource_ == L10G_CORE_GENERATOR_SOURCE) )
        {
            cmdstr = "L10gb_cmd_upload_SlX";
            datstr = "L10gb_data_upload_SlX";
            polstr = "L10gb_cmd_poll_SlX";
        }
    else if ( (dataSource_ == L6G_SOURCE) || (dataSource_ == L6G_CORE_GENERATOR_SOURCE) || (dataSource_ == L6G_LOOPBACK_GENERATOR_SOURCE) )
        {
            cmdstr = "L5gb_cmd_upload_SlX";
            datstr = "L5gb_data_upload_SlX";
            polstr = "L5gb_cmd_poll_SlX";
        }
    else if ( dataSource_ == GENERATOR_SOURCE )
        {
            if ( rw == READ )
                data = -1;
            return;
        }
    else 
        {
            std::stringstream msg;
            msg << "FATAL SOFTWARE BUG: dataSource " << dataSource_ << " not expected in SlinkExpressCore.";
            XCEPT_RAISE( utils::exception::FerolException, msg.str() );
        }
    if (linkno == 0)
        {
            cmdstr += "0";
            datstr += "0";
            polstr += "0";
        } 
    else 
        {
            cmdstr += "1";
            datstr += "1";
            polstr += "1";
        }

    try
        {
            if ( rw == WRITE ) 
                {
                    adr = adr + 0x80000000;
                    ferolDevice_P->write( cmdstr, adr );
                    ferolDevice_P->write( datstr, data );
                }
            else 
                {
                    ferolDevice_P->write( cmdstr, adr );
                }

            try
                {
                    ferolDevice_P->pollItem( polstr, 1, 50, &pollres );
                }
            catch( HAL::TimeoutException &e )
                {
                    std::string error = "Timeout while sending command to SlinkExpress sender core.";
                    XCEPT_RETHROW( utils::exception::FerolException, error, e );
                }    

            if ( rw == READ )
                {
                    ferolDevice_P->read( datstr, &data );
                }
        }
    catch( HAL::HardwareAccessException &e )
        {
            std::string error = "Problems while sending command to SlinkExpress sender core.";
            XCEPT_RETHROW( utils::exception::FerolException, error, e );
        }    
}

