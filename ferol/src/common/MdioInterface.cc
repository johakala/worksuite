#include "ferol/MdioInterface.hh"
#include <sstream>
#include "d2s/utils/Exception.hh"
#include "ferol/loggerMacros.h"

ferol::MdioInterface::MdioInterface( HAL::PCIDevice *ferolDevice, 
                                     log4cplus::Logger logger )
    : mdioLock_( toolbox::BSem::FULL ),
      ferolDevice_P( ferolDevice ),
      logger_( logger )
{
}

void 
ferol::MdioInterface::mdiolock()
{
    mdioLock_.take();
}

void 
ferol::MdioInterface::mdiounlock()
{
    mdioLock_.give();
}

void ferol::MdioInterface::mdioRead( uint32_t vitesseNo, uint32_t reg, uint32_t *value_ptr, uint32_t device )
{
    std::string linkStr;
    if (vitesseNo == 0)
        linkStr = "B0";
    else if( vitesseNo == 1)
        linkStr = "A0";
    else 
        {
            std::stringstream err;
            err  << "Illegal vitesseNo for MDIO operation (must be 0 or 1, but received: \"" << vitesseNo << "\").";
            XCEPT_DECLARE( utils::exception::FerolException, top, err.str() );
            throw(top);
        }
    std::string MDIO_ITEM = "MDIO_LINK10GB_" + linkStr;
    std::string MDIO_POLL = "MDIO_LINK10GB_POLL_" + linkStr;    
    uint32_t pollres;
    try
        {
            mdiolock();
            // See that the MDIO bus is free.
            ferolDevice_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
            uint32_t cmd;

            // Set the MDIO address
            cmd = (reg & 0xffff) + 0x20000 + ((device & 0x1f) << 18);
            ferolDevice_P->write( MDIO_ITEM, cmd );
            // Wait until the address is set.
            ferolDevice_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );

            // Issue the read command
            cmd = ((device & 0x1f) << 18) + 0x30000000;
            ferolDevice_P->write( MDIO_ITEM, cmd );
            // Wait until data is read by the 2 wire interface into the internal register.
            ferolDevice_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );

            // read out the value
            ferolDevice_P->read( MDIO_ITEM, value_ptr );            
            //DEBUG( "mdioRead from " << MDIO_ITEM << " is " << std::hex << *value_ptr );
            mdiounlock();
        }
    catch( HAL::TimeoutException &e )
        {
            std::stringstream err;
            err << "Timeout while while reading mdio interface ( mdioRead ). (poll result: " << pollres << " " << MDIO_POLL << " )";
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
        }
    catch( HAL::HardwareAccessException &e )
        {
            std::stringstream err;
            err << "Problem accessing the MDIO hardware. " << e.what();
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
        }
}

void ferol::MdioInterface::mdioWrite( uint32_t vitesseNo, uint32_t reg, uint32_t value, uint32_t device  )
{
    std::string linkStr;
    if (vitesseNo == 0)
        linkStr = "B0";
    else if( vitesseNo == 1)
        linkStr = "A0";
    else 
        {
            std::stringstream err;
            err << "Illegal vitesseNo for MDIO operation (must be 0 or 1, but received: \"" << vitesseNo << "\").";
            XCEPT_DECLARE( utils::exception::FerolException, top, err.str() );
            throw(top);
        }
    std::string MDIO_ITEM = "MDIO_LINK10GB_" + linkStr;
    std::string MDIO_POLL = "MDIO_LINK10GB_POLL_" + linkStr;    
    uint32_t pollres;

    try
        {
            mdiolock();
            // See that the MDIO bus is free.
            //uint32_t tmp;
            //ferolDevice_P->read( MDIO_ITEM, &tmp );
            //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
            ferolDevice_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
            uint32_t cmd;
            // Set the MDIO address
            cmd =(reg & 0xffff) + 0x20000 + ((device & 0x1f) << 18);
            //DEBUG("set address. write  " << MDIO_ITEM << " : " << std::hex << cmd);
            ferolDevice_P->write( MDIO_ITEM, cmd);
            // Wait until the address is set.
            //ferolDevice_P->read( MDIO_ITEM, &tmp );
            //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
            ferolDevice_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );

            cmd = (0xffff & value) + 0x20000 + ((device & 0x1f) << 18) + 0x10000000;
            //DEBUG("Write command. write " << MDIO_ITEM << " : " << std::hex << cmd);
            ferolDevice_P->write( MDIO_ITEM, cmd);
            // Wait until the address is set.            
            //ferolDevice_P->read( MDIO_ITEM, &tmp );
            //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
            ferolDevice_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
            //DEBUG( "MDIO write successfull" );
            mdiounlock();
        }
    catch( HAL::TimeoutException &e )
        {
            std::stringstream err;
            err << "Timeout while writing to the MDIO interface. (poll result: " << pollres << " )";
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
        }
    catch( HAL::HardwareAccessException &e )
        {
            std::stringstream err;
            err << "Problem accessing the MDIO hardware. " << e.what();
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
        }
}

void ferol::MdioInterface::mdioReadBlock( uint32_t vitesseNo, uint32_t reg, char data[], 
                                  uint32_t length, uint32_t device )
{
    std::string linkStr;
    if (vitesseNo == 0)
        linkStr = "B0";
    else if( vitesseNo == 1)
        linkStr = "A0";
    else 
        {
            std::stringstream err;
            err << "Illegal vitesseNo for MDIO operation (must be 0 or 1, but received: \"" << vitesseNo << "\").";
            XCEPT_DECLARE( utils::exception::FerolException, top, err.str() );
            throw(top);
        }
    std::string MDIO_ITEM = "MDIO_LINK10GB_" + linkStr;
    std::string MDIO_POLL = "MDIO_LINK10GB_POLL_" + linkStr;    
    uint32_t pollres;
    try
        {
            mdiolock();
            // See that the MDIO bus is free.
            //uint32_t tmp;
            //ferolDevice_P->read( MDIO_ITEM, &tmp );
            //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
            ferolDevice_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
            uint32_t cmd;

            for ( uint32_t ic = 0; ic < length; ic++ )
                {
                    // Set the address
                    cmd = ((reg + ic) & 0xffff)+ 0x20000 + ((device & 0x1f) << 18);
                    //DEBUG("set address. write  " << MDIO_ITEM << " : " << std::hex << cmd);
                    ferolDevice_P->write( MDIO_ITEM, cmd );
                    // Wait until the address is set.
                    //ferolDevice_P->read( MDIO_ITEM, &tmp );
                    //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
                    ferolDevice_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                   
                    // Issue the read command
                    cmd = ((device & 0x1f) << 18) + 0x30000000;
                    //DEBUG("Read command. write " << MDIO_ITEM << " : " << std::hex << cmd);
                    ferolDevice_P->write( MDIO_ITEM, cmd );
                    // Wait until data is read by the 2 wire interface into the internal register.
                    //ferolDevice_P->read( MDIO_ITEM, &tmp );
                    //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
                    ferolDevice_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );

                    // read out the value
                    uint32_t rv;
                    ferolDevice_P->read( MDIO_ITEM, &rv);
                    //DEBUG( "To get the result read " << MDIO_ITEM << " : " << std::hex << rv );
                    data[ic] = (char)(0xff & rv);
                }
            mdiounlock();
        }
    catch( HAL::TimeoutException &e )
        {
            std::stringstream err;
            err << "Timeout while writing to the MDIO interface. (poll result: " << pollres << " )";
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
        }
    catch( HAL::HardwareAccessException &e )
        {
            std::stringstream err;
            err << "Problem accessing the MDIO hardware. " << e.what();
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
        }
}

