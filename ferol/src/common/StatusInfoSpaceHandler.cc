#include "ferol/StatusInfoSpaceHandler.hh"
#include "ferol/loggerMacros.h"

ferol::StatusInfoSpaceHandler::StatusInfoSpaceHandler( xdaq::Application* xdaq, 
                                                       utils::InfoSpaceUpdater *updater )
    : utils::InfoSpaceHandler( xdaq, "Status_IS", updater )
{
    // these values are set by the application and do not come "from outside"
    createuint32( "instance", 0, "", NOUPDATE );
    createuint32( "slotNumber", 0, "", NOUPDATE );
    createstring( "stateName", "uninitialized" );
    createstring( "subState", "" );
    createstring( "failedReason", "");
    createstring( "lockStatus", "unknown" );
}
