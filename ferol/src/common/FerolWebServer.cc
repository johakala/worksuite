#include "ferol/FerolWebServer.hh"
#include "ferol/FerolController.hh"
#include "ferol/loggerMacros.h"

ferol::FerolWebServer::FerolWebServer( utils::Monitor &monitor, 
                                       std::string url,
                                       std::string urn,
                                       Logger logger )
    : utils::WebServer( monitor, urn, url, logger )
{
    utils::WebTableTab *configTab = this->registerTableTab( "Config", 
                                                     "Configuration Items for application and hardware components.",
                                                     3 );

    configTab->registerTable( "Appl Config", 
                              "Items with which the Ferol have been configured.<br>They usually do not change during a run.", 
                              "Application Config" );

    configTab->registerTable( "FRL Config", 
                         "Items to configure the FRL.", 
                         "Frl Config" );

    configTab->registerTable( "FEROL Config", 
                         "Items to configure the FEROL.", 
                         "Ferol Config" );

    configTab->registerTable( "Stream 0 Config", 
                         "Items to configure the TCP Stream 0.", 
                         "Stream 0 Config" );

    configTab->registerTable( "Stream 1 Config", 
                         "Items to configure the TCP Stream 1.", 
                         "Stream 1 Config" );


    utils::WebTableTab *monTab = this->registerTableTab( "Monitoring", 
                                                  "Monitoring information for various components.", 3  );

    monTab->registerTable( "Application State", 
                         "Items from the FerolController appl.", 
                         "Application State" );

    monTab->registerTable( "FRL", 
                         "Items from the FRL Hardware", 
                         "FRL items" );

    monTab->registerTable( "FEROL", 
                         "Items FEROL Hardware", 
                         "FEROL" );

    monTab->registerTable( "FRL Stream 0", 
                         "FRL items related to Stream 0", 
                         "FRL Stream 0" );

    monTab->registerTable( "FRL Stream 1", 
                         "FRL items related to Stream 0", 
                         "FRL Stream 1" );

    // make an empty entry to leave empty this position in the table grid.
    monTab->registerTable( "__notab__", 
                         "Dummy", 
                         "" );

    monTab->registerTable( "Input Stream 0",
                           "Items from the input stream (Slink or SlinkExpress).",
                           "Input Stream 0" );

    monTab->registerTable( "Input Stream 1",
                           "Items from the input stream (Slink or SlinkExpress).",
                           "Input Stream 1" );

    // make an empty entry to leave empty this position in the table grid.
    monTab->registerTable( "__notab__", 
                         "Dummy", 
                         "" );

    monTab->registerTable( "TCP Stream 0",
                           "Items from the TCP/IP output stream.",
                           "TCP Stream 0" );

    monTab->registerTable( "TCP Stream 1",
                           "Items from the TCP/IP output stream.",
                           "TCP Stream 1" );

    // make an empty entry to leave empty this position in the table grid.
    monTab->registerTable( "__notab__", 
                         "Dummy", 
                         "" );

    monTab->registerTable( "FEROL Stream 0", 
                         "Items From the Ferol, TCP/IP Stream 0", 
                         "FEROL Stream 0" );

    monTab->registerTable( "FEROL Stream 1", 
                         "Items From the Ferol, TCP/IP Stream 1", 
                         "FEROL Stream 1" );




    utils::WebTableTab *sfpTab = this->registerTableTab( "SFP+ Info", 
                                                  "Monitoring information for the 10 Gbps SFP+ components.", 2  );


    sfpTab->registerTable( "SFP 1", 
                         "Monitoring data of the 10Gb/s SFP+ 1 (upper SFP+).<br>The SFP+ 1 is really the upper one! This is the SFP+ with the 10Gbb/s TCP/IP link to the DAQ.", 
                         "FEROL SFP 1" );

    sfpTab->registerTable( "SFP 0", 
                         "Monitoring data of the 10Gbb/s SFP 0 (lower SFP).<br>The SFP 0 is really the lower one! This is the SFP+ with the potential 10Gb/s input link for the AMC13v2.", 
                         "FEROL SFP 0" );

    sfpTab->registerTable( "Thresholds SFP 1",
                         "Thresholds for various quantities programmed into the SFP+",
                         "Thresholds SFP1" );

    sfpTab->registerTable( "Thresholds SFP 0",
                         "Thresholds for various quantities programmed into the SFP+",
                         "Thresholds SFP0" );
}


void 
ferol::FerolWebServer::printDebugTable( std::string name, std::list< utils::HardwareDebugItem > regList, xgi::Input *in, xgi::Output *out )
{
    printHeader( out );

    std::list< utils::HardwareDebugItem >::iterator it;

    *out << "<p class=itemTableTitle>" << name << "</p>\n";

    *out << "<table id=\"registerTable\" class=\"tablesorter\">\n<thead><tr><th>item</th><th>value</th><th>offset</th><th>description</th></tr></thead>\n<tbody>\n";
    for( it = regList.begin(); it != regList.end(); it++ )
        {
            *out << "<tr><td>" << (*it).item << "</td><td>" << (*it).valStr << "</td><td>" << (*it).adrStr << "</td><td>" << (*it).description << "</td></tr>\n";
        }
    *out << "</tbody>\n</table>\n";
    *out << "<script>\n $(document).ready(function() \n{\n $('#registerTable').tablesorter( { headers: { 1 : {sorter:false}, 3 : {sorter:false}}, sortList:  [[2,0]] }); \n}); \n</script>";
    printFooter( out );

}

std::string
ferol::FerolWebServer::getCgiPara( cgicc::Cgicc cgi, std::string name, std::string defaultval )
{
    cgicc::form_iterator iter = cgi.getElement(name);
    if ( iter == cgi.getElements().end() )
        {
            return defaultval;
        }
    else 
        {
            return **iter;
        }
}

void
ferol::FerolWebServer::expertDebugging( xgi::Input *in, xgi::Output *out, Ferol *ferol, Frl *frl,  FerolController *const fctl )
{
    cgicc::Cgicc cgi(in);
    uint32_t linkno = atoi( getCgiPara( cgi, "linkId", "0" ).c_str() );
    std::string command = getCgiPara( cgi, "command", "" );
    std::string parameter = getCgiPara( cgi, "parameter", "" );
    
    //std::cout << "\ncommand " << command << std::endl;
    //std::cout << "parameter " << parameter << std::endl;

  if ( command == "dumpRegisters" ) 
    {
      fctl->dumpHardwareRegisters();
      *out << "<p> Dumped registers in /tmp.</p>\n";
    }
  else if ( command == "resyncSlinkexpress" )
    {
      ferol->resyncSlinkExpress( linkno );
      *out << "<p>Did resync of Slink Express no " << linkno << ".</p>\n";
    }
  else if ( command == "softwareReset" )
    {
      ferol->resyncSlinkExpress( linkno );
      ferol->getFerolDevice()->setBit("SOFTWARE_RESET");
      *out << "<p>Did a softwareReset of the Ferol.</p>\n";
    }
  else if ( command == "daqOn" )
    {
      ferol->daqOn( linkno );
      *out << "<p>Did daqOn of Slink Express no " << linkno << ".</p>\n";
    }
  else if ( command == "daqOff" )
    {
      ferol->daqOff( linkno );
      *out << "<p>Did daqOff of Slink Express no " << linkno << ".</p>\n";
    }
  else if ( command == "generateFrlError" )
      {
          if ( parameter == "crc" )
              {
                  DEBUG("generating a crc error in the FRL" );
                  frl->getFrlDevice()->write( "gen_error", 1 );
              }
          else if ( parameter == "sizeError" ) 
              {
                  frl->getFrlDevice()->write( "gen_error", 2 );                  
              }
          else if ( parameter == "insertWrongEvt" ) 
              {
                  frl->getFrlDevice()->write( "gen_Outsync_gen", 1 );                  
             }
          else if ( parameter == "wrongEvtNr" ) 
              {
                  frl->getFrlDevice()->write( "gen_Outsync_gen", 2 );                  
              }
          else if ( parameter == "dropEvent" )
              {
                  frl->getFrlDevice()->write( "gen_Outsync_gen", 4 );                  
              }
          else if ( parameter == "skipEvents" )
              {
                  frl->getFrlDevice()->write( "gen_Outsync_gen", 8 );                  
              }
      }

  *out << "<form id=\"debugForm\" method=\"post\" >\n";

  *out << "<h3> Here you really need to know what you are doing... </h3>\n";
  *out << "<p>urn: " << urn_ << "<br>url: " << url_ << " </p>\n";
  *out << "<p><h3>General debugging features</h3>\n";
  *out << "<button onclick=\"ferolWebCommand( \'dumpRegisters\', \'\' )\">Register Dump</button> Dump all registers of the FRL/Ferol into files in /tmp<br>\n";
  *out << "<button onclick=\"ferolWebCommand( \'softwareReset\', \'\' )\"> Software reset of Ferol </button> Resets the Ferol. This is the main reset.<br>\n";
  *out << "</p><hr/>\n";

  *out << "<p><h3>Commands for a specific 5Gb Slink Express input link</h3>\n";
  *out << "<input type=\"hidden\" id=\"command\" name=\"command\" value=\"\"/>\n";
  *out << "<input type=\"hidden\" id=\"parameter\" name=\"parameter\" value=\"\"/>\n";
  *out << "5GB link number [0|1] : <input id=\"linkId\" name=\"linkId\" type=\"text\" value=\"0\"><br>\n";
  *out << "<button onclick=\"ferolWebCommand( \'resyncSlinkexpress\', \'\' )\"> Resync SlinkExpress </button> Send a resync command to the selected Slink Express.<br>\n";
  *out << "<button onclick=\"ferolWebCommand( \'daqOnSlinkexpress\', \'\' )\"> DaqOn </button> Send a daqOn command to the selected Slink Express.<br>\n";
  *out << "<button onclick=\"ferolWebCommand( \'daqOffSlinkexpress\', \'\' )\"> DaqOff </button> Send a daqOff command to the selected Slink Express.<br>\n";
  *out << "</p><hr/>\n";

  *out << "<p><h3>Generate artificial errors in the plastic events from the FRL event generator</h3>\n";
  *out << "<button onclick=\"ferolWebCommand( \'generateFrlError\', \'crc\' )\"> Generate CRC error </button> Generates an SLINK CRC error.<br>\n";
  *out << "<button onclick=\"ferolWebCommand( \'generateFrlError\', \'sizeError\' )\"> Generate wrong length</button> Generates an event with a wrong length in the trailer.<br>\n";
  *out << "<button onclick=\"ferolWebCommand( \'generateFrlError\', \'insertWrongEvt\' )\"> Insert Event </button> Generates an additional event with a wrong event number, e.g. 1,2,3,199,4,5,6... <br>\n";
  *out << "<button onclick=\"ferolWebCommand( \'generateFrlError\', \'wrongEvtNr\' )\"> Wrong event number </button> Generates an event with a wrong event number, e.g. 1,2,3,199,5,6... <br>\n";
  *out << "<button onclick=\"ferolWebCommand( \'generateFrlError\', \'dropEvent\' )\"> Miss an event </button> Drops one event number, e.g. 1,2,3,5,6... <br>\n";
  *out << "<button onclick=\"ferolWebCommand( \'generateFrlError\', \'skipEvents\' )\"> Skip events </button> Skip many events, e.g. 1,2,3,342,343... <br>\n";
  
  *out << "</p>\n";
  *out << "</p>To generate the \"wrong\" items the 2 comlement of the correct item is used. This holds for the event numbers, fragment length,  and the CRC.</p>\n";
  *out << "</form>\n";
}

void 
ferol::FerolWebServer::printHeader( xgi::Output * out )
{

    std::stringstream urn,url;
    out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
    *out << "\n\
<html>\n\
  <head>\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tab.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tablesort.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/properties.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol/html/ferol.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol/html/blue/style.css\">\n\
\n\
    <script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-2.1.0.min.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/ferol/html/jquery.tablesorter.min.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/xgi/html/tabpane.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/ferol/html/ferol.js\"></script>\n\
  </head>\n\
  <body>\n\
    <table class=\"ferolheader\"><tr>\
    <th>FEROL Controller</th>\n";
    *out << "<td><a href=\"" << url_ << "\">XDAQ page</a></td>\n";
    *out << "<td><a href=\"/" << urn_ << "\">Main</a></td>\n";
    *out << "<td><a href=\"/" << urn_ << "/debugFerol\"> Ferol-registers </a></td>\n"; 
    *out << "<td><a href=\"/" << urn_ << "/debugFrl\"> Frl-registers </a></td>\n"; 
    *out << "<td><a href=\"/" << urn_ << "/debugBridge\"> Bridge-registers </a></td>\n"; 
    *out << "<td><a href=\"/" << urn_ << "/debugSlinkExpress\"> SlinkExpress-Core-registers </a></td>\n"; 
    *out << "<td><a href=\"/" << urn_ << "/expertDebugPage\"> Expert Debugging </a></td>\n"; 
    *out << "</tr></table><hr>\n                  \
<div id=\"debug\">\n\
</div>\n\
";

}

