#include "ferol/FerolStreamInfoSpaceHandler.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/loggerMacros.h"

ferol::FerolStreamInfoSpaceHandler::FerolStreamInfoSpaceHandler( xdaq::Application *xdaq,
                                                                 std::string name,
                                                                 utils::InfoSpaceHandler *appIS,
                                                                 bool noAutoPush )
    : StreamInfoSpaceHandler( xdaq, name, NULL, appIS, noAutoPush )
{
    //    createuint32( "expectedFedId", "", "", "", NOUPDATE, NOUPDATE, "", "The fedid expected by the configuration for this input stream. This value is used as a key to identify the flashlist entries.");
}

void
ferol::FerolStreamInfoSpaceHandler::createstring( std::string name,
                                                  std::string frlHwName,
                                                  std::string ferol6GHwName,
                                                  std::string ferol10GHwName,
                                                  UpdateType frlUpdateType,
                                                  UpdateType ferolUpdateType,
                                                  std::string format,
                                                  std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   STRING, format, frlUpdateType,   doc);
    ISItem *itferol = new ISItem( name, ferol6GHwName, STRING, format, ferolUpdateType, doc);
    ISItem *it10Gferol = new ISItem( name, ferol10GHwName, STRING, format, ferolUpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol6GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol ) );
    ferol10GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol ) );

    // to do all the stuff in the base classes
    // StreamInfoSpaceHandler::createstring( name, "not initialised", format, frlUpdateType, doc );
}


void
ferol::FerolStreamInfoSpaceHandler::createuint32( std::string name,
                                                  std::string frlHwName,
                                                  std::string ferol6GHwName,
                                                  std::string ferol10GHwName,
                                                  UpdateType frlUpdateType,
                                                  UpdateType ferolUpdateType,
                                                  std::string format,
                                                  std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   UINT32, format, frlUpdateType,   doc);
    ISItem *itferol = new ISItem( name, ferol6GHwName, UINT32, format, ferolUpdateType, doc);
    ISItem *it10Gferol = new ISItem( name, ferol10GHwName, UINT32, format, ferolUpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol6GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol ) );
    ferol10GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol ) );

    // to do all the stuff in the base classes
    // StreamInfoSpaceHandler::createuint32( name, 0, format, frlUpdateType, doc );
}


void
ferol::FerolStreamInfoSpaceHandler::createuint64( std::string name,
                                                  std::string frlHwName,
                                                  std::string ferol6GHwName,
                                                  std::string ferol10GHwName,
                                                  UpdateType frlUpdateType,
                                                  UpdateType ferolUpdateType,
                                                  std::string format,
                                                  std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   UINT64, format, frlUpdateType,   doc);
    ISItem *itferol = new ISItem( name, ferol6GHwName, UINT64, format, ferolUpdateType, doc);
    ISItem *it10Gferol = new ISItem( name, ferol10GHwName, UINT64, format, ferolUpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol6GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol ) );
    ferol10GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol ) );

    // to do all the stuff in the base classes
    //StreamInfoSpaceHandler::createuint64( name, 0, format, frlUpdateType, doc );

}


void
ferol::FerolStreamInfoSpaceHandler::createdouble( std::string name,
                                                  std::string frlHwName,
                                                  std::string ferol6GHwName,
                                                  std::string ferol10GHwName,
                                                  UpdateType frlUpdateType,
                                                  UpdateType ferolUpdateType,
                                                  std::string format,
                                                  std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   DOUBLE, format, frlUpdateType,   doc);
    ISItem *itferol = new ISItem( name, ferol6GHwName, DOUBLE, format, ferolUpdateType, doc);
    ISItem *it10Gferol = new ISItem( name, ferol10GHwName, DOUBLE, format, ferolUpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol6GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol ) );
    ferol10GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol ) );

    // to do all the stuff in the base classes
    //StreamInfoSpaceHandler::createdouble( name, 0., format, frlUpdateType, doc );

}


void
ferol::FerolStreamInfoSpaceHandler::createbool( std::string name,
                                                std::string frlHwName,
                                                std::string ferol6GHwName,
                                                std::string ferol10GHwName,
                                                UpdateType frlUpdateType,
                                                UpdateType ferolUpdateType,
                                                std::string format,
                                                std::string doc)
                                                  
{
    ISItem *itfrl   = new ISItem( name, frlHwName,   BOOL, format, frlUpdateType,   doc);
    ISItem *itferol = new ISItem( name, ferol6GHwName, BOOL, format, ferolUpdateType, doc);
    ISItem *it10Gferol = new ISItem( name, ferol10GHwName, BOOL, format, ferolUpdateType, doc);
    frlItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itfrl ) );
    ferol6GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *itferol ) );
    ferol10GItemMap_.insert( std::make_pair< std::string, ISItem& > ( name, *it10Gferol ) );

    // to do all the stuff in the base classes
    //StreamInfoSpaceHandler::createbool( name, false, format, frlUpdateType, doc );

}

void
ferol::FerolStreamInfoSpaceHandler::setInputSource( HwInput inputSource )
{

    INFO("SETTING INPUT SOURCE in FerolStreamInfoSpaceHandler");

    inputSource_ = inputSource;
    std::tr1::unordered_map< std::string, ISItem&>::iterator it;
    std::tr1::unordered_map< std::string, ISItem&> cmap;
    if ( inputSource_ == FEROL6G )
        {
            it = ferol6GItemMap_.begin();
            cmap = ferol6GItemMap_;
        }
    else if ( inputSource_ == FEROL10G )
        {
            it = ferol10GItemMap_.begin();
            cmap = ferol10GItemMap_;
        }
    else if ( inputSource_ == FRL )
        {
            it = frlItemMap_.begin();
            cmap = frlItemMap_;
        }
    else
        {
            std::cout << "Heavy software bug in FerolStreamInfoSpaceHandler::setInputSource...";
        }


    itemlock_.take();

    //itemMap_.clear();
    for ( ; it != cmap.end(); it++ ) 
        {
            ISItem &isitem = (*it).second;
            std::string name = isitem.name;
            //DEBUG( "loop " << name);
            if ( itemMap_.find( name ) != itemMap_.end() ) 
                {
                    std::string err = "An element with the name \"" + name + "\" exists already in this infospace.";
                    XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
                    xdaq_P->notifyQualified( "fatal", top );
                    ERROR( err );                    
                }

            itemMap_.insert( std::make_pair< std::string, ISItem&> ( name, (*it).second ) );
            //DEBUG("insterted in tiem map");
            is_P->lock();
            if ( cpis_P )
                cpis_P->lock();
            if ( isitem.type == STRING )
                {
                    xdata::String *ptr = new xdata::String( "not initialized" );
                    stringMap_.insert( std::make_pair< std::string, std::pair< std::string, xdata::String* > >
                                       ( name, std::make_pair< std::string, xdata::String* > ( "not initialized", ptr )));
                    std::vector<std::string> vals;
                    vals.push_back( "uninitialised" );
                    vals.push_back( "uninitialised" );
                    stringStreamValues_[name] = vals;

                    is_P->fireItemAvailable( name, ptr );
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );
                }
            else if( isitem.type == UINT32 )
                {
                    //DEBUG("uin32");
                    xdata::UnsignedInteger32 *ptr = new xdata::UnsignedInteger32( 0 );
                    uint32Map_.insert( std::make_pair< std::string, std::pair< uint32_t, xdata::UnsignedInteger32* > >
                                       ( name, std::make_pair< uint32_t, xdata::UnsignedInteger32* > ( 0, ptr )));
                    std::vector<uint32_t> vals;
                    vals.push_back( 0 );
                    vals.push_back( 0 );
                    uint32StreamValues_[name] = vals;
                    //DEBUG("inserted in map");

                    is_P->fireItemAvailable( name, ptr );
                    //DEBUG( "filred tiem ava");
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );

                    //DEBUG( "IS fired ");
                }
            else if ( isitem.type == UINT64 )
                {
                    xdata::UnsignedInteger64 *ptr = new xdata::UnsignedInteger64( 0 );
                    uint64Map_.insert( std::make_pair< std::string, std::pair< uint64_t, xdata::UnsignedInteger64* > >
                                       ( name, std::make_pair< uint64_t, xdata::UnsignedInteger64* > ( 0, ptr )));
                    std::vector<uint64_t> vals;
                    vals.push_back( 0 );
                    vals.push_back( 0 );
                    uint64StreamValues_[name] = vals;
                    is_P->fireItemAvailable( name, ptr );
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );
               }
            else if ( isitem.type == DOUBLE )
                {
                    xdata::Double *ptr = new xdata::Double( 0.0 );
                    doubleMap_.insert( std::make_pair< std::string, std::pair< double, xdata::Double* > >
                                       ( name, std::make_pair< double, xdata::Double* > ( 0.0, ptr )));
                    std::vector<double> vals;
                    vals.push_back( 0. );
                    vals.push_back( 0. );
                    doubleStreamValues_[name] = vals;
                    is_P->fireItemAvailable( name, ptr );
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );
               }
            else if ( isitem.type == BOOL )
                {
                    xdata::Boolean *ptr = new xdata::Boolean( false );
                    boolMap_.insert( std::make_pair< std::string, std::pair< bool, xdata::Boolean* > >
                                     ( name, std::make_pair< bool, xdata::Boolean* > ( false, ptr )));
                    std::vector<bool> vals;
                    vals.push_back( false );
                    vals.push_back( false );
                    boolStreamValues_[name] = vals;
                    is_P->fireItemAvailable( name, ptr );
                    if ( cpis_P )
                        cpis_P->fireItemAvailable( name, ptr );
                }
            else
                {
                    ERROR("Unknown type for item " << isitem.name << "  => " << isitem.type );
                }


            if ( cpis_P )
                cpis_P->unlock();
            is_P->unlock();
            
            //DEBUG("add docs");
            addParameterDocumentation( name, "string", getFormatted( name, 0, isitem.format ), isitem.update, isitem.documentation );
            //DEBUG("done");
            
        }
    itemlock_.give();
    DEBUG("setting done");
}

