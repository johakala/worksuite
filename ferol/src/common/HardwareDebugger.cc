#include "ferol/HardwareDebugger.hh"
#include "d2s/utils/SOAPFSMHelper.hh"
#include "ferol/SlinkExpressCore.hh"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include <iomanip>
#include "ferol/loggerMacros.h"

ferol::HardwareDebugger::HardwareDebugger( Logger logger, ferol::Ferol *ferol, ferol::Frl *frl)
    : logger_( logger )
{
    ferolDevice_P = ferol->getFerolDevice();
    slot_ = ferol->getSlot();
    slexp_P = ferol->getSlinkExpressCore();
    if ( frl ) 
        {
            frlDevice_P = frl->getFrlDevice();
            bridgeDevice_P = frl->getBridgeDevice();
        } 
    else 
        {
            frlDevice_P = NULL;
            bridgeDevice_P = NULL;
        }
}


std::list<utils::HardwareDebugItem>
ferol::HardwareDebugger::getFerolRegisters()
{
    return getPCIRegisters( ferolDevice_P );
}

std::list<utils::HardwareDebugItem>
ferol::HardwareDebugger::getFrlRegisters()
{
    return getPCIRegisters( frlDevice_P );
}
std::list<utils::HardwareDebugItem>
ferol::HardwareDebugger::getBridgeRegisters()
{
    return getPCIRegisters( bridgeDevice_P );
}

void
ferol::HardwareDebugger::dumpHardwareRegisters( std::string suffix )
{

    std::list<utils::HardwareDebugItem> ferolRegisters = getPCIRegisters( ferolDevice_P );
    std::list<utils::HardwareDebugItem> sle0Registers = getSlinkExpressRegisters( 0 );
    std::list<utils::HardwareDebugItem> sle1Registers = getSlinkExpressRegisters( 1 );
    dumpRegistersToFile( "Ferol", ferolRegisters, suffix );
    dumpRegistersToFile( "SlinkExpress_0", sle0Registers, suffix );
    dumpRegistersToFile( "SlinkExpress_1", sle1Registers, suffix );

    if ( frlDevice_P )
        {
            std::list<utils::HardwareDebugItem> frlRegisters = getPCIRegisters( frlDevice_P );
            std::list<utils::HardwareDebugItem> bridgeRegisters = getPCIRegisters( bridgeDevice_P );
            dumpRegistersToFile( "Frl", frlRegisters, suffix );
            dumpRegistersToFile( "Bridge", bridgeRegisters, suffix );
        }
}

void 
ferol::HardwareDebugger::dumpRegistersToFile( std::string name, 
                                              const std::list<utils::HardwareDebugItem> &registers, 
                                              std::string suffix )
{

    time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime (&rawtime);
    char timestr[256];
    strftime( timestr, 256, "%Y-%m-%d_%H-%M-%S", timeinfo );

    std::stringstream fileName;
    fileName << "/tmp/registerDump_slot_" << slot_ << "_" << name << "_" << timestr << suffix << ".csv" << std::ends; 
    std::ofstream fileStream( fileName.str().c_str() );
    if ( ! fileStream ) 
        {
            ERROR( "Cannot open file : " << fileName.str() );
            return;
        }
    
    fileStream << "itemName,adress_hex,value_hex,value,description" << std::endl;

    std::list<utils::HardwareDebugItem>::const_iterator it = registers.begin();
    for ( ; it != registers.end(); it++ )
        {
            fileStream << (*it).item << "," 
                       << (*it).adrStr << "," 
                       << (*it).valStr << "," 
                       << (*it).value << "," 
                       << (*it).description << std::endl;
        }
    fileStream.close();
}



std::list< utils::HardwareDebugItem >
ferol::HardwareDebugger::getSlinkExpressRegisters( uint32_t linkno )
{
    std::list<utils::HardwareDebugItem> itemList;
    if ( ferolDevice_P == 0 )
        {
            return itemList;
        }

    if ( slexp_P == 0 )
        {
            return itemList;
        }

    const HAL::AddressTableInterface &adrTab = slexp_P->getAddressTableInterface();
    std::tr1::unordered_map<std::string, HAL::AddressTableItem*, HAL::HalHash<std::string> >::const_iterator it = adrTab.getItemListBegin();
    std::string item, desc;
    uint32_t value, address;
    std::stringstream valueStr, addressStr;
    try
        {
            for ( ; it != adrTab.getItemListEnd(); it++ )
                {
                    if ( ! (*it).second->isReadable() ) continue;
                    
                    item    = (*it).first;
                    desc    = (*it).second->getDescription();
                    address = (*it).second->getGeneralHardwareAddress().getAddress();
                    slexp_P->read( item, &value, linkno );
                    addressStr.str("");
                    valueStr.str("");
                    addressStr << "0x" << std::hex << std::setw(8) << std::setfill('0') << address;
                    valueStr   << "0x" << std::hex << std::setw(8) << std::setfill('0') << value;
                    utils::HardwareDebugItem hwitem( item, desc, addressStr.str(), valueStr.str(), address, value); 
                    itemList.push_back( hwitem );
                }
        } 
    catch( utils::exception::FerolException &e )
        {
            INFO( e.what() );
        }
    return itemList;
}

std::list< utils::HardwareDebugItem >
ferol::HardwareDebugger::getPCIRegisters( HAL::HardwareDeviceInterface *device )
{

    std::list<utils::HardwareDebugItem> itemList;
    if ( device == 0 )
        {
            return itemList;
        }
    const HAL::AddressTableInterface &adrTab = device->getAddressTableInterface();
    std::tr1::unordered_map<std::string, HAL::AddressTableItem*, HAL::HalHash<std::string> >::const_iterator it = adrTab.getItemListBegin();
    std::string item, desc;
    uint32_t value, address;
    std::stringstream valueStr, addressStr;
    for ( ; it != adrTab.getItemListEnd(); it++ )
        {
            if ( ! (*it).second->isReadable() ) continue;

            item    = (*it).first;
            desc    = (*it).second->getDescription();
            address = (*it).second->getGeneralHardwareAddress().getAddress();
            device->read( item, &value );
            addressStr.str("");
            valueStr.str("");
            addressStr << "0x" << std::hex << std::setw(8) << std::setfill('0') << address;
            valueStr   << "0x" << std::hex << std::setw(8) << std::setfill('0') << value;
            utils::HardwareDebugItem hwitem( item, desc, addressStr.str(), valueStr.str(), address, value); 
            itemList.push_back( hwitem );
        }
    return itemList;
}

///////////////////////////////////////////// SOAP debugging functions ///////////////////////////////////////////////////////


//  Format of the read message:
//
// <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
//   <SOAP-ENV:Header>
//   </SOAP-ENV:Header>
//   <SOAP-ENV:Body>
//
//     <xdaq:readItem xmlns:xdaq=\"urn:xdaq-soap:3.0\" device = \"deviceType\" item=\"itemName\" offset=\"0\" />
//
//   </SOAP-ENV:Body>
// </SOAP-ENV:Envelope>
// 
// where deviceType is frl ferol or bridge

xoap::MessageReference ferol::HardwareDebugger::readItem( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{
  
    uint32_t result = 0;
    uint32_t offset = 0;
    std::string item = "";
  
    xoap::SOAPPart part = msg->getSOAPPart();
    xoap::SOAPEnvelope env = part.getEnvelope();
    xoap::SOAPBody body = env.getBody();
  
    xoap::SOAPName command("ReadItem","","");
    std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
    if ( bodyElements.size() != 1 ) { 
        return utils::SOAPFSMHelper::makeSoapFaultReply( "ReadItemResponse", "syntax error in request: wrong bodyElementCount" );
    }
  
    xoap::SOAPName offsetName("offset","","");
    try {
        std::string offsetStr = bodyElements[0].getAttributeValue( offsetName );
        offset = strtoul( offsetStr.c_str(), NULL, 0);
    } catch ( ... ) {
        // nothing to be done since no offset is legal
    }
  
    std::string deviceStr = "ferol";
    xoap::SOAPName deviceName("device","","");
    try {
        deviceStr = bodyElements[0].getAttributeValue( deviceName );
    } catch ( ... ) {
        // nothing to be done since no offset is legal
    }
    
    HAL::HardwareDeviceInterface *devicePtr = ferolDevice_P;
    if ( deviceStr == "frl" )
        devicePtr = frlDevice_P;
    else if( deviceStr == "bridge" )
        devicePtr = bridgeDevice_P;

    xoap::SOAPName itemName("item","","");
    try {
        item = bodyElements[0].getAttributeValue( itemName );
    } catch ( ... ) {
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "ReadItemResponse", 
                                                          "no item attribute in ReadItem soap request");
    }
  
    try { 
        if ( devicePtr ) {
            devicePtr->read( item, &result, offset );
        } else { 
            //WARN("FerolControllerCard not initialized." );
            return  utils::SOAPFSMHelper::makeSoapFaultReply( "ReadItemResponse", 
                                                              "PCI Device not initialized.");
        }
    } catch (HAL::HardwareAccessException& e) {
        std::string msg( e.what() );
        //ERROR(msg);
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "ReadItemResponse", msg );
    }
  
    // send back a reply:
    std::stringstream resultStr;
    resultStr << result;
    return  utils::SOAPFSMHelper::makeSoapReply( "ReadItemResponse", resultStr.str());
  
}

//////////////////////////////////////// Debugging only /////////////////////////

//  Format of the write message:
//
// <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
//   <SOAP-ENV:Header>
//   </SOAP-ENV:Header>
//   <SOAP-ENV:Body>
//
//     <xdaq:writeItem xmlns:xdaq=\"urn:xdaq-soap:3.0\" device=\"deviceType\" item=\"itemName\" data=\"value\" offset=\"0\" />
//
//   </SOAP-ENV:Body>
// </SOAP-ENV:Envelope>
// 
// where deviceType is frl ferol or bridge


xoap::MessageReference ferol::HardwareDebugger::writeItem( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{
  
    uint32_t data;
    uint32_t offset = 0;
    std::string   item = "";

    xoap::SOAPPart part = msg->getSOAPPart();
    xoap::SOAPEnvelope env = part.getEnvelope();
    xoap::SOAPBody body = env.getBody();

    xoap::SOAPName command("WriteItem","","");
    std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
    if ( bodyElements.size() != 1 ) { 
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", "syntax error in request: wrong bodyElementCount" );
    }
  
    xoap::SOAPName dataName("data","","");
    std::string dataStr;
    try {
        dataStr = bodyElements[0].getAttributeValue( dataName );
        if ( dataStr == "" ) {
            return  utils::SOAPFSMHelper::makeSoapFaultReply("WriteItemResponse", "No data found in WriteItemRequest!" );
        }
        data = strtoul( dataStr.c_str(), NULL, 0);
    } catch ( ... ) {
        // should not happen
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", 
                                                          toolbox::toString( "Error during preparation of data from SOAP Request \"WriteItem\": %s",
                                                                             dataStr.c_str() ) );
    }
    
    xoap::SOAPName offsetName("offset","","");
    try {
        std::string offsetStr = bodyElements[0].getAttributeValue( offsetName );
        offset = strtoul( offsetStr.c_str(), NULL, 0);
    } catch ( ... ) {
        // nothing to be done since no offset is legal
    }
    
    std::string deviceStr = "ferol";
    xoap::SOAPName deviceName("device","","");
    try {
        deviceStr = bodyElements[0].getAttributeValue( deviceName );
    } catch ( ... ) {
        // nothing to be done since no offset is legal
    }
    
    HAL::HardwareDeviceInterface *devicePtr = ferolDevice_P;
    if ( deviceStr == "frl" )
        devicePtr = frlDevice_P;
    else if( deviceStr == "bridge" )
        devicePtr = bridgeDevice_P;

    xoap::SOAPName itemName("item","","");
    try {
        item = bodyElements[0].getAttributeValue( itemName );
    } catch ( ... ) {
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", "no item attribute in WriteItem soap request" );
    }
    
    try { 
        if ( devicePtr ) {
            devicePtr->write( item, data, HAL::HAL_NO_VERIFY, offset );
        } else { 
            //WARN(toolbox::toString("FerolControllerCard not initialized.");
            return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", 
                                                              "PCI Device not initialized." );
        }
    } catch (HAL::HardwareAccessException& e) {
        std::string msg( e.what() );
        //ERROR(msg);
        return  utils::SOAPFSMHelper::makeSoapFaultReply( "WriteItemResponse", msg );
    }
  
    return  utils::SOAPFSMHelper::makeSoapReply( "WriteItemResponse", ""); 
}



