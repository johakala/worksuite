#include "ferol/ConfigSpaceSaver.hh"

ferol::ConfigSpaceSaver::ConfigSpaceSaver( HAL::PCIDevice **deviceH )
{
    device_H = deviceH;
}

void
ferol::ConfigSpaceSaver::writeConfigSpace()
    throw( HAL::HardwareAccessException )
{
    for ( uint32_t ioff = 0; ioff < 0x40; ioff += 4 ) 
        {
            (*device_H)->write( "ConfigStart", configSpace_[ioff/4], HAL::HAL_NO_VERIFY, ioff );
        }
}

void
ferol::ConfigSpaceSaver::saveConfigSpace()
    throw( HAL::HardwareAccessException )
{
    for ( uint32_t ioff = 0; ioff < 0x40; ioff += 4 ) 
        {
            (*device_H)->read( "ConfigStart", &configSpace_[ioff/4], ioff );
        }
}

void
ferol::ConfigSpaceSaver::rescanBAbits()
    throw( HAL::HardwareAccessException )
{
    for (uint32_t i=4;i<7;i++) 
        {
            (*device_H)->write("ConfigStart", 0xffffffff, HAL::HAL_NO_VERIFY, i*4);
        }
    uint32_t dummy; 
    for (uint32_t i=4;i<7;i++) 
        {
            (*device_H)->read("ConfigStart", &dummy, i*4);
        }
}

//void
//ferol::ConfigSpaceSaver::hwlock(toolbox::BSEM &lock)
//{
//    lock.take();
//}
//
//void
//ferol::ConfigSpaceSaver::hwunlock(toolbox::BSEM &lock)
//{
//    lock.give();
//}

//void ferol::ConfigSpaceSaver::dumpConfigSpace( uint32_t *csp_p ) const {
//
//    for ( uint32_t i = 0; i < 0x40; i += 4 ) {
//        if ( i % 16 == 0 ) {
//            std::cout << std::endl << std::hex << std::setw(8) << std::setfill('0') << i << " :";
//        }
//        std::cout << " " << std::hex << std::setw(8) << std::setfill('0') << csp_p[i/4];
//    }
//    std::cout << std::endl;    
//
//}

