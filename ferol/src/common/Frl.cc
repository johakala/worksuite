#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include "ferol/Frl.hh"
#include "ferol/BridgeAddressTableReader.hh"
#include "ferol/FrlEventGenerator.hh"
#include "hal/PCIAddressTableASCIIReader.hh"
#include "ferol/loggerMacros.h"
#include "ferol/ferolConstants.h"
#include "xcept/tools.h"

ferol::Frl::Frl( xdaq::Application *xdaq,
                 ferol::ApplicationInfoSpaceHandler &appIS,
                 ferol::FerolMonitor &monitor,
                 ferol::HardwareLocker &hwLocker )
   : ConfigSpaceSaver( &frlDevice_P ), 
      logger_( xdaq->getApplicationLogger() ),
      xdaq_ ( xdaq ),
      appIS_( appIS),
      monitor_( monitor ),
      hwLocker_( hwLocker ),
      frlIS_( xdaq, this ),
      busAdapter_(),
      deviceLock_(toolbox::BSem::FULL)
{
    hwCreated_     = false;
    stream0_       = false;
    stream1_       = false;
    checkWrongId0_ = true;
    checkWrongId1_ = true;
    fwChecker_P    = NULL;
    bridgeDevice_P = NULL;
    frlDevice_P    = NULL;
    slot_          = 0;
    index_         = 0;
    operationMode_ = "n.a.";
    dataSource_    = "n.a.";

    monitor_.addInfoSpace( &frlIS_ );

    try
        {
            // The Addresstables need to be created only once
            HAL::PCIAddressTableASCIIReader frlReader( FRL_ADDRESSTABLE_FILE );
            BridgeAddressTableReader bridgeReader;
            frlTable_P = new HAL::PCIAddressTable( "FRL Addresstable", frlReader );
            bridgeTable_P = new HAL::PCIAddressTable( "FRL-Bridge Addresstable",  bridgeReader );
        }
    catch (HAL::HardwareAccessException &e )
        {
            std::string msg = toolbox::toString("PROGRAM WILL END!!! Could not create AddressTables for Frl in Constructor. %s", 
                                                xcept::stdformat_exception_history(e).c_str()); 
            FATAL( msg );
            exit(-1);
        }
    
}


ferol::Frl::~Frl()
{

    shutdownHwDevice();

    // from the constructor: lifecycle is the lifecycle of this object.
    delete frlTable_P;
    delete bridgeTable_P;
}

void 
ferol::Frl::shutdownHwDevice()
    throw ( utils::exception::FrlException )
{

    // We must protect the destruction of the hardware since the monitoring 
    // task could be active and be reading out stuff from the hardware. We 
    // ensure not to destroy in the middle of this update. 
    this->hwlock();

    hwCreated_ = false;
    index_ = 0;

    // from the createHwDevice call: lifecycle is from the call to
    // createHwDevice until the call to shutownHwDevice()

    delete slinkStream0_P;
    slinkStream0_P = NULL;
    delete slinkStream1_P;
    slinkStream1_P = NULL;

    if ( fwChecker_P ) 
        {
            delete fwChecker_P;
            fwChecker_P = NULL;
        }

    if ( frlDevice_P ) 
        {
            std::cout << "shutting donw hw" << std::endl;
            delete frlDevice_P;
            frlDevice_P = NULL;
        }

    if ( bridgeDevice_P ) 
        {
            bridgeDevice_P = NULL;
            delete bridgeDevice_P;
        }

    this->hwunlock();
}


/**
 * Algorithm: Scan through all possible PCI indices (0-15) and probe if 
 * an FRL is present at this index. If an FRL is found, read out the slot
 * number for the corresponding FRL (the slot number is read from the 
 * backplane: It has some lines which for each slot encodes the slot number.
 * These lines are mapped to a register in the Bridge FPGA). If the read
 * slot number is equal to the slotnumber which has been given to this 
 * program as configuration parameter, we have found the correct frl.
 **/
bool
ferol::Frl::createHwDevice( ConfigSpaceSaver *ferolCfgIF )
    throw ( utils::exception::Exception )
{
    // we only touch hardware if we have the lock. In principle this check was already
    // done in the FerolController, but to have this class not depend on this check we
    // better do it here. 
    if ( ! hwLocker_.lockedByUs() )
        return false;

    // Scan PCI to find slot
    uint32_t slotread;
    bool changeFw = false;
    uint32_t requiredSlot = appIS_.getuint32( "slotNumber" );

    for ( uint32_t i=0; i<18; i++ ) 
	{
            try 
		{
                    // open first the bridge/spy device, to read the geographic slot
                    INFO( "make bridge");
                    HAL::PCIDevice* dev = new HAL::PCIDevice(*bridgeTable_P, 
                                                             busAdapter_, 
                                                             BRIDGE_VENDORID, 
                                                             BRIDGE_DEVICEID, 
                                                             i, 
                                                             false);
                    dev->read("geogrSlot", &slotread);
                    INFO( "look for FRL in slot " << requiredSlot << ";  read geoslot " << slotread << " for PCI index " << i );

                    if ( slotread == 0 || slotread > 21 ) 
			{
                            std::ostringstream msg; 
                            msg <<  "Geographic Slot badly read." << slotread << std::endl;
                            FATAL( msg.str() );
                            XCEPT_RAISE( utils::exception::HardwareAccessFailed, msg.str() );
			}

                    if( requiredSlot == slotread ) 
			{
                            bridgeDevice_P = dev;
                            bridgeDevice_P->writePulse( "softReset" );
                            ::usleep( 1000 );  // It takes 82us
                            DEBUG("did a softReset of the bridge");

                            frlDevice_P    = new HAL::PCIDevice( *frlTable_P, 
                                                                 busAdapter_,
                                                                 FRL_VENDORID,
                                                                 FRL_DEVICEID,
                                                                 i, 
                                                                 false);
                            // check the firmware types loaded and the versions:
                            fwChecker_P = new ferol::FrlFirmwareChecker( frlDevice_P, bridgeDevice_P, this, ferolCfgIF,appIS_.getbool( "noFirmwareVersionCheck" ) );


                            frlIS_.setuint32( "FrlHwRevision", fwChecker_P->getFrlHardwareRevision() );
                            frlIS_.setuint32( "FrlFwType", fwChecker_P->getFrlFirmwareType() );
                            frlIS_.setuint32( "FrlFwVersion", fwChecker_P->getFrlFirmwareVersion() );
                            frlIS_.setuint32( "BridgeHwRevision", fwChecker_P->getBridgeHardwareRevision() );
                            frlIS_.setuint32( "BridgeFwType", fwChecker_P->getBridgeFirmwareType() );
                            frlIS_.setuint32( "BridgeFwVersion", fwChecker_P->getBridgeFirmwareVersion() );
                            frlIS_.setuint32( "slotNumber", slotread );

                            std::string errorstr;
                            if ( ! fwChecker_P->checkFirmware( operationMode_, dataSource_, errorstr, &changeFw ) )
                                {
                                    XCEPT_RAISE( utils::exception::FrlException, errorstr );
                                }

                            if ( changeFw )
                                {
                                    bridgeDevice_P->writePulse( "softReset" );
                                    ::usleep( 1000 ); // It takes 82us
                                    DEBUG("Did a softReset of the bridge after firmware change.");
                                }
                            frlIS_.setbool( "FrlFwChanged", changeFw );

                            // the frl firmware versions might change during the check
                            frlIS_.setuint32( "FrlHwRevision", fwChecker_P->getFrlHardwareRevision() );
                            frlIS_.setuint32( "FrlFwType", fwChecker_P->getFrlFirmwareType() );
                            frlIS_.setuint32( "FrlFwVersion", fwChecker_P->getFrlFirmwareVersion() );

                            // Once the hardware is created the SlinkStream Objects are created, too
                            // They are created In any case (also if not activated)
                            
                            slinkStream0_P = new SlinkStream( logger_, appIS_, frlIS_, frlDevice_P, bridgeDevice_P, 0 );
                            slinkStream1_P = new SlinkStream( logger_, appIS_, frlIS_, frlDevice_P, bridgeDevice_P, 1 );

                            // read some monitoring info which are imutable and only need to be read once
                            uint32_t data;

                            frlDevice_P->read( "FEROL_BA", &data );
                            frlIS_.setuint32( "FEROL_BA", data );


                            hwCreated_ = true;
                           
                            index_ = i;
                            DEBUG( "created the Frl device.");

                            break;
			} 
                    else 
			{
                            delete dev;
			}
		} 
            catch(HAL::NoSuchDeviceException & e) 
		{
                    DEBUG("No Frl card found at index." << i );
		}
            catch(HAL::HardwareAccessException &e)
                {
                    FATAL( "Hardware Access failed: Could not create Frl PCIDevice and read geo slot.");
		    XCEPT_RETHROW( utils::exception::FrlException, 
                                   "Failed to create Frl PCIDevice and read the geo-slot.", e );		    
                }
	}
    
    // If we have not found the frl there is something wrong...
    if( ! hwCreated_ ) 
	{
            std::ostringstream msg; 
            msg << "No FRLCard in geographic slot " << appIS_.getuint32( "slotNumber" );
            ERROR( msg.str() );
            XCEPT_RAISE(utils::exception::FrlException , msg.str() );
	} 
    return changeFw;
}

////////////////////////////////////////////////////////////////////////

bool
ferol::Frl::configure( ConfigSpaceSaver *ferolCfgIF )
    throw ( utils::exception::FrlException )
{


    // initialization of parameters for the run
    checkWrongId0_ = true;
    checkWrongId1_ = true;
    // get some essential parameters from the application infospace
    INFO("3 frl");
    stream0_ = appIS_.getbool( "enableStream0");
    stream1_ = appIS_.getbool( "enableStream1");   
    INFO("3 done");
    operationMode_ = appIS_.getstring( "OperationMode" );
    dataSource_    = appIS_.getstring( "DataSource" );    
    slot_ = appIS_.getuint32( "slotNumber" );

    // try to create the hardware device
    bool changeFw = this->createHwDevice( ferolCfgIF );

    // if no Hardware was created there is no point to continue
    if ( ! hwCreated_ ) return changeFw;    

    try
        {
	    frlDevice_P->resetBit( "DAQ_mode" );    // sets the slink in command mode. Also resets/flushes the internal fifo in sender.
            ::usleep(2);         // sending the bit to the slinks takes a bit of time. 
	    frlDevice_P->setBit( "softwareReset" );
            frlDevice_P->resetBit("enableFifoMonCounter");
            frlDevice_P->resetBit("enableHistograms");
            frlDevice_P->write("max_frag_size", appIS_.getuint32( "Maximal_fragment_size") );
            slinkStream0_P->setExpectedSourceId();
            slinkStream1_P->setExpectedSourceId();
        }
    catch( HAL::HardwareAccessException &e)
        {
	    std::string msg( e.what() );
	    ERROR( msg );
	    XCEPT_RETHROW( utils::exception::FrlException, "Problem to configure FRL FPGA", e );
        }
    catch( utils::exception::HardwareAccessFailed &e )
        {
	    std::string msg( e.what() );
	    ERROR( msg );
	    XCEPT_RETHROW( utils::exception::FrlException, "Problem to configure FRL FPGA", e );
        }
  
    

    // Only in DAQ mode. 
    if ( dataSource_ == SLINK_SOURCE ) 
        {
            try 
                {
                    // the following commands only act on enabled streams. 
                    slinkStream0_P->setDCBalance( appIS_.getbool( "enableDCBalance" ));
                    if ( appIS_.getbool( "enableDeskew" ) ) {                        
                        slinkStream0_P->deskewLink();
                    }
                    slinkStream0_P->testSlink( appIS_.getuint32("testDurationMs") );
                    
                    slinkStream1_P->setDCBalance( appIS_.getbool( "enableDCBalance" ));
                    if ( appIS_.getbool( "enableDeskew" ) ) {                        
                        slinkStream1_P->deskewLink();
                    }
                    slinkStream1_P->testSlink( appIS_.getuint32("testDurationMs") );

                    slinkStream0_P->enable();
                    slinkStream1_P->enable();
                }
            catch (utils::exception::Exception & e)
                {                            
                    std::string msg =  toolbox::toString( "Failed to prepare SLINKs: %s.", 
                                                          xcept::stdformat_exception_history(e).c_str());
                    ERROR( msg );
                    XCEPT_RETHROW( utils::exception::FrlException, msg, e );		    
                }
        }
    return changeFw;
}

//////////////////////////////////////////////// stop ////////////////////////////////////

void
ferol::Frl::stop( )
    throw ( utils::exception::FrlException )
{

    if ( ! hwCreated_ ) return;

    try
        {


            if ( dataSource_ == GENERATOR_SOURCE )
                {
                    frlDevice_P->resetBit("gen_startGenerator");
                    usleep( 200000 );
                }
	    frlDevice_P->setBit( "softwareReset" );
	    frlDevice_P->resetBit( "DAQ_mode" );    // sets the slink in command mode 
            frlDevice_P->resetBit("enableFifoMonCounter");
            frlDevice_P->resetBit("enableHistograms");
            uint32_t maxFragPostReset;
            frlDevice_P->read( "max_frag_size", &maxFragPostReset );
            if (maxFragPostReset != 0xffffff) {
                WARN("Max Frag Size after reset is not at 0x00ffffff but at " << std::hex << maxFragPostReset );
            }
            frlDevice_P->write("max_frag_size", appIS_.getuint32( "Maximal_fragment_size") );
            slinkStream0_P->setExpectedSourceId();
            slinkStream1_P->setExpectedSourceId();

        }
    catch( HAL::HardwareAccessException &e)
        {
	    std::string msg( e.what() );
	    ERROR( msg );
	    XCEPT_RETHROW( utils::exception::FrlException, "Problem to stop the FRL ", e );
        }
    catch( utils::exception::HardwareAccessFailed &e )
        {
	    std::string msg( e.what() );
	    ERROR( msg );
	    XCEPT_RETHROW( utils::exception::FrlException, "Problem to stop the FRL", e );
        }
  
    

    // Only in DAQ mode. 
    if ( ( operationMode_ == FRL_MODE ) && ( dataSource_ == SLINK_SOURCE ) ) 
        {
            try 
                {
                    // Note: No slink test here.
                    slinkStream0_P->enable();
                    slinkStream1_P->enable();
                }
            catch (utils::exception::Exception & e)
                {                            
                    std::string msg =  toolbox::toString( "Failed to prepare SLINKs: %s.", 
                                                          xcept::stdformat_exception_history(e).c_str());
                    ERROR( msg );
                    XCEPT_RETHROW( utils::exception::FrlException, msg, e );		    
                }
        }
}

//////////////////////////////////// enable ///////////////////////////////////
void
ferol::Frl::enable()
    throw ( utils::exception::FrlException )
{

    if ( ! hwCreated_ ) return;
    

    // We know we have at least one stream enabled i.e. this frl/ferol is used in the global daq.
    // therefore it is safe to reset counters, also if the optical slink is used, since we know 
    // that the hardware is locked by us and cannot be used by someone else (e.g. minidaq).
    // We clear these counters to have clean values (also if the Frl is not used: reading 0 is less
    // confusing than reading some random value from the past).
    frlDevice_P->setBit("resetFifoMonCounter");
    frlDevice_P->setBit("enableFifoMonCounter");
    frlDevice_P->setBit("resetHistograms");      
    frlDevice_P->resetBit("enableHistograms");     

    if ( operationMode_ != FRL_MODE ) return;



    // we are in FRL mode 

    if (  dataSource_ == GENERATOR_SOURCE )
        {
            try
                {
                    // set the thresholds for the Backpressure
                    frlDevice_P->write( "gen_Thre_busy", appIS_.getuint32("gen_Thre_busy" ));
                    frlDevice_P->write( "gen_Thre_ready", appIS_.getuint32("gen_Thre_ready" ));
                    this->setupEventGen();
                    DEBUG("Now starting FRL event generator");
                    frlDevice_P->setBit("gen_startGenerator");
                }
            catch( HAL::HardwareAccessException &e )
                {
                    std::stringstream err;
                    err << "Hardware access problem while enabling Frl.";
                    ERROR( err.str() );
                    XCEPT_RETHROW( utils::exception::FrlException, err.str(), e);             
                }
        }

     else if( dataSource_ == SLINK_SOURCE )

        {
            frlDevice_P->setBit("globalFifoReset" );
            uint32_t fifoResetPoll = 0;
            try 
                {                    
                    frlDevice_P->pollItem( "globalFifoReset", 0, 1000, &fifoResetPoll );
                    frlDevice_P->setBit( "DAQ_mode" );      // do we need a 2u sleep afterwards?
                }
            catch( HAL::TimeoutException &e )
                {
                    std::stringstream err;
                    err << "Timeout while wainting for global FRL Fifo reset. (poll result: " << fifoResetPoll << " )";
                    ERROR( err.str() );
                    XCEPT_RETHROW( utils::exception::FrlException, err.str(), e);                     
                }
            catch( HAL::HardwareAccessException &e )
                {
                    std::stringstream err;
                    err << "Hardware access problem while enabling Frl.";
                    ERROR( err.str() );
                    XCEPT_RETHROW( utils::exception::FrlException, err.str(), e);                     
                }
        }
}

void
ferol::Frl::halt()
    throw ( utils::exception::FrlException )
{
    if ( ! hwCreated_ ) return;

    frlDevice_P->resetBit( "DAQ_mode" );
    // can always be done, also if we do not generate events 
    frlDevice_P->resetBit("gen_startGenerator");

    frlDevice_P->resetBit("enableFifoMonCounter");
    frlDevice_P->resetBit("enableHistograms");     

    //monitor_.updateAllInfospaces();
    shutdownHwDevice();
}

void
ferol::Frl::suspendEvents()
    throw ( utils::exception::FrlException )
{
    if ( ! hwCreated_ ) return;

    if ( ( operationMode_ == FRL_MODE ) && ( dataSource_ == GENERATOR_SOURCE ) )
        {
            try
                {
                    frlDevice_P->setBit("gen_suspendGenerator");
                }
            catch( HAL::HardwareAccessException &e )
                {
                    FATAL( "Hardware Access failed: Could not supend Frl Event Generator.");
                    XCEPT_RETHROW( utils::exception::FrlException, 
                                   "Hardware Access failed: Could not supend Frl Event Generator.", e );		    
                }
        }
}

void
ferol::Frl::resumeEvents()
    throw ( utils::exception::FrlException )
{
    if ( ! hwCreated_ ) return;

    if ( ( operationMode_ == FRL_MODE ) && ( dataSource_ == GENERATOR_SOURCE ) )
        {
            try
                {
                    frlDevice_P->resetBit("gen_suspendGenerator");
                }
            catch( HAL::HardwareAccessException &e )
                {
                    FATAL( "Hardware Access failed: Could not resume Frl Event Generator.");
                    XCEPT_RETHROW( utils::exception::FrlException, 
                                   "Hardware Access failed: Could not resume Frl Event Generator.", e );		    
                }
        }
}

void 
ferol::Frl::setupEventGen() 
    throw ( utils::exception::FrlException ) 
{
    if ( ! hwCreated_ ) return;
    

    frlDevice_P->resetBit( "gen_suspendGenerator");
    frlDevice_P->resetBit( "gen_startGenerator"); 
    frlDevice_P->resetBit( "gen_considerDelays"); // the default. It will be set if the event generator is used and 
    // delays > 10ns are requested.

    if ( slinkStream0_P->isActive() ) 
        {
            FrlEventGenerator gen( bridgeDevice_P, frlDevice_P, logger_ );
            gen.setupEventGenerator( appIS_.getuint32( "Event_Length_bytes_FED0" ),
                                     appIS_.getuint32( "Event_Length_Max_bytes_FED0" ),
                                     appIS_.getuint32( "Event_Length_Stdev_bytes_FED0" ),
                                     appIS_.getuint32( "Event_Delay_ns_FED0" ),
                                     appIS_.getuint32( "Event_Delay_Stdev_ns_FED0" ),
                                     appIS_.getuint32( "N_Descriptors_FRL" ),
                                     appIS_.getuint32( "Seed_FED0"),
                                     0,
                                     appIS_.getuint32( "expectedFedId_0"));
        }
    if ( slinkStream1_P->isActive() )
        {
            FrlEventGenerator gen( bridgeDevice_P, frlDevice_P, logger_ );
            gen.setupEventGenerator( appIS_.getuint32( "Event_Length_bytes_FED1" ),
                                     appIS_.getuint32( "Event_Length_Max_bytes_FED1" ),
                                     appIS_.getuint32( "Event_Length_Stdev_bytes_FED1" ),
                                     appIS_.getuint32( "Event_Delay_ns_FED1" ),
                                     appIS_.getuint32( "Event_Delay_Stdev_ns_FED1" ),
                                     appIS_.getuint32( "N_Descriptors_FRL" ),
                                     appIS_.getuint32( "Seed_FED1"),
                                     1,
                                     appIS_.getuint32( "expectedFedId_1"));
        }

    try
        {
            std::string triggerMode = appIS_.getstring( "FrlTriggerMode" );
            if ( triggerMode == FRL_AUTO_TRIGGER_MODE )
                {
                    frlDevice_P->resetBit( "gen_enableSoftTrigger");
                    frlDevice_P->setBit( "gen_considerDelays");
                    frlDevice_P->setBit( "gen_autoTrigger");
                }
            else if ( triggerMode == FRL_GTPE_TRIGGER_MODE )
                {
                    frlDevice_P->resetBit( "gen_enableSoftTrigger");
                    frlDevice_P->resetBit( "gen_considerDelays");
                    frlDevice_P->resetBit( "gen_autoTrigger");
                }
            else if ( triggerMode == FRL_SOAP_TRIGGER_MODE )
                {
                    frlDevice_P->setBit( "gen_enableSoftTrigger");
                    frlDevice_P->resetBit( "gen_considerDelays");
                    frlDevice_P->resetBit( "gen_autoTrigger");
                }
            if ( stream0_ )
                frlDevice_P->setBit( "gen_enableGeneratorStream0" );
            if ( stream1_ )
                frlDevice_P->setBit( "gen_enableGeneratorStream1" );

            INFO( "FRL emulator should now be ready to use.");

        }
    catch( HAL::HardwareAccessException &e )
        {
            FATAL( "Hardware Access failed: Could not set up event generator in FRL.");
            XCEPT_RETHROW( utils::exception::FrlException, 
                           "Hardware Access failed: Could not set up event generator in FRL.", e );		    
        }
    catch( utils::exception::HardwareAccessFailed &e )
        {
	    std::string msg( e.what() );
	    ERROR( msg );
	    XCEPT_RETHROW( utils::exception::FrlException, 
                           "Could not set up event descriptors for FRL event Generator", e );
         }


    return;
}

uint32_t
ferol::Frl::getIndex()
{
    return index_;
}

void
ferol::Frl::resetMonitoring()
{
  this->hwlock();
  if (  ! hwCreated_ )
        {
            this->hwunlock();
	    XCEPT_RAISE( utils::exception::FrlException,
			 "Could not reset monitoring in FRL since not FRL hardware is instantiated." );
        }

  try 
      {
     	  frlDevice_P->write("monitorReset",1);
	  //frlDevice_P->setBit("resetFifoMonCounter"); 
      }
  catch( HAL::HardwareAccessException &e )
      {
	  this->hwunlock();
	  std::string msg( e.what() );
	  ERROR( msg );
	  XCEPT_RETHROW( utils::exception::FrlException, 
			 "Could not reset monitoring information: ", e );
      }
  this->hwunlock();
  return;
}

bool 
ferol::Frl::updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo )
{

    // take the lock to make sure that nobody calls the destructor in the 
    // middle of an update. 
    this->hwlock();

    if ( ! hwCreated_ )
        {
            this->hwunlock();
            return true;
        }

    // update monitoring info
    std::list< utils::InfoSpaceHandler::ISItem > items = is->getItems();
    std::list< utils::InfoSpaceHandler::ISItem >::const_iterator it;

    SlinkStream *currentStreamP = NULL;
    if ( streamNo == 0 )
        currentStreamP = slinkStream0_P;
    else if ( streamNo == 1 )
        currentStreamP = slinkStream1_P;

    try
        {
            frlDevice_P->writePulse( "latch_BP_cnt" );

            for ( it = items.begin(); it != items.end(); it++ )
                {
                    
                    /////////////////// items read from hardware registers ///////////////////

                    if ( (*it).update == utils::InfoSpaceHandler::HW32 ) 
                        {
                            uint32_t value;
                            frlDevice_P->read( (*it).gethwname(), &value );
                            if ( (*it).type == utils::InfoSpaceHandler::UINT32 )                                
                                is->setuint32( (*it).name, value );                                
                            else
                                is->setuint64( (*it).name, value );
                        }
                    else if ( (*it).update == utils::InfoSpaceHandler::HW64 ) 
                        {
                            uint64_t value;
                            uint32_t lo,hi;
                            frlDevice_P->read( (*it).gethwname(), &lo );
                            frlDevice_P->read( (*it).gethwname(), &hi, 0x04 );
                            value = lo + ( ((uint64_t)hi) << 32 );
                            is->setuint64( (*it).name, value );
                        }
                    else if ( (*it).update == utils::InfoSpaceHandler::PROCESS ) 
                        {
                            if ( (*it).name == "SenderFwVersion" ) 
                                {
                                    is->setuint32( (*it).name, currentStreamP->getCmcFwVersion() );                                  
                                } 
                            else if ( (*it).name == "SLinkPacketCRCError" ) {
                                is->setuint64( (*it).name, 0 );
                            }
                            else if ( (*it).name == "FEDFrequency" ) 
                                // We promise to give the value in KHz. We have to multiply the hardware valye by 10.
                                {
                                    uint32_t freqKHz;
                                    frlDevice_P->read( (*it).gethwname(), &freqKHz );
                                    freqKHz *= 10;
                                    is->setuint32( (*it).name, freqKHz ); 
                                }
                            else if ( (*it).name == "AccBackpressureSeconds" )
                                {
                                    uint64_t value;
                                    uint32_t lo,hi;
                                    // This item is a "PROCESS" item: the _LO for HW64 is not automatically appended.
                                    frlDevice_P->read( (*it).gethwname()+"_LO", &lo );
                                    frlDevice_P->read( (*it).gethwname()+"_LO", &hi, 0x04 );
                                    //           value = lo + ( ((uint64_t) (0xFFF & hi)) << 32 );
                                    value = lo + ( ((uint64_t)hi) << 32 );
                                    // The frequency for the counter comes from a 100MHz clock
                                    double acctime = (double)value/((double)100000000.0 );
                                    is->setdouble( "AccBackpressureSeconds", acctime);
                                  
                                    // Also update the Timestamp to calculate deltas and rates
                                    frlDevice_P->read( "BP_running_cnt_LO", &lo);
                                    frlDevice_P->read( "BP_running_cnt_HI", &hi);
                                    
                                    uint64_t timestamp = (((uint64_t)hi) << 32) + lo;
                                    double t_s = (double)timestamp / 100000000.0; // in s
                                    is->setdouble( "LatchedTimeFrontendSeconds", t_s);
                                }


                            //else if ( ((*it).name == "BP_L0_LO") or ((*it).name == "BP_L1_LO") )  // old frlmonitoring 
                            else if ( ((*it).name == "AccSlinkFullSeconds") )
                                {
                                    uint64_t value;
                                    uint32_t lo,hi;
                                    std::stringstream itemstr;
                                    itemstr << "BP_L" << std::dec << streamNo << "_LO";
                                    frlDevice_P->read( itemstr.str(), &lo );
                                    frlDevice_P->read( itemstr.str(), &hi, 4 );
                                    value = 0xFFFFFFFFFFULL & (lo + ( ((uint64_t)hi) << 32 ));

                                    // now calculate the integrated backpressure in seconds
                                    uint32_t freqKhz = is->getuint32( "FEDFrequency" );


                                    if (freqKhz > 0)
                                        {
                                            double acctime = (double)value/(1000.0 * (double)freqKhz );
                                            is->setdouble( "AccSlinkFullSeconds", acctime);
                                        }
                                    else
                                        {
                                            is->setdouble( "AccSlinkFullSeconds", -1.0);
                                        }

                                }
                        }
                }
        }

    catch ( HAL::HardwareAccessException &e )
        {
            ERROR( "exception");
            ERROR( e.what() );
        }

    this->hwunlock();

    is->writeInfoSpace();

    // At this point we can do some post processing. Spontaneous state changes 
    // which are indications of failures should be detected here:
    
    // We check for mismatches in the expected and read fedid. During times when the fedid has 
    // not yet been transferred, the bit is not set by the hardware. Therefore we can do the check
    // at all times.


    // For the time being the following checks should only be executed on the old FEROL_IS
    // When the migration of the monitoring is finished this FEROL_IS dissapears and the 
    // checks will be executed on the new Input Info space handler.
    if (is->name() == "InputStream" ) 
        {
            StreamInfoSpaceHandler *in_is = dynamic_cast< StreamInfoSpaceHandler * >(is);

            if (stream0_ && checkWrongId0_) {
                bool wrongId0 = in_is->getuint32( "WrongFEDIdDetected", 0 );
                if ( wrongId0 )
                    {
                        uint32_t wrongFedId;
                        frlDevice_P->read( "FEDID_WRONG_L0", &wrongFedId );
                        uint32_t expFedid = appIS_.getuint32( "expectedFedId_0");
                        checkWrongId0_ = false; // the check should be reported only once.
                        std::stringstream msg;
                        msg << "Detected unexpected FED Id in incoming fragment of Frl (link 0). Exptected Id is " 
                            << expFedid
                            << " but the received Id is "
                            << wrongFedId
                            << ".";
                        ERROR(msg.str());
                        XCEPT_DECLARE(utils::exception::FrlException, top, msg.str());
                        top.setProperty( "tag", "channel_0,fedsrcid_" + expFedid );
                        xdaq_->notifyQualified( "error", top );
                        if ( appIS_.getbool( "gotoFailedOnWrongFedId" ) )
                            {
                                throw(top);  // catched by the monitor and goes to failed
                            }
                    }
            }

            if (stream1_ && checkWrongId1_) {
                bool wrongId1 = in_is->getuint32( "WrongFEDIdDetected", 1 );
                if ( stream1_ && wrongId1 )
                    {
                        uint32_t wrongFedId;
                        frlDevice_P->read( "FEDID_WRONG_L1", &wrongFedId );
                        uint32_t expFedid = appIS_.getuint32( "expectedFedId_1");
                        checkWrongId1_ = false; // the check should be reported only once.
                        std::stringstream msg;
                        msg << "Detected unexpected FED ID in incoming fragment of Frl (link 1). Exptected Id is " 
                            << expFedid
                            << " but the received Id is "
                            << wrongFedId
                            << ".";
                        ERROR(msg.str());
                        XCEPT_DECLARE(utils::exception::FrlException, top, msg.str());
                        top.setProperty( "tag", "channel_1,fedsrcid_" + expFedid );
                        xdaq_->notifyQualified( "error", top );
                        if ( appIS_.getbool( "gotoFailedOnWrongFedId" ) )
                            {
                                throw(top);  // catched by the monitor and goes to failed
                            }                 
                    }
            }
        } return true;
}

void
ferol::Frl::softTrigger()
{
    if ( ! hwCreated_ ) return;
    this->hwlock();
    try
        {
            frlDevice_P->writePulse( "gen_PCI_trigger" ); 
            INFO( "Generated a software trigger." );
        }
    catch( HAL::HardwareAccessException &e )
        {
            ERROR( e.what() );           
        }
    this->hwunlock();
}

void
ferol::Frl::hwlock()
{
    deviceLock_.take();
}

void
ferol::Frl::hwunlock()
{
    deviceLock_.give();
}

HAL::HardwareDeviceInterface *
ferol::Frl::getFrlDevice()
{
    return frlDevice_P;
}
HAL::HardwareDeviceInterface *
ferol::Frl::getBridgeDevice()
{
    return bridgeDevice_P;
}
