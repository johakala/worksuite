#include <sstream>
#include <iomanip>
#include "ferol/StreamInfoSpaceHandler.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/loggerMacros.h"

ferol::StreamInfoSpaceHandler::StreamInfoSpaceHandler( xdaq::Application *xdaq,
                                                       std::string name,
                                                       utils::InfoSpaceUpdater *updater, 
                                                       utils::InfoSpaceHandler *appIS,
                                                       bool noAutoPush )
    : utils::InfoSpaceHandler( xdaq, name, updater, noAutoPush ),
      appIS_P(appIS),
      currentStream_(0)
{
    //DEBUG( "stream : appis ptr " << appIS_P);
    createuint32( "expectedFedId", 0, "", NOUPDATE, "The fedid expected by the configuration for this input stream. This value is used as a key to identify the flashlist entries.");
    createuint32( "slotNumber", 0, "", NOUPDATE, "The slotNumber of the Ferol used in the key. (Preferred to be used over expectedFedId, since the database is organised in terms of slot number and streamNumber). ");
    createuint32( "streamNumber", 0, "", NOUPDATE, "The stream number of this entry needed to distinguish streams of the same Ferol.");
}


void 
ferol::StreamInfoSpaceHandler::createstring( std::string name, std::string value, std::string format, UpdateType updateType, std::string doc )
{
    itemlock_.take();
    utils::InfoSpaceHandler::createstring( name, value, format, updateType, doc );
    std::vector<std::string> vals;
    vals.push_back( "uninitialised" );
    vals.push_back( "uninitialised" );
    stringStreamValues_[name] = vals;
    itemlock_.give();
}

void 
ferol::StreamInfoSpaceHandler::createuint32( std::string name, uint32_t value, std::string format, UpdateType updateType, std::string doc )
{
    //DEBUG( " ----> insert " << name );

    itemlock_.take();
    utils::InfoSpaceHandler::createuint32( name, value, format, updateType, doc );
    std::vector<uint32_t> vals;
    vals.push_back( 0 );
    vals.push_back( 0 );
    uint32StreamValues_[name] = vals;
    
    itemlock_.give();
}

void 
ferol::StreamInfoSpaceHandler::createuint64( std::string name, uint64_t value, std::string format, UpdateType updateType, std::string doc )
{
    itemlock_.take();
    utils::InfoSpaceHandler::createuint64( name, value, format, updateType, doc );
    std::vector<uint64_t> vals;
    vals.push_back( 0 );
    vals.push_back( 0 );
    uint64StreamValues_[name] = vals;
    itemlock_.give();
}

void
ferol::StreamInfoSpaceHandler::createdouble( std::string name, double value, std::string format, UpdateType updateType, std::string doc )
{
    itemlock_.take();
    utils::InfoSpaceHandler::createdouble( name, value, format, updateType, doc );
    std::vector<double> vals;
    vals.push_back( 0. );
    vals.push_back( 0. );
    doubleStreamValues_[name] = vals;
    itemlock_.give();
}

void
ferol::StreamInfoSpaceHandler::createbool( std::string name, bool value, std::string format, UpdateType updateType, std::string doc )
{
    itemlock_.take();
    utils::InfoSpaceHandler::createbool( name, value, format, updateType, doc );
    std::vector<bool> vals;
    vals.push_back( false );
    vals.push_back( false );
    boolStreamValues_[name] = vals;
    itemlock_.give();
}


void 
ferol::StreamInfoSpaceHandler::setstring( std::string name, std::string value, bool push ) 
{
    itemlock_.take();
    utils::InfoSpaceHandler::setstring( name, value, push );
    stringStreamValues_[ name ][currentStream_] = value;
    itemlock_.give();
}
void 
ferol::StreamInfoSpaceHandler::setuint32( std::string name, uint32_t value, bool push ) 
{
    //DEBUG("in Streaminfospaceh setuint32 " << name << value);
    itemlock_.take();
    utils::InfoSpaceHandler::setuint32( name, value, push );    
    uint32StreamValues_[ name ][currentStream_] = value;
    itemlock_.give();
}
void 
ferol::StreamInfoSpaceHandler::setuint64( std::string name, uint64_t value, bool push ) 
{
    itemlock_.take();
    utils::InfoSpaceHandler::setuint64( name, value, push );
    uint64StreamValues_[ name ][currentStream_] = value;
    itemlock_.give();
}
void 
ferol::StreamInfoSpaceHandler::setuint64( std::string name, uint32_t low, uint32_t high, bool push ) 
{
    itemlock_.take();
    utils::InfoSpaceHandler::setuint64( name, low, high, push );
    uint64StreamValues_[ name ][currentStream_] = (((uint64_t)high) << 32) + low;
    itemlock_.give();
}
void 
ferol::StreamInfoSpaceHandler::setbool( std::string name, bool value, bool push ) 
{
    itemlock_.take();
    utils::InfoSpaceHandler::setbool( name, value, push );
    boolStreamValues_[ name ][currentStream_] = value;
    itemlock_.give();
}
void 
ferol::StreamInfoSpaceHandler::setdouble( std::string name, double value, bool push ) 
{
    itemlock_.take();
    utils::InfoSpaceHandler::setdouble( name, value, push );
    doubleStreamValues_[ name ][currentStream_] = value;
    itemlock_.give();
}


std::string 
ferol::StreamInfoSpaceHandler::getstring( std::string name, uint32_t streamNo )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::vector< std::string > >::iterator iter;
    iter = stringStreamValues_.find( name );
    if ( iter == stringStreamValues_.end() ) 
        {
            std::string err = "Try to retrieve a non-existing item named \"" + name + "\" in StreamInfoSpace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );
            itemlock_.give();
           return "";
        }
    else
        {
            // We have to create a lock here, since strings are not threadsafe.
            // We borrow the lock of the infospace since we are lazy.
            is_P->lock();
            if ( cpis_P ) cpis_P->lock();
            std::string localcopy = ((*iter).second)[streamNo];
            if ( cpis_P ) cpis_P->unlock();
            is_P->unlock();
            itemlock_.give();
            return localcopy;
        }
}

uint32_t
ferol::StreamInfoSpaceHandler::getuint32( std::string name, uint32_t streamNo )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::vector<uint32_t> >::iterator iter;
    iter = uint32StreamValues_.find( name );
    if ( iter == uint32StreamValues_.end() ) 
        {
            std::string err = "Try to get a non-existing uint32 item named \"" + name + "\" in StreamInfoSpace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );   
            itemlock_.give();
            return 0;
        }
    else
        {
            uint32_t val =  ((*iter).second)[streamNo];
            itemlock_.give();
            return val;
        }
}

uint64_t
ferol::StreamInfoSpaceHandler::getuint64( std::string name, uint32_t streamNo )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::vector<uint64_t> >::iterator iter;
    iter = uint64StreamValues_.find( name );
    if ( iter == uint64StreamValues_.end() ) 
        {
            std::string err = "Try to get a non-existing uint64 item named \"" + name + "\" in StreamInfoSpace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );   
            itemlock_.give();
            return 0;
        }
    else
        {
            uint64_t val =  ((*iter).second)[streamNo];
            itemlock_.give();
            return val;
        }
}

bool
ferol::StreamInfoSpaceHandler::getbool( std::string name, uint32_t streamNo )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::vector<bool> >::iterator iter;
    iter = boolStreamValues_.find( name );
    if ( iter == boolStreamValues_.end() ) 
        {
            std::string err = "Try to get a non-existing bool item named \"" + name + "\" in StreamInfoSpace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );   
            itemlock_.give();
            return 0;
        }
    else
        {
            bool val =  ((*iter).second)[streamNo];
            itemlock_.give();
            return val;
       }
}

double
ferol::StreamInfoSpaceHandler::getdouble( std::string name, uint32_t streamNo )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, std::vector<double> >::iterator iter;
    iter = doubleStreamValues_.find( name );
    if ( iter == doubleStreamValues_.end() ) 
        {
            std::string err = "Try to get a non-existing double item named \"" + name + "\" in StreamInfoSpace.";
            XCEPT_DECLARE( utils::exception::SoftwareProblem, top, err );
            xdaq_P->notifyQualified( "fatal", top );
            ERROR( err );   
            itemlock_.give();
            return 0;
        }
    else
        {
            double val =  ((*iter).second)[streamNo];
            itemlock_.give();
            return val;
        }
}


std::string 
ferol::StreamInfoSpaceHandler::getFormatted( std::string item, uint32_t streamNo, std::string format)
{
    itemlock_.take();
    std::stringstream result;
    std::tr1::unordered_map<std::string, ISItem& >::const_iterator itf = itemMap_.find( item );

    //DEBUG( "getting item " << item << " from an Stream Infospace");

    if ( itf == itemMap_.end() ) 
        {
            //we have a problem: the item does not exist
            ERROR( "item \"" + item + "\" does not exist!" );
            itemlock_.give();
            return "item \"" + item + "\" does not exist!";
        }

    std::string fmt = (*itf).second.format;
    ItemType type = (*itf).second.type;
    //size_t space = fstr.find_first_of( ' ' );
    //std::string type = fstr.substr(0, space);
    //std::string fmt = fstr.substr( space + 1, std::string::npos);

    //DEBUG( "getting item " << item << " witdh format " << fmt);

    // Now we start the ugly if over the types
    if ( type == UINT32 )
        {
            if ( fmt == "" || fmt == "dec" ) 
                {
                    result << std::dec << this->getuint32( item, streamNo ); 
                }
            else if ( fmt == "hex" )
                {
                    result << "0x" << std::setw(8) << std::setfill('0') << std::hex << this->getuint32( item, streamNo ); 
                }
            else if ( fmt == "hex/dec" )
                {
                    result << "0x" << std::setw(8) << std::setfill('0') << std::hex << this->getuint32( item, streamNo ) << " / " << std::dec << this->getuint32( item, streamNo ) ; 
                }
            else if ( fmt == "ip" )
                {

		  uint32_t val = this->getuint32( item, streamNo ); 
		  
		  int byte4 = (val & 0xff000000) >> 24;
		  int byte3 = (val & 0xff0000) >> 16;
		  int byte2 = (val & 0xff00) >> 8;
		  int byte1 = (val & 0xff);
		  result << std::dec << byte4 << "." << byte3 << "." << byte2 << "." << byte1;
                }
            else if ( fmt.find( "field%" ) != std::string::npos )
                {
                    std::list< std::string > bits;
                    size_t pos = 6;
                    size_t npos = 0;
                    while ( (npos = fmt.find( "%", pos )) != std::string::npos  )
                        {
                            bits.push_back( fmt.substr( pos, npos-pos ));
                            pos = npos+1;
                        }
                    std::list<std::string>::reverse_iterator itr = bits.rbegin();
                    uint32_t work = this->getuint32( item, streamNo );                    
                    std::string bitclass = "";
                    for ( ; itr != bits.rend(); itr++ )
                        {
                            uint32_t bit = work & 0x1;
                            work >>= 1;
                            if ( bit )
                                bitclass = "bitset";
                            else 
                                bitclass = "bitreset";
                            result << "<span class=\"" << bitclass << "\">" << *itr << "</span> ";
                        }
                }
            else
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << std::dec << this->getuint32( item, streamNo );	
                }
            //DEBUG( "result is " << result.str() );
        } 

    else if ( type == UINT64 )
        {
            if ( fmt == "" || fmt == "dec" ) 
                {
                    result << std::dec << this->getuint64( item, streamNo ); 
                }
            else if ( fmt == "hex" )
                {
                    result << "0x" << std::setw(16) << std::setfill('0') << std::hex << this->getuint64( item, streamNo ); 
                }
            else if ( fmt == "hex/dec" )
                {
                    result << "0x" << std::setw(16) << std::setfill('0') << std::hex << this->getuint64( item, streamNo ) << " / " << std::dec << this->getuint64( item, streamNo ) ; 
                }
            else if ( fmt == "mac" )
	      {
		uint64_t val = this->getuint64( item, streamNo );
		uint32_t byte1 = val & 0xffll;
		uint32_t byte2 = (val & 0xff00ll ) >> 8;
		uint32_t byte3 = (val & 0xff0000ll ) >> 16;
		uint32_t byte4 = (val & 0xff000000ll ) >> 24;
		uint32_t byte5 = (val & 0xff00000000ll ) >> 32;
		uint32_t byte6 = (val & 0xff0000000000ll ) >> 40;

		result << std::setw(2) << std::setfill('0') << std::hex << byte6 << ":" << std::setw(2) << byte5 << ":" << std::setw(2) << byte4 << ":" << std::setw(2) << byte3 << ":" << std::setw(2) << byte2 << ":" << std::setw(2) << byte1; 
                }
            else 
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << std::dec << this->getuint64( item, streamNo );	
                }
        }

    else if ( type == STRING )
        {
            if ( fmt == "" ) 
                {
                    result << this->getstring( item, streamNo ); 
                }
            else 
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << this->getstring( item, streamNo );	
                }
        }

    else if ( type == BOOL )
        {
            if ( fmt == "" ) 
                {
                    result << this->getbool( item, streamNo ); 
                }
            else 
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << this->getbool( item, streamNo );	
                }
        }

    else if ( type == DOUBLE )
        {
            if ( fmt == "" ) 
                {
                    result << this->getdouble( item, streamNo ); 
                }
            else if ( fmt == "percent" )
                {
                    result << std::fixed << std::setprecision(2) << 100.0 * this->getdouble( item, streamNo ) << "%";
                }
            else if ( fmt == "us" )
                {
                    result << std::fixed << std::setprecision(2) << this->getdouble( item, streamNo ) << "&#956;s"; 
                }
            else if ( fmt == "rate" )
                {
                    result << std::scientific << std::setprecision(2) << this->getdouble( item, streamNo ) << " Hz";
                }
            else if ( fmt == "bw" )
                {
                    result << std::scientific << std::setprecision(2) << this->getdouble( item, streamNo ) << " B/s";
                }
            else if ( fmt == "V" )
                {
                    result << std::fixed << std::setprecision(3) << this->getdouble( item, streamNo ) << " V";
                }
            else if ( fmt == "mA" )
                {
                    result << std::fixed << std::setprecision(3) << this->getdouble( item, streamNo ) << " mA";
                }
            else if ( fmt == "C" )
                {
                    result << std::fixed << std::setprecision(2) << this->getdouble( item, streamNo ) << " &#176C";
                }
            else if ( fmt == "uW" )
                {
                    result << std::fixed << std::setprecision(2) << this->getdouble( item, streamNo ) << " &#956;W";
                }
            else if ( fmt == "bitbw" )
                {
                    result << std::scientific << std::setprecision(2) << this->getdouble( item, streamNo ) << " bps";
                }
            else if ( fmt == "Gbitbw" )
                {
                    result << std::fixed << std::setprecision(3) << this->getdouble( item, streamNo ) / 1000000000.0 << " Gbps";
                }
            else 
                {
                    ERROR( "format \"" + fmt + "\" is not supported" );
                    result << this->getdouble( item, streamNo );	
                }
        }
    else
        {
            ERROR( "type \"" << type <<  "\" is not supported" );
            return "item type is not supported";
        }

    itemlock_.give();
    return result.str();
}


// Not thread safe in the sense that if another thread is changing this setting at arbitrary times, 
// the first thread does not know it and might write values for another stream.
void
ferol::StreamInfoSpaceHandler::setSuffix( uint32_t suffix )
{
    itemlock_.take();
    std::tr1::unordered_map< std::string, ISItem& >::iterator it;
    currentStream_ = suffix;
    for ( it = itemMap_.begin(); it != itemMap_.end(); it++) 
        {
            std::stringstream nsuffix;
            nsuffix << suffix;
            if ((*it).second.update == HW64 )
                {
                    nsuffix << "_LO";
                }

            // No suffixes for SLE; but we want updates for PROCESS, TRACKER. NOUPDATE we do not care. 
            if ((*it).second.update == SLE )
                //            if ((*it).second.update == HW64 || (*it).second.update == HW32 )
                {
                    (*it).second.suffix = "";
                }
            else 
                {
                    (*it).second.suffix = nsuffix.str();
                }
        }
    itemlock_.give();
}


void
ferol::StreamInfoSpaceHandler::update()
{
    //DEBUG( "udpate streaminfosp");
    if ( (updater_P != NULL) ) {
        //DEBUG( "somethign in updated_P");
        //DEBUG( "ptr " << appIS_P );
        if ( appIS_P->getbool("enableStream0") )
            { 
                uint32_t fedid = appIS_P->getuint32( "expectedFedId_0" );
                this->setSuffix( 0 );
                this->setuint32( "expectedFedId", fedid );
                this->setuint32( "streamNumber", 0 );
                this->setuint32( "slotNumber", appIS_P->getuint32("slotNumber") );
                updater_P->updateInfoSpace( this );
                if ( ! this->isNoAutoPush() )
                    this->pushInfospace();
            }
        if ( appIS_P->getbool( "enableStream1" ) )
            {
                uint32_t fedid = appIS_P->getuint32( "expectedFedId_1" );
                this->setSuffix( 1 );
                this->setuint32( "expectedFedId", fedid );
                this->setuint32( "streamNumber", 1 );
                this->setuint32( "slotNumber", appIS_P->getuint32("slotNumber") );
                updater_P->updateInfoSpace( this );
                if ( ! this->isNoAutoPush() )
                    this->pushInfospace();
            }
    } else {
        ERROR("BUG: no updater defined");
    }
}
