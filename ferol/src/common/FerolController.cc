#include <iomanip>
#include <stdio.h>
#include <sstream>
#include <unistd.h>
#include <iostream>
#include <bitset>
#include <list>

#include "ferol/FerolController.hh"
#include "ferol/FerolStateMachine.hh"
#include "ferol/FerolWebServer.hh"
#include "ferol/FerolMonitor.hh"
#include "ferol/loggerMacros.h"
#include "ferol/HardwareDebugger.hh"
#include "d2s/utils/WebStaticContentTab.hh"

#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"

#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/Event.h"
#include "toolbox/Runtime.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"

#include "xdata/InfoSpaceFactory.h"

#include "xcept/tools.h"


XDAQ_INSTANTIATOR_IMPL(ferol::FerolController);

ferol::FerolController::FerolController(xdaq::ApplicationStub* stub)   
    throw (xdaq::exception::Exception):
    xdaq::WebApplication(stub),
    utils::InfoSpaceUpdater(),

    logger_( this->getApplicationLogger() ),
    timer_(10),
    appIS_( this ),
    statusIS_( this, this ),
    inputIS_( this, &appIS_ ),
    fsm_( this, &statusIS_, &appIS_ ),
    monitor_( this->getApplicationLogger(), appIS_, this, fsm_ ),
    hwLocker_( logger_, appIS_, statusIS_ ),
    frl_( this, appIS_, monitor_, hwLocker_ ),
    ferol_( this, appIS_, statusIS_, monitor_, hwLocker_ )
{
    timer_.start();
    htmlDocumentation_ = this->fillDocumentation();
    operationMode_ = "n.a.";
    dataSource_ = "n.a.";
    monitor_.addInfoSpace( &statusIS_ );

    stub->getDescriptor()->setAttribute("icon","/ferol/images/FerolController.png");
    stub->getDescriptor()->setAttribute("icon16x16", "/ferol/images/FerolController_16x16.png");
    instance_ = getApplicationDescriptor()->getInstance();
    statusIS_.setuint32( "instance", instance_, true );

    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

    // The soap interface exposed to the outside world:
    // The state transitions called by RCMS
    // The changeState callback in this class is just forwarding the call to the 
    // FerolStateMachine object. It is not possible to delegate the callback directly
    // to a non-xdaq Application. 
    xoap::bind( this, &FerolController::changeState, "Configure", XDAQ_NS_URI );
    xoap::bind( this, &FerolController::changeState, "Enable"   , XDAQ_NS_URI );
    xoap::bind( this, &FerolController::changeState, "Pause"    , XDAQ_NS_URI );
    xoap::bind( this, &FerolController::changeState, "Resume"   , XDAQ_NS_URI );
    xoap::bind( this, &FerolController::changeState, "Halt"     , XDAQ_NS_URI );
    xoap::bind( this, &FerolController::changeState, "Stop"     , XDAQ_NS_URI );

    // to change conifguration parameters on the fly
    xoap::bind (this, &FerolController::ParameterSet, "ParameterSet", XDAQ_NS_URI);

    // for testing
    xoap::bind( this, &FerolController::changeState, "Fail"     , XDAQ_NS_URI );
    
    // to switch on and off the laser
    xoap::bind( this, &FerolController::laserOn, "laserOn"      , XDAQ_NS_URI );
    xoap::bind( this, &FerolController::laserOff, "laserOff"    , XDAQ_NS_URI );

    // for debugging the fedkit: toggle the ldown flag on the sender side
    xoap::bind( this, &FerolController::l0DaqOn, "daqOn"      , XDAQ_NS_URI );
    xoap::bind( this, &FerolController::l0DaqOff, "daqOff"     , XDAQ_NS_URI );

    // for debugging we want to be able to read and write any item to the hardware
    xoap::bind( this, &FerolController::readItem, "ReadItem"  , XDAQ_NS_URI );
    xoap::bind( this, &FerolController::writeItem, "WriteItem", XDAQ_NS_URI );

    // to issue a software trigger to the FRL event generator if in FRL_SOAP_TRIGER_MODE
    xoap::bind( this, &FerolController::softTrigger, "softTrigger", XDAQ_NS_URI );

    // Read out the SFP status information via its I2C interface
    xoap::bind( this, &FerolController::sfpStatus, "sfpStatus", XDAQ_NS_URI );

    // Reset the Vitesse chip.
    xoap::bind( this, &FerolController::resetVitesse, "resetVitesse", XDAQ_NS_URI );

    // Reset some monitoring items.
    xoap::bind( this, &FerolController::resetMonitoring, "resetMonitoring", XDAQ_NS_URI );

    // Dump the Hardware Registers.
    xoap::bind( this, &FerolController::dumpHardwareRegisters, "dumpHardwareRegisters", XDAQ_NS_URI );

 
    // bind the web pages
    xgi::bind(this, &FerolController::monitoringWebPage, "Default");
    xgi::bind(this, &FerolController::debugFerol, "debugFerol");
    xgi::bind(this, &FerolController::debugFrl, "debugFrl");
    xgi::bind(this, &FerolController::debugBridge, "debugBridge");
    xgi::bind(this, &FerolController::debugSlinkExpress, "debugSlinkExpress");
    xgi::bind(this, &FerolController::expertDebugging, "expertDebugPage");
    xgi::bind(this, &FerolController::testPage, "test");
    xgi::bind(this, &FerolController::jsonUpdate, "update");
    xgi::bind(this, &FerolController::jsonInfoSpaces, "infospaces");
    xgi::bind(this, &FerolController::xdaqWebPage, "basic");

    timer_.stop();
    uint32_t time = timer_.read();
    INFO("Constructor needed " << time << " mu seconds.");
}

void
ferol::FerolController::expertDebugging( xgi::Input *in, xgi::Output *out)
{
    webServer_P->printHeader(out);
    webServer_P->expertDebugging( in, out, &ferol_, &frl_, this );
    webServer_P->printFooter(out);
}

void 
ferol::FerolController::testPage( xgi::Input *in, xgi::Output *out)
{   
    webServer_P->printHeader(out);
    *out << "Hello world";
    *out << "<script> $('#debug').append( 'debug'); </script>";
    webServer_P->printFooter(out);
}

void 
ferol::FerolController::debugSlinkExpress( xgi::Input *in, xgi::Output *out)
{   
    // we should here check on the mode of operation or if an SFP is plugged in and connected to a core.
    ferol::HardwareDebugger hwdebug( logger_, &ferol_, &frl_ );
    if ( appIS_.getbool( "enableStream0" ) )
        {
            std::list< utils::HardwareDebugItem > slinkExpressRegisters = hwdebug.getSlinkExpressRegisters( 0 );
            webServer_P->printDebugTable( "Slink Express Registers link 0", slinkExpressRegisters, in, out );
        }

    if ( appIS_.getbool( "enableStream1" ) )
        {
            std::list< utils::HardwareDebugItem > slinkExpressRegisters = hwdebug.getSlinkExpressRegisters( 1 );
            webServer_P->printDebugTable( "Slink Express Registers link 1", slinkExpressRegisters, in, out );
        }
}

void 
ferol::FerolController::debugFerol( xgi::Input *in, xgi::Output *out)
{   
    ferol::HardwareDebugger hwdebug( logger_, &ferol_, &frl_ );
    std::list< utils::HardwareDebugItem > ferolRegisters = hwdebug.getFerolRegisters();
    webServer_P->printDebugTable( "Ferol Registers", ferolRegisters, in, out );
}

void 
ferol::FerolController::debugFrl( xgi::Input *in, xgi::Output *out)
{   
    ferol::HardwareDebugger hwdebug( logger_, &ferol_, &frl_ );
    std::list< utils::HardwareDebugItem > frlRegisters = hwdebug.getFrlRegisters();
    webServer_P->printDebugTable( "Frl Registers", frlRegisters, in, out );
}

void 
ferol::FerolController::debugBridge( xgi::Input *in, xgi::Output *out)
{   
    ferol::HardwareDebugger hwdebug( logger_, &ferol_, &frl_ );
    std::list< utils::HardwareDebugItem > bridgeRegisters = hwdebug.getBridgeRegisters();
    webServer_P->printDebugTable( "Bridge Registers", bridgeRegisters, in, out );
}

void 
ferol::FerolController::monitoringWebPage( xgi::Input *in, xgi::Output *out )
{
    webServer_P->monitoringWebPage( in, out );
}

void
ferol::FerolController::dumpHardwareRegisters()
{
    HardwareDebugger hwdebug( logger_, &ferol_, &frl_ );
    hwdebug.dumpHardwareRegisters( "_manual");
}

void 
ferol::FerolController::jsonUpdate( xgi::Input *in, xgi::Output *out )
{
    uint32_t tmp = appIS_.getuint32( "testCounter" ) + 1;
    appIS_.setuint32( "testCounter", tmp );
    webServer_P->jsonUpdate( in, out );
}

void 
ferol::FerolController::jsonInfoSpaces( xgi::Input *in, xgi::Output *out )
{
    webServer_P->jsonInfoSpaces( in, out );
}

void
ferol::FerolController::xdaqWebPage( xgi::Input *in, xgi::Output *out )
{
    this->Default( in, out );
}

// This function just forwards the state change callback to the Statemachine. 
// Unfortunately in xdaq it is not possible to let the callback work on another
// object than "this". One would have to re-implement the bind function so that
// it takes another object as argument on which the callback function will be 
// called.
xoap::MessageReference
ferol::FerolController::changeState( xoap::MessageReference msg ) 
    throw (xoap::exception::Exception)
{
    return fsm_.changeState( msg );
}

xoap::MessageReference 
ferol::FerolController::ParameterSet( xoap::MessageReference msg ) 
    throw (xoap::exception::Exception)
{
    xoap::MessageReference reply = xdaq::Application::ParameterSet(msg);
    DEBUG("ParameterSet called");
    // update local copies of parameters
    appIS_.readInfoSpace();
    // this pushes the changes to the monitorable copy of the application infospace:
    appIS_.pushInfospace();
    return reply;
}

ferol::FerolController::~FerolController() 
{

}

void
ferol::FerolController::actionPerformed( xdata::Event &e )
{

    if ( e.type() == "urn:xdaq-event:setDefaultValues" )
        {
            DEBUG( "setting default values: here we initiize StreamInfoSpaces" );
            operationMode_ = appIS_.getstring( "OperationMode" );
            dataSource_ = appIS_.getstring( "DataSource" );
            if ( operationMode_ == FRL_MODE )
                { 
                    DEBUG( "setting input source to FRL" );
                    inputIS_.setInputSource( FerolStreamInfoSpaceHandler::FRL );
                    inputIS_.registerUpdater( &frl_ );
                }
            else
                {
                    if ( (dataSource_ == GENERATOR_SOURCE) ||
                         (dataSource_ == L6G_SOURCE) ||
                         (dataSource_ == L6G_CORE_GENERATOR_SOURCE) ||
                         (dataSource_ == L6G_LOOPBACK_GENERATOR_SOURCE) )
                        { 
                            DEBUG( "setting input source to FEROL6G" );
                            inputIS_.setInputSource( FerolStreamInfoSpaceHandler::FEROL6G );
                        }
                    else if ( (dataSource_ == L10G_SOURCE) ||
                              (dataSource_ == L10G_CORE_GENERATOR_SOURCE ) )
                        { 
                            DEBUG( "setting input source to FEROL10G" );
                            inputIS_.setInputSource( FerolStreamInfoSpaceHandler::FEROL10G );
                        }
                    else
                        {
                            ERROR( "FATAL Software/configuration bug: no reasonable data Source set: " << dataSource_ );
                            exit(-1);
                        }
                    inputIS_.registerUpdater( &ferol_ );
                }
        }
   // setup the utils::Monitoring. 
    // First we build infospaces. Then we setup the itemSets for the 
    // use in the Hyperdaq web page (i.e. the "flashlists" of the hyperdaq
    // page)


    monitor_.addInfoSpace( &inputIS_ );

    webServer_P = new ferol::FerolWebServer( monitor_, 
                                             this->getApplicationDescriptor()->getURN(), 
                                             this->getApplicationDescriptor()->getContextDescriptor()->getURL(), 
                                             logger_ );



    const std::tr1::unordered_map<std::string, utils::InfoSpaceHandler *> ismap = monitor_.getInfoSpaceMap();
    std::tr1::unordered_map<std::string, utils::InfoSpaceHandler *>::const_iterator it;
    for ( it=ismap.begin(); it!=ismap.end(); it++ )
        {
            std::string name = (*it).first;
            utils::InfoSpaceHandler *info = (*it).second;

            utils::WebStaticContentTab *docTab = new utils::WebStaticContentTab( name + " Doc.",
                                                                   info->getDocumentation());
            webServer_P->registerTab( docTab );

        }

    utils::WebStaticContentTab *docTab = new utils::WebStaticContentTab( "Documentation", htmlDocumentation_ );

    webServer_P->registerTab( docTab );

    // the Infospaces are registered with the utils::Monitor, and the Itemsets are already
    // built. Therefore we can start the monitoring.
    monitor_.startMonitoring();

}

///////////////////////////////// Callbacks for state transitions ///////////////////////////////////////////////

// In case the error was caused by a StateMachine error (e.g. a wrong state transistion)
// we get a toolbox::fsm::FailedEvent. In case we have a spontaneous error due to for 
// example synch lost draining in the middle of a run, we get a simple toolbox Event. 
// The latter does not contain information on the originating exception. Therefore there
// is no usefull stuff we could do here (not event printing stuff out).
void ferol::FerolController::FailAction (toolbox::Event::Reference e)
{	  
    ERROR ("Entered FailAction");
}


void ferol::FerolController::ConfigureAction(toolbox::Event::Reference e)
    throw( toolbox::fsm::exception::Exception )
{
    timer_.start();    

    appIS_.readInfoSpace(); 
    appIS_.pushInfospace();
    
    statusIS_.setuint32( "slotNumber", appIS_.getuint32( "slotNumber" ), true );

    if ( (! appIS_.getbool( "enableStream0" ))  && (! appIS_.getbool( "enableStream1" )) )
        {
            statusIS_.setstring( "subState", "inactive", true );
            return;
        }
    else 
        {
            statusIS_.setstring( "subState", "active", true );
        }

    // try and lock the hardware

    hwLocker_.lock();

    if ( ! hwLocker_.lockedByUs()  )
        {
            std::string lockstate = hwLocker_.updateLockStatus();
            std::stringstream msg;
            msg <<  "Cannot get Hardware Lock for FerolController in slot " 
                << appIS_.getuint32( "slotNumber" )
                << ". The lock status is \"" << lockstate << "\"";
            FATAL( msg.str() );
            XCEPT_DECLARE(utils::exception::HardwareNotAvailable, e, msg.str());
            notifyQualified( "error", e );
            fsm_.gotoFailed( e );
        }

    try
        {
            // if the firmware in the Frl needs to be changed, the ferol Configuration Space also needs to tbe 
            // saved and re-loaded. For this the PCIDevice of the Ferol must exist. Therfore the instantiateHardware
            // is called on the ferol before the configuration of the Frl. 
            // The entire configuration of the Ferol must not be done before the FRL configuration since the TCP
            // connection on the DAQ link breaks during the re-writing of the configuration space (WHY?). If then
            // the configuration is repeated everything is fine but currently the ptFerol does not survive this 
            // re-connection procedure. (Remember that the Ferol just sends a "reset" to abort this connection. This
            // might get lost if there is heavy load on the network. Then the receiver would need some additional 
            // intelligence to handle an incoming TCP-Sync request on the same port.)
            ferol_.instantiateHardware();
            if ( operationMode_ != FEDKIT_MODE )
                {
                    frl_.configure( &ferol_ );
                }
            ferol_.configure(); 

        }
    catch ( utils::exception::Exception &e )
        {
            INFO("going to failed since caught exception during frl configure");
            fsm_.gotoFailed( e );
        }
    timer_.stop();
    uint32_t time = timer_.read();
    INFO("ConfigureAction needed " << time << " mu seconds.");
}

void ferol::FerolController::EnableAction(toolbox::Event::Reference e) 
    throw( toolbox::fsm::exception::Exception )
{
    timer_.start();
    try
        {
            ferol_.enable();
            frl_.enable();
        }
    catch ( utils::exception::Exception &e )
        {
            fsm_.gotoFailed( e );
        }

    timer_.stop();
    uint32_t time = timer_.read();
    INFO("EnableAction needed " << time << " mu seconds.");
    
}

void ferol::FerolController::SuspendAction(toolbox::Event::Reference e) 
    throw( toolbox::fsm::exception::Exception )
{
    try        
        {
            // The intelligence is in the Frl and Ferol objects: they know 
            // if they have to do something.
            ferol_.suspendEvents();
            frl_.suspendEvents();
        }
    catch ( utils::exception::Exception &e )
        {
            fsm_.gotoFailed( e );
        }
}
 
 
 
void ferol::FerolController::ResumeAction(toolbox::Event::Reference e) 
    throw( toolbox::fsm::exception::Exception )
{
    try
        {
            // The intelligence is in the Frl and Ferol objects: they know 
            // if they have to do something.
            ferol_.resumeEvents();
            frl_.resumeEvents();
        }
    catch ( utils::exception::Exception &e )
        {
            fsm_.gotoFailed( e );
        }
}

 

void ferol::FerolController::StopAction(toolbox::Event::Reference e) 
    throw( toolbox::fsm::exception::Exception )
{
    timer_.start();
    if ( appIS_.getbool( "lightStop" ) ) 
        {
            try
                {
                    frl_.stop();
                    ferol_.stop();
                }
            catch ( utils::exception::Exception &e )
                {
                    fsm_.gotoFailed( e );
                }
        }
    else
        { 
            this->HaltAction( e );
            this->ConfigureAction( e );
        }
    timer_.stop();
    uint32_t time = timer_.read();
    INFO("StopAction needed " << time << " mu seconds.");
}



void ferol::FerolController::HaltAction(toolbox::Event::Reference e)
    throw( toolbox::fsm::exception::Exception )
{
    timer_.start();
    try
        {
            frl_.halt();
            ferol_.halt();
        }
    catch ( utils::exception::Exception &e )
        {
            hwLocker_.unlock();
            timer_.stop();
            fsm_.gotoFailed( e );
        }

    hwLocker_.unlock();
    timer_.stop();
    uint32_t time = timer_.read();
    INFO("HaltAction needed " << time << " mu seconds.");

}

xoap::MessageReference ferol::FerolController::laserOn( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{
    try
        {
            ferol_.controlSerdes( true );
        }
    catch (utils::exception::FerolException &e)
        {
            ERROR( "Problem switching on the laser: " << e.what() );
            return utils::SOAPFSMHelper::makeSoapFaultReply("laserOnResponse", e.what() );            
        }

    DEBUG( "Laser has been switched on, serdes is up." );
    return utils::SOAPFSMHelper::makeSoapReply( "laserOnResponse", "ok" );
}

xoap::MessageReference ferol::FerolController::laserOff( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{
    try
        {
            ferol_.controlSerdes( false );
        }
    catch (utils::exception::FerolException &e)
        {
            ERROR( "Problem switching off the laser: " << e.what() );
            return utils::SOAPFSMHelper::makeSoapFaultReply("laserOffResponse", e.what() );            
        }

    DEBUG( "Laser has been switched OFF, serdes is down." );
    return utils::SOAPFSMHelper::makeSoapReply( "laserOffResponse", "ok" );
}

xoap::MessageReference ferol::FerolController::l0DaqOn( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{
    int ret = ferol_.daqOn( 0 );
    if ( ret == 0 )
        {
            DEBUG( "LDOWN on sender site should be ON." );
            return utils::SOAPFSMHelper::makeSoapReply( "l0DaqOnResponse", "ok" );
        } 
    else 
        {
            return utils::SOAPFSMHelper::makeSoapReply( "l0DaqOnResponse", "error" );
        }
}

xoap::MessageReference ferol::FerolController::l0DaqOff( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{

    int ret = ferol_.daqOff( 0 );
    if ( ret == 0 )
        {
            DEBUG( "LDOWN on sender site should be off." );
            return utils::SOAPFSMHelper::makeSoapReply( "l0DaqOffResponse", "ok" );
        } 
    else 
        {
            return utils::SOAPFSMHelper::makeSoapReply( "l0DaqOnResponse", "error" );
        }
}

xoap::MessageReference ferol::FerolController::softTrigger( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{
    
    frl_.softTrigger();
    // Always answer with ok. This is a bit lazy...
    return utils::SOAPFSMHelper::makeSoapReply( "softTriggerResponse", "ok" );

}

xoap::MessageReference ferol::FerolController::sfpStatus( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{
    try {
        DEBUG("trying 1");
        ferol_.readSFP( 1 );
        // Always answer with ok. This is a bit lazy...
        DEBUG("trying 1 ok");
    }
    catch( utils::exception::FerolException e )
        {
            ERROR( "Problem when reading the SFP status: " << e.what() );
        }
    try {
        DEBUG("trying 0");
        ferol_.readSFP( 0 );
        DEBUG("trying 0 ok");
        // Always answer with ok. This is a bit lazy...
    }
    catch( utils::exception::FerolException e )
        {
            ERROR( "Problem when reading the SFP status: " << e.what() );
        }
    return utils::SOAPFSMHelper::makeSoapReply( "sfpStatusResponse", "ok" );

}



//////////////////////////////////////// Debugging only /////////////////////////
xoap::MessageReference ferol::FerolController::readItem( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{
  
    ferol::HardwareDebugger hwdebug( logger_, &ferol_, &frl_ );

    return hwdebug.readItem( msg );

}

xoap::MessageReference ferol::FerolController::writeItem( xoap::MessageReference msg ) 
    throw( xoap::exception::Exception) 
{
    
    ferol::HardwareDebugger hwdebug( logger_, &ferol_, &frl_ );

    return hwdebug.writeItem( msg );

}

xoap::MessageReference ferol::FerolController::resetMonitoring( xoap::MessageReference msg )
    throw( xoap::exception::Exception)
{
    try
        {
	    frl_.resetMonitoring();
	}
    catch( utils::exception::FrlException &e )
        {
	    return  utils::SOAPFSMHelper::makeSoapReply( "resetMonitoring", e.what() );
	}
    return utils::SOAPFSMHelper::makeSoapReply( "resetMonitoringResponse", "ok" );
}

xoap::MessageReference ferol::FerolController::dumpHardwareRegisters( xoap::MessageReference msg )
    throw( xoap::exception::Exception)
{
    try
        {
	    this->dumpHardwareRegisters();
	}
    catch( utils::exception::FrlException &e )
        {
	    return  utils::SOAPFSMHelper::makeSoapReply( "resetMonitoring", e.what() );
	}
    return utils::SOAPFSMHelper::makeSoapReply( "dumpHardwareRegistersResponse", "ok" );
}

xoap::MessageReference ferol::FerolController::resetVitesse( xoap::MessageReference msg )
    throw( xoap::exception::Exception)
{
    try
        {
            ferol_.resetVitesse( 0 );
            ferol_.resetVitesse( 1 );
        }
    catch( utils::exception::FerolException &e )
        {
            return utils::SOAPFSMHelper::makeSoapReply( "resetVitesse", e.what() );
        }
    return utils::SOAPFSMHelper::makeSoapReply( "resetVitesseResponse", "ok" );
}

std::string
ferol::FerolController::fillDocumentation()
{
    const char *doc =
        "<center> <H1>Ferol Documentation for Insiders</H1></center>\n"
        "<H2>Documentation of activities in the software during the state transitions</H2>\n"
        "\n"
        "<H3>Configure</H3>\n"
        "  <ol>\n"
        "    <li>\n"
        "      Read and push the Application Infospace. This guarantees that the values set in the \n"
        "      Application Infospace are copied to the local variables in the Infospacehandlers. This\n"
        "      is valid for values set in the xdaq configuration file and for values set via the \n"
        "      function manager in the soap \"Configure\" function call.\n"
        "    </li>\n"
        "    <li>\n"
        "      Copy the slot number from the Application Infospace (a configuration parameter) into\n"
        "      the StatusInfospace.\n"
        "    </li>\n"
        "    <li>\n"
        "      Set the subState to \"active\" if at least one input stream is enabled.\n"
        "    </li>\n"
        "    <li>\n"
        "      Try and lock the hardware via the HardwareLocker class instance.\n"
        "    </li>\n"
        "    <li>\n"
        "      Instantiate the Ferol Hardware.\n"
        "      <br/>\n"
        "      This must be done before the configuration of the Frl\n"
        "      since during the Frl configuration it might be necessary to change the Frl firmware.\n"
        "      If this needs to be done the, the Ferol hardware device needs to exist already since\n"
        "      the PCI configuration of the Ferol needs to be saved and written back to the PCI \n"
        "      config space during this operation. \n"
        "      <br/>\n"
        "      The entire configuration of the Ferol must not be done before the FRL configuration since the TCP\n"
        "      connection on the DAQ link breaks during the re-writing of the configuration space (WHY?). If then\n"
        "      the configuration is repeated everything is fine but currently the ptFerol does not survive this \n"
        "      re-connection procedure. (Remember that the Ferol just sends a \"reset\" to abort this connection. This\n"
        "      might get lost if there is heavy load on the network. Then the receiver would need some additional \n"
        "      intelligence to handle an incoming TCP-Sync request on the same port.)\n"
        "    </li>\n"
        "    <ol>\n"
        "      <li>\n"
        "        Call ferol.createFerolDevice\n"
        "      </li>\n"
        "       <ol>\n"
        "         <li>\n"
        "           Check that the hardware is locked by this application.\n"
        "         </li>\n"
        "         <li>\n"
        "           Scan the crate for the Ferol hardware corresponding to this application. The hardware \n"
        "           which corresponds to the slot number given in the configuration needs to be found. This\n"
        "           is possible since the slot number is encoded with some custom pins in the CompactPCI \n"
        "           backplane and this number is mapped into the PCI configuration space. (In case of the \n"
        "           FEDKit, where no CompactPCI crate is present, the slot number is interpreted as PCI \n"
        "           unit number.).\n"
        "         </li>\n"
        "         <li>\n"
        "           Once the device is found the firmware version is checked against the values hardcoded \n"
        "           in the file include/ferol/ferolConstants.h except for the configuration parameter \n"
        "           \"noFirmwareVersionCheck\" is set to true. This check indeed checks the hardware revision, \n"
        "           the firmware type and the firmware version agains the operational mode of the application.\n"
        "         </li>\n"
        "         <li>\n"
        "           The SlinkExpressCore object is instantiated. (No action on the hardware.)\n"
        "         </li>\n"
        "         <li>\n"
        "           At this point the global flag \"ferol.hardwareCreated_\" is set. This flag is used by various \n"
        "           threads and routines to determine if the the pointer to HAL device is valid. \n"
        "         </li>\n"
        "         <li>\n"
        "           Serial number and hardware MAC addresse are read.\n"
        "         </li>\n"
        "       </ol>\n"
        "       <li>\n"
        "         Reset the TCP/IP connections of both TCP/IP streams. (In case there is something left over\n"
        "         from a previous run which did not finish properly.)\n"
        "       </li>\n"
        "    </ol>\n"
        "    <li>\n"
        "      Configure the frl object if the operationMode is anything but the FEDKIT_MODE.\n"
        "    </li>\n"
        "     <ol>\n"
        "       <li>\n"
        "         Call frl.createHwDevice\n"
        "       </li>\n"
        "       <ol>\n"
        "         <li>\n"
        "           Verify that the hardware is locked by this application.\n"
        "         </li>\n"
        "         <li>\n"
        "           Scan the PCI bus for the hardware in the slot corresponding to the configutation parameter.\n"
        "           The procedure is the same as in the case for the Ferol. The geographical slot number encoded\n"
        "           in the custom backplane is mapped into the PCI Configuration Space of the Bridge FPGA.\n"
        "         </li>\n"
        "         <li>\n"
        "           Once the hardware has been set issue a softReset of the bridge FPGA. Then wait 1000us (the soft\n"
        "           reset takes 82us according to Dominique.)\n"
        "         </li>\n"
        "         <li>\n"
        "           Create the HAL PCIDevice for the Frl FPGA (the main FPGA on the Frl board. Both the \n"
        "           bridge and the frl FPGA are handled by dedicated PCI Device instances). \n"
        "         </li>\n"
        "         <li>\n"
        "           Check the hardware revision, firmware type and firmware version of the Frl against the operatinal\n"
        "           mode (except for the case that the configuration parameter \"noFirmwareVersionChec\" is set). In\n"
        "           case it is necessary the firmware is automatically changed from the DAQ version to the emulator\n"
        "           version. (The EPROM of the Frl FPGA contains different images for these two modes of operation.)\n"
        "         </li>\n"
        "         <li>\n"
        "           If the firmware had been changed in the previous step, issue another softReset to the bridge.\n"
        "         </li>\n"
        "         <li>\n"
        "           For every enabled input stream create a SlinkStream object.\n"
        "         </li>\n"
        "       </ol>\n"
        "       <li>\n"
        "         Reset the bit DAQ_mode of the SLINK. This brings the SLINK into \"command mode\".\n"
        "       </li>\n"
        "       <li>\n"
        "         Issue a softwareReset.\n"
        "       </li>\n"
        "       <li>\n"
        "         Reset the bits \"enableFifoMonCounter\" and \"enableHistograms\".\n"
        "       </li>\n"
        "       <li>\n"
        "         Set the maximal fragment size. (Beyond this size fragments are truncated in the Frl.)\n"
        "       </li>\n"
        "       <li>\n"
        "         Set the expected source IDs for both streams.\n"
        "       </li>\n"
        "       <li>\n"
        "         The following is done for both Slink inputs. The SlinkStream objects execute the commands \n"
        "         (but only in case stream is enabled): \n"
        "         Set the DC balance option of the SLINK if the configuration parameter is set, and deskew the links \n"
        "         if the configuration demands it. (NB: The deskew procedure requires the DCBalance to be switched on!) \n"
        "         Then test the SLINK with the test procedure. Then enable the Slink. In normal operation in CMS we \n"
        "         DO NOT USE Deskew OR DCBalance on these links!\n"
        "       </li>\n"
        "     </ol>\n"
        "    <li>\n"
        "      Configure the ferol object.\n"
        "    </li>\n"
        "     <ol>\n"
        "       <li>\n"
        "         On any enabled input stream call daqOff. This is done in case the previous run ended \n"
        "         ungracefully.\n"
        "       </li>\n"
        "       <li>\n"
        "         Do a SoftwareReset.\n"
        "       </li>\n"
        "       <li>\n"
        "         Do the Reset_Counters.\n"
        "       </li>\n"
        "       <li>\n"
        "         Setup the input source. Call createDataSource on the DataSourceFactory. \n"
        "       </li>\n"
        "       <li>\n"
        "         Set the dataSource in the Ferol register. This implies setting a code in a register of the Ferol.\n"
        "         Only in case the data source is one of the optical inputs if the FEROL the following operations\n"
        "         are executed before this:\n"
        "         <ol>\n"
        "           <li>Check that an SFP is plugged into the corresponding SFP case of the link.</li>\n"
        "           <li>Check that the SERDES of the link is up and running.</li>\n"
        "           <li>Issue a Re-sync on the link: this resets the sequence numbers of the packets of this link\n"
        "             and clears all internal buffer memories.</li>\n"
        "         </ol>\n"
        "       </li>\n"
        "       <li>\n"
        "         Call suspendEvents in case the previous run with event generators did not end gracefully.\n"
        "       </li>\n"
        "       <li>\n"
        "         Set the L5gb_FEDIS_SlX0/1 registers (the event ids of the streams participating in the run.\n"
        "       </li>\n"
        "       <li>\n"
        "         Now set up the 10Gb TCP/IP link to the DAQ.\n"
        "       </li>\n"
        "       <ol>\n"
        "         <li>\n"
        "           Set the bit TCP_SOCKET_BUFFER_DDR if this is requested by the configuration.\n"
        "         </li>\n"
        "         <li>\n"
        "           Check that the XAUI link is up: Read SERDES_STATUS and check the value.\n"
        "         </li>\n"
        "         <li>\n"
        "           Check the status of the Vitesse chip via the MDIO bus: \n"
        "         </li>\n"
        "         <li>\n"
        "           Write the NETMASK. If the netmask is different from 0 the IP_GATEWAY has to be set by the\n"
        "           user, too, since the ferol assumes the user wants to route packets. (This is not used in\n"
        "           the CMS network.)\n"
        "         </li>\n"
        "         <li>\n"
        "           Check the configuration parameter \"SourceIP\". If it is set to \"auto\" it is assumed \n"
        "           that the source ip has been set in the hardware by a startup script on the controller\n"
        "           PC. This IP is checked to be different from '0'. If there is a \"normal\" IP string in\n"
        "           this configuration parameter it is used as the source address and written into the hardware.\n"
        "           IP numbers and hostnames are accepted.\n"
        "         </li>\n"
        "         <li>\n"
        "           Check that the IP addresse and the MAC address are unique on the network. This is done by\n"
        "           sending ARP-probe packets. \n"
        "         </li>\n"
        "         <li>\n"
        "           Set a long list of TCP/IP specific parameters. \n"
        "         </li>\n"
        "         <li>\n"
        "           Do the arp-request. The number of trials and the time between the trials are configuration\n"
        "           parameters.\n"
        "         </li>\n"
        "         <li>\n"
        "           Open the TCP/IP connection to the RU.\n"
        "         </li>\n"
        "         <li>\n"
        "           Set the ENA_PAUSE_FRAME register according to the configuration.\n"
        "         </li>\n"
        "       </ol>\n"
        "    </ol>\n"
        "  </ol>\n"
        "<H3>Enable</H3>\n"
        "  <ol>\n"
        "    <li>\n"
        "      Enable the ferol object: This resets the counters of the TCP monitoring and, with a different\n"
        "      reset command, the monitoring counters for the SLINK express links. If the datasource\n"
        "      is the SLINKExpress the daqOn command is sent to the links sender cores which participate in the \n"
        "      run. <br/>\n"
        "      Having the monitoring counters reset only in enable does allow for some post mortem analysis after the \n"
        "      run has stopped. On the other hand it makes it more difficult to understand if already in Configure \n"
        "      something is flowing/counting. But you cannot have everything! :-)\n" 
        "    </li>\n"
        "    <li>\n"
        "      Enable the frl object. The resetFifoMonCounter is executed followed by enableFifoMonCounter.\n"
        "      The resetHistograms is executed followed by enableHistrograms.\n"
        "      If in Frl Mode and the data source is the SLINK, the globalFifoReset is executed. Then DAQ_mode\n"
        "      is set. \n"
        "    </li>\n"
        "  </ol>\n";
    return std::string( doc );
}

bool
ferol::FerolController::updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo )
{
    return true;
}
