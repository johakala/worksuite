#include "ferol/DataSourceIF.hh"
#include "ferol/loggerMacros.h"

ferol::DataSourceIF::DataSourceIF( HAL::HardwareDeviceInterface *device_P_in,
                                   utils::InfoSpaceHandler & appIS,
                                   Logger logger )
    : appIS_( appIS )
{
  operationMode_ = appIS.getstring( "OperationMode" );
  dataSource_    = appIS.getstring( "DataSource" );
  stream0_       = appIS.getbool( "enableStream0" );
  stream1_       = appIS.getbool( "enableStream1" );
  device_P       = device_P_in;
  logger_        = logger;
}
