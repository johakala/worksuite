#!/usr/bin/perl -w
use strict;


# my $answer = "HTTP/1.1 200 OK
# Server: Apache-Coyote/1.1
# Content-Type: text/xml;charset=utf-8
# Transfer-Encoding: chunked
# Date: Fri, 16 Nov 2012 09:12:39 GMT
# Connection: close
# 
# 341
# <?xml version=\"1.0\" encoding=\"utf-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soapenv:Body><ns1:sendNotificationReplyResponse soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns1=\"http://replycommandreceiver.ws.fm.rcms\"><sendNotificationReplyReturn href=\"#id0\"/></ns1:sendNotificationReplyResponse><multiRef id=\"id0\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xsi:type=\"ns2:ReplyCommandBean\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns2=\"urn:ReplyCommandReceiver\"><accepted xsi:type=\"soapenc:string\">OK</accepted><userField xsi:type=\"soapenc:string\" xsi:nil=\"true\"/></multiRef></soapenv:Body></soapenv:Envelope>
# 0
# ";
# 

my $answer = "HTTP/1.0 200 OK\r\n\r\n\<?xml version=\"1.0\" encoding=\"utf-8\"?><soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soapenv:Body><ns1:sendNotificationReplyResponse soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns1=\"http://replycommandreceiver.ws.fm.rcms\"><sendNotificationReplyReturn href=\"#id0\"/></ns1:sendNotificationReplyResponse><multiRef id=\"id0\" soapenc:root=\"0\" soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xsi:type=\"ns2:ReplyCommandBean\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:ns2=\"urn:ReplyCommandReceiver\"><accepted xsi:type=\"soapenc:string\">OK</accepted><userField xsi:type=\"soapenc:string\" xsi:nil=\"true\"/></multiRef></soapenv:Body></soapenv:Envelope>\r\n";

while (1) {
    print "\n\nListening again...\n\n";
#    `{ echo -ne \"HTTP/1.0 200 OK\r\n\r\n\"; cat reply.txt; } | nc -l 3003 `;

    my $out = `nc -l 3003 << eof
$answer
eof`;
    print "$out\n\n";

}
#    print "RCMSStateListener Received \n$out\n\n";
#
#
#    $out = `nc srv-c2d06-18.cms 2002 <<eof
#$answer
#eof`;



#   <soap-env:Envelope soap-env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
#     <soap-env:Header/>
#     <soap-env:Body>
#       <ns1:sendNotificationReply soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://replycommandreceiver.ws.fm.rcms">
#         <notReply href="#id0"/>
#       </ns1:sendNotificationReply>
#       <multiRef id="id0" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns2="urn:ReplyCommandReceiver" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns2:NotificationReplyBean">
#         <asynchronousReply href="#id1"/>
#         <sourceURL xsi:type="soapenc:string">http://srv-c2d06-18.cms:2002/urn:xdaq-application:lid=109</sourceURL>
#         <stateName xsi:type="soapenc:string">Enabled</stateName>
#         <userField xsi:type="soapenc:string"></userField>
#         <timeOut href="#id2"/>
#         <timeStamp href="#id3"/>
#         <transactionID href="#id4"/>
#       </multiRef>
#       <multiRef id="id1" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="xsd:boolean">true
#       </multiRef>
#       <multiRef id="id2" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="xsd:long">0
#       </multiRef>
#       <multiRef id="id3" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="xsd:long">1352984285
#       </multiRef>
#       <multiRef id="id4" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="xsd:long">0
#       </multiRef>
#     </soap-env:Body>
#   </soap-env:Envelope>
#   
#   #Answer:
#   <?xml version="1.0" encoding="utf-8"?>
#   
#   <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
#     <soapenv:Body>
#       <ns1:sendNotificationReplyResponse soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="http://replycommandreceiver.ws.fm.rcms">
#   <sendNotificationReplyReturn href="#id0"/>
#       </ns1:sendNotificationReplyResponse>
#       <multiRef id="id0" soapenc:root="0" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xsi:type="ns2:ReplyCommandBean" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns2="urn:ReplyCommandReceiver">
#         <accepted xsi:type="soapenc:string">OK</accepted>
#         <userField xsi:type="soapenc:string" xsi:nil="true"/>
#       </multiRef>
#     </soapenv:Body>
#   </soapenv:Envelope>
