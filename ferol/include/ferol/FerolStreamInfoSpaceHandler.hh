#ifndef __FerolStreamInfoSpaceHandler
#define __FerolStreamInfoSpaceHandler

#include "ferol/StreamInfoSpaceHandler.hh"

namespace ferol
{
    class FerolStreamInfoSpaceHandler : public StreamInfoSpaceHandler 
    {

    public:
        
        enum HwInput { FEROL6G, FEROL10G, FRL };

        FerolStreamInfoSpaceHandler(  xdaq::Application *xdaq,
                                      std::string name,
                                      utils::InfoSpaceHandler *appIS,
                                      bool noAutoPush = false );

        void createstring( std::string name, std::string frlHwName, std::string ferol6GHwName,
                           std::string ferol10GHwName,
                           UpdateType frlUpdateType, UpdateType ferolUpdateType, 
                           std::string format, std::string doc);

        void createuint32( std::string name, std::string frlHwName, std::string ferol6GHwName,
                           std::string ferol10GHwName,
                           UpdateType frlUpdateType, UpdateType ferolUpdateType, 
                           std::string format, std::string doc);

        void createuint64( std::string name, std::string frlHwName, std::string ferol6GHwName,
                           std::string ferol10GHwName,
                           UpdateType frlUpdateType, UpdateType ferolUpdateType, 
                           std::string format, std::string doc);

        void createdouble( std::string name, std::string frlHwName, std::string ferol6GHwName,
                           std::string ferol10GHwName,
                           UpdateType frlUpdateType, UpdateType ferolUpdateType, 
                           std::string format, std::string doc);

        void createbool  ( std::string name, std::string frlHwName, std::string ferol6GHwName,
                           std::string ferol10GHwName,
                           UpdateType frlUpdateType, UpdateType ferolUpdateType, 
                           std::string format, std::string doc);


        /**
         *
         *     @short This defines if the items are filled from SLINKexpress or 
         *            SLINK
         *            
         *            This routine is actually filling the infospace. 
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void setInputSource( HwInput inputSource );
        

    private:
        HwInput inputSource_;
        std::tr1::unordered_map< std::string, ISItem& > frlItemMap_;
        std::tr1::unordered_map< std::string, ISItem& > ferol6GItemMap_;
        std::tr1::unordered_map< std::string, ISItem& > ferol10GItemMap_;
    };
}

#endif /* __FerolStreamInfoSpaceHandler */
