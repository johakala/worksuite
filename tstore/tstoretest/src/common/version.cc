// $Id: version.cc,v 1.3 2007/05/11 16:01:31 brett Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/tstoretest/version.h"
#include "tstore/client/version.h"
#include "config/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xcept/version.h"
#include "xoap/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(tstoretest)

void tstoretest::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xoap);  
	CHECKDEPENDENCY(xdata); 
	CHECKDEPENDENCY(xdaq); 
	CHECKDEPENDENCY(tstoreclient);  
}

std::set<std::string, std::less<std::string> > tstoretest::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xoap);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xdaq);
	ADDDEPENDENCY(dependencies,tstoreclient);

	return dependencies;
}	
	
