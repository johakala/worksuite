// $Id: version.h,v 1.13 2008/09/11 08:56:30 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstoretest_version_h_
#define _tstoretest_version_h_

#include "config/PackageInfo.h"

#define TSTORETEST_VERSION_MAJOR 1
#define TSTORETEST_VERSION_MINOR 16
#define TSTORETEST_VERSION_PATCH 0
// If any previous versions available E.g. #define TSTORETEST_PREVIOUS_VERSIONS "3.8.0,3.8.1"

#define TSTORETEST_PREVIOUS_VERSIONS ""

//
// Template macros
//
#define TSTORETEST_VERSION_CODE PACKAGE_VERSION_CODE(TSTORETEST_VERSION_MAJOR,TSTORETEST_VERSION_MINOR,TSTORETEST_VERSION_PATCH)
#ifndef TSTORETEST_PREVIOUS_VERSIONS
#define TSTORETEST_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TSTORETEST_VERSION_MAJOR,TSTORETEST_VERSION_MINOR,TSTORETEST_VERSION_PATCH)
#else 
#define TSTORETEST_FULL_VERSION_LIST  TSTORETEST_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TSTORETEST_VERSION_MAJOR,TSTORETEST_VERSION_MINOR,TSTORETEST_VERSION_PATCH)
#endif 

namespace tstoretest
{
	const std::string package  =  "tstoretest";
	const std::string versions =  TSTORETEST_FULL_VERSION_LIST;
	const std::string description = "Table store test application";
	const std::string summary = "Table store test application";
	const std::string authors = "Angela Brett";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/TStore";
	
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
