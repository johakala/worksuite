// $Id: TStoreTest.h,v 1.9 2008/04/25 08:25:57 brett Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _TStoreTest_h_
#define _TStoreTest_h_

#include "xdaq/Application.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdaq/WebApplication.h"
#include "toolbox/Properties.h"
#include "xdata/Table.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPEnvelope.h"
#include "xdata/TimeVal.h"
#include "xdata/Mime.h"
#include "xdata/Vector.h"
#include "tstoretest/RandomData.h"

class TStoreTest: public xdaq::Application 
{	
	public:
	
	XDAQ_INSTANTIATOR();
	
	TStoreTest(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void configureRandomData();
	
	//test standard interface
	void connect(std::string viewName,std::string password,std::string username,std::string timeout);
	void disconnect(std::string connectionID);
	void testQuery(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testDefinition(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testUpdate(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testInsert(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testConnect(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testRenew(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testDisconnect(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testClear(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testDelete(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testAddTable(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testAddTableToConfig(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testRemoveTable(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testDestroy(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	//test administrative interface
	void testGetViews(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testAddView(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testRemoveView(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testGetConfig(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testSetConfig(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void testSync(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	//other interface
	void addParameter(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	void setParameters(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);

	private:
	//web interface
	void outputCheckbox(xgi::Output * out,bool on,const std::string &name);
	void outputException(xgi::Output * out,xcept::Exception &e);
	void outputTable(xgi::Output * out,xdata::Table &results);
	void outputParameters(xgi::Output * out);
	void outputOption(xgi::Output * out,const std::string &name);
	void outputViewSelector(xgi::Output * out);
	void outputConnectionSelector(xgi::Output * out);
	void outputAdministrativeInterface(xgi::Output * out);
	void outputStandardInterface(xgi::Output * out);
	void outputControls(xgi::Input * in,xgi::Output * out);
	void outputHeader(xgi::Output * out);
	void outputFooter(xgi::Output * out);
	
	void setBoolean(xgi::Input * in,bool &on,const std::string &name);
	std::string getParameter(cgicc::Cgicc &cgi,const std::string &name);
	
	//constructing message
	std::string connection(xgi::Input * in);
	std::string connection(cgicc::Cgicc &cgi);
	std::string classNameForConnection(const std::string &view) throw (xcept::Exception);
	void addParameter(xoap::SOAPEnvelope &envelope,xoap::SOAPElement &element,const std::string &parameterName,const std::string &parameterValue,const std::string &viewClass);
	void addParametersToMessage(xoap::SOAPEnvelope &envelope,xoap::SOAPElement &element,const std::string &viewClass) throw (xcept::Exception);
	xoap::SOAPElement addCommand(xoap::SOAPEnvelope &envelope,const std::string &command,const std::string &connectionID);
	xoap::SOAPElement addAdministrativeCommand(xoap::SOAPEnvelope &envelope,const std::string &command,const std::string &attributeName="",const std::string &value="") throw (xoap::exception::Exception);
	static std::string keyString(const std::vector<std::string> &keyColumns);
	static void addTableTag(xoap::SOAPEnvelope &envelope,xoap::SOAPElement &command,const std::string &tableName,const std::vector<std::string> &tableNames=std::vector<std::string>()) throw (xgi::exception::Exception);
	static DOMDocument *parseString(const std::string &config) throw (xcept::Exception);
	static void copyNamespaceDeclarations(xoap::SOAPElement &destination,const DOMNode *source);
	void addConfigurationToMessage(xoap::SOAPElement &commandElement,const std::string &configuration) throw (xcept::Exception);

	//sending message
	xoap::MessageReference sendSyncMessage(const std::string &connectionID,const std::string &pattern,const std::string &mode) throw (xcept::Exception);
	xoap::MessageReference sendCommandToView(const std::string &viewName,const std::string &command) throw (xcept::Exception);
	xoap::MessageReference postSOAPMessage(xoap::MessageReference msg) throw (xcept::Exception);
	xoap::SOAPElement addConfigCommand(xoap::MessageReference msg,cgicc::Cgicc &cgi,const std::string &command) throw (xcept::Exception);
	void testTableRequest(xgi::Input * in, xgi::Output * out,const std::string& command) throw (xgi::exception::Exception);
	void runTableRequest(const std::string &viewName,const std::string &command,xdata::Table &results) throw (xcept::Exception);
	void runTableSend(const std::string &connectionID,const std::string &command,xdata::Table &results) throw (xcept::Exception);
	
	//processing reply
	void getNodesNamed(std::vector<DOMNode *> &nodes,xoap::MessageReference msg,const std::string &nodeName) throw (xcept::Exception);
	DOMNode *getNodeNamed(xoap::MessageReference msg,const std::string &nodeName) throw (xcept::Exception);
	bool responseHasSyncActions(xoap::MessageReference response) throw (xcept::Exception);
	
	//manipulating tables

	void changeCaseOfColumnNames(xdata::Table &table);
	std::string writeXML(DOMNode *node) throw ();
	
	std::map<const std::string,std::string> queryParameters_;
	std::string view_;
	std::string connectionID_;
	std::map<std::string,std::string> connectionIDs_;
	std::vector<std::string> views_;
	std::string lastMessage_; //the latest SOAP message
	std::string lastResponse_; //the response of the latest SOAP message
	std::string config_; //the results of the latest getConfig message
	std::string initialViewConfig_; //initial config for the last view added
	std::string dbname_; //database name for the view to be added
	std::string username_; //username for the view to be added
	std::string path_;
	std::string prefix_;
	std::string pattern_;
	std::string viewPath_; //path to add a new view to
	bool insertNulls_;
	bool insertInfinities_;
	bool dryRun_;
	bool subtables_;
	bool longStrings_;
	RandomData randomData_;
};

#endif
