// $Id: Client.cc,v 1.3 2007/06/05 09:14:28 brett Exp $
// A library to be used by TStore client applications for more easily generating required configuration etc.

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xoap/domutils.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "tstore/client/AttachmentUtils.h"
#include "tstore/client/LoadDOM.h"
#include "tstore/client/Client.h"

DOMElement *tstoreclient::createDOMElementForTable(DOMDocument *doc,/*const*/ xdata::Table &table,const std::string &tableName,const std::string &key) throw (xcept::Exception) {
	try {
		DOMElement *tableNode=doc->createElementNS(xoap::XStr("urn:xdaq-tstore:1.0"),xoap::XStr("tstore:table"));
		tableNode->setAttribute(xoap::XStr("name"),xoap::XStr(tableName));
		tableNode->setAttribute(xoap::XStr("key"),xoap::XStr(key));
		//viewNode->appendChild(tableNode);
		//otherwise, or perhaps in all cases, we need to add the same node to the response. But in this case the doc will be different.
		std::vector<std::string> columns=table.getColumns();
		for (std::vector<std::string>::iterator column=columns.begin(); column!=columns.end(); ++column) {
			DOMElement *columnNode=doc->createElementNS(xoap::XStr("urn:xdaq-tstore:1.0"),xoap::XStr("tstore:column"));
			columnNode->setAttribute(xoap::XStr("name"),xoap::XStr(*column));
			columnNode->setAttribute(xoap::XStr("type"),xoap::XStr(table.getColumnType(*column)));
			tableNode->appendChild(columnNode);
			
		}
		//tableNode->addNamespaceDeclaration("tstore","urn:xdaq-tstore:1.0");
		return tableNode;
	} catch (DOMException &e) {
		std::ostringstream error;
		error << "Could not create DOM element for table " << tableName << " because of error " << e.code << ": " << xoap::XMLCh2String(e.msg);
		XCEPT_RAISE(xcept::Exception,error.str());
	}
}

std::string tstoreclient::classNameForView(const std::string &viewName) throw () {
  	std::string className;
	std::string::size_type firstColon=viewName.find(':');
	if (firstColon!=std::string::npos) {
		std::string::size_type secondColon=viewName.find(':',firstColon+1);
		if (secondColon!=std::string::npos) {
			className=viewName.substr(0,secondColon);
		}
	}
	return className;
}

//returns the connection ID from the response to a connect message
std::string tstoreclient::connectionID(xoap::MessageReference response) throw (xcept::Exception) {
	DOMNode *connectResponse=tstoreclient::getNodeNamed(response,"connectResponse");
	try {
		return xoap::getNodeAttribute(connectResponse,"connectionID");
	} catch (xoap::exception::Exception &e) {
		XCEPT_RETHROW(xcept::Exception,"Response does not contain a connection ID",e);
	}
}
	
//I'm sure there was some rational reason why I didn't use getChildElements for this, but I have forgotten what it is
//todo: if possible, rewrite this using getChildElements and test it.
void tstoreclient::getNodesNamed(std::vector<DOMNode *> &nodes,xoap::MessageReference msg,const std::string &nodeName) throw (xcept::Exception) {
	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
 	xoap::SOAPBody body = envelope.getBody();
 	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();
	for (unsigned int itemIndex = 0; itemIndex < bodyList->getLength(); itemIndex++) {
		DOMNode* child = bodyList->item(itemIndex);
		if (child->getNodeType() == DOMNode::ELEMENT_NODE) {
			if (xoap::XMLCh2String(child->getLocalName()) == nodeName) {
				nodes.push_back(child);
			}
		}
	}
}

DOMNode *tstoreclient::getNodeNamed(xoap::MessageReference msg,const std::string &nodeName) throw (xcept::Exception) {
	std::vector<DOMNode *> nodes;
	getNodesNamed(nodes,msg,nodeName);
	if (nodes.empty()) {
		XCEPT_RAISE(xcept::Exception,"No node named "+nodeName);
	} else if (nodes.size()>1) {
		XCEPT_RAISE(xcept::Exception,"Multiple nodes named "+nodeName);
	} else {
		return nodes[0];
	}
}

//returns whether \a response contains any information on actions that were performed in a sync
//\a response should be the response of a TStore sync message, if it isn't then this function will throw an exception.
//It should be easy to create similar functions using getNodesNamed
//to return whether there is a node signifying a particular action was taken.
bool tstoreclient::responseHasSyncActions(xoap::MessageReference response) throw (xcept::Exception) {
	DOMNode *syncResponseNode=getNodeNamed(response,"syncResponse");
	return syncResponseNode->hasChildNodes();
}
