// $Id: GetViewsResponse.cc,v 1.4 2007/10/12 15:34:59 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/api/admin/GetViewsResponse.h"
#include "tstore/api/NS.h"
#include "xoap/MessageFactory.h"
#include "toolbox/net/URN.h"

tstore::api::admin::GetViewsResponse::GetViewsResponse(std::list<std::string>& sqlviews, std::list<std::string>& nestedviews ):
	sqlviews_ (sqlviews), nestedviews_(nestedviews)
{

	aheaders_.setAction(tstore::api::NamespaceUri + "/getViewsResponse");
	// empty configuratin in this case	
}

tstore::api::admin::GetViewsResponse::~GetViewsResponse()
{
}
	
tstore::api::admin::GetViewsResponse::GetViewsResponse(xoap::MessageReference& msg) 
	throw (tstore::api::admin::exception::Exception)
{	
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	xoap::SOAPName getViewsResponseName("getViewsResponse", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(getViewsResponseName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::admin::exception::Exception, "Invalid message: missing or bad <getViewsResponse/> element" );
	}
	
	xoap::SOAPName viewName("view", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> viewElements = bodyElements[0].getChildElements (viewName);
	
	xoap::SOAPName nameAttribute("name", "", "");
	for (std::vector<xoap::SOAPElement>::iterator vi = viewElements.begin(); vi != viewElements.end(); ++vi)
	{
		toolbox::net::URN id((*vi).getAttributeValue (nameAttribute));
		if ( tstore::api::nested::NamespaceUri.find(id.getNamespace()) != std::string::npos )
		{
			nestedviews_.push_back ( id.getNSS() );
		}
		else if ( tstore::api::sql::NamespaceUri.find(id.getNamespace()) != std::string::npos  )
		{
			sqlviews_.push_back ( id.getNSS() );
		}
		else
		{
		  	XCEPT_RAISE (tstore::api::admin::exception::Exception, "Invalid view id namespace" );	
		}	
	}
}		

xoap::MessageReference tstore::api::admin::GetViewsResponse::toSOAP()
{
	xoap::MessageReference response = xoap::createMessage();
	xoap::SOAPPart soap = response->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(response);

	xoap::SOAPName command = envelope.createName
		("getViewsResponse",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement commandElement = responseBody.addBodyElement(command);

	for (std::list<std::string>::iterator i = nestedviews_.begin(); i != nestedviews_.end(); ++i)
	{
		xoap::SOAPName viewName = envelope.createName
			("view",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
		xoap::SOAPName nameAttribute = envelope.createName("name", "", "");
		xoap::SOAPElement viewElement = commandElement.addChildElement(viewName);
		viewElement.addAttribute ( nameAttribute, tstore::api::nested::NamespaceUri + ":" + (*i) );
	}
	
	for (std::list<std::string>::iterator i = sqlviews_.begin(); i != sqlviews_.end(); ++i)
	{
		xoap::SOAPName viewName = envelope.createName
			("view",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
		xoap::SOAPName nameAttribute = envelope.createName("name", "", "");
		xoap::SOAPElement viewElement = commandElement.addChildElement(viewName);
		viewElement.addAttribute ( nameAttribute, tstore::api::sql::NamespaceUri + ":" + (*i) );
	}
	
	return response;
}
	
std::list<std::string>& tstore::api::admin::GetViewsResponse::getSQLViews()
{
	return sqlviews_;
}

std::list<std::string>& tstore::api::admin::GetViewsResponse::getNestedViews()
{
	return nestedviews_;
}
