// $Id: Sync.cc,v 1.4 2007/08/17 15:45:32 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xoap/MessageFactory.h"
#include "tstore/api/admin/Sync.h"
#include "tstore/api/NS.h"


tstore::api::admin::Sync::Sync(const std::string & connectionId, const std::string & mode, const std::string & pattern)
	: connectionId_(connectionId), mode_(mode), pattern_(pattern)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/sync");
}

tstore::api::admin::Sync::~Sync()
{
}

tstore::api::admin::Sync::Sync(xoap::MessageReference& msg) throw (tstore::api::admin::exception::Exception)
{
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName syncName("sync", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(syncName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::admin::exception::Exception, "Invalid message: missing or bad <sync/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName connectionId = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName mode = envelope.createName("mode", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName pattern = envelope.createName("pattern", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
      	connectionId_= bodyElements[0].getAttributeValue(connectionId);
	mode_ = bodyElements[0].getAttributeValue(mode);
	pattern_ = bodyElements[0].getAttributeValue(pattern);
}


xoap::MessageReference tstore::api::admin::Sync::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPName command = envelope.createName
		("sync",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);
	element.addNamespaceDeclaration(tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addNamespaceDeclaration(tstore::api::sql::NamespacePrefix, tstore::api::sql::NamespaceUri);
	xoap::SOAPName connectionId = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(connectionId, connectionId_);
	xoap::SOAPName pattern = envelope.createName("pattern", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(pattern, pattern_);
	xoap::SOAPName mode = envelope.createName("mode", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(mode, mode_);
	
	return request;
}

std::string tstore::api::admin::Sync::getConnectionId()
{
	return connectionId_;
}

std::string tstore::api::admin::Sync::getPattern()
{
	return pattern_;
}

std::string tstore::api::admin::Sync::getMode()
{
	return mode_;
}


	
