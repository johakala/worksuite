// $Id: AddViewResponse.cc,v 1.3 2007/08/15 14:45:33 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/api/admin/AddViewResponse.h"
#include "tstore/api/NS.h"
#include "xoap/MessageFactory.h"

tstore::api::admin::AddViewResponse::AddViewResponse()
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/AddViewResponse");
	// empty configuratin in this case	
}

tstore::api::admin::AddViewResponse::~AddViewResponse()
{
}
	
tstore::api::admin::AddViewResponse::AddViewResponse(xoap::MessageReference& msg) 
	throw (tstore::api::admin::exception::Exception)
{
	
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName addViewResponseName("addViewResponse", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(addViewResponseName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::admin::exception::Exception, "Invalid message: missing or bad <addViewResponse/> element" );
	}

}

		

xoap::MessageReference tstore::api::admin::AddViewResponse::toSOAP()
{
	xoap::MessageReference response = xoap::createMessage();
	xoap::SOAPPart soap = response->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(response);

	xoap::SOAPName command = envelope.createName
		("addViewResponse",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	responseBody.addBodyElement(command);

	return response;
}
	
