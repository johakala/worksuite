// $Id: Insert.cc,v 1.3 2007/08/16 13:59:32 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/MessageFactory.h"
#include "tstore/api/Insert.h"
#include "tstore/api/NS.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
		
tstore::api::Insert::Insert(const std::string & connectionId, const std::string & insertStatementName, xdata::Table::Reference& table)
	: connectionId_(connectionId), insertStatementName_(insertStatementName), table_(table)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/insert");
}

tstore::api::Insert::~Insert()
{
}
		
tstore::api::Insert::Insert(xoap::MessageReference& msg)
	throw (tstore::api::exception::Exception)
	:connectionId_(""), insertStatementName_(""), table_(0)
{
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName viewName("insert", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(viewName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::exception::Exception, "Invalid message: missing or bad <Insert/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName insertStatementName = envelope.createName("name", tstore::api::sql::NamespacePrefix, tstore::api::sql::NamespaceUri);
      	connectionId_ = bodyElements[0].getAttributeValue(connectionID);
	insertStatementName_ = bodyElements[0].getAttributeValue(insertStatementName);
	
	std::list<xoap::AttachmentPart*> attachments = msg->getAttachments();
	std::list<xoap::AttachmentPart*>::iterator j;
	for ( j = attachments.begin(); j != attachments.end(); j++ )
	{
		if ((*j)->getContentType() == "application/xdata+table")
		{
			xdata::exdr::FixedSizeInputStreamBuffer inBuffer((*j)->getContent(),(*j)->getSize());
			std::string contentEncoding = (*j)->getContentEncoding();
			std::string contentId = (*j)->getContentId();
			xdata::Table * t = new xdata::Table();
			try 
			{
				xdata::exdr::Serializer serializer;
				serializer.import(t, &inBuffer );
				xdata::Table::Reference tmp(t);
				table_ = tmp;
			}
			catch(xdata::exception::Exception & e )
			{
				// failed to import table
			}
		}
		else
		{
			// unknown attachment type
			std::stringstream msg;
			msg << "Unknown attachment type '" << (*j)->getContentType() << "' in <insert> message";
			XCEPT_RAISE (tstore::api::exception::Exception, msg.str() );
		}
	}
}



xoap::MessageReference tstore::api::Insert::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPName command = envelope.createName
		("insert",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);
	element.addNamespaceDeclaration(tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addNamespaceDeclaration(tstore::api::sql::NamespacePrefix, tstore::api::sql::NamespaceUri);

	
	xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(connectionID, connectionId_);
	
	xoap::SOAPName insertStatementName = envelope.createName("name", tstore::api::sql::NamespacePrefix, tstore::api::sql::NamespaceUri);
	element.addAttribute(insertStatementName, insertStatementName_);
	
	xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
	xdata::exdr::Serializer serializer;
	serializer.exportAll( &(*table_), &outBuffer );
	xoap::AttachmentPart* attachment = request->createAttachmentPart(outBuffer.getBuffer(), outBuffer.tellp(), "application/xdata+table");
	attachment->setContentEncoding("exdr");
	
	// Content-ID currently not used, so set a static text
	attachment->setContentId("table");
	request->addAttachmentPart(attachment);
	
	return request;
}

std::string tstore::api::Insert::getConnectionId()
{
	return connectionId_;
}

std::string tstore::api::Insert::getInsertStatementName()
{
	return insertStatementName_;
}

xdata::Table::Reference tstore::api::Insert::getTable()
{
	return table_;
}


