// $Id: ClearNested.cc,v 1.1 2007/10/12 15:36:28 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/MessageFactory.h"
#include "tstore/api/ClearNested.h"
#include "tstore/api/NS.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
		
tstore::api::ClearNested::ClearNested
(
	const std::string & connectionId,
	const std::string & tableName
)
	: connectionId_(connectionId), tableName_(tableName)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/clear");
}

tstore::api::ClearNested::~ClearNested()
{
}
		
tstore::api::ClearNested::ClearNested(xoap::MessageReference& msg)
	throw (tstore::api::exception::Exception)
	:connectionId_(""), tableName_("")
{
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName commandName("clear", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(commandName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::exception::Exception, "Invalid message: missing or bad <clear/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName tableName = envelope.createName("name", tstore::api::nested::NamespacePrefix, tstore::api::nested::NamespaceUri);
      	connectionId_ = bodyElements[0].getAttributeValue(connectionID);
	tableName_ = bodyElements[0].getAttributeValue(tableName);
}



xoap::MessageReference tstore::api::ClearNested::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPName command = envelope.createName
		("clear",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);
	element.addNamespaceDeclaration(tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addNamespaceDeclaration(tstore::api::nested::NamespacePrefix, tstore::api::nested::NamespaceUri);


	xoap::SOAPName connectionID = envelope.createName("connectionID", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(connectionID, connectionId_);
	
	xoap::SOAPName tableName = envelope.createName("table", tstore::api::nested::NamespacePrefix, tstore::api::nested::NamespaceUri);
	element.addAttribute(tableName, tableName_);
	
	return request;
}

std::string tstore::api::ClearNested::getConnectionId()
{
	return connectionId_;
}

std::string tstore::api::ClearNested::getTableName()
{
	return tableName_;
}

