// $Id: version.cc,v 1.1 2007/08/02 13:11:53 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/api/version.h"
#include "config/version.h"
#include "xdata/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(tstoreapi)

void tstoreapi::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xdata);  
}

std::set<std::string, std::less<std::string> > tstoreapi::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xdata);

	return dependencies;
}	
	
