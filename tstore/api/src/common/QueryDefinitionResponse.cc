// $Id: QueryDefinitionResponse.cc,v 1.2 2007/08/15 14:45:33 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/api/QueryDefinitionResponse.h"
#include "tstore/api/NS.h"
#include "xoap/MessageFactory.h"

tstore::api::QueryDefinitionResponse::QueryDefinitionResponse(xdata::Table::Reference& table)
	: table_(table)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/definitionResponse");
	// empty configuratin in this case	
}

tstore::api::QueryDefinitionResponse::~QueryDefinitionResponse()
{
}
	
tstore::api::QueryDefinitionResponse::QueryDefinitionResponse(xoap::MessageReference& msg) 
	throw (tstore::api::exception::Exception)
{	
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName QueryDefinitionResponseName("definitionResponse", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(QueryDefinitionResponseName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::exception::Exception, "Invalid message: missing or bad <QueryDefinitionResponse/> element" );
	}
	
	std::list<xoap::AttachmentPart*> attachments = msg->getAttachments();
	std::list<xoap::AttachmentPart*>::iterator j;
	for ( j = attachments.begin(); j != attachments.end(); j++ )
	{
		if ((*j)->getContentType() == "application/xdata+table")
		{
			xdata::exdr::FixedSizeInputStreamBuffer inBuffer((*j)->getContent(),(*j)->getSize());
			std::string contentEncoding = (*j)->getContentEncoding();
			std::string contentId = (*j)->getContentId();
			xdata::Table * t = new xdata::Table();
			try 
			{
				xdata::exdr::Serializer serializer;
				serializer.import(t, &inBuffer );
				xdata::Table::Reference tmp(t);
				table_ = tmp;
			}
			catch(xdata::exception::Exception & e )
			{
				// failed to import table
			}
		}
		else
		{
			// unknown attachment type
			std::stringstream msg;
			msg << "Unknown attachment type '" << (*j)->getContentType() << "' in <insert> message";
			XCEPT_RAISE (tstore::api::exception::Exception, msg.str() );
		}
	}
}

xoap::MessageReference tstore::api::QueryDefinitionResponse::toSOAP()
{
	xoap::MessageReference response = xoap::createMessage();
	xoap::SOAPPart soap = response->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(response);

	xoap::SOAPName command = envelope.createName
		("definitionResponse",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	responseBody.addBodyElement(command);
	
	xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
	xdata::exdr::Serializer serializer;
	serializer.exportAll( &(*table_), &outBuffer );
	xoap::AttachmentPart* attachment = response->createAttachmentPart(outBuffer.getBuffer(), outBuffer.tellp(), "application/xdata+table");
	attachment->setContentEncoding("exdr");
	
	// Content-ID currently not used, so set a static text
	attachment->setContentId("table");
	response->addAttachmentPart(attachment);
	
	return response;
}
	
xdata::Table::Reference tstore::api::QueryDefinitionResponse::getTable()
{
	return table_;
}
