// $Id: NS.h,v 1.5 2007/10/12 15:35:00 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_NS_h_
#define _tstore_api_NS_h_

#include <string>

namespace tstore
{
	namespace api
	{
		
		const std::string NamespacePrefix = "ts";
		const std::string NamespaceUri = "http://xdaq.web.cern.ch/xdaq/xsd/2006/tstore-10.xsd";

		namespace admin 
                {
                        const std::string NamespacePrefix = "admin";
                        const std::string NamespaceUri = "urn:xdaq-tstore:1.0";
                }

		namespace sql
		{
			const std::string NamespacePrefix = "sql";
			const std::string NamespaceUri = "urn:tstore-view-SQL";
		}
		
		namespace nested
		{
			const std::string NamespacePrefix = "nested";
			const std::string NamespaceUri = "urn:tstore-view-Nested";
		}
	}
}
#endif

