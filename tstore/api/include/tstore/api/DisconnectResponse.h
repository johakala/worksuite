// $Id: DisconnectResponse.h,v 1.3 2007/08/09 09:24:48 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_DisconnectResponse_h_
#define _tstore_api_DisconnectResponse_h_

#include <string>
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/exception/Exception.h"
#include "tstore/api/Request.h"

namespace tstore
{
namespace api
{
	class DisconnectResponse: public tstore::api::Request
	{
		public:
		
		DisconnectResponse();
		
		DisconnectResponse(xoap::MessageReference& msg) throw (tstore::api::exception::Exception);
        
		virtual ~DisconnectResponse();
				
		xoap::MessageReference toSOAP();		
	};
}}

#endif
