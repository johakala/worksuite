// $Id: QueryDefinition.h,v 1.1 2007/08/10 09:14:04 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_QueryDefinition_h_
#define _tstore_api_QueryDefinition_h_

#include <string>
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/exception/Exception.h"
#include "tstore/api/Request.h"
#include "xdata/Table.h"

namespace tstore
{
namespace api
{
	/*!
		Use this request message to retrieve the table definition for the
		tables used in an insert statement identified by \param insertStatementName.
		The response contains the xdata::Table objects used in the insert statement
		as attachements to the message. The tables returned are empty.
	*/
	class QueryDefinition: public tstore::api::Request
	{
		public:
		
		QueryDefinition
		(
			const std::string& connectionId,
			const std::string& insertStatementName
		);
		
		QueryDefinition
		(
			xoap::MessageReference& msg
		) 
		throw (tstore::api::exception::Exception);
        
		virtual ~QueryDefinition();
				
		xoap::MessageReference toSOAP();

		std::string getConnectionId();
		
		std::string getQueryDefinitionStatementName();

		private:

		std::string connectionId_;
		std::string insertStatementName_;
	};
}}

#endif
