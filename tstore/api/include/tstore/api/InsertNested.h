// $Id: InsertNested.h,v 1.2 2007/10/17 09:49:05 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_InsertNested_h_
#define _tstore_api_InsertNested_h_

#include <string>
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/exception/Exception.h"
#include "tstore/api/Request.h"
#include "xdata/Table.h"

namespace tstore
{
namespace api
{
	class InsertNested: public tstore::api::Request
	{
		public:
		
		InsertNested
		(
			const std::string& connectionId,
			const std::string& tableName,
			xdata::Table::Reference& table,
		 	size_t depth	
		);
		
		InsertNested(xoap::MessageReference& msg) throw (tstore::api::exception::Exception);
        
		virtual ~InsertNested();
				
		xoap::MessageReference toSOAP();

		xdata::Table::Reference getTable();

		std::string getConnectionId();
		
		std::string getTableName();

		private:

		std::string connectionId_;
		std::string tableName_;
		xdata::Table::Reference table_;
		size_t depth_;
	};
}}

#endif
