// $Id: InsertResponse.h,v 1.1 2007/08/09 09:24:29 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_InsertResponse_h_
#define _tstore_api_InsertResponse_h_

#include <string>
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/exception/Exception.h"
#include "tstore/api/Request.h"

namespace tstore
{
namespace api
{
	class InsertResponse: public tstore::api::Request
	{
		public:
		
		InsertResponse();
		
		InsertResponse(xoap::MessageReference& msg) throw (tstore::api::exception::Exception);
        
		virtual ~InsertResponse();
				
		xoap::MessageReference toSOAP();		
	};
}}

#endif
