// $Id: AddViewResponse.h,v 1.2 2007/08/08 13:03:17 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_admin_AddViewResponse_h_
#define _tstore_api_admin_AddViewResponse_h_

#include <string>
#include "toolbox/net/exception/Exception.h"
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/admin/exception/Exception.h"
#include "tstore/api/Request.h"
#include "tstore/api/admin/Configuration.h"

namespace tstore
{
namespace api
{
namespace admin
{
	class AddViewResponse: public tstore::api::Request
	{
		public:
		
		AddViewResponse();
		
		AddViewResponse(xoap::MessageReference& msg) throw (tstore::api::admin::exception::Exception);
        
		virtual ~AddViewResponse();
				
		xoap::MessageReference toSOAP();
	};
}}}

#endif
