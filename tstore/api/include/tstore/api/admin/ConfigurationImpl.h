// $Id: ConfigurationImpl.h,v 1.8 2007/10/18 12:40:36 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_admin_ConfigurationImpl_h_
#define _tstore_api_admin_ConfigurationImpl_h_

#include <string>
#include "tstore/api/admin/exception/Exception.h"
#include <xercesc/dom/DOM.hpp>
#include "xdata/Table.h"
#include "tstore/api/admin/Configuration.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"

XERCES_CPP_NAMESPACE_USE

namespace tstore
{
namespace api
{
namespace admin
{
	class ConfigurationImpl: public Configuration
	{
		public:
		
		ConfigurationImpl() throw (tstore::api::admin::exception::Exception);
		
		//ConfigurationImpl(DOMDocument * document) throw (tstore::api::admin::exception::Exception);
        
		virtual ~ConfigurationImpl(); 
		
		xdata::Table::Reference getTableDefinition(const std::string & name) throw (tstore::api::admin::exception::Exception);

		
		void addTableDefinition(const std::string & name, const std::string key, xdata::Table::Reference table ) 
			throw (tstore::api::admin::exception::Exception);
		
		void removeTableDefinition(const std::string & name ) 
			throw (tstore::api::admin::exception::Exception);
		
		bool hasTableDefinition(const std::string & name );
			
		void addInsertStatement
		(
			const std::string & name, 
			const std::string & tableName, 
			std::vector<std::string> & columns 
		) 
		throw (tstore::api::admin::exception::Exception);
		
		void removeInsertStatement(const std::string & name ) 
			throw (tstore::api::admin::exception::Exception);
			
			
		ConfigurationImpl& import (ConfigurationImpl & configuration);
		
		void toSOAP(xoap::SOAPElement& element, xoap::SOAPEnvelope& envelope);
		
		DOMDocument * getDocument();	
		private:
		
		DOMDocument * document_;
	};
}}}

#endif
