/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_admin_Configuration_h_
#define _tstore_api_admin_Configuration_h_

#include <string>
#include "tstore/api/admin/exception/Exception.h"
#include "xdata/Table.h"
#include <xercesc/dom/DOM.hpp>
XERCES_CPP_NAMESPACE_USE


namespace tstore
{
namespace api
{
namespace admin
{
	class Configuration
	{
		public:
		
		virtual ~Configuration() {}; 
		
		virtual xdata::Table::Reference getTableDefinition(const std::string & name) 
			throw (tstore::api::admin::exception::Exception) = 0 ;
		
	
		/*! Create a simple insert statement from the table provided. The statement will insert all columns
		    of the given table.
                 */
		virtual void addInsertStatement
		(
			const std::string & name, 
			const std::string & tableName, 
			std::vector<std::string> & columns
		) 
		throw (tstore::api::admin::exception::Exception) = 0;	
		
		virtual void removeTableDefinition(const std::string & name ) 
			throw (tstore::api::admin::exception::Exception) = 0;
			
		virtual void removeInsertStatement(const std::string & name ) 
			throw (tstore::api::admin::exception::Exception) = 0;
			
		virtual bool hasTableDefinition(const std::string & name ) = 0;
	
			
		virtual  DOMDocument * getDocument() = 0;	
	
	};
}}}

#endif
