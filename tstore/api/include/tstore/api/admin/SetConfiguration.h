// $Id: SetConfiguration.h,v 1.2 2007/08/08 13:03:17 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_admin_SetConfiguration_h_
#define _tstore_api_admin_SetConfiguration_h_

#include <string>
#include "toolbox/net/exception/Exception.h"
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/admin/exception/Exception.h"
#include "tstore/api/admin/ConfigurationImpl.h"
#include "tstore/api/Request.h"

namespace tstore
{
namespace api
{
namespace admin
{
	class SetConfiguration: public tstore::api::Request
	{
		public:
		
		SetConfiguration(const std::string & viewId, const std::string& viewType, const std::string & xpath);
		
		SetConfiguration(xoap::MessageReference& msg) throw (tstore::api::admin::exception::Exception);
        
		virtual ~SetConfiguration();
		
		ws::addressing::Headers & getAddressingHeaders();		
		
		xoap::MessageReference toSOAP();

		tstore::api::admin::Configuration & getConfiguration();	
		
		tstore::api::admin::Configuration & setConfiguration(tstore::api::admin::Configuration & configuration);	
				
		std::string getViewId();
		std::string getViewType();
		std::string getXPath();
				
		private:
		
		std::string  viewId_;
		std::string  viewType_;
		std::string  xpath_;
		tstore::api::admin::ConfigurationImpl configuration_;
	};
}}}

#endif
