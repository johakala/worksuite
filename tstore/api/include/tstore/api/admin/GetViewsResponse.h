// $Id: GetViewsResponse.h,v 1.3 2007/10/12 15:34:59 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_admin_GetViewsResponse_h_
#define _tstore_api_admin_GetViewsResponse_h_

#include <string>
#include <list>
#include "toolbox/net/exception/Exception.h"
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/admin/exception/Exception.h"
#include "tstore/api/Request.h"
#include "tstore/api/admin/Configuration.h"

namespace tstore
{
namespace api
{
namespace admin
{
	class GetViewsResponse: public tstore::api::Request
	{
		public:
		
		GetViewsResponse(std::list<std::string>& sqlviews, std::list<std::string>& nestedviews);
		
		GetViewsResponse(xoap::MessageReference& msg) throw (tstore::api::admin::exception::Exception);
        
		virtual ~GetViewsResponse();
				
		xoap::MessageReference toSOAP();
		
		std::list<std::string>& getSQLViews();
		std::list<std::string>& getNestedViews();
		
		
		private:
		
		std::list<std::string> sqlviews_;
		std::list<std::string> nestedviews_;
	};
}}}

#endif
