// $Id: Exception.h,v 1.1 2007/08/08 13:46:28 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_exception_Exception_h_
#define _tstore_api_exception_Exception_h_

#include "tstore/exception/Exception.h"


namespace tstore {
	namespace api {
			namespace exception { 
				class Exception: public tstore::exception::Exception 
				{
					public: 
					Exception( std::string name, std::string message, std::string module, int line, std::string function ): 
						tstore::exception::Exception(name, message, module, line, function) 
					{} 

					Exception( std::string name, std::string message, std::string module, int line, std::string function,
						xcept::Exception& e ): 
						tstore::exception::Exception(name, message, module, line, function, e) 
					{} 

				};  
			}
	}		
}

#endif
