// $Id: OracleConnection.cc,v 1.14 2009/05/11 15:51:33 brett Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "tstore/OracleConnection.h" 
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/Float.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Vector.h"
#include "xdata/exdr/Serializer.h"
#include <xdata/Mime.h>
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"

#include "toolbox/string.h"
#include <occiControl.h>
#include <occiCommon.h>
#include <sstream>
#include <list>
#include <vector>
#include <float.h>
#include <iomanip>
#include <list>

int tstore::OracleConnection::connectionCount_=0;

void tstore::OracleConnection::init() throw (tstore::exception::Exception) {
	if (connectionCount_==0 || environment_==NULL) {
		try {
			//using multiple Environments is inefficient, according to the docs, so we'll just keep using the same one.
			environment_ = oracle::occi::Environment::createEnvironment (
				oracle::occi::Environment::OBJECT
				//this seems to work but is not documented anywhere
				//static_cast<oracle::occi::Environment::Mode>(oracle::occi::Environment::THREADED_MUTEXED | oracle::occi::Environment::OBJECT)
			);
		} catch (oracle::occi::SQLException &e) {
			XCEPT_RAISE(tstore::exception::Exception, std::string("Could not create environment. ")+e.what());
		}
	}

	connectionCount_++;
/*
	connection_=NULL;
	expectedRowCount_=100;
*/
}

tstore::OracleConnection::OracleConnection() throw (tstore::exception::Exception) {
	environment_ = NULL;
	connection_ = NULL;
	expectedRowCount_ = 100;
	commitNeeded = false;
}

tstore::OracleConnection::OracleConnection(std::string db,std::string user, std::string passwd) throw (tstore::exception::Exception) : Connection(user,passwd,db) {
	environment_ = NULL;
	connection_ = NULL;
	expectedRowCount_ = 100;
	commitNeeded = false;
}

tstore::OracleConnection* tstore::OracleConnection::create(std::string db, std::string user, std::string passwd) {
	OracleConnection* obj = new OracleConnection(db, user, passwd);
	obj->init();

	return obj;
}

bool tstore::OracleConnection::isConnected() {
	return connection_!=NULL;
}

void tstore::OracleConnection::checkNotEmpty(const std::string &key)  throw (tstore::exception::Exception) {
	if (getProperty(key).empty()) {
		XCEPT_RAISE(tstore::exception::Exception, "Could not connect to database because no "+key+" was supplied.");
	}
}

//opens a connection with the current credentials, throws an exception if a connection cannot be opened.
void tstore::OracleConnection::openConnection()  throw (tstore::exception::Exception) {
	if (!isConnected()) {
		try {
			checkNotEmpty(databaseKey);
			checkNotEmpty(userKey);
			checkNotEmpty(passwordKey);

			connection_ = environment_->createConnection (getProperty(userKey), getProperty(passwordKey), getProperty(databaseKey));
			if (connection_==NULL) {
				XCEPT_RAISE(tstore::exception::Exception, "Could not connect to database "+getProperty(databaseKey)+".");
			}
			connection_->setStmtCacheSize(10);
			//1590056
			//xdata::TimeVals are always in UTC
			//previously we just adjusted for timezone on reading, and specified the timezone explicitly when writing
			//however, this does not work for comparing dates e.g. in a select with a date in the where clause.
			/*When you compare date and timestamp values, Oracle converts the data to the more precise datatype 
			before doing the comparison. For example, if you compare data of TIMESTAMP WITH TIME ZONE datatype 
			with data of TIMESTAMP datatype, Oracle converts the TIMESTAMP data to TIMESTAMP WITH TIME ZONE, 
			using the session time zone.*/
			//so we set the session timezone to UTC.
			SQLStatement statement("ALTER SESSION SET TIME_ZONE = '0:0'");
			execute(statement);
		} catch (oracle::occi::SQLException &e) {
			//I really should make a new exception class or classes for this...
			//simply calling std::string(e.getMessage()) causes
			//*** glibc detected *** free(): invalid pointer: 0x08a2e990 ***
			//XCEPT_RAISE(tstore::exception::Exception, "Could not connect to database "+getProperty(databaseKey)+". Oracle returned error:"+std::string(e.getMessage()));
	
			XCEPT_RAISE(tstore::exception::Exception, "Could not connect to database "+getProperty(databaseKey)+". Oracle returned error: " + std::string(e.what()));
		} catch (xcept::Exception &e) {
			XCEPT_RETHROW(tstore::exception::Exception, "Could not connect to database "+e.message(),e);
		}
	}
}

void tstore::OracleConnection::closeConnection() {
	if (isConnected()) {
		if (connection_) {
			environment_->terminateConnection (connection_);
			connection_=NULL;
		}
	}
}

void tstore::OracleConnection::terminateEnvironment() {
    //if (--connectionCount_==0) {
	if (--connectionCount_==0 && environment_) {
		oracle::occi::Environment::terminateEnvironment (environment_);
		environment_=NULL;
	}
}

tstore::OracleConnection::~OracleConnection() {
	closeConnection();
    terminateEnvironment();
}

std::string tstore::OracleConnection::quoteSchemaObjectName(std::string name) {
	//previously used toolbox::toupper here but column names in xdata::Tables are case sensitive, so they should be in the database as well.
	//return "\""+name+"\"";
	return toolbox::toupper(name);
}

template<class T>
std::string integerCheckConstraint(const std::string &columnName) {
	std::ostringstream intType;
	//make sure the number will always fit into the type. max()+1 is reserved for storing xdata infinity values.
	//must add 1 in the SQL since adding it here will overflow
	intType << columnName << ">=" << std::numeric_limits<T>::min() << " AND " << columnName << "<=1+" << std::numeric_limits<T>::max();
	//std::cout << toolbox::toupper(intType.str()) << std::endl;
	return toolbox::toupper(intType.str());
}

template<class T>
std::string integerColumnType(const std::string &columnName) {
	std::ostringstream intType;
	unsigned int digits=(unsigned int)ceil(log10(std::numeric_limits<T>::max())); //std::numeric_limits<T>::digits10  seems to give one less than it should
	intType << "NUMBER(" << digits << ") CHECK (" << integerCheckConstraint<T>(columnName)+")";
	return intType.str();
}

std::string tstore::OracleConnection::notNegativeInfinityConstraint(const std::string &type,const std::string &columnName) {
	return toolbox::toupper(columnName+"!=-BINARY_"+type+"_INFINITY");
}

std::string tstore::OracleConnection::booleanConstraint(const std::string &columnName) {
	return toolbox::toupper(columnName+"=1 OR "+columnName+"=0");
}

std::string tstore::OracleConnection::binaryColumnType(const std::string &xdataType,const std::string &columnName) {
	//xdata float types do not distinguish between infinity and -infinity, so do not allow the columns to store -infinity
	return toolbox::toupper("BINARY_"+xdataType+" CHECK ("+notNegativeInfinityConstraint(xdataType,columnName)+")");
}

//returns whether the two types have exactly the same column types/check constraints in Oracle
//for example an unsigned int can be the same as an unsigned int 32 on a 32 bit system
bool tstore::OracleConnection::typesAreIdentical(const std::string &type1,const std::string &type2) throw () {
	std::string dummyColumnName="DUMMY"; //it doesn't matter what column name we use, as long as it's the same for both of them
	std::string oracleType1 = columnTypeFromXdataType(type1, dummyColumnName);
	std::string oracleType2 = columnTypeFromXdataType(type2, dummyColumnName);

	try {
		// if it's BLOBs, compare mappings assigned to those columns
		return (oracleType1 == oracleType2 && oracleType1 == "BLOB") ? type1 == type2 : oracleType1 == oracleType2;
	} catch (...) {
		return false;
	}
}

//columns which are guaranteed to hold all the values that can be stored in an xdata type, and nothing more.
//(with the exception of string columns, which only hold a maxium of 1000 characters. I had to decide on an arbitrary length.)
std::string tstore::OracleConnection::columnTypeFromXdataType(const std::string &xdataType,const std::string &columnName) throw (tstore::exception::Exception) {
	if (xdataType=="float" || xdataType=="double") {
		return binaryColumnType(toolbox::toupper(xdataType),columnName);
	} else if (xdataType=="int") {
		return integerColumnType<xdata::Integer>(columnName);
	} else if (xdataType=="unsigned long") {
		return integerColumnType<xdata::UnsignedLong>(columnName);
	} else if (xdataType=="unsigned int") {
		return integerColumnType<xdata::UnsignedInteger>(columnName);
	} else if (xdataType=="unsigned int 32") {
		return integerColumnType<xdata::UnsignedInteger32>(columnName);
	} else if (xdataType=="unsigned int 64") {
		return integerColumnType<xdata::UnsignedInteger64>(columnName);
	} else if (xdataType=="unsigned short") {
		return integerColumnType<xdata::UnsignedShort>(columnName);
	}
	else if (xdataType=="bool") return "NUMBER(1) CHECK ("+booleanConstraint(columnName)+")";
	else if (xdataType=="string") return "VARCHAR2(1000)";
	else if (xdataType=="time") return "TIMESTAMP";
	else if (xdataType=="mime" || toolbox::startsWith(xdataType, "vector ")) return "BLOB";
	else {
		XCEPT_RAISE(tstore::exception::Exception, "No Oracle column type defined for "+xdataType);
	}
}

std::string tstore::OracleConnection::xdataTypeMatchingCheckConstraint(const std::string &constraint,const std::string &columnName) {
	std::string upperConstraint(toolbox::toupper(constraint));
	//currently the same constraints could match unsigned 64/32 and unsigned long/int. We should use the more explicit versions,
	//so these types come first.
	if (upperConstraint==integerCheckConstraint<xdata::UnsignedInteger32>(columnName)) {
		return "unsigned int 32";
	} else if (upperConstraint==integerCheckConstraint<xdata::UnsignedInteger64>(columnName)) {
		return "unsigned int 64"; 
	} else if (constraint==integerCheckConstraint<xdata::Integer>(columnName)) {
		return "int";
	} else if (upperConstraint==integerCheckConstraint<xdata::UnsignedLong>(columnName)) {
		return "unsigned long";
	} else if (upperConstraint==integerCheckConstraint<xdata::UnsignedInteger>(columnName)) {
		return "unsigned int";
	} else if (upperConstraint==integerCheckConstraint<xdata::UnsignedShort>(columnName)) {
		return "unsigned short";
	} else if (upperConstraint==notNegativeInfinityConstraint("FLOAT",columnName)) {
		return "float";
	} else if (upperConstraint==notNegativeInfinityConstraint("DOUBLE",columnName)) {
		return "double";
	} else if (upperConstraint==booleanConstraint(columnName)) {
		return "bool";
	}
	return "";
}

//for testing
//static long int statementsOpen=0;

//creates an oracle::occi::Statement from the string passed in.
//either returns a valid Statement pointer, or throws an exception.
//you must terminate the statement when you're finished with it.
oracle::occi::Statement *tstore::OracleConnection::createStatement(const std::string &statement) throw (tstore::exception::Exception) {
	try {
		//statementsOpen++;
		//std::cout << "creating statement " << statementsOpen << std::endl;
		if (!statement.empty()) return connection()->createStatement(statement);
		return connection()->createStatement();
	} catch (oracle::occi::SQLException &e) {
		//XCEPT_RAISE(tstore::exception::Exception,"Could not create statement. Oracle returned error: "+e.getMessage());
		XCEPT_RAISE(tstore::exception::Exception,"Could not create statement. Oracle returned error: " + std::string(e.what()));
	}
}

//call this for any statement created with createStatement()
void tstore::OracleConnection::terminateStatement(oracle::occi::Statement *statement) throw (tstore::exception::Exception) {
	//statementsOpen--;
	//std::cout << "terminating statement " << statementsOpen << std::endl;
	try {
		if (statement) connection()->terminateStatement(statement);
	} catch (oracle::occi::SQLException &e) {
		//XCEPT_RAISE(tstore::exception::Exception,"Could not terminate statement. Oracle returned error: "+e.getMessage());
		XCEPT_RAISE(tstore::exception::Exception,"Could not terminate statement. Oracle returned error: " + std::string(e.what()));
	}

}

void printTableDefinition(xdata::Table &definition) {
	std::vector<std::string> columns=definition.getColumns();
	std::vector<std::string>::iterator column;
	std::cout << columns.size() << " columns" << std::endl;
	for(column=columns.begin(); column!=columns.end(); ++column) {
		std::cout << *column << " (" << definition.getColumnType(*column) << ")" << std::endl;
	}
}

//for debugging only... compares the intended table definition with what can be deduced by looking at the database (what would be recreated
//if the table were removed from the config and then added back during a sync.)
void tstore::OracleConnection::checkTableDefinition(std::string name,xdata::Table &definition) throw () {
	xdata::Table readDefinition;
	try {
		getTableDefinition(name,readDefinition);		
		std::vector<std::string> columns=definition.getColumns();
		std::vector<std::string>::iterator column;

		for(column=columns.begin(); column!=columns.end(); ++column) {
			//std::cout << *column << std::endl;
			if (!typesAreIdentical(readDefinition.getColumnType(toolbox::toupper(*column)),definition.getColumnType(*column))) {
				std::cout << "definition for the table "+name << std::endl;
				std::cout << "intended definition:" << std::endl;
				printTableDefinition(definition);
				std::cout << "read definition:" << std::endl;
				printTableDefinition(readDefinition);
				std::string error="Column types for column "+*column+" are not identical. Column of type "+definition.getColumnType(*column)+" is read as "+readDefinition.getColumnType(*column);
				XCEPT_RAISE(tstore::exception::Exception,error);
			}
		}
	} catch (std::exception &e) {
		std::cout << "encountered exception while checking definition: " << e.what() << std::endl;
	}
}

void tstore::OracleConnection::createTable(std::string name,std::string primaryKey,xdata::Table &definition) throw (tstore::exception::Exception) {
	std::string SQL("create table "+quoteSchemaObjectName(name)+" (");
	std::vector<std::string> columns=definition.getColumns();
	std::vector<std::string>::iterator columnIterator;
	if (columns.empty()) {
		XCEPT_RAISE(tstore::exception::Exception, "The table has no columns.");
	}
	for(columnIterator=columns.begin(); columnIterator!=columns.end(); columnIterator++) {
		std::string xdataType = definition.getColumnType(*columnIterator);
		std::string oracleType = columnTypeFromXdataType(xdataType, *columnIterator);

		if (oracleType == "BLOB") {
			storeBlobToXdataMapping(name, *columnIterator, xdataType);
		}

		if (columnIterator!=columns.begin()) SQL += ",";
		SQL += quoteSchemaObjectName(*columnIterator) + " " + oracleType;
	}
	if (!primaryKey.empty()) {
		SQL+=",primary key ("+primaryKey+")";
	}
	SQL+=")";
	//std::cout << SQL << std::endl;
	oracle::occi::Statement *statement = createStatement();
	try {
		statement->executeUpdate(SQL);
		terminateStatement(statement);
		//connection_->flushCache();
	} catch (oracle::occi::SQLException &e) {
		terminateStatement(statement);
		//XCEPT_RAISE(tstore::exception::Exception, "Could not create table, using statement:"+SQL+". Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not create table, using statement:"+SQL+". Oracle returned error: "+std::string(e.what()));
	}
	//this is only for testing, to check that the columns would be read as the correct type.
	checkTableDefinition(name,definition);
}

//this is intended to be used for tables created with createTable, with no foreign key constraints etc.
//maybe it should also delete backup tables (though I have not yet decided whether backup tables should be managed by the connection or
//by TStore.)
void tstore::OracleConnection::dropTable(std::string name) throw (tstore::exception::Exception) {
	std::string SQL("drop table "+quoteSchemaObjectName(name));
	oracle::occi::Statement *statement = createStatement();
	try {
		statement->executeUpdate(SQL);
		terminateStatement(statement);

		try {
			removeBlobToXdataMapping(name);
		} catch(tstore::exception::Exception& e) {
			std::cout << std::string(e.what()) << "\n";
		}
	} catch (oracle::occi::SQLException &e) {
		terminateStatement(statement);
		//std::cout << std::string(e.getMessage()) << std::endl;
		//XCEPT_RAISE(tstore::exception::Exception, "Could not drop table. Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not drop table. Oracle returned error: "+std::string(e.what()));
	}
}

std::map<const int,std::string> tstore::OracleConnection::oracleToXdata;

void tstore::OracleConnection::fillTypeMappingTable() {
	if (oracleToXdata.empty()) {
		int floatTypes[]={
			/*OCCI_SQLT_BFLOAT,*/
			oracle::occi::OCCIBFLOAT,
			oracle::occi::OCCIIBFLOAT
		};
		int doubleTypes[]={
			oracle::occi::OCCIDOUBLE,
			oracle::occi::OCCIFLOAT,
			oracle::occi::OCCIIBDOUBLE/*,
			OCCI_SQLT_BFLOAT*/
		};
		int intTypes[]={
			oracle::occi::OCCIUNSIGNED_INT,
			oracle::occi::OCCIINT
		};
		int boolTypes[]={oracle::occi::OCCIBOOL};
		int timeTypes[]={
			oracle::occi::OCCI_SQLT_TIMESTAMP,
			oracle::occi::OCCI_SQLT_TIMESTAMP_TZ,
			oracle::occi::OCCI_SQLT_TIMESTAMP_LTZ,
			oracle::occi::OCCI_SQLT_DAT
		};
		int stringTypes[]={
			//these ones are converted to strings
			oracle::occi::OCCI_SQLT_RDD,
			oracle::occi::OCCI_SQLT_INTERVAL_DS,
			oracle::occi::OCCI_SQLT_INTERVAL_YM,
			oracle::occi::OCCI_SQLT_CLOB,
			oracle::occi::OCCI_SQLT_LBI,
			//these ones actually are strings
			oracle::occi::OCCICHAR,
			oracle::occi::OCCI_SQLT_LVC,
			oracle::occi::OCCI_SQLT_STR,
			oracle::occi::OCCI_SQLT_CHR,
			oracle::occi::OCCI_SQLT_VCS,
			oracle::occi::OCCI_SQLT_AFC
		};
		int mimeTypes[]={
			oracle::occi::OCCI_SQLT_BLOB
		};

		int unsignedTypes[]={oracle::occi::OCCIUNSIGNED_INT};
		unsigned int typeIndex;
	 	for (typeIndex=0;typeIndex<sizeof(floatTypes)/sizeof(int);typeIndex++) oracleToXdata[floatTypes[typeIndex]]="float";
		for (typeIndex=0;typeIndex<sizeof(doubleTypes)/sizeof(int);typeIndex++) oracleToXdata[doubleTypes[typeIndex]]="double"; 	
		for (typeIndex=0;typeIndex<sizeof(intTypes)/sizeof(int);typeIndex++) oracleToXdata[intTypes[typeIndex]]="int";
		for (typeIndex=0;typeIndex<sizeof(boolTypes)/sizeof(int);typeIndex++) oracleToXdata[boolTypes[typeIndex]]="bool";	 	
		for (typeIndex=0;typeIndex<sizeof(stringTypes)/sizeof(int);typeIndex++) oracleToXdata[stringTypes[typeIndex]]="string";
		for (typeIndex=0;typeIndex<sizeof(unsignedTypes)/sizeof(int);typeIndex++) oracleToXdata[unsignedTypes[typeIndex]]="unsigned long";
		for (typeIndex=0;typeIndex<sizeof(timeTypes)/sizeof(int);typeIndex++) oracleToXdata[timeTypes[typeIndex]]="time";
		for (typeIndex=0;typeIndex<sizeof(mimeTypes)/sizeof(int);typeIndex++) oracleToXdata[mimeTypes[typeIndex]]="mime";
 	}
}

bool tstore::OracleConnection::isColumnNumeric(const oracle::occi::MetaData &metaData) {
	fillTypeMappingTable();
	int oracleType=metaData.getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);
 	if (oracleType==oracle::occi::OCCI_SQLT_NUM || oracleType==oracle::occi::OCCI_SQLT_LNG) return true;
 	if (oracleToXdata.count(oracleType)) {
		return oracleToXdata[oracleType]!="string" && oracleToXdata[oracleType]!="time" && oracleToXdata[oracleType]!="mime";
	}
	return false;
}

std::string tstore::OracleConnection::defaultXdataTypeForVArray(const oracle::occi::MetaData &metaData) {
	std::string typeName=metaData.getString(oracle::occi::MetaData::ATTR_TYPE_NAME);
	//std::cout << "typeName=" << typeName << std::endl;
	const oracle::occi::MetaData typeMetaData=connection()->getMetaData(typeName);
	int arrayType=typeMetaData.getInt(oracle::occi::MetaData::ATTR_COLLECTION_TYPECODE);
	if (arrayType==oracle::occi::OCCI_TYPECODE_VARRAY) {
		const oracle::occi::MetaData elementMetaData=typeMetaData.getMetaData(oracle::occi::MetaData::ATTR_COLLECTION_ELEMENT);
		int elementType=elementMetaData.getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);
		//std::cout << "data type of element=" << elementType << std::endl;
		if (oracleToXdata.count(elementType)) {
			return "vector "+oracleToXdata[elementType];
	 	}
		return "vector string";
	} else {
		//it's a nested table rather than a varray
		//in my testing nested tables gave a different value for the data type attribute of the column,
		//so we shouldn't get to this code. But this does not seem to be documented. I have not decided
		//whether I want these functions to throw exceptions or wait until there is a problem reading
		//a value. For now let's just try to read the values
		//into a string as is done with other unknown types.
		return "string";
	}
}

std::string tstore::OracleConnection::defaultXdataTypeForColumn(const oracle::occi::MetaData &metaData) {
	fillTypeMappingTable();
 	int oracleType=metaData.getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);
 	if (oracleToXdata.count(oracleType)) {
		return oracleToXdata[oracleType];
 	} else if (oracleType==oracle::occi::OCCI_SQLT_NUM || oracleType==oracle::occi::OCCI_SQLT_LNG) {
 		unsigned int precision=metaData.getInt(oracle::occi::MetaData::ATTR_PRECISION);
 		unsigned int scale=metaData.getInt(oracle::occi::MetaData::ATTR_SCALE);
 		if (scale==0) { //if it's an integer
 			if (precision<std::log10((long double)INT_MAX)) return "int";
			return "string"; 
 		} else {
 			if (precision<=FLT_DIG) return "float";
 			if (precision<=DBL_DIG) return "double";
 			return "string";
 		}
 		//vector types
 	} else if (oracleType==oracle::occi::OCCI_SQLT_NCO+1 || oracleType==oracle::occi::OCCI_SQLT_NTY) { 
 			//for some reason, the ATTR_DATA_TYPE value for a varray column in a resultset is 123, 
 			//which equals oracle::occi::OCCI_SQLT_NCO+1,
 			//but I can't find a constant defined to that.
 			//OCCI_SQLT_NCO is a nested table, which we don't support
 			//however, for a varray column in the column metadata, the ATTR_DATA_TYPE is OCCI_SQLT_NTY
 			std::string vecType=defaultXdataTypeForVArray(metaData);
 			return vecType;
 	}
	//maybe this should be an exception, but for now I know there are some missing, I don't know what to map them to.
 	std::cout << "No mapping for Oracle type " << oracleType << std::endl;
 	return "string";
}

std::string tstore::OracleConnection::mappingTypes[]= {
	"CHAR",
	"CHARACTER",
	"CHAR VARYING",
	"CHARACTER VARYING",
	"INTEGER",
	"INT",
	"SMALLINT",
	"FLOAT",
	"DOUBLE PRECISION",
	"REAL",
	"NUMERIC",
	"DECIMAL",
	//the above types are ANSI SQL types and so should be valid for all connection types
	//so maybe they should be in Connection
	"TIMESTAMP",
	"TIMESTAMP WITH TIME ZONE",
	"TIMESTAMP WITH LOCAL TIME ZONE",
	"DATE"
};

bool tstore::OracleConnection::isMappingTypeValid(const std::string &mappingType) {
	if (mappingType.empty()) return true;
	for (unsigned int typeIndex=0;typeIndex<sizeof(mappingTypes)/sizeof(mappingTypes[0]);typeIndex++) {
		if (mappingType==mappingTypes[typeIndex]) return true;
	}
	return false;
}

std::vector<std::string> tstore::OracleConnection::mappingTypeList() {
	std::vector<std::string> types(mappingTypes,mappingTypes+sizeof(mappingTypes)/sizeof(mappingTypes[0]));
	return types;
}

//checks whether the column matches a given 'mapping' data type string (which should be an ANSI SQL data type.) 
//This is decided according to http://www.lc.leidenuniv.nl/awcourse/oracle/server.920/a96524/c13datyp.htm#18296
//if any new types are added, they should also be added to isMappingTypeValid (this is so that TStore can check the mappings
//and log something using the application logger, before trying to use them.)
bool tstore::OracleConnection::columnMatchesMappingDataType(const oracle::occi::MetaData &metaData,const std::string &mappingType) {
	int oracleType=metaData.getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);
	if (mappingType.empty()) return true;
	if (mappingType=="CHAR" || mappingType=="CHARACTER") {
		return (oracleType==oracle::occi::OCCI_SQLT_AFC);
	} if (mappingType=="CHAR VARYING" || mappingType=="CHARACTER VARYING") {
		return (oracleType==oracle::occi::OCCICHAR || 
				oracleType==oracle::occi::OCCI_SQLT_LVC || 
				oracleType==oracle::occi::OCCI_SQLT_STR || 
				oracleType==oracle::occi::OCCI_SQLT_CHR ||
				oracleType==oracle::occi::OCCI_SQLT_VCS);
	} 
	if (mappingType=="INTEGER" || mappingType=="INT" || mappingType=="SMALLINT") {
		unsigned int precision=metaData.getInt(oracle::occi::MetaData::ATTR_PRECISION);
 		unsigned int scale=metaData.getInt(oracle::occi::MetaData::ATTR_SCALE);
 		return scale==0 && precision==38;
	}
	if (mappingType=="FLOAT" || mappingType=="DOUBLE PRECISION" || mappingType=="REAL" || mappingType=="NUMERIC" || mappingType=="DECIMAL") {
		return oracleType==oracle::occi::OCCI_SQLT_NUM;
		//maybe should also return true if it's a binary float or binary double, but let's just stick to the ANSI->Oracle mappings for now
	} if (mappingType=="TIMESTAMP") {
		return oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP;
	} if (mappingType=="TIMESTAMP WITH TIME ZONE") {
		return oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP_TZ;
	} if (mappingType=="TIMESTAMP WITH LOCAL TIME ZONE") {
		return oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP_LTZ;
	} if (mappingType=="DATE") {
		return oracleType==oracle::occi::OCCI_SQLT_DAT;
	}
	return false;
}

bool tstore::OracleConnection::getMappingForColumn(tstore::Mapping &result,const oracle::occi::MetaData &metaData,const tstore::MappingList &mappings,const std::string &tableName) {
	std::string columnName=metaData.getString(oracle::occi::MetaData::ATTR_NAME);
	for (MappingList::const_iterator mappingIterator=mappings.begin();mappingIterator!=mappings.end();mappingIterator++) {
		if ((*mappingIterator).matchesTable(tableName)) {
			if ((*mappingIterator).matchesColumn(columnName)) {
				if (columnMatchesMappingDataType(metaData,(*mappingIterator).dbType())) {
					result=*mappingIterator;
					return true;
				}
			}
		}
	}
	return false;
}

std::string tstore::OracleConnection::xdataTypeForColumn(const oracle::occi::MetaData &metaData,const tstore::MappingList &mappings,const std::string &tableName) {
	Mapping mapping;
	if (getMappingForColumn(mapping,metaData,mappings,tableName)) {
		return mapping.xdataType();
	}
	return defaultXdataTypeForColumn(metaData);
}

std::string tstore::OracleConnection::defaultFormatForColumn(const oracle::occi::MetaData &metaData) {
	int oracleType=metaData.getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);
	if (oracleType==oracle::occi::OCCI_SQLT_DAT) {
		return "DD-Mon-YYYY";
	} else if (oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP) {
		return "DD-Mon-YYYY HH24:MI:SSXFF4";
	} else if (oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP_TZ || oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP_LTZ) {
		return "DD-Mon-YYYY HH24:MI:SSXFF4 TZH:TZM";
	} 
	return "";
}

std::string tstore::OracleConnection::formatForColumn(const oracle::occi::MetaData &metaData,const tstore::MappingList &mappings,const std::string &tableName) {
	Mapping mapping;
	if (getMappingForColumn(mapping,metaData,mappings,tableName)) {
		if (!mapping.format().empty()) return mapping.format();
	}
	return defaultFormatForColumn(metaData);
}

//copies a single column of results from a query with a single parameter into the collection referred to by \a results
template <class OutputIterator>
void tstore::OracleConnection::selectSingleColumn(OutputIterator results,const std::string &parameter,const std::string &query) throw (tstore::exception::Exception) {
	oracle::occi::Statement *statement=createStatement(query);
	if (!statement) {
		XCEPT_RAISE(tstore::exception::Exception,"Could not create statement.");
	} try {
		//std::cout << query << std::endl;
		statement->setString(1, parameter);
		oracle::occi::ResultSet *rs = statement->executeQuery();
		while (rs->next()) {
			*results++=rs->getString(1);
			//std::cout << rs->getString(1) << std::endl;
			//keys.insert(rs->getString(1));
		}
	} catch (oracle::occi::SQLException &e) {
		terminateStatement(statement);
		//XCEPT_RAISE(tstore::exception::Exception,"Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception,"Oracle returned error: "+std::string(e.what()));
	}
	terminateStatement(statement);
}

//call only when there is a connection open
//this information doesn't seem to be available in the metadata, so we'll have to get it from the data dictionary
template <class OutputIterator>
void tstore::OracleConnection::getPrimaryKeyColumnsForTable(std::string tableName,/*std::set<std::string> &keys*/OutputIterator keys) throw (tstore::exception::Exception) {
	std::string primaryKeyStatement="select column_name from all_constraints,all_cons_columns where all_constraints.constraint_type='P' and all_constraints.constraint_name=all_cons_columns.constraint_name and upper(all_constraints.table_name)=upper(:tablename) order by position";
	//The above query does not work with fully qualified table names
	std::list<std::string> tokens = toolbox::parseTokenList(tableName,".");
	if (tokens.empty()) {
	  XCEPT_RAISE(tstore::exception::Exception,"Could not tokenize table name '"+tableName+"'");
	}

	std::string name = *tokens.rbegin();
	std::cout << "name = " << name << std::endl;
	try {
		selectSingleColumn(keys,name,primaryKeyStatement);
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not read metadata for table '"+tableName+"'.",e);
	}
}

static const std::string backupVersionPattern="_\\d\\d\\d$";

unsigned short tstore::OracleConnection::getHighestVersionOfTable(const std::string &tableName) throw (tstore::exception::Exception) {
	std::string likeExpression=toolbox::toupper(tableName)+backupVersionPattern; //might have to do something here to escape characters in tableName.
	std::vector<std::string> versionNumber;
	std::insert_iterator<std::vector<std::string> > iterator(versionNumber,versionNumber.begin());
	std::string statement="select * from ("
							"select REGEXP_SUBSTR(table_name,'\\d\\d\\d$') ver from user_tables "
							"where REGEXP_LIKE(table_name,:likeExpression) order by ver desc"
						") where rownum=1";
	try {
		selectSingleColumn(iterator,likeExpression,statement);
		if (versionNumber.empty()) {
			//std::cout << "No version so far for " << tableName << std::endl;
			return 0;
		} else if (versionNumber.size()==1) {
			std::istringstream stream(versionNumber[0]);
			unsigned short versionNumberShort;
			if (stream >> versionNumberShort) {
				//std::cout << "latest version number for " << tableName << " is " << versionNumberShort << std::endl;
				return versionNumberShort;
			} else {
				XCEPT_RAISE(tstore::exception::Exception,"Version "+versionNumber[0]+" of table '"+tableName+"' is not a number");
			}
		} else {
			XCEPT_RAISE(tstore::exception::Exception,"Too many rows returned while searching for the version of '"+tableName+"'");
		}
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not read latest version number of '"+tableName+"'.",e);
	}
}

void tstore::OracleConnection::backupTable(const std::string &tableName) throw (tstore::exception::Exception) {
	unsigned short currentVersion=getHighestVersionOfTable(tableName);
	std::ostringstream newName; //Moon child! (note for confused maintainers... it's a completely useless reference to The Neverending Story)
	newName << tableName << "_" << std::setfill('0') << std::setw(3) << currentVersion+1;
	renameTable(tableName,newName.str());
	renameBlobToXdataMapping(tableName, newName.str());
	//here we could remove constraints from the backup version as it is assumed that constraints will be changed to use to the new table, and we don't want backup tables
	//showing up in a NestedView. However for ease of restoring from the backup, it is better to leave the constraints there and just 
	//filter out backup tables when looking for constraints.
	if (unsigned short newVersion=getHighestVersionOfTable(tableName)!=currentVersion+1) {
		std::ostringstream error;
		error << "Backing up table " << tableName << " as " << newName.str() << " seems to have failed. New version number is " << newVersion << ", should be " << currentVersion+1;
		//we should rename or copy the table back to its original name since if the new table is not recognised as being a backup version, it might get deleted on the next sync to database.
			try {
				renameTable(newName.str(),tableName);
				renameBlobToXdataMapping(newName.str(), tableName);
			} catch (tstore::exception::Exception &removeException) {
				error << " Additionally, encountered the following error while trying to rename the table to its original name:" << removeException.what();
			}
		XCEPT_RAISE(tstore::exception::Exception,error.str());
	}
}

void tstore::OracleConnection::getTablesMatchingPattern(std::set<std::string> &tableNames,const std::string &pattern) throw (tstore::exception::Exception) {
	std::insert_iterator<std::set<std::string> > iterator(tableNames,tableNames.begin());
	std::string statement="select table_name from user_tables where REGEXP_LIKE(table_name,:pattern,'i')" // previously used table_name like :pattern escape '@'" but it seems like most RDBMS have regexp support so we can use that.
				" and not REGEXP_LIKE(table_name,'.*"+backupVersionPattern+"')"; //filter out backed up versions of tables (this has to be done in here since the names of backup tables is determined by this class. Perhaps in future backup table names will be determined by TStore)
	try {
		selectSingleColumn(iterator,pattern,statement);
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not read table names matching pattern '"+pattern+"'.",e);
	}
}

//in fact this is useless because in any case to sync from database we need to know what the keys are.
bool tstore::OracleConnection::keysMatch(const std::string &tableName,const std::string &keys) throw (tstore::exception::Exception) {
	std::vector<std::string> keysInDatabase;
	std::back_insert_iterator<std::vector<std::string> > iterator(keysInDatabase);
	std::ostringstream keyInDatabaseString;
	getPrimaryKeyColumnsForTable(tableName,iterator);
	std::copy(keysInDatabase.begin(),keysInDatabase.end(),std::ostream_iterator<std::string>(keyInDatabaseString, ","));
	//std::cout << "in database: " << keyInDatabaseString.str() << " compared with " << toolbox::toupper(keys)+"," << std::endl;
	return keyInDatabaseString.str()==toolbox::toupper(keys)+",";
}

void tstore::OracleConnection::getKeysForTable(const std::string &tableName,std::vector<std::string> &keys) throw (tstore::exception::Exception) {
	std::back_insert_iterator<std::vector<std::string> > iterator(keys);
	getPrimaryKeyColumnsForTable(tableName,iterator);
}

//this gets the columns and types as guessed from the database. The actual column types may differ due to mappings and other view configuration
void tstore::OracleConnection::getTableDefinition(const std::string &tableName,xdata::Table &definition) throw (tstore::exception::Exception) {
	oracle::occi::Statement *constraintStatement=NULL;
	constraintStatement=createStatement();
	constraintStatement->setSQL("select user_cons_columns.column_name,search_condition from user_cons_columns,user_constraints where user_cons_columns.constraint_name=user_constraints.constraint_name and search_condition is not null and upper(user_constraints.table_name) = :tablename");
	if (!constraintStatement) {
		XCEPT_RAISE(tstore::exception::Exception,"Could not read metadata for table '"+tableName+"'. Could not create statement.");
	}
	std::vector<std::string> blobColumns;
	std::map<std::string, std::string> types;
	definition.clear();
	try {
		//if columns have the exact check constraints that would have been set if the table were added by TStore, then we can recognise them
		//and use the corresponding xdata types.
		constraintStatement->setMaxParamSize(1,tableName.size()+1);
		constraintStatement->setString(1, toolbox::toupper(tableName));
		oracle::occi::ResultSet *rs = constraintStatement->executeQuery();
		rs->setMaxColumnSize(1,200);
		rs->setMaxColumnSize(2,200);
		while (rs->next()) {
			std::string columnName=rs->getString(1);
			std::string checkConstraint=rs->getString(2);
			//std::cout << "looking for type matching check constraint " + checkConstraint + " on column " + columnName << std::endl;
			std::string type=xdataTypeMatchingCheckConstraint(checkConstraint,columnName);
			if (!type.empty()) {
				std::cout << "matches type " << type << std::endl;
				types[columnName]=type;
			} else {
				//std::cout << "no match" << std::endl;
			}
		}

		//get the default types for the columns which did not have recognisable check constraints
		oracle::occi::MetaData tableMetaData = connection()->getMetaData(quoteSchemaObjectName(tableName), oracle::occi::MetaData::PTYPE_TABLE);
		std::vector<oracle::occi::MetaData> columns = tableMetaData.getVector(oracle::occi::MetaData::ATTR_LIST_COLUMNS);
		for (std::vector<oracle::occi::MetaData>::iterator column = columns.begin(); column != columns.end(); column++) {
			std::string columnName=(*column).getString(oracle::occi::MetaData::ATTR_NAME);
			int columnType = (*column).getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);

			if (columnType == oracle::occi::OCCI_SQLT_BLOB) {
				//xdataTypeOfBlobColumn() cannot be used here because it closes connection.
				//Thus blob columns are processed afterwards
				blobColumns.push_back(columnName);
			}
			else if (!types.count(columnName)) {
				types[columnName]=defaultXdataTypeForColumn(*column);
				//std::cout << "using default type " << types[columnName] << " for column " << columnName << std::endl;
			}
		}
	} catch (xdata::exception::Exception &e) {
		terminateStatement(constraintStatement);
		XCEPT_RETHROW(tstore::exception::Exception, "Could not get column types for table " + tableName + ".", e);
	} catch (oracle::occi::SQLException &e) {
		terminateStatement(constraintStatement);
		XCEPT_RAISE(tstore::exception::Exception, "Could not get column types for table "+tableName+". Oracle returned error: "+std::string(e.what()));
	}
	terminateStatement(constraintStatement);

	//Retrieving BLOB column types
	for (std::vector<std::string>::iterator columnName = blobColumns.begin(); columnName != blobColumns.end(); columnName++) {
		std::string xdataType = xdataTypeOfBlobColumn(tableName, *columnName);
		// Raise an exception if blob column has no mapping
		if (xdataType == "")
			XCEPT_RAISE(tstore::exception::Exception, "BLOB column " + tableName + "." + *columnName + " has no mapping in TSTORE_META table.");
		types[*columnName] = xdataType;
	}

	//now copy them into the table definition
	for (std::map<std::string,std::string>::iterator columnType = types.begin(); columnType != types.end(); columnType++) {
		definition.addColumn((*columnType).first, (*columnType).second);
	}
}

void tstore::OracleConnection::getColumnTypes(std::string tableName,std::map<const std::string,std::string> &types,const MappingList &mappings) throw (tstore::exception::Exception) {
	try {
		oracle::occi::MetaData tableMetaData=connection()->getMetaData(quoteSchemaObjectName(tableName));//,oracle::occi::MetaData::PTYPE_TABLE);
		std::vector<oracle::occi::MetaData> columns=tableMetaData.getVector(oracle::occi::MetaData::ATTR_LIST_COLUMNS);
		std::vector<oracle::occi::MetaData>::iterator columnIterator;
		for (columnIterator=columns.begin();columnIterator!=columns.end();columnIterator++) {
			std::string type=xdataTypeForColumn(*columnIterator,mappings,tableName);
			types[(*columnIterator).getString(oracle::occi::MetaData::ATTR_NAME)]=type;
		} 
	} catch (oracle::occi::SQLException &e) {
		//XCEPT_RAISE(tstore::exception::Exception, "Could not get metadata for table "+tableName+". Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not get metadata for table "+tableName+". Oracle returned error: "+std::string(e.what()));
	}
}

bool tstore::OracleConnection::isTimestampType (unsigned int oracleType) {
	return (oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP || oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP_TZ || oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP_LTZ);
}

void tstore::OracleConnection::setTimestamp(oracle::occi::Statement *statement,int parameterIndex,xdata::TimeVal *timeValue) throw (oracle::occi::SQLException) {
	/* In theory, we should use the Date datatype for an OCCI_SQLT_DATE.
	however, the fromText method for Date crashes without throwing an exception if the input is not a valid date string.
	the fromText method in Timestamp throws an exception, so it's much safer, and OCCI doesn't mind if we use a timestamp
	as a date. */
	oracle::occi::Timestamp timestamp;
	if (timeValue->isNaN()) {
		timestamp.setNull();
	} else {
		//since there is no way in strftime to include fractional seconds, we'll use the default format of TimeVal which
		//has 6 decimal places.
		std::string timeString(((toolbox::TimeVal)*timeValue).toString(/*"%Y/%m/%d %H:%M:%S.%s"*/"",toolbox::TimeVal::gmt));
		timeString.erase(timeString.end()-1);
		std::string::size_type location=timeString.find('T');
		if (location!=std::string::npos) timeString.replace(location,1," ");
		timeString+=" 00:00"; //make sure Oracle knows it's UTC
		//std::cout << "timeString=" << timeString << std::endl;
		timestamp.fromText(timeString,/*"YYYY/MM/DD HH24:MI:SS TZH:TZM"*/"YYYY-MM-DD HH24:MI:SS.FF6 TZH:TZM","",environment_);
	}
	statement->setTimestamp(parameterIndex,timestamp);
}

void tstore::OracleConnection::setString(oracle::occi::Statement *statement,int parameterIndex,const oracle::occi::MetaData &metadata,std::string value,const MappingList &mappings,const std::string &tableName) throw (oracle::occi::SQLException,tstore::exception::Exception) {
	unsigned int maxLength=metadata.getInt(oracle::occi::MetaData::ATTR_CHAR_SIZE);
	//std::cout << "data type of " << metadata.getString(oracle::occi::MetaData::ATTR_NAME) << " is " << metadata.getInt(oracle::occi::MetaData::ATTR_DATA_TYPE) << " of size " << maxLength << std::endl;
	if (metadata.getInt(oracle::occi::MetaData::ATTR_DATA_TYPE)==oracle::occi::OCCI_SQLT_CLOB) {
		//This can be set using setString, but only if the clob is 4000 characters or less. So we will set it as a Clob.
		setEmptyClob(statement,parameterIndex);
		//unsigned int stringLength=value.size();
		//std::cout << "clob of size " << stringLength << std::endl;
	} else { 
		if (maxLength==0) maxLength=4000; //a varchar2 column should have a non-zero length. However, a column of a view might not (although, it probably wouldn't be updateable anyway) so we will assume 4000, the maximum length of a varchar2.
		value=value.substr(0,maxLength); 
		//if the string is too long, Oracle will complain. 
		//I'm not sure whether we want to pass that error on to clients, for now let's just fix it
		
		if (statement->getMaxParamSize(parameterIndex)==0) {
			statement->setMaxParamSize(parameterIndex,maxLength); //this needs to be set to something because otherwise the maxIterations gets overwritten when any data is set
		}
		statement->setString(parameterIndex,value);
	}
}

void tstore::OracleConnection::setEmptyBlob(oracle::occi::Statement *statement,int parameterIndex) throw (oracle::occi::SQLException) {
	oracle::occi::Blob blob(connection());
	blob.setEmpty();
	statement->setBlob(parameterIndex,blob);
}

void tstore::OracleConnection::setEmptyClob(oracle::occi::Statement *statement,int parameterIndex) throw (oracle::occi::SQLException) {
	oracle::occi::Clob clob(connection());
	clob.setEmpty();
	statement->setClob(parameterIndex,clob);
}

void tstore::OracleConnection::copyStringToClob(oracle::occi::Clob &clob,xdata::Serializable *value) throw (oracle::occi::SQLException,tstore::exception::Exception) {
	try {
		xdata::String *stringValue=dynamic_cast<xdata::String *>(value);
		if (stringValue) {
			std::string s=stringValue->toString();
			int stringLength=s.size();
			if (stringLength) {
				unsigned char *cstring=new unsigned char[stringLength+1];
				strcpy((char *)cstring,s.c_str());
				clob.write(stringLength,cstring,stringLength+1);
				delete[] cstring;
			}
		}
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not copy string into clob",e);
	}
}

void tstore::OracleConnection::copyMimeToBlob(oracle::occi::Blob &blob,xdata::Serializable *value) throw (oracle::occi::SQLException,tstore::exception::Exception) {
	try {
		xdata::Mime *mime=dynamic_cast<xdata::Mime *>(value);
		if (mime && mime->getEntity()) {
			xdata::exdr::AutoSizeOutputStreamBuffer sbuf;
			xdata::exdr::Serializer serializer;
			serializer.exportAll(value,&sbuf);
			/*unsigned long bytesWritten=*/blob.write(sbuf.tellp(),(unsigned char*)sbuf.getBuffer(),sbuf.tellp());
			//std::cout << bytesWritten << " bytes written" << std::endl;
		}
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not copy xdata::Mime into blob",e);
	}
}

void tstore::OracleConnection::copyBlobToMime(xdata::Serializable *value,oracle::occi::Blob &blob) throw (oracle::occi::SQLException,tstore::exception::Exception) {
	unsigned char *buffer=NULL;
	xdata::Mime *mime=dynamic_cast<xdata::Mime *>(value);
	if (mime) try {
		xdata::exdr::Serializer serializer;
		unsigned int length=blob.length();
		buffer=new unsigned char[length];
		blob.read(length,buffer,length);
		xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char *)buffer,length);
		serializer.import(value, &inBuffer );
		delete buffer;
	} catch (xdata::exception::Exception &e) {
		delete buffer;
		XCEPT_RETHROW(tstore::exception::Exception,"Could not copy blob into xdata::Mime",e);
	}
}

//for the writeVectorOfBlobs version
/*void tstore::OracleConnection::copyMimeToBuffer(unsigned char *&buffer,oraub8 &bufferLength,xdata::Serializable *value) throw (tstore::exception::Exception) {
	xdata::Mime *mime=dynamic_cast<xdata::Mime *>(value);
	if (mime && mime->getEntity()) {
		xdata::exdr::AutoSizeOutputStreamBuffer sbuf;
		xdata::exdr::Serializer serializer;
		serializer.exportAll(value,&sbuf);
		bufferLength=sbuf.tellp();
		std::cout << bufferLength << " bufferLength" << std::endl;
		if (bufferLength) {
			buffer=new unsigned char[bufferLength];
			memcpy(buffer,(unsigned char*)sbuf.getBuffer(),bufferLength);
		}
		//unsigned long bytesWritten=blob.write(sbuf.tellp(),(unsigned char*)sbuf.getBuffer(),sbuf.tellp());
		//std::cout << bytesWritten << " bytes written" << std::endl;
	} else {
		bufferLength=0;
		//XCEPT_RAISE(tstore::exception::Exception,"Co
	}
}*/

void freeBuffers(unsigned char *buffer[],unsigned int count) throw () {
	for (unsigned int bufferIndex=0;bufferIndex<count;bufferIndex++) {
		if (buffer[count]) delete buffer[count];
	}
}

template<class xdataType>
void tstore::OracleConnection::setNumber(oracle::occi::Statement *statement,xdata::Serializable *value,int parameterIndex) {
	xdataType *typedValue=static_cast<xdataType *>(value);
	oracle::occi::Number number(0);
	//std::string number=typedValue->toString();
	//std::cout << typedValue->type() << " ";
	if (typedValue->isNaN()) {
		number.setNull();
	} else if (typedValue->isInf()) {
		number=numberFromString(std::numeric_limits<xdataType>::max().toString());
		++number;
		//std::cout << "Infinity is " << std::numeric_limits<simpleType>::infinity() << std::endl;
	} else {
		number=numberFromString(typedValue->toString());
		//number.fromText(environment_,typedValue->toString(),"TM9");
	}
	//std::cout << (simpleType)*typedValue << std::endl;
	statement->setNumber(parameterIndex,number);
}

template<class xdataType,typename occiType>
occiType convertBinaryNumber(xdata::Serializable *value) {
	xdataType *xdataValue=static_cast<xdataType *>(value);
	occiType occiValue;
	if (xdataValue->isNaN()) occiValue.isNull=true;
	occiValue.value=*xdataValue;
	return occiValue;
}

//this is used when setting bind variables which do not necessarily correspond to columns
void tstore::OracleConnection::setStatementParameter(oracle::occi::Statement *statement,const std::string &columnType,unsigned int parameterIndex,xdata::Serializable *value) throw (oracle::occi::SQLException) {
	if (columnType=="string") {
		statement->setString(parameterIndex,value->toString());
	} else if (columnType=="time") {
		xdata::TimeVal *timeValue=static_cast<xdata::TimeVal *>(value);
		setTimestamp(statement,parameterIndex,timeValue);
	} else if (columnType=="float") {
		statement->setBFloat(parameterIndex,convertBinaryNumber<xdata::Float,oracle::occi::BFloat>(value));
	}
	else if (columnType=="double") {
		statement->setBDouble(parameterIndex,convertBinaryNumber<xdata::Double,oracle::occi::BDouble>(value));
	}
	else if (columnType=="int") {
		setNumber<xdata::Integer>(statement,value,parameterIndex);
	} else if (columnType=="unsigned long") {
		setNumber<xdata::UnsignedLong>(statement,value,parameterIndex);
	} else if (columnType=="mime") {
		setEmptyBlob(statement,parameterIndex);
	} else if (columnType=="bool") {
		xdata::Boolean *boolValue=static_cast<xdata::Boolean *>(value);
		oracle::occi::Number number((xdata::BooleanT)*boolValue);
		if (boolValue->isNaN()) {
			number.setNull();
		}
		statement->setNumber(parameterIndex,number);
		//setNumber<xdata::Boolean>(statement,value,parameterIndex);
	} else if (columnType=="unsigned int") {
		setNumber<xdata::UnsignedInteger>(statement,value,parameterIndex);
	} else if (columnType=="unsigned int 32") {
		setNumber<xdata::UnsignedInteger32>(statement,value,parameterIndex);
	} else if (columnType=="unsigned int 64") {
		setNumber<xdata::UnsignedInteger64>(statement,value,parameterIndex);
	} else if (columnType=="unsigned short") {
		setNumber<xdata::UnsignedShort>(statement,value,parameterIndex);
	} 
}

template <class occiClass,class xdataClass> 
void tstore::OracleConnection::setVector(oracle::occi::Statement *statement,unsigned int parameterIndex,xdata::Serializable *value,const std::string &typeName) throw (oracle::occi::SQLException) {
	typename std::vector<occiClass> occiVector;
	typename xdata::Vector<xdataClass> *xdataVector=dynamic_cast<xdata::Vector<xdataClass> *>(value);
	if (xdataVector) {
		for (typename xdata::Vector<xdataClass>::iterator s=xdataVector->begin();s!=xdataVector->end();++s) {
			//std::cout << "adding value to array " << (*s).toString() << std::endl;
			//oracle::occi::UString uString(stdString);

			occiVector.push_back((occiClass)(*s));				
		}
	} 
	else XCEPT_RAISE(tstore::exception::Exception,"could not set a "+value->type());
	oracle::occi::setVector(statement,parameterIndex,occiVector,"",typeName);
}


template <class occiClass,class xdataClass> 
void tstore::OracleConnection::setBinaryVector(oracle::occi::Statement *statement,unsigned int parameterIndex,xdata::Serializable *value,const std::string &typeName) throw (oracle::occi::SQLException) {
	typename std::vector<occiClass> occiVector;
	typename xdata::Vector<xdataClass> *xdataVector=dynamic_cast<xdata::Vector<xdataClass> *>(value);
	if (xdataVector) {
		for (typename xdata::Vector<xdataClass>::iterator s=xdataVector->begin();s!=xdataVector->end();++s) {
			//std::cout << "adding value to array " << (*s).toString() << std::endl;
			//oracle::occi::UString uString(stdString);
			occiClass occiValue;
			occiValue.isNull=(*s).isNaN();
			occiValue.value=(*s);
			occiVector.push_back(/*(occiClass)(*s)*//*(*s).toString()*/occiValue);				
		}
	} 
	else XCEPT_RAISE(tstore::exception::Exception,"could not set a "+value->type());
	oracle::occi::setVector(statement,parameterIndex,occiVector,"",typeName);
}

//used for setting the values to insert in a DML statement
void tstore::OracleConnection::setStatementParameter(oracle::occi::Statement *statement,const std::string &columnType,unsigned int parameterIndex,const oracle::occi::MetaData *metadata,xdata::Serializable *value,const MappingList &mappings,const std::string &tableName) 
	throw (tstore::exception::Exception) {
	if (!statement) XCEPT_RAISE(tstore::exception::Exception,"Internal error: No statement supplied to setStatementParameter");
	if (!metadata) XCEPT_RAISE(tstore::exception::Exception,"Internal error: No metadata supplied to setStatementParameter");
	try {
		if (columnType=="string") {
			//we can tell whether it should be inserted as a CLOB or not, so don't use the 'simple' implementation for setting a string
			xdata::String *stringValue=dynamic_cast<xdata::String *>(value);
			setString(statement,parameterIndex,*metadata,stringValue?(std::string)*stringValue:"",mappings,tableName);
		} else if (columnType.find("vector ")==0) {
			setEmptyBlob(statement, parameterIndex);
		} else {
			setStatementParameter(statement,columnType,parameterIndex,value);
		}
	} catch (oracle::occi::SQLException &e) {
		std::string valueAsString;
		try {
			valueAsString=value->toString();
		} catch (xdata::exception::Exception) {
			valueAsString="(cannot display value)";
		}
		std::ostringstream error;
		std::string format=formatForColumn(*metadata,mappings,tableName);
		error << "Could not put " << columnType << " value " << valueAsString << " into column " << metadata->getString(oracle::occi::MetaData::ATTR_NAME) << " using parameter index " << parameterIndex;
		if (!format.empty()) error << " using format "+format;
		//error << ". Oracle returned error: " << std::string(e.getMessage());
		error << ". Oracle returned error: " << std::string(e.what());
		//std::cout << error.str() << std::endl;
		XCEPT_RAISE(tstore::exception::Exception,error.str());
	}
}

//this method creates and returns an SQL statement for doing an insert according to the insert parameter.
std::string tstore::OracleConnection::createInsertStatement(SQLInsert &insert,std::string &blobColumnNames,std::map<const std::string,oracle::occi::MetaData*> &columnMetadata) {
	xdata::Table *data=insert.data;
	if (data && data->getRowCount()) {
		std::string columnNames,values;
		std::vector<std::string> columns=data->getColumns();
		std::vector<std::string>::iterator columnIterator;
		bool addedColumn=false;
		bool addedBlob=false;
		for(columnIterator=columns.begin(); columnIterator!=columns.end(); columnIterator++) {
			if (insert.shouldChangeColumn(*columnIterator)) {
				unsigned int columnType=0;
				if (columnMetadata.count(*columnIterator)) {
					columnType=columnMetadata[*columnIterator]->getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);
				}
				//if (data->getColumnType(*columnIterator)!="mime") {
					if (addedColumn) {
						columnNames+=",";
						values+=",";
					} else addedColumn=true;
					columnNames+=quoteSchemaObjectName(*columnIterator);
					values+=":"+*columnIterator;
				if (data->getColumnType(*columnIterator)=="mime" ||
					toolbox::startsWith(data->getColumnType(*columnIterator), "vector ") ||
					columnType==oracle::occi::OCCI_SQLT_CLOB)
				{
					if (addedBlob) {
						blobColumnNames+=",";
					} else addedBlob=true;
					blobColumnNames+=quoteSchemaObjectName(*columnIterator);
				}
			}
		}
		if (addedColumn) {
			//selectForUpdate="select ("+blobColumnNames+") from "+quoteSchemaObjectName(insert.tableName())+" for update";
			return "insert into "+quoteSchemaObjectName(insert.tableName())+" ("+columnNames+") values ("+values+")";
		}
	}
	return "";
}

//fills in metadata with table names as keys and table metadata as values.
//you must call freeMetaData on the resulting map. (It must be a map of pointers since MetaData has no default constructor.)
//if columnNames is non-NULL, also fills in columnNames with the names of the columns in the table.
//this is just for convenience, the same array of columns could be obtained from the keys of metadata.
void tstore::OracleConnection::getMetaDataForTableColumns(std::map<const std::string,oracle::occi::MetaData*> &metadata,const std::string &tableName,std::vector<std::string> *columnNames) {
	oracle::occi::MetaData tableMetaData=connection()->getMetaData(quoteSchemaObjectName(tableName)/*,oracle::occi::MetaData::PTYPE_TABLE*/);
	std::vector<oracle::occi::MetaData> columns=tableMetaData.getVector(oracle::occi::MetaData::ATTR_LIST_COLUMNS);
	std::vector<oracle::occi::MetaData>::iterator columnIterator;
	
	for (columnIterator=columns.begin();columnIterator!=columns.end();columnIterator++) {
		std::string columnName=(*columnIterator).getString(oracle::occi::MetaData::ATTR_NAME);
		metadata[columnName]=new oracle::occi::MetaData(*columnIterator);
		if (columnNames) columnNames->push_back(columnName);
	}
}

//frees the memory allocated by getMetaDataForTableColumns
void tstore::OracleConnection::deleteMetaData(std::map<const std::string,oracle::occi::MetaData*> &metadata) {
	std::map<const std::string,oracle::occi::MetaData*>::iterator columnIterator;
	for (columnIterator=metadata.begin();columnIterator!=metadata.end();columnIterator++) {
		delete (*columnIterator).second;
		metadata.erase(columnIterator);
	}
}

void tstore::OracleConnection::execute(SQLInsert &insert) throw (tstore::exception::Exception) {
	MappingList mappings;
	execute(insert,mappings);
}

//inserts the data in \p insert into the database table specified by \p insert
//throws an exception if there is a database error.
void tstore::OracleConnection::execute(SQLInsert &insert,const MappingList &mappings) throw (tstore::exception::Exception) {
	std::map<const std::string,oracle::occi::MetaData*> columnMetadata;
	std::vector<std::string> columnNames;
	oracle::occi::Statement *statement = createStatement();
	try {
		xdata::Table *data=insert.data;
		std::vector<std::string> columns=data->getColumns();
		getMetaDataForTableColumns(columnMetadata,insert.tableName(),&columnNames);
		insert.setExistingColumns(columnNames.begin(),columnNames.end());
		std::string blobColumns;
		std::string sql=createInsertStatement(insert,blobColumns,columnMetadata);
		//std::cout << sql << std::endl;
		if (sql.length()) {
			unsigned long rowCount=insert.data->getRowCount();
			unsigned long currentRow;
			//std::cout << "SQL: " << sql << std::endl;
			statement->setSQL(sql);
			statement->setMaxIterations(rowCount);
			std::vector<std::string>::iterator columnIterator;

			for (currentRow=0;currentRow<rowCount;currentRow++) {
				int parameterIndex=0;
				if (currentRow>0) statement->addIteration();
				for(columnIterator=columns.begin(); columnIterator!=columns.end(); ++columnIterator) {
					std::string columnName = toolbox::toupper(*columnIterator);
					// columns in xdata table are in upper/lowercase
					xdata::Serializable *value=data->getValueAt(currentRow,*columnIterator);					
					std::string columnType=data->getColumnType(*columnIterator);
					// Columns in SQL world are in uppercase
					if (insert.shouldChangeColumn(columnName)/* && columnType!="mime"*/) {
						parameterIndex++;
						//std::cout << "trying to put " << columnType << " value into column " << columnMetadata[columnName]->getString(oracle::occi::MetaData::ATTR_NAME) << " using parameter index " << parameterIndex << std::endl;
						if (columnMetadata.count( columnName )) {
							setStatementParameter(statement,columnType,parameterIndex,columnMetadata[columnName],value,mappings,insert.tableName());
						}
						//std::cout << "done" << std::endl;
					}
				}
			}
			unsigned int rowsInserted=statement->executeUpdate();
			commitNeeded = true;
			deleteMetaData(columnMetadata);
			
			terminateStatement(statement);
			if (rowsInserted!=rowCount) {
				//throw an exception?
			}
			if (!blobColumns.empty()) {
				updateBlobs(insert,mappings);
			}
		} else {
			//although it is not an error to insert nothing (that can never fail!)
			//perhaps there should be an exception if some data was given but none in the columns to update
			//currently this is checked by TStore
		}
	} catch (oracle::occi::SQLException &e) {
		deleteMetaData(columnMetadata);
		terminateStatement(statement);
		//XCEPT_RAISE(tstore::exception::Exception, "Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Oracle returned error: "+std::string(e.what()));
	} catch (tstore::exception::Exception &e) {
		deleteMetaData(columnMetadata);
		terminateStatement(statement);
		XCEPT_RETHROW(tstore::exception::Exception, e.what(),e);
	}
}


void tstore::OracleConnection::execute(SQLUpdate &update) throw (tstore::exception::Exception) {
	MappingList mappings;
	execute(update,mappings);
}

bool tstore::OracleConnection::addToWhereClauseIfKey(std::string &whereClause,unsigned int &addedKeys,const std::string &columnName,const std::set<std::string> &keys) {
	std::string columnString=quoteSchemaObjectName(columnName)+"=:"+columnName;
	if (keys.count(toolbox::toupper(columnName))) {
		if (addedKeys>0) {
			whereClause+=" and ";
		}
		whereClause+=columnString;
		addedKeys++;
		return true;
	}
	return false;
}

//throws an exception if the number of rows affected by the operation was not equal to the number of rows expected.
void tstore::OracleConnection::checkKeysMatched(unsigned int rowCount,unsigned int affectedCount,const std::string &tableName,const std::string &operation) throw (tstore::exception::Exception) {
	if (affectedCount!=rowCount) {
		//if the keys have been modified, then some updates may not match any rows.
		//I can't think of anything that would cause rowsUpdated to be greater than rowCount (we have already checked for not specifying all the keys)
		//but we'd better throw an exception if that happens too.
		std::ostringstream error;
		error << "Key fields did not all match records in table "+tableName+": " << rowCount << " rows were supplied but " << affectedCount << " could be found to "+operation+"." << std::flush;
		XCEPT_RAISE(tstore::exception::Exception,error.str());
	}
}

void tstore::OracleConnection::checkKeysSupplied(unsigned int keyCount,unsigned int suppliedKeyCount,const std::string &tableName,const std::string &operation) throw (tstore::exception::Exception) {
	if (keyCount!=suppliedKeyCount) {
		XCEPT_RAISE(tstore::exception::Exception,"Table supplied for "+operation+" does not include all key columns of "+tableName);					
	}
}

//deletes the given rows from the table specified in deletion
//if deletion.data is NULL, deletes everything in the table. (todo: This logic ought to be inside SQLDelete)
void tstore::OracleConnection::execute(SQLDelete &deletion,const MappingList &mappings) throw (tstore::exception::Exception) {
	oracle::occi::Statement *statement=NULL;
	std::string deleteClause="delete from "+quoteSchemaObjectName(deletion.tableName());
	try {
		if (!deletion.data) { //only if the data is actually null
			statement = createStatement(deleteClause);
			statement->executeUpdate();
			commitNeeded = true;
			terminateStatement(statement);
		} else {
			std::vector<std::string> columns=deletion.data->getColumns();
			std::vector<std::string>::iterator columnIterator;
			std::set<std::string> keys;
			std::insert_iterator<std::set<std::string> > iterator(keys,keys.begin());
			unsigned int addedKeys=0;
			std::string whereClause=" where ";
			getPrimaryKeyColumnsForTable(deletion.tableName(),iterator);
			if (keys.empty()) {
				XCEPT_RAISE(tstore::exception::Exception, "Could not delete because the table "+quoteSchemaObjectName(deletion.tableName())+" does not have a primary key.");
			}
			for(columnIterator=columns.begin(); columnIterator!=columns.end(); columnIterator++) {
				addToWhereClauseIfKey(whereClause,addedKeys,*columnIterator,keys);
			}
			//if (!deletion.allowNonUniqueRows()) {
				checkKeysSupplied(keys.size(),addedKeys,deletion.tableName(),"delete");
			//}
			
			statement = createStatement();
			unsigned int rowCount=deletion.data->getRowCount();
			statement->setSQL(deleteClause+whereClause);
			statement->setMaxIterations(rowCount);
			std::map<const std::string,oracle::occi::MetaData*> columnMetadata;
			try {
				unsigned long currentRow;
				getMetaDataForTableColumns(columnMetadata,deletion.tableName());
				//std::cout << deleteClause+whereClause << std::endl;
				for (currentRow=0;currentRow<rowCount;currentRow++) {
					int keyIndex=1;
					if (currentRow>0) statement->addIteration();
					for(columnIterator=columns.begin(); columnIterator!=columns.end(); columnIterator++) {
						xdata::Serializable *value=deletion.data->getValueAt(currentRow,*columnIterator);
						std::string columnType=deletion.data->getColumnType(*columnIterator);
						std::string columnName(toolbox::toupper(*columnIterator));
						if (keys.count(columnName)) {
							//std::cout << "delete setting " << columnName << " of " << deletion.tableName() << " to " << value->toString() << std::endl;
							setStatementParameter(statement,columnType,keyIndex,columnMetadata[columnName],value,mappings,deletion.tableName());
							keyIndex++;
						}
					}
				}
				deleteMetaData(columnMetadata);
			} catch (...) {
				deleteMetaData(columnMetadata);
				throw;
			}
			unsigned int deletedCount=statement->executeUpdate();
			commitNeeded = true;
			terminateStatement(statement);
			checkKeysMatched(rowCount,deletedCount,deletion.tableName(),"delete");
		}
	} catch (oracle::occi::SQLException &e) {
		if (statement) terminateStatement(statement);
		//XCEPT_RAISE(tstore::exception::Exception, "Could not Delete. Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not Delete. Oracle returned error: "+std::string(e.what()));
	}
}

void tstore::OracleConnection::execute(SQLDelete &deletion) throw (tstore::exception::Exception) {
	MappingList mappings;
	execute(deletion,mappings);
}

void tstore::OracleConnection::separateKeysFromUpdateableRows(tstore::SQLDataManipulation &update,std::string &columnsToUpdate,std::string &whereClause,std::set<std::string> &keys,std::map<const std::string,oracle::occi::MetaData*> &columnMetadata,bool blobColumns,unsigned int &addedValues) throw (tstore::exception::Exception) {
	xdata::Table *data=update.data;
	if (data) {
		std::vector<std::string> columns=data->getColumns();
		//unsigned long rowCount=data->getRowCount();
		//unsigned long currentRow;
		unsigned int addedKeys=0;
		addedValues=0;
		std::vector<std::string> columnNames;
		std::vector<std::string>::iterator columnIterator;
		
		std::insert_iterator<std::set<std::string> > iterator(keys,keys.begin());
		getPrimaryKeyColumnsForTable(update.tableName(),iterator);
		if (keys.empty()) {
			XCEPT_RAISE(tstore::exception::Exception, "Could not update because the table "+quoteSchemaObjectName(update.tableName())+" does not have a primary key.");
		}
		
		whereClause=" where ";
		
		getMetaDataForTableColumns(columnMetadata,update.tableName(),&columnNames);
		update.setExistingColumns(columnNames.begin(),columnNames.end());
		for(columnIterator=columns.begin(); columnIterator!=columns.end(); columnIterator++) {
			std::string columnString;
			columnString=quoteSchemaObjectName(*columnIterator)+"=:"+*columnIterator;
			if (keys.count(toolbox::toupper(*columnIterator))) {
				if (addedKeys>0) {
					whereClause+=" and ";
				}
				whereClause+=columnString;
				addedKeys++;
			} else if (update.shouldChangeColumn(*columnIterator)) {
				unsigned int columnType=0;
				if (columnMetadata.count(*columnIterator)) {
					columnType=columnMetadata[*columnIterator]->getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);
				}

				if (!blobColumns || data->getColumnType(*columnIterator)=="mime" ||
					toolbox::startsWith(data->getColumnType(*columnIterator), "vector ") ||
					columnType==oracle::occi::OCCI_SQLT_CLOB) //we still want to insert blobs, just empty ones
				{
					if (blobColumns) columnString=*columnIterator; //for blobs we only want to select them, not update them. This should really be in a separate function rather than having a control boolean like this
					if (addedValues>0) {
						columnsToUpdate+=",";
					}
					columnsToUpdate+=columnString;
					addedValues++;
				}
			}
		}
		if (addedValues==0 && !blobColumns) {
			XCEPT_RAISE(tstore::exception::Exception,"No values to update (only key columns were supplied)");					
		}
	}
}

/*void tstore::OracleConnection::deleteBlobBuffers(std::map<std::string,std::vector<oracle::occi::Blob> > &updatedBlobs,std::map<std::string,unsigned char **> &buffers,std::map<std::string,oraub8*> &bufferLengths) {
	for (std::map<std::string,std::vector<oracle::occi::Blob> >::iterator blobColumn=updatedBlobs.begin();blobColumn!=updatedBlobs.end();++blobColumn) {
		std::string columnName=(*blobColumn).first;
		if (buffers[columnName]) {
			freeBuffers(buffers[columnName],(*blobColumn).second.size());
			delete []buffers[columnName];
		}
		if (bufferLengths[columnName]) {
			delete []bufferLengths[columnName];
		}
	}
}*/

void tstore::OracleConnection::copyXdataValueToBlob(xdata::Serializable* value, uint32_t type, oracle::occi::Blob& blob) {
	xdata::exdr::Serializer serializer;
	xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
	outBuffer.encodeUInt32(type); // column type
	serializer.exportAll(value, &outBuffer);

	blob.write(outBuffer.tellp(), (unsigned char*) outBuffer.getBuffer(), outBuffer.tellp());
}

void tstore::OracleConnection::storeBlobToXdataMapping(const std::string& tableName, const std::string& columnName, const std::string& xdataType) {
	oracle::occi::Statement* insertStmt = NULL;

	try {
		insertStmt = createStatement(
			"INSERT INTO TSTORE_META "
			"(TABLE_NAME, COLUMN_NAME, XDATA_TYPE, CHANGE_DATE) "
			"VALUES (:1, :2, :3, :4)");

		insertStmt->setString(1, tableName);
		insertStmt->setString(2, columnName);
		insertStmt->setString(3, xdataType);

		xdata::TimeVal tv = toolbox::TimeVal::gettimeofday();
		setTimestamp(insertStmt, 4, &tv);

		insertStmt->executeUpdate();
		connection()->commit();

		terminateStatement(insertStmt);
	} catch(oracle::occi::SQLException& e) {
		if (insertStmt) terminateStatement(insertStmt);

		// TStore meta data table does not exist, so create it
		if (e.getErrorCode() == TABLE_NOT_EXIST_EXCEPTION) {
			oracle::occi::Statement* createTblStmt = NULL;

			try {
				createTblStmt = createStatement(
					"CREATE TABLE TSTORE_META ("
					"TABLE_NAME VARCHAR2(30), "
					"COLUMN_NAME VARCHAR2(30), "
					"XDATA_TYPE VARCHAR2(30), "
					"CHANGE_DATE TIMESTAMP, "
					"CONSTRAINT TSTORE_META_PK PRIMARY KEY (TABLE_NAME, COLUMN_NAME))");

				createTblStmt->execute();
				terminateStatement(createTblStmt);
			} catch(oracle::occi::SQLException& e) {
				if (createTblStmt) terminateStatement(createTblStmt);
				XCEPT_RAISE(tstore::exception::Exception, "Could not create TStore meta data table. Oracle returned error: " + std::string(e.what()));
			}

			// Table exists, try to insert now
			storeBlobToXdataMapping(tableName, columnName, xdataType);
		}
		// Mapping assigned to such column exists already
		else if (e.getErrorCode() == UNIQUE_CONSTRAINT_VIOLATED_EXCEPTION) {
			XCEPT_RAISE(tstore::exception::Exception, "Mapping for " + tableName + "." + columnName + " exists already in TSTORE_META table.\n");
		}
		else {
			XCEPT_RAISE(tstore::exception::Exception, "Could not insert BLOB to xdata mapping. Oracle returned error: " + std::string(e.what()));
		}
	}
}

void tstore::OracleConnection::removeBlobToXdataMapping(const std::string& tableName) {
	oracle::occi::Statement* stmt = NULL;

	try {
		stmt = createStatement(
			"DELETE FROM TSTORE_META "
			"WHERE TABLE_NAME = :1");

		stmt->setString(1, tableName);
		stmt->executeUpdate();
		connection()->commit();

		terminateStatement(stmt);
	} catch(oracle::occi::SQLException& e) {
		if (stmt) terminateStatement(stmt);

		// Do not raise if table does not exist
		if (e.getErrorCode() != TABLE_NOT_EXIST_EXCEPTION) {
			XCEPT_RAISE(tstore::exception::Exception, "Could not remove BLOB to xdata mapping. Oracle returned error: " + std::string(e.what()));
		}
	}
}

void tstore::OracleConnection::renameBlobToXdataMapping(const std::string& oldTableName, const std::string& newTableName) {
	oracle::occi::Statement* stmt = NULL;

	try {
		stmt = createStatement(
			"UPDATE TSTORE_META "
			"SET TABLE_NAME = :1 "
			"WHERE TABLE_NAME = :2");

		stmt->setString(1, newTableName);
		stmt->setString(2, oldTableName);
		stmt->executeUpdate();
		connection()->commit();

		terminateStatement(stmt);
	} catch(oracle::occi::SQLException& e) {
		if (stmt) terminateStatement(stmt);

		// Do not raise if table does not exist
		if (e.getErrorCode() != TABLE_NOT_EXIST_EXCEPTION) {
			XCEPT_RAISE(tstore::exception::Exception, "Could not rename table name in TSTORE_META. Oracle returned error: " + std::string(e.what()));
		}
	}
}

std::string tstore::OracleConnection::xdataTypeOfBlobColumn(const std::string& tableName, const std::string& columnName) {
	oracle::occi::Statement* stmt = NULL;
	oracle::occi::ResultSet* rs = NULL;
	std::string xdataType = "";

	try {
		stmt = createStatement(
			"SELECT XDATA_TYPE FROM TSTORE_META "
			"WHERE LOWER(TABLE_NAME) = :1 AND LOWER(COLUMN_NAME) = :2");

		stmt->setString(1, toolbox::tolower(tableName));
		stmt->setString(2, toolbox::tolower(columnName));
		rs = stmt->executeQuery();

		if (rs->next()) xdataType = rs->getString(1);

		// There should be only one mapping record for specified table and column
		if (rs->next())
			XCEPT_RAISE(tstore::exception::Exception,
				"TSTORE_META table has more than 1 mapping for " +
				tableName + "." + columnName);

		stmt->closeResultSet(rs);
		terminateStatement(stmt);
	} catch(oracle::occi::SQLException& e) {
		if (rs && stmt) stmt->closeResultSet(rs);
		if (stmt) terminateStatement(stmt);

		XCEPT_RAISE(tstore::exception::Exception,
			"Could not retrieve xdata type for BLOB column: " + tableName + "." + columnName +
			". Error: " + std::string(e.what()));
	}

	return xdataType;
}

void tstore::OracleConnection::updateBlobs(SQLDataManipulation &update,const MappingList &mappings) throw (tstore::exception::Exception) {
	std::map<const std::string,oracle::occi::MetaData*> columnMetadata;
	//std::vector<std::string> columnNames;
	unsigned long rowCount=0;
	commit();
	try {
		std::string tableToUpdate=update.tableName();
		//convert the table name to uppercase, as we are quoting it in the SQL.
		//In theory people might want to use a table with a mixed-case name, in which case this line
		//would have to be removed and also the upper() calls in getPrimaryKeyColumnsForTable would have to be removed.
		//however people don't usually use case-sensitive table names and it would be a hassle to make all the client code
		//do the uppercasing so for convenience just do it here. (Actually it should probably be done within SQLUpdate)
		xdata::Table *data=update.data;
		if (data) {
			rowCount=data->getRowCount();
			unsigned long currentRow;
			std::string sql;
			std::string whereClause;
			std::string columnsToUpdate;
			std::set<std::string> keys;
			unsigned int addedValues;
			separateKeysFromUpdateableRows(update,columnsToUpdate,whereClause,keys,columnMetadata,true,addedValues);

			sql="select "+columnsToUpdate+" from "+quoteSchemaObjectName(tableToUpdate)+whereClause+" for update";
			//std::cout << "SQL UpdateBlobs: "<< sql << std::endl;
			//statement->setMaxIterations(rowCount);
			
			//I mistakenly thought that blobs had to be updated with writeVectorOfBlobs, but it turns out
			//that is only available in 10gr2, and does not seem to be declared in the version of occi I'm using.
			//instead we can just update the individual blobs and they will be written on commit
			//I have left the old code commented out as it might be a good idea if we move to the right version of occi.
			//std::vector<std::string> columns=data->getColumns();
			//std::vector<std::string>::iterator column;
			//std::map<std::string,std::vector<oracle::occi::Blob> > updatedBlobs;
			//std::map<std::string,unsigned char **> buffers;
			//std::map<std::string,oraub8*> bufferLengths;
			std::vector<std::string> columns=data->getColumns();
			std::vector<std::string>::iterator column;
			oracle::occi::Statement *statement = createStatement(sql);
			for (currentRow=0;currentRow<rowCount;currentRow++) {
				try {
					//statement->setSQL(sql);
					//int keyIndex=addedValues+1;
					//int valueIndex=1;
					int parameterIndex=0;
					//if (currentRow>0) statement->addIteration();
					for(column=columns.begin(); column!=columns.end(); ++column) {
						std::string columnName(toolbox::toupper(*column));
						xdata::Serializable *value=data->getValueAt(currentRow,*column);
						std::string columnType=data->getColumnType(*column);
						if (keys.count(columnName)) parameterIndex++;//=keyIndex++;
						//else if (update.shouldChangeColumn(*columnIterator)) parameterIndex=valueIndex++;
						else continue; //this column is not used as a key, and in this statement keys are the only bind variables
						if (columnMetadata.count( columnName )) {
							setStatementParameter(statement,columnType,parameterIndex,columnMetadata[columnName],value,mappings,update.tableName());
						}
					}
					oracle::occi::ResultSet *rs = runQuery(statement);

					if (rs->next()) { //there should only be one row
						unsigned int columnIndex=0;
						//now fill in the values of the blobs of that row, and add them to vectors
						for(column=columns.begin(); column!=columns.end(); ++column) {
							std::string columnType = data->getColumnType(*column);
							xdata::Serializable* value = data->getValueAt(currentRow, *column);

							if (columnType=="mime") {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::Mime), blob);
								/*if (!bufferLengths[*column]) {
									buffers[*column]=new unsigned char *[rowCount];
									bufferLengths[*column]=new oraub8[rowCount];
o								}*/
								//copyMimeToBlob(blob,data->getValueAt(currentRow,*column));
								//std::cout << "copying mime to blob for column " << *column << " row " << currentRow <<std::endl;
								//in the newer version we would copy it to a character buffer instead.
								//copyMimeToBuffer(buffers[*column][currentRow],bufferLengths[*column][currentRow],data->getValueAt(currentRow,*column));
								//let's hope that writeVectorOfBlobs work if the pointer is NULL and the length is zero
								//byteAmounts[*column][currentRow]=bufferLengths[*column][currentRow];
								//updatedBlobs[*column].push_back(blob);
								columnIndex++;
							}
							else if (columnType == "vector int")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorInteger), blob);
								columnIndex++;
							}
							else if (columnType == "vector int 32")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorInteger32), blob);
								columnIndex++;
							}
							else if (columnType == "vector int 64")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorInteger64), blob);
								columnIndex++;
							}
							else if (columnType == "vector unsigned int")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorUnsignedInteger), blob);
								columnIndex++;
							}
							else if (columnType == "vector unsigned int 32")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorUnsignedInteger32), blob);
								columnIndex++;
							}
							else if (columnType == "vector unsigned int 64")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorUnsignedInteger64), blob);
								columnIndex++;
							}
							else if (columnType == "vector unsigned short")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorUnsignedShort), blob);
								columnIndex++;
							}
							else if (columnType == "vector float")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorFloat), blob);
								columnIndex++;
							}
							else if (columnType == "vector double")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorDouble), blob);
								columnIndex++;
							}
							else if (columnType == "vector string")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorString), blob);
								columnIndex++;
							}
							else if (columnType == "vector bool")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorBoolean), blob);
								columnIndex++;
							}
							else if (columnType == "vector time")  {
								oracle::occi::Blob blob = rs->getBlob(columnIndex+1);
								copyXdataValueToBlob(value, static_cast<uint32_t> (xdata::exdr::Serializer::VectorTimeVal), blob);
								columnIndex++;
							}
							else if (columnMetadata.count( *column )) {
								if (columnMetadata[*column]->getInt(oracle::occi::MetaData::ATTR_DATA_TYPE)==oracle::occi::OCCI_SQLT_CLOB) {
									oracle::occi::Clob clob=rs->getClob(columnIndex+1);
									copyStringToClob(clob, value);
									columnIndex++;
								}
							}
						}
					}
					
					if (rs->next()) {
						XCEPT_RAISE(tstore::exception::Exception,"Could not update blob column: too many rows returned from query for update");
					}
					statement->executeUpdate();
					commitNeeded = true;
					statement->closeResultSet(rs);
					
				} catch (...) {
					terminateStatement(statement);
					throw;
				}
			}
			terminateStatement(statement);
			deleteMetaData(columnMetadata);

			//this functionality only exists in 10gr2
			/*oraub8 *offsets=new oraub8[rowCount];
			for (std::map<std::string,std::vector<oracle::occi::Blob> >::iterator blobColumn=updatedBlobs.begin();blobColumn!=updatedBlobs.end();++blobColumn) {
				std::string columnName=(*blobColumn).first;
				oracle::occi::writeVectorOfBlobs(connection_,(*blobColumn).second,bufferLengths[columnName],offsets,buffers[columnName],bufferLengths[columnName]);
			}
			deleteBlobBuffers(updatedBlobs,buffers,bufferLengths);*/
		}
	} catch (oracle::occi::SQLException &e) {
		deleteMetaData(columnMetadata);
		//deleteBlobBuffers(updatedBlobs,buffers,bufferLengths);
		//XCEPT_RAISE(tstore::exception::Exception, "Could not update blobs. Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not update blobs. Oracle returned error: "+std::string(e.what()));
	} catch (tstore::exception::Exception &e) {
		deleteMetaData(columnMetadata);
		//deleteBlobBuffers(updatedBlobs,buffers,bufferLengths);
		//terminateStatement(statement);
		XCEPT_RETHROW(tstore::exception::Exception, e.what(),e);
	}
	//if (!update.allowNonUniqueRows()) {
	//	checkKeysMatched(rowCount,rowsUpdated,update.tableName(),"update");
	//}
}


//updates the table based on the data in the update object.
//only columns allowed by the update object are updated,
//but columns in the data which form part of the primary key are also used, to identify rows.
//Throws an exception if the connection is not open
//Throws an exception if not all the primary key columns are in the data, and the update does not allow non-unique rows
//Throws an exception if some of the rows could not be updated (probably because the primary key has been changed from what is in the database)
//Throws an exception if there is an Oracle error
//todo: define different types for these exceptions
void tstore::OracleConnection::execute(SQLUpdate &update,const MappingList &mappings) throw (tstore::exception::Exception) {
	std::map<const std::string,oracle::occi::MetaData*> columnMetadata;
	//std::vector<std::string> columnNames;
	oracle::occi::Statement *statement = NULL; // create statement, when it's needed, otherwise connection will be closed by separateKeysFromUpdateableRows()
	unsigned long rowCount=0;
	unsigned long rowsUpdated=0;
	try {
		std::string tableToUpdate=update.tableName();
		//convert the table name to uppercase, as we are quoting it in the SQL.
		//In theory people might want to use a table with a mixed-case name, in which case this line
		//would have to be removed and also the upper() calls in getPrimaryKeyColumnsForTable would have to be removed.
		//however people don't usually use case-sensitive table names and it would be a hassle to make all the client code
		//do the uppercasing so for convenience just do it here. (Actually it should probably be done within SQLUpdate)
		xdata::Table *data=update.data;
		if (data) {
			rowCount=data->getRowCount();
			unsigned long currentRow;
			unsigned int addedValues=0;
			std::vector<std::string>::iterator columnIterator;
			std::set<std::string> keys;
			std::string columnsToUpdate;
			std::string whereClause;
			std::string sql="update "+quoteSchemaObjectName(tableToUpdate)+" set ";
			separateKeysFromUpdateableRows(update,columnsToUpdate,whereClause,keys,columnMetadata,false,addedValues);
			sql+=columnsToUpdate+whereClause;
			//std::cout << "SQL Update: "<< sql << std::endl;
			statement = createStatement();
			statement->setSQL(sql);
			statement->setMaxIterations(rowCount);
			std::vector<std::string> columns=data->getColumns();
			bool hasMimeColumns=false;
			for (currentRow=0;currentRow<rowCount;currentRow++) {
				int keyIndex=addedValues+1;
				int valueIndex=1;
				int parameterIndex;
				if (currentRow>0) statement->addIteration();
				for(columnIterator=columns.begin(); columnIterator!=columns.end(); columnIterator++) {
					std::string columnName(toolbox::toupper(*columnIterator));
					xdata::Serializable *value=data->getValueAt(currentRow,*columnIterator);
					std::string columnType=data->getColumnType(*columnIterator);
					if (keys.count(columnName)) parameterIndex=keyIndex++;
					else if (update.shouldChangeColumn(*columnIterator)/* && columnType!="mime"*/) {
						parameterIndex=valueIndex++;
						if (columnType=="mime") hasMimeColumns=true;
					}
					else continue; //this column is not used as a key nor should it be updated
					if (columnMetadata.count( columnName )) {
						setStatementParameter(statement,columnType,parameterIndex,columnMetadata[columnName],value,mappings,update.tableName());
					}
				}
			}
			deleteMetaData(columnMetadata);
			rowsUpdated=statement->executeUpdate();
			commitNeeded = true;
			//now update the blobs
			if (hasMimeColumns) {
				updateBlobs(update,mappings);
			}
		}
	} catch (oracle::occi::SQLException &e) {
		deleteMetaData(columnMetadata);
		terminateStatement(statement);
		//XCEPT_RAISE(tstore::exception::Exception, "Could not Update. Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not Update. Oracle returned error: "+std::string(e.what()));
	} catch (tstore::exception::Exception &e) {
		deleteMetaData(columnMetadata);
		terminateStatement(statement);
		XCEPT_RETHROW(tstore::exception::Exception, e.what(),e);
	}
	
	terminateStatement(statement);
	if (!update.allowNonUniqueRows()) {
		checkKeysMatched(rowCount,rowsUpdated,update.tableName(),"update");
	}
}

xdata::String *tstore::OracleConnection::readString(oracle::occi::ResultSet *rs,int columnIndex,const oracle::occi::MetaData &metadata,const MappingList &mappings,const std::string &tableName) 
	throw (oracle::occi::SQLException,xdata::exception::Exception) {
	xdata::String *value=NULL;
	unsigned int oracleType=metadata.getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);
	if (oracleType==oracle::occi::OCCI_SQLT_CLOB) {
		oracle::occi::Clob clob=rs->getClob(columnIndex);
		if (!clob.isNull() && clob.length()>0) {
			unsigned int length=clob.length();
			//std::cout << "length is " << length << std::endl;
			unsigned char *clobString=new unsigned char[length+1]; 
			unsigned int bytesRead=clob.read(length,clobString,length+1);
			clobString[bytesRead]='\0';
			std::stringstream stream;
 			stream << clobString;
			delete[] clobString;
			value=new xdata::String(stream.str());
		} else value=new xdata::String();
	} else if (oracleType==oracle::occi::OCCI_SQLT_INTERVAL_DS) {
		oracle::occi::IntervalDS interval=rs->getIntervalDS(columnIndex);
		if (!interval.isNull()) {
			value=new xdata::String(interval.toText(4,4));
		}
		else value=new xdata::String();
	} else if (oracleType==oracle::occi::OCCI_SQLT_INTERVAL_YM) {
		oracle::occi::IntervalYM interval=rs->getIntervalYM(columnIndex);
		if (!interval.isNull()) {
			value=new xdata::String(interval.toText(4));
		}
		else value=new xdata::String();
	}
	else {
		value=new xdata::String(rs->getString(columnIndex));
	}
	return value;
}

//creates an oracle::occi::Statement and binds the variables to it.
//either returns a valid Statement pointer, or throws an exception.
//you must terminate the statement when you're finished with it.
oracle::occi::Statement *tstore::OracleConnection::createParameterisedStatement(SQLStatement &query) throw (tstore::exception::Exception) {
	oracle::occi::Statement *statement = createStatement();
	try {
		unsigned int parameterCount=query.parameterCount();
		//std::cout << "statement: " << query.getProperty("statement") << std::endl;
		statement->setSQL(query.getProperty("statement"));
		for (unsigned int parameterIndex=0;parameterIndex<parameterCount;parameterIndex++) {
			xdata::Serializable *parameter=query.parameterAtIndex(parameterIndex);
			//std::cout << "setting parameter: " << parameter->toString() << " at index " << parameterIndex << std::endl;
			setStatementParameter(statement,parameter->type(),parameterIndex+1,parameter);
		}
	} catch (oracle::occi::SQLException &e) {
		terminateStatement(statement);
		//XCEPT_RAISE(tstore::exception::Exception,"Could not bind parameters to statement. Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception,"Could not bind parameters to statement. Oracle returned error: "+std::string(e.what()));
	}
	return statement;
}

//executes the query and ignores any results
void tstore::OracleConnection::execute(tstore::SQLStatement &s) throw (tstore::exception::Exception) {
	oracle::occi::Statement *statement=createParameterisedStatement(s);
	try {
		statement->executeUpdate();
		commitNeeded = true;
		terminateStatement(statement);
	} catch (oracle::occi::SQLException &e) {
		terminateStatement(statement);
		//XCEPT_RAISE(tstore::exception::Exception, "Could not run statement. Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not run statement. Oracle returned error: "+std::string(e.what()));
	}
}

oracle::occi::ResultSet *tstore::OracleConnection::runQuery(oracle::occi::Statement *statement) throw (tstore::exception::Exception) {
	std::string sqlText = "";
	try {
		//this statement either crashes or throws ORA-00932 or ORA-01007 if it is called after the definition of the table has been changed by dropping and recreating the table.
		//The problem seems to lie in some kind of caching in the connection rather than the environment or elsewhere, because a new connection can access the table with no
		//problems. I tried calling flushCache on the connection object after dropping and creating tables but it didn't help.
		//std::cout << "runQuery: " << std::hex << statement << std::endl;
		//std::cout << "runQuery: " << statement->getSQL() << std::endl;
		sqlText = statement->getSQL();
		return statement->executeQuery();
	} catch (oracle::occi::SQLException &e) {
		//XCEPT_RAISE(tstore::exception::Exception,"Could not run query. Oracle returned error: "+std::string(e.getMessage()));
		std::stringstream msg;
		msg << "Could not run query: '" << sqlText << "'. Oracle returned error:" << e.what();
		XCEPT_RAISE(tstore::exception::Exception, msg.str());
	}
}
namespace tstore {

template <class T>
T *getNaN(oracle::occi::ResultSet *rs,int columnIndex) throw (oracle::occi::SQLException) {
	//std::cout << rs->getString(columnIndex) << std::endl;
	if (rs->isNull(columnIndex)) {
		return new T(std::numeric_limits<T>::quiet_NaN());
	}
	return NULL;
}

oracle::occi::Number tstore::OracleConnection::numberFromString(const std::string &numberString) throw (oracle::occi::SQLException) {
	oracle::occi::Number number(0); //initialise to 0 because if it starts out NULL, then not even fromText will work.
	//because there is no way to use fromText without a format string (even though the Oracle function TO_NUMBER doesn't need it)
	//it is necessary to work backwards from the string to the format string.
	//you might think it would be easier to just use setString directly instead of using numbers, but that
	//can lead to the 'can not change number of iterations' exception that is sometimes thrown for reasons completely unrelated to the number of iterations
	std::string format=numberString;
	std::replace_if(format.begin(),format.end(),std::ptr_fun(isdigit),'9');
	if (format[0]=='-') format[0]='S';
	//std::cout << numberString << " with format " << format <<std::endl;
	number.fromText(environment_,numberString,format);
	return number;
}

template <class T>
T *tstore::OracleConnection::getNaNOrInfinity(oracle::occi::ResultSet *rs,int columnIndex) throw (oracle::occi::SQLException) {
	T *value=NULL;
	//std::cout << rs->getString(columnIndex) << std::endl;
	if (NULL==(value=getNaN<T>(rs,columnIndex))) {
		if (rs->getString(columnIndex).rfind("Inf")!=std::string::npos) { //this only works for binary_float and binary_double
			value=new T(std::numeric_limits<T>::infinity());
		} else if (std::numeric_limits<T>::is_integer && std::numeric_limits<T>::has_infinity) {
			oracle::occi::Number infinity=numberFromString(std::numeric_limits<T>::max().toString());
			//infinity.fromText(environment,std::numeric_limits<T>::max().toString(),"9999999999");
			++infinity;
			if (rs->getNumber(columnIndex)==infinity) { //numbers have no infinity in Oracle, so we store infinity as max()+1. Any higher numbers will cause an exception
				value=new T(std::numeric_limits<T>::infinity());
			}
		}
	}
	return value;
};

template<class xdataType>
xdataType *tstore::OracleConnection::getNumber(oracle::occi::ResultSet *rs,int columnIndex,const std::string &columnType,const oracle::occi::MetaData &metaData) throw (tstore::exception::Exception,oracle::occi::SQLException) {
	//oracle::occi::ResultSet::getNumber crashes without throwing an exception if the type is a timestamp, and probably for 
	//other non-numeric types too.
	//So if somebody has mapped such a type to a numeric xdata type, we'd better check that it's at least a numeric type before 
	//calling getNumber.
	xdataType *value;
	if (NULL==(value=getNaNOrInfinity<xdataType>(rs,columnIndex))) {
		std::string columnName=metaData.getString(oracle::occi::MetaData::ATTR_NAME);
		if (tstore::OracleConnection::isColumnNumeric(metaData)) {
			oracle::occi::Number number=rs->getNumber(columnIndex);
			std::string numberString=number.toText(environment_,"TM9");
			try {
				value=new xdataType();
				value->fromString(numberString);
				//value->fromString(rs->getString(columnIndex));
			} catch (xcept::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,"The value "+numberString+" in column "+columnName+" can not be converted to an "+columnType+". You may need to change your data type mappings.",e);
			}
		} else {
			XCEPT_RAISE(tstore::exception::Exception, "Column "+columnName+" can not be converted to an "+columnType+". You may need to change your data type mappings.");
		}
	}
	return value;
}
/*
//reduce reallocation
template <class xdataType>
bool tstore::OracleConnection::getNaNOrInfinity(oracle::occi::ResultSet *rs,int columnIndex,xdataType *value) throw (oracle::occi::SQLException) {
	//std::cout << rs->getString(columnIndex) << std::endl;
	if (NULL==(value=getNaN<xdataType>(rs,columnIndex))) {
		if (rs->getString(columnIndex).rfind("Inf")!=std::string::npos) { //this only works for binary_float and binary_double
			*value=std::numeric_limits<xdataType>::infinity();
			return true;
		} else if (std::numeric_limits<xdataType>::is_integer && std::numeric_limits<xdataType>::has_infinity) {
			oracle::occi::Number infinity=numberFromString(std::numeric_limits<xdataType>::max().toString());
			//infinity.fromText(environment,std::numeric_limits<T>::max().toString(),"9999999999");
			++infinity;
			if (rs->getNumber(columnIndex)==infinity) { //numbers have no infinity in Oracle, so we store infinity as max()+1. Any higher numbers will cause an exception
				*value=std::numeric_limits<xdataType>::infinity();
				return true;
			}
		}
	}
	return false;
};

template<class xdataType>
void tstore::OracleConnection::getNumber(oracle::occi::ResultSet *rs,int columnIndex,const std::string &columnType,const oracle::occi::MetaData &metaData,xdataType *value) throw (tstore::exception::Exception,oracle::occi::SQLException) {
	//oracle::occi::ResultSet::getNumber crashes without throwing an exception if the type is a timestamp, and probably for 
	//other non-numeric types too.
	//So if somebody has mapped such a type to a numeric xdata type, we'd better check that it's at least a numeric type before 
	//calling getNumber.
	if (!getNaNOrInfinity(rs,columnIndex,value)) {
		std::string columnName=metaData.getString(oracle::occi::MetaData::ATTR_NAME);
		if (tstore::OracleConnection::isColumnNumeric(metaData)) {
			oracle::occi::Number number=rs->getNumber(columnIndex);
			std::string numberString=number.toText(environment_,"TM9"); //number is converted to text and back because this way we can find out whether it fits in the xdata type.
			try {
				// *value=rs->getInt(columnIndex); //this is a bit faster, but I'd have to have separate code for each xdata type and catch an occi exception in case of overflow
				value->fromString(numberString);
				//value->fromString(rs->getString(columnIndex));
			} catch (xcept::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,"The value \"+numberString+\" in column "+columnName+" can not be converted to an "+columnType+". You may need to change your data type mappings.",e);
			}
		} else {
			XCEPT_RAISE(tstore::exception::Exception, "Column "+columnName+" can not be converted to an "+columnType+". You may need to change your data type mappings.");
		}
	}
}
*/
} //end of namespace "tstore"

xdata::TimeVal *tstore::OracleConnection::readTimestamp(oracle::occi::ResultSet *rs,int columnIndex,int oracleType) throw (oracle::occi::SQLException,xdata::exception::Exception) {
	//this needs to adjust for timezone
	oracle::occi::Timestamp timestamp=rs->getTimestamp(columnIndex);
	if (timestamp.isNull()) {
		return new xdata::TimeVal(std::numeric_limits<xdata::TimeVal>::quiet_NaN());
	} else {
		toolbox::TimeVal timeValue;
		if (oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP_TZ || oracleType==oracle::occi::OCCI_SQLT_TIMESTAMP_LTZ) {
			int hour,minute;
			timestamp.getTimeZoneOffset(hour,minute);
			//std::cout << "timezone offset=" << hour << ":" << minute << std::endl;
			oracle::occi::IntervalDS offset(environment_,0,hour,minute);
			timestamp=timestamp.intervalSub(offset);
			timestamp.setTimeZoneOffset(0,0);
		}
		std::string timeString(timestamp.toText("YYYY-MM-DD HH24:MI:SS.FF6",0));
		std::string::size_type location=timeString.find(' ');
		if (location!=std::string::npos) timeString.replace(location,1,"T");
		
		timeValue.fromString (timeString,/*"%Y/%m/%d %H:%M:%S"*/"",toolbox::TimeVal::gmt);
		return new xdata::TimeVal(timeValue);
	}
}

xdata::TimeVal *tstore::OracleConnection::readDate(oracle::occi::ResultSet *rs,int columnIndex) throw (oracle::occi::SQLException,xdata::exception::Exception) {
	oracle::occi::Date date=rs->getDate(columnIndex);
	if (date.isNull()) {
		return new xdata::TimeVal(std::numeric_limits<xdata::TimeVal>::quiet_NaN());
	} else {
		toolbox::TimeVal timeValue;
		std::string dateString(date.toText("YYYY-MM-DD HH24:MI:SS"));
		//std::cout << "read date: " << dateString << std::endl;
		std::string::size_type location=dateString.find(' ');
		if (location!=std::string::npos) dateString.replace(location,1,"T");
		dateString+=".000000";
		//fromString uses all its own code instead of strptime when the format argument is blank
		//surprisingly this way is a lot easier to get to work.
		timeValue.fromString (dateString,/*"%Y/%m/%d %H:%M:%S"*/"",toolbox::TimeVal::gmt);
		
		return new xdata::TimeVal(timeValue);
	}
}

//this is to avoid allocating and deleting an object each time around the loop
//it seems like it would be much faster but in fact only saved about half a second
//out of 24 before any other optimisation
//(which is probably within the uncertainty)
//for a dataset of 45056 rows of 8 integers
//so for now it has not been done.
//To get this working, we would need to:
//	make sure that all of the readX methods took an existing pointer instead of allocating one
//	uncomment all of the code marked with //reduce reallocation
//	test like crazy
/*
xdata::Serializable *tstore::OracleConnection::newSerializableOfType(const std::string &columnType) {
	if (columnType=="string") {
		return new xdata::String;
	}
	else if (columnType=="time") {
		return new xdata::TimeVal;
	} 
	else if (columnType=="float") {
		return new xdata::Float;
	}
	else if (columnType=="double") {
		return new xdata::Double;
	}
	else if (columnType=="int") {
		return new xdata::Integer;
	} else if (columnType=="unsigned long") {
		return new xdata::UnsignedLong;
	} else if (columnType=="unsigned int") {
		return new xdata::UnsignedInteger;
	} else if (columnType=="unsigned int 32") {
		return new xdata::UnsignedInteger32;
	} else if (columnType=="unsigned int 64") {
		return new xdata::UnsignedInteger64;
	} else if (columnType=="unsigned short") {
		return new xdata::UnsignedShort;
	} 
	else if (columnType=="bool") {
		return new xdata::Boolean;
	} else if (columnType=="mime") {
		return new xdata::Mime();
	}
	return NULL;
}

//use this to delete the buffers allocated by newSerializableOfType
//reduce reallocation
void tstore::OracleConnection::deleteValueBuffers(std::vector<xdata::Serializable *> &currentRow) {
	for (std::vector<xdata::Serializable *>::iterator valueBuffer=currentRow.begin();valueBuffer!=currentRow.end();++valueBuffer) {
		if (*valueBuffer) delete *valueBuffer;
	}
}*/

template<class xdataType,class oracleType>
typename xdata::Vector<xdataType> *tstore::OracleConnection::getBinaryVector(oracle::occi::ResultSet *rs,int columnIndex) throw (tstore::exception::Exception,oracle::occi::SQLException) {
	xdata::Vector<xdataType> *xdataVector=new xdata::Vector<xdataType>;
	std::vector<oracleType> values;
	getVector(rs,columnIndex,values);
	typename std::vector<oracleType>::iterator value;
	for (value=values.begin();value!=values.end();++value) {
		xdataType xdataValue=std::numeric_limits<xdataType>::quiet_NaN();
		if (!(*value).isNull) xdataValue=(*value).value;
		xdataVector->push_back(xdataType((*value).value));					
	}
	return xdataVector;
}

xdata::Serializable* tstore::OracleConnection::blobToXdataValue(const oracle::occi::Blob& blob) {
	size_t blobSize = blob.length();
	char* buffer = NULL;
	xdata::Serializable* value = NULL;

	try {
		buffer = new char[blobSize];
		blob.read(blobSize, (unsigned char*) buffer, blobSize);
		xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buffer, blobSize);
		xdata::exdr::Serializer serializer;

		uint32_t ctype;
		inBuffer.decodeUInt32(ctype); // detect type of value

		if (ctype == xdata::exdr::Serializer::Mime) {
			value = new xdata::Mime();
		}
		else if (ctype == xdata::exdr::Serializer::VectorInteger) {
			value = new xdata::Vector<xdata::Integer>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorInteger32) {
			value = new xdata::Vector<xdata::Integer32>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorInteger64) {
			value = new xdata::Vector<xdata::Integer64>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorUnsignedInteger) {
			value = new xdata::Vector<xdata::UnsignedInteger>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorUnsignedInteger32) {
			value = new xdata::Vector<xdata::UnsignedInteger32>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorUnsignedInteger64) {
			value = new xdata::Vector<xdata::UnsignedInteger64>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorUnsignedShort) {
			value = new xdata::Vector<xdata::UnsignedShort>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorFloat) {
			value = new xdata::Vector<xdata::Float>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorDouble) {
			value = new xdata::Vector<xdata::Double>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorString) {
			value = new xdata::Vector<xdata::String>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorBoolean) {
			value = new xdata::Vector<xdata::Boolean>();
		}
		else if (ctype == xdata::exdr::Serializer::VectorTimeVal) {
			value = new xdata::Vector<xdata::TimeVal>();
		}

		serializer.import(value, &inBuffer);
		delete buffer;
	}
	catch (xdata::exception::Exception &e) {
		delete buffer;
		XCEPT_RETHROW(tstore::exception::Exception,"Could not copy blob into xdata::Serializable",e);
	}

	return value;
}

//Pascale suggested having an object for each column which would read the value in the appropriate way (and also contain
// the pointer to the value and take care of releasing it)
//This would make the code tidier, but it would take quite a bit of work and in fact does not
//speed up the execution very much (0.4s of 24)
void tstore::OracleConnection::copyResultsToTable(oracle::occi::ResultSet *rs,xdata::Table &results,const MappingList &columnTypes,const std::string &tableName) throw (tstore::exception::Exception) {
	//std::vector<xdata::Serializable *> currentRow;//reduce reallocation
	try {
		std::vector<oracle::occi::MetaData> columnMetadata=rs->getColumnListMetaData();
		int columnIndex=0;
		unsigned long rowIndex=0;
		std::vector<oracle::occi::MetaData>::iterator columnIterator;
		
		results.clear();
		//results.reserve(45060); //this makes no difference to performance
		for(columnIterator=columnMetadata.begin(); columnIterator!=columnMetadata.end(); columnIterator++) {
			std::string name=(*columnIterator).getString(oracle::occi::MetaData::ATTR_NAME);
			//std::string columnType="int"; //added to test how much time was taken up by checking the column type properly. It only decreased the time by about 0.4 seconds out of 24, which is smaller than the fluctuations in measurements.
			std::string columnType=OracleConnection::xdataTypeForColumn(*columnIterator,columnTypes,tableName);
			results.addColumn(name,columnType);
			//currentRow.push_back(newSerializableOfType(columnType)); //reduce reallocation
		}
		while (rs->next()) {
			columnIndex=0;
			for(columnIterator=columnMetadata.begin(); columnIterator!=columnMetadata.end(); columnIterator++) {
				std::string columnName=(*columnIterator).getString(oracle::occi::MetaData::ATTR_NAME);
				std::string columnType=results.getColumnType(columnName);
				xdata::Serializable *value=NULL;//currentRow[columnIndex];//reduce reallocation
				oracle::occi::Number number;

				try {
					columnIndex++; //I suppose there's not much point in an iterator if we have to keep count anyway...
					if (columnType=="string") {
						value=readString(rs,columnIndex,columnMetadata[columnIndex-1],columnTypes,"");
					}
					else if (columnType=="time") {
						unsigned int oracleType=(*columnIterator).getInt(oracle::occi::MetaData::ATTR_DATA_TYPE);
						if (isTimestampType(oracleType)) {
							value=readTimestamp(rs,columnIndex,oracleType);
						} else if (oracleType==oracle::occi::OCCI_SQLT_DAT) {
							value=readDate(rs,columnIndex);
						}
					} 
					else if (columnType=="float") {
						if (NULL==(value=getNaNOrInfinity<xdata::Float>(rs,columnIndex))) {
							value=new xdata::Float(rs->getFloat(columnIndex));
						}
					}
					else if (columnType=="double") {
						if (NULL==(value=getNaNOrInfinity<xdata::Double>(rs,columnIndex))) {
							value=new xdata::Double(rs->getDouble(columnIndex));
						}
					}
					else if (columnType=="int") {
						value=getNumber<xdata::Integer>(rs,columnIndex,columnType,*columnIterator);
						//reduce reallocation
						//getNumber(rs,columnIndex,columnType,*columnIterator,static_cast<xdata::Integer *>(value));
					} else if (columnType=="unsigned long") {
						value=getNumber<xdata::UnsignedLong>(rs,columnIndex,columnType,*columnIterator);
					} else if (columnType=="unsigned int") {
						value=getNumber<xdata::UnsignedInteger>(rs,columnIndex,columnType,*columnIterator);
					} else if (columnType=="unsigned int 32") {
						value=getNumber<xdata::UnsignedInteger32>(rs,columnIndex,columnType,*columnIterator);
					} else if (columnType=="unsigned int 64") {
						value=getNumber<xdata::UnsignedInteger64>(rs,columnIndex,columnType,*columnIterator);
					} else if (columnType=="unsigned short") {
						value=getNumber<xdata::UnsignedShort>(rs,columnIndex,columnType,*columnIterator);
					} 
					else if (columnType=="bool") {
						if (NULL==(value=getNaNOrInfinity<xdata::Boolean>(rs,columnIndex))) {
							value=new xdata::Boolean(rs->getInt(columnIndex));
						}
					} else if (columnType=="mime" || toolbox::startsWith(columnType, "vector ")) {
						oracle::occi::Blob blob = rs->getBlob(columnIndex);
						value = blobToXdataValue(blob);

						if (value) {
							std::string decodedType = value->type();
							if (decodedType  == "vector") {
								decodedType = "vector " + dynamic_cast<xdata::AbstractVector*>(value)->getElementType();
							}

							if (columnType != decodedType) {
								XCEPT_RAISE(tstore::exception::Exception,
									"Column '" + columnName + "' type '" + columnType +
									"' does not match decoded type '" + decodedType +
									"', or a view file does not have column mappings.");
							}
						}
					}
				} catch (oracle::occi::SQLException &e) { //getFloat/getDouble etc. failed
					std::ostringstream error;
					error << "Could not read row " << rowIndex << " of column " << columnName << " into a " << columnType << ". ";
					error << "You may need to change your data type mappings. ";
					//error << "Oracle returned error "+std::string(e.getMessage());
					error << "Oracle returned error "+std::string(e.what());
					XCEPT_RAISE(tstore::exception::Exception, error.str());
				} catch (xdata::exception::Exception &e) { //successfully got the result, but it couldn't be turned into an xdata object (due to extra restrictions on the xdata object... though I can't think of any examples)
					std::ostringstream error;
					error << "Could not read row " << rowIndex << " of column " << columnName << " into a " << columnType << ". ";
					error << "You may need to change your data type mappings. ";
					XCEPT_RETHROW(tstore::exception::Exception, error.str(),e);
				}
				
				if (!value) {
					XCEPT_RAISE(tstore::exception::Exception,"Could not read value for column "+columnName+" into a "+columnType);
				}
				else if (columnType!=value->type() && value->type()!="vector") { //make sure these strings I've hard-coded actually do correspond to the data types used. This is not the case for vectors since all vectors are of type "vector" but the column type specifies what kind of vector it is.
					std::string error="Internal error: Invalid type string: "+value->type()+" instead of "+columnType+" for column "+columnName;
					delete value;						
					XCEPT_RAISE(tstore::exception::Exception,error); //this should be its own exception type	
				}
				try {
					results.setValueAt(rowIndex,(*columnIterator).getString(oracle::occi::MetaData::ATTR_NAME),*value);
					delete value;
				} catch (xdata::exception::Exception e) {
					if (value) delete value;
					//deleteValueBuffers(currentRow);//reduce reallocation
					std::ostringstream error;
					error << "Could not insert row " << rowIndex << " of column " << columnName << " into the xdata::Table." << std::string(e.what());
					XCEPT_RETHROW(tstore::exception::Exception, error.str(),e);
				}				
			}
			rowIndex++;
		}
		//deleteValueBuffers(currentRow);//reduce reallocation
	} catch (oracle::occi::SQLException &e) {
		//deleteValueBuffers(currentRow);//reduce reallocation
		//XCEPT_RAISE(tstore::exception::Exception, "Could not get query results. Oracle returned error "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not get query results. Oracle returned error "+std::string(e.what()));
	}
}

//executes the query and puts the resulting rows into results
//raises an exception if the SQL is invalid, not a query, or if there is some other Oracle error (perhaps it should be more picky about that)
//the connection must be open (which it will be if it was created or reset with valid login details)
void tstore::OracleConnection::execute(tstore::SQLQuery &query,xdata::Table &results,const MappingList &columnTypes) throw (tstore::exception::Exception) {
	oracle::occi::Statement *statement = createParameterisedStatement(query); //this must stay around until we have finished using everything to do with it
	//setting this to a large number speeds things up dramatically when there are many rows,
	//but if there are many small queries it can cause TStore to use up all available memory.
	//ideally we'd set the prefetchMemorySize instead, to guard against problems with large rows,
	//then this could be the same for all tables. But I have no idea what to use as a default.
	statement->setPrefetchRowCount(expectedRowCount_); 
	//std::cout << "statement->getPrefetchMemorySize()=" << statement->getPrefetchMemorySize() << std::endl;
	oracle::occi::ResultSet* resultset = NULL;
	try {
		//toolbox::TimeVal queryStart = toolbox::TimeVal::gettimeofday();
		resultset=runQuery(statement);
		//toolbox::TimeVal queryStop = toolbox::TimeVal::gettimeofday();
		
		//toolbox::TimeVal copyStart = toolbox::TimeVal::gettimeofday();
		copyResultsToTable(resultset,results,columnTypes,query.tableName());

		/*toolbox::TimeVal copyStop = toolbox::TimeVal::gettimeofday();
		
		toolbox::TimeInterval queryT = queryStop - queryStart;
		toolbox::TimeInterval copyT  = copyStop  - copyStart;
		toolbox::TimeInterval totalT = copyStop  - queryStart;
		std::ofstream out("/tmp/tstore_profiling.txt",std::ios_base::app);
		out << "\n\n\n----------- Profiling measurements:\n"
		    << "-- numberOfRows: "       << results.getRowCount() << "\n"
		    << "-- runQuery: "           <<   ((float)queryT.sec() + ((float)queryT.millisec())*.001) << "\n"
		    << "-- copyResultsToTable: " <<   ((float)copyT.sec() + ((float)copyT.millisec())*.001) << "\n"
		    << "-- total: "              <<   ((float)totalT.sec() + ((float)totalT.millisec())*.001)
		    << std::endl;
		out.close();*/
	} catch (tstore::exception::Exception) {
		if (resultset) statement->closeResultSet(resultset);
		terminateStatement(statement);
		throw;
	}
	if (resultset) statement->closeResultSet(resultset);
	terminateStatement(statement);
}


void tstore::OracleConnection::execute(tstore::SQLQuery &query,xdata::Table &results) throw (tstore::exception::Exception) {
	MappingList columnTypes;
	execute(query,results,columnTypes);
}

//returns a valid, open connection, or throws an exception
oracle::occi::Connection *tstore::OracleConnection::connection() throw (tstore::exception::Exception) {
	if (!connection_) XCEPT_RAISE(tstore::exception::Exception, "not connected");
	return connection_;
}

void tstore::OracleConnection::commit() throw (tstore::exception::Exception) {
	try {
		connection()->commit();
	} catch (oracle::occi::SQLException &e) {
		//XCEPT_RAISE(tstore::exception::Exception, "Could not commit the transaction. Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not commit the transaction. Oracle returned error: "+std::string(e.what()));
	}
}

void tstore::OracleConnection::rollback() throw (tstore::exception::Exception) {
	try {
		connection()->rollback();
	} catch (oracle::occi::SQLException &e) {
		//XCEPT_RAISE(tstore::exception::Exception, "Could not roll back the transaction. Oracle returned error: "+std::string(e.getMessage()));
		XCEPT_RAISE(tstore::exception::Exception, "Could not roll back the transaction. Oracle returned error: "+std::string(e.what()));
	}
} 

void tstore::OracleConnection::test() {
}

void tstore::OracleConnection::getForeignKeys(xdata::Table &results) throw (tstore::exception::Exception) { //gets all the foreign key constraints
	std::string queryString="SELECT "
			"CO.table_name \""+tstore::childField+"\", "
			"CC.table_name \""+tstore::parentField+"\", "
			"CC.column_name \""+tstore::parentColumnField+"\", "
			"CCP.column_name \""+tstore::childColumnField+"\", "
			"CO.constraint_name \""+tstore::constraintField+"\" "
		"FROM USER_CONSTRAINTS CO, USER_CONS_COLUMNS CC, USER_CONS_COLUMNS CCP "
		"WHERE CO.r_constraint_name = CC.constraint_name " //cc contains information about the child table
		"and co.constraint_name=ccp.constraint_name "	//ccp about the parent table
		"and cc.position=ccp.position " //when there is a composite foreign key, make sure the parent columns match with the right child columns
		//filter out backup tables here. This way we can leave the constraints in place (allowing easy reverting to backups)
		//without having them show up in nested queries. Perhaps in the interests of performance it would be better to remove the constraints when the tables
		//are backed up, though.
		"and not REGEXP_LIKE(ccp.table_name,'.*"+backupVersionPattern+"') " //filter out backed up versions of tables (this has to be done in here since the names of backup tables is determined by this class. Perhaps in future backup table names will be determined by TStore)
		"and not REGEXP_LIKE(cc.table_name,'.*"+backupVersionPattern+"') " //filter out backed up versions of tables (this has to be done in here since the names of backup tables is determined by this class. Perhaps in future backup table names will be determined by TStore)
		"ORDER BY CO.r_constraint_name,CO.constraint_name";
	//std::cout << "queryString=" << queryString << std::endl;
	tstore::SQLQuery query(queryString);
	execute(query,results);
}

void tstore::OracleConnection::getForeignKeys(const std::string &tableName,xdata::Table &results) throw (tstore::exception::Exception) { //gets all the foreign key constraints
	std::string queryString="SELECT "
			//"CO.table_name \""+tstore::childField+"\", "
			"CC.table_name \""+tstore::parentField+"\", "
			"CC.column_name \""+tstore::parentColumnField+"\", "
			"CCP.column_name \""+tstore::childColumnField+"\", "
			"CO.constraint_name \""+tstore::constraintField+"\" "
		"FROM USER_CONSTRAINTS CO, USER_CONS_COLUMNS CC, USER_CONS_COLUMNS CCP "
		"where ccp.table_name=:tablename "
		"and CO.r_constraint_name = CC.constraint_name " //cc contains information about the child table
		"and co.constraint_name=ccp.constraint_name "	//ccp about the parent table
		"and cc.position=ccp.position " //when there is a composite foreign key, make sure the parent columns match with the right child columns
		"and not REGEXP_LIKE(cc.table_name,'.*"+backupVersionPattern+"') " //filter out backed up versions of tables (this has to be done in here since the names of backup tables is determined by this class. Perhaps in future backup table names will be determined by TStore)
		"ORDER BY CO.r_constraint_name,CO.constraint_name";
	//std::cout << queryString << std::endl;
	tstore::SQLQuery query(queryString);
	query.setParameterAtIndex(0,tableName);
	execute(query,results);
}

void tstore::OracleConnection::addForeignKey
(
	const std::string &tableName,
	const std::string &referencedTableName,
	const std::map<std::string,std::string, xdata::Table::ci_less> &columns
) 
throw (tstore::exception::Exception) 
{
	std::string key;
	std::string columnsString;
	std::string referencedColumnsString;
	for (std::map<std::string,std::string>::const_iterator columnPair=columns.begin();columnPair!=columns.end();++columnPair) {
		if (!columnsString.empty()) {
			columnsString+=",";
			referencedColumnsString+=",";
		}
		columnsString+=(*columnPair).first;
		referencedColumnsString+=(*columnPair).second;
	}
	std::string addForeignKey("alter table "+tableName+" add foreign key ("+columnsString+") references "+referencedTableName+" ("+referencedColumnsString+") on delete cascade");
	//std::cout << addForeignKey << std::endl;
	SQLStatement statement(addForeignKey);
	execute(statement);
}
