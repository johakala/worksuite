/*
 * GlobalStatelessConnectionPool.cc
 *
 *  Created on: Jul 15, 2010
 *      Author: janulis
 */

#include "tstore/GlobalStatelessConnectionPool.h"
#include "toolbox/string.h"
#include "tstore/exception/Exception.h"

tstore::GlobalStatelessConnectionPool::GlobalStatelessConnectionPool(
	unsigned int minConn, unsigned int maxConn, unsigned int incrConn) :
		GlobalGeneralConnectionPool(minConn, maxConn, incrConn) {
}

tstore::GlobalStatelessConnectionPool::~GlobalStatelessConnectionPool() {
	for(StatelessConnPoolMap::iterator it = connPools.begin(); it != connPools.end(); it++)
		try {
			if (it->second) {
				env->terminateStatelessConnectionPool(it->second, oracle::occi::StatelessConnectionPool::SPD_FORCE);
				delete it->second;
			}
		} catch(std::exception& e) {
			std::cout << e.what();
		}

	connPools.clear();
}

oracle::occi::Connection* tstore::GlobalStatelessConnectionPool::getConnection(const std::string& user, const std::string& pass, const std::string& db, const std::string &tag) throw (tstore::exception::Exception) {
	oracle::occi::StatelessConnectionPool* connPool = connPools[db];

	try {
		if (!connPool) {
			//std::cout << "CREATED NEW CONN POOL FOR DB: " << db << "\n";

			connPools[db] = env->createStatelessConnectionPool(user, pass, db,
				maxConn, minConn, incrConn, oracle::occi::StatelessConnectionPool::HETEROGENEOUS);

			connPool = connPools[db];
			connPool->setBusyOption(oracle::occi::StatelessConnectionPool::FORCEGET);
			connPool->setTimeOut(3600);
		}

		return connPool->getConnection(user, pass, tag);
	} catch(oracle::occi::SQLException& e) {
    			std::stringstream msg;
                        msg << "Oracle returned error: " << e.what();
                        XCEPT_RAISE(tstore::exception::Exception, msg.str());

	}

	return NULL;
}

void tstore::GlobalStatelessConnectionPool::releaseConnection(oracle::occi::Connection* conn, const std::string& db, const std::string &tag) throw (tstore::exception::Exception) {
	oracle::occi::StatelessConnectionPool* connPool = connPools[db];

	try {
		if (connPool && conn) connPool->releaseConnection(conn, tag);
	} catch(oracle::occi::SQLException& e) {
    			std::stringstream msg;
                        msg << "Oracle returned error: " << e.what();
                        XCEPT_RAISE(tstore::exception::Exception, msg.str());

	}
}

void tstore::GlobalStatelessConnectionPool::terminateConnection(oracle::occi::Connection* conn, const std::string& db) throw (tstore::exception::Exception) {
	oracle::occi::StatelessConnectionPool* connPool = connPools[db];

	try {
		if (connPool && conn) connPool->terminateConnection(conn);
	} catch(oracle::occi::SQLException& e) {
		    std::stringstream msg;
                        msg << "Oracle returned error: " << e.what();
                        XCEPT_RAISE(tstore::exception::Exception, msg.str());

	}
}

unsigned int tstore::GlobalStatelessConnectionPool::getBusyConnections(const std::string& db) {
	oracle::occi::StatelessConnectionPool* connPool = connPools[db];
	return (connPool) ? connPool->getBusyConnections() : 0;
}

unsigned int tstore::GlobalStatelessConnectionPool::getOpenConnections(const std::string& db) {
	oracle::occi::StatelessConnectionPool* connPool = connPools[db];
	return (connPool) ? connPool->getOpenConnections() : 0;
}

tstore::StatelessConnPoolMap::const_iterator tstore::GlobalStatelessConnectionPool::poolsBegin() {
	return connPools.begin();
}

tstore::StatelessConnPoolMap::const_iterator tstore::GlobalStatelessConnectionPool::poolsEnd() {
	return connPools.end();
}

void tstore::GlobalStatelessConnectionPool::printStats() throw (tstore::exception::Exception) {
	std::cout << "======================================\n";

	for(StatelessConnPoolMap::const_iterator it = connPools.begin(); it != connPools.end(); it++) {
		try {
			std::string db = it->first;
			oracle::occi::StatelessConnectionPool* connPool = connPools[db];
			std::cout << "db: " << db << " statelessConnPool: " << connPool << "\n";

			if (connPool) {
				std::cout << "Database " << db << " connection info:\n"
					 << "Busy: " << connPool->getBusyConnections() << " "
					 << "Open: " << connPool->getOpenConnections() << "\n\n";
			}
		} catch(oracle::occi::SQLException& e) {
			std::stringstream msg;
			msg << "Oracle returned error: " << e.what();
			XCEPT_RAISE(tstore::exception::Exception, msg.str());
		}
	}
}
