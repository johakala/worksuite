// $Id: version.h,v 1.12 2009/05/14 13:35:20 brett Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstoreutils_version_h_
#define _tstoreutils_version_h_

#include "config/PackageInfo.h"

#define TSTOREUTILS_VERSION_MAJOR 3
#define TSTOREUTILS_VERSION_MINOR 1
#define TSTOREUTILS_VERSION_PATCH 2
// If any previous versions available E.g. #define TSTOREUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define TSTOREUTILS_PREVIOUS_VERSIONS "3.1.0,3.1.1"

//
#define TSTOREUTILS_VERSION_CODE PACKAGE_VERSION_CODE(TSTOREUTILS_VERSION_MAJOR,TSTOREUTILS_VERSION_MINOR,TSTOREUTILS_VERSION_PATCH)
#ifndef TSTOREUTILS_PREVIOUS_VERSIONS
#define TSTOREUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TSTOREUTILS_VERSION_MAJOR,TSTOREUTILS_VERSION_MINOR,TSTOREUTILS_VERSION_PATCH)
#else 
#define TSTOREUTILS_FULL_VERSION_LIST  TSTOREUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TSTOREUTILS_VERSION_MAJOR,TSTOREUTILS_VERSION_MINOR,TSTOREUTILS_VERSION_PATCH)
#endif 

namespace tstoreutils
{
	const std::string package  =  "tstoreutils";
	const std::string versions =  TSTOREUTILS_FULL_VERSION_LIST;
	const std::string description = "Table store utility library for use with Oracle RDBMS";
	const std::string authors = "Angela Brett";
	const std::string summary = "Table store utility library for use with Oracle RDBMS";	
	const std::string link = "http://xdaqwiki.cern.ch/index.php/TStore";
	
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
