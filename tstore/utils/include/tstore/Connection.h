// $Id: Connection.h,v 1.4 2009/05/11 15:51:33 brett Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
/* AbstractDBConnection
Extends toolbox::Properties To be able to set Connection parameters Easily.
The DBConnection should Also implement a ‘reset’ Method to easily re-create A connection with modified
Parameters. Such we can also easily Create automatic Web User interfaces.*/

#ifndef _tstore_Connection_h_
#define _tstore_Connection_h_

#include "toolbox/Properties.h"

#include "tstore/SQLQuery.h"
#include "tstore/SQLUpdate.h"
#include "tstore/SQLInsert.h"
#include "tstore/SQLDelete.h"
#include "Mapping.h"

namespace tstore {

const std::string userKey="username";
const std::string passwordKey="password";
const std::string databaseKey="dbname";

//these are the column names in the table returned from getForeignKeys
const std::string parentField="parent";
const std::string childField="child";
const std::string childColumnField="child column";
const std::string parentColumnField="parent column";
const std::string constraintField="constraint name"; //this is to make it easy to tell one constraint from another, even when there are two constraints between the same two tables

class Connection: public toolbox::Properties {

	public:
	
	virtual ~Connection() {}
	Connection() throw (tstore::exception::Exception) {} 
	Connection(std::string user, std::string passwd, std::string db) throw (tstore::exception::Exception) {
    	this->setProperty(userKey,user);
		this->setProperty(passwordKey,passwd);
		this->setProperty(databaseKey,db);
	}
	
	//executes the query, adds the appropriate columns to results (with column types determined by mappings, or the default
	//column types) and puts the resulting rows into results
	//raises an exception if the SQL is invalid, not a query, or if there is some other database error
	//the connection must be open (which it will be if you got it from a TStoreAPI)
	virtual void execute(tstore::SQLQuery &query,xdata::Table &results,const tstore::MappingList &mappings) throw (tstore::exception::Exception)=0;

	//same as above, but with no type mappings
	virtual void execute(tstore::SQLQuery &query,xdata::Table &results) throw (tstore::exception::Exception)=0;
	
	//updates the table based on the data in the update object.
	//only columns allowed by the update object are updated,
	//but columns in the data which form part of the primary key are also used, to identify rows.
	//Throws an exception if the connection is not open
	//Throws an exception if not all the primary key columns are in the data, and the update does not allow non-unique rows
	//Throws an exception if some of the rows could not be updated (probably because the primary key has been changed from what is in the database)
	//Throws an exception if there is a database error
	//(I need to define different types for these exceptions!)
	virtual void execute(tstore::SQLUpdate &update) throw (tstore::exception::Exception)=0;
	//for updates and inserts, mappings are only used for formats (e.g. for timestamps)
	virtual void execute(tstore::SQLUpdate &insert,const tstore::MappingList &columnTypes) throw (tstore::exception::Exception)=0;
	
	//inserts the data in \p insert into the database table specified by \p insert
	//throws an exception if there is a database error.
	virtual void execute(tstore::SQLInsert &update) throw (tstore::exception::Exception)=0;
	virtual void execute(tstore::SQLInsert &update,const tstore::MappingList &columnTypes) throw (tstore::exception::Exception)=0;
	
	virtual void execute(tstore::SQLStatement &statement) throw (tstore::exception::Exception)=0;
	virtual void execute(tstore::SQLDelete &d) throw (tstore::exception::Exception)=0;
	virtual void createTable(std::string name,std::string primaryKey,xdata::Table &definition) throw (tstore::exception::Exception)=0;
	virtual void dropTable(std::string name) throw (tstore::exception::Exception)=0;
	
	//commits the transaction. You should call this after calling execute if you want to keep the changes.
	virtual void commit() throw (tstore::exception::Exception)=0;
	
	//rolls back the transaction.
	virtual void rollback() throw (tstore::exception::Exception)=0;
	//attempts to roll back the transaction but may fail silently (safe to call from an exception handler)
	void attemptRollback() {
		try {
			rollback();
		} catch (tstore::exception::Exception) {
			;
		}
	}
	
	//adds entries to \p map where they keys are names of the columns in the database table \p tableName,
	//and the values are strings representing the xdata types of the columns (according to \p mappings or the default types.)
	virtual void getColumnTypes(std::string tableName,std::map<const std::string,std::string> &types,const MappingList &mappings)=0; //fills type with the names and xdata types of columns of the specified table
	
	//returns whether the primary key(s) in the table \a tableName match those in \a keys (in the right order)
	virtual bool keysMatch(const std::string &tableName,const std::string &keys) throw (tstore::exception::Exception)=0;

	//gets the names of all tables whose names match \a pattern (I have not decided yet whether \a pattern should be a regexp or something else. For now it is a LIKE condition)
	//ignores tables with a version number (the format of a version number is determined by backupTable)
	virtual void getTablesMatchingPattern(std::set<std::string> &tableNames,const std::string &pattern) throw (tstore::exception::Exception)=0;
	//these should not be used by a view
	virtual void openConnection() throw (tstore::exception::Exception)=0;
	virtual void closeConnection()=0;
  	virtual bool isConnected()=0;
	//experimental, creates a database table with appropriate columns for definition
	//virtual void createTable(std::string name,xdata::Table &definition)=0;
	
	//gets the definition of \a tableNameas deduced from the column types and constraints in the database.
	virtual void getTableDefinition(const std::string &tableName,xdata::Table &definition) throw (tstore::exception::Exception)=0;
	virtual void reset() {
		closeConnection();
		openConnection();
	};
	
	//returns whether the two xdata types given map to identical types in the database
	virtual bool typesAreIdentical(const std::string &type1,const std::string &type2) throw ()=0;
	
	virtual void renameTable(const std::string &oldName,const std::string &newName) throw (tstore::exception::Exception) {
		SQLStatement statement("rename \""+toolbox::toupper(oldName)+"\" to \""+toolbox::toupper(newName)+"\"");
		execute(statement);
	}
	
	virtual void getKeysForTable(const std::string &tableName,std::vector<std::string> &keys) throw (tstore::exception::Exception)=0;
	
	void getKeysForTable(const std::string &tableName,std::string &keys) throw (tstore::exception::Exception) {
		try {
			std::vector<std::string> keysInDatabase;
			getKeysForTable(tableName,keysInDatabase);
			std::ostringstream keyStream;
			keys="";
			if (!keysInDatabase.empty()) {
				std::copy(keysInDatabase.begin(),keysInDatabase.end(),std::ostream_iterator<std::string>(keyStream, ","));
				keys=keyStream.str();
				keys.erase(keys.size()-1); //remove the trailing comma
			}
		} catch (std::exception &e) {
			XCEPT_RAISE(tstore::exception::Exception,"Could not get keys for table '"+tableName+"':"+e.what());
		}
	}

	//in connections to DBs which do not support enforced foreign keys, these will have to be faked somehow, or there will have to be
	//a supportsForeignKeys function.
	//add a foreign key constraint to \a tableName so that the columns of \a tableName mentioned in the keys of \a columns reference the columns in \a referencedTableName of the corresponding values in \a columns
	virtual void addForeignKey
	(
		const std::string &tableName,
		const std::string &referencedTableName,
		const std::map<std::string,std::string, xdata::Table::ci_less> &columns
	) throw (tstore::exception::Exception)=0;
	
	//get all foreign key constraints in the database. The results have column names as specified above
	virtual void getForeignKeys(xdata::Table &results) throw (tstore::exception::Exception)=0; //gets all the foreign key constraints
	
	//get foreign key constraints for the table \a tableName
	virtual void getForeignKeys(const std::string &tableName,xdata::Table &results) throw (tstore::exception::Exception)=0;
	
	//rename table \a tableName to have a version number, so that a new table can be created with that name
	virtual void backupTable(const std::string &tableName) throw (tstore::exception::Exception)=0;
	
	//give a rough estimate of the number of rows you expect to retrieve in each subsequent query.
	//This is used by subclasses to optimise speed and memory usage
	virtual void setExpectedRowCount(unsigned int expectedRowCount)=0;
}; 


}

#endif
