/*
 * OraclePoolConnection.h
 *
 *  Created on: Jul 15, 2010
 *      Author: janulis
 */

#ifndef ORACLEPOOLCONNECTION_H_
#define ORACLEPOOLCONNECTION_H_

#include <string>
#include "tstore/GlobalGeneralConnectionPool.h"
#include "tstore/OracleConnection.h"
using namespace std;

namespace tstore {

class OraclePoolConnection : public tstore::OracleConnection {
private:
	GlobalGeneralConnectionPool* connPool;

protected:
	OraclePoolConnection(const string& db, const string& user, const string& passwd, GlobalGeneralConnectionPool* connPool);
	virtual OraclePoolConnection* create(std::string db, std::string user, std::string passwd="");

	void terminateStatement(oracle::occi::Statement *statement) throw (tstore::exception::Exception);
	oracle::occi::Connection *connection() throw (tstore::exception::Exception);
public:
	static OraclePoolConnection* create(std::string db, std::string user, std::string passwd="", GlobalGeneralConnectionPool* connPool=NULL);
	~OraclePoolConnection();

	void openConnection() throw (tstore::exception::Exception);
	void closeConnection();
	bool isConnected();

	void commit() throw (tstore::exception::Exception);
	void rollback() throw (tstore::exception::Exception);

private:
	static const int IDLE_TIMEOUT_EXCEPTION = 2396;
	static const int ORA_25401_EXCEPTION 	= 25401; // can not continue fetches
	static const int ORA_25402_EXCEPTION 	= 25402; // transaction must roll back
	static const int ORA_25403_EXCEPTION 	= 25403; // could not reconnect
	static const int ORA_25404_EXCEPTION 	= 25404; // lost instance
	static const int ORA_25405_EXCEPTION 	= 25405; // transaction status unknown
	static const int ORA_25406_EXCEPTION 	= 25406; // could not generate a connect address
	static const int ORA_25407_EXCEPTION 	= 25407; // connection terminated
	static const int ORA_25408_EXCEPTION 	= 25408; // can not safely replay call
	static const int ORA_25409_EXCEPTION 	= 25409; // failover happened during the network operation,cannot continue

	oracle::occi::Connection* prepareConnection(const std::string& user, const std::string& pass, const std::string& db) throw (tstore::exception::Exception);
};

}

#endif /* ORACLEPOOLCONNECTION_H_ */
