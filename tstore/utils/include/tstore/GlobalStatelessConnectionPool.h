/*
 * GlobalStatelessConnectionPool.h
 *
 *  Created on: Jul 15, 2010
 *      Author: janulis
 */

#ifndef GLOBALSTATELESSCONNECTIONPOOL_H_
#define GLOBALSTATELESSCONNECTIONPOOL_H_

#include "GlobalGeneralConnectionPool.h"
#include <map>

namespace tstore {

typedef std::map<std::string, oracle::occi::StatelessConnectionPool*> StatelessConnPoolMap;

class GlobalStatelessConnectionPool : public GlobalGeneralConnectionPool {
private:
	 StatelessConnPoolMap connPools;

public:
	GlobalStatelessConnectionPool(unsigned int minConn=1, unsigned int maxConn=15, unsigned int incrConn=1);
	~GlobalStatelessConnectionPool();

	oracle::occi::Connection* getConnection(const std::string& user, const std::string& pass, const std::string& db, const std::string &tag="") throw (tstore::exception::Exception);
	void releaseConnection(oracle::occi::Connection* conn, const std::string& db, const std::string &tag="") throw (tstore::exception::Exception);
	void terminateConnection(oracle::occi::Connection* conn, const std::string& db) throw (tstore::exception::Exception);

	unsigned int getBusyConnections(const std::string& db);
	unsigned int getOpenConnections(const std::string& db);

	void printStats() throw (tstore::exception::Exception);

	StatelessConnPoolMap::const_iterator poolsBegin();
	StatelessConnPoolMap::const_iterator poolsEnd();
};

}

#endif /* GLOBALSTATELESSCONNECTIONPOOL_H_ */
