// $Id: SQLDelete.h,v 1.1 2007/03/26 13:56:36 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_SQLDelete_h_
#define _toolbox_SQLDelete_h_


#include <string>
#include <vector>

#include "tstore/Query.h" 
#include "tstore/exception/Exception.h" 

namespace tstore {

class SQLDelete : public toolbox::Properties {
	public:
	xdata::Table *data; //the keys of the data to be deleted. If this is NULL, everything in the table will be deleted.
	//create an SQLDelete to delete everything in a table
	//the table can also be set using setProperty("table",statement);
	SQLDelete( const std::string table ) : data(NULL) {
		setProperty("table",table);
	}
    std::string tableName() {
    	return getProperty("table");
	}
	private:
};

}

#endif
