// $Id#include "toolbox/Properties.h": SQLDataManipulation.h,v 1.1 2005/11/17 10:41:25 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "xdata/Table.h"
#include "toolbox/Properties.h"

#ifndef _tstore_DataManipulation_h_
#define _tstore_DataManipulation_h_

#include <set>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include "tstore/exception/Exception.h"
#include "toolbox/string.h"

namespace tstore {

//abstract class with the common functionality from both SQLUpdate and SQLInsert
//this defines which table and which columns of the table should be affected.
//unless you either pass a set of column names into the constructor, or add columns with addColumn, then all columns of the
//table can be affected.
class SQLDataManipulation {
	public:
		xdata::Table *data; //the new data to be put into the database
		SQLDataManipulation(std::string(tableName)) : name_(tableName) {}
		SQLDataManipulation(const std::set<std::string> &columns,std::string(tableName)) : name_(tableName) {
			std::vector<std::string> uppercaseColumns(columns.begin(),columns.end());
			std::transform(columns.begin(), columns.end(), uppercaseColumns.begin(), toolbox::toupper);
			columns_=std::set<std::string>(uppercaseColumns.begin(),uppercaseColumns.end());
		}
		bool shouldChangeColumn(const std::string columnName) {
			if (columns_.empty()) {
				return true; //default behaviour is to allow updating all columns
			}
			return columns_.count(toolbox::toupper(columnName));
		}
		void addColumn(std::string column) {
			columns_.insert(toolbox::toupper(column));
		}
		std::string tableName() {
			return name_;
		}
		//call this to say which columns actually exist in the table in the database
		//this is not necessary but it allows better checking when all columns are affected... 
		//it prevents shouldChangeColumn from returning true for a column does not exist.
		//This method also throws an exception if a column name explicitly specified in the constructor does not exist in the table.
		//(which would indicate bad configuration.)
		template <class InputIterator>
		void setExistingColumns(const InputIterator &first,const InputIterator & last) throw (tstore::exception::Exception) {
			if (columns_.empty()) {
				columns_=std::set<std::string>(first,last);
			} else {
				std::set<std::string> existingColumns(first,last); //this is to make sure the elements are sorted, otherwise set_difference won't work
				std::ostringstream extraColumns;
				std::set_difference(columns_.begin(),columns_.end(),existingColumns.begin(),existingColumns.end(),std::ostream_iterator<std::string>(extraColumns, ", "));
				if (!extraColumns.str().empty()) {
					XCEPT_RAISE(tstore::exception::Exception, "The following columns were specified in the configuration, but do not exist in the table '"+tableName()+"': "+extraColumns.str());
				}
			}
		}
	protected:
		std::set<std::string> columns_;
		std::string name_;
};

}

#endif
