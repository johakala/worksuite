// $Id: TStoreAPI.cc,v 1.5 2009/05/11 15:51:33 brett Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tstore/TStoreAPI.h"
#include "tstore/exception/Exception.h"
#include "xoap/domutils.h"
#include "xoap/SOAPEnvelope.h"
#include "xdata/TableIterator.h"
#include "tstore/client/AttachmentUtils.h"
#include "tstore/client/LoadDOM.h"
#include "tstore/client/Client.h"

//this is very similar to the code in NestedView. I tried to change OracleConnection to return the results as vectors of ForeignKeys but it
//turned out to be too complicated to support the bidirectional links which NestedView uses and also use the same code for getting just the 
//foreign keys of a single table. It would be useful for Connection to be able to return the keys in this format, though, so maybe I'll go
//back to it some day. It would be trivial to make the getForeignKeys for a specific table work this way but then it would be too different
//from the oter overload.
void tstore::processForeignKeys(std::vector<tstore::ForeignKey> &processedKeys,xdata::Table &foreignKeysInDatabase) throw (tstore::exception::Exception) {
	try {
		tstore::ForeignKey currentKey;
		std::map<const std::string,std::string> joinColumns;
		xdata::Table::iterator constraint;
		std::string thisConstraintID,lastConstraintID;
		std::string lastTable;
		//std::cout << foreignKeysInDatabase.numberOfRows_ << " keys in the database! Ahahahahhahaha!" << std::endl; //yep, it's The Cout from Sesame Street
		for (constraint=foreignKeysInDatabase.begin();constraint!=foreignKeysInDatabase.end();++constraint) {
			std::string parent=(*constraint).getField(tstore::parentField)->toString();
			std::string parentColumn=(*constraint).getField(tstore::parentColumnField)->toString();
			std::string childColumn=(*constraint).getField(tstore::childColumnField)->toString();
			thisConstraintID=(*constraint).getField(tstore::constraintField)->toString();
			if (!lastConstraintID.empty() && thisConstraintID!=lastConstraintID) {
				currentKey.childTableName=lastTable;
				processedKeys.push_back(currentKey);
				currentKey.linkedColumns.clear();
			}
			currentKey.linkedColumns[childColumn]=parentColumn;
			lastTable=parent;
			lastConstraintID=thisConstraintID;
		}
		if (!lastTable.empty()) {
			currentKey.childTableName=lastTable;
			processedKeys.push_back(currentKey);
		}
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception, "Could not process foreign key information", e);
	}
}

xoap::SOAPElement addLogElement(xoap::SOAPElement &mainResponseElement,const std::string &elementName,const std::string &tableName) throw (tstore::exception::Exception) {
	try {
		DOMDocument *message=mainResponseElement.getDOM()->getOwnerDocument();
		//xoap::SOAPElement logElement(message->createElementNS(xoap::XStr(TSTORE_NS_URI),xoap::XStr("tstoresoap:"+elementName)));
		DOMElement *logElement=message->createElementNS(xoap::XStr(TSTORE_NS_URI),xoap::XStr("tstoresoap:"+elementName));
		//xoap::SOAPName table("table","tstoresoap",TSTORE_NS_URI);
		logElement->setAttribute(xoap::XStr("table"),xoap::XStr(tableName));
		xoap::SOAPElement addedElement=mainResponseElement.addChildElement(logElement);
		//for a moment I thought it would be more convenient to work with SOAPElements all the way through, but it turns out it isn't.
		return addedElement;//logElement;
	} catch (DOMException &e) {
		std::ostringstream error;
		error << "Could not add " << elementName << "element because of error " << e.code << ": " << xoap::XMLCh2String(e.msg);
		XCEPT_RAISE(tstore::exception::Exception,error.str());
	}
}

//a wrapper around a real tstore connection, which adds an element to a SOAPElement for each action that changes the schema,
//and also supports 'dry run' mode where it does not actually change anything in the database but just records what it would have done.
//it also prevents opening or closing the connection, since this should be done by TStore itself rather than any users of a TStoreAPI
class LoggingConnection: public tstore::Connection {
	private:
	tstore::Connection *realConnection_;
	xoap::SOAPElement *log_;
	bool dryRun_;
	public:
	
	LoggingConnection(xoap::SOAPElement &log,tstore::Connection &realConnection,bool dryRun) throw (tstore::exception::Exception) : realConnection_(&realConnection),log_(&log),  dryRun_(dryRun) {
	}
	
	LoggingConnection(std::string user, std::string passwd, std::string db) throw (tstore::exception::Exception) {
		XCEPT_RAISE(tstore::exception::Exception,"Logging connection can not be instantiated without a message to log to.");
	}
	
	virtual void execute(tstore::SQLQuery &query,xdata::Table &results,const tstore::MappingList &mappings) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->execute(query,results,mappings);
	}
	virtual void execute(tstore::SQLQuery &query,xdata::Table &results) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->execute(query,results);
	}
	virtual void execute(tstore::SQLUpdate &update) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->execute(update);
	}
	virtual void execute(tstore::SQLUpdate &insert,const tstore::MappingList &columnTypes) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->execute(insert,columnTypes);
	}
	virtual void execute(tstore::SQLInsert &update) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->execute(update);
	}
	
	virtual void execute(tstore::SQLInsert &update,const tstore::MappingList &columnTypes) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->execute(update,columnTypes);
	}
	virtual void execute(tstore::SQLStatement &statement) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->execute(statement);
	}
	virtual void execute(tstore::SQLDelete &d) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->execute(d);
	}
	virtual void createTable(std::string name,std::string primaryKey,xdata::Table &definition) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->createTable(name,primaryKey,definition);
		xoap::SOAPElement addTableElement=addLogElement("addTableToDatabase",name);
		xoap::SOAPName key("key","","");
		addTableElement.addAttribute(key,primaryKey);
		//logElement->setAttribute(xoap::XStr("key"),xoap::XStr(primaryKey));	
	}
	virtual void dropTable(std::string name) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->dropTable(name);
		//std::cout << (dryRun_?"pretended to remove ":"removed ") << name << std::endl;
		addLogElement("removeTableFromDatabase",name);
	}

	virtual void addForeignKey
	(
		const std::string &tableName,
		const std::string &referencedTableName,
		const std::map<std::string,std::string, xdata::Table::ci_less> &columns
	) 
	throw (tstore::exception::Exception) 
	{
		if (!dryRun_) realConnection_->addForeignKey(tableName,referencedTableName,columns);
		xoap::SOAPElement logElement=addLogElement("addForeignKeyToDatabase",tableName);
		xoap::SOAPName referencedTable("referencedTable","","");
		logElement.addAttribute(referencedTable,referencedTableName);
		//logElement->setAttribute(xoap::XStr("referencedTable"),xoap::XStr(referencedTableName));
	}
	
	virtual void backupTable(const std::string &tableName) throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->backupTable(tableName);
		addLogElement("backupTable",tableName);
	}
	
	virtual void commit() throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->commit();
	}
	virtual void rollback() throw (tstore::exception::Exception) {
		if (!dryRun_) realConnection_->rollback();
	}
	void attemptRollback() {
		if (!dryRun_) realConnection_->attemptRollback();
	}
	
	virtual void getColumnTypes(std::string tableName,std::map<const std::string,std::string> &types,const tstore::MappingList &mappings) {
		realConnection_->getColumnTypes(tableName,types,mappings);
	}
	
	virtual bool keysMatch(const std::string &tableName,const std::string &keys) throw (tstore::exception::Exception) {
		return keysMatch(tableName,keys);
	}
	
	virtual void getTablesMatchingPattern(std::set<std::string> &tableNames,const std::string &pattern) throw (tstore::exception::Exception) {
		getTablesMatchingPattern(tableNames,pattern);
	}
	
	//these should not be used by a view
	virtual void openConnection() throw (tstore::exception::Exception) {
		XCEPT_RAISE(tstore::exception::Exception,"View attempted to open a connection. This should only be done by TStore itself.");
	}
	virtual void closeConnection() {
		XCEPT_RAISE(tstore::exception::Exception,"View attempted to close a connection. This should only be done by TStore itself.");
	}
  	virtual bool isConnected() {
		return realConnection_->isConnected();
	}
	virtual void getTableDefinition(const std::string &tableName,xdata::Table &definition) throw (tstore::exception::Exception) {
		realConnection_->getTableDefinition(tableName,definition);
	}
	
	//returns whether the two xdata types given map to identical types in the database
	virtual bool typesAreIdentical(const std::string &type1,const std::string &type2) throw () {
		return realConnection_->typesAreIdentical(type1,type2);
	}
	
	virtual void getKeysForTable(const std::string &tableName,std::vector<std::string> &keys) throw (tstore::exception::Exception) {
		realConnection_->getKeysForTable(tableName,keys);
	}
	
	virtual void getForeignKeys(xdata::Table &results) throw (tstore::exception::Exception) {
		realConnection_->getForeignKeys(results);
	}
	virtual void getForeignKeys(const std::string &tableName,xdata::Table &results) throw (tstore::exception::Exception) {
		realConnection_->getForeignKeys(tableName,results);
	}
	xoap::SOAPElement addLogElement(const std::string &elementName,const std::string &tableName) throw (tstore::exception::Exception) {
		try {
			return ::addLogElement(*log_,elementName,tableName);
		} catch (tstore::exception::Exception &e) {
			//if it's not a dry run then the client might not care about what was done enough to cancel it halfway through
			//we don't want to leave things half-done in the database just because recording the action failed.
			//on the other hand, it'll fail anyway if we return NULL;
			XCEPT_RETHROW(tstore::exception::Exception,"Could not record DML operation in response",e);
		}
	}
	
	virtual void setExpectedRowCount(unsigned int expectedRowCount) {
		realConnection_->setExpectedRowCount(expectedRowCount);
	}

}; 


tstore::TStoreAPI::TStoreAPI(tstore::Connection *connection,xoap::MessageReference request,xoap::MessageReference response,tstore::MappingList &mappings,bool dryRun,DOMNode *config,std::map<const std::string,std::string> *tableNames) 
	 throw (tstore::exception::Exception)
	: /*connection_(connection),*/request_(request),response_(response),mappings_(mappings),tableNames_(tableNames),config_(config),mainResponseElement_(response->getSOAPPart().getEnvelope()),dryRun_(dryRun) {
	//the table names could be parsed from the message directly by TStoreAPI, but for now TStore needs them too so it may as well get them.
	//the DOMNode, on the other hand, comes from the config file and may have already been modified in memory.
	if (!connection) {
		XCEPT_RAISE(tstore::exception::Exception, "Cannot create TStoreAPI");
	}
	xoap::SOAPEnvelope envelope = response->getSOAPPart().getEnvelope();
 	xoap::SOAPBody body = envelope.getBody();
 	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	std::vector<xoap::SOAPElement> children(body.getChildElements());
	if (children.size()!=1) {
		XCEPT_RAISE(tstore::exception::Exception,"Wrong number of root elements in the response");
	}
	
	mainResponseElement_=children[0];
	connection_=new LoggingConnection(mainResponseElement_,*connection,dryRun_);
	if (!connection_) {
		//if for whatever reason we could not create our wrapper connection, just create a normal one. There will be no logging but that's
		//okay, as long as we are not trying to do a dry run
		if (dryRun_) {
			XCEPT_RAISE(tstore::exception::Exception,"Could not create logging connection for dry run.");
		}
		connection_=connection;
	}
}

tstore::TStoreAPI::~TStoreAPI() {
	for (std::multimap<std::string,tstore::ForeignKey>::iterator keyToAdd=keysToAdd_.begin();keyToAdd!=keysToAdd_.end();++keyToAdd) {
		connection_->addForeignKey((*keyToAdd).first,(*keyToAdd).second.childTableName,(*keyToAdd).second.linkedColumns);
	}
	delete connection_;
}
	
tstore::Connection *tstore::TStoreAPI::connection() {
	return connection_;
}

//the table should really be const, but it would either have to be copied, or Serializer.exportAll would have to be made const
void tstore::TStoreAPI::addResultTable(xdata::Table &result,const std::string &contentId) throw (tstore::exception::Exception) {
	try {
		tstoreclient::addAttachment(response_,result,contentId);
	} catch (xoap::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not add results table with ID: "+contentId,e);
	}
}

bool tstore::TStoreAPI::getTable(xdata::Table &table,std::string contentId) throw (tstore::exception::Exception) {
	try {
		return tstoreclient::getAttachmentWithID(request_,table,contentId);
	} catch (xoap::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not get attached table with ID "+contentId,e);
	}
}

bool tstore::TStoreAPI::getFirstTable(xdata::Table &table) throw (tstore::exception::Exception) {
	try {
		return tstoreclient::getFirstAttachmentOfType(request_,table);
	} catch (xoap::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not get first attached table.",e);
	}
}

void tstore::TStoreAPI::getMappingsForTable(std::string tableName,tstore::MappingList &mappings) throw (tstore::exception::Exception) {
	try {
		mappings.clear();
		for (tstore::MappingList::iterator mappingIterator=mappings_.begin();mappingIterator!=mappings_.end();mappingIterator++) {
			if ((*mappingIterator).matchesTable(tableName)) {
				mappings.push_back(*mappingIterator);
			}
		}
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not get mappings for table: "+tableName,e);
	}
}

//perhaps this should be changed to return the actual tables as well (since they are usually needed and it is guaranteed by this point that they exist in the message)
void tstore::TStoreAPI::getTableNames(std::map<const std::string,std::string> &tableNames) throw (tstore::exception::Exception) {
	if (tableNames_) {
		tableNames=*tableNames_;
	} else {
		XCEPT_RAISE(tstore::exception::Exception,"Error in view class: getTableNames called from a method other than addTables or removeTables");
	}
}

void tstore::TStoreAPI::getTableNames(std::vector<std::string> &tableNames) throw (tstore::exception::Exception) {
	if (tableNames_) {
		std::map<std::string,std::string>::iterator tableName;
		tableNames.clear();
		for (tableName=tableNames_->begin();tableName!=tableNames_->end();++tableName) {
			tableNames.push_back((*tableName).first);
		}
	} else {
		XCEPT_RAISE(tstore::exception::Exception,"Error in view class: getTableNames called from a method other than addTables or removeTables");
	}
}


DOMNode *tstore::TStoreAPI::getConfiguration() throw (tstore::exception::Exception) {
	if (config_) {
		return config_;
	} else {
		XCEPT_RAISE(tstore::exception::Exception,"Error in view class: getConfiguration called from a method which can not alter the configuration");
	}
}

//this one has code copied from TStore, need to check that this can be removed from TStore
void tstore::TStoreAPI::addColumnDefinition(tstore::MappingList &mappings, const std::string &xdataType,const std::string &columnName,const std::string &tableName) {
	tstore::Mapping mapping(xdataType,columnName,tableName);
	mapping.setMatchesExactly();
	mappings.push_back(mapping);
}

void tstore::TStoreAPI::addTableMetadata(/*const*/ xdata::Table &table,const std::string &tableName,const std::string &key) throw (tstore::exception::Exception) {
	if (!dryRun_) {
		std::vector<std::string> columns=table.getColumns();
		//this stuff is unnecessary since it will be reread from the config later.
		for (std::vector<std::string>::iterator column=columns.begin(); column!=columns.end(); ++column) {
			addColumnDefinition(mappings_,table.getColumnType(*column),*column,tableName);
		}
	}
	addTableMetadataToConfig(table,tableName,key);
}

void tstore::TStoreAPI::addTableMetadata(/*const*/ xdata::Table &table,const std::string &tableName,const std::string &key,const std::vector<tstore::ForeignKey> &foreignKeys) throw (tstore::exception::Exception) {
	addTableMetadata(table,tableName,key);
	//std::cout << " adding " << foreignKeys.size() << " keys for newly added table" << std::endl;
	for (std::vector<tstore::ForeignKey>::const_iterator foreignKey=foreignKeys.begin();foreignKey!=foreignKeys.end();++foreignKey) {
		addForeignKeyMetadata(tableName,(*foreignKey).childTableName,(*foreignKey).linkedColumns);
	}
}


bool tstore::TStoreAPI::hasTable(const std::string &tableName) {
	return getNodeForTable(tableName)!=NULL;
	/* previously looked at the mapping list to see whether the view had that table
	however, the mapping list is not updated in dry run mode, while the DOM is (it just isn't written to disk afterwards)
	so now to make sure we have the latest information, check whether the table is in the DOM.
	tstore::MappingList mappings=mappings_;
	for (tstore::MappingList::iterator mappingIterator=mappings.begin();mappingIterator!=mappings.end();mappingIterator++) {
		if ((*mappingIterator).matchesExactly()) { //only check the mappings which match only one specific table (the ones added due to table definitions rather than old-style mappings)
			if ((*mappingIterator).matchesTable(tableName)) return true;
		}
	}
	return false;*/
}

//in future the base View class might handle everything to do with mappings, so this would be done in addTables.
//but for now we still might want the possibility of having mappings which apply to all views, or are stored in other files.
void tstore::TStoreAPI::addTableMetadataToConfig(/*const*/ xdata::Table &table,const std::string &tableName,const std::string &key) throw (tstore::exception::Exception) {
	try {
		DOMNode *viewNode=getConfiguration();
		DOMDocument *doc=viewNode->getOwnerDocument();
		DOMElement *newTableElement=tstoreclient::createDOMElementForTable(doc,table,tableName,key);
		
		viewNode->appendChild(newTableElement);
	
		//DOMDocument *message=mainResponseElement_->getOwnerDocument();
		mainResponseElement_.addNamespaceDeclaration("tstore","urn:xdaq-tstore:1.0");
		xoap::SOAPElement logElement(addLogElement("addTableToConfig",tableName));
		logElement.addChildElement(newTableElement);
	} catch (xcept::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Cannot add metadata for table "+tableName,e);
	} catch (DOMException &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Cannot add metadata for table "+tableName+":  "+xoap::XMLCh2String(e.msg));
	}
}

void tstore::TStoreAPI::addTable(const std::string &tableName,const std::string &key,xdata::Table &definition) throw (tstore::exception::Exception) {
	try {
		if (!hasTable(tableName)) {
			connection()->createTable(tableName,key,definition);
			addTableMetadata(definition,tableName,key);
			//addTableMetadataToConfig(viewNode,definition,tableName);
		} else {
			XCEPT_RAISE(tstore::exception::Exception,"The view already has type metadata for a table named '"+tableName+"'");
		}
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not create table "+tableName+" in the database.",e);
	}
}

DOMNode *tstore::TStoreAPI::getNodeForTable(const std::string &tableName) {
	std::vector<DOMNode *> tstoreNodes(tstoreclient::nodesWithPrefix(config_,"urn:xdaq-tstore:1.0"));
	for (std::vector<DOMNode *>::iterator node=tstoreNodes.begin();node!=tstoreNodes.end();++node) {
		if (xoap::XMLCh2String((*node)->getLocalName())=="table") {
			if (toolbox::toupper(xoap::getNodeAttribute((*node),"name"))==toolbox::toupper(tableName)) {
				return *node;
			}
		}
	}
	return NULL; //throw an exception?
}

xoap::SOAPElement tstore::TStoreAPI::addLogElement(const std::string &elementName,const std::string &tableName) {
	return ::addLogElement(mainResponseElement_,elementName,tableName);
}

void tstore::TStoreAPI::removeTableMetadataFromConfig(const std::string &tableName) {
	DOMNode *node=getNodeForTable(tableName);
	if (node) {
		config_->removeChild(node);
	}
	addLogElement("removeTableFromConfig",tableName);
}

void tstore::TStoreAPI::removeTableMetadata(const std::string &tableName) throw (tstore::exception::Exception) {
	tstore::MappingList &mappings=mappings_;
	try {
		if (!dryRun_) {
			tstore::MappingList mappingsCopy;
			//std::remove_if(mappings.begin(),mappings.end(),
			for (tstore::MappingList::iterator mapping=mappings.begin();mapping!=mappings.end();++mapping) {
				if ((*mapping).matchesExactly()) { //only check the mappings which match only one specific table (the ones added due to table definitions rather than old-style mappings)
					if ((*mapping).matchesTable(tableName)) {
						continue;
					}
				}
				mappingsCopy.push_back(*mapping);
			}
			mappings_=mappingsCopy;
		}
		removeTableMetadataFromConfig(tableName);
	} catch (std::exception &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Cannot remove metadata for table "+tableName+":  "+e.what());
	}
}

void tstore::TStoreAPI::addForeignKeyMetadata
(
	const std::string &tableName,
	const std::string &referencedTableName,
	const std::map<std::string,std::string, xdata::Table::ci_less> &columns
) 
throw (tstore::exception::Exception) 
{
	DOMNode *tableNode=getNodeForTable(tableName);
	//std::cout << "adding key referencing table : " << referencedTableName << std::endl;
	if (tableNode) {
		DOMDocument *doc=tableNode->getOwnerDocument();
		DOMElement *keyNode=doc->createElementNS(xoap::XStr("urn:xdaq-tstore:1.0"),xoap::XStr("tstore:foreignkey"));
		keyNode->setAttribute(xoap::XStr("references"),xoap::XStr(referencedTableName));
		tableNode->appendChild(keyNode);
		for (std::map<std::string,std::string>::const_iterator column=columns.begin(); column!=columns.end(); ++column) {
			DOMElement *columnNode=doc->createElementNS(xoap::XStr("urn:xdaq-tstore:1.0"),xoap::XStr("tstore:keycolumn"));
			columnNode->setAttribute(xoap::XStr("column"),xoap::XStr((*column).first));
			//std::cout << "adding key column referencing : " << (*column).second << std::endl;
			columnNode->setAttribute(xoap::XStr("references"),xoap::XStr((*column).second));
			keyNode->appendChild(columnNode);
		}
		mainResponseElement_.addNamespaceDeclaration("tstore","urn:xdaq-tstore:1.0");
		xoap::SOAPElement logElement(addLogElement("addForeignKeyToConfig",tableName));
		logElement.addChildElement(keyNode);
	} else {
		XCEPT_RAISE(tstore::exception::Exception,"Could not add foreign key metadata to table '"+tableName+"'. The table does not exist.");
	}
}

void tstore::TStoreAPI::addForeignKey
(
	const std::string &tableName,
	const std::string &referencedTableName,
	const std::map<std::string,std::string, xdata::Table::ci_less> &columns
) 
throw (tstore::exception::Exception) 
{
	try {
		if (hasTable(tableName)) {
			//connection()->addForeignKey(tableName,referencedTableName,columns);
			tstore::ForeignKey key;
			key.childTableName=referencedTableName;
			key.linkedColumns=columns;
			keysToAdd_.insert	(
											std::pair<const std::string,tstore::ForeignKey >(tableName,key)
										); //add the key later, when we can be sure all the tables exist.
			addForeignKeyMetadata(tableName,referencedTableName,columns);
			//removeTableMetadataFromConfig(tableName);
		} else {
			XCEPT_RAISE(tstore::exception::Exception,"The view does not have type metadata for a table named '"+tableName+"'");
		}
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not add foreign key to table '"+tableName+"' in the database.",e);
	}
}

void tstore::TStoreAPI::removeTable(const std::string &tableName) throw (tstore::exception::Exception) {
	try {
		if (hasTable(tableName)) {
			connection()->dropTable(tableName);
			removeTableMetadata(tableName);
			//removeTableMetadataFromConfig(tableName);
		} else {
			XCEPT_RAISE(tstore::exception::Exception,"The view does not have type metadata for a table named '"+tableName+"'");
		}
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not drop table '"+tableName+"' from the database.",e);
	}
}

void tstore::TStoreAPI::getForeignKeysForTable(const std::string &tableName,std::vector<tstore::ForeignKey> &foreignKeysInDatabase) throw (tstore::exception::Exception) {
	xdata::Table rawKeys;
	try {
		connection_->getForeignKeys(tableName,rawKeys);
		processForeignKeys(foreignKeysInDatabase,rawKeys);
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not get foreign keys for table '"+tableName+"'",e);
	}
}
