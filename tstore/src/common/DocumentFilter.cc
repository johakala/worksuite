// $Id: DocumentFilter.cc,v 1.6 2008/07/18 15:28:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <sstream>

#include "tstore/DocumentFilter.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/framework/LocalFileInputSource.hpp"
#include "xercesc/parsers/XercesDOMParser.hpp"
#include "xercesc/validators/schema/SchemaSymbols.hpp"
#include "xercesc/util/XMLUri.hpp"

#include "toolbox/utils.h"
#include "xoap/SOAPElement.h"
#include "xoap/domutils.h"
#include "xoap/Lock.h"

//#include <xqilla/xqilla-dom3.hpp>

XERCES_CPP_NAMESPACE_USE;

const XMLCh XMLChXS[] =
{ chLatin_x, chLatin_s, chNull };
const XMLCh XMLChXSI[] =
{ chLatin_x, chLatin_s, chLatin_i, chNull };
const XMLCh XMLChFN[] =
{ chLatin_f, chLatin_n, chNull };
const XMLCh XMLChLOCAL[] =
{ chLatin_l, chLatin_o, chLatin_c, chLatin_a, chLatin_l, chNull };

tstore::DocumentFilter::DocumentFilter (const std::string& filterExpression)
{
	filter_ = filterExpression;
}

tstore::DocumentFilter::DocumentFilter (xoap::SOAPElement& element)
{
	filter_ = toolbox::trim(element.getValue());
}

std::string tstore::DocumentFilter::getFilterExpression ()
{
	return filter_;
}

bool tstore::DocumentFilter::match (DOMDocument* doc) throw (tstore::exception::Exception)
{

	xoap::lock();
	bool match = false;

	//DOMDocument* doc = message->getDocument();
	DOMElement* root = doc->getDocumentElement();
	try
	{
		AutoRelease < DOMXPathNSResolver > resolver(doc->createNSResolver(root));

		resolver->addNamespaceBinding(XMLChXS, SchemaSymbols::fgURI_SCHEMAFORSCHEMA);
		resolver->addNamespaceBinding(XMLChXSI, SchemaSymbols::fgURI_XSI);

		AutoRelease < DOMXPathExpression > expr(doc->createExpression(xoap::XStr(filter_), resolver));
		AutoRelease < DOMXPathResult > result(expr->evaluate(doc, DOMXPathResult::ORDERED_NODE_SNAPSHOT_TYPE, 0));

		XMLSize_t nLength = result->getSnapshotLength();
		if (nLength > 0)
		{
			match = true;
		}
		xoap::unlock();
		return match;

	}
	catch (const DOMXPathException& e)
	{
		std::stringstream msg;
		msg << "An error occurred during processing of the XPath expression. Msg is:" << xoap::XMLCh2String(e.getMessage());
		xoap::unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg.str());
	}
	catch (const DOMException& e)
	{
		std::stringstream msg;
		msg << "An error occurred during processing of the XPath expression. Msg is:" << xoap::XMLCh2String(e.getMessage());
		xoap::unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg.str());
	}

}

std::string tstore::DocumentFilter::evaluate (DOMDocument* doc) throw (tstore::exception::Exception)
{
	xoap::lock();

	//DOMDocument* doc = message->getDocument();
	DOMElement* root = doc->getDocumentElement();
	try
	{
		AutoRelease < DOMXPathNSResolver > resolver(doc->createNSResolver(root));

		// Set the namespace prefix for the people namespace that
		// we can use reliably in XPath expressions regardless of
		// what is used in XML documents.
		//
		resolver->addNamespaceBinding(XMLChXS, SchemaSymbols::fgURI_SCHEMAFORSCHEMA);
		resolver->addNamespaceBinding(XMLChXSI, SchemaSymbols::fgURI_XSI);

		// Create XPath expression.
		//
		AutoRelease < DOMXPathExpression > expr(doc->createExpression(xoap::XStr(filter_), resolver));

		// Execute the query.
		//
		AutoRelease < DOMXPathResult > result(expr->evaluate(doc, DOMXPathResult::ORDERED_NODE_SNAPSHOT_TYPE, 0));

		//AutoRelease<DOMXPathResult> result(doc->evaluate( xpathStr, root, resolver, DOMXPathResult::ORDERED_NODE_SNAPSHOT_TYPE, NULL));

		std::stringstream s;

		XMLSize_t nLength = result->getSnapshotLength();
		for (XMLSize_t i = 0; i < nLength; i++)
		{
			result->snapshotItem(i);
			s << xoap::XMLCh2String(result->getNodeValue()->getTextContent());

		}

		xoap::unlock();

		return s.str();

	}
	catch (const DOMXPathException& e)
	{
		std::stringstream msg;
		msg << "An error occurred during processing of the XPath expression. Msg is:" << xoap::XMLCh2String(e.getMessage());
		xoap::unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg.str());
	}
	catch (const DOMException& e)
	{
		std::stringstream msg;
		msg << "An error occurred during processing of the XPath expression. Msg is:" << xoap::XMLCh2String(e.getMessage());
		xoap::unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg.str());
	}

}

std::list<xoap::SOAPElement> tstore::DocumentFilter::extract (DOMDocument* doc) throw (tstore::exception::Exception)
{
	xoap::lock();

	//DOMDocument* doc = message->getDocument();
	DOMElement* root = doc->getDocumentElement();
	try
	{

		AutoRelease < DOMXPathNSResolver > resolver(doc->createNSResolver(root));

		resolver->addNamespaceBinding(XMLChXS, SchemaSymbols::fgURI_SCHEMAFORSCHEMA);
		resolver->addNamespaceBinding(XMLChXSI, SchemaSymbols::fgURI_XSI);

		AutoRelease < DOMXPathExpression > expr(doc->createExpression(xoap::XStr(filter_), resolver));
		AutoRelease < DOMXPathResult > result(expr->evaluate(doc, DOMXPathResult::ORDERED_NODE_SNAPSHOT_TYPE, 0));

		std::list < xoap::SOAPElement > elements;
		XMLSize_t nLength = result->getSnapshotLength();
		for (XMLSize_t i = 0; i < nLength; i++)
		{
			result->snapshotItem(i);
			elements.push_back(xoap::SOAPElement(result->getNodeValue()));
		}

		xoap::unlock();
		return elements;

	}
	catch (const DOMXPathException& e)
	{
		std::stringstream msg;
		msg << "An error occurred during processing of the XPath expression. Msg is:" << xoap::XMLCh2String(e.getMessage());
		xoap::unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg.str());
	}
	catch (const DOMException& e)
	{
		std::stringstream msg;
		msg << "An error occurred during processing of the XPath expression. Msg is:" << xoap::XMLCh2String(e.getMessage());
		xoap::unlock();
		XCEPT_RAISE(xoap::exception::Exception, msg.str());
	}

}

