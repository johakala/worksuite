// $Id: SQLViewInternal.cc,v 1.3 2009/05/11 15:51:33 brett Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tstore/SQLViewInternal.h"
#include "xoap/domutils.h"
#include "xercesc/dom/DOMNode.hpp"
#include "toolbox/string.h"
#include "tstore/exception/Exception.h"
#include "tstore/Mapping.h"
#include "tstore/SQLView.h"
#include <sstream>

//this should be in its own file.

bool tstore::ViewTable::isColumnNode(const DOMNode *node) {
	return node->getNodeType()==DOMNode::ELEMENT_NODE && xoap::XMLCh2String(node->getLocalName())=="column";
}

tstore::ViewTable::ViewTable(DOMNode *node) {
	setTableName(xoap::getNodeAttribute(node, "name"));
	DOMNodeList *children=node->getChildNodes();
	int childrenCount=children->getLength();
	for (int childIndex=0;childIndex<childrenCount;childIndex++) {
		DOMNode *child=children->item(childIndex);
		if (isColumnNode(child)) {
			std::string column=xoap::getNodeAttribute(child, "name");
			
			//std::cout << "Read column [" << column << "]" << std::endl;
			
			addColumn( column/*,type*/);
		}
	}
}

void tstore::SQLViewInternal::resetParameters() {
	queryParameters_=defaultQueryParameters_;
	queryBindParameters_=defaultQueryBindParameters_;
}

void tstore::SQLViewInternal::setParameter(const std::string &parameterName,const std::string &value) throw (tstore::exception::Exception) {
	tstore::View::setParameter(queryParameters_,queryBindParameters_,parameterName,value);
}

bool tstore::SQLViewInternal::canQuery() const {
	return !query_.empty();
}

bool tstore::SQLViewInternal::canDestroy() const {
	return true; //perhaps should only return true if it is a simple query... but insert etc should cover the same tables as are selected in a query.
}

void tstore::SQLViewInternal::setQuery(const std::string query) {
	query_=query;
}

const std::string tstore::SQLViewInternal::queryString(std::vector<std::string> *bindParameters) {
	return tstore::View::substituteParameters(bindParameters,queryParameters_,queryBindParameters_,query_);
}

bool tstore::SQLViewInternal::isTableNode(const DOMNode *node) {
	return node->getNodeType()==DOMNode::ELEMENT_NODE && xoap::XMLCh2String(node->getLocalName())=="table";
}

/*
bool tstore::SQLViewInternal::isMappingNode(const DOMNode *node) {
	return node->getNodeType()==DOMNode::ELEMENT_NODE && xoap::XMLCh2String(node->getLocalName())=="columntypes";
}
*/

void tstore::SQLViewInternal::addCreateStatement(std::string statement) {
	createStatements_.push_back(statement);
}

//could also get parameters, but for now let's keep it simple.
void tstore::SQLViewInternal::addCreate(DOMNode *node) {
	DOMNodeList *children=node->getChildNodes();
	int childrenCount=children->getLength();
	int childIndex;
	for (childIndex=0;childIndex<childrenCount;childIndex++) {
		DOMNode *child=children->item(childIndex);
		if (child->getNodeType()==DOMNode::CDATA_SECTION_NODE) {
			addCreateStatement(toolbox::trim(xoap::XMLCh2String(child->getNodeValue())));
		}
	}
}

void tstore::SQLViewInternal::addQuery(DOMNode *node) {
	DOMNodeList *children=node->getChildNodes();
	int childrenCount=children->getLength();
	int childIndex;
	bool simpleQuery=false;
	std::string expectedRowsString=xoap::getNodeAttribute(node, "expectedRows");
	std::istringstream myStream(expectedRowsString);
	myStream >> queryExpectedRows_;
	if (myStream.fail()) {
		if (expectedRowsString.size()) std::cout << "could not read expectedRows value '" << expectedRowsString << "' as integer" << std::endl;
		queryExpectedRows_=100;
	}
	//std::cout << "expected rows for query " << xoap::getNodeAttribute(node, "name") << ": " << queryExpectedRows_ << std::endl;
	for (childIndex=0;childIndex<childrenCount;childIndex++) {
		DOMNode *child=children->item(childIndex);
		if (child->getNodeType()==DOMNode::CDATA_SECTION_NODE) {
			setQuery(toolbox::trim(xoap::XMLCh2String(child->getNodeValue())));
			queryTable_=""; //we don't know which tables the data comes from, so match mappings for any table.
			simpleQuery=false;
		} else if (child->getNodeType()==DOMNode::ELEMENT_NODE && xoap::XMLCh2String(child->getLocalName())=="parameter") {
			tstore::View::readParameterDefinition(queryParameters_,queryBindParameters_,child);
		} else if (isTableNode(child)) { //you can just specify a table/column list rather than an SQL statement.
			//always choose a complicated query over a simple one
			if (query_.empty()) {
				setQuery(createSimpleQuery(child,queryTable_));
				simpleQuery=true;
			}
		}
	}
	if (simpleQuery) {
		queryParameters_.clear(); //simple queries do not have parameters. Perhaps we should log a warning here
		queryBindParameters_.clear();
	} else {
		defaultQueryParameters_=queryParameters_;
		defaultQueryBindParameters_=queryBindParameters_;
	}
}

//for simplicity it could return the normal query from the query() method, but still leave the details accessible.
std::string tstore::SQLViewInternal::createSimpleQuery(DOMNode *node,std::string &tableName/*,std::map<const std::string,std::string> &customTypes*/) {
	tstore::ViewTable table(node);
	bool columnAdded=false;
	std::string columnList;
	const std::set<std::string> *columns=&table.columns();
	std::set<std::string>::iterator columnIterator;
	for (columnIterator=columns->begin();columnIterator!=columns->end();++columnIterator) {
		if (columnAdded) columnList+=",";
		columnList+=*columnIterator+" \""+*columnIterator+"\""; //select from a normal case-insensitive column name, but rename the result column to have the right case
		columnAdded=true;
	}
	tableName=xoap::getNodeAttribute(node, "name");
	std::string fromClause=" FROM "+tableName;
	//customTypes=table.customTypes();
	simpleQueryTableName_=toolbox::toupper(tableName);
	if (!columnAdded) return "SELECT *"+fromClause;
	else return "SELECT "+columnList+fromClause;
}

//creates ViewTable objects from the "table" children of node, and adds them to vectorToUpdate
void tstore::SQLViewInternal::addTablesToVector(const DOMNode *node,std::vector<tstore::ViewTable> &vectorToUpdate) {
	DOMNodeList *children=node->getChildNodes();
	int childrenCount=children->getLength();
	int childIndex;
	//SOAPElement's getChildElements would be handy here, but it's not worth the hassle of converting just for that.
	for (childIndex=0;childIndex<childrenCount;childIndex++) {
		DOMNode *child=children->item(childIndex);
		if (isTableNode(child)) {
			tstore::ViewTable newTable(child);
			//there should be checking for duplicates, which would be easy with something like a map
			//but we might want them to stay in order due to database triggers or other constraints
			vectorToUpdate.push_back(newTable);
		}
	}
}

void tstore::SQLViewInternal::addTableToNode(DOMNode *node,std::vector<std::string> &columns,const std::string &tableName) {
	std::string namespaceURI="urn:tstore-view-SQL";//((SQLView *)NULL)->namespaceURI();
	DOMDocument *doc=node->getOwnerDocument();
	DOMElement *tableNode=doc->createElementNS(xoap::XStr(namespaceURI),xoap::XStr("sql:table"));
	tableNode->setAttribute(xoap::XStr("name"),xoap::XStr(tableName));
	
	//the following is not necessary, since a table node with no columns is shorthand for including all columns.
	//However since we know the column names, we may as well put them in so that it is easier for a human to edit.
	//also, this gives the correct capitalisation.
	for (std::vector<std::string>::iterator column=columns.begin(); column!=columns.end(); ++column) {
		DOMElement *columnNode=doc->createElementNS(xoap::XStr(namespaceURI),xoap::XStr("sql:column"));
		columnNode->setAttribute(xoap::XStr("name"),xoap::XStr(*column));
		tableNode->appendChild(columnNode);
	}
	node->appendChild(tableNode);
}

void tstore::SQLViewInternal::addSimpleConfigurationSection(DOMNode *node,std::vector<std::string> &columns,const std::string &tableName,const std::string &sectionName) {
	std::string namespaceURI="urn:tstore-view-SQL";//((SQLView *)NULL)->namespaceURI();
	DOMDocument *doc=node->getOwnerDocument();
	DOMElement *section=doc->createElementNS(xoap::XStr(namespaceURI),xoap::XStr("sql:"+sectionName));
	section->setAttribute(xoap::XStr("name"),xoap::XStr(tableName));
	addTableToNode(section,columns,tableName);
	addConfiguration(sectionName,section);
	node->appendChild(section);
}

void tstore::SQLViewInternal::addConfiguration(DOMNode *node,std::vector<std::string> &columns,const std::string &tableName) {
	addSimpleConfigurationSection(node,columns,tableName,"query");
	addSimpleConfigurationSection(node,columns,tableName,"insert");
	addSimpleConfigurationSection(node,columns,tableName,"update");
}

bool tstore::SQLViewInternal::canInsert() const {
	return !insertTables_.empty();
}

bool tstore::SQLViewInternal::canRemove() const {
	return canInsert();
}

void tstore::SQLViewInternal::addInsert(const DOMNode *node) throw (tstore::exception::Exception) {
	addTablesToVector(node,insertTables_);
	//keep a record of the case of the column names, so that we can return columns with those cases from the definition message
	for (std::vector<tstore::ViewTable>::iterator insertTable=insertTables_.begin();insertTable!=insertTables_.end();insertTable++) {
		const std::set<std::string> &columns=(*insertTable).columns();
		for (std::set<std::string>::iterator column=columns.begin();column!=columns.end();column++) {
			std::string uppercaseColumn=toolbox::toupper(*column);
			if (insertColumnNames_.count(uppercaseColumn)) {
				if (insertColumnNames_[uppercaseColumn]!=*column) {
					//it would be possible to allow different capitalisations of column names for different tables,
					//leaving columns in the definition which differ only by case. However I think it's more likely to be a mistake
					//than something people would do deliberately.
					XCEPT_RAISE(tstore::exception::Exception,"Two different capitalisations of the same column name exist in the insert configuration: "+insertColumnNames_[uppercaseColumn]+" and "+*column);
				}
			}
			insertColumnNames_[uppercaseColumn]=*column;
		}
	}
}

bool tstore::SQLViewInternal::canUpdate() const {
	try {
		return !updateTables_.empty();
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
		return true;
	} catch (...) {
		return false;
	}
}

void tstore::SQLViewInternal::addUpdate(const DOMNode *node) {
	addTablesToVector(node,updateTables_);
	//do other stuff specific to update
}

std::vector<tstore::ViewTable>::iterator tstore::SQLViewInternal::beginUpdateTables() {
	return updateTables_.begin();
}

std::vector<tstore::ViewTable>::iterator tstore::SQLViewInternal::endUpdateTables() {
	return updateTables_.end();
}

std::vector<tstore::ViewTable>::iterator tstore::SQLViewInternal::beginInsertTables() {
	return insertTables_.begin();
}

std::vector<tstore::ViewTable>::iterator tstore::SQLViewInternal::endInsertTables() {
	return insertTables_.end();
}

//delete in reverse order from inserting, so that there won't be problems with key constraints
//later there might be a way to specify delete tables separately in the configuration
std::vector<tstore::ViewTable>::reverse_iterator tstore::SQLViewInternal::beginDeleteTables() {
	return insertTables_.rbegin();
}

std::vector<tstore::ViewTable>::reverse_iterator tstore::SQLViewInternal::endDeleteTables() {
	return insertTables_.rend();
}


//methods for accessing some of the internal data used to create the queries
//these are so that information about the view can be displayed, they are not necessary for actually performing a query
std::string tstore::SQLViewInternal::rawQueryString() {
	return query_;
}

std::map<const std::string,std::string> tstore::SQLViewInternal::queryParameters() {
	std::map<const std::string,std::string> allParameters;
	allParameters.insert(queryParameters_.begin(),queryParameters_.end());
	allParameters.insert(queryBindParameters_.begin(),queryBindParameters_.end());
	return allParameters;
}

bool tstore::SQLViewInternal::isBindParameter(std::string parameter) {
	return queryBindParameters_.count(parameter)!=0;
}

void tstore::SQLViewInternal::query(TStoreAPI *API) throw (tstore::exception::Exception) {
	try {
		xdata::Table results;
		std::vector<std::string> bindValues;
		const std::string queryString=this->queryString(&bindValues);
		tstore::MappingList mappings;
		std::string tableName=simpleQueryTableName_; //the table name to use the mappings of. If we know the database table name, use that (since a table by that name would have been added for any exact-match mappings)
		if (tableName.empty()) {
			//if they have an SQL query, and they have a table definition in the config, then the table definition had better
			//be named after the subview and with have the columns returned from a query (rather than just one table, when we don't know which columns come from it)
			//in order to actually get into this situation they would have to have modified the config themselves anyway, since the addTable message would have actually created a table with those columns
			tableName=name();
		}
		API->getMappingsForTable(queryTable_,mappings);
		//std::cout << "query: " << queryString << std::endl;
		tstore::SQLQuery query(queryString,bindValues,tableName);
		API->connection()->setExpectedRowCount(queryExpectedRows_);
		API->connection()->execute(query,results,mappings);
		addResultTable(API,results);
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception, e.message(), e);
	}
}

bool tstore::SQLViewInternal::getTable(TStoreAPI *API,xdata::Table &table) {
	//in case more than one operation is being performed in a single SOAP message,
	//identify the table by the table name (perhaps should also include the enclosing view and the operation being performed
	//in order to make sure it is unique)
	if (API->getTable(table,name())) return true;
	return API->getFirstTable(table);
}

void tstore::SQLViewInternal::addResultTable(TStoreAPI *API,xdata::Table &table) {
	//in case more than one operation is being performed in a single SOAP message,
	//identify the table by the table name (perhaps should also include the enclosing view and the operation being performed
	//in order to make sure it is unique)
	API->addResultTable(table,name());
}

void tstore::SQLViewInternal::update(TStoreAPI *API) throw (tstore::exception::Exception) {
	std::vector<tstore::ViewTable>::iterator updateIterator;
	xdata::Table rowsToUpdate;
	if (getTable(API,rowsToUpdate)) {
		try {
			for (updateIterator=beginUpdateTables();updateIterator!=endUpdateTables();updateIterator++) {
				tstore::MappingList mappings;
				API->getMappingsForTable((*updateIterator).tableName(),mappings);
				tstore::SQLUpdate update((*updateIterator).columns(),(*updateIterator).tableName());
				update.data=&rowsToUpdate;
				API->connection()->execute(update,mappings);
			}
			API->connection()->commit();
		} catch (tstore::exception::Exception &e) {
			API->connection()->attemptRollback(); //if one fails, roll back the updates of all the tables
			XCEPT_RETHROW (tstore::exception::Exception, "Could not update "+(*updateIterator).tableName()+". "+e.message(), e);
		}
	} else {
		XCEPT_RAISE(tstore::exception::Exception, "No data given");
	}
}

void tstore::SQLViewInternal::addTableNamesToCollection(std::set<std::string> &allTables,const std::vector<tstore::ViewTable> &tables) {
	for (std::vector<tstore::ViewTable>::const_iterator table=tables.begin();table!=tables.end();++table) {
		if (!allTables.count((*table).tableName())) allTables.insert((*table).tableName());
	}
}

std::vector<std::string> tstore::SQLViewInternal::allTables() {
	std::set<std::string> allTables;
	//get names of all tables in insert, update or simple queries
	//delete each one exactly once. This does not delete tables used in SQL queries
	if (!simpleQueryTableName_.empty()) allTables.insert(simpleQueryTableName_); //perhaps should be an error if the query is not simple, since we can't tell which tables it might query.
	addTableNamesToCollection(allTables,insertTables_);
	addTableNamesToCollection(allTables,updateTables_);
	std::vector<std::string> tableVector(allTables.begin(),allTables.end());
	return tableVector;
}

void tstore::SQLViewInternal::destroy(TStoreAPI *API) throw (tstore::exception::Exception) {
	std::vector<std::string> allTables(this->allTables());
	try {	
		for (std::vector<std::string>::iterator table=allTables.begin();table!=allTables.end();++table) {
			API->connection()->dropTable(*table);
		}
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not drop all tables of "+name(),e);
	}
}

void tstore::SQLViewInternal::remove(TStoreAPI *API) throw (tstore::exception::Exception) {
	std::vector<tstore::ViewTable>::reverse_iterator tableIterator;
	xdata::Table rowsToDelete;
	if (getTable(API,rowsToDelete)) {
		try {
			for (tableIterator=beginDeleteTables();tableIterator!=endDeleteTables();tableIterator++) {
				tstore::SQLDelete deletion((*tableIterator).tableName());
				deletion.data=&rowsToDelete;
				API->connection()->execute(deletion);
			}
			API->connection()->commit();
		} catch (tstore::exception::Exception &e) {
			API->connection()->attemptRollback(); //if one fails, roll back the deletes of all the tables
			XCEPT_RETHROW (tstore::exception::Exception, "Could not delete from "+name()+". "+e.message(), e);
		}
	} else {
		XCEPT_RAISE(tstore::exception::Exception, "No data given");
	}
}

void tstore::SQLViewInternal::clear(TStoreAPI *API) throw (tstore::exception::Exception) {
	std::vector<tstore::ViewTable>::reverse_iterator tableIterator;
	try {
		for (tableIterator=beginDeleteTables();tableIterator!=endDeleteTables();tableIterator++) {
			tstore::SQLDelete deletion((*tableIterator).tableName());
			API->connection()->execute(deletion);
		}
		API->connection()->commit();
	} catch (tstore::exception::Exception &e) {
		API->connection()->attemptRollback(); //if one fails, roll back the deletes of all the tables
		XCEPT_RETHROW (tstore::exception::Exception, "Could not clear "+name()+". "+e.message(), e);
	}
}

bool tstore::SQLViewInternal::tableMatchesDefinition(TStoreAPI *API,xdata::Table &input) {
	try {
		xdata::Table definition;
		getDefinition(API,definition);
		return definition.getTableDefinition()==input.getTableDefinition();
		//if people are required to get the definition before inserting, then the column names and types must match exactly.
		//however an insert will actually work if the column names have the wrong capitalisation, so we'll let themn get away with it
		/*std::map<std::string, std::string, std::less<std::string> > definitionColumns=definition.getTableDefinition();
		std::map<std::string, std::string, std::less<std::string> > inputColumns=input.getTableDefinition();
		if (inputColumns.size()!=definitionColumns.size()) return false;
		std::map<std::string, std::string, std::less<std::string> >::iterator definitionColumn;
		std::map<std::string, std::string, std::less<std::string> >::iterator inputColumn;
		//since maps are sorted we can just compare them one by one
		for (definitionColumn=definitionColumns.begin(),inputColumn=inputColumns.begin();
			definitionColumn!=definitionColumns.end() && inputColumn!=inputColumns.end();
			definitionColumn++,inputColumn++) {
			if (toolbox::tolower((*definitionColumn).first)!=toolbox::tolower((*inputColumn).first)) return false;
			if ((*definitionColumn).second!=(*inputColumn).second) return false;
		}
		return true;*/
	} catch (std::exception &e) {
		//this is just a check so we can give a better error message.
		//if we can't get the definition, then we can just continue with the insert/update and see if there is an error.
		std::cout << e.what() << std::endl;
		return true;
	}
}


void tstore::SQLViewInternal::insert(TStoreAPI *API) throw (tstore::exception::Exception) {
	xdata::Table newRows;
	std::vector<tstore::ViewTable>::iterator insertIterator;
	if (getTable(API,newRows)) {
		if (tableMatchesDefinition(API,newRows)) {
			try {
				for (insertIterator=beginInsertTables();insertIterator!=endInsertTables();insertIterator++) {
					tstore::MappingList mappings;
					API->getMappingsForTable((*insertIterator).tableName(),mappings);
					tstore::SQLInsert insert((*insertIterator).columns(),(*insertIterator).tableName());
					insert.data=&newRows;
					API->connection()->execute(insert,mappings);
				}
				API->connection()->commit();
			} catch (tstore::exception::Exception &e) {
				API->connection()->attemptRollback(); //if one fails, roll back the updates of all the tables
				XCEPT_RETHROW (tstore::exception::Exception, "Could not insert into "+(*insertIterator).tableName()+". "+e.message(), e);
			}
		} else {
			XCEPT_RAISE(tstore::exception::Exception, "Columns in the data supplied do not match those of the table definition.");
		}
	} else {
		XCEPT_RAISE(tstore::exception::Exception, "No data given");
	}
}

//while the column names in the database are case-insensitive, the SQLView uses the capitalisation given in the configuration file
//for table definitions and simple queries
std::string tstore::SQLViewInternal::caseForColumnName(const std::string &columnName) {
	if (insertColumnNames_.count(toolbox::toupper(columnName))) {
		return insertColumnNames_[toolbox::toupper(columnName)];
	} else {
		return columnName; //if the table name is in the insert section but no columns are explicitly mentioned, then we should just use the column name as it is
	}
}

void tstore::SQLViewInternal::getDefinition(TStoreAPI *API,xdata::Table &results) throw (tstore::exception::Exception) {
	std::set<std::string> columnsAdded;
	std::vector<tstore::ViewTable>::iterator tableIterator;
	try {
		for (tableIterator=beginInsertTables();tableIterator!=endInsertTables();tableIterator++) {
			std::map<const std::string,std::string> types;
			tstore::SQLUpdate update((*tableIterator).columns(),(*tableIterator).tableName());
			tstore::MappingList mappings;
			API->getMappingsForTable((*tableIterator).tableName(),mappings);
			API->connection()->getColumnTypes(update.tableName(),types,mappings);
			std::map<const std::string,std::string>::iterator columnIterator;
			for (columnIterator=types.begin();columnIterator!=types.end();columnIterator++) {
				std::string columnName=(*columnIterator).first;
				if (update.shouldChangeColumn(columnName)) {
					columnName=caseForColumnName(columnName);
					if (columnsAdded.count(columnName)==0) { //a column with the same name might be updated in more than one table. We only need to add the column to the definition once.
						columnsAdded.insert(columnName);
						std::string columnType=(*columnIterator).second;
						results.addColumn(columnName,columnType);
					}
				}
			}
		}
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception, "Could not get definition for table "+name()+". "+e.message(),e);
	}
}

void tstore::SQLViewInternal::definition(TStoreAPI *API) throw (tstore::exception::Exception) {
	xdata::Table results;
	getDefinition(API,results);
	addResultTable(API,results);
}

bool tstore::SQLViewInternal::canCreate() const {
	return !createStatements_.empty();
}
	
void tstore::SQLViewInternal::create(TStoreAPI *API) throw (tstore::exception::Exception) {
	std::vector<std::string>::iterator statementIterator;
	try {
		for (statementIterator=createStatements_.begin();statementIterator!=createStatements_.end();statementIterator++) {
			tstore::SQLStatement statement(*statementIterator);
			API->connection()->execute(statement);
		}
		API->connection()->commit();
	} catch (tstore::exception::Exception &e) {
		API->connection()->attemptRollback();
		XCEPT_RAISE(tstore::exception::Exception, "Could not create "+name()+". Oracle said: "+e.what());
	}
}

void tstore::SQLViewInternal::addConfiguration(const std::string &name,DOMNode *node) {
	if (name=="query") addQuery(node);
	else if (name=="insert") addInsert(node);
	else if (name=="update") addUpdate(node);
	//else if (name=="create") addCreate(node);
	else if (name!="#text" && name!="") {
		//there is a node we don't understand. Perhaps we should throw an exception, but it would be nice if newer config files
		//with extra values could be used with older versions, so for now let's just log the error.
		std::cout << "Unknown view node: " << name << std::endl;
	}
}
