// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A.Forrest and C.Wakefield AND L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_DocumentFilter_h_
#define _tstore_DocumentFilter_h_

#include "toolbox/string.h"
#include "xoap/SOAPElement.h"
#include "tstore/exception/Exception.h"

namespace tstore
{
	class DocumentFilter
	{
		public:

			DocumentFilter (const std::string& filterExpression);

			DocumentFilter (xoap::SOAPElement& element);

			std::string getFilterExpression ();

			/*! Apply filter expression to the \param message provided and
			 \returns true if the filter matches any of the elements in
			 the message. \returns false if no element in the \param message
			 matches the expression.
			 */
			bool match (DOMDocument* doc) throw (tstore::exception::Exception);

			/*! Apply filter expression to the \param message provided and
			 \returns true if the filter matches any of the elements in
			 the message. \returns false if no element in the \param message
			 matches the expression.
			 */
			std::string evaluate (DOMDocument* doc) throw (tstore::exception::Exception);

			/*! Apply filter expression to the \param message provided. Return a list of
			 nodes in the message that match the filter expression. The returned list
			 may be empty if no node matched the filter expression.
			 */
			std::list<xoap::SOAPElement> extract (DOMDocument* doc) throw (tstore::exception::Exception);

		protected:

			std::string filter_;
	};
}

#endif
