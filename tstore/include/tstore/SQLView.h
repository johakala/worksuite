/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_SQLView_h_
#define _tstore_SQLView_h_


#include <string>
#include <map>

#include "xoap/DOMParser.h"
#include "xoap/domutils.h"
#include "tstore/View.h"
#include "tstore/SQLViewInternal.h"

namespace tstore 
{

	typedef std::map<const std::string,tstore::SQLViewInternal> SubviewList;

	class SQLView : public tstore::View 
	{
		public:
		//standard View interface
		SQLView(std::string configurationPath,std::string name) throw (tstore::exception::Exception);
		virtual ~SQLView() {}
		virtual bool canQuery() const;
		virtual bool canUpdate() const;
		virtual bool canInsert() const;
		virtual bool canCreate() const;
		virtual bool canRemove() const;
		virtual bool canDestroy() const;

		virtual void create(TStoreAPI *API) throw (tstore::exception::Exception);
	   	virtual void query(TStoreAPI *API) throw (tstore::exception::Exception);
	    	virtual void update(TStoreAPI *API) throw (tstore::exception::Exception);
	    	virtual void insert(TStoreAPI *API) throw (tstore::exception::Exception);
	    	virtual void definition(TStoreAPI *API) throw (tstore::exception::Exception);
	    	virtual void clear(TStoreAPI *API) throw (tstore::exception::Exception);
	   	 virtual void remove(TStoreAPI *API) throw (tstore::exception::Exception);
		virtual void destroy(TStoreAPI *API) throw (tstore::exception::Exception);
		virtual void addTables(TStoreAPI *API) throw (tstore::exception::Exception);
		virtual void removeTables(TStoreAPI *API) throw (tstore::exception::Exception);
		virtual void setParameter(const std::string &parameterName,const std::string &value) throw (tstore::exception::InvalidView);
		virtual void resetParameters();
		virtual std::vector<std::string> tablesToDestroy();
		std::string namespaceURI();
		private:
		tstore::SQLViewInternal *currentSubview_;
		tstore::SubviewList subViews_;
		std::map<const std::string,std::string> parameters_;
		bool configure_;
		tstore::SQLViewInternal &getSubview() throw (tstore::exception::Exception);
		void throwOperationNotSupported(const std::string &operation,const std::string &tableName) throw (tstore::exception::InvalidView);
		tstore::SQLViewInternal &getSubviewWithName(const std::string &name) throw (tstore::exception::InvalidView);
		void removeSubviewWithName(DOMNode *viewNode,const std::string &name) throw (tstore::exception::Exception);
		void removeSubviewWithName(const std::string &name);
		tstore::SQLViewInternal &createSubviewWithName(const std::string &name);
		bool subviewExists(const std::string &name);
	};

}

#endif
