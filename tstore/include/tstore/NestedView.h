/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_NestedView_h_
#define _tstore_NestedView_h_

#include "View.h"

namespace tstore 
{
typedef std::multimap<const std::string,std::map<const std::string,std::string> > constraintList;

	class NestedView : public tstore::View 
	{
		public:
		//standard View interface
		NestedView(std::string configurationPath,std::string name) throw (tstore::exception::Exception);
		virtual ~NestedView() {}
		virtual bool canQuery() const;
		virtual bool canUpdate() const;
		virtual bool canInsert() const;
		virtual bool canCreate() const;
		virtual bool canRemove() const;

		virtual void create(TStoreAPI *API) throw (tstore::exception::Exception);
	    	virtual void query(TStoreAPI *API) throw (tstore::exception::Exception);
	   	virtual void insert(TStoreAPI *API) throw (tstore::exception::Exception);
	   	virtual void definition(TStoreAPI *API) throw (tstore::exception::Exception);
	    	virtual void clear(TStoreAPI *API) throw (tstore::exception::Exception);
	    	virtual void remove(TStoreAPI *API) throw (tstore::exception::Exception);
		virtual void setParameter(const std::string &parameterName,const std::string &value) throw (tstore::exception::InvalidView);
		void removeTables(TStoreAPI *API) throw (tstore::exception::Exception);
		void addTables(TStoreAPI *API) throw (tstore::exception::Exception);
		std::string namespaceURI();
		virtual void resetParameters();
		private:
		void separateParameterValues(std::pair<parameterList,parameterList> &parameters) throw (tstore::exception::Exception);
		void sortParameterValues(std::pair<parameterList,parameterList> &parameters) throw (tstore::exception::Exception);
		void parseWhereClause(DOMNode *node,const std::string &tableName) throw (tstore::exception::Exception);
		bool keysMatch(const std::string &keys1,const std::string &keys2);
		xdata::Table &constraints(tstore::Connection *connection) throw (tstore::exception::Exception);
		void addConstraintToList(constraintList &constraintsForTable,const std::string &tableName,std::map<const std::string,std::string> &joinColumns);
		void getConstraintsForTable(tstore::Connection *connection,const std::string &tableName,constraintList &constraintsForTable,bool bothWays=true) throw (tstore::exception::Exception);
		void createTable(TStoreAPI *API,const std::string &tableName,const std::string &primaryKey,xdata::Table &definition,std::map<const std::string,std::string> &tablesCreated) throw (tstore::exception::Exception);
		void checkTableSpecified() throw (tstore::exception::Exception);
		
		bool parentColumnExists
		(
			std::map<std::string, std::string, xdata::Table::ci_less > &columns,
			const std::string &columnName
		);
		
		void addColumn(std::vector<std::string> &columns,const std::string &newColumn,const std::string &name) throw (tstore::exception::Exception);
		
		void splitParentColumn
		(
			std::string &parentColumn,
			std::string &childColumn,
			std::string &source,
			std::map<std::string, std::string, xdata::Table::ci_less > &columns
		) 
		throw (tstore::exception::Exception);
		
		void parseChildColumnName
		(
			std::string &tableName,
			std::map<std::string, std::string, xdata::Table::ci_less> &linkedColumns,
			const std::string &columnName,
			std::map<std::string, std::string, xdata::Table::ci_less> &columns
		) 
		throw (tstore::exception::Exception);
		
		void addTable(TStoreAPI *API,xdata::Table &definition,const std::string &tableName,std::map<const std::string,std::string> &tablesCreatedWithKeys,std::vector<std::string> &tablesCreated,const std::string &primaryKey="") throw (tstore::exception::Exception);
		void removeTable(TStoreAPI *API,const std::string &tableName,std::set<std::string> &tablesRemoved) throw (tstore::exception::Exception);
		void checkForExistingColumn(xdata::Table &results,const std::string &tableName,const std::string &columnName) throw (tstore::exception::Exception);
		std::string columnNameForChildColumn(const std::string &tableName,std::map<const std::string,std::string> &joinColumns) throw (tstore::exception::Exception);
		void setValueNULL(tstore::SQLQuery &query,std::pair<const std::string,std::string> &joinColumn);
		std::string condition(std::pair<const std::string,std::string> &joinColumn);
		std::string selectStatementForChildColumn(const std::string &tableName,std::map<const std::string,std::string> &joinColumns) throw (tstore::exception::Exception);
		void getSubTables(xdata::Table &results,TStoreAPI *API,unsigned int recursionDepth,const std::string &parentTableName="") throw (tstore::exception::Exception);
		void runSubQuery(xdata::Table &results,TStoreAPI *API,const std::string &tableName,std::map<const std::string,std::string> &joinColumns,unsigned int recursionDepth) throw (tstore::exception::Exception);
		void getChildTableDefinition(TStoreAPI *API,xdata::Table &results,const std::string &tableName,std::set<std::string> &columnsToExclude,unsigned int recursionDepth) throw (tstore::exception::Exception);
		void insertChildTables(TStoreAPI *API,const std::string &tableName,xdata::Table &newRows,unsigned int recursionDepth) throw (tstore::exception::Exception,tstore::exception::Exception);
		void insert(TStoreAPI *API, const std::string &tableName,
				xdata::Table &newRows,
				xdata::Table &keyValues, //the values of key columns, which we already know from the parent table
				unsigned int recursionDepth)
			throw (tstore::exception::Exception);

		xdata::Table constraints_;
		bool constraintsLoaded_;
		std::string mainTable_;
		std::string whereClause_;
		unsigned int recursionDepth_;
		unsigned int queryCount_; //just for debug info
		std::map<const std::string,unsigned int> defaultDepths_;
		std::map<const std::string,std::string> whereClauses_;
		bool depthSet_;
		parameterList tableParameters_; //the current parameters for the current table
		std::map<const std::string,std::pair<parameterList,parameterList> > defaultTableParameters_; //default parameters configured for all tables
	};

}

#endif
