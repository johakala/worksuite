The purpose of ferol_lib_extractor is to refactor the daq/ferol library so that all the shared classes that have been pulled 
out into daq/d2s/utils are linked from there rather than built directly into this project.

To use the script ferol_lib_extractor, place it in the root directory of the ferol code and run it from there.
Currently all the helper files are in an afs public directory, this will work if you run it on a machine with access to afs folders. 

The script has no progress report and takes between 5 and 10 mins depending on the speed of the machine you use. 
Just let it run silently and complete. don't stop it half way through for obvious reasons. 
Start from a clean checkout of the daq/ferol code.
This was tested on revision 32616 of daq/ferol. I cannot guarantee that it will work on later revisions of the code.

There are three files needed to complete the extraction process, which can be found in the ferol_resources folder here. 
If running the script on AFS, these files should be automatically pulled from /afs/cern.ch/user/h/hcooke/public/ferolfiles, 
and copied to the correct locations in the ferol software. If not on AFS, or if this doesn't work for any reason, the path to 
the files is set at the beginning of the script and can be changed to point to the local ferol_resources folder.

When the script runs, it will create a file in the ferol root dir called deprecated_files.dat. This is a list of all the files 
(and paths from root dir) that are no longer needed in the ferol code, as the d2s/utils versions are now in use. For safety, 
the script does not remove any of these files itself, but this list is to allow easy removal of them if wanted. i.e. 
'for file in deprecated_files.dat; do; svn rm $file; done'

Once it has completed, it will write a file containing a list of all the files that can safely be deleted from the project.

NOTE: This script does not edit any version information. I leave that up to the user.

