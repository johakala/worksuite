#include "ferol/HardwareLocker.hh"
#include "ferol/ferolConstants.h"
#include "ferol/loggerMacros.h"


// 33 locks: 
// [0] is not used.
// [1]-[16] for slot 1-16 in ferol mode.
// [17]-[33] for 16 possible MOL units. 
ferol::HardwareLocker::HardwareLocker( Logger &logger, 
        ApplicationInfoSpaceHandler &appIS,
        StatusInfoSpaceHandler &statusIS)
    : utils::HardwareLocker(logger, dynamic_cast<utils::InfoSpaceHandler&>(appIS), dynamic_cast<utils::InfoSpaceHandler&>(statusIS), LOCKFILE, LOCK_PROJECT_ID) {}

    // returns true if
    //  - initally the status was "unlocked"
    //  - and the lock could be taken successfully.
    // returns false in all other cases.
    //
    // E.g. if initially the lockstatus is unknown (since the slot/unit 
    // was not yet set) this routing never succeeds.
bool ferol::HardwareLocker::lock()
{
    updateLockStatus();
    // lock if we are unlocked and there is the need to lock. 
    // otherwise leave unlocked to give the hardware to minidaq or others.
    if ( (lockStatus_ == "unlocked") &&
            ( appIS_.getbool( "enableStream0" )  || appIS_.getbool( "enableStream1" )  )
       )

    {
        bool res = hwLock_P->takeIfUnlocked( slotunit_ );
        updateLockStatus();
        return res;
    }

    else

    {
        return false;
    }
}
