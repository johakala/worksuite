#include "d2s/utils/WebTableTab.hh"

utils::WebTableTab::WebTableTab(  std::string name, 
        std::string description, 
        uint32_t columns,
        utils::Monitor &monitor )
: monitor_ (monitor)
{
    isAlt_ = false;
    name_ = name;
    description_ = description;
    columns_ = columns;
    localMon_ = false;
    levels_.insert(1);
    //for (uint32_t i = 1; i <= 9; i++) levels_.insert(i);
}

    void
utils::WebTableTab::registerTable( std::string name,
        std::string description,
        std::string itemSetName )
{
    WebTable webtable( name, description, itemSetName, monitor_ );
    tableList_.push_back( webtable );
}

    void
utils::WebTableTab::processLevels()
{
    levels_.clear();
    levels_.insert(1);
    for (std::list<WebTable>::iterator it = tableList_.begin(); 
            it != tableList_.end(); it++)
    {
        std::string set = it->getItemSetName();
        if ( set == "" ) continue;
        std::list< std::pair<std::string, utils::Monitor::Item> >
            items = monitor_.getItems(set);

        for (std::list< std::pair<std::string, utils::Monitor::Item> >::iterator
                it = items.begin(); it != items.end(); it++)
        {
            utils::Monitor::Item item = it->second; 
            uint32_t level = item.is->getItem(item.name)->level;
            if (level > 0) levels_.insert(level);
        }
    }
}

    void
utils::WebTableTab::print( std::ostream *out )
{
    uint32_t icol = 1;
    std::string isChecked = localMon_ ? " checked" : "";
    std::string dropdownClass = "dropdownContainer";
    std::string switchClass = "pageright level-control";
    if ( localMon_ || (*levels_.rbegin()) == 1 )
    {
        dropdownClass += " hidden";
    }
    if ( (*levels_.rbegin()) == 1 )
    {
        switchClass += " hidden";
    }
    *out << "\n\
        <div class=\"tab-page\" id=\"page1\">\n\
        <h2 class=\"tab\">" << name_ << "</h2>\n\
        \n\
        <div class=\"toprow\">\n\
        <div class=\"tabdescription\">\n" 
        << description_ << "\n\
        </div>\n\
        \n\
        <div class=\"" << switchClass << "\">\n\
        <p>Increased level control</p>\n\
        <label class=\"switch\">\n\
        <input type=\"checkbox\" name=\"" << name_
        << "\" onchange=\"levelToggle(this)\"" << isChecked << ">\n\
        <span class=\"slider\"></span>\n\
        </label>\n\
        </div>\n\
        \n\
        </div>\n\
        \n\
        <div class=\"" << dropdownClass << "\">\n\
        <select name=\"" << name_ << "\" onchange=\"globalLevelChange(this)\">\n";
    for (std::set<uint32_t>::iterator it = levels_.begin(); it != levels_.end();
            it++)
    {
        uint32_t i = (*it);
        *out << "<option value=\"" << i << "\" ";
        if ( i == globalLevel_ )
            *out << "selected=\"selected\" ";
        *out << ">" << i << "</option>\n";    
    }
    *out << "</select>\n</div>\n";

    if (!isAlt_)
    {
        *out << "<table class=\"tabtable\" id=\"" << name_ << "\">\n";
        std::list< WebTable >::iterator it;
        for ( it = tableList_.begin(); it != tableList_.end(); it++ ) 
        {

            if ( icol == 1 )
            {
                *out << "<tr>\n";
            }
            *out << "  <td>\n";
            (*it).print( out );
            *out << " </td>\n";
            if ( icol == columns_ ) 
            {
                *out << "</tr>\n";
                icol = 1;
            }
            else 
            {
                icol ++;
            }
        }

        *out << "</table>\n";
    }
    else
    {
        *out << "<div class=\"tablesContainer\">\n";
        std::list< WebTable >::iterator it;
        for ( it = tableList_.begin(); it != tableList_.end(); it++ ) 
        {
            (*it).print( out, isAlt_ );
        }

        *out << "</div>\n";
    }

    *out << "</div>\n";
}


    void 
utils::WebTableTab::jsonUpdate( std::ostream *out )
{
    std::list< WebTable >::iterator it;
    for ( it = tableList_.begin(); it != tableList_.end(); it++ )
    {
        (*it).jsonUpdate( out );
    }            
}

    bool
utils::WebTableTab::isLocalMonitoring()
{
    return localMon_;
}

    void
utils::WebTableTab::isLocalMonitoring(bool localMon)
{
    localMon_ = localMon;
    for (std::list<WebTable>::iterator it = tableList_.begin();
            it != tableList_.end(); it++)
    {
        it->isLocalMonitoring(localMon);
    }
}

    std::string
utils::WebTableTab::getName()
{
    return name_;
}

    void
utils::WebTableTab::setGlobalLevel(uint32_t level)
{
    globalLevel_ = level;

    for (std::list<WebTable>::iterator it = tableList_.begin();
            it != tableList_.end(); it++)
    {
        it->setItemSetLevel(level);
    }
}

    uint32_t
utils::WebTableTab::getGlobalLevel()
{
    return globalLevel_;
}

    bool
utils::WebTableTab::isAlternate()
{
    return isAlt_;
}

    void
utils::WebTableTab::isAlternate(bool isAlt)
{
    isAlt_ = isAlt;
}
