#ifndef __InfoSpaceUpdater
#define __InfoSpaceUpdater

namespace utils
{
    class InfoSpaceHandler;
    
    class InfoSpaceUpdater {

    public:
        
        virtual bool updateInfoSpace( InfoSpaceHandler *is, uint32_t streamNo = 0 ) = 0;
        virtual ~InfoSpaceUpdater() {};
        
    };
}

#endif /* __InfoSpaceUpdater */
