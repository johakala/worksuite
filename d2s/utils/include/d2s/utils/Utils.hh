#ifndef __d2s_utils_Utils
#define __d2s_utils_Utils

#include <map>
#include <string>
#include "stdint.h"
//#include "inttypes.h"

namespace utils {
    // Helper method to parse a fedEnableMask string into an easy map.
    typedef std::map<uint16_t, uint8_t> fedEnableMask;
    fedEnableMask parseFedEnableMask(std::string const& fedEnableMask);
}
#endif
