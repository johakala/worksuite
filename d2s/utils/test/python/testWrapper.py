#!/usr/bin/python

import ctypes
import time

def main():

    #load the D2S_Utils shared object library
    TestLib = ctypes.cdll.LoadLibrary('/opt/xdaq/lib/libD2S_Utils.so')

    #call the test function hellowWorld
    TestLib.helloWorld()

    #try to lock a Ferol in slot 4
    print (TestLib.lockFerol(4))
    time.sleep(10)
    
    #try to unlock a Ferol in slot 4
    print (TestLib.unlockFerol(4))
    


if __name__ == '__main__':
    main()
