#ifndef __RAMix139
#define __RAMix139

#include "VMEDevice.hh"

using namespace HAL;

class RAMix139 : public VMEDevice {
public:
  RAMix139( VMEAddressTable & vmeAddressTable,
            VMEBusAdapterInterface & vmeBusAdapter,
            unsigned long baseaddress );
  
  void initialize( unsigned long VMELowAddress,
                   unsigned long VMEHighAddress );

};

#endif /* __RAMix139 */
