#include "RAMix139.hh"

RAMix139::RAMix139( VMEAddressTable & vmeAddressTable,
                    VMEBusAdapterInterface & vmeBusAdapter,
                    unsigned long baseaddress ) 
  : VMEDevice( vmeAddressTable,
               vmeBusAdapter,
               baseaddress ) {
}

void RAMix139::initialize( unsigned long lowAddress,
                           unsigned long highAddress ) {
  unsigned long lowBits, highBits;
  if ( (lowAddress >= highAddress) ||
       (lowAddress & 0x000fffff != 0 ) ||
       (highAddress & 0x000fffff != 0) ) {
    cout << "ERROR in parameters of initialize --> abort\n" 
         << " lowAddress : " << hex << lowAddress 
         << "\n highAddress : " << hex << highAddress << endl;
  }
  cout << "first access in initialize" << endl;
  lowBits = lowAddress >> 20;
  highBits = highAddress >> 20;
  write("VMELowLimit", lowBits   );
  write("VMEHighLimit",highBits   );
  write("A24D32AM",0x39);
  write("A24D32BlockAM",0x3b);
  write("A24D64BlockAM",0x38);
  write("A32D32AM",0x09);
  write("A32D32BlockAM",0x0b);
  write("A32D64BlockAM",0x08);
  resetBit("ParityEnable");
}
