#ifndef _CMSDAQFEROL40HARDCODEDADDRESSTABLEREADER_H
#define _CMSDAQFEROL40HARDCODEDADDRESSTABLEREADER_H

/**
 *     @class CMSDAQFEROL40HardcodedAddressTableReader
 *
 *     @short a hard-coded address table for the CMSDAQFEROL40
 *
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.1 $
 *     $Date: 2007/03/27 08:02:32 $
 *
 *
 **/

#include "hal/PCIAddressTableDynamicReader.hh"

namespace d2s {
  
  class CMSDAQFEROL40HardcodedAddressTableReader : public HAL::PCIAddressTableDynamicReader {

  public:
    CMSDAQFEROL40HardcodedAddressTableReader();
  };

}


#endif
