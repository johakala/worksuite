/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/03/28 12:53:52 $
 *
 *
 **/
#include "d2s/gtpe/version.h"

#include "tts/ttsbase/version.h"
#include "tts/ipcutils/version.h"

#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(d2sgtpe)

void d2sgtpe::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(ttsttsbase);  
	CHECKDEPENDENCY(ttsipcutils);  

	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
}

std::set<std::string, std::less<std::string> > d2sgtpe::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,ttsttsbase); 
	ADDDEPENDENCY(dependencies,ttsipcutils); 

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	 
	return dependencies;
}	
	
