/**
*      @file GTPeStatus.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 08:02:35 $
*
*
**/
#include "d2s/gtpe/GTPeStatus.hh"


d2s::GTPeStatus::GTPeStatus(uint32_t reg_tts_status, uint32_t partstat) 
  : _reg_tts_status(reg_tts_status),
    _partstat(partstat) {
}

d2s::GTPeStatus::~GTPeStatus() {
}
    
tts::TTSState d2s::GTPeStatus::getDetPartitionInputState(uint32_t part_idx)
  throw (xcept::Exception) {

  if (part_idx > 7) 
    XCEPT_RAISE(xcept::Exception, "partition index out of range");

  return ttsStateFromGTPeInputState( ( _reg_tts_status & (0x3<<(2*part_idx+16)) ) >> (2*part_idx+16) );
}
    
tts::TTSState d2s::GTPeStatus::getDAQPartitionInputState(uint32_t part_idx)
  throw (xcept::Exception) {

  if (part_idx > 7) 
    XCEPT_RAISE(xcept::Exception, "partition index out of range");

  return ttsStateFromGTPeInputState( ( _reg_tts_status & (0x3<<(2*part_idx)) ) >> (2*part_idx) );
}

tts::TTSState d2s::GTPeStatus::getTCSPartitionState(uint32_t part_idx)
  throw (xcept::Exception) {

  if (part_idx > 7) 
    XCEPT_RAISE(xcept::Exception, "partition index out of range");

  return ttsStateFromGTPeOutputState( ( _partstat & (0x3<<(2*part_idx+16)) ) >> (2*part_idx+16) );
}

bool d2s::GTPeStatus::isTCSPartitionActive(uint32_t part_idx)
  throw (xcept::Exception) {

  if (part_idx > 7) 
    XCEPT_RAISE(xcept::Exception, "partition index out of range");

  return _partstat & (1<<part_idx);
}

bool d2s::GTPeStatus::isLinkDown() {

  return _partstat & 0x0000200;

}
    
bool d2s::GTPeStatus::isLinkFull() {

  return _partstat & 0x0000100;

}
    
bool d2s::GTPeStatus::isBufferFull() {

  return _partstat & 0x0000400;

}

tts::TTSState d2s::GTPeStatus::ttsStateFromGTPeInputState(uint32_t gtpestate) {

  switch (gtpestate) {
  case 0x0: return tts::TTSState::READY;
  case 0x1: return tts::TTSState::BUSY;
  case 0x2: return tts::TTSState::WARNING;
  case 0x3: return tts::TTSState::DISCONNECT1;
  default: return 16;
  }

}

tts::TTSState d2s::GTPeStatus::ttsStateFromGTPeOutputState(uint32_t gtpestate) {

  switch (gtpestate) {
  case 0x0: return tts::TTSState::READY;
  case 0x1: return tts::TTSState::BUSY;
  case 0x2: return tts::TTSState::WARNING;
  case 0x3: return tts::TTSState::IDLE;
  default: return 16;
  }

}
