/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/03/28 12:53:53 $
 *
 *
 **/
#include "d2s/gtpecontroller/version.h"
#include "d2s/gtpe/version.h"

#include "tts/ttsbase/version.h"
#include "tts/ipcutils/version.h"

#include "config/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xgi/version.h"
#include "xoap/version.h"


GETPACKAGEINFO(d2sgtpecontroller)

void d2sgtpecontroller::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(d2sgtpe);  

	CHECKDEPENDENCY(ttsttsbase);  
	CHECKDEPENDENCY(ttsipcutils);  

	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xdata);  
	CHECKDEPENDENCY(xdaq);  
	CHECKDEPENDENCY(xgi);  
	CHECKDEPENDENCY(xoap);  

}

std::set<std::string, std::less<std::string> > d2sgtpecontroller::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,d2sgtpe); 

	ADDDEPENDENCY(dependencies,ttsttsbase); 
	ADDDEPENDENCY(dependencies,ttsipcutils); 

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xdaq);
	ADDDEPENDENCY(dependencies,xgi);
	ADDDEPENDENCY(dependencies,xoap);
	 
	return dependencies;
}	
	
