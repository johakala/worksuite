/**
*      @file GTPeCounterMonitor.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.9 $
*     $Date: 2007/03/28 12:53:53 $
*
*
**/
#include "d2s/gtpecontroller/GTPeCounterMonitor.hh"

#include "d2s/gtpe/GTPeCard.hh"

#include <time.h>

#include "sys/time.h"


d2s::GTPeCounterMonitor::GTPeCounterMonitor(GTPeCard& gtpe, log4cplus::Logger& logger)
  : _gtpe(gtpe), 
    _logger(logger),
    _running(false),
    _t_lastread(0),
    _counters( d2s::GTPeCard::NCOUNTERS , (uint64_t)0 ),
    _update_semaphore(toolbox::BSem::FULL) {
}	

d2s::GTPeCounterMonitor::~GTPeCounterMonitor() {
}

void d2s::GTPeCounterMonitor::start() {

  _update_semaphore.take();

  std::vector<uint64_t>::iterator it = _counters.begin();
  for (;it != _counters.end(); ++it)
    (*it) = (uint64_t) 0;

  _t_lastread = getTime();
  _running = true;

  _update_semaphore.give();
}


void d2s::GTPeCounterMonitor::stop() 
  throw (HAL::HardwareAccessException) {

  _update_semaphore.take();

  if (_running)
    do_update();

  _running = false;

  _update_semaphore.give();
}


void d2s::GTPeCounterMonitor::update() 
  throw (HAL::HardwareAccessException) {


  _update_semaphore.take();

  if (_running)
    do_update();

  _update_semaphore.give();
}

void d2s::GTPeCounterMonitor::readCounters(std::vector<uint64_t>& counters) 
  throw (HAL::HardwareAccessException) {

  _update_semaphore.take();

  if (_running) 
    do_update();

  counters.resize(d2s::GTPeCard::NCOUNTERS);
  counters = _counters;

  _update_semaphore.give();
}


void d2s::GTPeCounterMonitor::do_update() 
  throw (HAL::HardwareAccessException) {


  uint32_t tnow = getTime();

  // need to do check below like this since tnow may be 1 sec smaller than t_lastread if do_update was called in different threads
  // (despite the fact that calls to do_update are synchronized ) 
  if ( tnow > _t_lastread + 3600) 
    LOG4CPLUS_WARN(_logger, 
		   "Maximum delay for counter update exceeded. Counters have to be updated every hour. Delay was " 
		   << (tnow - _t_lastread) << " seconds. (t_now=" << tnow << ", t_lastread=" << _t_lastread << ")");

  std::vector<uint32_t> counters32bit;
  _gtpe.readCountersSnapshot(counters32bit);


  for (uint32_t i=0; i<d2s::GTPeCard::NCOUNTERS; ++i) {
    uint32_t newvalue = counters32bit[i];

    if (i==4 || i==6) continue; // they are not really counters

    if (i != 2) {
      uint32_t oldcount_high = _counters[i] >> 32;
      uint32_t oldcount_low = _counters[i] & (uint64_t) 0xffffffff;

      if (newvalue < oldcount_low)
	oldcount_high++;

      _counters[i] = ((uint64_t) oldcount_high) << 32 | (uint64_t) newvalue;
    }
    else { // counter2 is only 16 bit FIXME!!
      uint64_t oldcount_high = _counters[i] >> 16;
      uint32_t oldcount_low = _counters[i] & (uint64_t) 0xffff;

      if (newvalue < oldcount_low)
	oldcount_high++;

      _counters[i] = oldcount_high << 16 | (uint64_t) newvalue;
    }

  }

  _t_lastread = tnow;

}


uint32_t d2s::GTPeCounterMonitor::getTime() {

  struct timeval tv;

  gettimeofday( &tv, 0 );
  return  tv.tv_sec;
}
