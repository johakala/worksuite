/**
*      @file GTPeROOTMonitor.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.11 $
*     $Date: 2007/03/27 08:02:38 $
*
*
**/
#include "d2s/gtpecontroller/GTPeROOTMonitor.hh"
#include "d2s/gtpecontroller/GTPeCounterMonitor.hh"
#include "d2s/gtpe/GTPeCard.hh"

#include <time.h>
#include "sys/time.h"
#include <sstream>

#include "TGraph.h"
#include "TCanvas.h"

d2s::GTPeROOTMonitor::GTPeROOTMonitor(GTPeCard& gtpe, 
				      std::string counter_item,
				      log4cplus::Logger& logger, 
				      uint32_t historydepth)
  : GTPeRateHistoryMonitor( gtpe, counter_item, logger, historydepth) {
}	

d2s::GTPeROOTMonitor::~GTPeROOTMonitor() {
}


void d2s::GTPeROOTMonitor::generateHistoryPlot(char *fn) {


  //  LOG4CPLUS_INFO(_logger, "creating rate history plot with root fn=" << fn);  
  TCanvas* c1;
  TCanvas *old = (TCanvas*) gROOT->GetListOfCanvases()->FindObject("c1");
  if (old) 
    c1=old;
  else
    c1 = new TCanvas("c1", "trigger rate history",400,250);


  _deque_semaphore.take();


  std::deque<std::pair<double, double> >::size_t size = _ratehistory.size();
  Double_t x[size];
  Double_t y[size];
  Double_t duration;

  if (size > 0) {

    for (std::deque<std::pair<double, double> >::size_t i=0; i< _ratehistory.size(); i++) {
      x[i] = (_ratehistory[i].first - _ratehistory[_ratehistory.size()-1].first ) / 1.e6;
      y[i] = _ratehistory[i].second / 1.e3;
    }
    
    duration = (_ratehistory[_ratehistory.size()-1].first - _ratehistory[0].first) / 1.e6;

  }

  _deque_semaphore.give();


  if (size > 0) {

    
    TGraph* gr = new TGraph(size, x, y);
  
    time_t now;
    now = time(NULL);

    std::stringstream ss;
    ss << "trigger rate in last " << duration << " seconds (at " << ctime(&now) <<")";
    gr->SetTitle(ss.str().c_str());
    gr->GetXaxis()->SetTitle("time / seconds");
    gr->GetYaxis()->SetTitle("trig. rate / kHz");
  

    gr->Draw("AC*");

    c1->Print(fn);
    c1->Clear();
  
    //    LOG4CPLUS_INFO(_logger, "finished creating history plot");
  
    delete gr;
  }


} 
