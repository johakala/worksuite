/**
*      @file GTPeController.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.24 $
*     $Date: 2008/05/26 09:43:48 $
*
*
**/
#include "log4cplus/helpers/sleep.h"

#include "d2s/gtpecontroller/GTPeController.hh"

// for web callback binding and HTML formatting
#include "d2s/gtpecontroller/GTPeWebInterface.hh"
#include "xgi/Method.h"

// for SOAP messaging
#include "d2s/gtpecontroller/GTPeSOAPUtility.hh"
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"

// state machine
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/Event.h"

#include "d2s/gtpecontroller/GTPeMonitor.hh"

#ifdef GTPE_WITHMONITOR
#include "d2s/gtpecontroller/GTPeROOTMonitor.hh"
#endif
#include "d2s/gtpecontroller/GTPeRateHistoryMonitor.hh"

// for access to GTPe Hardware
#include "d2s/gtpe/GTPeCard.hh"
#include "d2s/gtpe/GTPeCrate.hh"

// XDAQ exception handling
#include "xcept/Exception.h"

XDAQ_INSTANTIATOR_IMPL(d2s::GTPeController);

d2s::GTPeController::GTPeController(xdaq::ApplicationStub * s) 
  throw (xdaq::exception::Exception)
  : xdaq::WebApplication(s),
    _runNumber(0),
    _gtpecrate(0),
    _gtpeM(0),
    _gtpePartitionM(0),
    _wi(0),
    _wl(0),
    _autoUpdateSignature(0),
    _autoUpdateCount(0),
    _daqPartitionId(999),
    _totalTriggerRate(0.),
    _totalTriggerNumber(0),
    _partitionTriggerRate(0.),
    _partitionTriggerNumber(0),
    _rateBitsBeforePause(0) {

  // initialize non-required configuration items
  _config.daqPartitionId = 0;
  _config.enableATTSInput = true;

  _config.clockedMode = false;
  _config.clockedModeFrequency = 0.;
  _config.SLINKRequired = true;

  // make the config available so that our application can be configured
  // these parameters must not be changed after the application is configured
  //
  getApplicationInfoSpace()->fireItemAvailable ("daqPartitionId", &_config.daqPartitionId);
  getApplicationInfoSpace()->fireItemAvailable ("detPartitionEnableMask", &_config.detPartitionEnableMask);
  getApplicationInfoSpace()->fireItemAvailable ("triggerRate", &_config.triggerRate);
  getApplicationInfoSpace()->fireItemAvailable ("autoDAQPartitionId", &_config.autoDAQPartitionId);

  getApplicationInfoSpace()->fireItemAvailable ("enableATTSInput", &_config.enableATTSInput);

  getApplicationInfoSpace()->fireItemAvailable ("clockedMode", &_config.clockedMode);
  getApplicationInfoSpace()->fireItemAvailable ("clockedModeFrequency", &_config.clockedModeFrequency);
  getApplicationInfoSpace()->fireItemAvailable ("SLINKRequired", &_config.SLINKRequired);

  // run number must be set before enable 
  getApplicationInfoSpace()->fireItemAvailable("runNumber", &_runNumber);                    

  // For RunControl to determine the state of the application
  getApplicationInfoSpace()->fireItemAvailable("stateName", &_stateName);
  getApplicationInfoSpace()->fireItemAvailable("actualDAQPartitionId", &_daqPartitionId);
  getApplicationInfoSpace()->fireItemAvailable("totalTriggerRate", &_totalTriggerRate);
  getApplicationInfoSpace()->fireItemAvailable("totalTriggerNumber", &_totalTriggerNumber);
  getApplicationInfoSpace()->fireItemAvailable("partitionTriggerRate", &_partitionTriggerRate);
  getApplicationInfoSpace()->fireItemAvailable("partitionTriggerNumber", &_partitionTriggerNumber);

  // instantiate the GTPe Crate
  try {
    _gtpecrate = new d2s::GTPeCrate();
  }
  catch (xcept::Exception& e) { // handle xcept exceptions
    XCEPT_RETHROW(xdaq::exception::Exception, "Could not instantiate GTPeCrate", e);
  }

  // instantiate the GTPe Counter Monitor
  try {
#ifdef GTPE_WITHMONITOR
    _gtpeM = (GTPeMonitor*) new GTPeROOTMonitor(_gtpecrate->getGTPe(), 
						"AcceptedTriggers",
						getApplicationLogger() );
#else
    _gtpeM = (GTPeMonitor*) new GTPeRateHistoryMonitor(_gtpecrate->getGTPe(), 
						       "AcceptedTriggers",
						       getApplicationLogger() );

#endif
    _gtpePartitionM = (GTPeMonitor*) new GTPeRateHistoryMonitor(_gtpecrate->getGTPe(), 
								"GeneratedPart0",
								getApplicationLogger() );
  }
  catch (xcept::Exception& e) { // handle xcept exceptions
    cleanup();
    XCEPT_RETHROW(xdaq::exception::Exception, "Could not instantiate GTPeMonitor or GTPePartitionMonitor", e);
  }

  // register a default web page for display of status
  _wi = new d2s::GTPeWebInterface( this,
				   _config, 
				   _daqPartitionId,
				   _gtpecrate, 
				   _gtpeM, 
				   _gtpePartitionM,
				   getApplicationLogger());

  xgi::bind(this, &d2s::GTPeController::displayDefaultWebPage, "Default");

  // initialize finite state machine
  _fsm.addState ('H', "Halted"   , this, &d2s::GTPeController::stateChanged );
  _fsm.addState ('R', "Ready"    , this, &d2s::GTPeController::stateChanged );
  _fsm.addState ('E', "Enabled"  , this, &d2s::GTPeController::stateChanged );
  _fsm.addState ('P', "Paused"   , this, &d2s::GTPeController::stateChanged );
      
  _fsm.addStateTransition ('H','R', "Configure", this, &d2s::GTPeController::ConfigureAction);
  _fsm.addStateTransition ('F','R', "Configure", this, &d2s::GTPeController::ConfigureAction); // allow to recover from failed state
  _fsm.addStateTransition ('R','E', "Enable",    this, &d2s::GTPeController::EnableAction);
  _fsm.addStateTransition ('E','R', "Stop",      this, &d2s::GTPeController::StopAction);
  _fsm.addStateTransition ('P','R', "Stop",      this, &d2s::GTPeController::StopAction);
  _fsm.addStateTransition ('E','P', "Pause",     this, &d2s::GTPeController::PauseAction);
  _fsm.addStateTransition ('P','E', "Resume",    this, &d2s::GTPeController::ResumeAction);
  _fsm.addStateTransition ('E','H', "Halt",      this, &d2s::GTPeController::HaltAction);
  _fsm.addStateTransition ('R','H', "Halt",      this, &d2s::GTPeController::HaltAction);
  _fsm.addStateTransition ('P','H', "Halt",      this, &d2s::GTPeController::HaltAction);
  //  _fsm.addStateTransition ('H','H', "Halt",      this, &d2s::GTPeController::HaltAction);
      
  // Failure state setting
  _fsm.setFailedStateTransitionAction(this, &d2s::GTPeController::transitionToFailed );
  _fsm.setFailedStateTransitionChanged(this, &d2s::GTPeController::stateChanged );
      
  _fsm.setInitialState('H');
  _fsm.reset();
  _stateName = "Halted";
  
  // Bind SOAP callbacks for control messages
  xoap::bind (this, &d2s::GTPeController::changeStateCommandHandler, "Configure", XDAQ_NS_URI);
  xoap::bind (this, &d2s::GTPeController::changeStateCommandHandler, "Enable", XDAQ_NS_URI);
  xoap::bind (this, &d2s::GTPeController::changeStateCommandHandler, "Stop", XDAQ_NS_URI);
  xoap::bind (this, &d2s::GTPeController::changeStateCommandHandler, "Pause", XDAQ_NS_URI);
  xoap::bind (this, &d2s::GTPeController::changeStateCommandHandler, "Resume", XDAQ_NS_URI);
  xoap::bind (this, &d2s::GTPeController::changeStateCommandHandler, "Halt", XDAQ_NS_URI);

  // create a workloop
  _wl = toolbox::task::getWorkLoopFactory()->getWorkLoop("GTPeControllerMainWorkLoop", "polling");
  _autoUpdateSignature = toolbox::task::bind (this, &d2s::GTPeController::autoUpdateAction, "GTPeControllerAutoUpdateAction");
  _wl->submit(_autoUpdateSignature);
}
	

d2s::GTPeController::~GTPeController() {

  // stop the workloop
  if ( _wl->isActive() )
    _wl->cancel();
  _wl->remove(_autoUpdateSignature);

  cleanup();
}

void d2s::GTPeController::cleanup() {

  if (_wi) 
    delete _wi;

  if (_gtpeM)
    delete _gtpeM;

  if (_gtpePartitionM)
    delete _gtpePartitionM;

  if (_gtpecrate) 
    delete _gtpecrate;
}

void d2s::GTPeController::displayDefaultWebPage(xgi::Input * in, xgi::Output * out ) 
  throw (xgi::exception::Exception) {

  _gtpecrate->getGTPe().lock();
  try {
    _wi->displayDefaultWebPage( in, out, _stateName);
  }
  catch (...) {
    _gtpecrate->getGTPe().unlock();
    throw;
  }
  _gtpecrate->getGTPe().unlock();

}

xoap::MessageReference d2s::GTPeController::changeStateCommandHandler (xoap::MessageReference msg) 
    throw (xoap::exception::Exception) {

  std::string command = d2s::GTPeSOAPUtility::extractSOAPCommand( msg );

  LOG4CPLUS_INFO(getApplicationLogger(), "received command " << command); 

  try {
    toolbox::Event::Reference e(new toolbox::Event(command, this));
    _fsm.fireEvent(e);
  }
  catch (toolbox::fsm::exception::Exception & e) {
    // no need to log the error. the SOAP Dispatcher will do that for us
    XCEPT_RETHROW(xoap::exception::Exception, "Command not allowed", e);
  }
  
  return d2s::GTPeSOAPUtility::createSOAPResponse( command + "Response", _stateName);
}



void d2s::GTPeController::ConfigureAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {

  LOG4CPLUS_INFO(getApplicationLogger(), "configuring GTPe ...");

  try {
    // stop the workloop if it is active (we may have got here form failed state)
    if ( _wl->isActive() )
      _wl->cancel();


    _gtpecrate->getGTPe().lock(); // lock hardware and other semaphores

    // check if we are the first partition to be started
    bool first = true;
    for (int i=0; i<8; ++i) 
      if ( _gtpecrate->getGTPe().isDAQPartitionLocked(i) ) 
	first = false;

    
    if ( ! _config.autoDAQPartitionId ) {

      LOG4CPLUS_INFO(getApplicationLogger(), "Going to configure GTPe DAQ-partition ID=" << std::dec << _config.daqPartitionId << ".");

      if ( _gtpecrate->getGTPe().isDAQPartitionLocked( _config.daqPartitionId ) ) {
	std::stringstream msg;
	msg << "Requested DAQ Partition with ID=" 
	    << _config.daqPartitionId 
	    << "is already in use by process with PID="
	    <<  _gtpecrate->getGTPe().getDAQPartitionLockOwnerPID(  _config.daqPartitionId );
	XCEPT_RAISE(xcept::Exception, msg.str() );
      }
      _daqPartitionId = _config.daqPartitionId;
      

    } 
    else { // find a free DAQ Partition ID, automatically
      LOG4CPLUS_INFO(getApplicationLogger(), "Going to configure GTPe in autoDAQPartitionId mode.");

      uint32_t dpid;
      for (dpid=0; dpid < 8; ++dpid) 
	if ( ! _gtpecrate->getGTPe().isDAQPartitionLocked( dpid ) ) 
	  break;

      if ( dpid==8 ) 
	XCEPT_RAISE(xcept::Exception, "autoDAQPartitionId is true but all available DAQ partitions are in use.");


      _daqPartitionId = dpid;

      LOG4CPLUS_INFO(getApplicationLogger(), "Going to use DAQ-partition ID=" << std::dec << _daqPartitionId << ".");
    }
    
    
    // check if det partitions are available
    bool detPartitionsFree = true;
    for (int i=0; i<8; ++i)
      if ( ( _config.detPartitionEnableMask & (1<<i) ) &&
	   _gtpecrate->getGTPe().isDetPartitionLocked( i ) )
	detPartitionsFree = false;

    if ( ! detPartitionsFree ) 
      XCEPT_RAISE(xcept::Exception, "One or more of the required detector partitons are already in use by another process.");
      
    // check that we are compatible with other running partitions
    if (!first) {
      
      if ( _gtpecrate->getGTPe().isClockedMode() )
	XCEPT_RAISE(xcept::Exception, "There is already another process using the GTPe in clocked mode. Multiple partitions can only be run independently in random mode.");

      if ( _config.clockedMode )
	XCEPT_RAISE(xcept::Exception, "There is already another process using the GTPe. Cannot start this partition in clocked mode. Clocked mode is only possible if this is the first and only partition to be used.");

      if (  _gtpecrate->getGTPe().isSLINKRequired() != _config.SLINKRequired  ) {
	std::stringstream msg;

	msg << "SLINKRequired setting must be same for all partitions. "
	    << "Running partitions are set to " << _gtpecrate->getGTPe().isSLINKRequired()
	    << ", this partition asks for " << _config.SLINKRequired;
	XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    // lock DAQ Partition
    _gtpecrate->getGTPe().lockDAQPartition( _daqPartitionId );
    LOG4CPLUS_INFO(getApplicationLogger(), "Successfully locked DAQ-partition ID=" << std::dec << _daqPartitionId << ".");

    // lock Det Partitions
    for (int i=0; i<8; ++i)
      if ( ( _config.detPartitionEnableMask & (1<<i) ) ) {
	_gtpecrate->getGTPe().lockDetPartition( i );
	LOG4CPLUS_INFO(getApplicationLogger(), "Successfully locked Detector-partition ID=" << std::dec <<  i << ".");
      }

	

    // do global reset if we are first
    if (first) {    
      LOG4CPLUS_INFO(getApplicationLogger(), "No other partition has configured the GTPe yet. Performing a global reset.");

      _gtpecrate->getGTPe().globalReset();
      _gtpecrate->getGTPe().toggleSLINKRequired( _config.SLINKRequired );

      // clear partition table
      for (int i=0 ; i<8; ++i) 
	_gtpecrate->getGTPe().setPartitionDef(i, 0, 0.);

      _gtpecrate->getGTPe().setATTSInputMask( 0x0 );
      _gtpecrate->getGTPe().setSTTSInputMask( 0x0 );

      
      _gtpecrate->getGTPe().toggleClockedMode( false );
      _gtpecrate->getGTPe().setClockPeriod( 0xffffffff );
    }


    // halt any left-over partitions which may have kept running when their controller was killed
    for (int i=0; i<8; ++i) 
      if ( ! _gtpecrate->getGTPe().isDAQPartitionLocked(i) ) 
	_gtpecrate->getGTPe().setPartitionDef( i, 0, 0.);
    
    
    // and now we really configure our partition

    LOG4CPLUS_INFO(getApplicationLogger(),
		   "setting up DAQ partition " << std::dec << _daqPartitionId << ": selected partitions = 0x" << std::hex << _config.detPartitionEnableMask 
		   << ", rate = " << _config.triggerRate << " Hz");

    _gtpecrate->getGTPe().setPartitionDef( _daqPartitionId, 0, 0.); // set det bits to zero in case they were set (this will reset the counters)
    _gtpecrate->getGTPe().setPartitionDef( _daqPartitionId, _config.detPartitionEnableMask, 0.); // leave rate at zero until enable

    _gtpecrate->getGTPe().setSTTSInputMask( _gtpecrate->getGTPe().readbackSTTSInputMask() | _config.detPartitionEnableMask );

    uint32_t attsMask = 0;
    if ( _config.enableATTSInput ) 
      attsMask = (1<<_daqPartitionId);

    _gtpecrate->getGTPe().setATTSInputMask( _gtpecrate->getGTPe().readbackATTSInputMask() | attsMask );
    _gtpecrate->getGTPe().forceATTSOutput( _daqPartitionId, false);

    _gtpecrate->getGTPe().toggleClockedMode(_config.clockedMode);

    if (_config.clockedMode) {
      uint32_t period = (uint32_t) (40000000./_config.clockedModeFrequency);
      if (period < 1) period = 2;
      _gtpecrate->getGTPe().setClockPeriod(period);
      LOG4CPLUS_INFO(getApplicationLogger(),
		     "setting GTPe to clocked mode, trigger rate = " << _config.clockedModeFrequency 
		     << " Hz, real trigger rate = " << (40000000./(double)period) << " Hz.");
    }
    else
      LOG4CPLUS_INFO(getApplicationLogger(), "setting GTPe to random mode");

    _gtpecrate->getGTPe().unlock();

    std::stringstream regname;
    regname << "GeneratedPart" << _daqPartitionId;
    if (_gtpePartitionM) _gtpePartitionM->setCounterItem( regname.str() );
  }
  catch (xcept::Exception &e) { 
    // FIXME: should also try to unlock DAQ and Det partitions, here??
    // may not be necessary, as a kill of the App will do that for us.

    _gtpecrate->getGTPe().unlock();
    XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error during configuration", e);

  }
}

// here we reset some counters
void d2s::GTPeController::EnableAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {

  try {
    _gtpecrate->getGTPe().lock();

    if ( ! _gtpecrate->getGTPe().isRunning() ) {
      LOG4CPLUS_INFO(getApplicationLogger(), "Enable: GTPe is not yet running. Starting SLINK and Triggers.");
      _gtpecrate->getGTPe().startSLINK();
      log4cplus::helpers::sleepmillis(1);    
      _gtpecrate->getGTPe().startTriggers();
    }

    LOG4CPLUS_INFO(getApplicationLogger(), "Setting runNumber " << _runNumber << " for GTPe DAQ-partition ID=" << std::dec << _daqPartitionId << ".");
    _gtpecrate->getGTPe().setPartitionRunNumber(_daqPartitionId, _runNumber );

    LOG4CPLUS_INFO(getApplicationLogger(), "Enabling GTPe DAQ-partition ID=" << std::dec << _daqPartitionId << ".");
    _gtpecrate->getGTPe().setPartitionDef( _daqPartitionId, _config.detPartitionEnableMask, _config.triggerRate);

    if (_gtpeM) _gtpeM->start();
    if (_gtpePartitionM) _gtpePartitionM->start();

    _autoUpdateCount=0;
    _wl->activate();

    _gtpecrate->getGTPe().unlock();
  }
  catch (xcept::Exception &e) { 
    _gtpecrate->getGTPe().unlock();
    XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error during enable.", e);
  }


}

void d2s::GTPeController::StopAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {

  try {
    // stop the workloop
    _wl->cancel();

    LOG4CPLUS_INFO(getApplicationLogger(), "Stopping GTPe DAQ-partition ID=" << std::dec << _daqPartitionId << ".");
    _gtpecrate->getGTPe().lock();
    _gtpecrate->getGTPe().setPartitionDef( _daqPartitionId, _config.detPartitionEnableMask, 0.);

    bool last = true;
    for (int i=0; i<8; ++i) 
      if (i != _daqPartitionId && _gtpecrate->getGTPe().isDAQPartitionLocked(i) ) 
	last = false;

    if (last) {
      LOG4CPLUS_INFO(getApplicationLogger(), "No other DAQ partitions are using the GTPe. Stopping also the triggers and SLINK.");
      _gtpecrate->getGTPe().stopTriggers();
      log4cplus::helpers::sleep(1);
      _gtpecrate->getGTPe().stopSLINK();
    }


    if (_gtpeM) _gtpeM->stop(); // this does a last update
    if (_gtpePartitionM) _gtpePartitionM->stop(); // this does a last update

    // update event number to the final event number and reset trigger rate
    _totalTriggerRate = 0.;
    _partitionTriggerRate = 0.;
    _gtpecrate->getGTPe().latchCounters();
    _totalTriggerNumber = _gtpecrate->getGTPe().readCounterAcceptedTriggers();
    _partitionTriggerNumber = _gtpecrate->getGTPe().readCounterPartitionAcceptedTriggers( _daqPartitionId );

    _gtpecrate->getGTPe().unlock();
  }
  catch (xcept::Exception &e) {
    _gtpecrate->getGTPe().unlock();
    XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error during stop.", e);
  }
}


void d2s::GTPeController::PauseAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {


  try {
    _gtpecrate->getGTPe().lock();
    if ( _config.clockedMode ) {
      LOG4CPLUS_INFO(getApplicationLogger(), "pausing all GTPe triggers. This partition (ID=" << std::dec 
		     << _daqPartitionId << ") is in clocked mode and therefore uses the GTPe, exclusively.");
      _gtpecrate->getGTPe().pauseTriggers();
    }
    else {
      LOG4CPLUS_INFO(getApplicationLogger(), "pausing GTPe DAQ-Parititon ID=" << std::dec << _daqPartitionId << ".");
      _rateBitsBeforePause = _gtpecrate->getGTPe().readbackRate( _daqPartitionId );
      _gtpecrate->getGTPe().setPartitionDef( _daqPartitionId, _config.detPartitionEnableMask, 0.);
    }
    _gtpecrate->getGTPe().unlock();
  }
  catch (xcept::Exception &e) { 
    _gtpecrate->getGTPe().unlock();
    XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error in PauseAction.", e);
  }


}

void d2s::GTPeController::ResumeAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {

  try {
    _gtpecrate->getGTPe().lock();
    if ( _config.clockedMode ) {
      LOG4CPLUS_INFO(getApplicationLogger(), "resuming all GTPe triggers. This partition (ID=" << std::dec 
		     << _daqPartitionId << ") is in clocked mode and therefore uses the GTPe, exclusively.");
      _gtpecrate->getGTPe().resumeTriggers();
    }
    else {
      LOG4CPLUS_INFO(getApplicationLogger(), "resuming GTPe DAQ-Parititon ID=" << std::dec << _daqPartitionId << ".");
      _gtpecrate->getGTPe().setPartitionDef( _daqPartitionId, _config.detPartitionEnableMask, _rateBitsBeforePause);
    }
    _gtpecrate->getGTPe().unlock();
  }
  catch (xcept::Exception &e) { 
    _gtpecrate->getGTPe().unlock();
    XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error in ResumeAction.", e);
  }


}

void d2s::GTPeController::HaltAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {

  try {
    // we may get here from basically any state
    // all the actions below should not make an assumption about the state in which they are executed

    // stop the workloop
    if ( _wl->isActive() )
      _wl->cancel();

    _gtpecrate->getGTPe().lock();
    LOG4CPLUS_INFO(getApplicationLogger(), "Halting GTPe DAQ-partition ID=" << std::dec << _daqPartitionId << ".");
    _gtpecrate->getGTPe().setPartitionDef( _daqPartitionId, _config.detPartitionEnableMask, 0.);

    bool last = true;
    for (int i=0; i<8; ++i) 
      if (i != _daqPartitionId && _gtpecrate->getGTPe().isDAQPartitionLocked(i) ) 
	last = false;

    if (last) {
      LOG4CPLUS_INFO(getApplicationLogger(), "No other DAQ partitions are using the GTPe. Stopping also the triggers and SLINK.");
      _gtpecrate->getGTPe().stopTriggers();
      log4cplus::helpers::sleep(1);
      _gtpecrate->getGTPe().stopSLINK();
    }

    if (_gtpeM) _gtpeM->stop(); // this does a last update
    if (_gtpePartitionM) _gtpePartitionM->stop(); // this does a last update

    // update event number to the final event number and reset trigger rate
    _totalTriggerRate = 0.;
    _partitionTriggerRate = 0.;
    _gtpecrate->getGTPe().latchCounters();
    _totalTriggerNumber = _gtpecrate->getGTPe().readCounterAcceptedTriggers();
    _partitionTriggerNumber = _gtpecrate->getGTPe().readCounterPartitionAcceptedTriggers( _daqPartitionId );
    

    // release locks 
    _gtpecrate->getGTPe().unlockDAQPartition( _daqPartitionId );
    LOG4CPLUS_INFO(getApplicationLogger(), "Successfully un-locked DAQ partition ID=" << _daqPartitionId << ".");

    // unlock det Partitions
    for (int i=0; i<8; ++i)
      if ( ( _config.detPartitionEnableMask & (1<<i) ) ) {
	_gtpecrate->getGTPe().unlockDetPartition( i );
	LOG4CPLUS_INFO(getApplicationLogger(), "Successfully un-locked Detector-partition ID=" << std::dec <<  i << ".");
      }

    _gtpecrate->getGTPe().unlock();
  }
  catch (xcept::Exception &e) {
    _gtpecrate->getGTPe().unlock();
    XCEPT_RETHROW(toolbox::fsm::exception::Exception, "error during halt.", e);
  }
}

// called when a transition throws an exception ...
void d2s::GTPeController::transitionToFailed (toolbox::Event::Reference e)
  throw (toolbox::fsm::exception::Exception) {

  toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
  LOG4CPLUS_ERROR (getApplicationLogger(), 
		   "Failure occurred when performing transition from: "
		   << fe.getFromState() <<  " to: " << fe.getToState() << 
		   " exception: " << fe.getException().what() );
}

// called after every state change
void d2s::GTPeController::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) 
  throw (toolbox::fsm::exception::Exception) {

  _stateName = fsm.getStateName (fsm.getCurrentState());

  LOG4CPLUS_INFO (getApplicationLogger(), "New state is:" << _stateName.toString() );
}


// runs in a different thread
bool d2s::GTPeController::autoUpdateAction(toolbox::task::WorkLoop* wl) {

  _autoUpdateCount = ( _autoUpdateCount + 1 ) % 10;

  if (_gtpeM) {
    if (_autoUpdateCount == 0) {
      _gtpecrate->getGTPe().lock();
      _gtpeM->update();
      _gtpePartitionM->update();

      //
      // update exported parameters
      //
      _totalTriggerRate = _gtpeM->getLastComputedRate().second;
      _partitionTriggerRate = _gtpePartitionM->getLastComputedRate().second;
      //      std::vector<uint64_t> counters;
      //      _gtpeCounterMonitor->readCounters(counters);
      
      _totalTriggerNumber = _gtpecrate->getGTPe().readCounterAcceptedTriggers();
      _partitionTriggerNumber = _gtpecrate->getGTPe().readCounterPartitionAcceptedTriggers( _daqPartitionId );


      _gtpecrate->getGTPe().unlock();
    }
  }

  log4cplus::helpers::sleepmillis(100);    

  // instead of sleeping for 1 hour, we sleep 30000 times for 100 ms. 
  // the result should be slightly less than 1 hour of sleeping

  return true; // go on
}; 
