/**
*      @file GTPeRateHistoryMonitor.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.8 $
*     $Date: 2007/03/28 12:53:53 $
*
*
**/
#include "d2s/gtpecontroller/GTPeRateHistoryMonitor.hh"
#include "d2s/gtpecontroller/GTPeCounterMonitor.hh"
#include "d2s/gtpe/GTPeCard.hh"

#include <time.h>
#include "sys/time.h"
#include <sstream>


d2s::GTPeRateHistoryMonitor::GTPeRateHistoryMonitor(GTPeCard& gtpe, 
						    std::string counter_item,
						    log4cplus::Logger& logger, 
						    uint32_t historydepth)
  : _gtpe(gtpe), 
    _counter_item(counter_item),
    _logger(logger), 
    _historydepth(historydepth),
    _distrtrig_old(0),
    _ticks_lastread(0),
    _deque_semaphore(toolbox::BSem::FULL) {
}	

d2s::GTPeRateHistoryMonitor::~GTPeRateHistoryMonitor() {
}

void d2s::GTPeRateHistoryMonitor::start() {

  _deque_semaphore.take();
  
  _ratehistory.clear();

  _deque_semaphore.give();

  
  _gtpe.latchCounters();
  _ticks_lastread = _gtpe.readGlobalTimer();
  _distrtrig_old = _gtpe.readCounter64bit(_counter_item);

}


void d2s::GTPeRateHistoryMonitor::stop() {

}


void d2s::GTPeRateHistoryMonitor::update() {

  _gtpe.latchCounters();

  uint64_t ticks_now = _gtpe.readGlobalTimer();
  uint64_t distrtrig = _gtpe.readCounter64bit(_counter_item);

  double rate = (double) ( distrtrig - _distrtrig_old ) * 40.e6 / ( (double)  ( ticks_now - _ticks_lastread) );

  _ticks_lastread = ticks_now;
  _distrtrig_old = distrtrig;
  
  struct timeval tv;
  gettimeofday( &tv, 0 );
  double microtime = tv.tv_sec * 1000000. + (double) tv.tv_usec;

  _deque_semaphore.take();

  _ratehistory.push_back( std::make_pair( microtime, rate) );
  if (_ratehistory.size() > _historydepth) _ratehistory.pop_front(); 

  _deque_semaphore.give();
}


void d2s::GTPeRateHistoryMonitor::generateHistoryPlot(char *fn) {

  // nothing to do

} 

std::pair <double, double> d2s::GTPeRateHistoryMonitor::getLastComputedRate() {

  _deque_semaphore.take();

  std::pair <double, double>  rate = std::make_pair(0., 0.);
  if (_ratehistory.size() != 0) 
    rate = _ratehistory.back();

  _deque_semaphore.give();

  return rate;
} 
