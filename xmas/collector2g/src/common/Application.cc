// $Id: Application.cc,v 1.23 2009/02/20 14:02:18 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "xoap/DOMParser.h"
#include "xoap/DOMParserFactory.h"

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/collector2g/Application.h"
#include "xmas/collector2g/exception/NoData.h"
#include "xmas/collector2g/exception/FailedSerialization.h"
#include "xplore/DiscoveryEvent.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xdata/InfoSpaceFactory.h"
#include "xplore/Interface.h"
#include "xplore/exception/Exception.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"

#include "xdata/Table.h"
#include "xdata/TableAlgorithms.h"
#include "xdata/exdr/Serializer.h"
#include "xdaq/ApplicationDescriptorImpl.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/utils.h"
#include "toolbox/regex.h"
#include "toolbox/BSem.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"


#include "b2in/nub/Method.h"
#include "b2in/nub/Messenger.h"
#include "b2in/utils/exception/Exception.h"


// Required for Namespace declaration used in configuration file
#include "xmas/xmas.h"
#include "xmas/collector2g/Collector.h"
#include "xmas/collector2g/IncomingDataEvent.h"


XDAQ_INSTANTIATOR_IMPL(xmas::collector2g::Application);

xmas::collector2g::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this), repositoryLock_(toolbox::BSem::FULL)
{
	pool_ = 0;
	
	dialup_ = 0;
	eventingPoolSubscriber_ = 0;
	topicGathering_ = 0;
	collector_ = 0;

	committedPoolSize_ = 0x100000 * 50; // 5 MB

	this->getApplicationInfoSpace()->fireItemAvailable("committedPoolSize",&committedPoolSize_);

	s->getDescriptor()->setAttribute("icon", "/xmas/collector2g/images/xmas-collector2g-icon.png");

	// broker properties
	subscribeBrokerURL_ = "";
	brokerWatchdog_ = "PT10S";
	this->getApplicationInfoSpace()->fireItemAvailable("publishBrokerURL",&publishBrokerURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeBrokerURL",&subscribeBrokerURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("brokerWatchdog",&brokerWatchdog_);

	stagePeriod_ = "PT1S";
	this->getApplicationInfoSpace()->fireItemAvailable("stagePeriod",&stagePeriod_);

	subscribeExpiration_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeExpiration",&subscribeExpiration_);
	
	maxTableSize_ = 0x40000; // 256 Kb
	this->getApplicationInfoSpace()->fireItemAvailable("maxTableSize", &maxTableSize_);

	// In which group to search for a b2in-eventing
	subscribeGroup_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeGroup", &subscribeGroup_);
	publishGroup_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("publishGroup", &publishGroup_);
	// network used to subscribe, maps default network where the broker is configured
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeNetworkName", &subscribeNetworkName_);
 	subscribeNetworkName_ = this->getApplicationDescriptor()->getAttribute("network");
	
	// bind HTTP callback
	xgi::framework::deferredbind(this, this, &xmas::collector2g::Application::Default, "Default");

	// bind B2IN callback
	b2in::nub::bind(this, &xmas::collector2g::Application::onMessage );

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationContext()->addActionListener(this); // attach to endpoint available events
}

xmas::collector2g::Application::~Application()
{

}

//
// Infospace listener
//

void xmas::collector2g::Application::actionPerformed( xdata::Event& event) 
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
	
		try 
		{
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			toolbox::net::URN urn("b2in", "collector");
			pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
			
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			std::stringstream msg; 
			msg << "Failed to create b2in/collector memory pool for size " << committedPoolSize_.toString();
			XCEPT_DECLARE_NESTED(xmas::collector2g::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal",q);
			return;
		}

		try
		{
			toolbox::TimeInterval stageInterval;
			stageInterval.fromString(stagePeriod_.toString());
			toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer("xmas-collector2g-stager");
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();
			timer->scheduleAtFixedRate( start, this, stageInterval, 0, "stager" );


			toolbox::TimeInterval brokerPeriod;
			brokerPeriod.fromString(brokerWatchdog_.toString());
			timer->scheduleAtFixedRate( start, this, brokerPeriod, 0, "broker-staging" );
		}		
		catch (toolbox::task::exception::InvalidListener& e)
		{
			XCEPT_RETHROW (xmas::collector2g::exception::Exception, "Failed to enable stager", e);
		}
		catch (toolbox::task::exception::InvalidSubmission& e)
		{
			XCEPT_RETHROW (xmas::collector2g::exception::Exception, "Failed to enable stager", e);
		}
		catch (toolbox::task::exception::NotActive& e)
		{
			XCEPT_RETHROW (xmas::collector2g::exception::Exception, "Failed to enable stager", e);
		}
		catch(toolbox::exception::Exception & e)
		{
			std::stringstream msg; 
			msg << "Invalid stagePeriod in configuration '" << stagePeriod_.toString() << "'";
			XCEPT_DECLARE_NESTED(xmas::collector2g::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal",q);
			return;
		}
	}
	else
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}

void xmas::collector2g::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	std::string name = e.getTimerTask()->name;

	if (name == "broker-staging")
	{
		try
		{
			topicGathering_->requesting();
		}
		catch (xmas::collector2g::exception::Exception & e)
		{
			/*
			std::set<std::string> emptySet;
			eventingPoolSubscriber_->updateTopics(emptySet);
			collector_->reset();
			*/
			LOG4CPLUS_WARN (this->getApplicationLogger(), "Collector failed to communicate with the broker, gracefully exiting...");
			::exit(1);
		}
	}
	else if (name == "stager")
	{
		repositoryLock_.take();
		this->publishCollectedData();
		repositoryLock_.give();
	}
}

void xmas::collector2g::Application::actionPerformed(toolbox::Event& event) 
{
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Received event " << event.type());
	if ( event.type() == "xdaq::EndpointAvailableEvent")
	{
                // If the available endpoint is a b2in endpoint we are ready to
                // discover other b2in endpoints on the network
                //
                std::string defaultNetworkName = this->getApplicationDescriptor()->getAttribute("network");

                xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(event);
                const xdaq::Network* network = ie.getNetwork();

                LOG4CPLUS_INFO (this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());
		toolbox::TimeInterval brokerPeriod;
                try
                {
                        brokerPeriod.fromString(brokerWatchdog_.toString());
                }
                catch (toolbox::exception::Exception& e)
                {
                        std::stringstream msg;
                        msg << "Failed to parse brokerWatchdog property '" << brokerWatchdog_.toString() << "'";
                        XCEPT_DECLARE_NESTED(xmas::collector2g::exception::Exception, q, msg.str(), e);
                        this->notifyQualified("fatal",q);
                        return;
                }

                toolbox::TimeInterval expirePeriod;
                try
                {
                        expirePeriod.fromString(subscribeExpiration_.toString());
                }
                catch (toolbox::exception::Exception& e)
                {
                        std::stringstream msg;
                        msg << "Failed to parse subscribeExpiration property '" << subscribeExpiration_.toString() << "'";
       			XCEPT_DECLARE_NESTED(xmas::collector2g::exception::Exception, q, msg.str(), e);
                        this->notifyQualified("fatal",q);
                        return;
                }

		std::string currentName = network->getName();


                if (currentName == defaultNetworkName)
                {
                        //dialup_ = new b2in::utils::SmartEventingDialup (this, publishGroup_, subscribeBrokerURL_, "", defaultNetworkName, brokerPeriod);
                        dialup_ = new b2in::utils::EventingDialup (this, publishGroup_, publishBrokerURL_, "", defaultNetworkName, brokerPeriod);

			try
			{
				topicGathering_ = new xmas::collector2g::TopicGathering (this, subscribeBrokerURL_, defaultNetworkName, brokerPeriod);
			}
			catch(xmas::collector2g::exception::Exception & ex)
			{
				XCEPT_DECLARE_NESTED(xmas::collector2g::exception::Exception, e, "No brokerURL specified in collector2g configuration", ex);
				this->notifyQualified("fatal",e);
			}
		}

		if ( this->getApplicationContext()->getNetGroup()->isNetworkExisting(subscribeNetworkName_) 
			&& this->getApplicationContext()->getNetGroup()->isNetworkExisting(defaultNetworkName) 
			&& ((currentName == subscribeNetworkName_.toString()) || (currentName == defaultNetworkName)))
		{
			try
			{
				std::set<std::string> empty;
				eventingPoolSubscriber_ = new b2in::utils::SmartEventingPoolSubscriber (this, subscribeBrokerURL_, defaultNetworkName, subscribeNetworkName_, brokerPeriod, expirePeriod, empty,
					toolbox::parseTokenSet(subscribeGroup_,","));
			}
			catch(b2in::utils::exception::Exception & ex)
			{
				XCEPT_DECLARE_NESTED(xmas::collector2g::exception::Exception, e, "No brokerURL specified in collector2g configuration", ex);
				this->notifyQualified("fatal",e);
			}
		}

	}
}


void xmas::collector2g::Application::protocolStackReceive (toolbox::mem::Reference *  msg, xdata::Properties & plist) 
	throw (b2in::nub::exception::Exception)
{
	std::string protocol =  plist.getProperty("urn:b2in-eventing:protocol");
	if ( protocol == "xmas" )
	{
		std::string action =  plist.getProperty("urn:xmas-protocol:action");
		if ( action  == "reset" )
		{
			if( collector_ == 0 )
			{
				// collector not assigned - just return
				if (msg != 0 ) msg->release();
				return;
			}

			try
			{
				collector_->reset();
			}
			catch (xmas::exception::Exception& e)
			{
				if (msg != 0 ) msg->release();
				std::stringstream msg;
				msg << "Failed to clear flashlist '" << collector_->getTopic().getFlashlistName() << "'";
				XCEPT_RETHROW (b2in::nub::exception::Exception, msg.str(), e);
			}
		}
		else if ( action  == "clear" )
		{
			std::string flashlist = plist.getProperty("urn:xmas-protocol:flashlist");
			std::string column = plist.getProperty("urn:xmas-protocol:column");
			std::string value = plist.getProperty("urn:xmas-protocol:value");

			if( collector_ == 0 )
			{
				// collector not assigned - just return
				if (msg != 0 ) msg->release();
				return;
			}

			if( (flashlist != "") && (collector_->getTopic().getFlashlistName() != flashlist) )
			{
				std::stringstream msg;
				msg << "Ignore clear operation for flashlist '" << flashlist << "' , uknown flashlist";
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), msg.str());
			}

			try
			{
				if ( column == "" )
				{
					// If there is no selective clear, clear flashlist completly
					collector_->clear();
				}
				else
				{
					// Otherwise loop over all flashlists in the repository
					// and clear according to the matching column
					collector_->clear(column, value);
				}
			}
			catch (xmas::exception::Exception& e)
			{
				if (msg != 0 ) msg->release();
				std::stringstream msg;
				msg << "Failed to clear column '" << column << "' with value '" << value << "' in flashlist '" << collector_->getTopic().getFlashlistName() << "'";
				XCEPT_RETHROW (b2in::nub::exception::Exception, msg.str(), e);
			}
		}
		else
		{
			if (msg != 0 ) msg->release();
			std::stringstream msg;
			msg << "Invalid xmas protocol action  " <<  action;
			XCEPT_RAISE (b2in::nub::exception::Exception, msg.str());
		}
		if (msg != 0 ) msg->release();
		return;
	}

	// broker requests

	std::string action = plist.getProperty("urn:xmasbroker2g:action");
	if ( action == "allocate" )
	{
		if( (topicGathering_ == 0) || (dialup_ == 0) || (eventingPoolSubscriber_ == 0) )
		{
			if (msg != 0) msg->release();
			return;
		}

		std::string model = plist.getProperty("urn:xmasbroker2g:model");
		if (model == "collector")
		{
			topicGathering_->update(plist);

			// get current topic
			xmas::collector2g::Topic topic;
			try
			{
				topic = topicGathering_->getAssignedTopic();
			}
			catch(xmas::collector2g::exception::Exception & e)
			{
				// not topics at all therefore reset collection
				if(collector_ != 0)
                        	{
					delete collector_;
				}
				collector_ = 0;
				// no topics to subscribe for 
				std::set<std::string> topics;
				eventingPoolSubscriber_->updateTopics(topics);
				if (msg != 0) msg->release();
				return;
			}

			try
			{
				if(collector_ == 0)
				{
					// create a new collector for the assigned topic
					collector_ = new xmas::collector2g::Collector(topic);
				}
				else
				{
					// previous  topic already assigned
					// recreate collector if any of the topics changed
					if(collector_->getTopic() != topic)
					{
						delete collector_;
						collector_ = 0;
						collector_ = new xmas::collector2g::Collector(topic);
					}
					else
					{
						// keep current topic as is
						if (msg != 0) msg->release();
						return;
					}
				}
			}
			catch(xmas::collector2g::exception::Exception & e)
			{
				if (msg != 0) msg->release();
				this->notifyQualified("error", e);
				return;
			}

			// update subscriptions
			std::set<std::string> topics;
			topics.insert(topic.getSubscribeTopic());
			eventingPoolSubscriber_->updateTopics(topics);
		}
		else if (model == "eventingpool")
		{
			// receivd list of eventing pool elements ( b2in-eventing)
			eventingPoolSubscriber_->updateEventings(plist);
		}
		else if (model == "eventing")
		{
			// b2in-eventing into which data must be injected
			dialup_->onMessage(msg, plist);
		}

		if (msg != 0) msg->release();
		return;
	}

	/* --------------------------------------------------------------------------------
	   It's not a message from the broker, so it is an incoming flashlist data message.
	   -------------------------------------------------------------------------------- 
	 */ 
	std::string flashlistName = plist.getProperty("urn:xmas-flashlist:name");
	
	if (flashlistName == "")
	{
		if (msg != 0) msg->release();
		XCEPT_RAISE (b2in::nub::exception::Exception, "Incoming B2IN message corrupted, 'urn:xmas-flashlist:name' property is empty");
	}
	else if ( msg != 0 )
	{
		if( (collector_ == 0) || (collector_->getTopic().getFlashlistName() != flashlistName) )
		{
			if (msg != 0) msg->release();
			std::stringstream errmsg;
			errmsg <<  "Received flashlist '" << flashlistName << "', but collector is not configured for this flashlist";
			XCEPT_DECLARE(xmas::collector2g::exception::Exception, e, errmsg.str());
			this->notifyQualified("fatal",e);
			return;
		}
		else
		{
			toolbox::task::EventReference reference(new xmas::collector2g::IncomingDataEvent (msg, plist, this));
			try
			{	
				collector_->getDispatcher().fireEvent(reference);
				collector_->incrementFireCounter();
				return;
			}
			catch(toolbox::task::exception::Overflow & e )
			{					
				collector_->incrementInternalLossCounter();
				if (msg != 0) msg->release();
			}
			catch(toolbox::task::exception::OverThreshold & e )
			{
				collector_->incrementInternalLossCounter();
				if (msg != 0) msg->release();
			}
			catch(toolbox::task::exception::InternalError & e )
			{		
				collector_->incrementInternalLossCounter();
				if (msg != 0) msg->release();
				
				std::stringstream errmsg;
				errmsg << "Internal error, report lost for flashlist " << flashlistName;;
				LOG4CPLUS_FATAL (this->getApplicationLogger(), errmsg.str());
				XCEPT_DECLARE_NESTED (xmas::collector2g::exception::Exception, ex, errmsg.str(), e);
				this->notifyQualified("fatal", ex);	
			}
		}
	}
	else
	{
		XCEPT_RAISE (b2in::nub::exception::Exception, "Empty data for flashlist " + flashlistName);
	}
}

void xmas::collector2g::Application::onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) 
	throw (b2in::nub::exception::Exception)
{
	repositoryLock_.take();

	try
	{
		protocolStackReceive(msg, plist);
	}
	catch(b2in::nub::exception::Exception & e)
	{
		this->notifyQualified("error", e);	
	}

	repositoryLock_.give();
}


void xmas::collector2g::Application::publishCollectedData()
{
	if ( collector_ == 0 )
	{
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "publishDataCollection failed due to missing collector object");
		return;
	}

	if ( ! collector_->hasChanged() )
	{
		return; // no new data produced since last time, therefore no need for sending
	}

	if( dialup_ == 0)
	{
		collector_->incrementCommunicationLossCounter();
		return;
	}

	if( !dialup_->isEstablished() )
	{
		collector_->incrementCommunicationLossCounter();
		return;
	}

	toolbox::mem::Reference* ref = 0;
	try
	{
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, (xdata::UnsignedInteger32T) maxTableSize_);
	}	
	catch (toolbox::mem::exception::Exception & ex )
	{
		collector_->incrementMemoryLossCounter();
		std::stringstream msg;
		msg << "Failed to allocate " << maxTableSize_.toString() << " bytes for sending flashlist '" << collector_->getName() << "'";
		XCEPT_DECLARE_NESTED (xmas::collector2g::exception::Exception, e, msg.str(), ex);
		this->notifyQualified("fatal", e);	
		return;
	}
	
	xdata::Properties plist;
	
	try
	{
		collector_->extractDataCollection(ref, plist);
	}
	catch (xmas::collector2g::exception::NoData& e)
	{
		ref->release();
		// ignore since it could be that there was a clear from an external agent and therefore no data to send
		return;	
	}
	catch (xmas::collector2g::exception::FailedSerialization & e)
	{
		collector_->incrementSerializationFailedCounter();
		ref->release();
		std::stringstream msg;
		msg << "Failed to serialize flashlist '" << collector_->getName() << "'";
		XCEPT_DECLARE_NESTED(xmas::collector2g::exception::Exception, ce, msg.str(), e);
		this->notifyQualified("fatal",ce);
		return;	
	}
	
	plist.setProperty("urn:b2in-protocol:service", "b2in-eventing");
	plist.setProperty("urn:b2in-eventing:action", "notify");
	
	toolbox::net::URL originator(getApplicationContext()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN());
	plist.setProperty("urn:xmas-flashlist:originator",originator.toString());

	try
	{
		dialup_->send(ref,plist);
		return;
	}
	catch (b2in::utils::exception::Exception & e)
        {
		collector_->incrementCommunicationLossCounter();

                ref->release();
                std::stringstream msg;
                msg << "Failed to report flashlist (internal error) '" << collector_->getName() << "'";
                                
                XCEPT_DECLARE_NESTED(xmas::collector2g::exception::Exception, ex , msg.str(), e);
                this->notifyQualified("fatal",ex);
                return;
	} 
}

void xmas::collector2g::Application::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	this->TabPanel(out);
}

void xmas::collector2g::Application::TabPanel( xgi::Output * out )
{
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
	
	// Tabbed pages
	*out << "<div class=\"xdaq-tab\" title=\"Flashlists\">" << std::endl;
	this->FlashlistsTabPage(out);
	*out << "</div>";
	
	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";
	
	*out << "<div class=\"xdaq-tab\" title=\"Collector\">" << std::endl;
	this->CollectorTabPage(out);
	*out << "</div>";
	
	*out << "</div>";
}

void xmas::collector2g::Application::FlashlistsTabPage( xgi::Output * out )
{
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class", "xdaq-sortable");
	*out << cgicc::th("Subscribe Topic");
	*out << cgicc::th("Publish Topic");
	*out << cgicc::th("Hash Key");
	*out << cgicc::th("Staging Period");
	*out << cgicc::th("Auto Clear");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	
	*out << cgicc::tbody();

	repositoryLock_.take();

	if(collector_ != 0)
	{
		*out << cgicc::tr() << std::endl;
		
		*out << cgicc::td( collector_->getTopic().getFlashlistName() ) << std::endl;
		*out << cgicc::td( collector_->getTopic().getSubscribeTopic() ) << std::endl;
		*out << cgicc::td( collector_->getTopic().getPublishTopic() ) << std::endl;
		*out << cgicc::td( toolbox::printTokenSet(collector_->getTopic().getHashKey(), ",") ) << std::endl;
		*out << cgicc::td( collector_->getTopic().getStagingPeriod().toString() ) << std::endl;

		if( collector_->getTopic().getAutoClear() )
		{
			*out << cgicc::td("true").set("class","xdaq-green") << std::endl;
		}
		else
		{
			*out << cgicc::td("false").set("class","xdaq-red") << std::endl;
		}

		*out << cgicc::tr() << std::endl;
	}
	
	repositoryLock_.give();

	*out << cgicc::tbody();	
	*out << cgicc::table();
	
}

void xmas::collector2g::Application::StatisticsTabPage( xgi::Output * out )
{
	repositoryLock_.take();
	// Eventing Dialup

	*out << cgicc::table().set("class","xdaq-table-vertical");
	*out << cgicc::caption("Eventing Dialup");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");
	if(dialup_ != 0)
	{
		*out << dialup_->getStateName();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getBrokerURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getBrokerLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Eventing URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Eventing address";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getEventingURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports sent
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total outgoing reports";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports lost
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total reports lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 

	// Eventing Pool Subscriber

	*out << cgicc::table().set("class","xdaq-table-vertical");
	*out << cgicc::caption("Eventing Pool Subscriber");
	*out << cgicc::tbody() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");;
	if(eventingPoolSubscriber_ != 0)
	{
		*out << eventingPoolSubscriber_->getBrokerURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if(eventingPoolSubscriber_ != 0)
	{
		*out << eventingPoolSubscriber_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if(eventingPoolSubscriber_ != 0)
	{
		*out << eventingPoolSubscriber_->getBrokerLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Eventing URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Eventing address";
	*out << cgicc::th();
	*out << cgicc::td();
	if(eventingPoolSubscriber_ != 0)
	{
		*out << toolbox::printTokenSet(eventingPoolSubscriber_->getEventingURLs(), ",");
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Subscriptions sent
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total eventing subscriptions";
	*out << cgicc::th();
	*out << cgicc::td();
	if(eventingPoolSubscriber_ != 0)
	{
		*out << eventingPoolSubscriber_->getEventingCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Subscriptions lost
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total eventing subscriptions lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if(eventingPoolSubscriber_ != 0)
	{
		*out << eventingPoolSubscriber_->getEventingLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 


 	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;");
	*out << cgicc::caption("Collector");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Collection Operation");
	*out << cgicc::th("Out of Memory Loss");
	*out << cgicc::th("Serialization Failed");
	*out << cgicc::th("Internal Loss");
	*out << cgicc::th("Communication Loss");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();
	
	if ( collector_ != 0 )
	{
		xdata::UnsignedInteger64 num;
  		*out << cgicc::tr() << std::endl;
		num = collector_->getFireCounter();
        	*out << cgicc::td(num.toString()) << std::endl;
		num = collector_->getMemoryLossCounter();
        	*out << cgicc::td(num.toString()) << std::endl;
		num = collector_->getSerializationFailedCounter();
        	*out << cgicc::td(num.toString()) << std::endl;
		num = collector_->getInternalLossCounter();
        	*out << cgicc::td(num.toString()) << std::endl;
		num = collector_->getCommunicationLossCounter();
        	*out << cgicc::td(num.toString()) << std::endl;
        	*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
        *out << cgicc::table();

	repositoryLock_.give();


}

void xmas::collector2g::Application::CollectorTabPage( xgi::Output * out )
{
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;");

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Subscribe Topic") << std::endl;
	*out << cgicc::th("Rows") << std::endl;
	*out << cgicc::th("Last Update") << std::endl;
	*out << cgicc::th("Processed Since Last Send") << std::endl;
	*out << cgicc::th("Deserialize Time (Rate)") << std::endl;
	*out << cgicc::th("Process Time (Max Rate)") << std::endl;
	*out << cgicc::th("Merge Rate") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;
	
	*out << cgicc::tbody() << std::endl;

	repositoryLock_.take();

	if(collector_ != 0)
	{
		*out << cgicc::tr() << std::endl;
		
		*out << cgicc::td( collector_->getTopic().getSubscribeTopic() ) << std::endl;

		std::stringstream rowcount;
		rowcount << collector_->getRowCount();
		*out << cgicc::td( rowcount.str() ) << std::endl;

		*out << cgicc::td( collector_->getLastUpdate().toString(toolbox::TimeVal::loc) ) << std::endl;

		std::stringstream dataEventCounter;
		dataEventCounter << collector_->getDataEventCounter();
		*out << cgicc::td( dataEventCounter.str() ) << std::endl;

		std::stringstream averageDeserializeTime;
		averageDeserializeTime << collector_->getAverageDeserializeTime() << " (" << (1/collector_->getAverageDeserializeTime()) << ")";
		*out << cgicc::td( averageDeserializeTime.str() ) << std::endl;

		std::stringstream averageMergeTime;
		averageMergeTime << collector_->getAverageMergeTime() << " (" << (1/collector_->getAverageMergeTime()) << ")";
		*out << cgicc::td( averageMergeTime.str() ) << std::endl;

		std::stringstream mergeRate;
		mergeRate << collector_->getDataMergeRate();
		*out << cgicc::td( mergeRate.str() ) << std::endl;
			
		*out << cgicc::tr() << std::endl;
	}

	repositoryLock_.give();
	
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

