// $Id: $


#include "xmas/smarthub/Application.h"

XDAQ_INSTANTIATOR_IMPL (xmas::smarthub::Application)

xmas::smarthub::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this)
{

	s->getDescriptor()->setAttribute("icon", "/xmas/smarthub/images/smarthub.png");

	xgi::framework::deferredbind(this, this, &Application::Default, "Default");
	xgi::framework::deferredbind(this, this, &xmas::smarthub::Application::flexDisplay, "flexDisplay");

}

void xmas::smarthub::Application::Default(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<script type=\"text/javascript\" src=\"/xmas/smarthub/html/js/smarthub-design.js\"></script> " << std::endl;
	*out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/xmas/smarthub/html/css/smarthub-design.css\">";

	std::stringstream baseurl;
	baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

*out << "<div class=\"xdaq-wrapper\">" << std::endl;
	//*out << "<div id=\"button-wrapper\">" <<std::endl;
	//	*out << "<button type=\"button\">Smarthub</button>" << std::endl;
	//	*out << "<button type=\"button\">Settings</button>" << std::endl;
	//*out << "</div>" <<std::endl;
//	*out << "<div id=\"container\">
	*out << "<button id=\"refresh-table\" class=\"xdaq-button\">refresh</button>" << std::endl;

	*out << "<div id=\"table-wrapper\" class=\"xdaq-wrapper\">" <<std::endl;
		*out << "<table id=\"names-table\" class=\"xdaq-table\">" << std::endl;

			*out <<"<thead>"<< std::endl;
				*out << "<th>Zone</th>" << std::endl;
				*out << "<th>Context</th>" << std::endl;
			*out <<"</thead>"<< std::endl;
			*out <<"<tbody>"<< std::endl;
				*out << "<tr><td></td><td></td></tr>" <<std::endl;
				*out << "<tr><td></td><td></td></tr>" <<std::endl;
				*out << "<tr><td></td><td></td></tr>" <<std::endl;
				*out << "<tr><td></td><td></td></tr>" <<std::endl;
				*out << "<tr><td></td><td></td></tr>" <<std::endl;
			*out <<"</tbody>"<< std::endl;
		*out << "</table>" <<std::endl;
	*out << "</div>" <<std::endl;
*out << "</div>" <<std::endl;

}

void xmas::smarthub::Application::flexDisplay(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{

	std::stringstream home;
	home << this->getApplicationDescriptor()->getContextDescriptor()->getURL();
	std::string xploreURL =  cgicc::form_urlencode(home.str());

	std::string url = cgicc::form_urlencode(xploreURL);

	*out << "<a href=\"/xmas/smarthub/html/smarthub.swf?xploresurl=" << url << "\" target=\"_blank\"><button><br/><img src=\"/xmas/smarthub/images/smarthub.png\"><br />Smarthub Flex Interface<br/><br/></button></a>";

}

xmas::smarthub::Application::~Application ()
{


}

