<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
	<!-- Compulsory  Plugins -->
	<xp:Application class="executive::Application" id="0"  service="executive" network="local">
		<properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
			<logUrl xsi:type="xsd:string">console</logUrl>
                	<logLevel xsi:type="xsd:string">INFO</logLevel>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>
	
	<xp:Application class="pt::http::PeerTransportHTTP" id="1"  network="local">
		 <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
		 	<documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
                        <aliasName xsi:type="xsd:string">tmp</aliasName>
                        <aliasPath xsi:type="xsd:string">/tmp</aliasPath>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>

 	<xp:Application heartbeat="false" class="pt::tcp::PeerTransportTCP" id="20" network="local">
                        <properties xmlns="urn:xdaq-application:PeerTransportTCP" xsi:type="soapenc:Struct">
                                <autoSize xsi:type="xsd:boolean">true</autoSize>
                                <maxPacketSize xsi:type="xsd:unsignedInt">0x800000</maxPacketSize>
                                <committedPoolSize xsi:type="xsd:double">100000000</committedPoolSize>
                                <lowThreshold xsi:type="xsd:double">0.5</lowThreshold>
                                <highThreshold xsi:type="xsd:double">0.8</highThreshold>
                                <outputQueueSize xsi:type="xsd:unsignedInt">1024</outputQueueSize>

                        </properties>
         </xp:Application>
          <xp:Module>${XDAQ_ROOT}/lib/libpttcp.so</xp:Module>


	 <xp:Endpoint protocol="tcp" service="b2in" subnet="10.176.0.0" port="1910" publish="true" maxport="1950" autoscan="true" network="slimnet"/>


	<xp:Application class="pt::fifo::PeerTransportFifo" id="8"  network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>
	
	<!-- XRelay -->
	<xp:Application class="xrelay::Application" id="4"  service="xrelay"  network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libxrelay.so</xp:Module>
	
	<!-- HyperDAQ -->
	<xp:Application class="hyperdaq::Application" id="3"  service="hyperdaq" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>	
		
	<!-- XPlore requires the installation of Power Pack  -->
	<!-- xp:Application class="xplore::Application" id="9"  network="local">
	 	<properties xmlns="urn:xdaq-application:xplore::Application" xsi:type="soapenc:Struct">
			<settings xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/xplore/shortcuts.service.xml</settings>
			<republishInterval xsi:type="xsd:string">60</republishInterval>
                </properties>
	</xp:Application>	
	<xp:Module>/lib/libslp.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libxslp.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libxploreutils.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libxplore.so</xp:Module -->

	<!-- xp:Application heartbeat="false" class="sentinel::probe2g::Application" id="21"  network="slimnet" group="exception" service="sentinelprobe2g">
                <properties xmlns="urn:xdaq-application:Sentinel" xsi:type="soapenc:Struct">
                        <publishGroup xsi:type="xsd:string">exception</publishGroup>
                        <watchdog xsi:type="xsd:string">PT5S</watchdog>
                        <eventingURL xsi:type="xsd:string">tcp://srv-c2d04-22.cms:1977</eventingURL>
                </properties>
        </xp:Application>
        <xp:Module>${XDAQ_ROOT}/lib/libsentinelutils.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libsentinelprobe2g.so</xp:Module -->


 	<xp:Application heartbeat="true" class="xmas::sensor2g::Application" id="5" network="slimnet" group="xservice" service="xmassensor2g">
                <properties xmlns="urn:xdaq-application:xmas::sensor2g::Application" xsi:type="soapenc:Struct">
                        <autoConfigure xsi:type="xsd:boolean">true</autoConfigure>
                        <maxReportMessageSize xsi:type="xsd:unsignedInt" >0x100000</maxReportMessageSize>
                        <autoConfSearchPath xsi:type="xsd:string">http://srv-c2d04-22.cms:9966/directory/sensor</autoConfSearchPath>
                        <publishGroup xsi:type="xsd:string">xservice</publishGroup>
                        <heartbeatWatchdog xsi:type="xsd:string">PT5S</heartbeatWatchdog>
                        <brokerWatchdog xsi:type="xsd:string">PT5S</brokerWatchdog>
                        <eventingURL xsi:type="xsd:string">tcp://srv-c2d04-22.cms:1977</eventingURL>
                </properties>
        </xp:Application>
        <xp:Module>${XDAQ_ROOT}/lib/libwsaddressing.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxmasutils.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxmassensor2g.so</xp:Module>

  	
	<!-- xp:Application class="sentinel::tester::Application" id="22"  network="local"  group="exception,init0" service="sentinel-tester">
        </xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libsentineltester.so</xp:Module -->

	<xp:Module>${XDAQ_ROOT}/lib/libxmastester.so</xp:Module>
	<xp:Application class="xmas::tester::Application" instance="0" id="23"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" instance="1" id="24"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" instance="2" id="25"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" instance="3" id="26"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" instance="4" id="27"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" instance="5" id="28"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<!-- xp:Application class="xmas::tester::Application" instance="6" id="29"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" instance="7" id="30"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" instance="8" id="31"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" instance="9" id="32"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application -->

	<!-- xp:Application class="xmas::tester::Application" id="33"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" id="34"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" id="35"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" id="36"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" id="37"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" id="38"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" id="39"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" id="40"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" id="41"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application>
	<xp:Application class="xmas::tester::Application" id="42"  network="local"  group="xmas,init0" service="xmas-tester">
        </xp:Application -->

	<!-- XMem -->
	<xp:Application class="xmem::Application" id="6"  network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libxmem.so</xp:Module>
</xp:Profile>
