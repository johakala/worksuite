// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, A. Petrucci, P. Roberts			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <sstream>
#include <string>
#include <iostream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"
#include "toolbox/net/URN.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/extreme/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/xmas.h"
#include "xmas/extreme/exception/Exception.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xmas/exception/Exception.h"

#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/InputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/TimeVal.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdata/TableIterator.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include "xoap/DOMParserFactory.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

#include <stdio.h>
#include <curl/curl.h>

#include "es/api/Cluster.h"
#include "es/api/Stream.h"

XDAQ_INSTANTIATOR_IMPL (xmas::extreme::Application);

XERCES_CPP_NAMESPACE_USE

xmas::extreme::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this)
{

	b2in::nub::bind(this, &xmas::extreme::Application::onMessage);

	std::srand((unsigned) time(0));

	getApplicationDescriptor()->setAttribute("icon", "/xmas/extreme/images/elasticsearch.png");

	// optional broker properties
	autoConfSearchPath_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("autoConfSearchPath", &autoConfSearchPath_);

	elasticsearchClusterUrl_ = "unknown";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchClusterUrl", &elasticsearchClusterUrl_);

	elasticsearchFlashIndexName_ = "flashlist";
	elasticsearchShelfIndexName_ = "shelflist";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchFlashIndexName", &elasticsearchFlashIndexName_);
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchShelfIndexName", &elasticsearchShelfIndexName_);

	elasticsearchFlashIndexStoreType_ = "memory";
	elasticsearchShelfIndexStoreType_ = "niofs";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchFlashIndexStoreType", &elasticsearchFlashIndexStoreType_);
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchShelfIndexStoreType", &elasticsearchShelfIndexStoreType_);

	ttl_ = "60s";
	this->getApplicationInfoSpace()->fireItemAvailable("ttl", &ttl_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this,  &xmas::extreme::Application::Default, "Default");

	xgi::bind(this,  &xmas::extreme::Application::displayFlashlist, "displayFlashlist");
	xgi::bind(this,  &xmas::extreme::Application::displayFlashlistMapping, "displayFlashlistMapping");
	//xgi::bind(this,  &xmas::extreme::Application::displayLatestData, "displayLatestData");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);
}

xmas::extreme::Application::~Application ()
{

}
void xmas::extreme::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		indexCreated_ = false;
		toolbox::Properties properties;
		//properties.setProperty("urn:stream:CURLOPT_VERBOSE","false");
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Attaching to elastic search...");
		member_ = new es::api::Member(this,properties);


		std::string path = autoConfSearchPath_.toString();

		std::vector < std::string > files;

		if (path.find("http:") != std::string::npos)
		{
			try
			{
				files = this->getFileListing(path + "/sensor");
				for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
				{
					(*j) = path + "/" + (*j);
				}
			}
			catch (xmas::extreme::exception::Exception & e)
			{
				XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, "Failed to get sensor settings", e);
				this->notifyQualified("fatal", q);
				return;
			}
		}
		else
		{
			// if no search path provided, rely on runtime information
			if (path != "")
			{
				try
				{
					std::string localpath = path + "/sensor/*.sensor";
					files = toolbox::getRuntime()->expandPathName(localpath);
				}
				catch (toolbox::exception::Exception& e)
				{
					std::stringstream msg;
					msg << "Failed to import flashlist definitions from '" << path << "', ";
					XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, msg.str(), e);
					this->notifyQualified("fatal",q);
					return;
				}
			}
		}

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Found " << files.size() << " sensor files");

		this->applySensorSettings(files);

		LOG4CPLUS_INFO(this->getApplicationLogger(), "path now " << path);

		if (path.find("http:") != std::string::npos)
		{
#warning "HTTP file listing not completed, need to scan settings files"
			try
			{
				files = this->getFileListing(path + "/broker/xmas");
				for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
				{
					(*j) = path + "/" + (*j);
				}
			}
			catch (xmas::extreme::exception::Exception & e)
			{
				XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, "Failed to get sensor settings", e);
				this->notifyQualified("fatal", q);
				return;
			}
		}
		else
		{
			// if no search path provided, rely on runtime information
			if (path != "")
			{
				try
				{
					std::string localpath = path + "/broker/*/collector.settings";
					files = toolbox::getRuntime()->expandPathName(localpath);
				}
				catch (toolbox::exception::Exception& e)
				{
					std::stringstream msg;
					msg << "Failed to import flashlist definitions from '" << path << "', ";
					XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, msg.str(), e);
					this->notifyQualified("fatal",q);
					return;
				}
			}
		}


		this->applyCollectorSettings(files);

		// check mapping exists for flashlists or create
		/*
		for (std::map<std::string, xmas::FlashListDefinition*>::iterator i = flashlists_.begin(); i != flashlists_.end(); i++ )
		{
			xmas::FlashListDefinition * flashlist = (*i).second;
			std::string name = flashlist->getProperty("name");
			toolbox::net::URN flashlistURN(name);
			std::string fname = flashlistURN.getNSS();

			this->createMapping(name);

		}
		*/

	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(xmas::extreme::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

void xmas::extreme::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string timerTaskName = e.getTimerTask()->name;
	if (timerTaskName == "extreme:scan")
	{
	}
}

void xmas::extreme::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) throw (b2in::nub::exception::Exception)
{
	if (msg == 0)
	{

		// these messages with properties only do not belong to this application ! Let's ignore them

		std::map<std::string, std::string, std::less<std::string> >& p = plist.getProperties();

		for (std::map<std::string, std::string, std::less<std::string> >::iterator i = p.begin(); i != p.end(); i++ )
		{
			//std::cout << (*i).first << " - " << (*i).second <<std::endl;
		}
		//XCEPT_DECLARE(xmas::extreme::exception::Exception, q, "no data no valid monitoring work to be done, ignore it");
		//this->notifyQualified("warning",q);
		return;
	}

	if (plist.getProperty("urn:b2in-protocol:action") == "flashlist")
	{
		std::string qname = plist.getProperty("urn:xmas-flashlist:name");
		toolbox::net::URN flashlistURN(qname);
		std::string fname = flashlistURN.getNSS();

		if ( blackFlashList_.find(qname)  != blackFlashList_.end())
		{
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Flashlist is blacklisted");
			if ( msg != 0 )
				msg->release();
			return;
		}

		if (! indexAvailable_ )
		{

			json_t * flashPayload = json_pack("{s:{s:s}}","settings", "index.store.type", elasticsearchFlashIndexStoreType_.toString().c_str());

			try
			{
				this->createIndex(elasticsearchFlashIndexName_.toString(), flashPayload);
				json_decref(flashPayload);
			}
			catch(xmas::extreme::exception::Exception & e)
			{
				json_decref(flashPayload);
				std::stringstream info;
				info << "Cannot validate index '" << elasticsearchFlashIndexName_.toString() << "' in ES cluster";
				XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, info.str(), e);
				this->notifyQualified("error",q);

				if ( msg != 0 )
					msg->release();
				lossCounters_[qname]++;
				this->resetActiveFlashlists();
				return;

#warning This would keep flooding  with exceptions for each received message, so we might ignore

			}

			json_t * shelfPayload = json_pack("{s:{s:s}}","settings", "index.store.type", elasticsearchShelfIndexStoreType_.toString().c_str());

			try
			{
				this->createIndex(elasticsearchShelfIndexName_.toString(), shelfPayload);
				json_decref(shelfPayload);

			}
			catch(xmas::extreme::exception::Exception & e)
			{
				json_decref(shelfPayload);
				std::stringstream info;
				info << "Cannot validate index '" << elasticsearchShelfIndexName_.toString() << "' in ES cluster";
				XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, info.str(), e);
				this->notifyQualified("error",q);

				if ( msg != 0 )
					msg->release();
				lossCounters_[qname]++;
				this->resetActiveFlashlists();
				return;

#warning This would keep flooding  with exceptions for each received message, so we might ignore

			}

			indexAvailable_ = true;
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Index has been validated");
			//std::cout << "Index has been validated" << std::endl;
		}

		// check if flashlist has already been mapped
		if ( activeFlashList_.find(qname)  != activeFlashList_.end())
		{
			if (!activeFlashList_[qname])
			{
				try
				{
					this->createMapping(elasticsearchFlashIndexName_.toString(), qname);
				}
				catch(xmas::extreme::exception::Exception & e)
				{
					std::stringstream info;
					info << "Cannot validate mapping for  '" << qname << "' in ES cluster";
					XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, info.str(), e);
					this->notifyQualified("error",q);

					if ( msg != 0 )
						msg->release();
					lossCounters_[qname]++;
					indexAvailable_ = false; // trigger reset of all actions, activeFlashList_ is also false
					this->resetActiveFlashlists();
					return;
#warning This would keep flooding  with exceptions for each received message, so we might ignore
				}
//needs to be corrected

				activeFlashList_[qname] = true;
				LOG4CPLUS_INFO(this->getApplicationLogger(), "Mapping has been validated for " << qname);
			}

		}
		else
		{
			std::stringstream info;
			info << "Cannot validate index '" << elasticsearchFlashIndexName_.toString() << "' in ES cluster";
			XCEPT_DECLARE(xmas::extreme::exception::Exception, q, info.str());
			this->notifyQualified("error",q);
			// blacklist this name
			if ( msg != 0 )
				msg->release();
			blackFlashList_[qname] = true;
			return;

		}

		try
		{
			this->publishReport(msg, plist, elasticsearchFlashIndexName_.toString());
			successCounters_[qname]++;
		}
		catch(xmas::extreme::exception::Exception & e)
		{
			std::stringstream info;
			info << "Cannot publish data for  '" << qname << "' in ES cluster";
			XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, info.str(), e);
			this->notifyQualified("error",q);

			indexAvailable_ = false; // trigger reset of all actions
			this->resetActiveFlashlists();
			//activeFlashList_[qname] = false;

			if ( msg != 0 )
				msg->release();
			lossCounters_[qname]++;
			return;
#warning This would keep flooding  with exceptions for each received message, so we might ignore

		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Data has been indexed for " << qname);


	} // valid message

	if ( msg != 0 )
			msg->release();
}

void xmas::extreme::Application::createIndex (const std::string & iname, json_t * args) throw (xmas::extreme::exception::Exception)
{
	es::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Joined cluster :  " << cname);

	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		/*
				XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, ne, msg.str(), e);
				this->notifyQualified("fatal", ne);
				return;
		 */

		XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(),e);
	}

	// check if index exists otherwise it creates

	try
	{
		if ( !cluster.exists(iname, ""))
		{
			try
			{
				//expected {"acknowledged":true}
				json_t * result = cluster.createIndexData(iname, args);
				json_t * acknowledged =json_object_get(result, "acknowledged");
				if (! json_boolean_value(acknowledged))
				{
					std::stringstream msg;
					msg << "failed to create index: '" << json_dumps(result, 0) << "'";
					json_decref(result);
					XCEPT_RAISE(xmas::extreme::exception::Exception, msg.str());
				}

				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Created index : " << json_dumps(result, 0));
				json_decref(result);

			}
			catch(es::api::exception::Exception & e)
			{
				std::stringstream msg;
				msg << "failed to create index with : '" << iname;
				XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str() ,e);
			}

		}
	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not check index:" << iname<< " exists on url: '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(), e);
	}

}



void xmas::extreme::Application::createMapping(const std::string & indexName, const std::string & qname) throw (xmas::extreme::exception::Exception)
{
	toolbox::net::URN flashlistURN(qname);
	std::string fname = flashlistURN.getNSS();

	xmas::FlashListDefinition * flashlist = flashlists_[qname];

	es::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Joined cluster :  " << cname);

	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(), e);
	}

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Converting flashlist : " << qname << " to JSON mapping.");
	json_t * mapping = 0;
	try
	{
		mapping = this->flashToJSON(flashlist);
	}
	catch(xmas::extreme::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not create mapping object for: " << indexName << "and flashlist: " << qname;
		XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(), e);
	}
	//{"error": "MergeMappingException[Merge failed with failures {[mapper [stateName] of different type, current_type [string], merged_type [integer]]}]", "status": 400}
	try
	{
		json_t * result = cluster.createMapping(indexName, fname, mapping);
		json_t * acknowledged =json_object_get(result, "acknowledged");
		if (! json_boolean_value(acknowledged))
		{

			json_t * status =json_object_get(result, "status");

			json_t * error =json_object_get(result, "error");

			std::stringstream msg;
			msg << "Could not create mapping for flashlist : '" << qname  << "' with error '" << json_string_value(error) << "' status result : " << json_integer_value(status) ;
			json_decref(result);
			json_decref(mapping);
			json_decref(acknowledged);
			XCEPT_RAISE(xmas::extreme::exception::Exception, msg.str());

		}

		/* DEBUG
				json_t * completemap = cluster.getMapping(indexName, fname);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Complete Mapping : " << json_dumps(completemap, 0));
				json_decref(completemap);
		 */
		json_decref(result);
		json_decref(mapping);
		json_decref(acknowledged);

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Mapping has been actually created for " << qname);


	}
	catch(es::api::exception::Exception& nae)
	{
		std::stringstream msg;
		msg << "could not create mapping for flashlist : '" << qname  << "'";
		json_decref(mapping);
		XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(),nae);
	}
}

void xmas::extreme::Application::publishReport (toolbox::mem::Reference * msg, xdata::Properties & plist, const std::string & indexName) throw (xmas::extreme::exception::Exception)
{

	std::string name = plist.getProperty("urn:xmas-flashlist:name");
	toolbox::net::URN flashlistURN(name);
	std::string fname = flashlistURN.getNSS();
	/*
	 * $ curl -XPUT 'http://localhost:9200/twitter/tweet/1' -d '{
	 *   "user" : "kimchy",
	 *   "post_date" : "2009-11-15T14:12:12",
	 *   "message" : "trying out Elasticsearch"
	 * }'
	 */
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "joining cluster ... ");
	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	/*try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Joining cluster :  " << cname);

	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url : '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(), e);
	}*/

	//std::string indexName = elasticsearchFlashIndexName_.toString();

	xdata::Table * t = 0;
	try
	{
		t = this->getDataTable(msg);
	}
	catch (xmas::extreme::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not extract data table from message for " << indexName << " with type : " << fname;
		XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(), e);
	}

	std::vector<std::string> columns = t->getColumns();
	//ti.operator=(t.begin());
	for (xdata::Table::iterator ti = t->begin(); ti != t->end(); ti++)
	{
		//xdata::UnsignedLong * number = dynamic_cast<xdata::UnsignedLong *>((*ti).getField("Number"));
		json_t * jsondata = 0;

		try
		{
			jsondata = this->tableRowToJSON(ti, columns, name);//full qualified flashlist name
		}
		catch(xmas::extreme::exception::Exception & e)
		{
			delete t;
			std::stringstream msg;
			msg << "could not create data entry for : " << indexName << " with type : " << fname;
			XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(), e);
		}

		// inject into elasticsearch here ...

		//std::cout << "inserting data for flashlist name : " << fname << std::endl;
		//std::cout << json_dumps(jsondata, 0) << std::endl;

		try
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Indexing data : " << json_dumps(jsondata, 0));
			json_t * result =cluster.index(indexName,fname,"", jsondata);
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Indexing data result:  " << json_dumps(result, 0));
			json_decref(result);
		}
		catch (es::api::exception::Exception & e)
		{
			delete t;
			json_decref(jsondata);
			std::stringstream msg;
			msg << "could not index data for : " << indexName << " and type : " << fname;
			XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(), e);
		}
		json_decref(jsondata);

	}
	delete t;
	return;


}




json_t * xmas::extreme::Application::flashToJSON (xmas::FlashListDefinition * flashlist ) throw (xmas::extreme::exception::Exception)
{
	std::string name = flashlist->getProperty("name");
	toolbox::net::URN flashlistURN(name);
	std::string fname = flashlistURN.getNSS();

	json_t * jsonflash = json_object();
	json_t * jsonproperties = json_object();
	json_t * jsonitems = json_object();

	json_object_set_new( jsonflash, fname.c_str() , jsonproperties );

	json_object_set_new( jsonproperties, "properties",jsonitems);

	// { "<flashlistname>" : {
	//        "properties": {

	std::vector<xmas::ItemDefinition*> items = flashlist->getItems();

	//std::cout << " found num items in flashlist:" << items.size() << std::endl;

	for (std::vector<xmas::ItemDefinition*>::iterator i =  items.begin(); i != items.end(); i++)
	{
		//std::cout << " processing item: " <<  (*i)->getProperty("name") << std::endl;
		std::string iname = (*i)->getProperty("name");
		json_t * jsondef = 0;
		try
		{
			jsondef = this->itemToJSON(*i,name); // recursive
		}
		catch(xmas::extreme::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "could not convert flashlist : " << name;
			json_decref(jsonflash);
			XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(), e);
		}
		json_object_set_new( jsonitems, iname.c_str(),jsondef);

	}

	// add meta data
	json_t * jsondef = json_object();
	json_object_set_new( jsondef, "type", json_string( "string" ) );
	json_object_set_new( jsondef, "store", json_boolean(true) );
	json_object_set_new( jsondef, "index", json_string( "not_analyzed" ) );
	json_object_set_new( jsonitems, "meta_unique_key",jsondef);
	jsondef = json_object();
	json_object_set_new( jsondef, "type", json_string( "string" ) );
	json_object_set_new( jsondef, "store", json_boolean(true) );
	json_object_set_new( jsondef, "index", json_string( "not_analyzed" ) );
	json_object_set_new( jsonitems, "meta_hash_key", jsondef);

	//custom copy_to for hash key used for quick aggregation
	jsondef = json_object();
	json_object_set_new( jsondef, "type", json_string( "string" ) );
	json_object_set_new( jsondef, "store", json_boolean(true) );
	json_object_set_new( jsondef, "index", json_string( "not_analyzed" ) );
	json_object_set_new( jsonitems, "flash_key", jsondef);



	//add _ttl
	json_t * ttl = json_object();
	json_object_set_new( ttl, "enabled" , json_boolean(true));
	json_object_set_new( ttl, "default" , json_string(ttl_.toString().c_str()));
	json_object_set_new( jsonproperties, "_ttl" , ttl );
	//

	/*add _timestamp*/
	 json_t * timestamp = json_object();
	 json_object_set_new( timestamp, "enabled" , json_boolean(true));
	 json_object_set_new( jsonproperties, "_timestamp" , timestamp);

	// }

	return jsonflash;
}


json_t * xmas::extreme::Application::itemToJSON (xmas::ItemDefinition * itemdef, const std::string & fname) throw (xmas::extreme::exception::Exception)
{
	json_t * jsondef = json_object();

	std::string name = itemdef->getProperty("name");
	std::string primitivetype = itemdef->getProperty("type");

	std::vector<xmas::ItemDefinition*> defs = itemdef->getItems();

	// sub properties
	if (defs.size() > 0 )
	{
		json_t * jsonitems = json_object();
		// { "<name>" : {
		//        "properties": {
		for (std::vector<xmas::ItemDefinition*>::iterator i =  defs.begin(); i != defs.end(); i++)
		{
				std::string iname = (*i)->getProperty("name");
				json_t * jsonitem = this->itemToJSON(*i,""); // recursive
				json_object_set_new( jsonitems, iname.c_str(),jsonitem);
		}
		// }
		json_object_set_new( jsondef, "properties",jsonitems);
	}
	else
	{
		try
		{
			std::string elasticsearchType = this->xdaqToElasticsearchType(primitivetype.c_str() );
			// { "<name>" : { "type" : "primitivetype", ... }
			json_object_set_new( jsondef, "type", json_string( elasticsearchType.c_str() ) );
			json_object_set_new( jsondef, "store", json_boolean(true) );
			json_object_set_new( jsondef, "index", json_string( "not_analyzed" ) );


			std::list<std::string> copy_to_list;
			if ( unique_keys_.find(fname) != unique_keys_.end() )
			{
				std::set<std::string> & key = unique_keys_[fname];
				if (key.find(name)  != key.end()) // valid only for first level flashlist items
				{
					copy_to_list.push_back("meta_unique_key");
				}
			}

			if ( hash_keys_.find(fname) != hash_keys_.end() )
			{
				std::set<std::string> & key = hash_keys_[fname];
				if (key.find(name)  != key.end()) // valid only for first level flashlist items
				{
					copy_to_list.push_back("meta_hash_key");
				}
			}
			if (copy_to_list.size() > 0  )
			{
				json_t * jsonvector = json_array();
				for (std::list<std::string>::iterator i = copy_to_list.begin(); i != copy_to_list.end(); i++)
				{
					//std::cout << "adding copy_to for " << name <<  "to " <<  (*i) << std::endl;
					json_array_append_new(jsonvector, json_string((*i).c_str()));
				}
				json_object_set_new( jsondef, "copy_to", jsonvector );
			}


			if (elasticsearchType == "date")
			{
				json_object_set_new( jsondef, "format", json_string("yyyy-MM-dd'T'HH:mm:ss'Z'") );
			}

		}
		catch (xmas::extreme::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "failed to build item : "<< name << " and type : " << primitivetype;
			json_decref(jsondef);
			XCEPT_RETHROW(xmas::extreme::exception::Exception, msg.str(), e);
		}
	}

	return jsondef;
}


json_t * xmas::extreme::Application::tableRowToJSON(xdata::Table::iterator & ti, std::vector<std::string> & columns, const std::string & name) throw (xmas::extreme::exception::Exception)
{
	json_t * document = json_object();

	for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
	{

		//std::string localName = columns[k].substr(columns[k].rfind(":")+1);
		std::string localName = columns[k];


		//std::cout << "Printing the column : " << columns[k] << std::endl;
		//xdata::Serializable * s = t->getValueAt(j, columns[k]);

		xdata::Serializable * s = (*ti).getField(columns[k]);

		if (s->type() == "string")
		{
			std::string value = s->toString();
			json_object_set_new( document, localName.c_str(), json_string( value.c_str()) );
		}
		else if (s->type() == "bool")
		{
			xdata::Boolean* b = dynamic_cast<xdata::Boolean*>(s);
			json_object_set_new( document, localName.c_str(), json_boolean((xdata::BooleanT)*b) );
		}
		else if ( s->type() == "int" )
		{
			xdata::Integer* i = dynamic_cast<xdata::Integer*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::IntegerT)*i) );
		}
		else if ( s->type() == "unsigned int" )
		{
			xdata::UnsignedInteger* i = dynamic_cast<xdata::UnsignedInteger*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedIntegerT)*i) );
		}
		else if ( s->type() == "unsigned int 32" )
		{
			xdata::UnsignedInteger32* i = dynamic_cast<xdata::UnsignedInteger32*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger32T)*i) );
		}
		else if ( s->type() == "unsigned int 64" )
		{
			xdata::UnsignedInteger64* i = dynamic_cast<xdata::UnsignedInteger64*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger64T)*i) );
		}
		else if ( s->type() == "unsigned long" )
		{
			xdata::UnsignedLong* i = dynamic_cast<xdata::UnsignedLong*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedLongT)*i) );
		}
		else if ( s->type() == "unsigned short" )
		{
			xdata::UnsignedShort* i = dynamic_cast<xdata::UnsignedShort*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedShortT)*i) );
		}
		//json_integer needs to change!
		else if ( s->type() == "time" )
		{
			xdata::TimeVal* i = dynamic_cast<xdata::TimeVal*>(s);
			std::string formattedTime = i->value_.toString("%FT%H:%M:%SZ", toolbox::TimeVal::gmt);
			json_object_set_new( document, localName.c_str(), json_string(formattedTime.c_str()) );
		}
		else if ( s->type() == "double" )
		{
			xdata::Double* i = dynamic_cast<xdata::Double*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::DoubleT)*i) );
		}
		else if ( s->type() == "vector unsigned int 32" )
		{
			xdata::Vector<xdata::UnsignedInteger32> * v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(s);

			json_t * jsonvector = json_array();
			for ( xdata::Vector<xdata::UnsignedInteger32>::iterator i = v->begin(); i != v->end(); i++ )
			{
				json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger32T)*i) );
				json_t * val = json_integer(((xdata::UnsignedInteger32T)*i));
				json_array_append_new(jsonvector, val);
			}
			json_object_set_new(document, localName.c_str(),jsonvector);
			//vector treated as primitive type
		}
		else if ( s->type() == "vector unsigned int 64" )
		{
			xdata::Vector<xdata::UnsignedInteger64> * v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>(s);
			json_t * jsonvector = json_array();
			for ( xdata::Vector<xdata::UnsignedInteger64>::iterator i = v->begin(); i != v->end(); i++ )
			{
				json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger64T)*i) );
				json_t * val = json_integer(((xdata::UnsignedInteger64T)*i));
				json_array_append_new(jsonvector, val);
			}
			json_object_set_new(document, localName.c_str(),jsonvector);
			//vector treated as primitive type
		}
		else if (s->type() == "table")
		{
			xdata::Table * st = dynamic_cast<xdata::Table*>(s);
			std::vector<std::string> cols = st->getColumns();
			json_t * jsontable = json_array();
			for (xdata::Table::iterator sti = st->begin(); sti != st->end(); sti++)
			{
				json_t * jsonrow  = this->tableRowToJSON( sti, cols, "");
				//tablename

				json_array_append_new(jsontable, jsonrow);

			}
			json_object_set_new(document, localName.c_str(),jsontable);
		}
		else
		{
			XCEPT_RAISE (xmas::exception::Exception, "Failed to convert data entry,  xdata to es type as "+s->type() );
		}

		//adding the flash key
		if ( hash_keys_.find(name) != hash_keys_.end() )
		{
			std::string flashkeyValue = "@";
			std::set<std::string> & key = hash_keys_[name];
			for (std::set<std::string>::iterator i = key.begin(); i != key.end(); i++) // valid only for first level flashlist items
			{

				xdata::Serializable * s = (*ti).getField(*i);
				flashkeyValue += s->toString() + "-";
			}
			json_object_set_new( document,"flash_key", json_string( flashkeyValue.c_str()) );

		}


	}
	return document;
}
//*out << cgicc::td(s->toString()).set("style","vertical-align: top;");
//xdata::Mime* m = dynamic_cast<xdata::Mime*>(s);

xdata::Table* xmas::extreme::Application::getDataTable(toolbox::mem::Reference * msg) throw (xmas::extreme::exception::Exception)
{
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) msg->getDataLocation(), msg->getDataSize());

	xdata::Table * t = new xdata::Table();

	try
	{
		serializer_.import(t, &inBuffer);
		return t;
	}
	catch (xdata::exception::Exception& e)
	{
		delete t;
		XCEPT_RETHROW (xmas::extreme::exception::Exception, "Failed to deserialize flashlist table", e);
	}

}

std::string xmas::extreme::Application::xdaqToElasticsearchType(const std::string & type) throw (xmas::extreme::exception::Exception)
{
	if ( type == "string")
		return "string";
	else if ( type == "bool" )
	{
		return "boolean";
	}
	else if ( type == "int" )
	{
		return "integer";
	}
	else if ( type == "unsigned int" )
	{
		return "long";
	}
	else if ( type == "unsigned int 32" )
	{
		return "long";
	}
	else if ( type == "unsigned int 64" )
	{
		return "long";
	}
	else if ( type == "unsigned long" )
	{
		return "long";
	}
	else if ( type == "unsigned short" )
	{
		return "integer";
	}
	else if ( type == "time" )
	{
		return "date";
	}
	/*this is resolved by list of sub items directly
	 * else if ( type == "table" )
	{
		return "string";
	}*/
	else if ( type == "double" )
	{
		return "double";
	}
	else if ( type == "vector unsigned int 32" )
	{
		return "long";
	}
	else if ( type == "vector unsigned int 64" )
	{
		return "long";
	}
	else
	{
		XCEPT_RAISE (xmas::exception::Exception, "Failed to convert type xdata to es type as "+ type );

	}

}
void xmas::extreme::Application::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "xdaq::EndpointAvailableEvent")
	{


	}
}

void xmas::extreme::Application::resetActiveFlashlists()
{
	for (std::map<std::string, bool>::iterator i =  activeFlashList_.begin(); i != activeFlashList_.end(); i++)
	{
		(*i).second = false;
	}
}

// TBD check when this sensor setting was called ( in sensro 2g - rendez vous style)
void xmas::extreme::Application::applySensorSettings (std::vector < std::string > & files)
{
	for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
	{
		DOMDocument* doc = 0;
		try
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "apply sensor setting for file  '" << (*j) << "'");
			doc = xoap::getDOMParserFactory()->get("configure")->loadXML(*j);
			std::vector<xmas::MonitorSettings*> settings = xmas::MonitorSettingsFactory::create(doc);

			for (std::vector<xmas::MonitorSettings *>::iterator k = settings.begin(); k != settings.end(); k++)
			{
				xmas::FlashListDefinition * flashlist = (*k)->getFlashListDefinition();
				std::string qname = flashlist->getProperty("name");
				std::set<std::string> key = toolbox::parseTokenSet(flashlist->getProperty("key"), ",");

				/*debug
				//std::cout << "found meta_unique_key ";
				for (std::set<std::string>::iterator w = key.begin(); w  != key.end(); w++)
				{
					std::cout << (*w) << ","	;
				}
				std::cout << std::endl;*/
				//debug

				if ( flashlists_.find(qname) == flashlists_.end())
				{
					successCounters_[qname] = 0;
					lossCounters_[qname] = 0;
					LOG4CPLUS_INFO(this->getApplicationLogger(), "Installed flashlist '" << qname << "'");
					flashlists_[qname] = flashlist;
					activeFlashList_[qname] = false;
					unique_keys_[qname] = key;
				}
				else
				{
					std::stringstream msg;
					msg << "Failed to create flashlist entry ( already existing) : '" << qname << "'";
					XCEPT_DECLARE(xmas::extreme::exception::Exception, q, msg.str());
					this->notifyQualified("error", q);
				}

				//delete (*k); // the vector goes out of scope at the end of the function
			}
			doc->release();
			doc = 0;
		}
		catch (xoap::exception::Exception& e)
		{
			// WE THINK, if directory services are not running, this is where it will fail. not fatal
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to load configuration file from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			return;
		}
		catch (xmas::exception::Exception& e)
		{
			// this is a parse error
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to parse configuration from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			continue;
		}
	}

}

void xmas::extreme::Application::applyCollectorSettings (std::vector < std::string > & files)
{
	for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
	{
		DOMDocument* doc = 0;
		try
		{
			LOG4CPLUS_INFO(this->getApplicationLogger(), "apply collector setting for file  '" << (*j) << "'");
			doc = xoap::getDOMParserFactory()->get("configure")->loadXML(*j);
			DOMNodeList* flashlistList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"), xoap::XStr("flashlist"));
				for (XMLSize_t i = 0; i < flashlistList->getLength(); i++)
				{
					DOMNode * flashlistNode = flashlistList->item(i);
					std::string qname = xoap::getNodeAttribute (flashlistNode, "name");

					DOMNodeList* collectorList = flashlistNode->getChildNodes();
					for (XMLSize_t j = 0 ; j < collectorList->getLength() ; j++)
					{
						DOMNode * collectorNode = collectorList->item(j);
						std::string nodeName = xoap::XMLCh2String(collectorNode->getLocalName());
						if(nodeName == "collector")
						{
							std::set<std::string> key = toolbox::parseTokenSet(xoap::getNodeAttribute (collectorNode, "hashkey"), ",");
							hash_keys_[qname] = key;
							continue;
						}
					}
				}



			doc->release();
			doc = 0;
		}
		catch (xoap::exception::Exception& e)
		{
			// WE THINK, if directory services are not running, this is where it will fail. not fatal
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to load collector file from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			return;
		}
		catch (xmas::exception::Exception& e)
		{
			// this is a parse error
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to parse collector from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::extreme::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			continue;
		}
	}

}
std::vector<std::string> xmas::extreme::Application::getFileListing (const std::string& directoryURL) throw (xmas::extreme::exception::Exception)
{
	std::vector < std::string > list;
	try
	{
		XMLURL xmlUrl(directoryURL.c_str());
		//BinInputStream* s = accessor->makeNew (xmlUrl);
		BinInputStream* s = xmlUrl.makeNewStream();

		if (s == 0)
		{
			std::string msg = "Failed to access ";
			msg += directoryURL;
			XCEPT_RAISE(xoap::exception::Exception, msg);
		}

		int r = 0;
		int curPos = 0;

		int bufferSize = 2048;
		XMLByte * buffer = new XMLByte[bufferSize];
		std::stringstream ss;
		while ((r = s->readBytes(buffer, bufferSize)) > 0)
		{
			int read = s->curPos() - curPos;
			curPos = s->curPos();
			ss.write((const char*) buffer, read);
		}

		delete s;
		delete[] buffer;

		while (!ss.eof())
		{
			std::string fileName;
			std::getline(ss, fileName);
			// apply blob
			// if "filename.sensorsomehtingelse" exists, we dont care
			if (fileName.find(".sensor") != std::string::npos)
			{
				list.push_back(fileName);
			}
		}

		return list;
	}
	catch (NetAccessorException& nae)
	{
		XCEPT_RAISE(xmas::extreme::exception::Exception, xoap::XMLCh2String(nae.getMessage()));
	}
	catch (MalformedURLException& mue)
	{
		XCEPT_RAISE(xmas::extreme::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	}
	catch (std::exception& se)
	{
		XCEPT_RAISE(xmas::extreme::exception::Exception, se.what());
	}
	catch (...)
	{
		std::string msg = "Caught unknown exception during 'get' operation on url ";
		msg += directoryURL;
		XCEPT_RAISE(xmas::extreme::exception::Exception, msg);
	}
}

void xmas::extreme::Application::displayFlashlist(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);
	std::string flashlistName = cgi["name"]->getValue();

	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string indexName = elasticsearchFlashIndexName_.toString();

	//LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Searching for Flashlist : " << json_dumps(completemap, 0));

	try
	{
		json_t * flashlistData = cluster.search(indexName, flashlistName, "", 0);
		*out << json_dumps(flashlistData, 0) << std::endl;
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Flashlist for " << flashlistName << " has been found. ");
		json_decref(flashlistData);

	}
	catch(xmas::extreme::exception::Exception & e)
	{
		XCEPT_RETHROW(xmas::extreme::exception::Exception, "failed to retrieve flashlist", e);
	}




}

void xmas::extreme::Application::displayFlashlistMapping(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);
	std::string flashlistName = cgi["name"]->getValue();

	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string indexName = elasticsearchFlashIndexName_.toString();

	try
	{
		json_t * mappingData = cluster.getMapping(indexName, flashlistName);
		*out << json_dumps(mappingData, 0) << std::endl;
		json_decref(mappingData);
	}
	catch(xmas::extreme::exception::Exception & e)
	{
		XCEPT_RETHROW(xmas::extreme::exception::Exception, "failed to retrieve flashlist mapping", e);
	}

}
/*
void xmas::extreme::Application::displayLatestData(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);
	std::string flashlistName = cgi["name"]->getValue();

	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string indexName = elasticsearchIndexName_.toString();

	try
	{
	//WEB -> http://localhost:9500/flashlist/jobcontrol/_search?sort=timestamp:desc&size=1

		std::string properties = "sort=timestamp:desc&size=1";
		json_t * result = cluster.search(indexName, flashlistName, properties, 0);
		*out << json_dumps(result, 0) << std::endl;
		std::cout << "result  = : " << json_dumps(result, 0) << std::endl;
		json_decref(result);
	}
	catch(xmas::extreme::exception::Exception & e)
	{
		XCEPT_RETHROW(xmas::extreme::exception::Exception, "failed to retrieve latest document", e);
	}
}
*/

//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void xmas::extreme::Application::TabPanel (xgi::Output * out)
{
	*out << "<script type=\"text/javascript\" src=\"/xmas/extreme/html/js/xmas-extreme.js\"></script> " << std::endl;
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\" id=\"tabPage2\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Flashlists\" id=\"tabPage3\">"<< std::endl;

	this->FlashlistsTabPage(out);
	*out << "</div> "<< std::endl;

	//panel-end-div
	*out << "</div>";
}

void xmas::extreme::Application::StatisticsTabPage (xgi::Output * out)
{
	//Dialup

	// Per flashlist loss of reports
	*out << cgicc::table().set("class","xdaq-table");
	*out << cgicc::caption("Flashlist");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class","xdaq-case").set("style", "min-width: 100px;");
	*out << cgicc::th("Active");
	*out << cgicc::th("Success");
	*out << cgicc::th("Loss");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	*out << cgicc::tbody();

	for (std::map<std::string, xmas::FlashListDefinition*>::iterator i = flashlists_.begin(); i != flashlists_.end(); i++ )
	{

			xmas::FlashListDefinition * flashlist = (*i).second;
			std::string name = flashlist->getProperty("name");
			toolbox::net::URN flashlistURN(name);
			std::string fname = flashlistURN.getNSS();

			*out << cgicc::tr() << std::endl;
			*out << cgicc::td(fname);

			*out << cgicc::td((activeFlashList_[name]) ?"true":"false");

			*out << cgicc::td(toolbox::toString("%d",successCounters_[name]));
			*out << cgicc::td(toolbox::toString("%d",lossCounters_[name]));

			*out << cgicc::tr() << std::endl;

	}

	*out << cgicc::tbody();
	*out << cgicc::table();

}
void xmas::extreme::Application::FlashlistsTabPage (xgi::Output * out){

	std::stringstream baseurl;
	baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

	*out << "<div id=\"flashlist-selection\">"<< std::endl;

	*out << "<button id=\"flashlist-refresh\" data-url=\"" << baseurl.str() << "\" >Reload Flashlist</button>" << std::endl;

	*out << " <select id=\"flashlist-combo\" style=\"width:350px\"> " << std::endl;
	*out << " <option  selected=\"selected\">Select a Flashlist ... </option>" << std::endl;
	for (std::map<std::string, xmas::FlashListDefinition*>::iterator i = flashlists_.begin(); i != flashlists_.end(); i++ )
	{
		xmas::FlashListDefinition * flashlist = (*i).second;
		std::string name = flashlist->getProperty("name");
		toolbox::net::URN flashlistURN(name);
		std::string fname = flashlistURN.getNSS();
		*out << "<option>" << fname << "</option>" << std::endl;
	}
	*out << " </select>" << std::endl;

	*out << " <select id=\"display-combo\" style=\"width:350px\"> " << std::endl;
	*out << " <option selected=\"selected\">Summary</option>" << std::endl;
	*out << " <option>Mapping</option>" << std::endl;
	*out << " <option>Table Data</option>" << std::endl;
	*out << " <option>JSON Data</option>" << std::endl;
	*out << " <option>Latest Data</option>" << std::endl;
	*out << " </select>" << std::endl;

	*out << "</div> "<< std::endl;

	*out << "<div id=\"flashlist-data\">"<< std::endl;
	*out << "</div> "<< std::endl;
}

void xmas::extreme::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{

	this->TabPanel(out);

}
