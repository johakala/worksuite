/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R. Moser                                                     *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xmas/broker2g/Model.h"
#include <sstream>

#include "xdata/Double.h"
#include "xdata/String.h"

xmas::broker2g::Model::Model(xdaq::Application * owner, const std::string& name) :
	xdaq::Object(owner),
	infospace_("urn:broker:"+name)
{
	name_ = name;
}

xmas::broker2g::Model::~Model()
{
}

xdata::InfoSpace* xmas::broker2g::Model::getInfoSpace()
{
	return &infospace_;
}

std::string xmas::broker2g::Model::getName()
{
	return name_;
}

