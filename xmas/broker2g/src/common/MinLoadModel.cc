/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xmas/broker2g/MinLoadModel.h"
#include "xmas/broker2g/Utils.h"
#include <sstream>
#include <set>

#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Vector.h"
#include "xdata/TimeVal.h"

#include "toolbox/string.h"
#include "toolbox/Runtime.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

XMAS_BROKER2G_MODEL_INSTANTIATOR_IMPL(xmas::broker2g::MinLoadModel);

xmas::broker2g::MinLoadModel::MinLoadModel(xdaq::Application * owner, const std::string& name) :
	xmas::broker2g::Model(owner, name)
{
	this->getInfoSpace()->fireItemAvailable("settings",      &settings_);
	this->getInfoSpace()->fireItemAvailable("service",       &service_);
	this->getInfoSpace()->fireItemAvailable("group",         &group_);
	this->getInfoSpace()->fireItemAvailable("maximumLoad",   &maximumLoad_);
	this->getInfoSpace()->fireItemAvailable("increaseLoad",  &increaseLoad_);
	this->getInfoSpace()->fireItemAvailable("maximumClients",&maximumClients_);

	requests_ = 0;
	results_ = 0;
}

xmas::broker2g::MinLoadModel::~MinLoadModel()
{
}

void xmas::broker2g::MinLoadModel::loaded() throw(xmas::broker2g::exception::ParserException)
{
	DOMDocument* doc = 0;

	std::vector<std::string> files;
	try 
	{ 
		files = toolbox::getRuntime()->expandPathName(settings_);
	}   
	catch (toolbox::exception::Exception& tbe)
	{
		XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Cannot parse pathname " + (std::string)settings_, tbe);
	}   

	if(files.size() != 1)
	{
		XCEPT_RAISE (xmas::broker2g::exception::ParserException, "Settings filename not unique: " + (std::string)settings_);
	}

	try
	{
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "CollectorModel loading settings from " << files[0]);
		doc = xmas::broker2g::loadDOM(files[0]);
	}
	catch (xmas::broker2g::exception::ParserException& xe)
	{
		XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Failed to load settings from " + (std::string)settings_, xe);
	}

	DOMNodeList* flashlistList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"), xoap::XStr("flashlist"));
	for (XMLSize_t i = 0; i < flashlistList->getLength(); i++)
	{ 
		DOMNode * flashlistNode = flashlistList->item(i);
		std::string flashlistName = xoap::getNodeAttribute (flashlistNode, "name");

		std::vector<xmas::broker2g::SensorSettings*> flashlistSettings;

		DOMNodeList* sensorList = flashlistNode->getChildNodes();
		for (XMLSize_t j = 0 ; j < sensorList->getLength() ; j++)
		{
			DOMNode * sensorNode = sensorList->item(j);
			std::string nodeName = xoap::XMLCh2String(sensorNode->getLocalName());
			if(nodeName == "sensor")
			{
				SensorSettings* settings = new SensorSettings(sensorNode, flashlistName);
				flashlistSettings.push_back(settings);
				LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Loaded sensor settings for flashlist [" << flashlistName << "]");
				LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(),
					"Flashlist [" << settings->getFlashlistName() << "]" <<
					"publish [" << settings->getPublish() << "]");
			}
		}

		if(!flashlistSettings.empty())
		{
				sensorSettings_[flashlistName] = flashlistSettings;
				sensorCounts_[flashlistName] = 0;
		}
	}
}

void xmas::broker2g::MinLoadModel::update(xdata::Table::Reference table) throw(xmas::broker2g::exception::ParserException)
{
	try
	{
		xdata::Serializable* value;

		// do not save flashlist if service or group do not match
		if( service_!="" )
		{
			value = table->getValueAt(0, "service");
			xdata::String* service = dynamic_cast<xdata::String*>(value);
			if( service_!=(*service) )
			{
				return;
			}
		}

		if( group_!="" )
		{
			value = table->getValueAt(0, "group");
			xdata::String* group = dynamic_cast<xdata::String*>(value);
			std::set<std::string> groups = toolbox::parseTokenSet(*group,",");
			if( groups.find(*group) == groups.end() )
			{
				return;
			}
		}

		// create new Table because some properties are modified on query()
		xdata::Table* newtable = new xdata::Table(*table);
		xdata::Table::Reference t(newtable);

		value = t->getValueAt(0, "url");
		xdata::String* url = dynamic_cast<xdata::String*>(value);

		services_[(std::string)*url] = t;
	}
	catch(xdata::exception::Exception& e)
	{
		XCEPT_RETHROW(xmas::broker2g::exception::ParserException, "Could not parse flashlist", e);
	}
}

xdata::Properties xmas::broker2g::MinLoadModel::query(xdata::Properties& plist)
{
	xdata::Double            *foundload    = &maximumLoad_;
	xdata::UnsignedInteger32 *foundclients = 0;
	xdata::String            *foundurl     = 0;
	xdata::String            *foundcontext     = 0;

	std::map<std::string, xdata::Table::Reference>::iterator iter;
	for(iter = services_.begin() ; iter != services_.end() ; iter++)
	{
		xdata::Table::Reference t = (*iter).second;

		try
		{
			xdata::Serializable* value;

			value = t->getValueAt(0, "url");
			xdata::String* url = dynamic_cast<xdata::String*>(value);
			value = t->getValueAt(0, "load");
			xdata::Double* load = dynamic_cast<xdata::Double*>(value);
			value = t->getValueAt(0, "clients");
			xdata::UnsignedInteger32* clients = dynamic_cast<xdata::UnsignedInteger32*>(value);
			value = t->getValueAt(0, "service");
			xdata::String* service = dynamic_cast<xdata::String*>(value);
			value = t->getValueAt(0, "group");
			xdata::String* group = dynamic_cast<xdata::String*>(value);
			value = t->getValueAt(0, "context");
			xdata::String * context = dynamic_cast<xdata::String*>(value);

			// ignore flashlists with missing properties
			if(context==NULL||clients==NULL || load==NULL || url==NULL || service==NULL || group==NULL)
			{
				continue;
			}

			// check for previously assigned service
			if((*url)==plist.getProperty("urn:xmasbroker2g:result0"))
			{
				// decrement previously used services
				if(((unsigned long)*clients) != 0)
				{
					*clients = *clients -1;
				}
				*load = *load - increaseLoad_;
				if(((double)*load) < 0) *load=0;

				// if previous assigned service not fully loaded -> reuse it
				if((*load)<=maximumLoad_)
				{
					foundload    = load;
					foundclients = clients;
					foundurl     = url;
					foundcontext = context;
					break;
				}
			}

			// requested service type matching?
			if(plist.getProperty("urn:xmasbroker2g:service")!="" && (*service)!=plist.getProperty("urn:xmasbroker2g:service"))
			{
				continue;
			}

			// requested group matching?
			std::set<std::string> groups = toolbox::parseTokenSet(*group,",");
			if( groups.size()==0 || groups.find(plist.getProperty("urn:xmasbroker2g:group")) == groups.end() )
			{
				continue;
			}

			// service already has more than maximum allowed clients assigned
			if(*clients>=maximumClients_)
			{
				continue;
			}

			// smaller load found using this service?
			if((*load)>(*foundload))
			{
				continue;
			}

			foundload    = load;
			foundclients = clients;
			foundurl     = url;
			foundcontext = context;
		}
		catch(xdata::exception::Exception& e)
		{
			// ignore flashlists with missing properties
		}
	}

	xdata::Properties returnplist;

	std::list<std::string> results;
	if(foundurl != 0)
	{
		// increase load by increaseLoad_
		*foundload = *foundload + increaseLoad_;
		*foundclients = *foundclients + 1;
		results.push_back(foundurl->toString());
		returnplist.setProperty("urn:xmasbroker2g:result0", foundurl->toString());
		returnplist.setProperty("urn:xmasbroker2g:context", foundcontext->toString());
	}

	// counting of requests and how many times a valid service has been found
	requests_++;
	if(results.size()!=0)
	{
		results_++;
	}

	std::set<std::string> flashlists = toolbox::parseTokenSet(plist.getProperty("urn:xmasbroker2g:flashlists"),",");
	for(std::set<std::string>::iterator iter = flashlists.begin() ; iter != flashlists.end() ; iter++)
	{
		std::string name = (*iter);
		std::string oldtopic = plist.getProperty(name);
		if(oldtopic == "")
		{
			std::map<std::string, std::vector<xmas::broker2g::SensorSettings*> >::iterator found;
			found = sensorSettings_.find(name);
			if(found != sensorSettings_.end())
			{
				// assign new topic if sensor did not have one before
				std::vector<xmas::broker2g::SensorSettings*> settings = (*found).second;
				std::map<std::string, size_t>::iterator citer = sensorCounts_.find(name);
				size_t count = 0;
				if(citer != sensorCounts_.end())
				{
					count = (*citer).second;
				}
				returnplist.setProperty(name, settings[count]->getPublish());
				count = (count + 1) % settings.size();
				sensorCounts_[name] = count;
			}
			else
			{
				// flashlists has not been defined - do not return a topic
			}
		}
		else
		{
			// reassign old topic
			returnplist.setProperty(name, oldtopic);
		}
	}

	returnplist.setProperty("urn:xmasbroker2g:group",           plist.getProperty("urn:xmasbroker2g:group"));
	returnplist.setProperty("urn:xmasbroker2g:service",         plist.getProperty("urn:xmasbroker2g:service"));

	return returnplist;
}

void xmas::broker2g::MinLoadModel::view(xgi::Output * out )
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	// Incoming Requests
	//
	*out << cgicc::tr();
	*out << cgicc::th("Requests");
	*out << cgicc::td().set("style","min-width: 100px;");
	*out << requests_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Found Responses
	//
	*out << cgicc::tr();
	*out << cgicc::th("Found");
	*out << cgicc::td();
	*out << results_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Lost Responses
	//
	*out << cgicc::tr();
	*out << cgicc::th("Lost");
	*out << cgicc::td();
	*out << (requests_-results_);
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Number of found services
	//
	*out << cgicc::tr();
	*out << cgicc::th("Found Services");
	*out << cgicc::td();
	*out << services_.size();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << cgicc::br() << std::endl; 

	bool tableInit = false;

	for ( std::map<std::string, xdata::Table::Reference>::iterator w = services_.begin(); w!=services_.end() ; w++ )
	{
		xdata::Table::Reference tref;
		
		tref = (*w).second;
		
		if ( tref.isNull() )
		{
			LOG4CPLUS_WARN(getOwnerApplication()->getApplicationLogger(), "denormalized empty tables are skipped, a notification" );
			continue;
		}

		std::vector<std::string> columns = tref->getColumns();

		if (!tableInit)
		{
			*out << cgicc::table().set("class", "xdaq-table") << std::endl;

			*out << cgicc::thead();
			*out << cgicc::tr();
			// By default it denormalize table
			for (std::vector<std::string>::size_type i = 0; i < columns.size(); i++ )
			{
				std::string localName = columns[i].substr(columns[i].rfind(":")+1);
				*out << cgicc::th(localName).set("title",columns[i]);
			}
			*out << cgicc::tr() << std::endl;
			*out << cgicc::thead();
	
			*out << cgicc::tbody();

			tableInit = true;
		}

		for ( size_t j = 0; j <  tref->getRowCount(); j++ )
		{
			*out << cgicc::tr() << std::endl;
			for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
			{
				xdata::Serializable * s = tref->getValueAt(j, columns[k]);

				if (s->type() == "mime")
				{

					*out << cgicc::td("MIME");
				}
				else if (s->type() == "table")
				{
					*out << cgicc::td("TABLE");
				}
				else
				{						
					*out << cgicc::td(s->toString());
				}
			}
			*out << cgicc::tr();
		}
	}

	if (tableInit)
	{
		*out << cgicc::tbody();
		*out << cgicc::table();
	}
}

void xmas::broker2g::MinLoadModel::cleanup(toolbox::TimeVal timeval)
{
	std::map<std::string, xdata::Table::Reference>::iterator iter = services_.begin();
	while(iter != services_.end())
	{
		xdata::Table::Reference t = (*iter).second;

		try
		{
			xdata::Serializable* value = t->getValueAt(0, "timestamp");
			xdata::TimeVal* timestamp = dynamic_cast<xdata::TimeVal*>(value);
			if((*timestamp) < xdata::TimeVal(timeval))
			{
				std::map<std::string, xdata::Table::Reference>::iterator deliter = iter;
				iter++;
				services_.erase(deliter);
			}
			else
			{
				iter++;
			}
		}
		catch(xdata::exception::Exception& e)
		{
			// ignore flashlists with missing properties
			iter++;
		}
	}
}

