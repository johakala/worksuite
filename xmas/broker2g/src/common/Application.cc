// $Id: Application.cc,v 1.15 2009/02/20 13:59:25 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "xmas/broker2g/Application.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "xgi/framework/Method.h"

#include "xcept/tools.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/Table.h"

#include "xplore/DiscoveryEvent.h"

#include "toolbox/TimeVal.h"


XDAQ_INSTANTIATOR_IMPL(xmas::broker2g::Application);

xmas::broker2g::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this), b2inEventingProxyInput_(0), b2inBrokerProxyOutput_(0)
{	
	s->getDescriptor()->setAttribute("icon", "/xmas/broker2g/images/broker2g-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/broker2g/images/broker2g-icon.png");

	subscribeGroup_      = "";
	watchdog_            = "";
	scanPeriod_          = "PT10S";
	subscribeExpiration_ = "PT30S";
	topics_              = "";
	autoloadPath_        = "${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/broker";

	this->getApplicationInfoSpace()->fireItemAvailable("subscribeGroup",      &subscribeGroup_);
	this->getApplicationInfoSpace()->fireItemAvailable("watchdog",            &watchdog_);
	this->getApplicationInfoSpace()->fireItemAvailable("scanPeriod",          &scanPeriod_);
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeExpiration", &subscribeExpiration_);
	this->getApplicationInfoSpace()->fireItemAvailable("topics",              &topics_);
	this->getApplicationInfoSpace()->fireItemAvailable("autoloadPath",        &autoloadPath_);

	b2inBrokerProxyOutput_  =  new b2in::utils::ServiceProxy(this, "", "", this);

	// bind B2IN callbacks
	b2in::nub::bind(this, &xmas::broker2g::Application::onMessage );

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &xmas::broker2g::Application::Default, "Default");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

xmas::broker2g::Application::~Application()
{
	if(modelRegistry_ != NULL)
	{
		delete modelRegistry_;
		modelRegistry_ = NULL;
	}
}


//
// Infospace listener
//

void xmas::broker2g::Application::actionPerformed( xdata::Event& event) 
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		eventingErrorHandler_ = new xmas::broker2g::EventingCommunicationErrorHandler(this);
		b2inEventingProxyInput_  =  new b2in::utils::ServiceProxy(this, "b2in-eventing", subscribeGroup_.toString(), eventingErrorHandler_);

		toolbox::task::Timer * timer = 0;
		// Create timer for refreshing subscriptions
		if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:broker2g-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:broker2g-timer");		
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:broker2g-timer");
		}
	
		// submit task
		toolbox::TimeInterval interval;
		interval.fromString(scanPeriod_.toString()); // in seconds
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		timer->scheduleAtFixedRate( start, this, interval, 0, "urn:xmas:broker2g-task" );

	
		// Initialize ModelRegistry and watchdog timer for clearing old services
		toolbox::TimeInterval watchdoginterval;
		try
		{
			if(watchdog_ != "")
			{
				watchdoginterval.fromString(watchdog_.toString());
			}
			else
			{
				watchdoginterval = toolbox::TimeInterval(0,0);
			}
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Watchdog interval [" << watchdoginterval << "]");
			modelRegistry_ = new xmas::broker2g::ModelRegistry(this, watchdoginterval, autoloadPath_.toString());
		}
		catch(xcept::Exception& e)
		{
			XCEPT_RETHROW (xdaq::exception::Exception, e.message(), e);
		}
	}
	else
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}

//
// XGI SUpport
//
void xmas::broker2g::Application::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
	
	// Tabbed pages
	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	std::map<std::string, xmas::broker2g::Model*> models = modelRegistry_->getModels();
	std::map<std::string, xmas::broker2g::Model*>::iterator iter;
	for( iter=models.begin() ; iter!=models.end() ; iter++)
	{
		std::string name = (*iter).first;

		*out << "<div class=\"xdaq-tab\" title=\"" << name << "\">" << std::endl;
		modelRegistry_->view(name, out);
		*out << "</div>";
	}

	*out << "</div>";
}

void xmas::broker2g::Application::StatisticsTabPage( xgi::Output * out )
{
	// print eventings
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Eventing");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	
	*out << cgicc::tbody();

	if(b2inEventingProxyInput_)
	{
		try
		{
			b2in::utils::MessengerCache* messengerCache = b2inEventingProxyInput_->getMessengerCache();
			std::list<std::string> destinations = messengerCache->getDestinations();
			std::list<std::string>::iterator iter;
			for( iter=destinations.begin() ; iter!=destinations.end() ; iter++)
			{
				*out << cgicc::tr();
				*out << cgicc::td(*iter) << std::endl;
				*out << cgicc::tr() << std::endl;
			}
		}
		catch(b2in::utils::exception::Exception& e)
		{
			// no messenger cache created -> ignore
		}
	}

	*out << cgicc::tbody();	
	*out << cgicc::table();

	*out << "<br />" << std::endl;

	// print topics
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Topics");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	
	*out << cgicc::tbody();

	std::set<std::string> topics = toolbox::parseTokenSet(topics_.toString(), ",");
	std::set<std::string>::iterator i;
	for (i = topics.begin(); i != topics.end(); ++i)
	{
		*out << cgicc::tr();
		*out << cgicc::td(*i) << std::endl;
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();	
	*out << cgicc::table();
}

//
// Discovery service
//

void xmas::broker2g::Application::timeExpired(toolbox::task::TimerEvent& event)
{
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Timer callback");
	
	try
	{
		b2inEventingInputScan_  = ! b2inEventingProxyInput_->scan();		
	}
	catch (b2in::utils::exception::Exception& e)
	{
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
	
	try
	{
		this->refreshSubscriptionsToEventing();
	}
	catch (xmas::exception::Exception& e)
	{
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
}

void xmas::broker2g::Application::refreshSubscriptionsToEventing() throw (xmas::exception::Exception)
{
	if (subscriptions_.empty())
	{
		// retrieve address of the local b2in endpoint
		LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Prepare subscription records");
		const xdaq::Network * network = 0;
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
		try
		{
			network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
		}
		catch (xdaq::exception::NoNetwork& nn)
		{
			std::stringstream msg;
			msg << "Failed to access b2in network " << networkName << ", not configured";
			XCEPT_RETHROW (xmas::exception::Exception, msg.str(), nn);
		}

		try
		{
			pt::Address::Reference localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());

			// Subscriptions are done based on a property in the model
			//
			std::set<std::string> topics = toolbox::parseTokenSet(topics_.toString(), ",");

			std::set<std::string>::iterator i;
			for (i = topics.begin(); i != topics.end(); ++i)
			{
				if ( subscriptions_.find(*i) == subscriptions_.end())
				{
					xdata::Properties plist;
					toolbox::net::UUID identifier;
					plist.setProperty("urn:b2in-eventing:action", "subscribe");
					plist.setProperty("urn:b2in-eventing:id", identifier.toString());
					plist.setProperty("urn:b2in-eventing:topic", (*i)); // Subscribe to collected data
					plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching
					plist.setProperty("urn:b2in-eventing:subscriberurl", localAddress->toString());
					plist.setProperty("urn:b2in-eventing:subscriberservice", "xmasbroker2g");
					plist.setProperty("urn:b2in-eventing:expires", subscribeExpiration_.toString("xs:duration"));
					subscriptions_[(*i)] = plist;

				}
			}
		}
		catch (xdaq::exception::NoEndpoint& nn)
		{
			std::stringstream msg;
			msg << "Failed to get address for context on network: " << networkName;
			XCEPT_RETHROW (xmas::exception::Exception, msg.str(), nn);
		}
	}
	
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "subscribe/Renew");
	b2in::utils::MessengerCache * messengerCache = 0;
	try
	{
		messengerCache = b2inEventingProxyInput_->getMessengerCache();
	}
	catch(b2in::utils::exception::Exception & e)
	{
		XCEPT_RETHROW (xmas::exception::Exception, "cannot access messenger cache", e);
	}
	
	// This lock is available all the time excepted when an asynchronous send will fail and a cache invalidate is performed
	// Therefore there is no loss of efficiency.
	//
	
	std::list<std::string> destinations = messengerCache->getDestinations();	
	
	for (std::list<std::string>::iterator j = destinations.begin(); j != destinations.end(); j++ )
	{
		//
		// Subscribe to OR Renew all existing subscriptions
		//
		std::map<std::string, xdata::Properties>::iterator i;
		for ( i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
		{
			try
			{
				// plist is already prepared for a subscribe/resubscribe message
				//
				LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Subscribe to topic " << (*i).first);
				messengerCache->send((*j),0,(*i).second);
			}
			catch (b2in::nub::exception::InternalError & e)
			{
				std::stringstream msg;
				msg << "failed to send subscription for topic (internal error) " << (*i).first;

				XCEPT_DECLARE_NESTED(xmas::broker2g::exception::Exception, ex , msg.str(), e);
				this->notifyQualified("fatal",ex);
				return;
			}
			catch ( b2in::nub::exception::QueueFull & e )
			{
				std::stringstream msg;
				msg << "failed to send subscription for topic (queue full) " << (*i).first;

				XCEPT_DECLARE_NESTED(xmas::broker2g::exception::Exception, ex , msg.str(), e);
				this->notifyQualified("error",ex);
				return;
			}
			catch ( b2in::nub::exception::OverThreshold & e)
			{
				// ignore just count to avoid verbosity                                
				return;
			}
		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "done with subscription");
	}
}


//
// Handling of B2IN messages from eventing
//

void xmas::broker2g::Application::onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) 
	throw (b2in::nub::exception::Exception)
{
	std::string brokerAction = plist.getProperty("urn:xmasbroker2g:action");
	if(brokerAction == "query")
	{
		// response url and service name for response
		std::string url        = plist.getProperty("urn:xmasbroker2g:url");
		std::string originator = plist.getProperty("urn:xmasbroker2g:originator");

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Query [model=" << plist.getProperty("urn:xmasbroker2g:model") << "]");

		xdata::Properties returnplist;
		try
		{
			returnplist = modelRegistry_->query(plist);
		}
		catch(xcept::Exception& e)
		{
			std::stringstream msg;
			msg << "Model '" << plist.getProperty("urn:xmasbroker2g:model") << "' not found";
			LOG4CPLUS_WARN(this->getApplicationLogger(), msg.str() );
			XCEPT_DECLARE_NESTED(xmas::broker2g::exception::Exception, ex, msg.str(),e);
			this->notifyQualified("warn",ex);
		}

		if(!returnplist.empty())
		{
			// b2in protocol properties
			returnplist.setProperty("urn:b2in-protocol:service",        originator);
			returnplist.setProperty("urn:b2in-protocol-tcp:connection", "close");

			// broker response
			returnplist.setProperty("urn:xmasbroker2g:action",          "allocate");
			returnplist.setProperty("urn:xmasbroker2g:model",           plist.getProperty("urn:xmasbroker2g:model"));

			try
			{
				b2inBrokerProxyOutput_->addURL(url);	
			}
			catch(b2in::utils::exception::Exception & e)
			{
				std::stringstream msg;
				msg << "Failed to parse URL " << url << " to send broker response to";
				XCEPT_DECLARE_NESTED(xmas::broker2g::exception::Exception, ex , msg.str() , e);
				this->notifyQualified("warn",ex);
			}

			b2in::utils::MessengerCache * messengerCache = 0;
			try
			{
				messengerCache = b2inBrokerProxyOutput_->getMessengerCache();
			}
			catch(b2in::utils::exception::Exception & e)
			{
				XCEPT_RETHROW (xmas::exception::Exception, "cannot access messenger cache", e);
			}
	
			try
			{
				LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Sending Allocate");
				messengerCache->send(url, 0, returnplist);
			}
			catch (b2in::nub::exception::InternalError & e)
			{
				std::stringstream msg;
				msg << "failed to send allocate to client (internal error) on url " << url;
				XCEPT_DECLARE_NESTED(xmas::broker2g::exception::Exception, ex , msg.str() , e);
				this->notifyQualified("warn",ex);
			}
			catch ( b2in::nub::exception::QueueFull & e )
			{
				std::stringstream msg;
				msg << "failed to send allocate to client (queue full) on url " << url;
				XCEPT_DECLARE_NESTED(xmas::broker2g::exception::Exception, ex , msg.str() , e);
				this->notifyQualified("error",ex);
			}
			catch ( b2in::nub::exception::OverThreshold & e)
			{
				// ignore 				
			}
		}
	}
	else
	{
		std::string flashlistName = plist.getProperty("urn:xmas-flashlist:name");
		if(flashlistName == "")
		{
			std::stringstream msg;
			msg << "Incoming B2IN message corrupted, 'urn:xmas-flashlist:name' property is empty";
			LOG4CPLUS_WARN(this->getApplicationLogger(), msg.str() );
			XCEPT_DECLARE(xmas::broker2g::exception::Exception, e, msg.str());
			this->notifyQualified("warn",e);
		}
		else if(msg != 0)
		{
			xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*)msg->getDataLocation(), msg->getDataSize());
			xdata::Table * t = new xdata::Table();
			xdata::Table::Reference  table (t);				

			try 
			{
				serializer_.import(t, &inBuffer );
				modelRegistry_->update(table);
			}
			catch(xdata::exception::Exception & e )
			{
				delete t;
				LOG4CPLUS_ERROR(this->getApplicationLogger(),xcept::stdformat_exception_history(e));
			}
		}
		else
		{
			std::stringstream msg;
			msg << "Empty data for flashlist '" << flashlistName << "'";
			LOG4CPLUS_WARN(this->getApplicationLogger(), msg.str() );
			XCEPT_DECLARE(xmas::broker2g::exception::Exception, e, msg.str());
			this->notifyQualified("warn",e);
		}
	}

	if(msg != 0)
	{
		msg->release();
	}
}

void xmas::broker2g::Application::asynchronousExceptionNotification(xcept::Exception& e)
{
	// only sending to subscription messages - thus subscription 
	LOG4CPLUS_ERROR (this->getApplicationLogger(), "Failed to send allocate response '" << e.message() << "'");
}

// ---------------------------------------------------------

xmas::broker2g::EventingCommunicationErrorHandler::~EventingCommunicationErrorHandler()
{
}

xmas::broker2g::EventingCommunicationErrorHandler::EventingCommunicationErrorHandler(xmas::broker2g::Application* app)
	: application_(app)
{	
}

void xmas::broker2g::EventingCommunicationErrorHandler::asynchronousExceptionNotification(xcept::Exception& e)
{
	LOG4CPLUS_ERROR (application_->getApplicationLogger(), "Failed to send subscription request '" << e.message() << "'");
}

