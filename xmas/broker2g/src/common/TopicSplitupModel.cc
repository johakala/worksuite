/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xmas/broker2g/TopicSplitupModel.h"
#include "xmas/broker2g/Utils.h"
#include <sstream>
#include <set>

#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Vector.h"
#include "xdata/TimeVal.h"

#include "toolbox/string.h"
#include "toolbox/Runtime.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

XMAS_BROKER2G_MODEL_INSTANTIATOR_IMPL(xmas::broker2g::TopicSplitupModel);

xmas::broker2g::TopicSplitupModel::TopicSplitupModel(xdaq::Application * owner, const std::string& name) :
	xmas::broker2g::Model(owner, name)
{
	this->getInfoSpace()->fireItemAvailable("settings",      &settings_);

	requests_ = 0;
}

xmas::broker2g::TopicSplitupModel::~TopicSplitupModel()
{
}

void xmas::broker2g::TopicSplitupModel::loaded() throw(xmas::broker2g::exception::ParserException)
{
	DOMDocument* doc = 0;

	std::vector<std::string> files;
	try 
	{ 
		files = toolbox::getRuntime()->expandPathName(settings_);
	}   
	catch (toolbox::exception::Exception& tbe)
	{
		XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Cannot parse pathname " + (std::string)settings_, tbe);
	}   

	if(files.size() != 1)
	{
		XCEPT_RAISE (xmas::broker2g::exception::ParserException, "Settings filename not unique: " + (std::string)settings_);
	}

	try
	{
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "CollectorModel loading settings from " << files[0]);
		doc = xmas::broker2g::loadDOM(files[0]);
	}
	catch (xmas::broker2g::exception::ParserException& xe)
	{
		XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Failed to load settings from " + (std::string)settings_, xe);
	}

	DOMNodeList* flashlistList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"), xoap::XStr("flashlist"));
	for (XMLSize_t i = 0; i < flashlistList->getLength(); i++)
	{ 
		DOMNode * flashlistNode = flashlistList->item(i);
		std::string flashlistName = xoap::getNodeAttribute (flashlistNode, "name");

		std::vector<xmas::broker2g::SensorSettings*> flashlistSettings;

		DOMNodeList* sensorList = flashlistNode->getChildNodes();
		for (XMLSize_t j = 0 ; j < sensorList->getLength() ; j++)
		{
			DOMNode * sensorNode = sensorList->item(j);
			std::string nodeName = xoap::XMLCh2String(sensorNode->getLocalName());
			if(nodeName == "sensor")
			{
				SensorSettings* settings = new SensorSettings(sensorNode, flashlistName);
				flashlistSettings.push_back(settings);
				LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Loaded sensor settings for flashlist [" << flashlistName << "]");
				LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(),
					"Flashlist [" << settings->getFlashlistName() << "]" <<
					"publish [" << settings->getPublish() << "]");
			}
		}

		if(!flashlistSettings.empty())
		{
				sensorSettings_[flashlistName] = flashlistSettings;
				sensorCounts_[flashlistName] = 0;
		}
	}
}

void xmas::broker2g::TopicSplitupModel::update(xdata::Table::Reference table) throw(xmas::broker2g::exception::ParserException)
{
	// no flashlist information needed
}

xdata::Properties xmas::broker2g::TopicSplitupModel::query(xdata::Properties& plist)
{
	xdata::Properties returnplist;

	// counting of requests and how many times a valid service has been found
	requests_++;

	std::set<std::string> flashlists = toolbox::parseTokenSet(plist.getProperty("urn:xmasbroker2g:flashlists"),",");
	for(std::set<std::string>::iterator iter = flashlists.begin() ; iter != flashlists.end() ; iter++)
	{
		std::string name = (*iter);
		std::string oldtopic = plist.getProperty(name);
		if(oldtopic == "")
		{
			std::map<std::string, std::vector<xmas::broker2g::SensorSettings*> >::iterator found;
			found = sensorSettings_.find(name);
			if(found != sensorSettings_.end())
			{
				// assign new topic if sensor did not have one before
				std::vector<xmas::broker2g::SensorSettings*> settings = (*found).second;
				std::map<std::string, size_t>::iterator citer = sensorCounts_.find(name);
				size_t count = 0;
				if(citer != sensorCounts_.end())
				{
					count = (*citer).second;
				}
				returnplist.setProperty(name, settings[count]->getPublish());
				settings[count]->incrementRequests();
				count = (count + 1) % settings.size();
				sensorCounts_[name] = count;
			}
			else
			{
				// flashlists has not been defined - do not return a topic
				returnplist.setProperty(name, "");
			}
		}
		else
		{
			// reassign old topic
			returnplist.setProperty(name, oldtopic);
		}
	}

	return returnplist;
}

void xmas::broker2g::TopicSplitupModel::view(xgi::Output * out )
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	// Incoming Requests
	// 
	*out << cgicc::tr().set("class", "xdaq-table-nohover");
	*out << cgicc::th();
	*out << "Requests";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");
	*out << requests_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 

	*out << cgicc::br() << std::endl; 

	*out << cgicc::table().set("class", "xdaq-table") << std::endl;

	*out << cgicc::thead();
	*out << cgicc::tr();

	*out << cgicc::th("Name").set("class", "xdaq-sortable");
	*out << cgicc::th("Topic");
	*out << cgicc::th("Requests");
	
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();
	*out << cgicc::tr() << std::endl;

	for(std::map<std::string, std::vector<xmas::broker2g::SensorSettings*> >::iterator iter = sensorSettings_.begin() ; iter != sensorSettings_.end() ; iter++)
	{
		std::stringstream rows;
		rows <<  (*iter).second.size();
                
		*out << cgicc::td((*iter).first).set("rowspan",rows.str()) << std::endl;

		std::vector<xmas::broker2g::SensorSettings*> settings = (*iter).second;
		for (size_t i = 0 ; i < settings.size() ; i++)
		{
			*out << cgicc::td(settings[i]->getPublish());
			std::stringstream ss;
			ss << settings[i]->getRequests();
			*out << cgicc::td(ss.str());
			*out << cgicc::tr();
		}
	}

	*out << cgicc::tbody();	
	*out << cgicc::table();
}

void xmas::broker2g::TopicSplitupModel::cleanup(toolbox::TimeVal timeval)
{
	// nothing to do
}

