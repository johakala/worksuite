/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xmas/broker2g/CollectorModel.h"
#include "xmas/broker2g/CollectorSettings.h"
#include "xmas/broker2g/Utils.h"
#include <sstream>
#include <set>

#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Vector.h"
#include "xdata/TimeVal.h"

#include "toolbox/string.h"
#include "toolbox/stl.h"
#include "toolbox/Runtime.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

XMAS_BROKER2G_MODEL_INSTANTIATOR_IMPL(xmas::broker2g::CollectorModel);

xmas::broker2g::CollectorModel::CollectorModel(xdaq::Application * owner, const std::string& name) :
	xmas::broker2g::Model(owner, name)
{
	this->getInfoSpace()->fireItemAvailable("settings", &settings_);
}

xmas::broker2g::CollectorModel::~CollectorModel()
{
}

void xmas::broker2g::CollectorModel::loaded() throw(xmas::broker2g::exception::ParserException)
{
	DOMDocument* doc = 0;

	std::vector<std::string> files;
	try 
	{ 
		files = toolbox::getRuntime()->expandPathName(settings_);
	}   
	catch (toolbox::exception::Exception& tbe)
	{
		XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Cannot parse pathname " + (std::string)settings_, tbe);
	}   

	if(files.size() != 1)
	{
		XCEPT_RAISE (xmas::broker2g::exception::ParserException, "Settings filename not unique: " + (std::string)settings_);
	}

	try
	{
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "CollectorModel loading settings from " << files[0]);
		doc = xmas::broker2g::loadDOM(files[0]);
	}
	catch (xmas::broker2g::exception::ParserException& xe)
	{
		XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Failed to load settings from " + (std::string)settings_, xe);
	}

	DOMNodeList* flashlistList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"), xoap::XStr("flashlist"));
	for (XMLSize_t i = 0; i < flashlistList->getLength(); i++)
	{ 
		DOMNode * flashlistNode = flashlistList->item(i);
		std::string flashlistName = xoap::getNodeAttribute (flashlistNode, "name");

		DOMNodeList* collectorList = flashlistNode->getChildNodes();
		for (XMLSize_t j = 0 ; j < collectorList->getLength() ; j++)
		{
			DOMNode * collectorNode = collectorList->item(j);
			std::string nodeName = xoap::XMLCh2String(collectorNode->getLocalName());
			if(nodeName == "collector")
			{
				CollectorSettings* settings = new CollectorSettings(collectorNode, flashlistName);
				collectorSettings_[settings->getSubscribe()] = settings;
				LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Loaded collector settings for flashlist [" << flashlistName << "]");
				LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(),
					"Flashlist [" << settings->getFlashlistName() << "]" <<
					"publish [" << settings->getPublish() << "]" <<
					"subscribe [" << settings->getSubscribe() << "]" <<
					"hashkey [" << settings->getHashKey() << "]" <<
					"flush [" << settings->getFlush() << "]" <<
					"clear [" << settings->getClear() << "]");
			}
		}
	}
}


void xmas::broker2g::CollectorModel::update(xdata::Table::Reference table) throw(xmas::broker2g::exception::ParserException)
{
	// ignore
}

xdata::Properties xmas::broker2g::CollectorModel::query(xdata::Properties& plist)
{
	{
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "--- IN  ---");
		;
		for(std::map<std::string, std::string>::iterator piter = plist.begin() ; piter != plist.end() ; piter++)
		{
			LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Property [" << (*piter).first << "=" << (*piter).second << "]");
		}
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "--- IN  ---");
	}

	std::string subscribe = plist.getProperty("urn:xmasbroker2g:subscribe");

	xmas::broker2g::CollectorSettings* found = 0;

	// was collector already assigned?
	std::map<std::string, xmas::broker2g::CollectorSettings*>::iterator iter;
	iter = collectorSettings_.find(plist.getProperty("urn:xmasbroker2g:subscribe"));
	if(iter != collectorSettings_.end())
	{
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Found previously assigned topic in request");

		CollectorSettings* settings = (*iter).second;
		if(settings->isExpired() || (settings->getCollectorAddress() == plist.getProperty("urn:xmasbroker2g:url")) )
		{
			LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Reassigning old topic");
			found = settings;
		}
	}

	if(found == 0)
	{
		// collector was not yet assigned
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Collector was not previously assigned");
		std::map<std::string, xmas::broker2g::CollectorSettings*>::iterator iter;
		for(iter = collectorSettings_.begin() ; iter != collectorSettings_.end() ; iter++)
		{
			CollectorSettings* settings = (*iter).second;
			if(settings->isExpired())
			{
				found = settings;
			}
		}
	}

	xdata::Properties returnplist;
	if(found == 0)
	{
		// nothing found
		return returnplist;
	}

	LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Found not assigned flashlist");
	returnplist.setProperty("urn:xmasbroker2g:flashlist", found->getFlashlistName());
	returnplist.setProperty("urn:xmasbroker2g:publish",   found->getPublish());
	returnplist.setProperty("urn:xmasbroker2g:subscribe", found->getSubscribe());
	returnplist.setProperty("urn:xmasbroker2g:hashkey",   found->getHashKey());
	returnplist.setProperty("urn:xmasbroker2g:flush",     found->getFlush());
	returnplist.setProperty("urn:xmasbroker2g:clear",     found->getClear());

	toolbox::TimeInterval expires;
	expires.fromString(plist.getProperty("urn:xmasbroker2g:expires"));
	found->setExpires(expires);
	found->setCollectorAddress(plist.getProperty("urn:xmasbroker2g:url"));

	{
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "--- OUT ---");
		;
		for(std::map<std::string, std::string>::iterator piter = returnplist.begin() ; piter != returnplist.end() ; piter++)
		{
			LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Property [" << (*piter).first << "=" << (*piter).second << "]");
		}
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "--- OUT ---");
	}

	return returnplist;
}

void xmas::broker2g::CollectorModel::view(xgi::Output * out )
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	size_t unassigned = 0;

	std::map<std::string, xmas::broker2g::CollectorSettings*>::iterator iter;
	for( iter=collectorSettings_.begin() ; iter!=collectorSettings_.end() ; iter++)
	{
		if((*iter).second->isExpired())
		{
			unassigned++;
		}
	}

	// Not assigned collector settings
	// 
	*out << cgicc::tr().set("class", "xdaq-table-nohover");
	*out << cgicc::th();
	*out << "Not assigned collector settings";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");
	*out << unassigned;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 

	*out << cgicc::br() << std::endl;


	// Collector Settings
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;

	*out << cgicc::thead();
	*out << cgicc::tr();

	*out << cgicc::th("Flashlist").set("class", "xdaq-sortable");
	*out << cgicc::th("Publish Topic");
	*out << cgicc::th("Subscribe Topic");
	*out << cgicc::th("Hashkey");
	*out << cgicc::th("Flush");
	*out << cgicc::th("Clear");
	*out << cgicc::th("Expires");
	*out << cgicc::th("Assigned To");

	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	for( iter=collectorSettings_.begin() ; iter!=collectorSettings_.end() ; iter++)
	{
		xmas::broker2g::CollectorSettings* settings = (*iter).second;

		*out << cgicc::tr() << std::endl;
		*out << cgicc::td(settings->getFlashlistName()) << std::endl;
		*out << cgicc::td(settings->getPublish()) << std::endl;
		*out << cgicc::td(settings->getSubscribe()) << std::endl;
		*out << cgicc::td(settings->getHashKey()) << std::endl;
		*out << cgicc::td(settings->getFlush()) << std::endl;
		*out << cgicc::td(settings->getClear()) << std::endl;
		if(settings->isExpired())
		{
			*out << cgicc::td("") << std::endl;
			*out << cgicc::td("").set("style","vertical-align: top;") << std::endl;
		}
		else
		{
			const toolbox::TimeVal& expire = settings->getExpires();
			*out << cgicc::td(expire.toString(toolbox::TimeVal::loc)) << std::endl;
			*out << cgicc::td(settings->getCollectorAddress()) << std::endl;
		}
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();	
	*out << cgicc::table();
}

void xmas::broker2g::CollectorModel::cleanup(toolbox::TimeVal timeval)
{
	// nothing todo
}

