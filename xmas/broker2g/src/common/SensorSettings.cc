/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xmas/broker2g/SensorSettings.h"
#include "xmas/broker2g/Utils.h"
#include <sstream>
#include <set>

#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Vector.h"
#include "xdata/TimeVal.h"

#include "toolbox/string.h"
#include "toolbox/Runtime.h"

xmas::broker2g::SensorSettings::SensorSettings(DOMNode* collectorNode, const std::string& flashlistName)
{
	flashlistName_ = flashlistName;
	publish_   = xoap::getNodeAttribute (collectorNode, "publish");
	requests_ = 0;
}

xmas::broker2g::SensorSettings::~SensorSettings()
{
}

const std::string& xmas::broker2g::SensorSettings::getFlashlistName()
{
	return flashlistName_;
}

const std::string& xmas::broker2g::SensorSettings::getPublish()
{
	return publish_;
}

xdata::UnsignedInteger32T xmas::broker2g::SensorSettings::getRequests()
{
	return requests_;
}

void xmas::broker2g::SensorSettings::incrementRequests()
{
	requests_++;
}

