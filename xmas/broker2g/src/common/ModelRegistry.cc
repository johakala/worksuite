/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R. Moser                                                     *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xmas/broker2g/ModelRegistry.h"
#include "xmas/broker2g/Utils.h"

#include "toolbox/Runtime.h"
#include "toolbox/lang/RTTI.h"

#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"
#include "xoap/MessageFactory.h"
#include "xoap/domutils.h"

#include "xdata/String.h"
#include "xdata/soap/Serializer.h"

xmas::broker2g::ModelRegistry::ModelRegistry(xdaq::Application * owner, toolbox::TimeInterval& interval, const std::string & autoloadpath) throw (xmas::broker2g::exception::ParserException):
	xdaq::Object(owner),
	mutex_(toolbox::BSem::FULL)
{
	std::string filename = autoloadpath + "/*.model";
	DOMDocument* doc = 0;

	std::vector<std::string> files;
	try 
	{ 
		files = toolbox::getRuntime()->expandPathName(filename);
	}   
	catch (toolbox::exception::Exception& tbe)
	{
		XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Cannot parse pathname " + filename, tbe);
	}   
      
	for(size_t i=0;i<files.size();i++)
	{
		try
		{
			LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), "Loading broker model '" << files[i] << "'");
			doc = xmas::broker2g::loadDOM(files[i]);
		}
		catch (xmas::broker2g::exception::ParserException& xe)
		{
			LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), "Failed to load broker model '" << files[i] << "'");
		}

		// Loop over all <Model> tags to be able to initialize all Broker Models
		DOMNodeList* modelList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2008/BrokerModel-10"), xoap::XStr("Model"));
		for (XMLSize_t i = 0; i < modelList->getLength(); i++)
		{ 
			DOMNode * modelNode = modelList->item(i);
			std::string className = xoap::getNodeAttribute (modelNode, "class");
			LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "class [" << className << "]");

			std::string modelName = xoap::getNodeAttribute (modelNode, "name");
			LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "name [" << modelName << "]");

			const xdaq::SharedObjectRegistry* reg = getOwnerApplication()->getApplicationContext()->getSharedObjectRegistry();

			xmas::broker2g::Model* (*instantiator)(xdaq::Application*, std::string);

			std::string modelInstantiatorFunction;
			modelInstantiatorFunction = "_ZN";
			modelInstantiatorFunction += toolbox::lang::scopemangle(className);
			modelInstantiatorFunction += "11instantiateEPN4xdaq11ApplicationERKSs";

			instantiator = (xmas::broker2g::Model* (*)(xdaq::Application*, std::string))reg->lookup ( modelInstantiatorFunction );
			xmas::broker2g::Model *model = instantiator(getOwnerApplication(), modelName);

			try
			{
				getProperties(modelNode, model->getInfoSpace());
				model->loaded();
			}
			catch (xcept::Exception & e)
			{
				delete model;
				XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Properties import failed", e);
			}

			models_[modelName] = model;
		}
	}

	interval_ = interval;
	if( interval != toolbox::TimeInterval(0,0) )
	{
		toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer("broker:CleanupTimer");
		toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
		timer->scheduleAtFixedRate( start, this, interval,  0, "" );
	}
}

xmas::broker2g::ModelRegistry::~ModelRegistry()
{
	std::map<std::string, xmas::broker2g::Model*>::iterator iter = models_.begin();
	for( iter=models_.begin() ; iter != models_.end() ; iter++)
	{
		delete((*iter).second);
	}

	models_.empty();
}


void xmas::broker2g::ModelRegistry::getProperties(DOMNode *modelNode, xdata::InfoSpace *is) throw (xmas::broker2g::exception::ParserException)
{
	DOMNodeList* childList = modelNode->getChildNodes();
	size_t i;

	// Only one child allowed. Return after the first processed child.
	//
	for ( i = 0; i < childList->getLength(); i++)
	{
		DOMNode *newNode = childList->item(i);
		if ( newNode->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			std::string applicationNamespaceURI = "urn:broker:Model";
			std::string receivedNamespaceURI = xoap::XMLCh2String(newNode->getNamespaceURI());

			if ( applicationNamespaceURI != receivedNamespaceURI )
			{
				std::string msg = toolbox::toString("Invalid or missing namespace '%s' in properties section, expected [%s]",receivedNamespaceURI.c_str(), applicationNamespaceURI.c_str());
				XCEPT_RAISE (xmas::broker2g::exception::ParserException, msg);
			}

			xdata::soap::Serializer serializer;
			try
			{
				serializer.import(is, newNode);
			}
			catch (xdata::exception::Exception & xde)
			{
				XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Parameter import failed in ParameterSet", xde);
			}
			return;
		}
	}

	XCEPT_RAISE (xmas::broker2g::exception::ParserException, "Invalid message: No <properties/> element found");
}


void xmas::broker2g::ModelRegistry::update(xdata::Table::Reference table)
{
	LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "Incoming flashlist");
	mutex_.take();

	std::map<std::string, xmas::broker2g::Model*>::iterator iter;
	for( iter=models_.begin() ; iter != models_.end() ; iter++)
	{
		try
		{
			(*iter).second->update(table);
		}
		catch(xmas::broker2g::exception::ParserException& e)
		{
			LOG4CPLUS_WARN(getOwnerApplication()->getApplicationLogger(), "Model [" << (*iter).first << "] could not parse flashlist");
		}
	}

	mutex_.give();
	LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "Incoming flashlist (done)");
}

std::map<std::string, xmas::broker2g::Model*> xmas::broker2g::ModelRegistry::getModels()
{
	return models_;
}

xdata::Properties xmas::broker2g::ModelRegistry::query(xdata::Properties& plist) throw (xmas::broker2g::exception::ModelNotFoundException)
{
	std::string model = plist.getProperty("urn:xmasbroker2g:model");

	mutex_.take();
	std::map<std::string, xmas::broker2g::Model*>::iterator iter = models_.find(model);
	if(iter == models_.end())
	{
		mutex_.give();
		XCEPT_RAISE (xmas::broker2g::exception::ModelNotFoundException, "Invalid model '"+model+"'specified");
	}

	LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "xmas::broker2g::Model::query(model=" << model << ",group=" << plist.getProperty("urn:xmasbroker2g:group") << ",service=" << plist.getProperty("urn:xmasbroker2g:service") << ") called");
	xdata::Properties returnplist = (*iter).second->query(plist);
	mutex_.give();

	return returnplist;
}

void xmas::broker2g::ModelRegistry::view(const std::string& name, xgi::Output * out )
{
	mutex_.take();
	std::map<std::string, xmas::broker2g::Model*>::iterator iter = models_.find(name);
	if(iter == models_.end())
	{
		mutex_.give();
		XCEPT_RAISE (xmas::broker2g::exception::ModelNotFoundException, "Invalid model '"+name+"'specified");
	}

	LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "xmas::broker2g::Model::view(" << name << ") called");
	(*iter).second->view(out);
	mutex_.give();
}

void xmas::broker2g::ModelRegistry::timeExpired (toolbox::task::TimerEvent& e)
{
	LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "xmas::broker2g::ModelRegistry::timeExpired()");
	mutex_.take();

	toolbox::TimeVal time = toolbox::TimeVal::gettimeofday() - interval_;

	std::map<std::string, xmas::broker2g::Model*>::iterator iter;
	for( iter=models_.begin() ; iter != models_.end() ; iter++)
	{
		(*iter).second->cleanup(time);
	}

	mutex_.give();
}

