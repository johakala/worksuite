// $Id: version.cc,v 1.2 2008/07/18 15:28:17 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/broker2g/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xoap/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xgi/version.h"
#include "b2in/utils/version.h"
#include "b2in/nub/version.h"

GETPACKAGEINFO(xmasbroker2g)

void xmasbroker2g::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xdata); 
	CHECKDEPENDENCY(xoap);
	CHECKDEPENDENCY(xdaq); 
	CHECKDEPENDENCY(b2inutils);
	CHECKDEPENDENCY(b2innub);
	CHECKDEPENDENCY(xgi); 
}

std::set<std::string, std::less<std::string> > xmasbroker2g::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config);
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,toolbox); 
	ADDDEPENDENCY(dependencies,xdata); 
	ADDDEPENDENCY(dependencies,xoap);
	ADDDEPENDENCY(dependencies,xdaq);  
	ADDDEPENDENCY(dependencies,b2inutils);  
	ADDDEPENDENCY(dependencies,b2innub);  
	ADDDEPENDENCY(dependencies,xgi);

	return dependencies;
}	
	
