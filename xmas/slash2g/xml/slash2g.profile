<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
	<!-- Compulsory  Plugins -->
	<xp:Application heartbeat="false" class="executive::Application" id="0" group="profile" service="executive" network="local">
		<properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
			<logUrl xsi:type="xsd:string">console</logUrl>
                	<logLevel xsi:type="xsd:string">INFO</logLevel>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>

	<!-- XPlore requires the installation of Power Pack  -->
        <xp:Application heartbeat="false" class="xplore::Application" id="9"  network="local">
                <properties xmlns="urn:xdaq-application:xplore::Application" xsi:type="soapenc:Struct">
                        <settings xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/xplore/shortcuts.xml</settings>
                        <republishInterval xsi:type="xsd:string">60</republishInterval>
                </properties>
        </xp:Application>       
        <xp:Module>/lib/libslp.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxslp.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxploreutils.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxplore.so</xp:Module>

 	<xp:Application heartbeat="false" class="pt::tcp::PeerTransportTCP" id="20" network="local">
                        <properties xmlns="urn:xdaq-application:PeerTransportTCP" xsi:type="soapenc:Struct">
  				<autoSize xsi:type="xsd:boolean">true</autoSize>
                                <maxPacketSize xsi:type="xsd:unsignedInt">0x800000</maxPacketSize>
                                <committedPoolSize xsi:type="xsd:double">100000000</committedPoolSize>
                                <lowThreshold xsi:type="xsd:double">0.5</lowThreshold>
                                <highThreshold xsi:type="xsd:double">0.8</highThreshold>
                                <outputQueueSize xsi:type="xsd:unsignedInt">1024</outputQueueSize>
                        </properties>
         </xp:Application>
          <xp:Module>${XDAQ_ROOT}/lib/libpttcp.so</xp:Module>

        <xp:Endpoint protocol="tcp" service="b2in" subnet="10.176.0.0" port="1910" publish="true" maxport="1950" autoscan="true" network="rnet"/>

 	<xp:Application heartbeat="false" class="pt::appweb::Application" id="1" network="local">
                <properties xmlns="urn:xdaq-application:PeerTransportAPPWEB" xsi:type="soapenc:Struct">
                        <threads xsi:type="xsd:unsignedInt">20</threads>
                        <maxBody xsi:type="xsd:unsignedInt">10000000</maxBody>
                        <documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
                        <aliases xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[1]">
                                <item xsi:type="soapenc:Struct" soapenc:position="[0]">
                                         <name xsi:type="xsd:string">/temporary</name>
                                         <path xsi:type="xsd:string">/tmp</path>
                                </item>
                        </aliases>
                </properties>
        </xp:Application>
        <xp:Module>${XDAQ_ROOT}/lib/libappWeb.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libptappweb.so</xp:Module>

	<xp:Application heartbeat="false" class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>
	
	<!-- HyperDAQ -->
	<xp:Application heartbeat="false" class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>	

	
	<xp:Application heartbeat="true" class="xmas::dfl::Application" id="400" network="rnet" group="xmas" service="xmasdfl" publish="true">
	 	<properties xmlns="urn:xdaq-application:xmas::las2g::Application" xsi:type="soapenc:Struct">
			<subscribeGroup xsi:type="xsd:string">xmas</subscribeGroup>
  			<!-- Load only flashlists that the slash for this domain should see, e.g. las-pool1 -->
            		<settings xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/sensor/slash2g.settings</settings>

			<!-- scan for eventings every 10 seconds -->
			<scanPeriod xsi:type="xsd:string">PT30S</scanPeriod>
			<!-- subscription expiration times out after 30 seconds without renewal -->
			<subscribeExpiration xsi:type="xsd:string">PT60S</subscribeExpiration>
		</properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libxmasdfl.so</xp:Module>

	<!-- XMem -->
	<xp:Application class="xmem::Application" id="6"  network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libxmem.so</xp:Module>
</xp:Profile>
