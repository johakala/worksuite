// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 ************************************************************************/
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "xoap/DOMParser.h"
#include "xoap/DOMParserFactory.h"

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/slash2g/Application.h"
#include "xmas/slash2g/exception/Exception.h"

#include "xplore/DiscoveryEvent.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xdata/InfoSpaceFactory.h"
#include "xplore/Interface.h"
#include "xplore/exception/Exception.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"

#include "xdata/Table.h"
#include "xdata/TableAlgorithms.h"
#include "xdata/exdr/Serializer.h"
#include "xdaq/ApplicationDescriptorImpl.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/utils.h"
#include "toolbox/regex.h"
#include "toolbox/BSem.h"
#include "toolbox/exception/Handler.h"

#include "b2in/nub/Method.h"
#include "b2in/nub/Messenger.h"
#include "b2in/utils/exception/Exception.h"

#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

// Required for Namespace declaration used in configuration file
#include "xmas/xmas.h"
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream

#include "jansson.h"


XDAQ_INSTANTIATOR_IMPL(xmas::slash2g::Application);

xmas::slash2g::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
	: xdaq::Application(s), xgi::framework::UIManager(this), repositoryLock_(toolbox::BSem::FULL)
{	
	b2in::nub::bind(this, &xmas::slash2g::Application::onMessage );

	s->getDescriptor()->setAttribute("icon", "/xmas/slash2g/images/Las2g_64x64.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/slash2g/images/Las2g_16x16.ico");

	reportLostCounter_ = 0;
	lastRetrieveTime_ = toolbox::TimeVal::gettimeofday();
	minDeltaBetweenRetrieve_ = 0.2; // By default allow 20 retrieveCollection requests per second

	// direct evetings addresses
	this->getApplicationInfoSpace()->fireItemAvailable("eventings", &eventings_);

	
	// In which group to search for a ws-eventing
	subscribeGroup_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeGroup", &subscribeGroup_);

	// Which flashlist tags to retrieve from the b2in-eventing
	settings_ = "";
        this->getApplicationInfoSpace()->fireItemAvailable("settings", &settings_);
	
	scanPeriod_ = "PT10S";
	this->getApplicationInfoSpace()->fireItemAvailable("scanPeriod",&scanPeriod_);
	subscribeExpiration_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeExpiration",&subscribeExpiration_);

	// bind HTTP callback
	xgi::bind(this, &xmas::slash2g::Application::retrieveCatalog, "retrieveCatalog");
	xgi::bind(this, &xmas::slash2g::Application::retrieveStaticCatalog, "retrieveStaticCatalog");
	xgi::bind(this, &xmas::slash2g::Application::retrieveCollection, "retrieveCollection");

	xgi::framework::deferredbind(this, this, &xmas::slash2g::Application::retrieveCatalog, "retrieveCatalogHTML");
	xgi::framework::deferredbind(this, this, &xmas::slash2g::Application::retrieveCollection, "retrieveCollectionHTML");
	xgi::framework::deferredbind(this, this, &xmas::slash2g::Application::Default, "Default");
	
	xgi::framework::deferredbind(this, this, &xmas::slash2g::Application::uploadFlashlist, "uploadFlashlist");
	xgi::framework::deferredbind(this, this, &xmas::slash2g::Application::uploadSettings, "uploadSettings");
	xgi::framework::deferredbind(this, this, &xmas::slash2g::Application::subscribeFlashlist, "subscribeFlashlist");


	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationContext()->addActionListener(this); // attach to endpoint available events
}

xmas::slash2g::Application::~Application()
{

}
void xmas::slash2g::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Timer callback");

	//std::cout << "timer expired " << e.type()  << std::endl;
	if (e.getTimerTask()->name  == "urn:xmas:slash2g-check" )
	{
		//std::cout << "going to process statistics" << std::endl;

		repositoryLock_.take();

		for ( std::map<std::string, FlashlistData*>::iterator i = repository_.begin(); i != repository_.end(); ++i )
		{
			(*i).second->processStatistics();

		}
		repositoryLock_.give();
		return;
	}
	

	if (eventings_.elements() == 0)
	{
		// use of discovery

		try
		{
			b2inEventingProxy_->scan();
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

	}

	try
	{
		this->refreshSubscriptionsToEventing();
	}
	catch (xmas::exception::Exception& e)
	{
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
	
	
}

void xmas::slash2g::Application::asynchronousExceptionNotification(xcept::Exception& e)
{

	std::stringstream msg; 
	msg << "Failed to subscribe to b2in eventing";
	XCEPT_DECLARE_NESTED(xmas::slash2g::exception::Exception, ex, msg.str(),e);
	this->notifyQualified("error",ex);

}


void xmas::slash2g::Application::actionPerformed( xdata::Event& event) 
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{	

		 // Read the configuration file
                this->applySettings( settings_.toString() );
        try
        {

        	b2inEventingProxy_ = new b2in::utils::ServiceProxy(this,"b2in-eventing",subscribeGroup_.toString(),this);
        }
        catch (b2in::utils::exception::Exception & e)
        {
        	std::stringstream msg;
        	msg << "Failed to create service proxy to b2in-eventing";
        	XCEPT_DECLARE_NESTED(xmas::slash2g::exception::Exception, ex, msg.str(),e);
        	LOG4CPLUS_ERROR (this->getApplicationLogger(), xcept::stdformat_exception_history(ex));
        }
		toolbox::task::Timer * timer = 0;
		// Create timer for refreshing subscriptions
		if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:slash2g-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:slash2g-timer");
			
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:slash2g-timer");
		}
		
		// submit scan task
		toolbox::TimeInterval interval;
		interval.fromString(scanPeriod_);
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		timer->scheduleAtFixedRate( start, this, interval, 0, "urn:xmas:slash2g-task" );

		interval.fromString("PT1S");
		timer->scheduleAtFixedRate( start, this,interval , 0, "urn:xmas:slash2g-check" );


		
/*
		// Load collector senttings for keys
		//
		LOG4CPLUS_INFO (this->getApplicationLogger(), "Load keys from '" << brokerCollectorSettings_.toString() << "'");
        	DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML( brokerCollectorSettings_.toString() );
	
        	DOMNodeList* flashlistList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"), xoap::XStr("flashlist"));
        	for (XMLSize_t i = 0; i < flashlistList->getLength(); i++)
        	{
                	DOMNode * flashlistNode = flashlistList->item(i);
                	std::string flashlistName = xoap::getNodeAttribute (flashlistNode, "name");
	
                	DOMNodeList* collectorList = flashlistNode->getChildNodes();
                	for (XMLSize_t j = 0 ; j < collectorList->getLength() ; j++)
                	{
                        	DOMNode * collectorNode = collectorList->item(j);
                        	std::string nodeName = xoap::XMLCh2String(collectorNode->getLocalName());
                        	if(nodeName == "collector")
                        	{
					std::string haskKey = xoap::getNodeAttribute (collectorNode, "hashkey");
                                	hashKeys_[flashlistName] = toolbox::parseTokenSet(haskKey,",");
                                	LOG4CPLUS_DEBUG( this->getApplicationLogger(), "Loaded haskey:[" << haskKey << "] for flashlist:" << flashlistName );
                        	}
                	}
        	}
*/
	
		//
		//
	}
	else
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}


void xmas::slash2g::Application::actionPerformed(toolbox::Event& e) 
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " << e.type());
	if ( e.type() == "urn:xdaq-event:profile-loaded")
	{
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Setting proxy to b2in-eventing services " );

		if (eventings_.elements() != 0)
				{
					for (xdata::Vector<xdata::String>::iterator i = eventings_.begin(); i != eventings_.end(); i++)
					{
						 try
						 {
							LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Adding eventing on " << (*i).toString() );

							 b2inEventingProxy_->addURL(*i);
						 }
						 catch(b2in::utils::exception::Exception & e)
						 {
							 std::stringstream msg;
							 msg << "Failed to add b2in-eventing address";
							 XCEPT_DECLARE_NESTED(xmas::slash2g::exception::Exception, ex, msg.str(),e);
							LOG4CPLUS_ERROR (this->getApplicationLogger(), xcept::stdformat_exception_history(ex));
						 }
					}
				}
	}
	/*
	if ( e.type() == "xdaq::EndpointAvailableEvent" )
	{
		// If the available endpoint is a b2in endpoint we are ready to
		// discover other b2in endpoints on the network
		//
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
		
        	xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
        	// xdaq::Endpoint* endpoint = ie.getEndpoint();
        	xdaq::Network* network = ie.getNetwork();
		
		LOG4CPLUS_INFO (this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());
		
		if (network->getName() == networkName)
		{		
			try
			{
				LOG4CPLUS_INFO (this->getApplicationLogger(), "Discover b2in endpoints on network " << networkName);
				this->discoverEndpoints();
				this->refreshSubscriptionsToEventing();
			}
			catch (xdaq::exception::Exception& e)
			{
				LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			}
		}
	}
	*/
}

void xmas::slash2g::Application::onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) 
	throw (b2in::nub::exception::Exception)
{

	std::string protocol =  plist.getProperty("urn:b2in-eventing:protocol");
	if ( protocol == "xmas" )
	{
		std::string action =  plist.getProperty("urn:xmas-protocol:action");
		if ( action  == "reset" || action == "clear" )
		{
			repositoryLock_.take();
			for ( std::map<std::string, xmas::slash2g::FlashlistData*>::iterator fl = repository_.begin(); fl != repository_.end(); ++fl)
			{
				((*fl).second)->reset();
			}
			repository_.clear();

			repositoryLock_.give();

		}
		else
		{
			if (msg != 0 ) msg->release();
			std::stringstream msg;
			msg << "Invalid xmas protocol action  " <<  action;
			XCEPT_RAISE (b2in::nub::exception::Exception, msg.str());
		} 
		if (msg != 0 ) msg->release();
		return;

	}

	std::string flashlistName = plist.getProperty("urn:xmas-flashlist:name");
	std::string topicName = plist.getProperty("urn:b2in-eventing:topic");
	if (flashlistName == "")
	{
		XCEPT_RAISE (b2in::nub::exception::Exception, "Incoming B2IN message corrupted, 'urn:xmas-flashlist:name' property is empty");
	}
	else if ( msg != 0 )
	{
		/* LAS does not de-serialize
		xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) msg->getDataLocation(), msg->getDataSize());		
		xdata::Table * t = new xdata::Table();
		xdata::Table::Reference  table (t);
	

		try
		{			
			serializer_.import(t, &inBuffer);			
			
		}
		catch (xdata::exception::Exception& e)
		{
			delete t;
			++reportLostCounter_;
			msg->release();
			XCEPT_RETHROW (b2in::nub::exception::Exception, "Failed to deserialize flashlist table", e);
		}
		// done with the buffer
		msg->release();
		*/
		
		std::map<std::string, FlashlistData*>::iterator fl;
		xmas::slash2g::FlashlistData* dataEntry = 0;
		repositoryLock_.take();
		
		fl = repository_.find(flashlistName);
		if (fl != repository_.end())
		{
			dataEntry = (*fl).second;
		}
		else
		{
			std::map<std::string, xmas::slash2g::Settings*>::iterator si = flashSettings_.find(topicName);
			if (si != flashSettings_.end())
			{
				xmas::slash2g::Settings* flashSettings = (*si).second;
				//std::cout << "Merge according to keys: " <<       flashSettings->getProperty("hashkey") << std::endl;
				std::set<std::string> hashkey = toolbox::parseTokenSet( flashSettings->getProperty("hashkey"),",");
				dataEntry = new xmas::slash2g::FlashlistData(hashkey);
				repository_[flashlistName] = dataEntry;
			}
			else
			{
				std::stringstream  mesg;
				mesg << "Cannot merge data into slash2g, missing settings for flashlist '" << flashlistName << "'";
				XCEPT_DECLARE(xmas::slash2g::exception::Exception, q, mesg.str());
				this->notifyQualified("error",q);
				repositoryLock_.give();
				msg->release();
				return;
			}
		}
		repositoryLock_.give();
		
		try
		{
			
			dataEntry->setData(msg, plist);
		}
		catch (xmas::slash2g::exception::Exception & e)
		{
			msg->release();
			LOG4CPLUS_ERROR (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}
	}
	else
	{
		XCEPT_RAISE (b2in::nub::exception::Exception, "Empty data for flashlist " + flashlistName);
	}
	
	// simple perfromance meauserment
	static size_t counter = 0;
	static toolbox::TimeVal start_ = toolbox::TimeVal::gettimeofday();

	if ((counter % 10000) == 9999)
	{
			toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
			std::cout << "Time: " << (double)(stop - start_) << ", rate: " << 10000/((double)(stop - start_)) << std::endl;
			start_ = toolbox::TimeVal::gettimeofday();
	}
	counter++;
		

}
void xmas::slash2g::Application::refreshSubscriptionsToEventing() 
	throw (xmas::exception::Exception)
{
	if (subscriptions_.empty())
	{
		this->prepareSubscriptionRecords();
	}
	
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "subscribe/Renew");
	b2in::utils::MessengerCache * messengerCache = 0;
	try
	{
		messengerCache = b2inEventingProxy_->getMessengerCache();
	}
	catch(b2in::utils::exception::Exception & e)
	{
			XCEPT_RETHROW (xmas::exception::Exception, "cannot access messenger cache", e);
	}
	
	// This lock is available all the time excepted when an asynchronous send will fail and a cache invalidate is performed
	// Therefore there is no loss of efficiency.
	//
	
	std::list<std::string> destinations = messengerCache->getDestinations();	
	
	for (std::list<std::string>::iterator j = destinations.begin(); j != destinations.end(); j++ )
	{
		//
		// Subscribe to OR Renew all existing subscriptions
		//
		std::map<std::string, xdata::Properties>::iterator i;
		for ( i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
		{
			try
			{
				// plist is already prepared for a subscribe/resubscribe message
				//
				LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Subscribe to topic " << (*i).first << " on url " << (*j));
				messengerCache->send((*j),0,(*i).second);
			}
			catch (b2in::nub::exception::InternalError & e)
                        {
				std::stringstream msg;
				msg << "failed to send subscription for topic (internal error) " << (*i).first << " to url " << (*j);

				LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());

				XCEPT_DECLARE_NESTED(xmas::slash2g::exception::Exception, ex , msg.str(), e);
				this->notifyQualified("fatal",ex);
				return;
                        }
                        catch ( b2in::nub::exception::QueueFull & e )
                        {                                
				std::stringstream msg;
				msg << "failed to send subscription for topic (queue full) " << (*i).first << " to url " << (*j);

				LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
                                
				XCEPT_DECLARE_NESTED(xmas::slash2g::exception::Exception, ex , msg.str(), e);
				this->notifyQualified("error",ex);
				return;
                        }
                        catch ( b2in::nub::exception::OverThreshold & e)
                        {
				// ignore just count to avoid verbosity                                
				std::stringstream msg;
				msg << "failed to send subscription for topic (over threshold) " << (*i).first << " to url " << (*j);
				LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
				return; 
                        }			
		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "done with subscription");
	}
}

//
// XGI SUpport
//
void xmas::slash2g::Application::retrieveCatalog(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	this->retrieveCatalogHTML(in, out); // Out of framework version
}

void xmas::slash2g::Application::retrieveCatalogHTML(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	try
	{
		cgicc::Cgicc cgi(in);

		std::string format = xgi::Utils::getFormElement(cgi, "fmt")->getValue();
		if (format == "plain")
		{
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
		}
		else if (format == "csv")
		{
			std::string disposition = "attachment; filename=catalogue.csv;";
			out->getHTTPResponseHeader().addHeader("Content-Type", "application/csv");
			out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
		}
		else if (format == "html")
		{
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		}
		else if (format == "xml")
		{
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/xml");
		}
		else if (format == "json")
		{
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
		}
		else if (format == "exdr")
		{
			std::string disposition = "attachment; filename=catalogue.exdr;";
			out->getHTTPResponseHeader().addHeader("Content-Type", "application/x-xdata+exdr");
			out->getHTTPResponseHeader().addHeader("Content-transfer-encoding", "binary");
			out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
		}
		else
		{
			std::stringstream msg;
			msg << "Unknown requested format '" <<  format << "'";
			(void) out->getHTTPResponseHeader().getStatusCode(415);
			(void) out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(415));
			(void) out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
			*out << xgi::Utils::getFailurePage(xgi::Utils::getResponsePhrase(415),msg.str());
			return;
		}

		if (format == "html")
		{
			this->displayFlashListCatalogToHTML(out);
		}
		else if (format == "xml")
		{
			this->displayFlashListCatalogToXML(out);
		}
		else if (format == "json")
		{
			this->displayFlashListCatalogToJSON(out);
		}
		else if ((format == "plain") || (format == "csv"))
		{
			this->displayFlashListCatalogToCSV(out);
		}
		else if (format == "exdr")
		{
			this->displayFlashListCatalogToEXDR(out);
		}
	}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
}

void xmas::slash2g::Application::retrieveStaticCatalog( xgi::Input* in, xgi::Output* out ) throw (xgi::exception::Exception)
{
	try
	{
		cgicc::Cgicc cgi(in);

		std::string format = xgi::Utils::getFormElement(cgi, "fmt")->getValue();
		if (format == "plain")
		{
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
		}
		else if (format == "xml")
		{
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/xml");
		}
		else if (format == "csv")
		{
			std::string disposition = "attachment; filename=catalogue.csv;";
			out->getHTTPResponseHeader().addHeader("Content-Type", "application/csv");
			out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
		}
		else if (format == "html")
		{
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		}
		else if (format == "json")
		{
			out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
		}
		else if (format == "exdr")
		{
			std::string disposition = "attachment; filename=catalogue.exdr;";
			out->getHTTPResponseHeader().addHeader("Content-Type", "application/x-xdata+exdr");
			out->getHTTPResponseHeader().addHeader("Content-transfer-encoding", "binary");
			out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
		}
		else
		{
			std::stringstream msg;
			msg << "Unknown requested format '" << format << "'";
			(void) out->getHTTPResponseHeader().getStatusCode(415);
			(void) out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(415));
			(void) out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
			*out << xgi::Utils::getFailurePage(xgi::Utils::getResponsePhrase(415), msg.str());
			return;
		}

		if (format == "html")
		{
			this->displayStaticCatalogInHTML(out);
		}
		else if (format == "xml")
		{
			this->displayStaticCatalogInXML(out);
		}
		else if (format == "json")
		{
			this->displayStaticCatalogInJSON(out);
		}
		else if ((format == "csv") || (format == "plain"))
		{
			this->displayStaticCatalogInCSV(out);
		}
		else if (format == "exdr")
		{
			this->displayStaticCatalogInEXDR(out);
		}
	}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
}

void xmas::slash2g::Application::displayFlashListCatalogToHTML( xgi::Output * out ) throw (xmas::exception::Exception)
{
	*out << cgicc::table().set("class", "xdaq-table");
	/*
			.set("cellpadding","0")
			.set("cellspacing","0")
			.set("border","")
			.set("style","font-size: 8pt; font-family: arial; text-align: left; width: 100%; background-color: rgb(255,255,255);");
	*/
	
	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Name"); //.set("title","FlashList name").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::tr(); // << std::endl;
	*out << cgicc::thead();
	
	*out << cgicc::tbody();
	
	repositoryLock_.take();
	
	for ( std::map<std::string, FlashlistData*>::iterator i = repository_.begin(); i != repository_.end(); ++i )
	{
		if ( (*i).second->hasData() )
		{
			*out << cgicc::tr();
			*out << cgicc::td( (*i).first ); // .set("style","vertical-align: top; font-weight: normal; background-color: rgb(255,255,255);") << std::endl;
			*out << cgicc::tr(); // << std::endl;
		}
	}
	
	repositoryLock_.give();

	
	*out << cgicc::tbody();	
	*out << cgicc::table();
}

void xmas::slash2g::Application::displayFlashListCatalogToJSON( xgi::Output * out ) throw (xmas::exception::Exception)
{
	*out << "{\"table\":{";
	// definition
	*out << "\"definition\":[{\"key\":\"Name\", \"type\":\"string\"},";
	*out << "{\"key\":\"LastUpdate\", \"type\":\"time\"},";
	*out << "{\"key\":\"Version\", \"type\":\"string\"},";
	*out << "{\"key\":\"LastOriginator\", \"type\":\"string\"},";
	*out << "{\"key\":\"Tag\", \"type\":\"string\"},";
	*out << "{\"key\":\"Rows\", \"type\":\"unsigned int\"}";
	*out << "],";
	// rows
	*out << "\"rows\":[";
	
	repositoryLock_.take();

	std::map<std::string, FlashlistData*>::iterator i = repository_.begin();
	while ( i != repository_.end() )
	{
		xmas::slash2g::FlashlistData* data = (*i).second;
		
		data->lock();
		if ( data->hasData() )
		{
			*out << "{\"Name\":\"" << (*i).first << "\", \"LastUpdate\":\"" << data->getLastUpdate().toString("%a, %b %d %Y %H:%M:%S GMT", toolbox::TimeVal::gmt) << "\"";
			*out << ", \"Version\":\"" << data->getVersion() << "\"";
			*out << ", \"Tag\":\"" << data->getTags() << "\"";
			*out << ", \"LastOriginator\":\"" << data->getLastOriginator() << "\"";
			*out << ", \"Size\":" << data->getSize();
			*out << "}";
		}
		data->unlock();
		++i;
		if ( i != repository_.end() )
			*out << ",";
	}
	
	repositoryLock_.give();

	
	*out << "]}";	
	*out << "}";
}


void xmas::slash2g::Application::displayFlashListCatalogToXML( xgi::Output * out ) throw (xmas::exception::Exception)
{
	*out << "<?xml version=\"1.0\"?><table>";
	
	repositoryLock_.take();

	for ( std::map<std::string, FlashlistData*>::iterator i = repository_.begin(); i != repository_.end(); ++i )
	{
		if ( (*i).second->hasData() )
		{
			*out << "<tr><td>" << (*i).first << "</td></tr>";
		}
	}
	
	repositoryLock_.give();

	
	*out << "</table>";
}

void xmas::slash2g::Application::displayFlashListCatalogToCSV( xgi::Output * out ) throw (xmas::exception::Exception)
{
	*out << "Name" << std::endl;	
	repositoryLock_.take();
	
	for ( std::map<std::string, FlashlistData*>::iterator i = repository_.begin(); i != repository_.end(); ++i )
	{
		if ( (*i).second->hasData() )
		{		
			*out << (*i).first << std::endl;
		}
	}
	
	repositoryLock_.give();

}

void xmas::slash2g::Application::displayFlashListCatalogToEXDR( xgi::Output * out ) throw (xmas::exception::Exception)
{
	xdata::Table t;
	t.addColumn("Name", "string");

	repositoryLock_.take();
	for (std::map<std::string, FlashlistData*>::iterator i = repository_.begin(); i != repository_.end(); i++)
	{
		if ( i->second->hasData() )
		{
			//Append row
			xdata::Table::iterator rowIt = t.append();

			xdata::String name(i->first);
			rowIt->setField("Name", name);
		}
	}
	repositoryLock_.give();

	xdata::exdr::Serializer serializer;
	xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
	try
	{
		serializer.exportAll(&t, &outBuffer);
	}
	catch (xdata::exception::Exception& e)
	{
		XCEPT_RETHROW (xmas::exception::Exception, "Failed to serialize in exdr format", e);
	}
	out->write(outBuffer.getBuffer(), outBuffer.tellp());
}

void xmas::slash2g::Application::displayStaticCatalogInHTML( xgi::Output * out ) throw (xmas::exception::Exception)
{
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Name") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	for (std::map<std::string, xmas::slash2g::Settings*>::iterator i = flashSettings_.begin(); i != flashSettings_.end(); ++i)
	{
		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*i).first) << std::endl;
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

void xmas::slash2g::Application::displayStaticCatalogInXML( xgi::Output * out ) throw (xmas::exception::Exception)
{
	*out << "<?xml version=\"1.0\"?><table>";
	for (std::map<std::string, xmas::slash2g::Settings*>::iterator i = flashSettings_.begin(); i != flashSettings_.end(); ++i)
	{
		*out << "<tr><td>" << i->first << "</td></tr>";
	}
	*out << "</table>";
}

void xmas::slash2g::Application::displayStaticCatalogInJSON( xgi::Output * out ) throw (xmas::exception::Exception)
{
	json_t* document = json_object();

	//table
	json_t* table = json_object();
	json_object_set_new(document, "table", table);

	//definition
	json_t* definition = json_array();
	json_object_set_new(table, "definition", definition);

	//definition items
	json_t* definitionitem;

	definitionitem = json_object();
	json_object_set_new(definitionitem, "key", json_string("Name"));
	json_object_set_new(definitionitem, "type", json_string("string"));
	json_array_append_new(definition, definitionitem);

	definitionitem = json_object();
	json_object_set_new(definitionitem, "key", json_string("Hashkey"));
	json_object_set_new(definitionitem, "type", json_string("string"));
	json_array_append_new(definition, definitionitem);

	//rows
	json_t* rows = json_array();
	json_object_set_new(table, "rows", rows);

	//row
	for (std::map<std::string, xmas::slash2g::Settings*>::iterator i = flashSettings_.begin(); i != flashSettings_.end(); ++i)
	{
		json_t* row= json_object();
		json_object_set_new(row, "Name", json_string(i->first.c_str()));
		json_object_set_new(row, "Hashkey", json_string(i->second->getProperty("hashkey").c_str()));
		json_array_append_new(rows, row);
	}

	char* dump = json_dumps(document, 0);
	*out << dump;
	free(dump);
	json_decref(document);
}

void xmas::slash2g::Application::displayStaticCatalogInCSV( xgi::Output * out ) throw (xmas::exception::Exception)
{
	*out << "Name" << std::endl;
	for (std::map<std::string, xmas::slash2g::Settings*>::iterator i = flashSettings_.begin(); i != flashSettings_.end(); ++i)
	{
		*out << i->first << std::endl;
	}
}

void xmas::slash2g::Application::displayStaticCatalogInEXDR( xgi::Output * out ) throw (xmas::exception::Exception)
{
	xdata::Table t;
	t.addColumn("Name", "string");
	t.addColumn("Hashkey", "string");
	for (std::map<std::string, xmas::slash2g::Settings*>::iterator i = flashSettings_.begin(); i != flashSettings_.end(); ++i)
	{
		//Append row
		xdata::Table::iterator rowIt = t.append();

		xdata::String name(i->first);
		rowIt->setField("Name", name);

		xdata::String hashkey(i->second->getProperty("hashkey"));
		rowIt->setField("Hashkey", hashkey);
	}

	xdata::exdr::Serializer serializer;
	xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
	try
	{
		serializer.exportAll(&t, &outBuffer);
	}
	catch (xdata::exception::Exception& e)
	{
		XCEPT_RETHROW (xmas::exception::Exception, "Failed to serialize in exdr format", e);
	}
	out->write(outBuffer.getBuffer(), outBuffer.tellp());
}

void xmas::slash2g::Application::retrieveCollection(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	this->retrieveCollectionHTML(in, out); // This should do the same thing but without the framework binding
}
void xmas::slash2g::Application::retrieveCollectionHTML(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	std::string name = "";
	std::string format = "";
	std::map<std::string,std::string> filter;
	try
	{
		cgicc::Cgicc cgi(in);
		
		// May raise an xgi::exception::Exception that will be returned automatically
		//
		cgicc::const_form_iterator i = cgi.getElement("delay");
		
		// Delay the request to avoid overload of the las (delay in msec)
		//
		unsigned long delay;
		if (i != cgi.getElements().end())
		{
			delay = 1000 * ((*i).getIntegerValue());
		}
		else
		{
			delay = 100000;
		}
		
		toolbox::u_sleep(delay);
		
		
		const std::vector<cgicc::FormEntry>& elements = cgi.getElements();
		for (size_t ei = 0; ei < elements.size(); ++ei)
		{
			if ( elements[ei].getName() == "flash" )
			{
				name = elements[ei].getValue();
			}
			else if ( elements[ei].getName() == "fmt" )
			{
				format = elements[ei].getValue();
			}
			else if ( elements[ei].getName() == "delay" )
			{
				// ignore this parameter
			}
			else
			{
				filter[ elements[ei].getName() ] = elements[ei].getValue();
			}
		}
	}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
	
	if (format == "plain")
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
		
	}
	else if (format == "csv")
	{
		std::string disposition = toolbox::toString("attachment; filename=%s.csv;", name.c_str());
		out->getHTTPResponseHeader().addHeader("Content-Type", "application/csv");
		out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
		
	}
	else if (format == "html")
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		
	}
	else if (format == "exdr")
	{
		std::string disposition = toolbox::toString("attachment; filename=%s.exdr;", name.c_str());
		out->getHTTPResponseHeader().addHeader("Content-Type", "application/x-xdata+exdr");
		out->getHTTPResponseHeader().addHeader("Content-transfer-encoding", "binary");
		out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
		
	}
	else if (format == "json")
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");	
	}
	
	else
	{
		std::stringstream msg;
		msg << "Unknown requested format '" <<  format << "'";
		(void) out->getHTTPResponseHeader().getStatusCode(415);
		(void) out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(415));
		(void) out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		*out << xgi::Utils::getFailurePage(xgi::Utils::getResponsePhrase(415),msg.str());
		return;
	}
	
	out->getHTTPResponseHeader().addHeader("Expires", "0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "post-check=0, pre-check=0");
	out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");
	
	// Monitoring reports are indexed by flashlist names
	// Each report has properties - if no filter on the properties
	// is specified, a merged data set is delivered
	//
	xmas::slash2g::FlashlistData* dataEntry = 0;
	
	repositoryLock_.take();
	
	std::map<std::string, FlashlistData*>::iterator r = repository_.find(name);
	if (r != repository_.end())
	{
		dataEntry = (*r).second;
		
	}
	
	repositoryLock_.give();
	
	
	if (dataEntry == 0)
	{
		std::stringstream msg;
		msg << "Failed to find flashlist '" <<  name << "' in 'retrieveCollection' command";
		(void) out->getHTTPResponseHeader().getStatusCode(500);
		(void) out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(500));
		(void) out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		*out << xgi::Utils::getFailurePage(xgi::Utils::getResponsePhrase(500), msg.str());
		return;
	}
	
	try 
	{
		if ( format == "html" )
		{
			*out << "<div style=\"color: blue\">Flashlist: " << name << "</div><br />";
			dataEntry->toHTML(*out, filter);
		}
		else if ( format == "csv"  || format == "plain"  )
		{
			dataEntry->toCSV(*out, filter);
		}
		else if ( format == "json"  )
		{
			dataEntry->toJSON(*out, filter);
		}
		else if ( format == "exdr" )
		{
			dataEntry->toEXDR(*out, filter);
		}
	}
	catch(xmas::slash2g::exception::Exception & me)
	{
		std::stringstream msg;
		msg << "Failed to retrieve flashlist '" <<  name << "' in 'retrieveCollection' command";
		XCEPT_DECLARE_NESTED (xmas::exception::Exception, exception , msg.str(), me);
		
		(void) out->getHTTPResponseHeader().getStatusCode(500);
		(void) out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(500));
		(void) out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		*out << xgi::Utils::getFailurePage(xgi::Utils::getResponsePhrase(500), xcept::htmlformat_exception_history(exception));
		return;
	}
	
}
void xmas::slash2g::Application::checkXMLParsing(const std::string & data)  throw (xmas::exception::Exception)
{
	DOMDocument* doc = 0;
	try
	{
		xoap::DOMParser* p = xoap::getDOMParserFactory()->get("configure");
		doc = p->parse(data);
		doc->release();
		return;
	}
	catch (xoap::exception::Exception& e)
	{
		XCEPT_RETHROW(xmas::exception::Exception, "Invalid XML document", e);
	}
}

void xmas::slash2g::Application::writeToFile(const std::string & filename, const std::string & data, bool override)   throw (xmas::exception::Exception)
{
	std::ifstream ifile(filename.c_str());

	if (ifile.is_open())
	{
	     if ( ! override )
	     {
	    	 XCEPT_RAISE(xmas::exception::Exception, "cannot create file, already exists ");
	     }
	}

	std::ofstream ofile ( filename.c_str(), std::ios::trunc );
	 if (! ofile.is_open())
     {
		 XCEPT_RAISE(xmas::exception::Exception, "failed to create file ");
	 }
	 ofile << data;
	 ofile.close();
}

void xmas::slash2g::Application::uploadFlashlist(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::cout << "uploadFlashlist: "   << std::endl;

	std::stringstream pathname;
	pathname << ::getenv("XDAQ_SETUP_ROOT") << "/" << ::getenv("XDAQ_ZONE") << "/flash/";

	/*try
	{*/
		cgicc::Cgicc cgi(in);

		bool override = cgi.queryCheckbox("override");

		cgicc::const_file_iterator file;
		file = cgi.getFile("FlashlistDefinitionFile");

		if (file != cgi.getFiles().end())
		{


			std::string data = (*file).getData();

			try
			{
				this->checkXMLParsing(data);
			}
			catch (xmas::exception::Exception& e)
			{
				XCEPT_RETHROW(xgi::exception::Exception, "Invalid flashlist definition", e);
			}

			std::string filename = pathname.str() + (*file).getFilename();

			std::cout << "Name: " << (*file).getName()  << std::endl;
			std::cout << "Filetype: " << (*file).getDataType()  << std::endl;
			std::cout << "Filename: " << (*file).getFilename()  << std::endl;
			std::cout << "Size: " << (*file).getDataLength() << std::endl;
			std::cout << "Full path: " << filename << std::endl;

			try
			{
				this->writeToFile(filename,data,override);
			}
			catch (xmas::exception::Exception & e)
			{
				XCEPT_RETHROW(xgi::exception::Exception, "Cannot save to file", e);
			}

		}

	/*}
	catch (const std::exception& e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());

	}*/
	this->Default(in,out);

}


void xmas::slash2g::Application::uploadSettings(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::cout << "uploadSettings: "   << std::endl;
	std::stringstream pathname;
	pathname << ::getenv("XDAQ_SETUP_ROOT") << "/" << ::getenv("XDAQ_ZONE") << "/sensor/";
	/*try
	{*/
		cgicc::Cgicc cgi(in);

		bool override = cgi.queryCheckbox("override");

		cgicc::file_iterator file;
		file = cgi.getFile("SensorSettingsFile");

		if (file != cgi.getFiles().end())
		{
			std::string data = (*file).getData();

			try
			{
				this->checkXMLParsing(data);
			}
			catch (xmas::exception::Exception& e)
			{
				XCEPT_RETHROW(xgi::exception::Exception, "Invalid flashlist definition", e);
			}

			std::string filename = pathname.str() + (*file).getFilename();

			std::cout << "Name: " << (*file).getName()  << std::endl;
			std::cout << "Filetype: " << (*file).getDataType()  << std::endl;
			std::cout << "Filename: " << (*file).getFilename()  << std::endl;
			std::cout << "Size: " << (*file).getDataLength() << std::endl;
			std::cout << "Full path: " << filename << std::endl;

			try
			{
				this->writeToFile(filename,data,override);
			}
			catch (xmas::exception::Exception & e)
			{
				XCEPT_RETHROW(xgi::exception::Exception, "Cannot save to file", e);
			}
		}

		file = cgi.getFile("PulserSettingsFile");

		if (file != cgi.getFiles().end())
		{
			std::string data = (*file).getData();

			try
			{
				this->checkXMLParsing(data);
			}
			catch (xmas::exception::Exception& e)
			{
				XCEPT_RETHROW(xgi::exception::Exception, "Invalid flashlist definition", e);
			}

			std::string filename = pathname.str() + (*file).getFilename();


			std::cout << "Name: " << (*file).getName()  << std::endl;
			std::cout << "Filetype: " << (*file).getDataType()  << std::endl;
			std::cout << "Filename: " << (*file).getFilename()  << std::endl;
			std::cout << "Size: " << (*file).getDataLength() << std::endl;
			std::cout << "Full path: " << filename << std::endl;

			try
			{
				this->writeToFile(filename,data,override);
			}
			catch (xmas::exception::Exception & e)
			{
				XCEPT_RETHROW(xgi::exception::Exception, "Cannot save to file", e);
			}
		}

	/*}
	catch (const std::exception& e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());

	}*/

	this->Default(in,out);

}

void xmas::slash2g::Application::subscribeFlashlist(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	std::cout << "going to subscribe " << std::endl;
	std::string name;
	std::string hashkey;

	/*try
	{*/
		cgicc::Cgicc cgi(in);
		//bool override = cgi.queryCheckbox("override");


		cgicc::form_iterator fi = cgi.getElement("flashlist");
		if(fi != cgi.getElements().end())
		{
			name = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception,"missing flashlist");
		}

		fi = cgi.getElement("hashkey");
		if (fi != cgi.getElements().end())
		{
			hashkey = (*fi).getValue();
		}
		else
		{
			XCEPT_RAISE(xgi::exception::Exception,"missing hashkey");
		}

	/*}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}*/

	repositoryLock_.take();


	xmas::slash2g::Settings* s = new xmas::slash2g::Settings();
	s->setProperty("name", name);
	s->setProperty("hashkey", hashkey);

	if (flashSettings_.find(s->getProperty("name")) != flashSettings_.end())
	{
		std::stringstream msg;
		msg << "Double definition for flashlist '" << s->getProperty("name") << "'";
		repositoryLock_.give();
		XCEPT_RAISE (xgi::exception::Exception, msg.str());
	}
	else
	{
		//std::cout << "Create settings for flashlist: " << s->getProperty("name") << std::endl;
		flashSettings_[s->getProperty("name")] = s;
	}

	this->prepareSubscriptionRecords();


	repositoryLock_.give();

	this->Default(in,out);
}

void  xmas::slash2g::Application::prepareSubscriptionRecords()  throw (xmas::exception::Exception)
{
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Prepare subscription records");
	const xdaq::Network * network = 0;
	std::string networkName =  this->getApplicationDescriptor()->getAttribute("network");
	try
	{
		network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
	}
	catch (xdaq::exception::NoNetwork& nn)
	{
		std::stringstream msg;
		msg << "Failed to access b2in network " << networkName << ", not configured";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), nn);
	}

	pt::Address::Reference localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());


	for (std::map<std::string, xmas::slash2g::Settings*>::iterator i = flashSettings_.begin(); i != flashSettings_.end(); ++i)
	{
		if ( subscriptions_.find((*i).first) == subscriptions_.end())
		{
			xdata::Properties plist;
			toolbox::net::UUID identifier;
			plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching

			plist.setProperty("urn:b2in-eventing:action", "subscribe");
			plist.setProperty("urn:b2in-eventing:id", identifier.toString());
			plist.setProperty("urn:b2in-eventing:topic", (*i).first); // Subscribe to collected data
			plist.setProperty("urn:b2in-eventing:subscriberurl", localAddress->toString());
			plist.setProperty("urn:b2in-eventing:subscriberservice", this->getApplicationDescriptor()->getAttribute("service"));
			plist.setProperty("urn:b2in-eventing:expires", subscribeExpiration_.toString("xs:duration"));
			subscriptions_[(*i).first] = plist;

		}
	}
}

void xmas::slash2g::Application::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	*out << cgicc::h3("Smart Life Access Server Hub") << std::endl;

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	*out << "<div class=\"xdaq-tab-wrapper\" id=\"processInfoTabPanel\">" << std::endl;

	// General Process Information ( collection tab )
	//
	*out << "<div class=\"xdaq-tab\" title=\"Collected Data\">" << std::endl;
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;") << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Flashlist").set("class", "xdaq-sortable") << std::endl;
	*out << cgicc::th("Last Update") << std::endl;
	*out << cgicc::th("Version") << std::endl;
	*out << cgicc::th("Tags") << std::endl;
	*out << cgicc::th("Originator") << std::endl;
	*out << cgicc::th("Size") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	std::map<std::string, xmas::slash2g::FlashlistData*>::iterator si; 
	for (si = repository_.begin(); si != repository_.end(); ++si)
	{
		*out << cgicc::tr() << std::endl;

		// output flashlist name
		std::stringstream link;
		link << "<a href=\"";
		link << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/";
		link <<  this->getApplicationDescriptor()->getURN() << "/";
		link << "retrieveCollectionHTML?fmt=html&flash=" << (*si).first << "\">" << (*si).first << "</a>";

		*out << cgicc::td( link.str() ) << std::endl;

		xmas::slash2g::FlashlistData* data = (*si).second;

		*out << cgicc::td( data->getLastUpdate().toString(toolbox::TimeVal::gmt) ) << std::endl;
		*out << cgicc::td( data->getVersion() ) << std::endl;
		*out << cgicc::td( data->getTags() ) << std::endl;
		*out << cgicc::td( data->getLastOriginator() ) << std::endl;

		std::stringstream rows;
		rows << data->getSize();
		*out << cgicc::td( rows.str() ) << std::endl;

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
	*out << "</div>";

	//
	// Topics Tab
	//

	*out << "<div class=\"xdaq-tab\" title=\"Subscriptions\">" << std::endl;
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;") << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Topic").set("class", "xdaq-sortable") << std::endl;
	*out << cgicc::th("Hashkey") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	for (std::map<std::string, xmas::slash2g::Settings*>::iterator i = flashSettings_.begin(); i != flashSettings_.end(); ++i)
	{
		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*i).first) << std::endl;
		*out << cgicc::td((*i).second->getProperty("hashkey")) << std::endl;
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << "</div>";

	// Uploading sensor/pulser settings
	*out << "<div class=\"xdaq-tab\" title=\"Settings\">" << std::endl;


	*out << cgicc::h3("Subscribe to flashlist") << std::endl;

	url = "/";
	url += getApplicationDescriptor()->getURN();

	url += "/subscribeFlashlist";

	*out << cgicc::form().set("method", "POST").set("action", url).set("class", "xdaq-form") << std::endl;

	*out << cgicc::table().set("class", "xdaq-table-nohover") << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Subscribe flashlist  (e.g. urn:xdaq-flashlist:MyFlashlist)")) << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "text").set("name", "flashlist").set("size", "40").set("value", "")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Hash Key (e.g. context,lid)")) << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "text").set("name", "hashkey").set("size", "40").set("value", "")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("") << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "submit").set("name", "submit").set("value", "Subscribe").set("class", "xdaq-controlpanel-button")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::h3("Upload Flashlist definition") << std::endl;

	url = "/";
	url += getApplicationDescriptor()->getURN();

	url += "/uploadFlashlist";

	*out << cgicc::form().set("method", "POST").set("action", url).set("class", "xdaq-form").set("enctype", "multipart/form-data") << std::endl;

	*out << cgicc::table().set("class", "xdaq-table-nohover") << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Flashlist definition (.flash file in XML format)")) << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "file").set("name", "FlashlistDefinitionFile").set("accept", "text/*")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("Override if exists") << std::endl;
    *out << cgicc::td(cgicc::input().set("type", "checkbox").set("name", "override")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("") << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "submit").set("name", "submit").set("value", "Upload").set("class", "xdaq-controlpanel-button")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::h3("Upload pulser and sensor settings") << std::endl;

	url = "/";
	url += getApplicationDescriptor()->getURN();

	url += "/uploadSettings";

	*out << cgicc::form().set("method", "POST").set("action", url).set("class", "xdaq-form").set("enctype", "multipart/form-data") << std::endl;

	*out << cgicc::table().set("class", "xdaq-table-nohover") << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Sensor settings (<service name>.sensor file in XML format)")) << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "file").set("name", "SensorSettingsFile").set("accept", "text/*")) << std::endl;
	*out << cgicc::tr() << std::endl;


	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(cgicc::label("Pulser settings (<service name>.pulser file in XML format)")) << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "file").set("name", "PulserSettingsFile").set("accept", "text/*")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("Override if exist") << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "checkbox").set("name", "override")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("") << std::endl;
	*out << cgicc::td(cgicc::input().set("type", "submit").set("name", "submit").set("value", "Upload").set("class", "xdaq-controlpanel-button")) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
	*out << cgicc::form() << std::endl;

	*out << "</div>";

}


void xmas::slash2g::Application::applySettings(const std::string & path)
{     
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Apply slash2g settings:" << path );

	// The profile may be a pattern, so expand it before
	std::vector<std::string> files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(path);
	} 
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg; 
		msg << "Failed to read slash2g settings from '" << path << "', ";
		XCEPT_DECLARE_NESTED(xmas::slash2g::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal",q);
		return;	
	}


	for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
	{
		DOMDocument* doc = 0;
		try
		{
			doc = xoap::getDOMParserFactory()->get("configure")->loadXML( *j );
			
			// One settings record per flashlist
			//			
			DOMNodeList* flList = doc->getElementsByTagNameNS(xoap::XStr(xmas::NamespaceUri), xoap::XStr("flash"));
			for (XMLSize_t i = 0; i < flList->getLength(); i++)
			{
				DOMNode* flashNode = flList->item(i);
				xmas::slash2g::Settings* s = new xmas::slash2g::Settings(flashNode);
				
				if (flashSettings_.find(s->getProperty("name")) != flashSettings_.end())
				{
					std::stringstream msg;
					msg << "Double definition for flashlist '" << s->getProperty("name") << "'";
                    			//std::cout << msg.str() << std::endl;
					XCEPT_RAISE (xmas::exception::Exception, msg.str());
				}
				else
				{
					//std::cout << "Create settings for flashlist: " << s->getProperty("name") << std::endl;
					flashSettings_[s->getProperty("name")] = s;
				}
			}
			doc->release();
			doc = 0;
			LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Loaded slash2g settings '" << (*j) << "'");				
		}
		catch (xmas::exception::Exception& e)
		{
			if (doc != 0) doc->release();
			doc = 0;
			std::stringstream msg; 
			msg << "Failed to process slash2g settings from '" << path << "', ";
			XCEPT_DECLARE_NESTED(xmas::slash2g::exception::Exception, q, msg.str(), e);
			LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
			this->notifyQualified("fatal",q);
			return;	
		}
		catch (xoap::exception::Exception& e)
		{
			if (doc != 0) doc->release();
			doc = 0;
			std::stringstream msg; 
			msg << "Failed to process slash2g settings from '" << path << "', ";
			XCEPT_DECLARE_NESTED(xmas::slash2g::exception::Exception, q, msg.str(), e);
			LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
			this->notifyQualified("fatal",q);
			return;	
		}
		catch (xcept::Exception& e)
		{
			if (doc != 0) doc->release();
			doc = 0;
			std::stringstream msg; 
			msg << "Failed to process slash2g settings from '" << path << "', ";
			XCEPT_DECLARE_NESTED(xmas::slash2g::exception::Exception, q, msg.str(), e);
			LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
			this->notifyQualified("fatal",q);
			return;	
		}
	}
	
}
