<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
	<!-- Compulsory  Plugins -->
	<xp:Application heartbeat="false" class="executive::Application" id="0" group="profile" service="executive" network="local">
		<properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
			<logUrl xsi:type="xsd:string">console</logUrl>
                	<logLevel xsi:type="xsd:string">INFO</logLevel>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>

	<!-- XPlore requires the installation of Power Pack  -->
        <!--xp:Application heartbeat="false" class="xplore::Application" id="9"  network="local">
                <properties xmlns="urn:xdaq-application:xplore::Application" xsi:type="soapenc:Struct">
                        <settings xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/xplore/shortcuts.xml</settings>
                        <republishInterval xsi:type="xsd:string">60</republishInterval>
                </properties>
        </xp:Application>       
        <xp:Module>/lib/libslp.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxslp.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxploreutils.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libxplore.so</xp:Module-->

 	<xp:Application heartbeat="false" class="pt::tcp::PeerTransportTCP" id="20" network="local">
                        <properties xmlns="urn:xdaq-application:PeerTransportTCP" xsi:type="soapenc:Struct">
                                <autoSize xsi:type="xsd:boolean">true</autoSize>
                                <maxPacketSize xsi:type="xsd:unsignedInt">262144</maxPacketSize>
                        </properties>
         </xp:Application>
          <xp:Module>${XDAQ_ROOT}/lib/libpttcp.so</xp:Module>

        <xp:Endpoint protocol="tcp" service="b2in" subnet="10.176.0.0" port="1910"  maxport="1950" autoscan="true" network="xmas"/> 

        <!-- xp:Endpoint protocol="tcp" service="b2in" subnet="10.176.0.0" port="1910"  maxport="1950" autoscan="true" network="sentinel"/ -->

	<xp:Application heartbeat="false" class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local">
		 <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
		 	<documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
                        <aliasName xsi:type="xsd:string">tmp</aliasName>
                        <aliasPath xsi:type="xsd:string">/tmp</aliasPath>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>

	<xp:Application heartbeat="false" class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>
	
	<!-- HyperDAQ -->
	<xp:Application heartbeat="false" class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>	

 	<!-- xp:Application heartbeat="false" class="sentinel::probe2g::Application" id="21"  network="sentinel" group="sentinel" service="sentinelprobe2g">
                <properties xmlns="urn:xdaq-application:Sentinel" xsi:type="soapenc:Struct">
                        <watchdog xsi:type="xsd:string">PT5S</watchdog>
                        <publishGroup xsi:type="xsd:string">sentinel</publishGroup>
                        <brokerWatchdog xsi:type="xsd:string">PT5S</brokerWatchdog>
                        <brokerURL xsi:type="xsd:string">tcp://dvsrv-c2f36-03.cms:1998</brokerURL>
			<brokerWatchdog xsi:type="xsd:string">PT5S</brokerWatchdog>
                </properties>
        </xp:Application -->
	<xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libsentinelutils.so</xp:Module>
        <xp:Module>${XDAQ_ROOT}/lib/libsentinelprobe2g.so</xp:Module>
	
	<xp:Application heartbeat="true" class="xmas::probe::Application" id="400" network="xmas" group="xmas" service="xmasprobe">
	 	<properties xmlns="urn:xdaq-application:xmas::sensor2g::Application" xsi:type="soapenc:Struct">
			<autoConfigure xsi:type="xsd:boolean">true</autoConfigure>
			<maxReportMessageSize xsi:type="xsd:unsignedInt" >0x100000</maxReportMessageSize>
			<autoConfSearchPath xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}/sensor</autoConfSearchPath>
			<heartbeatWatchdog xsi:type="xsd:string">PT5S</heartbeatWatchdog>
		</properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libwsaddressing.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libxmasutils.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libxmasprobe.so</xp:Module>
</xp:Profile>
