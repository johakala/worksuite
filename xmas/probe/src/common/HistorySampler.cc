// $Id: HistorySampler.cc,v 1.2 2008/07/18 15:28:34 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/probe/HistorySampler.h"

#include <iostream>
#include <string>
#include "toolbox/ActionListener.h"
#include "toolbox/Event.h"
#include "xdata/Table.h"
#include "xmas/SamplerSettings.h"
#include "xmas/probe/HistorySampler.h"
#include "xmas/probe/MonitorReportEvent.h"
#include "xmas/probe/exception/Exception.h"
#include "toolbox/TimeVal.h"

void displayTableToCSV( xdata::Table*  t )
{	
	std::vector<std::string> columns = t->getColumns();

	for (std::vector<std::string>::size_type i = 0; i < columns.size(); i++ )
	{
		if (i != 0) 
		{
			std::cout << ",";
		}
		std::cout << columns[i];
	}

	std::cout << std::endl;


	for ( size_t j = 0; j <  t->getRowCount(); j++ )
	{
		// Output collection time
		for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
		{
			if (k != 0) 
			{
				std::cout << ",";
			}
			xdata::Serializable * s = t->getValueAt(j, columns[k]);
			if (s != 0)
			{
				std::cout << s->toString();
			}
			else
			{
				std::cout << "null";
			}
		}
		std::cout << std::endl;
	}	
	
}

std::string xmas::probe::HistorySampler::type()
{
	return "urn:xmas-sampler:history";
}

xmas::probe::HistorySampler::HistorySampler(xmas::SamplerSettings* settings,xmas::FlashListDefinition * fld)
	: xmas::probe::Sampler(settings,fld)
{
	cursor_ = toolbox::TimeVal::gettimeofday(); // remember first start buffering time
}			
			
void xmas::probe::HistorySampler::actionPerformed(toolbox::Event & e)
{
	//std::cout << "received event in history sampler " << e.type()<< std::endl;
	
	xmas::probe::MonitorReportEvent & me = dynamic_cast<xmas::probe::MonitorReportEvent&>(e);
	
	std::string tag = me.getProperty("tag");
	if ((tag != "") && (!this->hasTag (tag)))
	{
		//std::cout << "if the tag in the report does not match the tag of the sampler, ignore the event" << tag <<  std::endl;
		
		return; // if the tag in the report does not match the tag of the sampler, ignore the event
	}

	std::map<std::string, xdata::Table::Reference>  metrics = me.getMetrics();
	for ( std::map<std::string, xdata::Table::Reference>::iterator i = metrics.begin(); i != metrics.end(); i++ )
	{
		try
		{
			xdata::Table::Reference t = (*i).second;
			toolbox::TimeVal timestamp;
			timestamp.fromString((*i).first,"", toolbox::TimeVal::gmt);
			repository_.add(timestamp, t);
		}
		catch (xmas::probe::exception::Exception& e)
		{
			// If the timestamp exists already, simply ignore the record
			// The granularity is microseconds, so this should never happen
			continue;
		}
		catch (toolbox::exception::Exception &e)
		{
			// This happens in case of an invalid time format. That should never
			// happen in this context. The format should already be checked before
			#warning "Check timeformat in xmas::probe::HistorySampler::actionPerformed"
			continue;
		}
	}
	//xdata::Table * table = me.getTable();
	//std::string name = me.getFlashListName();
	
	toolbox::TimeInterval range;
	toolbox::TimeVal current;
	current = toolbox::TimeVal::gettimeofday();
	range.fromString(this->getSettings()->getProperty("range").c_str());

	//std::cout << "Current: " << current.sec() << ", range: " << range.sec() << std::endl;

	toolbox::TimeVal olderThan( current.sec() - range.sec(), 0 );
	
	//std::cout << "Older than: " << olderThan.toString(toolbox::TimeVal::loc) << std::endl;
	
	// Clear if overflow
	try 
	{
		repository_.remove(olderThan);
	}
	catch ( xcept::Exception & e ) 
	{ 
		// ignore
	}	

	if ( cursor_ <=  olderThan ) // new window of data, therefore send notification
	{
		
		//std::cout << "new window of data, therefore send notification" << std::endl;
		
		xmas::probe::MonitorReportEvent report(me.getFlashListDefinition());
	
		report.setProperty ("tag", this->getSettings()->getProperty("tag"));
	
		std::map<toolbox::TimeVal, xdata::Table::Reference> history = repository_.getHistory
			(toolbox::TimeVal::zero(), toolbox::TimeVal::gettimeofday());
		
		if ( history.size() == 0 )
		{
			// The history can be empty. In this case do not further process
			return;
		}	
		
		if (  this->getSettings()->getProperty("output") == "merge" )
		{
			xdata::Table::Reference collection;
			std::map<toolbox::TimeVal, xdata::Table::Reference>::iterator i;
			for (i = history.begin(); i != history.end(); ++i)
			{
				if ( collection.isNull() )
				{
					collection = new xdata::Table();

					// create column entries (name and type) on incoming flashlist otherwise merging fails afterwards
					std::vector<std::string> columns = ((*i).second)->getColumns();
					for( std::vector<std::string>::iterator iter = columns.begin() ; iter != columns.end() ; iter++)
					{
						const std::string& name = (*iter);
						const std::string& type = ((*i).second)->getColumnType(name);
						collection->addColumn(name, type);
					}
				}

				{	
					xdata::Table::Reference t =  (*i).second;
					//std::cout << "Merge table " << t->getRowCount() << std::endl;
					//std::cout << "Collection table " << collection->getRowCount() << std::endl;
					//displayTableToCSV(&(*t));
					try
					{
						xdata::Table::merge(&(*collection), &(*t));
					}
					catch (	xdata::exception::Exception & e )
					{
						#warning "Exception handling to be done"
						// eventually just ignore this operation, but better to investigate why this happens
						return;
					}
				}			
			}
			
			// just for protection
			if (collection.isNull())
			{
				#warning "Exception handling to be done"
				return;
			}
			report.addMetrics( current.toString(toolbox::TimeVal::gmt), collection); // time is changed to merging time
		}
		else
		{
			std::map<toolbox::TimeVal, xdata::Table::Reference>::iterator i;
			for (i = history.begin(); i != history.end(); ++i)
			{
				report.addMetrics( (*i).first.toString(toolbox::TimeVal::gmt), (*i).second);
			}
		}
	
		// Synchronous fire
		this->fireEvent(report);
		
		cursor_ = current;	
	}
}

double xmas::probe::HistorySampler::getLatency()
{
	// NOT IMPLEMENTED
	return 0.0;
}						
						

xmas::probe::MonitorReport xmas::probe::HistorySampler::getMetrics()
{
	xmas::probe::MonitorReport report(this->getFlashListDefinition());
	report.setProperty ("tag", this->getSettings()->getProperty("tag"));
	
	std::map<toolbox::TimeVal, xdata::Table::Reference> history = repository_.getHistory
		(toolbox::TimeVal::zero(), toolbox::TimeVal::gettimeofday());
		
	std::map<toolbox::TimeVal, xdata::Table::Reference>::iterator i;
	for (i = history.begin(); i != history.end(); ++i)
	{
		report.addMetrics( (*i).first.toString(toolbox::TimeVal::gmt), (*i).second);
	}
	return report;
}			

