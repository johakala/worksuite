// $Id: InstantSampler.cc,v 1.2 2008/07/18 15:28:34 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/probe/InstantSampler.h"

#include <iostream>
#include <string>
#include "toolbox/ActionListener.h"
#include "toolbox/Event.h"
#include "xdata/Table.h"
#include "xmas/SamplerSettings.h"
#include "xmas/probe/InstantSampler.h"
#include "xmas/probe/MonitorReportEvent.h"
#include "xmas/probe/exception/Exception.h"
#include "toolbox/TimeVal.h"

std::string xmas::probe::InstantSampler::type()
{
	return "urn:xmas-sampler:instant";
}

xmas::probe::InstantSampler::InstantSampler(xmas::SamplerSettings* settings, xmas::FlashListDefinition * fld)
	: xmas::probe::Sampler(settings,fld)
{
}			
			
void xmas::probe::InstantSampler::actionPerformed(toolbox::Event & e)
{

	xmas::probe::MonitorReportEvent & me = dynamic_cast<xmas::probe::MonitorReportEvent&>(e);
	
	// if no tag, always process the event
	std::string tag = me.getProperty("tag");
	if ( (tag != "") && (!this->hasTag (tag)) )
	{
		//std::cout << "ignore event if tag does not match sampler tag" << std::endl;
		return; // ignore event if tag does not match sampler tag
	}
	
	repository_.clear();
	xmas::probe::MonitorReportEvent report(me.getFlashListDefinition());
	report.setProperty ("tag", this->getSettings()->getProperty("tag") );
	
	std::map<std::string, xdata::Table::Reference>  metrics = me.getMetrics();
	for ( std::map<std::string, xdata::Table::Reference>::iterator i = metrics.begin(); i != metrics.end(); i++ )
	{
		report.addMetrics((*i).first, (*i).second);
		xdata::Table::Reference t = (*i).second;
		toolbox::TimeVal timestamp;
		timestamp.fromString((*i).first,"", toolbox::TimeVal::gmt);
		repository_.add(timestamp, t);
	}
	
	// Synchronous forward new event with tag
	
	this->fireEvent(report);
	
}

double xmas::probe::InstantSampler::getLatency()
{
	// NOT IMPLEMENTED
	return 0.0;
}						


xmas::probe::MonitorReport xmas::probe::InstantSampler::getMetrics()
{
	xmas::probe::MonitorReport report(this->getFlashListDefinition()) ;
	
	std::map<toolbox::TimeVal, xdata::Table::Reference> history = repository_.getHistory
		(toolbox::TimeVal::zero(), toolbox::TimeVal::gettimeofday());
	std::map<toolbox::TimeVal, xdata::Table::Reference>::iterator i;
	for (i = history.begin(); i != history.end(); ++i)
	{
		report.addMetrics( (*i).first.toString(toolbox::TimeVal::gmt), (*i).second);
	}
	
	report.setProperty ("tag", this->getSettings()->getProperty("tag") );
	
	return  report;
}	

