// $Id: HashSampler.cc,v 1.2 2008/07/18 15:28:34 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/probe/HashSampler.h"

#include <iostream>
#include <string>
#include "toolbox/ActionListener.h"
#include "toolbox/Event.h"
#include "toolbox/string.h"
#include "xdata/Table.h"
#include "xdata/TableAlgorithms.h"
#include "xmas/SamplerSettings.h"
#include "xmas/probe/HashSampler.h"
#include "xmas/probe/MonitorReportEvent.h"
#include "xmas/probe/exception/Exception.h"
#include "xmas/probe/exception/AmbiguousFlashListDefinition.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/net/URN.h"



std::string xmas::probe::HashSampler::type()
{
	return "urn:xmas-sampler:hash";
}

xmas::probe::HashSampler::HashSampler(xmas::SamplerSettings* settings, xmas::FlashListDefinition * fld, xdaq::Application * owner)
	: xmas::probe::Sampler(settings,fld), xdaq::Object(owner), mutex_(toolbox::BSem::FULL)
{
	std::srand((unsigned) time(0));

	toolbox::task::Timer * timer = 0;
	// Create timer for refreshing subscriptions
	if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:probe-timer"))
	{
		timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:probe-timer");

		
	}
	else
	{
		  timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:probe-timer");
	}
	// submit task
        toolbox::TimeInterval interval;
	std::string every = this->getSettings()->getProperty("every");
	if ( every == "" ) every = "PT3S";
        interval.fromString(every); 
	
	// Random delay for starting, by default 15 seconds
	std::string delay = this->getSettings()->getProperty("delay");
	if ( delay == "" ) delay = "15";
	size_t intDelay = (rand() % toolbox::toUnsignedLong(delay)) + 1;
        toolbox::TimeVal start(toolbox::TimeVal::gettimeofday().sec() + intDelay);
	toolbox::net::URN name(this->getFlashListDefinition()->getProperty("name"));
	toolbox::net::UUID id;

        timer->scheduleAtFixedRate( start, this, interval, 0, "urn:xmas-hash-sampler-task:" +  name.getNSS() + "-" + id.toString());

}			
			
void xmas::probe::HashSampler::actionPerformed(toolbox::Event & e)
{
    mutex_.take();

    xmas::probe::MonitorReportEvent & me = dynamic_cast<xmas::probe::MonitorReportEvent&>(e);

    // if no tag, always process the event
    std::string tag = me.getProperty("tag");
    if ( (tag != "") && (!this->hasTag (tag)) )
    {
        mutex_.give();
        //std::cout << "ignore event if tag does not match sampler tag" << std::endl;
        return; // ignore event if tag does not match sampler tag
    }

    std::set<std::string> hashkey = toolbox::parseTokenSet( this->getSettings()->getProperty("hashkey"),",");
    std::map<std::string, xdata::Table::Reference>  metrics = me.getMetrics();
    for ( std::map<std::string, xdata::Table::Reference>::iterator i = metrics.begin(); i != metrics.end(); i++ )
    {
        if (current_.isNull())
        {
		current_ = new xdata::Table();

		// create column entries (name and type) on incoming flashlist otherwise merging fails afterwards
		std::vector<std::string> columns = ((*i).second)->getColumns();
		for( std::vector<std::string>::iterator iter = columns.begin() ; iter != columns.end() ; iter++)
		{
			const std::string& name = (*iter);
			const std::string& type = ((*i).second)->getColumnType(name);
			current_->addColumn(name, type);
		}
        }

        {
            try
            {
                xdata::merge(&(*current_),&(*((*i).second)), hashkey);
                //std::cout << "." << std::flush;
            }
            catch(xdata::exception::Exception & e)
            {
                // incopatible tables, flash list misconfigured in some probe ( not the same identical definition)
                std::stringstream  msg;
                msg << "Cannot sample, ambiguous monitor report with tag '" <<  me.getProperty("tag")
                << "' for flashlist '" <<  me.getFlashListName() << "' from originator '" << me.getProperty("originator") << "'";

                XCEPT_DECLARE_NESTED(xmas::probe::exception::AmbiguousFlashListDefinition, ex, msg.str(),e);
                this->getOwnerApplication()->notifyQualified("fatal",ex);
            }
        }
    }
    
    // Remember received report time stamp
    incomingReportTimes_.push_back ((double) toolbox::TimeVal::gettimeofday());

    mutex_.give();

}						


xmas::probe::MonitorReport xmas::probe::HashSampler::getMetrics()
{
	xmas::probe::MonitorReport report(this->getFlashListDefinition()) ;
	
#warning "xmas::probe::HashSampler::getMetrics() not implemented"
	
	return  report;
}	

double xmas::probe::HashSampler::getLatency()
{
	mutex_.take();
	if (incomingReportTimes_.size() == 0)
	{
		mutex_.give();
		return 0.0;
	}
	
	// Return difference between oldest and newest timestmap
	double min = *(incomingReportTimes_.begin());
	double max = *(incomingReportTimes_.begin());
	
	std::vector<double>::iterator i;
	for (i = incomingReportTimes_.begin(); i != incomingReportTimes_.end(); ++i)
	{
		if ((*i) < min)
		{
			min = (*i);
		}
		else if ((*i) > max)
		{
			max = (*i);
		}
	}	
	
	mutex_.give();
	return  (max - min);
}	

void xmas::probe::HashSampler::timeExpired(toolbox::task::TimerEvent& e)
{
	mutex_.take();

    if (current_.isNull())
    {
        mutex_.give();
        return;
    }

    xmas::probe::MonitorReportEvent report(this->getFlashListDefinition());
    report.setProperty ("tag", this->getSettings()->getProperty("tag") );

        std::string clear = this->getSettings()->getProperty("clear");
    if (clear == "true")
    {
        report.addMetrics(toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt), current_);
        current_ = 0;
    }
    else
    {
        xdata::Table & table = *(current_);
        xdata::Table::Reference tref(new xdata::Table(table)); // copy table
        report.addMetrics(toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt), tref);
    }

    // Clear the timestamp histogram
	incomingReportTimes_.clear();

    mutex_.give();

    // Synchronous forward new event with tag
    this->fireEvent(report);
    
}
