// $Id: MonitorReportEvent.cc,v 1.2 2008/07/18 15:28:34 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/probe/MonitorReportEvent.h"
#include <string>
#include <sstream>
#include <map>
#include "xdata/Table.h"
#include "xmas/exception/Exception.h"
#include "xmas/FlashListDefinition.h"
			
xmas::probe::MonitorReportEvent::MonitorReportEvent( xmas::FlashListDefinition * definition )
	: xmas::probe::MonitorReport(definition),
	  toolbox::Event("urn:xmas-probe-event:MonitorReport", 0)
{
	//std::cout << "$$$ CTOR of Monitor report event: " << this->getFlashListName() << " $$$ " << std::endl;
}

xmas::probe::MonitorReportEvent::~MonitorReportEvent()
{
	//std::cout << "$$$ DTOR of Monitor report event: " << this->getFlashListName() << " $$$ " << std::endl;
}
