// $Id: Sampler.cc,v 1.2 2008/07/18 15:28:34 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <list>
#include <string>
#include "xmas/probe/Sampler.h"
#include "xmas/SamplerSettings.h"
#include "toolbox/ActionListener.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/string.h"
#include "toolbox/stl.h"
#include "xmas/probe/MonitorReport.h"
#include "xmas/FlashListDefinition.h"
#include "toolbox/string.h"

			
xmas::probe::Sampler::Sampler (xmas::SamplerSettings* settings, xmas::FlashListDefinition* fld)
	: settings_(settings), definition_(fld)
{			
	toolbox::StringTokenizer tokenizer(settings_->getProperty("tag"),",");
	while (tokenizer.hasMoreTokens())
	{
		std::string g = toolbox::trim(tokenizer.nextToken());
		tag_.insert(g);
	}
}

xmas::SamplerSettings* xmas::probe::Sampler::getSettings()
{
	return settings_;
}

xmas::probe::Sampler::~Sampler () 
{
	/*
	std::list<toolbox::ActionListener*> listeners = this->getActionListeners();
	std::list<toolbox::ActionListener*>::iterator ai;
	for (ai = listeners.begin(); ai != listeners.end(); ++ai)
	{
		this->removeActionListener( *ai );
		delete (*ai);
	}
	*/
}

std::set<std::string>& xmas::probe::Sampler::getTagSet()
{
	return tag_;
}

bool xmas::probe::Sampler::hasTag (const std::string& tag)
{

	// apply an AND of filter tags with the reported tags
	// if filter is empty or tag matched return true
	//
	std::set<std::string> filter = toolbox::parseTokenSet(settings_->getProperty("filter"),",");
	if ( ! filter.empty() )
	{
		std::set<std::string> reporttags =  toolbox::parseTokenSet(tag,",");
		std::set<std::string> intersection = toolbox::stl::intersection(filter,reporttags);

		if ( filter.size() != intersection.size() )
		{
			return false;
		}
	}
	return true;
/*
	toolbox::StringTokenizer tokenizer(tag,",");
	while (tokenizer.hasMoreTokens())
	{
		std::string t = toolbox::trim(tokenizer.nextToken());
		if (tag_.find(t) != tag_.end())
		{
			return true;
		}
	}

	return false;
*/
	
}

xmas::FlashListDefinition* xmas::probe::Sampler::getFlashListDefinition()
{
	return definition_;
}
			
			
