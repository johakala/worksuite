// $Id: version.h,v 1.7 2009/02/20 13:50:33 rmoser Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_heartbeat_version_h_
#define _xmas_heartbeat_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define XMASHEARTBEAT_VERSION_MAJOR 2
#define XMASHEARTBEAT_VERSION_MINOR 4
#define XMASHEARTBEAT_VERSION_PATCH 0
// If any previous versions available E.g. #define XMASHEARTBEAT_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef XMASHEARTBEAT_PREVIOUS_VERSIONS
#define XMASHEARTBEAT_PREVIOUS_VERSIONS "2.2.0,2.2.1,2.3.0"

//
// Template macros
//
#define XMASHEARTBEAT_VERSION_CODE PACKAGE_VERSION_CODE(XMASHEARTBEAT_VERSION_MAJOR,XMASHEARTBEAT_VERSION_MINOR,XMASHEARTBEAT_VERSION_PATCH)
#ifndef XMASHEARTBEAT_PREVIOUS_VERSIONS
#define XMASHEARTBEAT_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(XMASHEARTBEAT_VERSION_MAJOR,XMASHEARTBEAT_VERSION_MINOR,XMASHEARTBEAT_VERSION_PATCH)
#else 
#define XMASHEARTBEAT_FULL_VERSION_LIST  XMASHEARTBEAT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(XMASHEARTBEAT_VERSION_MAJOR,XMASHEARTBEAT_VERSION_MINOR,XMASHEARTBEAT_VERSION_PATCH)
#endif 

namespace xmasheartbeat
{
	const std::string package  =  "xmasheartbeat";
	const std::string versions =  XMASHEARTBEAT_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Penelope Roberts";
	const std::string summary = "XDAQ Monitoring and Alarming System heartbeat server";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
