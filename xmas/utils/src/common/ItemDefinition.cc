// $Id: ItemDefinition.cc,v 1.7 2008/07/18 15:28:37 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <sstream>

#include "xmas/ItemDefinition.h"

xmas::ItemDefinition::ItemDefinition(DOMNode* item) throw (xmas::exception::Exception)
{
	DOMNamedNodeMap* attributes = item->getAttributes();
	for (unsigned int i = 0; i < attributes->getLength(); i++ ) 
	{
		std::string valueStr  = xoap::XMLCh2String( attributes->item(i)->getNodeValue() );
		std::string nameStr  = xoap::XMLCh2String( attributes->item(i)->getNodeName() );
		this->setProperty(nameStr,valueStr);
		//std::cout << "--> Flashlist [" << this->getProperty("name") << "] = (" << nameStr << ","
		//<<   valueStr << ")" << std::endl; 
	}

		

	if (! this->hasProperty("name" ) )
	{
		XCEPT_RAISE (xmas::exception::Exception, "missing 'name' attribute");
	}
	
	if (!  this->hasProperty("type" ) )
	{
		XCEPT_RAISE (xmas::exception::Exception,"missing 'name' attribute");
	}
	
	std::string name =  this->getProperty("name" );
	
	if ( !  this->hasProperty("source") )
	{
		this->setProperty("source",name);
	}
	
	/*
	if ( !  this->hasProperty("infospace" ) )
	{
		if ( !  this->hasProperty("function" ) )
		{
			XCEPT_RAISE (xmas::exception::Exception, "missing infospace/function attribute in item");
		
		}

	}
	
	*/
	DOMNodeList* items = item->getChildNodes();
	for (XMLSize_t j = 0; j < items->getLength(); j++)
	{
		DOMNode* itemDefinition = items->item(j);
		if ((itemDefinition->getNodeType() == DOMNode::ELEMENT_NODE) &&
			    (xoap::XMLCh2String(itemDefinition->getLocalName()) == "item"))
		{

				xmas::ItemDefinition  * definition = new xmas::ItemDefinition(itemDefinition);
				std::string name = definition->getProperty("name");
					
				std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i = items_.find(name);
				if (i != items_.end())
				{
					delete definition;
					std::stringstream msg;
					msg << "Ambiguous item name '";
					msg << name << "', already existing declaration in list of item";
					XCEPT_RAISE (xmas::exception::Exception, msg.str());
				}
				items_[name] = definition;


		}
	}
}
		

xmas::ItemDefinition::~ItemDefinition()
{
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i = items_.begin();
	while (i != items_.end())
	{
		delete (*i).second;
		++i;
	}
}

xmas::ItemDefinition* xmas::ItemDefinition::getItem(const std::string & name)
	 throw (xmas::exception::Exception)
{
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i = items_.find(name);
	
	if (i != items_.end() )
	{
		return (*i).second;
	}
	else
	{
		std::stringstream msg;
		msg << "Cannot find item '" << name << "'";
		XCEPT_RAISE (xmas::exception::Exception, msg.str());
	}
}



std::vector<xmas::ItemDefinition*> xmas::ItemDefinition::getItems()
{
	std::vector<xmas::ItemDefinition*> v;
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i;
	for (i = items_.begin(); i != items_.end(); i++)
	{
		v.push_back( (*i).second );
	}
	return v;
}

void xmas::ItemDefinition::getNestedDepth(size_t depth, size_t & maxdepth)
{
	if ( maxdepth < depth ) maxdepth = depth;
	
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i;
	for (i = items_.begin(); i != items_.end(); i++)
	{
		(*i).second->getNestedDepth(depth+1, maxdepth);
	}
}
