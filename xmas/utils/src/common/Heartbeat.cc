//$Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/utils/Heartbeat.h"
#include "xoap/MessageFactory.h"
#include "xoap/domutils.h"
#include "xmas/xmas.h"
#include "xdaq/ApplicationDescriptorImpl.h"


xmas::utils::Heartbeat::Heartbeat(xdaq::ApplicationDescriptor &  descriptor)
{
	xdata::Properties & attributes = dynamic_cast<xdata::Properties&>(descriptor);

      	for (std::map<std::string, std::string, std::less<std::string> >::iterator i = attributes.begin(); i != attributes.end(); ++i)
      	{
              	p_.setProperty ( (*i).first, (*i).second );
      	}

	url_ = descriptor.getContextDescriptor()->getURL();	
}
		
xmas::utils::Heartbeat::Heartbeat(xoap::MessageReference& msg) throw (xmas::exception::Exception)
{
        aheaders_.fromSOAP(msg);

        xoap::SOAPPart part = msg->getSOAPPart();
        xoap::SOAPEnvelope envelope = part.getEnvelope();
        xoap::SOAPBody body = envelope.getBody();
        //DOMNode* node = body.getDOMNode();
        //DOMNodeList* bodyList = node->getChildNodes();
        xoap::SOAPHeader header = envelope.getHeader();
        std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

        // Extract header fields to determine the subscription
        std::vector<xoap::SOAPHeaderElement>::iterator i;
        for (i = elements.begin(); i != elements.end(); ++i)
        {

        /*
                std::string name = (*i).getElementName().getLocalName();
                std::string value = toolbox::trim((*i).getValue());
                if (name == "Identifier")
                {
                        if (value.find("uuid:") == std::string::npos)
                        {
                                std::stringstream msg;
                                msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
                                XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
                        }

                        try
                        {
                                ws::eventing::Identifier id(value.substr(5));
                                identifier_ = id;
                        }
                        catch (toolbox::net::exception::Exception& e)
                        {
                                XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
                        }
                }
        */

        }

        // parse message body and fill Renew fields
        xoap::SOAPName viewName("heartbeat", xmas::NamespacePrefix, xmas::NamespaceUri);
        std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(viewName);

       if (bodyElements.size() != 1)
        {
                XCEPT_RAISE (xmas::exception::Exception, "Invalid message: missing or bad <heartbeat/> element" );
        }

        xoap::SOAPName url = envelope.createName("url", xmas::NamespacePrefix, xmas::NamespaceUri);
	url_ = bodyElements[0].getAttributeValue(url);

        xoap::SOAPName itemName("item", xmas::NamespacePrefix, xmas::NamespaceUri);
        std::vector<xoap::SOAPElement> itemElements = bodyElements[0].getChildElements(itemName);
	
       	xoap::SOAPName name = envelope.createName("name", "", "");
       	xoap::SOAPName value = envelope.createName("value", "", "");
	for ( std::vector<xoap::SOAPElement>::size_type i = 0; i < itemElements.size(); i++  )
	{
        	p_.setProperty(itemElements[i].getAttributeValue(name),itemElements[i].getAttributeValue(value));
	}

}
       
xmas::utils::Heartbeat::~Heartbeat()
{
}
				
xoap::MessageReference xmas::utils::Heartbeat::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
        xoap::SOAPPart soap = request->getSOAPPart();
        xoap::SOAPEnvelope envelope = soap.getEnvelope();
        xoap::SOAPBody responseBody = envelope.getBody();

        aheaders_.toSOAP(request);

        xoap::SOAPName command = envelope.createName ("heartbeat",xmas::NamespacePrefix, xmas::NamespaceUri);

        xoap::SOAPElement element = responseBody.addBodyElement(command);

        xoap::SOAPName url  = envelope.createName("url", xmas::NamespacePrefix, xmas::NamespaceUri);
        element.addAttribute(url, url_);
	
   	std::vector<std::string> names = p_.propertyNames();
        for ( std::vector<std::string>::iterator i = names.begin(); i != names.end(); i++)
 	{
	 	xoap::SOAPName item = envelope.createName ("item", xmas::NamespacePrefix, xmas::NamespaceUri);
		xoap::SOAPElement itemElement = element.addChildElement(item);

   		xoap::SOAPName name = envelope.createName("name", "", "");
   		itemElement.addAttribute(name, *i );

   		xoap::SOAPName value = envelope.createName("value", "", "");
   		itemElement.addAttribute(value, p_.getProperty(*i));
	}

	xoap::MimeHeaders* headers = request->getMimeHeaders();

	headers->addHeader("x-xdaq-tags", "heartbeat");

	return request;
}
			
toolbox::Properties & xmas::utils::Heartbeat::getProperties()
{
	return p_;
}

std::string xmas::utils::Heartbeat::getURL()
{
	return url_;
}


