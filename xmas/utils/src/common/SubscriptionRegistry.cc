// $Id: SubscriptionRegistry.cc,v 1.2 2008/07/18 15:28:37 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/utils/SubscriptionRegistry.h"
#include <sstream>

bool xmas::utils::SubscriptionRegistry::hasSubscription( const std::string& name, xdaq::ApplicationDescriptor * descriptor )
{
	std::map<std::string, std::map<xdaq::ApplicationDescriptor *, std::string> >::iterator i = subscriptions_.find(name);
	if ( i != subscriptions_.end() )
	{
		for (std::map<xdaq::ApplicationDescriptor *, std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++ )
		{
			if ( (*j).first->equals(*descriptor) )
			{
				return true;	
			}
		}
	}
	
	return false;
	
}
			
std::string xmas::utils::SubscriptionRegistry::getSubscription
	(
		const std::string& name, 
		xdaq::ApplicationDescriptor * descriptor
	) 
	throw (xmas::utils::exception::Exception )
{
	std::map<std::string, std::map<xdaq::ApplicationDescriptor *, std::string> >::iterator i = subscriptions_.find(name);
	if ( i != subscriptions_.end() )
	{
		for ( std::map<xdaq::ApplicationDescriptor *, std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++ )
		{
			if ( (*j).first->equals(*descriptor) )
			{
				return (*j).second;	
			}
		}
	}
	std::stringstream msg;
	msg << "Failed to find subscription for flashlist '" << name << "'";
	XCEPT_RAISE(xmas::utils::exception::Exception, msg.str());
	
}
			
void xmas::utils::SubscriptionRegistry::addSubscription
	(
		const std::string& name, 
		xdaq::ApplicationDescriptor * descriptor,
		const std::string & suscriptionUUID 
	)
	throw (xmas::utils::exception::Exception)
{

	std::map<std::string, std::map<xdaq::ApplicationDescriptor *, std::string> >::iterator i = subscriptions_.find(name);
	if ( i != subscriptions_.end() )
	{
		for ( std::map<xdaq::ApplicationDescriptor *, std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++ )
		{
			if ( (*j).first->equals(*descriptor) ) // found a destination for this flashlist
			{
				std::stringstream msg;
				msg << "Failed to add subscription for existing destination identifier by '" << descriptor->getUUID().toString() << "'";	
				XCEPT_RAISE(xmas::utils::exception::Exception, msg.str());	
			}
		}
	
		
		(*i).second[descriptor] = suscriptionUUID;
		
	}
	else
	{

		subscriptions_[name][descriptor] =  suscriptionUUID;
	}
}


std::map<xdaq::ApplicationDescriptor*, std::string>&
xmas::utils::SubscriptionRegistry::getSubscriptions (const std::string& name)
	throw (xmas::utils::exception::Exception )
{
	std::map<std::string, std::map<xdaq::ApplicationDescriptor *, std::string> >::iterator i = subscriptions_.find(name);
	if ( i != subscriptions_.end() )
	{
		return (*i).second;
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to get subscription for flashlist '" << name << "'";
		XCEPT_RAISE(xmas::utils::exception::Exception, msg.str());
	}	
}
			
			
void xmas::utils::SubscriptionRegistry::removeSubscription(const std::string& name, xdaq::ApplicationDescriptor * descriptor )
	throw (xmas::utils::exception::Exception)
{
	std::map<std::string, std::map<xdaq::ApplicationDescriptor *, std::string> >::iterator i = subscriptions_.find(name);
	if ( i != subscriptions_.end() )
	{
		for ( std::map<xdaq::ApplicationDescriptor *, std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++ )
		{
			if ( (*j).first->equals(*descriptor) ) // found a destination for this flashlist
			{	
				delete (*j).first;
				(*i).second.erase(j);
				if ( (*i).second.empty() ) // last emptry was removed, therefore remove also flashlist 
				{
					subscriptions_.erase(i); 
				}
				return;
			}
		}	
	}
	std::stringstream msg;
	msg << "Failed to remove subscription for flashlist '" << name << "'";
	XCEPT_RAISE(xmas::utils::exception::Exception, msg.str());
	
}

bool  xmas::utils::SubscriptionRegistry::empty()
{
	return subscriptions_.empty();
}
