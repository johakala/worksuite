// $Id: MonitorReport.cc,v 1.2 2008/07/18 15:28:37 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/utils/MonitorReport.h"
#include <string>
#include <sstream>
#include <map>
#include "xdata/Table.h"
#include "xmas/exception/Exception.h"
#include "xmas/FlashListDefinition.h"
			
xmas::utils::MonitorReport::MonitorReport( xmas::FlashListDefinition * definition )
	:flashListDefinition_(definition)
{
	//std::cout << "CTOR of MonitorReport for flashlist: " << flashListDefinition_->getProperty("name") << std::endl;
}

/* Destructor of the report deletes the tables that have passed to it */
xmas::utils::MonitorReport::~MonitorReport()
{
	//std::cout << "DTOR of MonitorReport for flashlist: " << flashListDefinition_->getProperty("name") << std::endl;
	
}

std::string xmas::utils::MonitorReport::getFlashListName()
{
	return flashListDefinition_->getProperty("name");
}

std::map<std::string, xdata::Table::Reference> xmas::utils::MonitorReport::getMetrics()
{
	return metrics_;
}

void xmas::utils::MonitorReport::addMetrics (const std::string& name, xdata::Table::Reference & table)
	throw (xmas::exception::Exception)
{
	if (metrics_.find(name) == metrics_.end())
	{
		metrics_[name] = table;
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to add already existing metrics '" << name << "'";
		XCEPT_RAISE (xmas::exception::Exception, msg.str());
	}
}

xmas::FlashListDefinition * xmas::utils::MonitorReport::getFlashListDefinition()
{
	return flashListDefinition_;
}




