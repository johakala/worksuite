// $Id: MonitorSettings.cc,v 1.8 2008/07/18 15:28:37 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/MonitorSettings.h"
#include "xoap/domutils.h"

xmas::MonitorSettings::MonitorSettings(DOMNode* monitorNode) throw (xmas::exception::Exception)
{	
	flashListDefinition_ = 0; // imnportant, initialize to null pointer
	
	std::string group = xoap::getNodeAttribute(monitorNode, "group");
			
	this->setProperty("group",group);
	
	try
	{
		// Retrieve <sampler> nodes
		DOMNodeList* samples = monitorNode->getChildNodes();
		for (XMLSize_t j = 0; j < samples->getLength(); j++)
		{
			DOMNode* node = samples->item(j);
			if ((node->getNodeType() == DOMNode::ELEMENT_NODE) 
				&& ( xoap::XMLCh2String(node->getLocalName()) == "sampler"))
			{
				this->createSamplerSettings(node);								
			}
			else if ((node->getNodeType() == DOMNode::ELEMENT_NODE) 
				&& (xoap::XMLCh2String(node->getLocalName()) == "flash"))
			{
				if (flashListDefinition_ == 0)
				{
					flashListDefinition_ = new xmas::FlashListDefinition(node);
				}
				else
				{
					std::stringstream msg;
					msg << "Multiple flashlist definition nodes in monitor settings not allowed";
					XCEPT_RAISE (xmas::exception::Exception, msg.str());
				}				
			}
		}
	}
	catch (xmas::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create monitor settings";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
}

xmas::MonitorSettings::~MonitorSettings()
{
	for (std::map<toolbox::net::UUID, xmas::SamplerSettings*>::iterator i = samplerSettings_.begin(); i != samplerSettings_.end(); i++ )
	{
		delete (*i).second;
	}
}

toolbox::net::UUID& xmas::MonitorSettings::getId()
{
	return id_;
}

xmas::SamplerSettings * xmas::MonitorSettings::createSamplerSettings(DOMNode* sampleNode)
throw (xmas::exception::Exception)
{
	try
	{
		
		xmas::SamplerSettings* sample = new xmas::SamplerSettings(sampleNode);
		// toolbox::net::UUID uuid;
		// samplerSettings_[uuid] = sample;
		
		samplerSettings_[sample->getId()] = sample;
		// samplerSettings_.insert (std::make_pair (sample->getId(), sample));
		return sample;
	}
	catch (xmas::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create sample configuration";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
}

xmas::SamplerSettings * xmas::MonitorSettings::createSamplerSettings()
throw (xmas::exception::Exception)
{
	try
	{
		xmas::SamplerSettings* sample = new xmas::SamplerSettings();
		samplerSettings_[sample->getId()] = sample;
		return sample;
	}
	catch (xmas::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create sample configuration";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
}

void xmas::MonitorSettings::removeSamplerSettings(toolbox::net::UUID & id)  throw (xmas::exception::Exception)
{
	std::map<toolbox::net::UUID, xmas::SamplerSettings*>::iterator i = samplerSettings_.find(id);
	if ( i != samplerSettings_.end() )
	{
		delete (*i).second;
		samplerSettings_.erase(i);
	}
	else
	{
		std::stringstream msg;
		msg << "Cannot remove sampler for id '" << id.toString() << "'";
		XCEPT_RAISE(xmas::exception::Exception, msg.str());
	}
}

xmas::SamplerSettings * xmas::MonitorSettings::getSamplerSettings(toolbox::net::UUID & id) throw (xmas::exception::Exception)
{
	std::map<toolbox::net::UUID, xmas::SamplerSettings*>::iterator i = samplerSettings_.find(id);
	if ( i != samplerSettings_.end() )
	{
		return (*i).second;
	}
	else
	{
		std::stringstream msg;
		msg << "Cannot retrieve sampler for id '" << id.toString() << "'";
		XCEPT_RAISE(xmas::exception::Exception, msg.str());
	}
}

std::vector<xmas::SamplerSettings*> xmas::MonitorSettings::getSamplerSettings()
{
	std::vector<xmas::SamplerSettings*> v;
	for (std::map<toolbox::net::UUID, xmas::SamplerSettings*>::iterator i = samplerSettings_.begin(); i != samplerSettings_.end(); i++ )
	{
		v.push_back( (*i).second );

	}
	return v;
}

xmas::FlashListDefinition * xmas::MonitorSettings::getFlashListDefinition()
{
	return flashListDefinition_;
}

