// $Id: PulserEvent.cc,v 1.6 2008/07/18 15:28:37 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/PulserEvent.h"
#include "xoap/domutils.h"
#include <sstream>

xmas::PulserEvent::PulserEvent()
{
	this->reset();
}

xmas::PulserEvent::PulserEvent(DOMNode* node)
	throw (xmas::exception::Exception)
{	
	std::string start_time = xoap::getNodeAttribute(node, "start");
	std::string start_delay = xoap::getNodeAttribute(node, "delay");
	std::string period = xoap::getNodeAttribute(node, "period");
	std::string name = xoap::getNodeAttribute(node, "name");
			
	this->setProperty("start",start_time);
	this->setProperty("delay",start_delay);
	this->setProperty("period",period);
	this->setProperty("name",name);
	
	try
	{
		// Retrieve <flash> nodes
		DOMNodeList* list = node->getChildNodes();
		for (XMLSize_t j = 0; j < list->getLength(); j++)
		{
			DOMNode* flash = list->item(j);
			if ((flash->getNodeType() == DOMNode::ELEMENT_NODE) 
				&& ( xoap::XMLCh2String(flash->getLocalName()) == "flash"))
			{
				std::string tag = xoap::getNodeAttribute(flash, "tag");
				std::string xlinkType = xoap::getNodeAttribute(flash, "xlink:type");
				std::string xlinkHref = xoap::getNodeAttribute(flash, "xlink:href");
				
				if ((xlinkType == "") || (xlinkHref == ""))
				{
					std::stringstream msg;
					msg << "Failed to get flashlist information, one of xlink type of href is empty";
					XCEPT_RAISE (xmas::exception::Exception, msg.str());
				}
				else
				{
					// currently only store the flashlist name of a local flashlist definition
					if (xlinkHref.find ("#urn:") != std::string::npos)
					{
						flashLists_[xlinkHref.substr(1)] = tag;
					}
					else
					{
						std::stringstream msg;
						msg << "xlink resolution of flash lists not yet supported'";
						msg << xlinkHref << "'";
						XCEPT_RAISE (xmas::exception::Exception, msg.str());
					}				
				}
			}
		}
	}
	catch (xmas::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create pulser event settings";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
	
	this->reset();
}

xmas::PulserEvent::~PulserEvent()
{
}

std::map<std::string, std::string>& xmas::PulserEvent::getSamples()
{
	return flashLists_;
}

toolbox::net::UUID& xmas::PulserEvent::getId()
{
	return id_;
}

bool xmas::PulserEvent::hasPeriod()
{
	return ( this->getProperty("period") != "");
}


toolbox::TimeVal xmas::PulserEvent::getStartTime()  throw (xmas::exception::Exception)
{
	try
	{
	
		if ( this->getProperty("start") != "" )
		{
			toolbox::TimeVal start;
			start.fromString(this->getProperty("start"),"", toolbox::TimeVal::gmt);
			return start;	
		}
		else if ( this->getProperty("delay") != "" )
		{
			toolbox::TimeInterval interval;
			interval.fromString(this->getProperty("delay"));
			return toolbox::TimeVal::gettimeofday() + interval;
		}
		else
		{
			return toolbox::TimeVal::gettimeofday();
		}
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW(xmas::exception::Exception, "failed to convert start/delay time for pulser event", e);
	}
}

toolbox::TimeInterval xmas::PulserEvent::getPeriod() throw (xmas::exception::Exception)
{	
	if ( this->getProperty("period") != "" )
	{

		try
		{
			toolbox::TimeInterval interval;
			interval.fromString(this->getProperty("period"));
			return  interval;
		}
		catch (toolbox::exception::Exception & e)
		{
			XCEPT_RETHROW(xmas::exception::Exception, "failed to convert period time for pulser event", e);
		}	
	}
	else
	{
		XCEPT_RAISE(xmas::exception::Exception, "cannot retrieve period time for pulser event");
	}	
}
		
size_t xmas::PulserEvent::getPulseCount()
{
	return pulseCount_;
}	

void xmas::PulserEvent::update()
{
	++pulseCount_;
	lastPulseTime_ = toolbox::TimeVal::gettimeofday();
}	


void xmas::PulserEvent::reset()
{
	pulseCount_ = 0;
	lastPulseTime_ = toolbox::TimeVal::zero();
}	

toolbox::TimeVal& xmas::PulserEvent::getLastPulseTime()
{
	return lastPulseTime_;
}
