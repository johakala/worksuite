// $Id: PulserSettings.cc,v 1.5 2008/07/18 15:28:37 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/PulserSettings.h"
#include "xoap/domutils.h"
#include <sstream>
#include "xmas/xmas.h"

xmas::PulserSettings::PulserSettings()
	: mutex_(toolbox::BSem::FULL, true)
{	

}

void xmas::PulserSettings::lock()
{	
	mutex_.take();
}

void xmas::PulserSettings::unlock()
{	
	mutex_.give();
}

void xmas::PulserSettings::addEvents(DOMDocument * document) throw (xmas::exception::Exception)
{
	try
	{
		DOMNodeList* list = document->getElementsByTagNameNS(xoap::XStr(xmas::NamespaceUri),xoap::XStr("event") );
	
		for (XMLSize_t j = 0; j < list->getLength(); j++)
		{
			DOMNode* event = list->item(j);
			{
				this->createPulserEvent(event);								
			}
		}
	}
	catch (xmas::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create pulser event";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}

}


xmas::PulserSettings::~PulserSettings()
{
	std::map<toolbox::net::UUID, PulserEvent*>::iterator i;
	for (i = settings_.begin(); i != settings_.end(); ++i)
	{
		delete (*i).second;
	}
}

bool xmas::PulserSettings::hasPulserEvent(toolbox::net::UUID& id)
{
	return (settings_.find(id) != settings_.end());
}

xmas::PulserEvent* xmas::PulserSettings::getPulserEvent(toolbox::net::UUID& id)
throw (xmas::exception::Exception)
{
	std::map<toolbox::net::UUID, PulserEvent*>::iterator i = settings_.find(id);
	if (i != settings_.end())
	{
		return (*i).second;
	}
	else
	{
		std::stringstream msg;
		msg << "Pulser event for id '" << id.toString() << "' not found";
		XCEPT_RAISE (xmas::exception::Exception, msg.str());
	}
}

toolbox::net::UUID xmas::PulserSettings::createPulserEvent(DOMNode* node)
throw (xmas::exception::Exception)
{
	try
	{
		xmas::PulserEvent* p = new xmas::PulserEvent(node);
		settings_[p->getId()] = p;
		return p->getId();
	}
	catch (xmas::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create pulser event";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
}

toolbox::net::UUID xmas::PulserSettings::createPulserEvent()
	throw (xmas::exception::Exception)
{
	try
	{
		xmas::PulserEvent* p = new xmas::PulserEvent();
		settings_[p->getId()] = p;
		return p->getId();
	}
	catch (xmas::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create pulser event";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
}

void xmas::PulserSettings::removePulserEvent(toolbox::net::UUID & id)  throw (xmas::exception::Exception)
{
	std::map<toolbox::net::UUID, xmas::PulserEvent*>::iterator i = settings_.find(id);
	if ( i != settings_.end() )
	{
		delete (*i).second;
		settings_.erase(i);
	}
	else
	{
		std::stringstream msg;
		msg << "Cannot remove pulser event id '" << id.toString() << "'";
		XCEPT_RAISE(xmas::exception::Exception, msg.str());
	}
}


std::map<toolbox::net::UUID, xmas::PulserEvent*>& xmas::PulserSettings::getPulserEvents()
{
	return settings_;
}

