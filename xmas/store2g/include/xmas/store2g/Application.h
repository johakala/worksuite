// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_store2g_Application_h_
#define _xmas_store2g_Application_h_

#include <string>
#include <map>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "toolbox/ActionListener.h"
#include "toolbox/task/AsynchronousEventDispatcher.h"

#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdata/UnsignedInteger64.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Bag.h"
#include "xdata/ActionListener.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xmas/exception/Exception.h"

#include "xdaq/ContextTable.h"

#include "xmas/utils/MonitorReport.h"
#include "xmas/utils/exception/Exception.h"
#include "xmas/store2g/exception/Exception.h"
#include "xmas/store2g/DescriptorsCache.h"
#include "xmas/store2g/Repository.h"
#include "xmas/store2g/Settings.h"
#include "xmas/store2g/Stager.h"
#include "xmas/store2g/TStoreProxy.h"

#include "b2in/nub/Method.h"
#include "b2in/utils/MessengerCache.h"
#include "b2in/utils/ServiceProxy.h"

namespace xmas
{
	namespace store2g
	{
		class Application : public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public b2in::utils::MessengerCacheListener, public toolbox::task::TimerListener
		{
			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
				~Application ();

				void actionPerformed (xdata::Event& e);

				void actionPerformed (toolbox::Event& event);

				void timeExpired (toolbox::task::TimerEvent& e);

				//
				// XGI Interface
				//
				void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				void TabPanel (xgi::Output * out);
				void SettingsTabPage (xgi::Output * out);
				void MetricsTabPage (xgi::Output * out);
				void FlashlistsTabPage (xgi::Output * out);
				void ServicesTabPage (xgi::Output * out);
				void SummaryTabPage (xgi::Output * out);

				/*! Apply settings:
				 */
				void applySettings (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				/*! Clear exception history
				 */
				void clear (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				//
				// B2IN interface
				//
				void onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) throw (b2in::nub::exception::Exception);

				void asynchronousExceptionNotification (xcept::Exception & e);

			protected:
				void loadOCL(const std::string & fname);

				void loadSettings (const std::string& path) throw (xmas::store2g::exception::Exception);

				void subscribeToDiscoveryServices () throw (xmas::store2g::exception::Exception);

				void refreshSubscriptionsToEventing () throw (xmas::store2g::exception::Exception);

			private:

				bool checkFlashlist (const std::string & flashListName, const std::string & version);

				xdata::UnsignedInteger64T reportLostCounter_;
				toolbox::Properties settings_;	 // hyperdaq settings

				xmas::store2g::DescriptorsCache descriptorsCache_;
				xmas::store2g::Repository repository_;
				xmas::store2g::Settings storeSettings_;
				xmas::store2g::TStoreProxy tstoreProxy_;
				xmas::store2g::Stager stager_;

				//
				// Application parameters
				//
				xdata::String subscribeGroup_;    // one or more comma separated groups hosting a ws-eventing service for monitoring
				xdata::String storeGroup_;      // one or more comma separated groups hosting a tstore service for storage
				xdata::String storeView_;      // view for tstore service for storage
				xdata::String period_;            // storage period
				xdata::String session_;           // either 'startup' or 'id'
				xdata::Vector<xdata::String> settingsURLs_;

				xdata::String authentication_;
				xdata::String credentials_;
				xdata::String db_;
				xdata::String oclFileName_;

				xdata::String subscribeExpiration_;

				b2in::utils::ServiceProxy *b2inEventingProxy_;

				std::map<std::string, xdata::Properties> subscriptions_; // indexed by topic
		};
	}
}

#endif

