// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_store2g_Repository_h_
#define _xmas_store2g_Repository_h_

#include <vector>
#include <map>
#include <string>
#include "xmas/store2g/exception/NoData.h"
#include "xmas/store2g/exception/FailedToInsert.h"
#include "xmas/store2g/exception/FailedToClear.h"

#include "xdata/Table.h"
#include "toolbox/BSem.h"
#include "toolbox/Properties.h"

namespace xmas
{
	namespace store2g 
	{	
		class Repository
		{
			public:

			Repository();
			~Repository();

			//! Add a \param table to a \param flashlist entry.
			//
			void add ( const std::string& flashlist, const std::string& originator,  xdata::Table::Reference& table )
				throw (xmas::store2g::exception::FailedToInsert);

			//! Clear all data of a \param flashlist entry
			//
			void clear ( const std::string& flashlist ) 
				throw (xmas::store2g::exception::FailedToClear);

			//! Clear the whole repository
			//
			void clear() throw (xmas::store2g::exception::FailedToClear);

			//! Retrieve data for a \param flashlist entry
			//
			xdata::Table::Reference getData ( const std::string& flashlist ) 
				throw (xmas::store2g::exception::NoData);
			
			toolbox::Properties getProperties ( const std::string& flashlist ) 
				throw (xmas::store2g::exception::NoData);
				
			//! Retrieve a list of all flashlists for which data is store2gd
			//
			std::set<std::string> getFlashlists();
			
			//! Lock repository
			//
			void lock();
			
			//! Unlock repository
			//
			void unlock();

			protected:

			std::map<std::string, xdata::Table::Reference> tables_;
			std::map<std::string, toolbox::Properties> properties_;
			toolbox::BSem mutex_;
		};	
	}
}

#endif

