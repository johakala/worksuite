// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_store2g_Settings_h_
#define _xmas_store2g_Settings_h_

#include <list>
#include <map>
#include <string>
#include "xmas/FlashListDefinition.h"
#include "xmas/store2g/exception/Exception.h"
#include "toolbox/Properties.h"
#include "toolbox/net/UUID.h"
#include "toolbox/TimeVal.h"


namespace xmas
{
	namespace store2g
	{
		class Settings
		{
			public:
			
			typedef struct
			{
				std::string tag;
				std::string mode;
				bool enable;
				xmas::FlashListDefinition* definition;
				size_t depth;
				bool validated;
				bool failed;
				// statistics
				size_t incomingReports;
				size_t insertActions;
				size_t totalRowsReceived;
				size_t totalRowsInserted;
				toolbox::TimeVal lastInsert;
			} Properties;
			
			Settings();
		 	~Settings();
			
			void add(DOMDocument * document) 
				throw (xmas::store2g::exception::Exception);


			xmas::FlashListDefinition*  getFlashListDefinition(const std::string & name)
				throw (xmas::store2g::exception::Exception);
				
			std::list<std::string>  getFlashlistsNames();
			
			xmas::store2g::Settings::Properties & getProperties(const std::string & name)
				throw (xmas::store2g::exception::Exception);
						
			bool hasFlashList(const std::string & name);
			
			
			protected:
			
			void createItem(DOMNode* node)
				throw (xmas::store2g::exception::Exception);
			
			

			std::map<std::string, xmas::store2g::Settings::Properties > settings_;
		};
	}
}

#endif

