// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_store2g_exception_IncompatibleDefinition_h_
#define _xmas_store2g_exception_IncompatibleDefinition_h_

#include "xmas/store2g/exception/Exception.h"

namespace xmas {
	namespace store2g {
		namespace exception { 
			class IncompatibleDefinition: public xmas::store2g::exception::Exception 
			{
				public: 
				IncompatibleDefinition( std::string name, std::string message, std::string module, int line, std::string function ): 
					xmas::store2g::exception::Exception(name, message, module, line, function) 
				{} 

				IncompatibleDefinition( std::string name, std::string message, std::string module, int line, std::string function,
					xcept::Exception& e ): 
					xmas::store2g::exception::Exception(name, message, module, line, function, e) 
				{} 

			};  
		}
	}		
}

#endif

