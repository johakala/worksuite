// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and d. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xmas_store2g_version_h_
#define _xmas_store2g_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define XMASSTORE2G_VERSION_MAJOR 2
#define XMASSTORE2G_VERSION_MINOR 1
#define XMASSTORE2G_VERSION_PATCH 5
// If any previous versions available E.g. #define XMASSTORE2G_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define XMASSTORE2G_PREVIOUS_VERSIONS "2.0.0,2.0.1,2.0.2,2.1.0,2.1.1,2.1.2,2.1.3,2.1.4"


//
// Template macros
//
#define XMASSTORE2G_VERSION_CODE PACKAGE_VERSION_CODE(XMASSTORE2G_VERSION_MAJOR,XMASSTORE2G_VERSION_MINOR,XMASSTORE2G_VERSION_PATCH)
#ifndef XMASSTORE2G_PREVIOUS_VERSIONS
#define XMASSTORE2G_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(XMASSTORE2G_VERSION_MAJOR,XMASSTORE2G_VERSION_MINOR,XMASSTORE2G_VERSION_PATCH)
#else 
#define XMASSTORE2G_FULL_VERSION_LIST  XMASSTORE2G_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(XMASSTORE2G_VERSION_MAJOR,XMASSTORE2G_VERSION_MINOR,XMASSTORE2G_VERSION_PATCH)
#endif 

namespace xmasstore2g
{
	const std::string package  =  "xmasstore2g";
	const std::string versions =  XMASSTORE2G_FULL_VERSION_LIST;
	const std::string description = "This package provide facilities to automatically make monitoring data persistent";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "An service used to store2g monitor data injected into the XMAS system";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();

}

#endif

