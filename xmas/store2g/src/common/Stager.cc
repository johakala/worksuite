// $Id: Stager.cc,v 1.11 2007/12/07 15:33:45 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "xmas/store2g/Stager.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerFactory.h"
#include "xcept/tools.h"


xmas::store2g::Stager::Stager(xdaq::Application * owner, xmas::store2g::Repository * repository, xmas::store2g::TStoreProxy * proxy,
	xmas::store2g::Settings * storeSettings)
	: xdaq::Object(owner), repository_(repository), proxy_(proxy), storeSettings_(storeSettings)
{
	if (!toolbox::task::getTimerFactory()->hasTimer("stager"))
	{
		(void) toolbox::task::getTimerFactory()->createTimer("stager");
	} 
	

	
}

void xmas::store2g::Stager::enable(toolbox::TimeInterval & interval, const std::string& storeGroup, const std::string& storeView, const std::string& database ) throw (xmas::store2g::exception::Exception)
{
	storeGroup_ = storeGroup;
	storeView_ = storeView;
	database_ = database;
	
	// submit task
	try
	{
		toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer("stager");
        	toolbox::TimeVal start;
        	start = toolbox::TimeVal::gettimeofday();
        	timer->scheduleAtFixedRate( start, this, interval, 0, "stager" );
		toolbox::TimeInterval renewInterval(120,0);  
        	timer->scheduleAtFixedRate( start, this, renewInterval, 0, "renew" );
		
	}		
	catch (toolbox::task::exception::InvalidListener& e)
	{
		XCEPT_RETHROW (xmas::store2g::exception::Exception, "Failed to enable stager", e);
	}
	catch (toolbox::task::exception::InvalidSubmission& e)
	{
		XCEPT_RETHROW (xmas::store2g::exception::Exception, "Failed to enable stager", e);

	}
	catch (toolbox::task::exception::NotActive& e)
	{
		XCEPT_RETHROW (xmas::store2g::exception::Exception, "Failed to enable stager", e);

	}
	catch (toolbox::exception::Exception& e)
	{
		XCEPT_RETHROW (xmas::store2g::exception::Exception, "Failed to enable stager", e);

	}	
}

void xmas::store2g::Stager::disable() throw (xmas::store2g::exception::Exception)
{
}

void xmas::store2g::Stager::timeExpired(toolbox::task::TimerEvent& e)
{


	if ( proxy_->discoveredTStores(storeGroup_) == 0 )
	{
		std::stringstream msg;
		msg << "tstore for group '" <<  storeGroup_ << "' not found";
		XCEPT_DECLARE(xmas::store2g::exception::Exception, e, msg.str() );
		LOG4CPLUS_DEBUG( this->getOwnerApplication()->getApplicationLogger(),msg.str() );
		this->getOwnerApplication()->notifyQualified("warning",e);
		return;
	
	}

    if (e.getTimerTask()->name == "renew")
    {
        try
        {
            proxy_->renewConnection(storeGroup_);
        }
        catch (xmas::store2g::exception::Exception& e)
        {
            LOG4CPLUS_DEBUG( this->getOwnerApplication()->getApplicationLogger(),xcept::stdformat_exception_history(e));
            this->getOwnerApplication()->notifyQualified("fatal",e);
	    repository_->lock();
	    try
	    {
	    	proxy_->connect (storeGroup_,storeView_); 
		std::set<std::string> flashlists = repository_->getFlashlists();
	        for (std::set<std::string>::iterator i = flashlists.begin(); i != flashlists.end(); ++i)
        	{
			xmas::store2g::Settings::Properties & properties = storeSettings_->getProperties(*i);
			properties.failed = false;
		}
	    }
	    catch (xmas::store2g::exception::Exception& e)
	    {
		this->getOwnerApplication()->notifyQualified("error",e);
	    }
	    repository_->unlock();
        }

        return;
    }

	repository_->lock();
		
	std::set<std::string> flashlists = repository_->getFlashlists();
	
	for (std::set<std::string>::iterator i = flashlists.begin(); i != flashlists.end(); ++i)
	{
		xdata::Table::Reference table = repository_->getData ( *i ); 
		
		xmas::store2g::Settings::Properties & properties = storeSettings_->getProperties(*i);
		
		if ( ! properties.enable )
			continue;
		
		if ( properties.failed )
			continue;	
		
		
		try
		{
			proxy_->insert (properties,table, storeGroup_, storeView_, database_); // key is empty string
			properties.totalRowsInserted +=  table->getRowCount();
			properties.insertActions++;
			properties.lastInsert = e.getTimerTask()->lastExecutionTime;
			repository_->clear ( *i );
		}
		catch (xmas::store2g::exception::Exception& e)
		{
			std::vector<xcept::ExceptionInformation> & history = e.getHistory();
        		std::vector<xcept::ExceptionInformation>::iterator j = history.begin();
			std::string identifier = (*j).getProperty("identifier");
			std::string message = (*j).getProperty("message");
                	if ((identifier ==  "tstore::exception::Exception" ) && ( message.find("ORA-00001") != std::string::npos ))
			{
				// unique constraint violated therefore clear current data and let the system go
				LOG4CPLUS_DEBUG( this->getOwnerApplication()->getApplicationLogger(),xcept::stdformat_exception_history(e));
				this->getOwnerApplication()->notifyQualified("fatal",e);
				repository_->clear ( *i );

			}
			else
			{
				LOG4CPLUS_DEBUG( this->getOwnerApplication()->getApplicationLogger(),xcept::stdformat_exception_history(e));
				this->getOwnerApplication()->notifyQualified("fatal",e);
				properties.failed = true; // disable so that stager will ignore this flaslist
				// no way to continue after this, whatever the problem connection is closed and let the xmas store attempt to re-connect at renew time
				try
                		{
						proxy_->closeConnection(storeGroup_);
				}
				catch (xmas::store2g::exception::Exception& e)
                		{
					LOG4CPLUS_DEBUG( this->getOwnerApplication()->getApplicationLogger(),xcept::stdformat_exception_history(e));
					this->getOwnerApplication()->notifyQualified("error",e);
				}
			}
		}
	}	
	
	// repository_->clear();
	
	repository_->unlock();
}
