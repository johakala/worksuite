// $Id: DescriptorsCache.cc,v 1.1 2007/08/10 14:58:07 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xcept/tools.h"
#include "toolbox/stl.h"

#include "xmas/store2g/DescriptorsCache.h" 
#include "xmas/store2g/exception/Exception.h" 

#include "xplore/Interface.h"
#include "xplore/exception/Exception.h"
#include "xplore/DiscoveryEvent.h"
 
xmas::store2g::DescriptorsCache::DescriptorsCache(xdaq::Application* owner): xdaq::Object(owner)
	, mutex_(toolbox::BSem::FULL)
{


}


void xmas::store2g::DescriptorsCache::actionPerformed( toolbox::Event& event) 
{
		xplore::DiscoveryEvent& de = dynamic_cast<xplore::DiscoveryEvent&>(event);
		std::vector<xplore::Advertisement::Reference> & resultSet = de.getResultSet();
		xplore::Interface * interface = dynamic_cast<xplore::Interface*> (this->getOwnerApplication()->getApplicationContext()->getFirstApplication("xplore::Application"));
	
		mutex_.take();
		
		for (std::list<xdaq::ApplicationDescriptor*>::iterator i = services_.begin(); 
			i != services_.end(); i++ )
		{
			delete (*i);
		}

		services_.clear();
		std::vector<xplore::Advertisement::Reference>::iterator vi;
		for (vi = resultSet.begin(); vi != resultSet.end(); ++vi)
		{

			toolbox::net::URL url((*vi)->getURL());
			std::string contextURL = url.getScheme() + "://" + url.getAuthority();

			//std::cout << "WS-Eventing Context URL: " << contextURL << std::endl;

			toolbox::Properties p;
			try
			{
				interface->retrieveProperties("service:peer:" + (*vi)->getURL(), p);
			}
			catch (xdaq::exception::Exception& e)
			{
				std::stringstream msg; 
				msg <<  "Failed to retrieve peer properties";
				XCEPT_DECLARE(xmas::store2g::exception::Exception, e, msg.str());
				this->getOwnerApplication()->notifyQualified("error",e);
				continue;	
			}

			const xdaq::ContextDescriptor* cd = 0;
			if (discoveredContexts_.hasContextDescriptor ( contextURL ) )
			{
				cd = discoveredContexts_.getContextDescriptor(contextURL  );
			}
			else
			{
				cd = discoveredContexts_.createContextDescriptor(contextURL );
			}

			xdaq::ApplicationDescriptorImpl* descriptor;
			try
			{
				//std::cout << "Create descriptor: "  << std::endl;
				descriptor =  new xdaq::ApplicationDescriptorImpl(cd, "", 0, "");
			}
			catch(xdaq::exception::Exception & e )
			{
				LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(),xcept::stdformat_exception_history(e));
				continue;
			}

			try
			{
				descriptor->setAttributes(p);
				services_.push_back(descriptor);

				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(),"created descriptor for service at: " << url.toString());
			}
			catch(xdaq::exception::Exception & e )
			{
				delete descriptor;
				LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(),xcept::stdformat_exception_history(e));
			}				
		}
		mutex_.give();
		
		
}
std::list<xdaq::ApplicationDescriptorImpl>  xmas::store2g::DescriptorsCache::getDescriptors(std::set<std::string>& groups, const std::string & service) 
{
	std::list<xdaq::ApplicationDescriptorImpl> destinations;

	mutex_.take();
	
	std::list<xdaq::ApplicationDescriptor*>::iterator i;
	for ( i = services_.begin(); i != services_.end(); ++i)
	{
		std::set<std::string> descriptorGroups = toolbox::parseTokenSet ( (*i)->getAttribute("group"), "," );
		if ( ! toolbox::stl::intersection(descriptorGroups, groups).empty() )
		{
			if ( (*i)->getAttribute("service") == service )
			{
				destinations.push_back ( *(dynamic_cast<xdaq::ApplicationDescriptorImpl*>(*i)) );
			}
		}
	}

	mutex_.give();
	
	return 	destinations;
}	
