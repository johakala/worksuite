// $Id: MonitorReportEvent.cc,v 1.2 2008/07/18 15:28:34 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "es/xtreme/Event.h"
#include <string>
#include <sstream>
#include <map>
#include "xdata/Table.h"
#include "xmas/exception/Exception.h"
#include "xmas/FlashListDefinition.h"
			
es::xtreme::Event::Event( xdata::Properties & plist, toolbox::mem::Reference * ref )
	: toolbox::Event("urn:es-xtreme:Event", 0), plist_(plist), ref_(ref)
{
}
