// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			              				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, P. Roberts							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		               		         *
 * For the list of contributors see CREDITS.   			   			     *
 *************************************************************************/

#include "es/xtreme/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xoap/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xgi/version.h"
#include "b2in/utils/version.h"
#include "xmas/utils/version.h"

GETPACKAGEINFO(esxtreme)

void esxtreme::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);
	CHECKDEPENDENCY(xcept);
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xdata); 
	CHECKDEPENDENCY(xoap);
	CHECKDEPENDENCY(xdaq); 
	CHECKDEPENDENCY(xgi);  
	CHECKDEPENDENCY(b2inutils); 
}

std::set<std::string, std::less<std::string> > esxtreme::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config);
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,toolbox); 
	ADDDEPENDENCY(dependencies,xdata); 
	ADDDEPENDENCY(dependencies,xoap);
	ADDDEPENDENCY(dependencies,xdaq);  
	ADDDEPENDENCY(dependencies,xgi);
	ADDDEPENDENCY(dependencies,b2inutils); 
	ADDDEPENDENCY(dependencies,xmasutils);

	return dependencies;
}	
	
