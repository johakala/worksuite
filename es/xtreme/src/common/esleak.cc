#include "es/api/Member.h"
#include "es/api/Cluster.h"

#include "xcept/tools.h"

#include "toolbox/Properties.h"
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>


#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"
#include "toolbox/net/URN.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h"

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationRegistry.h"

#include "xgi/Table.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"

#include "es/xtreme/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/xmas.h"
#include "es/xtreme/exception/Exception.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xmas/exception/Exception.h"

#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/InputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/TimeVal.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdata/TableIterator.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include "xoap/DOMParserFactory.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

#include <stdio.h>
#include <curl/curl.h>

#include "es/api/Cluster.h"
#include "es/api/Stream.h"

std::set<std::string>  hkey_;
xdata::exdr::Serializer serializer_;


json_t * tableRowToJSON(xdata::Table::iterator & ti, std::vector<std::string> & columns, const std::string & name) throw (es::xtreme::exception::Exception)
{
	json_t * document = json_object();

	for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
	{

		//std::string localName = columns[k].substr(columns[k].rfind(":")+1);
		std::string localName = columns[k];

		//std::cout << "->>>>" << localName << std::endl;

		//std::cout << "Printing the column : " << columns[k] << std::endl;
		//xdata::Serializable * s = t->getValueAt(j, columns[k]);

		xdata::Serializable * s = (*ti).getField(columns[k]);

		if (s->type() == "string")
		{
			std::string value = s->toString();
			json_object_set_new( document, localName.c_str(), json_string( value.c_str()) );
		}
		else if (s->type() == "bool")
		{
			xdata::Boolean* b = dynamic_cast<xdata::Boolean*>(s);
			json_object_set_new( document, localName.c_str(), json_boolean((xdata::BooleanT)*b) );
		}
		else if ( s->type() == "int" )
		{
			xdata::Integer* i = dynamic_cast<xdata::Integer*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::IntegerT)*i) );
		}
		else if ( s->type() == "unsigned int" )
		{
			xdata::UnsignedInteger* i = dynamic_cast<xdata::UnsignedInteger*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedIntegerT)*i) );
		}
		else if ( s->type() == "unsigned int 32" )
		{
			xdata::UnsignedInteger32* i = dynamic_cast<xdata::UnsignedInteger32*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger32T)*i) );
		}
		else if ( s->type() == "unsigned int 64" )
		{
			xdata::UnsignedInteger64* i = dynamic_cast<xdata::UnsignedInteger64*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger64T)*i) );
		}
		else if ( s->type() == "unsigned long" )
		{
			xdata::UnsignedLong* i = dynamic_cast<xdata::UnsignedLong*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedLongT)*i) );
		}
		else if ( s->type() == "unsigned short" )
		{
			xdata::UnsignedShort* i = dynamic_cast<xdata::UnsignedShort*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedShortT)*i) );
		}
		//json_integer needs to change!
		else if ( s->type() == "time" )
		{
			xdata::TimeVal* i = dynamic_cast<xdata::TimeVal*>(s);
			std::string formattedTime = i->value_.toString("%FT%H:%M:%SZ", toolbox::TimeVal::gmt);
			json_object_set_new( document, localName.c_str(), json_string(formattedTime.c_str()) );
		}
		else if ( s->type() == "double" )
		{
			xdata::Double* i = dynamic_cast<xdata::Double*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::DoubleT)*i) );
		}
		else if (s->type().find("vector") != std::string::npos )
		{
			xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(s);

			if ( v->getElementType() == "unsigned int 32" )
			{
				xdata::Vector<xdata::UnsignedInteger32> * v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(s);

				json_t * jsonvector = json_array();
				for ( xdata::Vector<xdata::UnsignedInteger32>::iterator i = v->begin(); i != v->end(); i++ )
				{
					json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger32T)*i) );
					json_t * val = json_integer(((xdata::UnsignedInteger32T)*i));
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(),jsonvector);
				//vector treated as primitive type
			}
			else if (  v->getElementType()  == "unsigned int 64" )
			{
				xdata::Vector<xdata::UnsignedInteger64> * v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>(s);
				json_t * jsonvector = json_array();
				for ( xdata::Vector<xdata::UnsignedInteger64>::iterator i = v->begin(); i != v->end(); i++ )
				{
					json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger64T)*i) );
					json_t * val = json_integer(((xdata::UnsignedInteger64T)*i));
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(),jsonvector);
				//vector treated as primitive type
			}
			else
			{
				XCEPT_RAISE (es::xtreme::exception::Exception, "Failed to convert data entry,  unsupported vector type as "+ v->getElementType()  );
			}
		}
		else if (s->type() == "table")
		{
			xdata::Table * st = dynamic_cast<xdata::Table*>(s);
			std::vector<std::string> cols = st->getColumns();
			json_t * jsontable = json_array();
			for (xdata::Table::iterator sti = st->begin(); sti != st->end(); sti++)
			{
				try
				{
					json_t * jsonrow  = tableRowToJSON( sti, cols, "");
				//tablename

					json_array_append_new(jsontable, jsonrow);
				}
				catch(es::xtreme::exception::Exception & e)
				{
					json_decref(document);
					XCEPT_RETHROW (es::xtreme::exception::Exception, "Failed to convert inner data table,  xdata to es type as "+s->type(), e );
				}

			}
			json_object_set_new(document, localName.c_str(),jsontable);
		}
		else
		{
			json_decref(document);
			XCEPT_RAISE (es::xtreme::exception::Exception, "Failed to convert data entry,  xdata to es type as "+s->type() );
		}





	}

	if ( name != "")
		{
			std::string flashkeyValue = "@";
			for (std::set<std::string>::iterator i = hkey_.begin(); i != hkey_.end(); i++) // valid only for first level flashlist items
			{
				//std::cout << "......>" << (*i)  << std::endl;
				xdata::Serializable * s = (*ti).getField(*i);
				flashkeyValue += s->toString() + "-";
			}
			json_object_set_new( document,"flash_key", json_string( flashkeyValue.c_str()) );

		}

	//std::cout << "-" ;
	return document;
}


xdata::Table* getDataTable(char * buffer, size_t size) throw (es::xtreme::exception::Exception)

//xdata::Table* es::xtreme::Application::getDataTable(toolbox::mem::Reference * msg) throw (es::xtreme::exception::Exception)
{
	//xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) msg->getDataLocation(), msg->getDataSize());
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buffer, size);

	xdata::Table * t = new xdata::Table();

	try
	{
		serializer_.import(t, &inBuffer);
		return t;
	}
	catch (xdata::exception::Exception& e)
	{
		delete t;
		XCEPT_RETHROW (es::xtreme::exception::Exception, "Failed to deserialize flashlist table", e);
	}

}

void publishReport (char * buffer, size_t size, const std::string & qname, const std::string & indexName, es::api::Cluster & cluster) throw (es::xtreme::exception::Exception)

//void es::xtreme::Application::publishReport (toolbox::mem::Reference * msg, xdata::Properties & plist, const std::string & indexName) throw (es::xtreme::exception::Exception)
{

	//std::string name = plist.getProperty("urn:xmas-flashlist:name");
	toolbox::net::URN flashlistURN(qname);
	std::string fname = flashlistURN.getNSS();
	/*
	 * $ curl -XPUT 'http://localhost:9200/twitter/tweet/1' -d '{
	 *   "user" : "kimchy",
	 *   "post_date" : "2009-11-15T14:12:12",
	 *   "message" : "trying out Elasticsearch"
	 * }'
	 */
	//LOG4CPLUS_DEBUG(this->getApplicationLogger(), "joining cluster ... ");
	//es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	/*try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Joining cluster :  " << cname);

	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url : '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
	}*/

	//std::string indexName = elasticsearchFlashIndexName_.toString();
	//std::cout << "<";

	xdata::Table * t = 0;
	try
	{
		t = getDataTable(buffer,size);
	}
	catch (es::xtreme::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not extract data table from message for " << indexName << " with type : " << fname;
		XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
	}

	//std::cout << t->getRowCount() << ">" <<std::endl;

	std::vector<std::string> columns = t->getColumns();
	//ti.operator=(t.begin());
	for (xdata::Table::iterator ti = t->begin(); ti != t->end(); ti++)
	{
		//std::cout << "[do row" << std::endl;
		//xdata::UnsignedLong * number = dynamic_cast<xdata::UnsignedLong *>((*ti).getField("Number"));
		json_t * jsondata = 0;

		try
		{
			jsondata = tableRowToJSON(ti, columns, qname);//full qualified flashlist name
		}
		catch(es::xtreme::exception::Exception & e)
		{
			delete t;
			std::stringstream msg;
			msg << "could not create data entry for : " << indexName << " with type : " << fname;
			XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
		}
		//std::cout << "]" << std::endl ;
		// inject into elasticsearch here ...

		//std::cout << "inserting data for flashlist name : " << fname << std::endl;
		//std::cout << json_dumps(jsondata, 0) << std::endl;
		//std::cout << ".";
		try
		{
			json_t * result =cluster.index(indexName,fname,"", jsondata);
			json_decref(result);
		}
		catch (es::api::exception::Exception & e)
		{
			delete t;
			json_decref(jsondata);
			std::stringstream msg;
			msg << "could not index data for : " << indexName << " and type : " << fname;
			XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
		}
		json_decref(jsondata);

	}
	//std::cout << std::endl;
	delete t;
	return;


}

int main(int argc, char* argv[])
{

	XMLPlatformUtils::Initialize();
	toolbox::Properties p;
	//p.setProperty("urn:es-api-stream:CURLOPT_VERBOSE", "true");
	//p.setProperty("urn:es-api-stream:CURLOPT_FORBID_REUSE", "true");
	es::api::Member * esmember = new  es::api::Member(0, p);
	es::api::Cluster & cluster = esmember->joinCluster("http://srv-c2d06-18:9200");

	hkey_.insert("context");

	std::streampos size;
	 char * memblock = 0;

	  std::ifstream file ("/nfshome0/proberts/daq/es/xtreme/urn_xdaq-flashlist_jobcontrol.exdr", std::ios::in| std::ios::binary|std::ios::ate);
	  if (file.is_open())
	  {
	    size = file.tellg();
	    memblock = new char [size];
	    file.seekg (0, std::ios::beg);
	    file.read (memblock, size);
	    file.close();

	    std::cout << "the entire file content is in memory";

	    //delete[] memblock;
	  }
	  else std::cout << "Unable to open file";




	  size_t samples = 10;

	for(;;)
	{
		 toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
		for ( size_t count =0; count < samples; count++)
		{
			////////
			try
			{
				//this->publishReport(msg, plist, elasticsearchFlashIndexName_.toString());
				publishReport(memblock, size, "urn:xmas-flashlist:jobcontrol", "flashlist", cluster);
			}
			catch(es::xtreme::exception::Exception & e)
			{
				std::cout << xcept::stdformat_exception_history(e) << std::endl;

			}


		}

		toolbox::TimeVal end = toolbox::TimeVal::gettimeofday();
		std::cout << "check " <<  end  - start <<  " start:" << start.toString(toolbox::TimeVal::gmt) << " end: "<< end.toString(toolbox::TimeVal::gmt) << std::endl;

		std::cout << "rate  " << (samples * 46)/( end - start)   << std::endl;
	}

}
