<?php
    
    include_once('tools.php');
    include_once('config/config.php');
    
    //ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    // get args

    $qflash_ = $_GET["flash"];
    $parts  = explode(':', $qflash_, 3);
    $flash_ = $parts[2];
    $index_ = $config['flash_index'];
    
    if(!empty($_GET["index"]))
    {
        $index_ = $_GET["index"];
    }
    
    //header('Content-type: application/json');
    //header('Content-Disposition: attachment; filename="' . $flash_ . '.json"');
    header("Cache-Control: no-cache, must-revalidate");
    
    echo '<!doctype html>';
    echo '<html>';
    echo '<head>';
    echo '<meta charset="utf-8">';
    echo '<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>';
    echo '<link href="css/xdaq-tables.css" rel="stylesheet" />';
    echo '</head>';
    echo '<body>';
    
    // HTTP/1.1
    $jsonmapping = getMapping($config['host'], $config['port'],  $index_ , $flash_);
    echo '<pre>';
    echo indent($jsonmapping);
    echo '</pre>';
    echo '</body>';
    echo '</html>';
    
?>