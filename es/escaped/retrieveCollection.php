<?php

    require 'vendor/autoload.php';
    
    include_once('tools.php');
    include_once('config/config.php');
    
   // echo PSX\Urn("urn:xdaq-flashlist:jobcontrol");
    
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    // get args
    $fmt_ = $_GET["fmt"];
    $qflash_ = $_GET["flash"];
    $tophits_ = true;
    $index_ = $config['flash_index'];
    $maxsources_ = $config['maxsources'];
    $delimiter_ = ',';
    
    $parts  = explode(':', $qflash_, 3);
    
    //$parts[0];
    //$parts[1];
    $flash_ = $parts[2];
    
    if(!empty($_GET["tophits"]))
    {
        $val = $_GET["tophits"];
        if ( $val == "false")
        {
            $tophits_ = false;
        }
    }
    
    if(!empty($_GET["maxsources"]))
    {
        $maxsources_ = $_GET["maxsources"];
    }
    
    if(!empty($_GET["index"]))
    {
        $index_ = $_GET["index"];
    }
    
    if(!empty($_GET["delimiter"]))
    {
        $delimiter_ = $_GET["delimiter"];
    }

    $filter =  array();
    // Additional query paramaters will be used as filter matching flashlist fields (e.g. sessionid )
    $parameters  = explode("&", $_SERVER["QUERY_STRING"] );
    foreach ($parameters as $pair) {
        $keyVal = explode('=',$pair);
        $key = &$keyVal[0];
        if ( $key != "fmt" AND $key != "flash" AND $key != "tophits" AND $key != "maxsources" AND $key != "index" AND $key != "delimiter")
        {
            $val = $keyVal[1];
            $filter[$key] = $val;
        }
        //echo "[".$key."]->[".urldecode($val)."]";
        //echo "";
    }
    
   
    

    
    
    if ( $fmt_ == "csv")
    {
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename="' . $flash_ . '.csv"');
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        
        if ( $tophits_)
            retrieveTopHitsCSV($config['host'],$config['port'], $index_ ,$flash_, $maxsources_, $delimiter_, $filter);
        else
            retrieveCSV($config['host'],$config['port'],$index_,$flash_, $maxsources_, $delimiter_, $filter);
        
    }
    else if ( $fmt_ == "html")
    {
        echo '<!doctype html>';
        echo '<html>';
        echo '<head>';
        echo '<meta charset="utf-8">';
        echo '<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>';
        echo '<link href="css/xdaq-tables.css" rel="stylesheet" />';
        echo '</head>';
        echo '<body>';
        if ( $tophits_)
            retrieveTopHitsHTML($config['host'],$config['port'],$index_,$flash_, $maxsources_, $filter);
        else
            retrieveHTML($config['host'],$config['port'],$index_,$flash_, $maxsources_, $filter);
        echo '</body>';
        echo '</html>';
    }
    else if ( $fmt_ == "json")
    {
        
        header('Content-type: application/json');
        header('Content-Disposition: attachment; filename="' . $flash_ . '.json"');
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        
        if ( $tophits_)
            retrieveTopHitsJSON($config['host'],$config['port'],$index_,$flash_, $maxsources_,false, $filter);
        else
            retrieveJSON($config['host'],$config['port'],$index_,$flash_, $maxsources_,false, $filter);

    }
    
    else if ( $fmt_ == "plain")
    {
        header('Content-type: text/plain');

        //print_r($filter);

        if ( $tophits_)
            retrieveTopHitsCSV($config['host'],$config['port'], $index_ ,$flash_, $maxsources_, $delimiter_, $filter);
        else            
            retrieveCSV($config['host'],$config['port'],$index_,$flash_, $maxsources_, $delimiter_, $filter);
      
    }
    else
    {
       header("HTTP/1.1 404 Not Found");
        
    }
?>
