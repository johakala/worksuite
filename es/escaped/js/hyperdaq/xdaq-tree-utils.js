/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest					 								 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			       			 *
 *************************************************************************/

/*
 * Augments the treetable with additional functionality
 * . tree aware checkbox selection
 * . svaing and loading state from session storage
 */

/*
 * To disable checkbox support, add the following class to the table root element
 * 
 * xdaq-table-tree-nosel
 */

/*
 * To auto expand the tree to its full extent, add the folling attribute to the table root element
 * 
 * data-expand="true"
 * 
 * To disable loading the state of the tree from sessionStorage on page refresh add the following attribute
 * 
 * data-allow-refresh="false"
 */

var internalMasterSwitchSelector = '.xdaq-table-tree:not(".xdaq-table-tree-nosel") thead input[type=checkbox]';

var xdaqTreeCount = 0;

var xdaqSaveStateEnabled = true;

function xdaqBuildTrees()
{
	var xdaqTrees = $(".xdaq-table-tree:not('.xdaq-param-viewer')");
	
	xdaqTrees.each(function() {
		xdaqBuildTree($(this));
	});
}

function xdaqBuildTree(root)
{
	// is tree already initiated
	if (root.attr("data-treeinit") == "true")
	{
		return;
	}
	root.attr("data-treeinit", "true");
	
	// make sure tree has a unique id
	if (!(root.attr("id")))
	{
		root.attr("id", "xdaq-tree-autoid-" + xdaqTreeCount);
	}
	xdaqTreeCount += 1;				
	
	// call the treetable plugin constructor
		
	var expandAll = root.attr("data-expand");
	if (expandAll == "true")
	{
		root.treetable({ initialState: "expanded" });	
	}
	else
	{
		xdaqSaveStateEnabled = false;
		root.treetable();
		xdaqSaveStateEnabled = true;
		
		if (root.attr("data-allow-refresh") != "false")
		{
			loadSessionStorage(root);		
		}
	}
	
	// This is not used by the framework, but provides a way to attach to a tree build complete
	root.trigger("initialized");
}

function xdaqAddTreeHandlers()
{
	//console.log("Adding tree handlers");
		
	// apply click events to checkboxes
	$("#xdaq-wrapper").on('click', '.xdaq-table-tree:not("xdaq-table-tree-nosel")>tbody input[type=checkbox]:not("xdaq-table-tree-nosel")', function(e) {
																	
		// 1. apply toggle down the tree
		// 2. update parent toggle (on if all children on)
		
		var root = $(this).closest("table");
		var row = $(this).closest("tr");
		var parentID = row.attr("data-treeid");
		
		var isChecked = false;
		if($(this).is(":checked")) 
		{
			isChecked = true;
		}
		
		// toggle children
		toggleCheckboxes(row.closest("table"), row.attr("data-treeid"), isChecked);
		
		// need to do parents
		parentID = row.attr("data-treeparent");
		var hasParent = false;
		var parent = root.find("tbody tr[data-treeid="+parentID+"]");
		if (parent.length == 1)
		{
			hasParent = true;
		}
		
		while (hasParent == true)
		{
			if (isChecked == false)
			{
				// if we are unchecking the clicked box, uncheck all parents
				var checkBoxes = parent.first().find("input[type=checkbox]");
				checkBoxes.prop("checked", isChecked);	
			}
			else
			{
				// need to check down from each parent to see if all children are checked
				var allChildren = childrenChecked(root, parent.first().attr("data-treeid"));
				var checkBoxes = parent.first().find("input[type=checkbox]");
				checkBoxes.prop("checked", allChildren);	
			}
			
			// get next parent
			parentID = parent.first().attr("data-treeparent");
			parent = root.find("tbody tr[data-treeid="+parentID+"]");
			if (parent.length != 1)
			{
				hasParent = false;
			}
		}
		
		// do internal master switch
		var allOn = true;
		if (root.find("tbody input[type=checkbox]:not(:checked)").length > 0)
		{
			allOn = false;
		}
		root.find("thead input[type=checkbox]").each(function(){
			$(this).prop("checked", allOn);	
		});
		$("input[type=checkbox][data-linked-table='"+root.attr("id")+"']:not("+internalMasterSwitchSelector+")").each(function() {
			$(this).prop("checked", allOn);
		});	
		
		saveSessionSelectionState(root);
	});
	  
	// create master toggle if it exists
	$(internalMasterSwitchSelector).on('click', function(e) {
					
		//console.log("master toggle clicked");
		
		root = $(this).closest("table");
		
		isChecked = false;
		if($(this).is(":checked")) 
		{
			isChecked = true;
		}
		
		masterToggle(root, isChecked);
		
		// check for external masters and toggle those too
		$("input[type=checkbox][data-linked-table="+root.attr("id")+"]:not("+internalMasterSwitchSelector+")").each(function() {
			$(this).prop("checked", isChecked);
		});	
	});
	  
	
	// create external master toggle if it exists
	$("#xdaq-wrapper").on('click', "input[type=checkbox]:not("+internalMasterSwitchSelector+"):not(.xdaq-table-tree)" ,function() {
					
		tableID = $(this).attr("data-linked-table-tree");
		if (tableID != undefined)
		{
			//console.log("external master toggle clicked for "+tableID);
						
			root = $("#"+tableID+".xdaq-table-tree");
		
			if (root.length > 0)
			{
				isChecked = false;
				if($(this).is(":checked")) 
				{
					isChecked = true;
				}
	
				masterToggle(root.first(), isChecked);
				
				// check for internal masters and toggle those too
				root.first().find("thead input[type=checkbox]").each(function() {
					$(this).prop("checked", isChecked);
				});	
			}
			else
			{
				console.log("Failed to find table : "+tableID);
			}
		}
	});
}

function masterToggle(root, isChecked)
{	
	children = root.find("tbody input[type=checkbox]");
	if (children.length > 0)
	{
		children.each(function() {
			$(this).prop("checked", isChecked);			
		});	
	}
	
	saveSessionSelectionState(root);
	
	return root;
}

// set all children to same toggle status
// root == "table"
// id == parent id
// on == true | false
function toggleCheckboxes(root, id, on)
{
	children = root.find("tbody tr[data-treeparent="+id+"]");
	if (children.length > 0)
	{
		children.each(function() {
			toggleCheckboxes(root, $(this).attr("data-treeid"), on);
			checkBoxes = $(this).find("input[type=checkbox]");
			checkBoxes.prop("checked", on);			
		});	
	}
	
	return root;
}

// return true if all children of parentID are checked
// root == "table"
// id == parent id
function childrenChecked(root, id)
{
	// check direct children
	el = root.find("tbody tr[data-treeparent="+id+"]");
	checkBoxes = el.find("input[type=checkbox]");
	directsOn = true;
	checkBoxes.each(function() {
		if(!($(this).is(":checked")))
		{
			directsOn = false;
		}
	});
	
	if (directsOn == false)
	{
		return false;
	}
	
	// direct children all on, check for each child
	children = root.find("tbody tr[data-treeparent="+id+"]");
	if (children.length > 0)
	{
		//allOn = true;
		children.each(function() {
			if(childrenChecked(root, $(this).attr("data-treeid")) == false)
			{
				//allOn = false;
				return false;
			}
		});	
		//return allOn;
		return true;
	}
	else
	{
		return true;
	}
	
	return root;
}

function loadSessionStorage(root)
{
	//console.log("loading tree table from sessionStorage is disabled");
	//return;
	
	xdaqSaveStateEnabled = false;

	tableID = root.attr("id");
	
	// List of classes to remove from all elements to restore state
	//removeList = "xdaq-table-tree-toggle-on xdaq-table-tree-hidden xdaq-table-tree-not-expanded xdaq-table-tree-expanded";
		
	if(typeof(Storage)!=="undefined")
	{
		try
		{
			sessionSaved = false;
			UUID = window.location.pathname+"-xdaqTreeTableStateStored-"+tableID;
			console.log("Loading session data for table with UUID '"+UUID+"'");
			
			if (sessionStorage.getItem(UUID))
			{
				sessionSaved = sessionStorage.getItem(UUID);
			}
			if (sessionSaved == "true")
			{
				rows = root.find("tbody tr");
				console.log("restoring data for "+rows.length+" rows");
				rows.each(function(){
					var rowID = $(this).attr("data-treeid");
					var isExpanded = sessionStorage.getItem(window.location.pathname+"-xdaqTreeTableExpansionState-"+tableID+"-"+rowID);
					//console.log("expansion state for " + tableID+"-"+rowID + " = " + isExpanded);
					if (isExpanded == "true")
					{
						//console.log("expanding");
						root.treetable("expandNode", rowID);
					}
					
					isSelected = sessionStorage.getItem(window.location.pathname+"-xdaqTreeTableSelectionState-"+tableID+"-"+rowID);
					checkboxes = $(this).find("input[type=checkbox]")
					if (checkboxes.length > 0)
					{
						
						if (isSelected == true || isSelected == "true")
						{
							checkboxes.attr("checked", true);	
						}
						else if (isSelected == false || isSelected == "false")
						{
							checkboxes.attr("checked", false);	
						}
						else
						{
							//console.log("Unsupported value : " + isSelected);
						}
					}
				});
				xdaqSaveStateEnabled = true;
				return true;
			}
		}
		catch (err)
		{
			console.log("There was an error trying to read from sessionStorage, could not restore state of treetable "+tableID+" : "+err.message);
		}
	}
	else
	{
		console.log("The current browser does not support web storage, could not restore state of treetable "+tableID);
	}
	xdaqSaveStateEnabled = true;
	return false;
}

function saveSessionExpansionState(root)
{
	if (xdaqSaveStateEnabled == false)
	{
		//console.log("save state disabled");
		return;
	}
	
	tableID = root.attr("id");
	
	if(typeof(Storage)!=="undefined")
	{
		if (sessionStorage)
		{
			try
			{
				UUID = window.location.pathname+"-xdaqTreeTableStateStored-"+tableID;
				//console.log("Saving expansion session data for table with UUID '"+UUID+"'");
				sessionStorage.setItem(UUID, "true");
				
				rows = root.find("tbody tr");
				rows.each(function(){
					var rowID = $(this).attr("data-treeid");
					var data = $(this).hasClass("expanded");
					//console.log("saving " + rowID + " to " + data);
					sessionStorage.setItem(window.location.pathname+"-xdaqTreeTableExpansionState-"+tableID+"-"+rowID, data);
				});
			}
			catch (err)
			{
				console.log("There was an error trying to write to sessionStorage, could not save state of treetable "+tableID);
			}
		}
		else
		{
			console.log("There was an error working with the sessionStorage object, could not save state of treetable "+tableID);
		}
	}
	else
	{
		console.log("The current browser does not support web storage, could not save state of treetable "+tableID);
	}
}

function saveSessionExpansionStateRow(root, row)
{
	if (xdaqSaveStateEnabled == false)
	{
		//console.log("save state disabled");
		return;
	}
	
	tableID = root.attr("id");
	
	if(typeof(Storage)!=="undefined")
	{
		if (sessionStorage)
		{
			try
			{
				UUID = window.location.pathname+"-xdaqTreeTableStateStored-"+tableID;
				//console.log("Saving expansion session data for table with UUID '"+UUID+"'");
				sessionStorage.setItem(UUID, "true");
				
				var rowID = row.attr("data-treeid");
				var data = row.hasClass("expanded");
				//console.log("saving " + rowID + " to " + data);
				sessionStorage.setItem(window.location.pathname+"-xdaqTreeTableExpansionState-"+tableID+"-"+rowID, data);				
			}
			catch (err)
			{
				console.log("There was an error trying to write to sessionStorage, could not save state of treetable "+tableID);
			}
		}
		else
		{
			console.log("There was an error working with the sessionStorage object, could not save state of treetable "+tableID);
		}
	}
	else
	{
		console.log("The current browser does not support web storage, could not save state of treetable "+tableID);
	}
}

function saveSessionSelectionState(root)
{
	tableID = root.attr("id");
	
	if(typeof(Storage)!=="undefined")
	{
		if (sessionStorage)
		{
			try
			{
				UUID = window.location.pathname+"-xdaqTreeTableStateStored-"+tableID;
				//console.log("Saving selection session data for table with UUID '"+UUID+"'");
				sessionStorage.setItem(UUID, "true");
				
				rows = root.find("tbody tr");
				rows.each(function(){
					rowID = $(this).attr("data-treeid");
					isSelected = "false";
					checkbox = $(this).find("input[type=checkbox]");
					if (checkbox.length > 0)
					{
						if (checkbox.first().is(":checked"))
						{
							isSelected = "true";
						}
					}
					sessionStorage.setItem(window.location.pathname+"-xdaqTreeTableSelectionState-"+tableID+"-"+rowID, isSelected);
				});
			}
			catch (err)
			{
				console.log("There was an error trying to write to sessionStorage, could not save state of treetable "+tableID);
			}
		}
		else
		{
			console.log("There was an error working with the sessionStorage object, could not save state of treetable "+tableID);
		}
	}
	else
	{
		console.log("The current browser does not support web storage, could not save state of treetable "+tableID);
	}
}