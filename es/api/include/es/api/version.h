// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _es_api_Version_h_
#define _es_api_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define ESAPI_VERSION_MAJOR 3
#define ESAPI_VERSION_MINOR 4
#define ESAPI_VERSION_PATCH 0
// If any previous versions available E.g. #define ESAPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef ESAPI_PREVIOUS_VERSIONS
#define ESAPI_PREVIOUS_VERSIONS "3.3.0"


//
// Template macros
//
#define ESAPI_VERSION_CODE PACKAGE_VERSION_CODE(ESAPI_VERSION_MAJOR,ESAPI_VERSION_MINOR,ESAPI_VERSION_PATCH)
#ifndef ESAPI_PREVIOUS_VERSIONS
#define ESAPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(ESAPI_VERSION_MAJOR,ESAPI_VERSION_MINOR,ESAPI_VERSION_PATCH)
#else 
#define ESAPI_FULL_VERSION_LIST  ESAPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(ESAPI_VERSION_MAJOR,ESAPI_VERSION_MINOR,ESAPI_VERSION_PATCH)
#endif 

namespace esapi
{
	const std::string package  =  "esapi";
	const std::string versions =  ESAPI_FULL_VERSION_LIST;
	const std::string summary = "Elasticsearch XDAQ interface";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andrea Petrucci, Andy Forrest, P.Roberts";
	const std::string link = "http://www.elasticsearch.org/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
