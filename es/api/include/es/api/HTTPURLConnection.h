// $Id: HTTPURLConnection.h,v 1.4 2008/07/18 15:28:41 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef es_api_HTTPURLConnection_h_
#define es_api_HTTPURLConnection_h_

#include "es/api/exception/Exception.h"
#include <string>

namespace es
{

	namespace api
	{

		class HTTPURLConnection
		{
			public:

				HTTPURLConnection() throw (es::api::exception::Exception);
				~HTTPURLConnection();

				//! receive  from established HTTP connection and returns data into a BufRef
				std::string receiveFrom() throw (es::api::exception::Exception);

				//! send data to an established HTTP connection
				void sendTo
				(
						char * path,
						char * host,
						unsigned int port,
						const char * buf,
						size_t len
				)
				throw (es::api::exception::Exception);

				//! send data to an established HTTP connection
				void sendTo
				(
						char * path,
						char * host,
						unsigned int port,
						const char * buf,
						size_t len,
						const char* soapAction
				)
				throw (es::api::exception::Exception);


				//! close connection
				void close();

				//! connect to URL
				void connect(const std::string& host, unsigned int port) throw (es::api::exception::Exception);

				//! send buffer of given lenght
				void send(const char * buf, size_t len)  throw (es::api::exception::Exception);

			protected:

				//! receive len characters into buf
				ssize_t receive(char * buf , size_t len )  throw (es::api::exception::Exception);


				//! Helper to re-creating a socket
				void open() throw (es::api::exception::Exception);

				std::string extractMIMEBoundary(const char * buf , size_t size) throw (es::api::exception::Exception);

				void writeHttpPostMIMEHeader
				(
						char * path,
						char * host,
						unsigned int port,
						std::string & boundaryStr,
						size_t len
				)
				throw (es::api::exception::Exception);

				void writeHttpPostMIMEHeader
				(
						char * path,
						char * host,
						unsigned int port,
						std::string & boundaryStr,
						size_t len,
						const char* soapAction
				)
				throw (es::api::exception::Exception);

				void writeHttpPostHeader
				(
						char * path,
						char * host,
						unsigned int port,
						size_t len,
						const char* soapAction
				)
				throw (es::api::exception::Exception);

				void writeHttpPostHeader
				(
						char * path,
						char * host,
						unsigned int port,
						size_t len
				)
				throw (es::api::exception::Exception);

				int socket_;
		};

	}}
#endif
