// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, P. Roberts										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _es_api_EventingBus_h_
#define _es_api_EventingBus_h_

#include "xdaq/Object.h"

#include "toolbox/Properties.h"

#include "es/api/exception/Exception.h"

#include "es/api/Stream.h"

//#include "toolbox/ActionListener.h"
//#include "toolbox/EventDispatcher.h"

namespace es
{
	namespace api
	{
		class Cluster /*: public toolbox::ActionListener, public toolbox::EventDispatcher*/
		{
			public:

				Cluster (xdaq::Application * owner, const std::string & url, toolbox::Properties & p) throw (xdaq::exception::Exception);

				std::string getClusterName () throw (es::api::exception::Exception);
				std::string getClusterURL ();

				size_t getNumberOfDocuments() throw (es::api::exception::Exception);

				// insert documents
				json_t * index(const std::string& index, const std::string& type, const std::string& id, const std::string& properties, json_t * json) throw (es::api::exception::Exception);
				/// insert,  a document with automatic id creation
				json_t * index(const std::string& index, const std::string& type, const std::string& properties, json_t * json) throw (es::api::exception::Exception);

				json_t * search(const std::string& index, const std::string& type, const std::string& properties, json_t * json) throw (es::api::exception::Exception);

				json_t * getDocument(const std::string& index, const std::string& type, const std::string& id) throw (es::api::exception::Exception);

				json_t * createIndex(const std::string& index) throw (es::api::exception::Exception);
				json_t * createIndexData(const std::string& index, json_t * properties) throw (es::api::exception::Exception);
				json_t * deleteIndex(const std::string& index) throw (es::api::exception::Exception);

				json_t * createMapping(const std::string& index, const std::string& type, json_t * mapping) throw (es::api::exception::Exception);
				json_t * getMapping(const std::string& index, const std::string& type) throw (es::api::exception::Exception);
				json_t * deleteMapping(const std::string& index, const std::string& type) throw (es::api::exception::Exception);

				json_t * getLatestDocument(const std::string& index, const std::string& type) throw (es::api::exception::Exception);
				//void actionPerformed(toolbox::Event& e);
				bool exists(const std::string& index, const std::string& type ) throw (es::api::exception::Exception);


				//testing

				json_t * bulkIndex(const std::string& index, const std::string& properties, std::stringstream & bulk) throw (es::api::exception::Exception);

				void showStats();
			private:

				toolbox::Properties properties_;
				std::string name_;
				std::string url_;
				//es::api::Stream stream_;
				std::vector<es::api::Stream*> streams_;
				size_t streamNum_;
				size_t numStreams_;

		};
	}
}

#endif
