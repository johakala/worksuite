// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, P. Roberts										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/


#ifndef _es_api_Curl_h_
#define _es_api_Curl_h_



namespace es
{
	namespace api
	{

		typedef enum {
			GET,
			PUT,
			POST,
			HEAD,
			DELETE
		} HTTPMethod;


		class  NetHTTPInfo
		{
			public:

				const char*     fPayload;
				size_t       fPayloadLen;
		};
	}
}

#endif
