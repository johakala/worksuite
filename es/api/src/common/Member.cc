// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "es/api/Member.h"
#include "es/api/Cluster.h"

#include "toolbox/task/Guard.h"

#include "xdaq/ApplicationRegistry.h"

#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"

es::api::Member::Member (xdaq::Application * owner, toolbox::Properties & p) throw (xdaq::exception::Exception)
	: xdaq::Object(owner), mutex_(toolbox::BSem::FULL, false), tribeurl_(""), properties_(p)
{
}

es::api::Member::Member (xdaq::Application * owner, std::list<std::string> & urls, toolbox::Properties & p) throw (xdaq::exception::Exception)
	: xdaq::Object(owner), mutex_(toolbox::BSem::FULL, false), tribeurl_(""), properties_(p)
{

			for (std::list<std::string>::iterator i = urls.begin(); i != urls.end(); i++)
			{
				this->joinCluster(*i);
			}

}

es::api::Member::Member (xdaq::Application * owner, const std::string & tribeurl, toolbox::Properties & p) throw (xdaq::exception::Exception)
	: xdaq::Object(owner), mutex_(toolbox::BSem::FULL, false), tribeurl_(tribeurl), properties_(p)
{

			//tbd
	// query the tribe to know what are all the cluster available and join them.

}

std::string es::api::Member::getTribeURL()
{
	return tribeurl_;
}


es::api::Cluster& es::api::Member::joinCluster (const std::string & url)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::map<std::string, es::api::Cluster*>::iterator i = esClusters_.find(url);

	if (i != esClusters_.end())
	{
			return *((*i).second);
	}

	es::api::Cluster* cluster = new es::api::Cluster(this->getOwnerApplication(), url, properties_);

	esClusters_[url] = cluster;

	return *(esClusters_[url]);
}


std::set<std::string> es::api::Member::getAvailableClusters ()
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::set < std::string > clusters;
	for (std::map<std::string, es::api::Cluster*>::iterator i = esClusters_.begin(); i != esClusters_.end(); i++)
	{
		clusters.insert((*i).first);
	}
	return clusters;
}

std::string es::api::Member::clustersToHTML()
{
	std::stringstream out;

	out << cgicc::table().set("class", "xdaq-table");

	out << cgicc::thead();

	out << cgicc::tr();
	out << cgicc::th("Url");
	out << cgicc::tr() << std::endl;

	out << cgicc::thead();
	out << cgicc::tbody();

	for (std::map<std::string, es::api::Cluster*>::iterator i = esClusters_.begin(); i != esClusters_.end(); i++)
	{
		//es::api::Cluster* cluster = (*i).second;

		out << cgicc::tr();
		out << cgicc::td((*i).first);
		out << cgicc::tr() << std::endl;
	}

	out << cgicc::tbody();
	out << cgicc::table();

	return out.str();
}
