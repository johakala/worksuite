// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, A. Petrucci, P. Roberts							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _es_xbeat_version_h_
#define _es_xbeat_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define ESXBEAT_VERSION_MAJOR 1
#define ESXBEAT_VERSION_MINOR 0
#define ESXBEAT_VERSION_PATCH 1
// If any previous versions available E.g. #define ESXBEAT_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef ESXBEAT_PREVIOUS_VERSIONS


//
// Template macros
//
#define ESXBEAT_VERSION_CODE PACKAGE_VERSION_CODE(ESXBEAT_VERSION_MAJOR,ESXBEAT_VERSION_MINOR,ESXBEAT_VERSION_PATCH)
#ifndef ESXBEAT_PREVIOUS_VERSIONS
#define ESXBEAT_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(ESXBEAT_VERSION_MAJOR,ESXBEAT_VERSION_MINOR,ESXBEAT_VERSION_PATCH)
#else 
#define ESXBEAT_FULL_VERSION_LIST  ESXBEAT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(ESXBEAT_VERSION_MAJOR,ESXBEAT_VERSION_MINOR,ESXBEAT_VERSION_PATCH)
#endif 

namespace esxbeat
{
	const std::string package  =  "esxbeat";
	const std::string versions =  ESXBEAT_FULL_VERSION_LIST;
	const std::string description = "XDAQ plugin for enabling monitoring on Elasticsearch";
	const std::string authors = "Luciano Orsini, Penelope Roberts";
	const std::string summary = "Elasticsearch XDAQ streamer";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
