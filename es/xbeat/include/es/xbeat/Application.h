// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, A. Petrucci, P. Roberts			 				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _es_xbeat_Application_h_
#define _es_xbeat_Application_h

#include <string>
#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/Application.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Table.h"
#include "xdata/ActionListener.h"
#include "xdata/exdr/Serializer.h"
#include "b2in/nub/Method.h"
#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "xmas/exception/Exception.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/string.h"
#include "toolbox/BSem.h"

#include "xoap/SOAPMessage.h"

#include "es/xbeat/exception/Exception.h"
#include "xmas/FlashListDefinition.h"

#include "es/api/Member.h"

namespace es
{
	namespace xbeat
	{
		class Application
			:public xdaq::Application, 
			 public xgi::framework::UIManager,
			 public toolbox::ActionListener, 
			 public xdata::ActionListener,
			 public toolbox::task::TimerListener
		{
		
			public:

			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
			~Application();

			void actionPerformed ( xdata::Event& e );
			void actionPerformed( toolbox::Event& event );
			
			void timeExpired(toolbox::task::TimerEvent& e);

			//void selfHeartbeat();

			void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
				
			void onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) throw (b2in::nub::exception::Exception);

			//void publishReport (toolbox::mem::Reference * msg, xdata::Properties & plist, const std::string & indexname) throw (es::xbeat::exception::Exception);
			void publishReport (char * buffer, size_t size, const std::string & qname, const std::string & indexName) throw (es::xbeat::exception::Exception);

			void publishHeartbeat (toolbox::mem::Reference *  msg, xdata::Properties & plist);
			void publishHeartbeat (toolbox::mem::Reference *  msg, xdata::Properties & plist, std::string indexName);

			json_t * listToJSON (xdata::Properties & plist, const std::string & timeToLive) throw (es::xbeat::exception::Exception);

			json_t * plistToJson(xdata::Properties & plist) throw (es::xbeat::exception::Exception);


			protected:

			void StatisticsTabPage( xgi::Output * out );
			void TabPanel( xgi::Output * out );

			void displayFlashlist(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
			void displayFlashlistMapping(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);

			void displayApplicationMapping(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);


			// curl easy opt
			void disableESCloud(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
			void enableESCloud(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
			void disableESIndex(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
			void enableESIndex(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);

			private:

			void createIndex (const std::string & iname, json_t * payload) throw (es::xbeat::exception::Exception);
			void createMapping(const std::string & indexname, const std::string & name, const std::string & ttl, xdata::Properties & plist) throw (es::xbeat::exception::Exception);

     		void resetActiveFlashlists();

			xdata::String elasticsearchClusterUrl_;
			xdata::String elasticsearchHeartIndexName_;
			xdata::String elasticsearchHeartIndexStoreType_;
			xdata::String ttl_;
			xdata::UnsignedInteger32  lossReportCounter_; // only print send errors every (msg % lossReportCounter) = 1		
			xdata::Boolean elasticsearchConnectionForbidReuse_;
			xdata::String numberOfChannels_;

			std::map<std::string, size_t> successCounters_;
			std::map<std::string, size_t> lossCounters_;

			std::map<std::string, size_t> bulkCounters_;
			std::map<std::string, size_t> lastBulkSizes_;
			std::map<std::string, size_t> maxBulkSizes_;

			std::map<std::string, bool> activeFlashList_;
			bool plistMapping_;
			es::api::Member * member_;

			std::map<std::string, bool> blackFlashList_;

			bool indexCreated_;
			bool indexAvailable_;

			toolbox::TimeVal lastTime_; // used to measure time interval for measuring rate
			xdata::UnsignedLong counter_;  // counter for all received messages
			xdata::UnsignedLong totalIndexOperationsCounter_;  // counter for all received messages
			xdata::Double rate_;
			xdata::String sampleTime_;
			xdata::Boolean enableESCloud_;
			xdata::Boolean enableESIndexOperation_;

			toolbox::BSem mutex_;
			std::map<std::string, std::string> ttls_;

		};
	}
}
#endif
