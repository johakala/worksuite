/**
 * @file frl-private.h
 * Private structures for FRL driver
 *
 * $Author: cschwick $
 * $Revision: 1.4 $
 * $Id: frl-private.h,v 1.4 2004/11/04 09:36:51 cschwick Exp $
*/
#ifndef _FRL_KERNEL_H_
#define _FRL_KERNEL_H_

#include "frl-types.h"

/* PCI identification of board */
#define _FRL_VENDOR              (0xECD6)
#define _FRL_DEVICE              (0xFF01) /* bridge FPGA */
#define _FRL1_DEVICE             (0xFF10) /* FRL    FPGA */

/*
 * offsets and masks for PCI=BRIDGE-LOCALREADOUT FPGA
 */
#define _FRL_BKSZ_OFFSET         (0x80)
#define _FRL_WCFADDR_OFFSET      (0x84)
#define _FRL_WCFWADDR_OFFSET     (0x88)

#define _FRL_FRBKFIFO_OFFSET     (0x04)  /* data block fifo : FRL Block Fifo */
#define _FRL_CSR_OFFSET          (0x00)
#define _FRL_DEBUG_OFFSET        (0x08)  /* debug info */
#define _FRL_BAR0_range          (0x8C)

/* CSR bit masks*/
#define _FRL_CSR_SPY_ENABLE                      (0x00000400)
#define _FRL_CSR_HALF_EMPTY_INT_STATUS           (0x00000002)
#define _FRL_CSR_EMPTY_INT_STATUS                (0x00000001)
#define _FRL_CSR_HALF_EMPTY_INT_ENABLE           (0x00000200)
#define _FRL_CSR_EMPTY_INT_ENABLE                (0x00000100)
#define _FRL_CSR_ALL_INTS_ENABLE                 (0x00000300)
#define _FRL_CSR_SOFT_RESET                      (0x00020000)

/*
 * offsets and masks for FRL FPGA
 */ 
#define _FRL1_CSR            (0x00000000)   
#define _FRL1_BLOCKFIFO	     (0x00000004)
#define _FRL1_HISTOCTRL	     (0x00000008)
#define _FRL1_SEGMENTCNT     (0x00000040)
#define _FRL1_FRAGMENTCNT    (0x00000044)
#define _FRL1_CURTRIGNO      (0x00000048)
#define _FRL1_FREEBLOCK	     (0x0000004C)
#define _FRL1_CSRDBG	     (0x00000050)
#define _FRL1_DEUGFIFORD     (0x00000054)
#define _FRL1_CMCVERSION     (0x00000058)
#define _FRL1_BERRBITS       (0x0000005C) /* multiplexed between low and high */
#define _FRL1_RDDEBUG_LOW    (0x00000100)
#define _FRL1_RDDEBUG_MID    (0x00000104)
#define _FRL1_RDDEBUG_HIG    (0x00000108)
#define _FRL1_RDDEBUG_RES    (0x0000010C)
#define _FRL1_FIFOSTAT	     (0x00000110)
#define _FRL1_BLOCKSIZE	     (0x00000080)
#define _FRL1_HEADERSIZE     (0x00000084)
#define _FRL1_SPY_EVTNOFIFO  (0x00000088)

// masks
// for _FRL1_CSR offset
// THE FRL FPGA IS SWAPPED !!!

// #define _FRL1_LVDS_SETUP     (0x00000001)   // w 
// #define _FRL1_LVDS_TESTRUN   (0x0000f000)   // rw
// #define _FRL1_SOFT_RESET     (0x00020000)   // w 
// #define _FRL1_DEBUG_MODE     (0x01000000)   // rw
// #define _FRL1_LINK_CTRL      (0x0f000000)   // rw
// #define _FRL1_SPY_ALL        (0x40000000)   // rw
// #define _FRL1_SPY_ENABLE     (0x80000000)   // rw
#define _FRL1_DISABLE_NIC    (0x20000000)   //rw
#define _FRL1_LVDS_SETUP     (0x01000000)   // w 
#define _FRL1_LVDS_TESTRUN   (0x00f00000)   // rw
#define _FRL1_SOFT_RESET     (0x00000200)   // w 
#define _FRL1_DEBUG_MODE     (0x00000001)   // rw
#define _FRL1_LINK_CTRL      (0x0000000f)   // rw
#define _FRL1_SPY_ALL        (0x00000040)   // rw
#define _FRL1_SPY_ENABLE     (0x00000080)   // rw


#define _FRL1_BAR0_range     (0x114)

/* FIFO sizes */
#define _FRL_free_data_fifo_depth        (512)
#define _FRL_wc_fifo_depth               (512)
#define _FRL_wc_fifo_address_mask        (0x000001FF)
#define _FRL_wc_fifo_mask                (0x000003FF)

/* Block sizes */
#define FRL_MAX_BLOCK_SIZE               (64*1024)
#define FRL_MIN_BLOCK_SIZE               (512)
#define FRL_SIZE_ALIGN_MASK              (0xFFFFFFF8)

/* default values */
#define FRL_DEFAULT_BLOCK_SIZE           (4096)
#define FRL_DEFAULT_BLOCK_NUMBER         (2048)
#define FRL_DELAULT_HEADER_SIZE          (0)

/* trailers and headers. should be taken from interface dir */
/* HEADER positions (in 32 bits words..) */
#define _FRLH_SHIFT_Evt_type     (24)
#define _FRLH_WORD_Evt_type              (1)
#define _FRLH_MASK_Evt_type              (0xF)

#define _FRLH_SHIFT_LV1_id               (0)
#define _FRLH_WORD_LV1_id                (1)
#define _FRLH_MASK_LV1_id                (0xFFFFFF)

#define _FRLH_SHIFT_BX_id                (20)
#define _FRLH_WORD_BX_id                 (0)
#define _FRLH_MASK_BX_id                 (0xFFF)

#define _FRLH_SHIFT_Source_id    (8)
#define _FRLH_WORD_Source_id             (0)
#define _FRLH_MASK_Source_id             (0xFFF)

#define _FRLH_SHIFT_FOV                  (4)
#define _FRLH_WORD_FOV                   (0)
#define _FRLH_MASK_FOV                   (0xF)

/* TRAILER positions (in 32 bits words..) */
#define _FRLT_SHIFT_Evt_lgth             (0)
#define _FRLT_WORD_Evt_lgth              (1)
#define _FRLT_MASK_Evt_lgth              (0xFFFFFF)

#define _FRLT_SHIFT_Evt_stat             (24)
#define _FRLT_WORD_Evt_stat              (0)
#define _FRLT_MASK_Evt_stat              (0xFF)

#define _FRLT_SHIFT_CRC                  (4)
#define _FRLT_WORD_CRC                   (0)
#define _FRLT_MASK_CRC                   (0xFFFF)


/* structure declaration */
struct _FRL_data_fifo {
    int read;
    int write; 
    int size;
    int done_read;
    int done_write;
    int int_disabled;
};

/* Some status is encoded by the Frl in the receied element.*/
struct _FRL_wc_fifo_element {
    U32    wc:24;
    U8     undef:5;
    U8     no_fragment:1;
    U8     truncated:1;
    U8     CRC_error:1;
};

struct _FRL_wc_fifo {
    struct _FRL_wc_fifo_element fifo_elements[_FRL_wc_fifo_depth];
    U32 read;
    U32 write;
};


#ifndef __KERNEL__
#include <sys/ioctl.h>
#endif

/* First (tentative) implementation of a native-linux-x86 version of the 
   frl. Allows things to be cleaner on a Linux system, at the cost of portability
 */

/*
 * Driver operations work the following way :
 *  - read and write not implemented
 *  - open only allowed once per device
 *  - minor number identifies senders/receivers. 32 receivers maximum (should be more than enough).
 *        minor above 32 means sender.
 *  - one ioctl read all base addresses
 *  - a mmap operation for those base addresses makes the frl_open or frl_sender_open operation ready
 *  - a setget receiver parameters ioctl allows driver/board and userland to agree on the parameters
 *  - a get receiver memory blocks, so that the receiver get the bigphys zones allocated by the kernel
 *  - a second mmap operation to mmap the bigphys zones into the process'es space.
 *  - still have to determine how to make both mmap opearations work with the same file descriptor...
 */


struct _frl_board_addresses {
  U32 base_address_0;    /* bridge */
  U32 base1_address_0;   /* FRL    */
  U32 FPGA_version;      /* bridge */
  U32 FPGA1_version;     /* FRL    */
};

struct _frl_receiver_parameters {
    U32 block_number;
    U32 block_size;
    U32 do_allocate_blocks;
};

struct _frl_spy_parameters {
    U32 block_size;
};

struct _frl_memory_blocks {
    void * free_blocks_fifo_kernel;
    void * free_blocks_fifo_physical;
    void * data_blocks_kernel;
    void * data_blocks_physical;
    void * wc_fifo_kernel;
    void * wc_fifo_physical;
};

#define FRL_MAJOR                (243)
#define FRL_IOCTL_MAGIC          (0xC1)   /* no conflict with kernel 2.4.20-8 (Redhat 9) */

#define FRL_GET_BOARD_ADDRESSES       _IOR  (FRL_IOCTL_MAGIC, 0, struct _frl_board_addresses)
#define FRL_SETGET_RECEIVER_PARAMS    _IOW  (FRL_IOCTL_MAGIC, 1, struct _frl_spy_parameters)
#define FRL_GET_RECEIVER_MEMBLOCKS    _IOR  (FRL_IOCTL_MAGIC, 2, struct _frl_memory_blocks) 
#define FRL_CONFIGURE_SPY             _IOW  (FRL_IOCTL_MAGIC, 3, struct _frl_spy_parameters)

#endif /* ifndef _FRL_KERNEL_H_ */







