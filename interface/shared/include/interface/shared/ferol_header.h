/*
 * ferol_header.h  
 *
 * This file contains the struct definition of the FEROL-header.
 * The FEROL-header is inserted by the FEROL at the top of a FED fragment
 */


#ifndef _FEROL_HEADER_H
#define _FEROL_HEADER_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


/*************************************************************************
 *
 * data structures and associated typedefs
 *
 *************************************************************************/


/*
 * FEROL header - in front of each FED header
 */

#define FEROL_SIGNATURE 0x475a

typedef struct ferolh_struct {
  uint8_t ferol[16];

  uint16_t signature() const      { return ( ferol[0]<<8 | ferol[1] ); }
  uint16_t packet_number() const  { return ( (ferol[2]&0x0e)<<8 | ferol[3] ); }
  bool is_first_packet() const    { return ( ferol[4] & 0x80 ); }
  bool is_last_packet() const     { return ( ferol[4] & 0x40 ); }
  uint32_t data_length() const    { return ( (ferol[6]&0x03)<<11 | ferol[7]<<3 ); }
  uint16_t fed_id() const         { return ( (ferol[8+2]&0x0f)<<8 | ferol[8+3] ); }
  uint32_t event_number() const   { return ( ferol[8+5]<<16 | ferol[8+6]<<8 | ferol[8+7] ); }

  void set_signature(const uint16_t value = FEROL_SIGNATURE)
  { ferol[0] = (value >> 8) & 0xff; ferol[1] = value & 0xff; }
  void set_packet_number(const uint16_t value)
  { ferol[2] = (value >> 8) & 0x0e; ferol[3] = value & 0xff; }
  void set_first_packet()
  { ferol[4] |= 0x80; }
  void set_last_packet()
  { ferol[4] |= 0x40; }
  void set_data_length(const uint32_t value)
  { ferol[6] = (value >> 11) & 0x03; ferol[7] = (value >> 3) & 0xff; }
  void set_fed_id(const uint16_t value)
  { ferol[8+2] = (value >> 8) & 0x0f; ferol[8+3] = value & 0xff; }
  void set_event_number(const uint32_t value)
  { ferol[8+5] = (value >> 16) & 0xff; ferol[8+6] = (value >> 8) & 0xff; ferol[8+7] = value & 0xff; }
} ferolh_t ;

#ifdef __cplusplus
}
#endif

#endif  /* _FEROL_HEADER_H */


