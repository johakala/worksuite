#ifndef _interface_shared_i2ogevb2g_h_
#define _interface_shared_i2ogevb2g_h_

#include "i2o/i2oDdmLib.h"
#include "i2o/i2o.h"
#include "i2o/Method.h"
//
// callback binding
//

const U8 I2O_FU_TAKE_EVENT    	= 0x30;	 // same as evbdm
const U8 I2O_ALLOCATE_N		= 0x31;  // same as evbdm
const U8 I2O_COLLECT		= 0x32;  // same as evbdm
const U8 I2O_DISCARD_N		= 0x33;  // same as evbdm

const U8 I2O_SHIP_FRAGMENTS    	= 0x51;
const U8 I2O_AVAILABLE  	= 0x53;
const U8 I2O_TRIGGER    	= 0x54;
const U8 I2O_CACHE		= 0x52;
const U8 I2O_PACKED_CACHE		= 0x55;
const U8 I2O_DATA_READY		= 0x66;
const U8 I2O_TRIGGER_ACCEPT	= 0x68;

const U8 I2O_SUPER_FRAGMENT_READY	= 0x69;

//
// interface ids ( code to be specified in XML configuration )
//
const U8 I2O_IM_RU_CLASSID  	= 0x92;
const U8 I2O_IM_EVM_CLASSID  	= 0x93;
const U8 I2O_IM_BU_CLASSID	= 0x80;   // same as evbdm


//
// gevb i2o  frame definitions 
//

typedef struct _I2O_COLLECT_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   U32				eventHandle;
   U32				fset;
#elif defined(BIG_ENDIAN__)
   I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   U32				eventHandle;
   U32				fset;
#endif
} I2O_COLLECT_MESSAGE_FRAME, *PI2O_COLLECT_MESSAGE_FRAME;





//
// New Cache for enabling little/big endian conversion
//
typedef struct _I2O_CACHE_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				elements;
	U32				fragmentId;
	U32				context;
	U32				totalLength; // in bytes
	U32				partLength; // in bytes
	U32				resourceId;
	U32				bx;	
	U32				fedid;
	U32				triggerno;
#elif defined(BIG_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				elements;
	U32				fragmentId;
	U32 				context;
	U32				totalLength; // in bytes
	U32				partLength; // in bytes
	U32				resourceId;
	U32				bx;
	U32				fedid;
	U32				triggerno;
#endif
} I2O_CACHE_MESSAGE_FRAME, *PI2O_CACHE_MESSAGE_FRAME;


//
// New Cache for enabling little/big endian conversion
//
typedef struct _I2O_PACKED_CACHE_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				frames;
#elif defined(BIG_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				frames;
#endif
} I2O_PACKED_CACHE_MESSAGE_FRAME, *PI2O_PACKED_CACHE_MESSAGE_FRAME;


/*
typedef struct _Element_Header 
{
#if defined(LITTLE_ENDIAN__)
	U32			totalLength; // in bytes
	U32			partLength; // in bytes
	U32			resourceId;
	U32			bx;
#elif defined(BIG_ENDIAN__)
	U32			totalLength; // in bytes
	U32			partLength; // in bytes
	U32			resourceId;
	U32			bx;
#endif

} ElementHeader, *PElementHeader;
*/

typedef struct _I2O_DISCARD_N_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   U32				n;
   U32				eventHandle[1];
#elif defined(BIG_ENDIAN__)
   I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   U32				n;
   U32				eventHandle[1];
#endif
} I2O_DISCARD_N_MESSAGE_FRAME, *PI2O_DISCARD_N_MESSAGE_FRAME;

typedef struct _I2O_ALLOCATE_N_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   U16				profile;
   U16				fuInstance;
   U32				fset;
   U32				n;
#elif defined(BIG_ENDIAN__)
   I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   U16				fuInstance;
   U16				profile;
   U32				fset;
   U32				n;
#endif
} I2O_ALLOCATE_N_MESSAGE_FRAME, *PI2O_ALLOCATE_N_MESSAGE_FRAME;


typedef struct _ResourceDescriptor
{
#if defined(LITTLE_ENDIAN__)
	U32			eventType;
	U32			resourceId;
	U32			context;
#elif defined(BIG_ENDIAN__)
	U32			resourceId;
	U32			eventType; 
	U32			context;
#endif

} ResourceDescriptor, *PResourceDescriptor;




typedef struct _I2O_AVAILABLE_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				resources;
	U32				destination;
	ResourceDescriptor			resource[1];
#elif defined(BIG_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				resources;
	U32				destination;
	ResourceDescriptor			resource[1];
#endif
} I2O_AVAILABLE_MESSAGE_FRAME, *PI2O_AVAILABLE_MESSAGE_FRAME;


//
// Incomming Trigger message
//
typedef struct _I2O_TRIGGER_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32   	triggers;
   	U32	bx[1];
#elif defined(BIG_ENDIAN__)
	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32   	triggers;
   	U32	bx[1];
#endif
} I2O_TRIGGER_MESSAGE_FRAME, *PI2O_TRIGGER_MESSAGE_FRAME;



// tell the number of trigger the EVM can accept
// This number can be used by the trigger supervisor 
// to program a backpressure mechanism


typedef struct _I2O_TRIGGER_ACCEPT_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				tokens;
#elif defined(BIG_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				tokens;
#endif
} I2O_TRIGGER_ACCEPT_MESSAGE_FRAME, *PI2O_TRIGGER_ACCEPT_MESSAGE_FRAME;



typedef struct _ShipRequest 
{
#if defined(LITTLE_ENDIAN__)
	U32			bx; 
	U32			destination;
	U32			resourceId;
	U32 		context;
#elif defined(BIG_ENDIAN__)
	U32			bx; 
	U32			resourceId;
	U32			destination; 
	U32			context;
#endif

} ShipRequest, *PShipRequest;

//
// New Cache for enabling little/big endian conversion
//
typedef struct _I2O_SHIP_FRAGMENTS_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				fragments;
	ShipRequest			request[1];
#elif defined(BIG_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				fragments;
	ShipRequest			request[1];
#endif
} I2O_SHIP_FRAGMENTS_MESSAGE_FRAME, *PI2O_SHIP_FRAGMENTS_MESSAGE_FRAME;

//
// FEDKit Data Format for enabling little/big endian conversion
// A chain of one or more of this blocks make a data fragment
// This message format is compatible with the ' zero copy cache' message
// to allow zero copy implementation in RU. The frame is 64 bits aligned.

	
typedef struct _I2O_DATA_READY_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame; // 28 bytes
   	U32				reserved;
	U32				reserved0;
	U32				reserved1;
	U32				totalLength; // in bytes
	U32				partLength; // in bytes
	U32				reserved2;
	U32				reserved3;
	U32				fedid;
	U32				triggerno;
#elif defined(BIG_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				reserved;
	U32				reserved0;
	U32 				reserved1;
	U32				totalLength; // in bytes
	U32				partLength; // in bytes
	U32				reserved2;
	U32				reserved3;
	U32				fedid;
	U32				triggerno;
#endif
} I2O_DATA_READY_MESSAGE_FRAME, *PI2O_DATA_READY_MESSAGE_FRAME;

typedef struct _I2O_FU_TAKE_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   I2O_PRIVATE_MESSAGE_FRAME   		PvtMessageFrame;
   U32					eventHandle;
   I2O_SG_ELEMENT		      	Fragment32;
#elif defined(BIG_ENDIAN__)
   I2O_PRIVATE_MESSAGE_FRAME   		PvtMessageFrame;
   U32					eventHandle;
   I2O_SG_ELEMENT      			Fragment32;
#endif
} I2O_FU_TAKE_MESSAGE_FRAME, *PI2O_FU_TAKE_MESSAGE_FRAME;

typedef struct _I2O_SUPER_FRAGMENT_READY_MESSAGE_FRAME {
#if defined(LITTLE_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame; // 28 bytes
   	U32				reserved;
	U32				reserved0;
	U32				reserved1;
	U32				totalLength; // in bytes
	U32				partLength; // in bytes
	U32				reserved2;
	U32				reserved3;
	U32				fedid;
	U32				triggerno;
#elif defined(BIG_ENDIAN__)
   	I2O_PRIVATE_MESSAGE_FRAME   	PvtMessageFrame;
   	U32				reserved;
	U32				reserved0;
	U32 				reserved1;
	U32				totalLength; // in bytes
	U32				partLength; // in bytes
	U32				reserved2;
	U32				reserved3;
	U32				fedid;
	U32				triggerno;
#endif
} I2O_SUPER_FRAGMENT_READY_MESSAGE_FRAME, *PI2O_SUPER_FRAGMENT_READY_MESSAGE_FRAME;

#endif
