// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_version_h_
#define _ibvla_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define IBVLA_VERSION_MAJOR 2
#define IBVLA_VERSION_MINOR 0
#define IBVLA_VERSION_PATCH 1
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define IBVLA_PREVIOUS_VERSIONS "2.0.0"

//
// Template macros
//
#define IBVLA_VERSION_CODE PACKAGE_VERSION_CODE(IBVLA_VERSION_MAJOR,IBVLA_VERSION_MINOR,IBVLA_VERSION_PATCH)
#ifndef IBVLA_PREVIOUS_VERSIONS
#define IBVLA_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(IBVLA_VERSION_MAJOR,IBVLA_VERSION_MINOR,IBVLA_VERSION_PATCH)
#else
#define IBVLA_FULL_VERSION_LIST  IBVLA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(IBVLA_VERSION_MAJOR,IBVLA_VERSION_MINOR,IBVLA_VERSION_PATCH)
#endif

namespace ibvla
{
	const std::string package = "ibvla";
	const std::string versions = IBVLA_FULL_VERSION_LIST;
	const std::string summary = "InfiniBand Verbs Layered Architecture";
	const std::string description = "C++ wrapper of the ibverbs library for InfiniBand";
	const std::string authors = "Andy Forrest, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
