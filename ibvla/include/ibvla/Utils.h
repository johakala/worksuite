// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_Utils_h
#define _ibvla_Utils_h

#include "ibvla/Device.h"
#include "ibvla/Context.h"
#include "ibvla/QueuePair.h"

#include <list>

#include <byteswap.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN
static inline uint64_t htonll (uint64_t x)
{
	return bswap_64(x);
}
static inline uint64_t ntohll (uint64_t x)
{
	return bswap_64(x);
}
#elif __BYTE_ORDER == __BIG_ENDIAN
static inline uint64_t htonll(uint64_t x)
{
	return x;
}
static inline uint64_t ntohll(uint64_t x)
{
	return x;
}
#else
#error __BYTE_ORDER is neither __LITTLE_ENDIAN nor __BIG_ENDIAN
#endif

namespace ibvla
{
	std::list<Device> getDeviceList () throw (ibvla::exception::Exception);
	void freeDeviceList (std::list<Device> & dlist) throw (ibvla::exception::Exception);

	Context createContext (Device & d) throw (ibvla::exception::Exception);

	void destroyContext (Context & c) throw (ibvla::exception::Exception);

	ibv_mtu mtu_to_enum (unsigned mtu);

	// returns a description of the event in relation to the actual event that occurred
	std::string toString (struct ibv_async_event *event);
	// returns a general description of the event
	std::string asyncEventToString (size_t event);
	// returns the event name as documented (e.g. IBV_EVENT_CQ_ERR)
	std::string asyncEventToStringLiteral (size_t event); // returns the name of the error as documented

	// returns a description of the work completion in relation to the actual event that occurred
	std::string toString (struct ibv_wc *wc);
	// returns a general description of the event
	std::string workCompletionToString (size_t event);
	// returns the event name as documented (e.g. IBV_WC_LOC_LEN_ERR)
	std::string workCompletionToStringLiteral (size_t event);

	// returns a string representation of the QP state
	std::string stateToString (ibvla::QueuePair & qp);
}

#endif
