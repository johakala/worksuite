// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_CompletionQueue_h
#define _ibvla_CompletionQueue_h

#include <infiniband/verbs.h>

#include "ibvla/exception/Exception.h"

#include "ibvla/Context.h"

namespace ibvla
{
	class CompletionQueue
	{
		public:
			Context context_;
			ibv_cq *cq_;

			CompletionQueue (Context &ctx, ibv_cq *cq);
			CompletionQueue ()
			{
				cq_ = 0;
			}

			~CompletionQueue ();

			Context& getContext ();

			void resize (int cqe) throw (ibvla::exception::Exception);

			unsigned poll (int num_entries, ibv_wc *wc) throw (ibvla::exception::Exception);
	};
}

#endif
