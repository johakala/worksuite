// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_ProtectionDomain_h
#define _ibvla_ProtectionDomain_h

#include <infiniband/verbs.h>

#include "ibvla/exception/Exception.h"

#include "ibvla/Context.h"

#include <stdint.h>

namespace ibvla
{

	class MemoryRegion;
	class QueuePair;
	//class AddressHandle;
	class CompletionQueue;

	class ProtectionDomain
	{
		public:

			Context context_;
			ibv_pd *pd_;

			ProtectionDomain ()
			{
				pd_ = 0;
			}

			ProtectionDomain (Context & ctx, ibv_pd *pd);
			~ProtectionDomain ();

			Context& getContext ();

			MemoryRegion registerMemoryRegion (void *addr, size_t length, ibv_access_flags access) throw (ibvla::exception::Exception);
			void deregisterMemoryRegion (MemoryRegion & mr) throw (ibvla::exception::Exception);

			QueuePair createQueuePair (ibvla::CompletionQueue & s, ibvla::CompletionQueue & r, uint32_t max_send_wr, uint32_t max_recv_wr, void* qp_context) throw (ibvla::exception::Exception);
			void destroyQueuePair (ibvla::QueuePair& qp) throw (ibvla::exception::Exception);

			//AH used when a Send Request (SR) is posted to an Unreliable Datagram QP
			//AddressHandle createAddressHandle (ibv_ah_attr &attr) throw (ibvla::exception::Exception);
			//void destroyAddressHandle (AddressHandle& ah) throw (ibvla::exception::Exception);
	};
}

#endif
