// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_exception_InternalError_h_
#define _ibvla_exception_InternalError_h_

#include "ibvla/exception/Exception.h"

namespace ibvla
{
	namespace exception
	{
		class InternalError : public ibvla::exception::Exception
		{
			public:
				InternalError (std::string name, std::string message, std::string module, int line, std::string function)
					: ibvla::exception::Exception(name, message, module, line, function)
				{
				}

				InternalError (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
					: ibvla::exception::Exception(name, message, module, line, function, e)
				{
				}
		};
	}
}

#endif
