// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/Buffer.h"

#include <stdio.h>
#include <string.h>

ibvla::Buffer::Buffer (ibvla::MemoryRegion & mr, toolbox::mem::Pool * pool, size_t size, void* address)
	: toolbox::mem::Buffer(pool, size, address), mr_(mr)
{
	memset(&buffer_sge_list, 0, sizeof(buffer_sge_list));

	buffer_sge_list.addr = (uintptr_t) address;
	buffer_sge_list.length = size;
	buffer_sge_list.lkey = mr_.getLKey();

	op_sge_list.addr = 0;
	op_sge_list.lkey = 0;
	op_sge_list.length = 0;
}

ibvla::MemoryRegion ibvla::Buffer::getMemoryRegion ()
{
	return mr_;
}

