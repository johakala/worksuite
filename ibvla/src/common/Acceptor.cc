// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/Acceptor.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <iostream>
#include <stdexcept>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <getopt.h>
#include <arpa/inet.h>
#include <time.h>

#include "ibvla/AcceptorListener.h"
#include "ibvla/ConnectionRequest.h"
#include "ibvla/QueuePair.h"
#include "ibvla/Utils.h"

#include "toolbox/task/WorkLoopFactory.h"

ibvla::Acceptor::Acceptor (const std::string & name, ibvla::AcceptorListener * listener, size_t mtu, ibvla::CompletionQueue cqr, toolbox::mem::Pool * rpool, size_t ibPort,size_t ibPath)
	: name_(name), listener_(listener), cqr_(cqr), rpool_(rpool), ibPort_(ibPort), ibPath_(ibPath)
{
	listenfd_ = -1;

	mtu_ = mtu_to_enum(mtu);

	srand48 (time (NULL)); // seed the random generator for QP packet number initialization

	process_ = toolbox::task::bind(this, &ibvla::Acceptor::process, "process");
}

bool ibvla::Acceptor::process (toolbox::task::WorkLoop* wl)
{
	errno = 0;

	struct sockaddr_in readAddr;
	socklen_t readAddrLen = sizeof(readAddr);

	int connfd = ::accept(listenfd_, (struct sockaddr *) &readAddr, &readAddrLen);

	if (connfd < 0)
	{
		std::stringstream ss;
		ss << "WARNING : Failed to accept on " << name_ << ", " << strerror(errno) << "";
		std::cout << ss.str() << std::endl;
		return true;
	}

	char msg[sizeof "0000:000000:000000"];

#warning unsafe read

	int n = read(connfd, msg, sizeof msg);
	if (n != sizeof msg)
	{
		close(connfd);
		std::stringstream ss;
		ss << "WARNING : Failed to read";
		std::cout << ss.str() << std::endl;
		return true;
	}

	int lid;
	int psn;
	int qpn;
	sscanf(msg, "%x:%x:%x", &lid, &qpn, &psn);

	std::string ip;
	ip = inet_ntoa(readAddr.sin_addr);
	struct hostent * h = gethostbyaddr((char*) &readAddr.sin_addr.s_addr, sizeof(readAddr.sin_addr.s_addr), AF_INET);
	std::string remoteHostName;
	if (h != 0)
	{
		remoteHostName = h->h_name;
	}
	else
	{
		remoteHostName = "";
	}

	//std::cout << "Received connection info from remote address " << ip << ", lid='" << lid << "', qpn='" << qpn << "', psn='" << psn << "', msg='" << msg << std::endl;

	listener_->connectionRequest(ibvla::ConnectionRequest(lid, psn, qpn, this, connfd, ip, remoteHostName));

	return true;
}

void ibvla::Acceptor::listen (const std::string & hostname, const std::string & port) throw (ibvla::exception::Exception)
{
	struct addrinfo *res, *t;
	struct addrinfo hints;
	memset(&hints, 0, sizeof(hints));

	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	int ret;

	ret = getaddrinfo(hostname.c_str(), port.c_str(), &hints, &res);

	if (ret < 0)
	{
		std::stringstream ss;
		ss << "Failed to get address info for host:port '" << hostname << ":" << port << ", " << gai_strerror(ret);
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	for (t = res; t; t = t->ai_next)
	{
		errno = 0;
		listenfd_ = socket(t->ai_family, t->ai_socktype, t->ai_protocol);
		if (listenfd_ < 0)
		{
			std::stringstream ss;
			ss << "Failed to create socket on host:port '" << hostname << ":" << port << ", " << strerror(errno);
			XCEPT_RAISE(ibvla::exception::Exception, ss.str());
		}

		errno = 0;
		int n = 1;
		int ret = setsockopt(listenfd_, SOL_SOCKET, SO_REUSEADDR, &n, sizeof n);
		if (ret < 0)
		{
			close(listenfd_);
			std::stringstream ss;
			ss << "Failed to set socket options on host:port '" << hostname << ":" << port << ", " << strerror(errno);
			XCEPT_RAISE(ibvla::exception::Exception, ss.str());
		}

		errno = 0;
		ret = bind(listenfd_, t->ai_addr, t->ai_addrlen);
		if (ret != 0)
		{
			close(listenfd_);
			std::stringstream ss;
			ss << "Failed to bind socket on host:port '" << hostname << ":" << port << ", " << strerror(errno);
			XCEPT_RAISE(ibvla::exception::Exception, ss.str());
		}
		break;
	}

	freeaddrinfo(res);

	ret = ::listen(listenfd_, 1024);
	if (ret != 0)
	{
		close(listenfd_);
		std::stringstream ss;
		ss << "Failed to listen on host:port '" << hostname << ":" << port << ", " << strerror(errno);
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	// start accept work-loop
	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "waiting")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "waiting")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		XCEPT_RETHROW(ibvla::exception::Exception, "Failed to submit job", e);

	}
	catch (std::exception& se)
	{
		std::stringstream msg;
		msg << "Failed to submit notification to worker thread, caught standard exception '";
		msg << se.what() << "'";
		XCEPT_RAISE(ibvla::exception::Exception, msg.str());
	}
	catch (...)
	{
		XCEPT_RAISE(ibvla::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}
}

// By the end of this call, the QP is RTS && RTR
void ibvla::Acceptor::accept (ibvla::ConnectionRequest &id, ibvla::QueuePair &qp) throw (ibvla::exception::Exception)
{
	// Query qp to check that it is in the correct state //, and that the port has been set
	struct ibv_qp_attr qp_attr;
	struct ibv_qp_init_attr qp_init_attr;
	int querymask = IBV_QP_PORT | IBV_QP_STATE;
	try
	{
		qp.query(querymask, qp_attr, qp_init_attr);
	}
	catch (ibvla::exception::Exception & e)
	{
		close(id.sockfd_);
		XCEPT_RETHROW(ibvla::exception::Exception, "Failed to query queue pair, cannot connect queue pair to remote", e);
	}

	if (qp_attr.qp_state != IBV_QPS_INIT)
	{
		close(id.sockfd_);
		std::stringstream ss;
		ss << "Queue pair not in initialized state, cannot connect to remote QP '"; // << id << "';
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
	/*
	 if (qp_attr.port_num <= 0)
	 {
	 close(id.sockfd_);
	 std::stringstream ss;
	 ss << "Queue pair port not set, cannot connect to remote QP '"; // << id << "';
	 XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	 }
	 */

	// Move the QP into RTR state
	ibv_qp_attr attr;
	memset(&attr, 0, sizeof(attr));

	//params already set
	//sscanf(msg, "%x:%x:%x", &attr.ah_attr.dlid, &attr.dest_qp_num, &attr.rq_psn);

	attr.ah_attr.dlid = id.remote_lid_;
	attr.dest_qp_num = id.remote_qpn_;
	attr.rq_psn = id.remote_psn_;

	//std::cout << "ibvla::Acceptor::accept : Connecting to remote QP, lid = '" << attr.ah_attr.dlid << "', psn = '" << attr.rq_psn << "', qp_num = '" << attr.dest_qp_num << "'" << std::endl;

	attr.qp_state = IBV_QPS_RTR;

	attr.path_mtu = mtu_;

	attr.max_dest_rd_atomic = 1;
	attr.min_rnr_timer = 0;
	attr.ah_attr.is_global = 0;
	attr.ah_attr.sl = 0; //!!!!!!!!?????????
	attr.ah_attr.src_path_bits = ibPath_;

	//std::cout << "setting src_path to " << ibPath_ << std::endl;

	// should have been set during transition from RESET to INIT
	attr.ah_attr.port_num = qp_attr.port_num;

	//qp.modify(&attr, ibv_qp_attr_mask(IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC | IBV_QP_MIN_RNR_TIMER));
	try
	{
		qp.modify(attr, ibv_qp_attr_mask(IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC | IBV_QP_MIN_RNR_TIMER));
	}
	catch (ibvla::exception::Exception & e)
	{
		close(id.sockfd_);
		std::stringstream ss;
		ss << "Failed to move local QP into RTR state";
		XCEPT_RETHROW(ibvla::exception::Exception, ss.str(), e);
	}

	/*
	 // Move the QP into a RTS state

	 int local_psn = lrand48() & 0xffffff; // Packet Sequence Number

	 attr.qp_state = IBV_QPS_RTS;
	 attr.timeout = 14;
	 attr.retry_cnt = 7;
	 attr.rnr_retry = 6; // 7 is a special number, it means infinite
	 attr.sq_psn = local_psn;
	 attr.max_rd_atomic = 1;
	 try
	 {
	 qp.modify(attr, ibv_qp_attr_mask(IBV_QP_STATE | IBV_QP_TIMEOUT | IBV_QP_RETRY_CNT | IBV_QP_RNR_RETRY | IBV_QP_SQ_PSN | IBV_QP_MAX_QP_RD_ATOMIC));
	 }
	 catch (ibvla::exception::Exception & e)
	 {
	 close(id.sockfd_);
	 std::stringstream ss;
	 ss << "Failed to move local QP into RTS state";
	 XCEPT_RETHROW(ibvla::exception::Exception, ss.str(), e);
	 }
	 */

	// Communicate information back to remote to complete connection
	// get port attributes
	struct ibv_port_attr p_att = qp.pd_.context_.queryPort(qp_attr.port_num);

	char msg[sizeof "0000:000000:000000"];
	sprintf(msg, "%04x:%06x:%06x", (p_att.lid + (unsigned int)ibPath_), qp.getNum(), attr.sq_psn);

	//std::cout << "REPLY to remote address, lid='" << p_att.lid << "', qpn='" << qp.getNum() << "', psn='" << attr.sq_psn << "', msg='" << msg << std::endl;

	if (write(id.sockfd_, msg, sizeof msg) != sizeof msg)
	{
		close(id.sockfd_);
		std::stringstream ss;
		ss << "Failed to respond to client, connection establishment failed";
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	// read "done" from client, dont care if this fails atm
	read(id.sockfd_, msg, sizeof msg);

	close(id.sockfd_);
}

void ibvla::Acceptor::reject (ibvla::ConnectionRequest &id) throw (ibvla::exception::Exception)
{
	close(id.sockfd_);
}
