// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/CompletionWorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include <sstream>

ibvla::CompletionWorkLoop::CompletionWorkLoop (const std::string & name, ibvla::CompletionQueue cq, ibvla::WorkRequestHandler * handler) throw (ibvla::exception::Exception)
	: cq_(cq), handler_(handler)
{
	name_ = name;
	process_ = toolbox::task::bind(this, &ibvla::CompletionWorkLoop::process, "process");

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		XCEPT_RETHROW(ibvla::exception::Exception, "Failed to submit job", e);

	}
	catch (std::exception& se)
	{
		std::stringstream msg;
		msg << "Failed to submit notification to worker thread, caught standard exception '" << se.what() << "'";
		XCEPT_RAISE(ibvla::exception::Exception, msg.str());
	}
	catch (...)
	{
		XCEPT_RAISE(ibvla::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}

	emptyDispatcherQueueCounter_ = 0;
	receivedEventCounter_ = 0;
}

ibvla::CompletionWorkLoop::~CompletionWorkLoop ()
{
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->cancel();
}

bool ibvla::CompletionWorkLoop::process (toolbox::task::WorkLoop* wl)
{
	struct ibv_wc wc;
	int ret;

	while (1)
	{
		try
		{
			ret = cq_.poll(1, &wc);
		}
		catch (ibvla::exception::Exception & e)
		{
			std::stringstream ss;
			ss << "Failed process event queue '";
			ss << e.what() << "'";
			std::cout << ss.str() << std::endl;
			return false;
		}
		if (ret == 0)
		{
			emptyDispatcherQueueCounter_++;
			continue;
		}

		receivedEventCounter_++;
		handler_->handleWorkRequest(&wc);
	}

	return true;
}

