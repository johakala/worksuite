// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/QueuePair.h"

#include "ibvla/Utils.h"

#include <errno.h>
#include <string.h>
#include <sstream>

ibvla::QueuePair::QueuePair (ibvla::ProtectionDomain &pd, ibv_qp * qp)
	: pd_(pd), qp_(qp)
{
}

ibvla::QueuePair::~QueuePair ()
{
}

void ibvla::QueuePair::modify (ibv_qp_attr &attr, int attr_mask) throw (ibvla::exception::Exception)
{
	errno = 0;
	int ret = ibv_modify_qp(qp_, &attr, attr_mask);
	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to modify queue pair, errno = " << strerror(errno);
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}

/*
 * @param in attr_mask
 * @param out attr
 * @param out init_attr
 */
void ibvla::QueuePair::query (int attr_mask, ibv_qp_attr &attr, ibv_qp_init_attr &init_attr) throw (ibvla::exception::Exception)
{
	errno = 0;
	memset(&attr, 0, sizeof(ibv_qp_attr));
	memset(&init_attr, 0, sizeof(ibv_qp_init_attr));

	int ret = ibv_query_qp(qp_, &attr, attr_mask, &init_attr);

	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to query queue pair, errno = " << strerror(errno);
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
		//throw error("ibv_query_qp", result);
	}
}

void ibvla::QueuePair::postSend (ibv_send_wr &wr) throw (ibvla::exception::InternalError, ibvla::exception::QueueFull)
{
	errno = 0;
	ibv_send_wr *bad_wr;

	int ret = ibv_post_send(qp_, &wr, &bad_wr);
	if (ret != 0)
	{
		std::stringstream ss;
		// warning, errno is not being set correctly by this call
		if (errno == EINVAL)
		{
			ss << "Failed to post send queue pair with invalid value in WR, " << strerror(errno) << "(" << errno << ")";
		}
		else if (errno == ENOMEM)
		{
			ss << "Failed to post send queue pair, not enough resources, " << strerror(errno) << "(" << errno << ")";
			XCEPT_RAISE(ibvla::exception::QueueFull, ss.str());
		}
		else if (errno == EFAULT)
		{
			ss << "Failed to post send queue pair, invalid value in QP, " << strerror(errno) << "(" << errno << ")";
		}
		else
		{
			ss << "Not understood error, (errno=" << errno << ", return value=" << ret << ")";
		}
		XCEPT_RAISE(ibvla::exception::InternalError, ss.str());
	}
}

void ibvla::QueuePair::postRecv (ibv_recv_wr &wr) throw (ibvla::exception::Exception)
{
	errno = 0;
	ibv_recv_wr *bad_wr;

	int ret = ibv_post_recv(qp_, &wr, &bad_wr);
	if (ret != 0)
	{
		std::stringstream ss;
		ss << "Failed to post recv on queue pair, errno = " << strerror(errno);
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}

uint32_t ibvla::QueuePair::getNum ()
{
	return qp_->qp_num;
}

enum ibv_qp_state ibvla::QueuePair::getState ()
{
	return qp_->state;
}

void* ibvla::QueuePair::getContext()
{
	return qp_->qp_context;
}

// USE FOR DEBUG ONLY
namespace ibvla
{
	std::ostream& operator<< (std::ostream &ss, ibvla::QueuePair & qp)
	{
		ss << "QPN = " << qp.qp_->qp_num;
		ss << " context[" << qp.qp_->context << "]";
		ss << ", pd[" << qp.qp_->pd << "]";
		ss << ", send_cq[" << qp.qp_->send_cq << "]";
		ss << ", recv_cq[" << qp.qp_->recv_cq << "]";
		ss << ", srq[" << qp.qp_->srq << "]";
		ss << ", STATE = '" << ibvla::stateToString(qp) << "'";
		//ss << ", eventsCompleted = '" << qp.qp_->events_completed << "'";

		//ss << ", remote QPN # -1";

		return ss;
	}
}
