// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/Allocator.h"

#include "ibvla/Buffer.h"
#include "ibvla/Context.h"

#include "toolbox/PolicyFactory.h"
#include "toolbox/AllocPolicy.h"

#include <sstream>
#include <string>
#include <limits>

#include "toolbox/string.h"

ibvla::Allocator::Allocator (ProtectionDomain & pd, const std::string & name, size_t committedSize) throw (toolbox::mem::exception::FailedCreation)
	: pd_(pd), name_(name)
{
	committedSize_ = committedSize;

	if (committedSize > static_cast<size_t>(std::numeric_limits < ssize_t > ::max()))
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator, requested size " << committedSize << ", allowed size " << std::numeric_limits < ssize_t > ::max();
		XCEPT_RAISE(toolbox::mem::exception::FailedCreation, msg.str());
	}

	toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
	toolbox::AllocPolicy * policy = 0;

	try
	{
		std::stringstream ss;
		ss << this->name_ << "-" << this->pd_.getContext().getDeviceName();
		toolbox::net::URN urn(ss.str(), this->type());
		policy = dynamic_cast<toolbox::AllocPolicy *>(factory->getPolicy(urn, "alloc"));
		buffer_ = (unsigned char*) (policy->alloc(committedSize));
	}
	catch (toolbox::exception::Exception & e)
	{
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, "Failed to allocate memory", e);
	}

	try
	{

		//buffer_ = new unsigned char[committedSize];

		// physical, user and kernel address are all the same in this case
		memPartition_.addToPool(buffer_, buffer_, buffer_, committedSize);
	}
	catch (toolbox::mem::exception::Corruption& ce)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, msg.str(), ce);
	}
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, msg.str(), ia);
	}
	catch (toolbox::mem::exception::MemoryOverflow& mo)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes";
		XCEPT_RETHROW(toolbox::mem::exception::FailedCreation, msg.str(), mo);
	}
	catch (std::bad_alloc& e)
	{
		std::stringstream msg;
		msg << "Failed to create committed heap allocator with memory region of " << committedSize << " bytes, reason: " << e.what();
		XCEPT_RAISE(toolbox::mem::exception::FailedCreation, msg.str());
	}
}

ibvla::Allocator::~Allocator ()
{
	delete buffer_;
}

toolbox::mem::Buffer * ibvla::Allocator::alloc (size_t size, toolbox::mem::Pool * pool) throw (toolbox::mem::exception::FailedAllocation)
{
	if (size > static_cast<size_t>(std::numeric_limits < ssize_t > ::max()))
	{
		std::stringstream msg;
		msg << "Failed to allocate " << size << " bytes from committed region(ibvla::Allocator) " << committedSize_ << ", maximum allowed is " << std::numeric_limits < ssize_t > ::max();
		XCEPT_RAISE(toolbox::mem::exception::FailedAllocation, msg.str());
	}

	unsigned char* buffer = 0;

	size_t maxSize = pd_.getContext().queryDevice().max_mr_size;
	if (size > maxSize)
	{
		std::stringstream msg;
		msg << "Failed to allocate " << size << " bytes from committed region(ibvla::Allocator) " << committedSize_ << ", maximum allowed is " << maxSize;
		XCEPT_RAISE(toolbox::mem::exception::FailedAllocation, msg.str());
	}
	try
	{
		//size_t alignedSize = size + DAT_OPTIMAL_ALIGNMENT;
		size_t alignedSize = size;

		buffer = (unsigned char*) memPartition_.alloc(alignedSize);

		ibvla::MemoryRegion mr = pd_.registerMemoryRegion((void*) buffer, size, IBV_ACCESS_LOCAL_WRITE);

		toolbox::mem::Buffer* bufPtr = new ibvla::Buffer(mr, pool, size, buffer);

		return bufPtr;
	}
	catch (ibvla::exception::Exception & e)
	{
		// free memory allocated bfore throwing 
		memPartition_.free((char*) buffer);

		BufferSizeType curalloc, totfree, maxfree;
		BufferSizeType nget, nrel;
		memPartition_.stats(&curalloc, &totfree, &maxfree, &nget, &nrel);
		std::string msg = toolbox::toString("Failed to create memory region of size %d bytes. Bytes used %d, bytes free %d, max free %d, number allocated %d, number released %d", size, curalloc, totfree, maxfree, nget, nrel);
		XCEPT_RETHROW(toolbox::mem::exception::FailedAllocation, msg, e);
	}
	catch (toolbox::mem::exception::FailedAllocation& fa)
	{
		BufferSizeType curalloc, totfree, maxfree;
		BufferSizeType nget, nrel;
		memPartition_.stats(&curalloc, &totfree, &maxfree, &nget, &nrel);
		std::string msg = toolbox::toString("Out of memory in ibvla::Allocator while allocating %d bytes. Bytes used %d, bytes free %d, max free %d, number allocated %d, number released %d", size, curalloc, totfree, maxfree, nget, nrel);
		XCEPT_RETHROW(toolbox::mem::exception::FailedAllocation, msg, fa);
	}
}

void ibvla::Allocator::free (toolbox::mem::Buffer * buffer) throw (toolbox::mem::exception::FailedDispose)
{
	try
	{
		ibvla::MemoryRegion mr = dynamic_cast<ibvla::Buffer*>(buffer)->getMemoryRegion();
		pd_.deregisterMemoryRegion(mr);
	}
	catch (ibvla::exception::Exception & e)
	{
		// this is fatal
		XCEPT_RETHROW(toolbox::mem::exception::FailedDispose, "Cannot destroy memory region, memory corruption", e);
	}

	try
	{
		memPartition_.free((char*) buffer->getAddress());
		delete buffer;
	}
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		// Retry possible, maybe pointer was only wrong, don't delete buffer
		std::string msg = toolbox::toString("Cannot free buffer %x(ibvla::Allocator), invalid pointer", buffer);
		XCEPT_RETHROW(toolbox::mem::exception::FailedDispose, msg, ia);
	}
	catch (toolbox::mem::exception::Corruption& c)
	{
		delete buffer; // delete in any case, there's a corruption, continuatio is actually not possible anymore
		XCEPT_RETHROW(toolbox::mem::exception::FailedDispose, "Cannot free buffer, memory corruption", c);
	}
}

std::string ibvla::Allocator::type ()
{
	return "ibvla";
}

bool ibvla::Allocator::isCommittedSizeSupported ()
{
	return true;
}

size_t ibvla::Allocator::getCommittedSize ()
{
	return committedSize_;
}

size_t ibvla::Allocator::getUsed ()
{
	return memPartition_.getUsed();
}
