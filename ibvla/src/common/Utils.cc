// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/Utils.h"

#include <infiniband/verbs.h>

#include <errno.h>
#include <string.h>
#include <sstream>

static struct ibv_device **devlist = 0;

std::list<ibvla::Device> ibvla::getDeviceList () throw (ibvla::exception::Exception)
{
#ifdef SUSPENDED
	int rc = ibv_fork_init();
	if (rc)
	{
		std::stringstream ss;
		ss << "Failure to initialize fork for ibv";
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
#endif
	errno = 0;

	int num_devices;

	if (devlist == 0)
	{
		devlist = ibv_get_device_list(&num_devices);
	}
	if (devlist == 0)
	{
		std::stringstream ss;
		ss << "Failure to get device list, errno = " << ::strerror(errno);
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	//std::cout << "Copying struct then freeing. This might not work, as freeing them may make them unusable in the future." << std::endl;

	std::list<Device> devices;
	for (int i = 0; i < num_devices; i++)
	{
		ibvla::Device d(devlist[i]);
		devices.push_back(d);
	}

	return devices;
}

void ibvla::freeDeviceList (std::list<ibvla::Device> & dlist) throw (ibvla::exception::Exception)
{

	if (devlist != 0)
	{
		ibv_free_device_list(devlist);
		devlist = 0;
	}
	else
	{
		XCEPT_RAISE(ibvla::exception::Exception, "Invalid device list");
	}
}

ibvla::Context ibvla::createContext (ibvla::Device & d) throw (ibvla::exception::Exception)
{
	errno = 0;

	ibv_context *ctx = ibv_open_device(d.device_);

	if (ctx == 0)
	{
		std::stringstream ss;
		ss << "Could not create context for device '" << d.getName() << "', GUID = ' 0x" << std::hex << ntohll(d.getGUID()) << std::dec << "'";
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return ibvla::Context(ctx);
}

void ibvla::destroyContext (ibvla::Context & c) throw (ibvla::exception::Exception)
{
	int i = ibv_close_device(c.context_);

	if (i == -1)
	{
		std::stringstream ss;
		ss << "Failure to destroy context";
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}

ibv_mtu ibvla::mtu_to_enum (unsigned mtu)
{
	switch (mtu)
	{
		case 256:
			return IBV_MTU_256;
		case 512:
			return IBV_MTU_512;
		case 1024:
			return IBV_MTU_1024;
		case 2048:
			return IBV_MTU_2048;
		case 4096:
			return IBV_MTU_4096;
		default:
			return IBV_MTU_4096;
	}
}

// returns a description of the event in relation to the actual event that occurred
std::string ibvla::toString (struct ibv_async_event *event)
{
	std::stringstream ss;
	switch (event->event_type)
	{
		/* QP events */
		case IBV_EVENT_QP_FATAL:
			ss << "QP fatal event for QP #" << event->element.qp->qp_num;
			break;
		case IBV_EVENT_QP_REQ_ERR:
			ss << "QP Requestor error for QP #" << event->element.qp->qp_num;
			break;
		case IBV_EVENT_QP_ACCESS_ERR:
			ss << "QP access error event for QP #" << event->element.qp->qp_num;
			break;
		case IBV_EVENT_COMM_EST:
			ss << "QP communication established event for QP #" << event->element.qp->qp_num;
			break;
		case IBV_EVENT_SQ_DRAINED:
			ss << "QP Send Queue drained event for QP #" << event->element.qp->qp_num;
			break;
		case IBV_EVENT_PATH_MIG:
			ss << "QP Path migration loaded event for QP #" << event->element.qp->qp_num;
			break;
		case IBV_EVENT_PATH_MIG_ERR:
			ss << "QP Path migration error event for QP #" << event->element.qp->qp_num;
			break;
		case IBV_EVENT_QP_LAST_WQE_REACHED:
			ss << "QP last WQE reached event for QP #" << event->element.qp->qp_num;
			break;

			/* CQ events */
		case IBV_EVENT_CQ_ERR:
			ss << "CQ error for CQ with handle " << event->element.cq; // %p
			break;

			/* SRQ events */
		case IBV_EVENT_SRQ_ERR:
			ss << "SRQ error for SRQ with handle " << event->element.srq; // %p
			break;
		case IBV_EVENT_SRQ_LIMIT_REACHED:
			ss << "SRQ limit reached event for SRQ with handle " << event->element.srq; // %p
			break;

			/* Port events */
		case IBV_EVENT_PORT_ACTIVE:
			ss << "Port active event for port number " << event->element.port_num; // %d
			break;
		case IBV_EVENT_PORT_ERR:
			ss << "Port error event for port number " << event->element.port_num;
			break;
		case IBV_EVENT_LID_CHANGE:
			ss << "LID change event for port number " << event->element.port_num;
			break;
		case IBV_EVENT_PKEY_CHANGE:
			ss << "P_Key table change event for port number " << event->element.port_num;
			break;
		case IBV_EVENT_GID_CHANGE:
			ss << "GID table change event for port number " << event->element.port_num;
			break;
		case IBV_EVENT_SM_CHANGE:
			ss << "SM change event for port number " << event->element.port_num;
			break;
		case IBV_EVENT_CLIENT_REREGISTER:
			ss << "Client reregister event for port number " << event->element.port_num;
			break;

			/* RDMA device events */
		case IBV_EVENT_DEVICE_FATAL:
			ss << "Fatal error event for device (IBV_EVENT_DEVICE_FATAL)";
			break;

		default:
			ss << "Unknown event (" << event->event_type << ")";
	}

	return ss.str();
}

// returns a general description of the event
std::string ibvla::asyncEventToString (size_t event)
{
	std::stringstream ss;
	switch (event)
	{
		/* QP events */
		case IBV_EVENT_QP_FATAL:
			ss << "QP fatal event";
			break;
		case IBV_EVENT_QP_REQ_ERR:
			ss << "QP Requestor error";
			break;
		case IBV_EVENT_QP_ACCESS_ERR:
			ss << "QP access error";
			break;
		case IBV_EVENT_COMM_EST:
			ss << "QP communication established";
			break;
		case IBV_EVENT_SQ_DRAINED:
			ss << "QP Send Queue drained";
			break;
		case IBV_EVENT_PATH_MIG:
			ss << "QP Path migration loaded";
			break;
		case IBV_EVENT_PATH_MIG_ERR:
			ss << "QP Path migration error";
			break;
		case IBV_EVENT_QP_LAST_WQE_REACHED:
			ss << "QP last WQE reached";
			break;

			/* CQ events */
		case IBV_EVENT_CQ_ERR:
			ss << "CQ error"; // %p
			break;

			/* SRQ events */
		case IBV_EVENT_SRQ_ERR:
			ss << "SRQ error"; // %p
			break;
		case IBV_EVENT_SRQ_LIMIT_REACHED:
			ss << "SRQ limit reached"; // %p
			break;

			/* Port events */
		case IBV_EVENT_PORT_ACTIVE:
			ss << "Port active event"; // %d
			break;
		case IBV_EVENT_PORT_ERR:
			ss << "Port error event";
			break;
		case IBV_EVENT_LID_CHANGE:
			ss << "LID change event";
			break;
		case IBV_EVENT_PKEY_CHANGE:
			ss << "P_Key table change event";
			break;
		case IBV_EVENT_GID_CHANGE:
			ss << "GID table change event";
			break;
		case IBV_EVENT_SM_CHANGE:
			ss << "SM change event";
			break;
		case IBV_EVENT_CLIENT_REREGISTER:
			ss << "Client reregister event";
			break;

			/* RDMA device events */
		case IBV_EVENT_DEVICE_FATAL:
			ss << "Fatal error event for device";
			break;

		default:
			ss << "Unknown event";
	}

	return ss.str();
}

// returns the event name as documented (e.g. IBV_EVENT_CQ_ERR)
std::string ibvla::asyncEventToStringLiteral (size_t event)
{
	std::stringstream ss;
	switch (event)
	{
		/* QP events */
		case IBV_EVENT_QP_FATAL:
			ss << "IBV_EVENT_QP_FATAL"; // %p
			break;
		case IBV_EVENT_QP_REQ_ERR:
			ss << "IBV_EVENT_QP_REQ_ERR";
			break;
		case IBV_EVENT_QP_ACCESS_ERR:
			ss << "IBV_EVENT_QP_ACCESS_ERR";
			break;
		case IBV_EVENT_COMM_EST:
			ss << "IBV_EVENT_COMM_EST";
			break;
		case IBV_EVENT_SQ_DRAINED:
			ss << "IBV_EVENT_SQ_DRAINED";
			break;
		case IBV_EVENT_PATH_MIG:
			ss << "IBV_EVENT_PATH_MIG";
			break;
		case IBV_EVENT_PATH_MIG_ERR:
			ss << "IBV_EVENT_PATH_MIG_ERR";
			break;
		case IBV_EVENT_QP_LAST_WQE_REACHED:
			ss << "IBV_EVENT_QP_LAST_WQE_REACHED";
			break;

			/* CQ events */
		case IBV_EVENT_CQ_ERR:
			ss << "IBV_EVENT_CQ_ERR"; // %p
			break;

			/* SRQ events */
		case IBV_EVENT_SRQ_ERR:
			ss << "IBV_EVENT_SRQ_ERR"; // %p
			break;
		case IBV_EVENT_SRQ_LIMIT_REACHED:
			ss << "IBV_EVENT_SRQ_LIMIT_REACHED"; // %p
			break;

			/* Port events */
		case IBV_EVENT_PORT_ACTIVE:
			ss << "IBV_EVENT_PORT_ACTIVE"; // %d
			break;
		case IBV_EVENT_PORT_ERR:
			ss << "IBV_EVENT_PORT_ERR";
			break;
		case IBV_EVENT_LID_CHANGE:
			ss << "IBV_EVENT_LID_CHANGE";
			break;
		case IBV_EVENT_PKEY_CHANGE:
			ss << "IBV_EVENT_PKEY_CHANGE";
			break;
		case IBV_EVENT_GID_CHANGE:
			ss << "IBV_EVENT_GID_CHANGE";
			break;
		case IBV_EVENT_SM_CHANGE:
			ss << "IBV_EVENT_SM_CHANGE";
			break;
		case IBV_EVENT_CLIENT_REREGISTER:
			ss << "IBV_EVENT_CLIENT_REREGISTER";
			break;

			/* RDMA device events */
		case IBV_EVENT_DEVICE_FATAL:
			ss << "IBV_EVENT_DEVICE_FATAL";
			break;

		default:
			ss << "Unknown event";
	}

	return ss.str();
}

// returns a description of the work completion in relation to the actual event that occurred
std::string ibvla::toString (struct ibv_wc *wc)
{
	return workCompletionToString(wc->status); // returns a description
}

// returns a general description of the event
std::string ibvla::workCompletionToString (size_t event)
{
	//return ibv_wc_status_str((enum ibv_wc_status) event); // returns a description
	std::stringstream ss;
	switch (event)
	{
		case IBV_WC_LOC_LEN_ERR:
		{
			ss << "Posted receive buffer is not big enough for the incoming message";
		}
			break;
		case IBV_WC_LOC_QP_OP_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_LOC_EEC_OP_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_LOC_PROT_ERR:
		{
			ss << "Send block size is smaller than the data to be sent";
		}
			break;
		case IBV_WC_WR_FLUSH_ERR:
		{
			/*
			 This event is generated when an invalid remote error is thrown when the responder detects an
			 invalid request. It may be that the operation is not supported by the request queue or there is insufficient
			 buffer space to receive the request.
			 */
			// KNOWN CAUSES :
			// 1. buffer->sge_list.length != remote buffer->sge_list.length
			// 2. work requested posted to a queue pair for sending with inactive remote qp and a work request has already returned a IBV_WC_RETRY_EXC_ERR error
			ss << "Work request flushed from QP with error code";
		}
			break;
		case IBV_WC_MW_BIND_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_BAD_RESP_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_LOC_ACCESS_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_REM_INV_REQ_ERR:
		{
			ss << "The receive buffer is smaller than the incoming send";
		}
			break;
		case IBV_WC_REM_ACCESS_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_REM_OP_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_RETRY_EXC_ERR:
		{
			/*
			 This event is generated when a sender is unable to receive feedback from the receiver. This means
			 that either the receiver just never ACKs sender messages in a specified time period, or it has been
			 disconnected or it is in a bad state which prevents it from responding.
			 */
			// For us, probably remote application has terminated
			ss << "Failed to communicate with remote host, communication timed out";
		}
			break;
		case IBV_WC_RNR_RETRY_EXC_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_LOC_RDD_VIOL_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_REM_INV_RD_REQ_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_REM_ABORT_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_INV_EECN_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_INV_EEC_STATE_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_FATAL_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_RESP_TIMEOUT_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		case IBV_WC_GENERAL_ERR:
		{
			ss << ibv_wc_status_str((enum ibv_wc_status) event);
		}
			break;
		default:
		{
			ss << "Unknown error";
		}
			break;
	}
	return ss.str();
}

// returns the event name as documented (e.g. IBV_WC_LOC_LEN_ERR)
std::string ibvla::workCompletionToStringLiteral (size_t event)
{
	std::stringstream ss;
	switch (event)
	{
		case IBV_WC_LOC_LEN_ERR:
		{
			ss << "IBV_WC_LOC_LEN_ERR";
		}
			break;
		case IBV_WC_LOC_QP_OP_ERR:
		{
			ss << "IBV_WC_LOC_QP_OP_ERR";
		}
			break;
		case IBV_WC_LOC_EEC_OP_ERR:
		{
			ss << "IBV_WC_LOC_EEC_OP_ERR";
		}
			break;
		case IBV_WC_LOC_PROT_ERR:
		{
			ss << "IBV_WC_LOC_PROT_ERR";
		}
			break;
		case IBV_WC_WR_FLUSH_ERR:
		{
			ss << "IBV_WC_WR_FLUSH_ERR";
		}
			break;
		case IBV_WC_MW_BIND_ERR:
		{
			ss << "IBV_WC_MW_BIND_ERR";
		}
			break;
		case IBV_WC_BAD_RESP_ERR:
		{
			ss << "IBV_WC_BAD_RESP_ERR";
		}
			break;
		case IBV_WC_LOC_ACCESS_ERR:
		{
			ss << "IBV_WC_LOC_ACCESS_ERR";
		}
			break;
		case IBV_WC_REM_INV_REQ_ERR:
		{
			ss << "IBV_WC_REM_INV_REQ_ERR";
		}
			break;
		case IBV_WC_REM_ACCESS_ERR:
		{
			ss << "IBV_WC_REM_ACCESS_ERR";
		}
			break;
		case IBV_WC_REM_OP_ERR:
		{
			ss << "IBV_WC_REM_OP_ERR";
		}
			break;
		case IBV_WC_RETRY_EXC_ERR:
		{
			ss << "IBV_WC_RETRY_EXC_ERR";
		}
			break;
		case IBV_WC_RNR_RETRY_EXC_ERR:
		{
			ss << "IBV_WC_RETRY_EXC_ERR";
		}
			break;
		case IBV_WC_LOC_RDD_VIOL_ERR:
		{
			ss << "IBV_WC_LOC_RDD_VIOL_ERR";
		}
			break;
		case IBV_WC_REM_INV_RD_REQ_ERR:
		{
			ss << "IBV_WC_REM_INV_RD_REQ_ERR";
		}
			break;
		case IBV_WC_REM_ABORT_ERR:
		{
			ss << "IBV_WC_REM_ABORT_ERR";
		}
			break;
		case IBV_WC_INV_EECN_ERR:
		{
			ss << "IBV_WC_INV_EECN_ERR";
		}
			break;
		case IBV_WC_INV_EEC_STATE_ERR:
		{
			ss << "IBV_WC_INV_EEC_STATE_ERR";
		}
			break;
		case IBV_WC_FATAL_ERR:
		{
			ss << "IBV_WC_FATAL_ERR";
		}
			break;
		case IBV_WC_RESP_TIMEOUT_ERR:
		{
			ss << "IBV_WC_FATAL_ERR";
		}
			break;
		case IBV_WC_GENERAL_ERR:
		{
			ss << "IBV_WC_GENERAL_ERR";
		}
			break;
		default:
			ss << "Unknown event";
	}
	return ss.str();
}

// returns a string representation of the QP state
std::string ibvla::stateToString (ibvla::QueuePair & qp)
{
	switch (qp.qp_->state)
	{
		case IBV_QPS_RESET:
			return "Reset";
		case IBV_QPS_INIT:
			return "Initialized";
		case IBV_QPS_RTR:
			return "Ready To Receive";
		case IBV_QPS_RTS:
			return "Ready To Send";
		case IBV_QPS_SQD:
			return "Send Queue Drain";
		case IBV_QPS_SQE:
			return "Send Queue Error";
		case IBV_QPS_ERR:
			return "Error";
		default:
			return "Unknown";
	}
}
