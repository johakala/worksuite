#include "xpci/Bus.h"

#define FRLCONTROLLER_VENDORID 0xecd6
#define FRLCONTROLLER_DEVICEID 0xff10
#define SPYCONTROLLER_VENDORID 0xecd6
#define SPYCONTROLLER_DEVICEID 0xff01



int main(int argc, char* argv[])
{
	xpci::Bus pcibus;
	for ( unsigned int i = 0; i < 16 ; i++ )
	{
		try
		{
		xpci::Address spycontroller  = xpci::Address::getConfigSpaceAddressByVendor(SPYCONTROLLER_VENDORID,SPYCONTROLLER_DEVICEID,i);
		xpci::Address frlcontroller  = xpci::Address::getConfigSpaceAddressByVendor(FRLCONTROLLER_VENDORID,FRLCONTROLLER_DEVICEID,i);
		xpci::Address frlcontrollerBAR = pcibus.BAR(0,frlcontroller);
		xpci::Address spycontrollerBAR = pcibus.BAR(0,spycontroller);

		std::cout << "Base Addres Register for spycontroller: 0x" << std::hex << spycontrollerBAR.getAddress() << std::dec << std::endl;
		std::cout << "Base Addres Register for frlcontroller: 0x" << std::hex << frlcontrollerBAR.getAddress() << std::dec << std::endl;

		uint32_t readparam;
		pcibus.read(spycontroller,0x00000050, readparam);
		std::cout << "Value of geogSlot: 0x" << std::hex <<  readparam <<  std::dec << std::endl; 
		pcibus.read(spycontroller,0x0000005c, readparam);
		std::cout << "Value of serialNumber: 0x" << std::hex <<  readparam <<  std::dec << std::endl; 
/*
		uint32_t readparam;
		uint32_t readparam1;
		pcibus.read(frlcontrollerBAR,0x00000128, readparam);
		std::cout << "4" << std::endl;
                pcibus.read(frlcontrollerBAR,0x0000012c, readparam1);
                uint64_t triggerCount   = (((uint64_t)readparam1) << 32) + (uint64_t)readparam;
		std::cout << "Value of TriggerCounter64: 0x" << std::hex <<  triggerCount <<  std::dec << std::endl; 
*/

		}
		catch(xpci::exception::OpenFileFailed & e)
		{
			std::cout << "Error:" << e.what() << std::endl;
		}
		catch(xpci::exception::IOError & e)
		{
			std::cout << "Error:" << e.what() << std::endl;
		}
		catch(xpci::exception::InvalidAddress &e )
		{
			std::cout << "Error:" << e.what() << std::endl;
		}
	}
 	
}
