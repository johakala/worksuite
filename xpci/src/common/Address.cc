// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xpci_Addressh_
#define _xpci_Addressh_


#include "xpci/Address.h" 
#include "xpci/xpci-kernel.h"


xpci::Address::Address (pciaddr_t address):
	type_(XPCI_MEMORY),
	address_(address)
{
}

xpci::Address::Address (unsigned int vendor, unsigned int device, unsigned int index):
	type_(XPCI_CONFIG_VENDOR), 
	address_(0),
	vendorbus_ (vendor), 
	device_(device), 
	indexfn_(index)
{
}

xpci::Address::~Address()
{
}
		
pciaddr_t xpci::Address::getAddress() 
{
	return address_;
} 

xpci::Address xpci::Address::getConfigSpaceAddressByBus(unsigned int bus, unsigned int device, unsigned int function)
{ 
	Address ret(bus, device, function);
	ret.type_=XPCI_CONFIG_BUS; 
	return ret; 
}

xpci::Address xpci::Address::getConfigSpaceAddressByVendor(unsigned int vendor, unsigned int device, unsigned int index) 
{
	return Address (vendor, device, index);
}

xpci::Address xpci::Address::getMemorySpaceAddress (pciaddr_t address) 
{
	return Address(address);
}

xpci::Address xpci::Address::getMemorySpaceIOAddress (pciaddr_t address) 
{
	Address ret(address); 
	ret.type_=XPCI_IO; 
	return ret;
}
#endif
