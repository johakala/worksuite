#include "amc13controller/InputStreamInfoSpaceHandler.hh"
#include "amc13controller/amc13Constants.h"

amc13controller::InputStreamInfoSpaceHandler::InputStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS )
    : utils::StreamInfoSpaceHandler( xdaq, "InputStream", NULL, appIS, false /*noAutoPush*/, NB_INPUT_STREAMS) // JRF , Removing all the functionality of this class that we nolonger need. AMC13StreamInfoSpaceHandler( xdaq, "InputStream", appIS )
{
    //Add values to monitor in here.
    //createuint32( "TCP_STAT_CURRENT_WND_FED", 0, "", HW32, "Snapshot of the TCP window size received by the AMC13");
createbool( "Enabled", false, "", PROCESS,         "'1' if this AMC input is enabled", 1 );

//----------AMC_Trigger----------
createuint64( "AMC13_BC0_err", 0, "hex", UPDATE, "AMC13 trigger BC0 mismatch error counter", 2);
createuint64( "AMC13_bcnt_err", 0, "hex", UPDATE, "AMC13 trigger bcnt mismatch error counter", 2);
createuint64( "AMC13_dbl_err", 0, "hex", UPDATE, "AMC13 trigger data multi-bit error counter", 1);
createuint64( "AMC13_sgl_err", 0, "hex", UPDATE, "AMC13 trigger data single bit error counter", 1);
createuint32( "TTC_LOCKED", 0, "hex", UPDATE, "1 if corresponding AMC enabled and TTC locked", 2);

//----------AMC_Links----------
createuint32( "ACKNUM_a", 0, "hex", UPDATE, "Guru meditation", 4);
createuint64( "AMC13_BcN_mismatch", 0, "hex", UPDATE, "HTR event BCN mismatch counter", 1);
createuint64( "AMC13_EvN_mismatch", 0, "hex", UPDATE, "HTR event EVN mismatch counter", 1);
createuint64( "AMC13_Events", 0, "hex", UPDATE, "Receive Event counter", 2);
createuint32( "AMC13_LINK_VER", 0, "hex", UPDATE, "AMC13 Link Version", 2);
createuint64( "AMC13_OrN_mismatch", 0, "hex", UPDATE, "HTR event OCN mismatch counter", 1);
createuint64( "AMC13_Words", 0, "hex", UPDATE, "AMC13 total word counter", 2);
createuint64( "AMC13_bad_length", 0, "hex", UPDATE, "AMC bad event length counter", 1);
createuint32( "AMCRdy", 0, "hex", UPDATE, "Guru meditation", 4);
createuint64( "AMC_BSY_time", 0, "hex", UPDATE, "AMC TTS BSY state time", 1);
createuint64( "AMC_BcN_Mismatch", 0, "hex", UPDATE, "AMC BcN mismatch counter", 1);
createuint64( "AMC_DIS_time", 0, "hex", UPDATE, "AMC TTS disconnect state time", 1);
createuint64( "AMC_ERR_time", 0, "hex", UPDATE, "AMC TTS ERR state time", 1);
createuint64( "AMC_EvN_Errors", 0, "hex", UPDATE, "event number error counter at link input", 1);
createuint64( "AMC_EvN_Mismatch", 0, "hex", UPDATE, "AMC Evn mismatch counter", 1);
createuint64( "AMC_Events", 0, "hex", UPDATE, "AMC received event counter", 1);
createuint64( "AMC_Headers", 0, "hex", UPDATE, "header word counter at link input", 2);
createuint32( "AMC_LINK_READY_AMC**", 0, "hex", UPDATE, "'1' indicates AMC1..12 Link Ready", 1);
createuint32( "AMC_LINK_VER", 0, "hex", UPDATE, "AMC Link Version", 2);
createuint64( "AMC_OFW_time", 0, "hex", UPDATE, "AMC TTS OFW state time", 1);
createuint64( "AMC_OrN_Mismatch", 0, "hex", UPDATE, "AMC OrN mismatch counter", 1);
createuint64( "AMC_SYN_time", 0, "hex", UPDATE, "AMC TTS SYN state time", 1);
createuint32( "AMC_TTS", 0, "TTSRaw", UPDATE, "TTS received from AMC", 2);
createuint64( "AMC_Trailers", 0, "hex", UPDATE, "trailer word counter at link input", 2);
createuint64( "AMC_bad_length", 0, "hex", UPDATE, "AMC bad event length counter", 1);
createuint64( "AMC_format_error", 0, "hex", UPDATE, "Badly formatted input word", 1);
createuint64( "AMC_trailer_EvN_bad", 0, "hex", UPDATE, "AMC event trailer Evn mismatch error counter", 1);
createuint64( "AMC_words", 0, "hex", UPDATE, "total word counter at link input", 2);
createuint32( "BC0_LOCKED_AMC**", 0, "hex", UPDATE, "AMC1..12 BC0 locked (local trigger only)", 3);
createuint32( "BC0_lock", 0, "hex", UPDATE, "Guru meditation", 2);
createuint32( "BP_Err", 0, "hex", UPDATE, "Backplane link CRC error", 1);
createuint64( "Bad_CRC", 0, "hex", UPDATE, "Bad CRC on event from AMC", 2);
createuint32( "Buffer_overflow", 0, "hex", UPDATE, "Buffer overflow on backplane link", 1);
createuint32( "Buffer_underflow", 0, "hex", UPDATE, "Buffer underflow on backplane link", 1);
createuint32( "DataBuf_RA", 0, "hex", UPDATE, "AMC DAQ_Link status: DataBuf_ra", 3);
createuint32( "DataBuf_WA", 0, "hex", UPDATE, "AMC DAQ_Link status: DataBuf_wa", 3);
createuint32( "DataRdEnWrdCount", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "Data_Too_long", 0, "hex", UPDATE, "AMC length exceeds 0xfffff words", 1);
createuint32( "Disc", 0, "hex", UPDATE, "TTC was in disconnected state", 2);
createuint64( "EVB_Events", 0, "hex", UPDATE, "Read Event counter", 2);
createuint32( "EVENTBUF_RA", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "EVENTBUF_WA", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "Err", 0, "hex", UPDATE, "TTC was in error state", 2);
createuint32( "EventInfo_a", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "EventStatus_ra", 0, "hex", UPDATE, "AMC DAQ_Link status: EventStatus_ra", 3);
createuint32( "EventStatus_wa", 0, "hex", UPDATE, "AMC DAQ_Link status: EventStatus_wa", 3);
createuint32( "EventWC", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "EvtInfoRdDoneTogl", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "EvtInfoTogl", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "Extra_header", 0, "hex", UPDATE, "AMC sent unexpected header", 1);
createuint32( "Extra_trailer", 0, "hex", UPDATE, "AMC sent unexpected trailer", 1);
createuint32( "FIFO_ovf", 0, "hex", UPDATE, "AMC DAQ_Link status: FIFO_ovf", 3);
createuint32( "InitLink", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "L1AINFO_RA", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "L1AINFO_WA", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "L1Ainfo_RA", 0, "hex", UPDATE, "AMC DAQ_Link status: L1Ainfo_ra", 3);
createuint32( "L1Ainfo_WA", 0, "hex", UPDATE, "AMC DAQ_Link status: L1Ainfo_wa", 3);
createuint32( "L1Ainfo_wap", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "LINK_OK", 0, "hex", UPDATE, "'0' AMC1..12 loss of sync", 1);
createuint32( "LINK_VERS_WRONG", 0, "hex", UPDATE, "'1' AMC1..12 link version wrong", 1);
createuint32( "Link_Buffer_Full", 0, "hex", UPDATE, "AMC_LINK buffer is full", 2);
createuint32( "Mismatch_BcN", 0, "hex", UPDATE, "If '0', BcN mismatch", 2);
createuint32( "Mismatch_EvN", 0, "hex", UPDATE, "If '0', EvN mismatch", 2);
createuint32( "Mismatch_OrN", 0, "hex", UPDATE, "If '0', OrN mismatch", 2);
createuint32( "Mismatch_TTS", 0, "hex", UPDATE, "Link TTS on first mismatch of EvN/BcN/OrN", 2);
createuint32( "Missing_header", 0, "hex", UPDATE, "AMC data received without header", 1);
createuint32( "RDERR", 0, "hex", UPDATE, "RDERR for eight FIFOs", 2);
createuint32( "RESYNC_fakes", 0, "hex", UPDATE, "Fake events generated during resync", 2);
createuint32( "RFIFO_a", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "ReSendQue_a", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "RxPllLock", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "RxRstDone", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "Syn", 0, "hex", UPDATE, "TTC was in sync lost state", 2);
createuint64( "TTC_Update", 0, "hex", UPDATE, "TTC update from AMC cards counter", 2);
createuint32( "TTC_lock", 0, "hex", UPDATE, "Guru meditation", 2);
createuint64( "TTS_Disconnected", 0, "hex", UPDATE, "TTS state 'disconnected' from AMC", 1);
createuint32( "TTS_Encoded", 0, "TTSEnc", UPDATE, "MSB..LSB: DIS,ERR,SYN,BSY,OFW", 2);
createuint64( "TTS_Error", 0, "hex", UPDATE, "TTS state 'error' from AMC", 1);
createuint32( "TTS_Raw", 0, "TTSRaw", UPDATE, "Raw TTS from AMC", 2);
createuint64( "TTS_Sync_Lost", 0, "hex", UPDATE, "TTS state 'sync lost' from AMC", 1);
createuint64( "TTS_Update", 0, "hex", UPDATE, "TTS update from AMC cards counter", 2);
createuint32( "TxRstDone", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "TxState", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "Version", 0, "hex", UPDATE, "AMC Link version", 2);
createuint32( "WRERR", 0, "hex", UPDATE, "WRERR for eight FIFOS", 2);
createuint32( "block32k", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "got_AMC_BC0", 0, "hex", UPDATE, "Guru meditation", 4);
createuint32( "reset_sync3", 0, "hex", UPDATE, "Guru meditation", 4);
    
}

/*
void
amc13controller::InputStreamInfoSpaceHandler::registerTrackerItems( utils::DataTracker &tracker )
{
    //JRF TODO put these back in once I implement the rate tracker for streams. 
    tracker.registerRateTracker( "EventGenRate", "GEN_EVENT_NUMBER", utils::DataTracker::HW32 );
}
*/
                
/*
FRL                         AMC13 5gb                             AMC13 10gb
============================================================================================================================================
slotNumber                                                                                               
input number
                                                                                                         
?Time_LO                                                                                                 A free running counter (64bit) with 1 Mhz clock 

FIFO_BP_LO                  BACK_PRESSURE_BIFI_FED  156.25Mhz    BACK_PRESSURE_BIFI_FED                  Counts the number of 100Mhz clocks the input Fifo of the FRL is in backpressure.
?BP_running_cnt_LO                                                                                        A free running counter at 100Mhz
?gen_pending_trg                                                                                          

                             
Interesting:


FRL related                 AMC13 related
===========                 =============
FrlFwVersion                AMC13FwVersion
FrlFwType                   AMC13FwType
FrlHwVersion                AMC13HwRevision
FrlHwRevision               slotNumnber
BridgeFwVersion             DestinationMACAddress
BridgeFwType                Source_MAC_EEPROM
BridgeHwRevision            
FrlFwChanged                
slotNumber                  
StatusFIFO                  


Question
----------
do we use latch_BP_cnt? which values are latched - yes
is FedIdWrong_Stream a mono flop? when reset? -> yes it stays until the software_reset

===>  BACK_PRESSURE_BIFI_FED0/1 : this counts with which frequency???

 */
