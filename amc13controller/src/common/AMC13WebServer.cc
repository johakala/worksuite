#include "amc13controller/AMC13WebServer.hh"
#include "amc13controller/AMC13Controller.hh"
#include "d2s/utils/loggerMacros.h"

amc13controller::AMC13WebServer::AMC13WebServer( utils::Monitor &monitor, 
                                       std::string url,
                                       std::string urn,
                                       Logger logger )
    : WebServer( monitor, urn, url, logger )
{
    css_ = "amc13.css";
    //WebTableTab *configTab = this->registerTableTab( "Config", 
    //                                                 "Configuration Items for application and shared hardware components.",
    //                                                 4);

    //configTab->registerTable( "Configuration",
    //                          "Items to configure the AMC13 and software.<br>", 
    //                          "AMC13Config" );
   
   

    //for (uint32_t i(0); i < NB_OUTPUT_STREAMS; i++)
    //{
    //    std::stringstream ss;
    //    ss << "outTest_Table_" << i; 
    //    outputMonTab->registerTable(ss.str().substr(3), ss.str().substr(3), ss.str());
    //
    //}
    const uint32_t nCols = 4;
    
    utils::WebTableTab *applStateTab = this->registerTableTab
    (
        "Appl_State",
        "of this monitoring application.",
        nCols
    );

    utils::WebTableTab *stateTab = this->registerTableTab
    ( 
        "Hardware_State",
        "Global state of the AMC13.",
        nCols
    );

    utils::WebTableTab *configTab = this->registerTableTab
    (
        "Config",
        "Global configurables of the AMC13.",
        nCols
    );

    utils::WebTableTab *triggerTab = this->registerTableTab
    (
        "Trigger",
        "Trigger information from TCDS input or generated signals.",
        nCols
    );

    utils::WebTableTab *inputTab = this->registerTableTab
    (
        "Input_Mon",
        "Monitorables for the 12 input AMC modules.",
        nCols
    );

    utils::WebTableTab *outputTab = this->registerTableTab
    (
        "Output_Mon",
        "Monitorables for the 3 SFP outputs.",
        nCols
    );

    //Application state tab:
    applStateTab->registerTable("Application State",
        "Values describing the state of this monitoring application. Not directly \
        linked to the hardware itself.",
        "appl_state");

    //Hardware state tab:
    stateTab->registerTable("Board",
        "Fixed values of the AMC13 chipset.",
        "0_Board");
    stateTab->registerTable("Monitor Buffer","","Monitor_Buffer");
    stateTab->registerTable("Event Builder","","Event_Builder");
    stateTab->registerTable("State Timers","","State_Timers");
    stateTab->registerTable("AMC13 Errors","","AMC13_Errors");
    stateTab->registerTable("Temperatures and Voltages","","Temps_Voltages");
    stateTab->registerTable("T2 TTC","","T2_TTC");
    stateTab->registerTable("TTC Rx","","TTC_Rx");

    //Config tab
    configTab->registerTable("General Configuration","","1_Config");
    configTab->registerTable("Control","","2_Control");
                                                     
    //===========Non-stream values==========
    triggerTab->registerTable("Local_Trigger","description here","Local_Trigger");
    triggerTab->registerTable("DT_Trig","description here","DT_Trig");
    //TTC_History and TTC_History_conf not currently included...
    inputTab->registerTable("AMC_Links","description here","AMC_Links");

    //==========Input Streams==========
    this->registerStreamTables(triggerTab, "AMC_Trigger_", "description here",
                               "AMC_Trigger_", NB_INPUT_STREAMS); 
    this->registerStreamTables(inputTab, "AMC_Links_", "description here",
                               "AMC_Links_", NB_INPUT_STREAMS); 

    //OUTPUT TAB:
    outputTab->registerTable("Slink_Express","description here","Slink_Express");
    this->registerStreamTables(outputTab, "Slink_Express_", "description here",
                               "Slink_Express_", NB_OUTPUT_STREAMS); 
    outputTab->registerTable("SFP_ROM","description here","SFP_ROM");
    this->registerStreamTables(outputTab, "Event_Builder_", "description here",
                               "Event_Builder_", NB_OUTPUT_STREAMS); 

    applStateTab->processLevels();
    stateTab->processLevels();
    configTab->processLevels();
    triggerTab->processLevels();
    inputTab->processLevels();
    outputTab->processLevels();
}

void amc13controller::AMC13WebServer::registerStreamTables( 
                       utils::WebTableTab *tab, std::string baseTableName, 
                       std::string desc, std::string baseSetName, uint32_t streamSize)
{
    for (uint32_t i(0); i < streamSize; i++)
    {
        std::stringstream tableName, setName;
        tableName << baseTableName << i;
        setName << baseSetName << i;
        tab->registerTable(tableName.str(), desc, setName.str());
    }
}


void 
amc13controller::AMC13WebServer::printDebugTable( std::string name, std::list< utils::HardwareDebugItem > regList, xgi::Input *in, xgi::Output *out )
{
    printHeader( out );

    std::list< utils::HardwareDebugItem >::iterator it;

    *out << "<p class=itemTableTitle>" << name << "</p>\n";

    *out << "<table id=\"registerTable\" class=\"tablesorter\">\n<thead><tr><th>item</th><th>value</th><th>offset</th><th>description</th></tr></thead>\n<tbody>\n";
    for( it = regList.begin(); it != regList.end(); it++ )
        {
            *out << "<tr><td>" << (*it).item << "</td><td>" << (*it).valStr << "</td><td>" << (*it).adrStr << "</td><td>" << (*it).description << "</td></tr>\n";
        }
    *out << "</tbody>\n</table>\n";
    *out << "<script>\n $(document).ready(function() \n{\n $('#registerTable').tablesorter( { headers: { 1 : {sorter:false}, 3 : {sorter:false}}, sortList:  [[2,0]] }); \n}); \n</script>";
    //printFooter( out );

}

std::string
amc13controller::AMC13WebServer::getCgiPara( cgicc::Cgicc cgi, std::string name, std::string defaultval )
{
    cgicc::form_iterator iter = cgi.getElement(name);
    if ( iter == cgi.getElements().end() )
        {
            return defaultval;
        }
    else 
        {
            return **iter;
        }
}

void
amc13controller::AMC13WebServer::expertDebugging( xgi::Input *in, xgi::Output *out, AMC13 *amc13,   AMC13Controller *const fctl )
{
    printHeader( out );
    cgicc::Cgicc cgi(in);
    uint32_t linkno = atoi( getCgiPara( cgi, "linkId", "0" ).c_str() );
    std::string command = getCgiPara( cgi, "command", "" );
    std::string parameter = getCgiPara( cgi, "parameter", "" );
    
    //std::cout << "\ncommand " << command << std::endl;
    //std::cout << "parameter " << parameter << std::endl;

  if ( command == "dumpRegisters" ) 
    {
      fctl->dumpHardwareRegisters();
      *out << "<p> Dumped registers in /tmp.</p>\n";
    }
  else if ( command == "resyncSlinkexpress" )
    {
      amc13->resyncSlinkExpress( linkno );
      *out << "<p>Did resync of Slink Express no " << linkno << ".</p>\n";
    }
  else if ( command == "softwareReset" )
    {
      //JRF TODO add reset for AMC13 here.
      //amc13->resyncSlinkExpress( linkno );
      //amc13->getAMC13Device()->setBit("SOFTWARE_RESET");
      amc13->getAMC13Device()->getFlash()->loadFlash();
      *out << "<p>Did a reload of FPGA from Flash of the AMC13.</p>\n";
    }
  else if ( command == "daqOn" )
    {
      amc13->daqOn( linkno );
      *out << "<p>Did daqOn of Slink Express no " << linkno << ".</p>\n";
    }
  else if ( command == "daqOff" )
    {
      amc13->daqOff( linkno );
      *out << "<p>Did daqOff of Slink Express no " << linkno << ".</p>\n";
    }

//JRF TODO remove all Frl stuff...
  else if ( command == "generateFrlError" )
      {
          if ( parameter == "crc" )
              {
                  DEBUG("generating a crc error in the FRL" );
                  //frl->getFrlDevice()->write( "gen_error", 1 );
              }
          else if ( parameter == "sizeError" ) 
              {
                 // frl->getFrlDevice()->write( "gen_error", 2 );                  
              }
          else if ( parameter == "insertWrongEvt" ) 
              {
               //   frl->getFrlDevice()->write( "gen_Outsync_gen", 1 );                  
             }
          else if ( parameter == "wrongEvtNr" ) 
              {
              //    frl->getFrlDevice()->write( "gen_Outsync_gen", 2 );                  
              }
          else if ( parameter == "dropEvent" )
              {
               //   frl->getFrlDevice()->write( "gen_Outsync_gen", 4 );                  
              }
          else if ( parameter == "skipEvents" )
              {
               //   frl->getFrlDevice()->write( "gen_Outsync_gen", 8 );                  
              }
      }

  *out << "<form id=\"debugForm\" method=\"post\" >\n";

  *out << "<h3> Here you really need to know what you are doing... </h3>\n";
  *out << "<p>urn: " << urn_ << "<br>url: " << url_ << " </p>\n";
  *out << "<p><h3>General debugging features</h3>\n";
  *out << "<button onclick=\"amc13WebCommand( \'dumpRegisters\', \'\' )\">Register Dump</button> Dump all registers of the AMC13 into files in /tmp<br>\n";
  *out << "<button onclick=\"amc13WebCommand( \'softwareReset\', \'\' )\">Software reload of FPGA from flash of AMC13 </button> Reloads the FPGA from flash AMC13. This is the main reset.<br>\n";
  *out << "</p><hr/>\n";

  *out << "<p><h3>Commands for a specific 5Gb Slink Express input link</h3>\n";
  *out << "<input type=\"hidden\" id=\"command\" name=\"command\" value=\"\"/>\n";
  *out << "<input type=\"hidden\" id=\"parameter\" name=\"parameter\" value=\"\"/>\n";
  *out << "5GB link number [0|1] : <input id=\"linkId\" name=\"linkId\" type=\"text\" value=\"0\"><br>\n";
  *out << "<button onclick=\"amc13WebCommand( \'resyncSlinkexpress\', \'\' )\"> Resync SlinkExpress </button> Send a resync command to the selected Slink Express.<br>\n";
  *out << "<button onclick=\"amc13WebCommand( \'daqOnSlinkexpress\', \'\' )\"> DaqOn </button> Send a daqOn command to the selected Slink Express.<br>\n";
  *out << "<button onclick=\"amc13WebCommand( \'daqOffSlinkexpress\', \'\' )\"> DaqOff </button> Send a daqOff command to the selected Slink Express.<br>\n";
  *out << "</p><hr/>\n";

  *out << "<p><h3>Generate artificial errors in the plastic events from the FRL event generator</h3>\n";
  *out << "<button onclick=\"amc13WebCommand( \'generateFrlError\', \'crc\' )\"> Generate CRC error </button> Generates an SLINK CRC error.<br>\n";
  *out << "<button onclick=\"amc13WebCommand( \'generateFrlError\', \'sizeError\' )\"> Generate wrong length</button> Generates an event with a wrong length in the trailer.<br>\n";
  *out << "<button onclick=\"amc13WebCommand( \'generateFrlError\', \'insertWrongEvt\' )\"> Insert Event </button> Generates an additional event with a wrong event number, e.g. 1,2,3,199,4,5,6... <br>\n";
  *out << "<button onclick=\"amc13WebCommand( \'generateFrlError\', \'wrongEvtNr\' )\"> Wrong event number </button> Generates an event with a wrong event number, e.g. 1,2,3,199,5,6... <br>\n";
  *out << "<button onclick=\"amc13WebCommand( \'generateFrlError\', \'dropEvent\' )\"> Miss an event </button> Drops one event number, e.g. 1,2,3,5,6... <br>\n";
  *out << "<button onclick=\"amc13WebCommand( \'generateFrlError\', \'skipEvents\' )\"> Skip events </button> Skip many events, e.g. 1,2,3,342,343... <br>\n";
  
  *out << "</p>\n";
  *out << "</p>To generate the \"wrong\" items the 2 comlement of the correct item is used. This holds for the event numbers, fragment length,  and the CRC.</p>\n";
  *out << "</form>\n";
}

void 
amc13controller::AMC13WebServer::printHeader( xgi::Output * out )
{

    std::stringstream urn,url;
    out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
    *out << "\n\
<html>\n\
  <head>\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tab.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tablesort.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/properties.css\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/amc13controller/html/" << getCSS()
    << "\">\n\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"/amc13controller/html/blue/style.css\">\n\
\n\
    <script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-2.1.0.min.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/amc13controller/html/jquery.tablesorter.min.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/xgi/html/tabpane.js\"></script>\n\
    <script type=\"text/javascript\" src=\"/amc13controller/html/amc13.js\"></script>\n\
  </head>\n\
  <body>\n\
    <table class=\"amc13header\"><tr>\
    <th>AMC13 Controller</th>\n";
    *out << "<td><a href=\"" << url_ << "\">XDAQ page</a></td>\n";
    *out << "<td><a href=\"/" << urn_ << "\">Main</a></td>\n";
    *out << "<td><a href=\"/" << urn_ << "/debugAMC13\"> AMC13-registers </a></td>\n"; 
    *out << "<td><a href=\"/" << urn_ << "/debugSlinkExpress\"> SlinkExpress-Core-registers </a></td>\n"; 
    *out << "<td><a href=\"/" << urn_ << "/expertDebugPage\"> Expert Debugging </a></td>\n"; 
    *out << "</tr></table><hr>\n                  \
<div id=\"debug\">\n\
</div>\n\
";

}

    void 
amc13controller::AMC13WebServer::printBody( xgi::Output * out )
{
    *out << "\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tab.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tablesort.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/properties.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/amc13controller/html/" << getCSS()
        << "\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/amc13controller/html/blue/\
        style.css\">\n\
        \n\
        <script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-2.1.0.min.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/amc13controller/html/jquery.tablesorter.min.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/xgi/html/tabpane.js\"></script>\n\
        <!--\
        <script type=\"text/javascript\" src=\"/xgi/html/favicon/util.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/xgi/html/favicon/ajaxCaller.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/xgi/html/favicon/favicon.js\"></script>\n\
        -->\
        <script type=\"text/javascript\" src=\"/amc13controller/html/amc13.js\"></script>\n\
        </head>\n\
        <body>\n\
        <table class=\"header\"><tr>\
        <th>AMC13 Controller</th><td><a href=\"basic\">Basic xdaq web page</a></td>\n\
        </tr></table><hr>\n\
        <div id=\"debug\">\n\
        </div>\n";


    std::string updateLink = "/" + urn_ + "/update";
    *out << "\n\
        <div class=\"tab-pane\" id=\"pane1\">\n";

    //resetTabs();
    printTabs( out );


    *out << "</div>\n\
        <script type=\"text/javascript\">\n\
        startUpdate( \"" << updateLink << "\" );\n\
        </script>\n\
        \n\
        ";
}

    void
amc13controller::AMC13WebServer::printFooter( xgi::Output * out )
{
    *out << "\n\
        <hr>\n\
        <div class=\"footer\">\n\
        <p>AMC13 footer</p>\n\
        </div>\n\
        </body>\n\
        </html>\n";
    //<div style=\"display: none\">";
}

