#include "d2s/utils/Watchdog.hh"
#include <time.h>

utils::Watchdog::Watchdog( uint32_t to )
    : timeoutval(to)
{ }

void 
utils::Watchdog::start() {
    gettimeofday(&startTime,NULL);
}

bool
utils::Watchdog::timeout() {
    timeval now;
    gettimeofday( &now, NULL );
    uint32_t elapsed = subtractTime( now, startTime );
    if (elapsed >= timeoutval)
        return true;
    return false;
}

uint32_t
utils::Watchdog::subtractTime(struct timeval& t, struct timeval& sub) {
    signed long sec, usec, rsec, rusec;
    sec = t.tv_sec - sub.tv_sec;
    usec = t.tv_usec - sub.tv_usec;
    if (usec < 0) {
        sec--;
        usec += 1000000;
    }
    if (sec < 0) {
        rsec = 0;
        rusec = 0;
    }
    else {
        rsec = (uint32_t) sec;
        rusec = (uint32_t) usec;
    }
    return (rsec * 1000000 + rusec)/1000;
}
