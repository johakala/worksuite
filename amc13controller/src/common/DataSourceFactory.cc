#include "amc13controller/DataSourceFactory.hh"
#include "amc13controller/FrlDataSource.hh"
#include "amc13controller/AMC13DataSource.hh"
#include "amc13controller/FedkitDataSource.hh"
#include <string>

amc13controller::DataSourceIF *
amc13controller::DataSourceFactory::createDataSource(HAL::HardwareDeviceInterface *device_P,
                                             utils::InfoSpaceHandler &appIS,
                                             Logger logger)
{
    std::string operationMode = appIS.getstring("OperationMode");
    if (operationMode == AMC13_MODE)
    {
        return new amc13controller::AMC13DataSource(device_P, appIS, logger);
    }
    /*else if ( operationMode == FEDKIT_MODE )
        return new amc13controller::FedkitDataSource( device_P,  appIS, logger );
    */
    else
        return NULL;
}
