#include "amc13/SlinkExpressCore.hh"
#include "amc13/Exception.hh"
#include "amc13/amc13Constants.h"
#include <string>
#include <unistd.h>
#include <sstream>

amc13controller::SlinkExpressCore::SlinkExpressCore( HAL::HardwareDeviceInterface *amc13, HAL::AddressTableInterface *addressTable, std::vector<std::string> dataSource )
    : amc13Device_P( amc13 ),
      addressTable_P( addressTable ),
      dataSource_( dataSource )
{
}

HAL::AddressTableInterface &
amc13controller::SlinkExpressCore::getAddressTableInterface()
{
    return *addressTable_P;
}

void
amc13controller::SlinkExpressCore::read( std::string item, uint32_t *result, uint32_t linkno )
{
    uint32_t adr = addressTable_P->getGeneralHardwareAddress( item ).getAddress();
    uint32_t val;
    slinkExpressCommand( adr, val, READ, linkno);
    *result = val;
}

void
amc13controller::SlinkExpressCore::write( std::string item, uint32_t data, uint32_t linkno )
{
    uint32_t adr = addressTable_P->getGeneralHardwareAddress( item ).getAddress();
    slinkExpressCommand( adr, data, WRITE, linkno);     
}

void
amc13controller::SlinkExpressCore::slinkExpressCommand( uint32_t adr, uint32_t &data, Slink_ReadWrite rw, uint32_t linkno )
{
    uint32_t pollres = 0, stream_offset = linkno * 0x4000;

    if ( (dataSource_[linkno] == L10G_SOURCE) || (dataSource_[linkno] == L10G_CORE_GENERATOR_SOURCE) )
        {
            //JRF Don't need these any more
	    //cmdstr = "L10gb_cmd_upload_SlX"; //SLINKXpress_cmd
            //datstr = "L10gb_data_upload_SlX"; //SLINKXpress_cmd_data
            //polstr = "L10gb_cmd_poll_SlX"; //SLINKXpress_cmd_done
            try
        	{
            	    if ( rw == WRITE ) 
                	{
                    	    adr = adr + 0x80000000;
                    	    amc13Device_P->write( "SLINKXpress_cmd", adr, HAL::HAL_NO_VERIFY, stream_offset );
                    	    amc13Device_P->write( "SLINKXpress_cmd_data", data, HAL::HAL_NO_VERIFY, stream_offset );
                	}
            	    else 
                	{
                    	    amc13Device_P->write( "SLINKXpress_cmd", adr, HAL::HAL_NO_VERIFY, stream_offset );
                	}
		
            	    try
                	{   
			    uint32_t done(0);	
			    std::cout << ", Command = " << std::hex <<adr << std::endl;
			    while (((done >> 1) &0x1 ) == 0 )
			    usleep(100000);
			    for (int i = 0 ; i < 51;i++)  
			    {
				amc13Device_P->read( "SLINKXpress_cmd_done", &done, stream_offset );
				if ( (done==0 && i == 50) || ( done != 0 && i < 2) )
				    std::cout << "LinkNo = " << linkno << ", Command = " << std::hex << adr << (rw==WRITE?", WRITE, ":", READ, ") << std::dec << ", count" << i << ", value=" << done << std::endl;
				if (done!=0) break; 
			    }
			    std::cout << "SLINKXpress_cmd = 0x" << std::hex << data << ", Command = " << adr << std::endl; 
                    	    amc13Device_P->pollItem( "SLINKXpress_cmd_done", 1, 50, &pollres, HAL::HAL_POLL_UNTIL_EQUAL, stream_offset );
			    std::cout << "poll result = "<< pollres << std::endl;
                            
                	}
            	    catch( HAL::TimeoutException &e )
                	{
                	    std::string error = "Timeout while sending command to SlinkExpress sender core. ";
			    std::stringstream errstr;
			    uint32_t slotread;
			    amc13Device_P->read("Geographic_Address", &slotread);
			    errstr << error << ", AMC13 PCI Slot = " << slotread << ", Slink Express Register Address = 0x" << std::hex << adr << ", linkno = " << std::dec << linkno << std::endl;
                    	    XCEPT_RETHROW( utils::exception::AMC13Exception, errstr.str(), e );
                	}    

            	    if ( rw == READ )
                	{
                	    amc13Device_P->read( "SLINKXpress_cmd_data", &data, stream_offset );
                	}
        	}
    	    catch( HAL::HardwareAccessException &e )
        	{
        	    std::string error = "Problems while sending command to SlinkExpress sender core.";
        	    XCEPT_RETHROW( utils::exception::AMC13Exception, error, e );
        	}
	}
/*    JRF I don't think any of these are used any more. TDO check.
 *    else if ( (dataSource_[linkno] == L6G_SOURCE) || (dataSource_[linkno] == L6G_CORE_GENERATOR_SOURCE) || (dataSource_[linkno] == L6G_LOOPBACK_GENERATOR_SOURCE) )
        {
            cmdstr = "L5gb_cmd_upload_SlX";
            datstr = "L5gb_data_upload_SlX";
            polstr = "L5gb_cmd_poll_SlX";
        }*/
    else if ( dataSource_[linkno] == GENERATOR_SOURCE )
        {
            if ( rw == READ )
                data = -1;
            return;
        }
    else 
        {
            std::stringstream msg;
            msg << "FATAL SOFTWARE BUG: dataSource " << dataSource_[linkno] << " not expected in SlinkExpressCore.";
            XCEPT_RAISE( utils::exception::AMC13Exception, msg.str() );
        }

}

