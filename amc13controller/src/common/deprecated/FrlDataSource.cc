#include <sstream>
#include "amc13/FrlDataSource.hh"
#include "amc13/Exception.hh"
#include "amc13/amc13Constants.h"
#include "amc13/loggerMacros.h"

amc13controller::FrlDataSource::FrlDataSource( HAL::HardwareDeviceInterface *device_P,
                                     utils::InfoSpaceHandler &appIS,
                                     Logger logger)
    : DataSourceIF( device_P, appIS, logger )
{
}

void
amc13controller::FrlDataSource::setDataSource() const
{
    if ( ( dataSource_ == GENERATOR_SOURCE ) ||
         ( dataSource_ == SLINK_SOURCE ) )
        {
            DEBUG( "Setting data source to 0x0. ");
            device_P->write( "FRAGMENT_DATA_SOURCE", 0x0 );
        }
    else
        {
            std::stringstream msg;
            msg << "Encountered illegal DataSource for operationMode \"" 
                << operationMode_ 
                << "\" : \""
                << dataSource_
                << "\". Cannot continue!";
            ERROR( msg );
            XCEPT_RAISE( utils::exception::AMC13Exception, msg.str() );

        }
}


