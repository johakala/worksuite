#include "amc13/FrlAddressTableReader.hh"
#include "hal/PCIHardwareAddress.hh"

/**
 * The implementation of this class has been chosen to increase
 * readability of the code.
 */
amc13controller::FrlAddressTableReader::FrlAddressTableReader() {

    /***********************************************************************************************************/
    /*                                      PCI Address Table : FrlController                                  */
    /*                                                                                                         */
    /*          item                           AdrSpace BAR      address         mask  r   w    description    */
    /***************************************************************************************&&&&&***************/
    createItem("enable",             HAL::CONFIGURATION,  0,  0x00000040,  0x00000001,  1,  1,  "");
    createItem("which",              HAL::CONFIGURATION,  0,  0x00000004,  0x00008000,  1,  0,  "");
    createItem("VendorId",           HAL::CONFIGURATION,  0,  0x00000000,  0xffffffff,  1,  0,  "");
    createItem("BAR0" ,              HAL::CONFIGURATION,  0,  0x00000010,  0xffffffff,  1,  1,  "");
    createItem("command",            HAL::CONFIGURATION,  0,  0x00000004,  0x000000ff,  1,  1,  "");
    createItem("CompileVersion",     HAL::CONFIGURATION,  0,  0x00000048,  0xffffffff,  1,  0,  "");
    createItem("FirmwareType",       HAL::CONFIGURATION,  0,  0x00000048,  0xfff00000,  1,  0,  "0xf03: DAQ; 0xf13: efed;");
    createItem("HardwareRevision",   HAL::CONFIGURATION,  0,  0x00000048,  0x000f0000,  1,  0,  "Two different versions exist: 1 and 2");
    createItem("FirmwareVersion",    HAL::CONFIGURATION,  0,  0x00000048,  0x0000ffff,  1,  0,  "");
    createItem("jtag",               HAL::CONFIGURATION,  0,  0x00000040,  0x00000002,  1,  1,  "");


    createItem("resetFifoMonCounter",       HAL::MEMORY,  0,  0x00000000,  0x00000001,  1,  1,  "resets the counters which monitor the Input Fifos Almost Full flags");
    createItem("enableFifoMonCounter",      HAL::MEMORY,  0,  0x00000000,  0x00000002,  1,  1,  "enables/disables the counters to monitor the Almost Full flags of the input fifos");    
    createItem("enableSoftTrigger",         HAL::MEMORY,  0,  0x00000000,  0x00000400,  1,  0,  "enable software trigger in version 0xff20");

    // not needed anymore; commented out since it overlaps with loop trigger functionality in firmware 1:
    //  createItem("TestLINKctn",               HAL::MEMORY,  0,  0x00000000,  0x0000f000,  1,  0,  "");
    createItem("softwareReset",             HAL::MEMORY,  0,  0x00000000,  0x00020000,  0,  1,  "");
    createItem("SelHighBERR",               HAL::MEMORY,  0,  0x00000000,  0x00200000,  1,  1,  "");
    createItem("UCtrlBERR",                 HAL::MEMORY,  0,  0x00000000,  0x00400000,  1,  0,  "");
    createItem("SlowREAD",                  HAL::MEMORY,  0,  0x00000000,  0x00800000,  1,  1,  "only valid for daq firmware");
    createItem("enableLink0",               HAL::MEMORY,  0,  0x00000000,  0x01000000,  1,  1,  "upper connector");
    createItem("enableLink1",               HAL::MEMORY,  0,  0x00000000,  0x02000000,  1,  1,  "lower connector");
    createItem("generateDESKEW",            HAL::MEMORY,  0,  0x00000000,  0x10000000,  1,  1,  "");
    createItem("SPYALLset",                 HAL::MEMORY,  0,  0x00000000,  0x40000000,  1,  1,  "");
    createItem("enableSPY",                 HAL::MEMORY,  0,  0x00000000,  0x80000000,  1,  1,  "");

    createItem("AddressBlockFifo",          HAL::MEMORY,  0,  0x00000004,  0xffffffff,  0,  1,  "");

    createItem("enableWCHisto",             HAL::MEMORY,  0,  0x00000008,  0x40000000,  1,  1,  "");
    createItem("WCHistoReset",              HAL::MEMORY,  0,  0x00000008,  0x80000000,  1,  1,  "");
    createItem("WChisto",                   HAL::MEMORY,  0,  0x00000008,  0xffffffff,  1,  1,  "");
    createItem("WCHistoBin",                HAL::MEMORY,  0,  0x00000008,  0x000000ff,  1,  1,  "");

    createItem("DescriptorFifo",            HAL::MEMORY,  0,  0x00000008,  0xffffffff,  0,  1,  "for event generator");

    createItem("Debug",                     HAL::MEMORY,  0,  0x00000010,  0xffffffff,  1,  1,  "");

    createItem("BXnumberLink0",             HAL::MEMORY,  0,  0x00000040,  0xffffffff,  1,  0,  "");
    createItem("BXnumberLink1",             HAL::MEMORY,  0,  0x00000044,  0xffffffff,  1,  0,  "");
    createItem("CurrentTrigNumLink0",       HAL::MEMORY,  0,  0x00000048,  0xffffffff,  1,  0,  "");
    createItem("CurrentTrigNumLink1",       HAL::MEMORY,  0,  0x0000004c,  0xffffffff,  1,  0,  "");
    createItem("MemBlockFree",              HAL::MEMORY,  0,  0x00000050,  0x000002ff,  1,  0,  "for event generator");
    createItem("StatusReg",                 HAL::MEMORY,  0,  0x00000060,  0xffffffff,  1,  0,  "");
  
    createItem("Spydebfifo",                HAL::MEMORY,  0,  0x00000054,  0xffffffff,  1,  0,  "");
    createItem("retryMyrPCI",               HAL::MEMORY,  0,  0x00000054,  0xffffffff,  1,  0,  "Counters for the PCI retry on the internal PCI bus (FRL-Myrinet)");
    createItem("CMCversion",                HAL::MEMORY,  0,  0x00000058,  0xffffffff,  1,  0,  "");
    createItem("BERRtestlink",              HAL::MEMORY,  0,  0x0000005c,  0xffffffff,  1,  0,  "");

    createItem("slowReadDataAv",            HAL::MEMORY,  0,  0x00000060,  0x10000000,  1,  0,  "Indicates if data for slow read is present");

    createItem("BlockSize",                 HAL::MEMORY,  0,  0x00000080,  0x0000ffff,  1,  1,  "payload without header in bytes");
    createItem("FRLHeaderSize",             HAL::MEMORY,  0,  0x00000084,  0x0000001f,  1,  1,  "header in 32 bit words");
    createItem("EvttoSpyFifo",              HAL::MEMORY,  0,  0x00000088,  0x00ffffff,  0,  1,  "event number to spy (max 1024)");

    createItem("SlowReadLow",               HAL::MEMORY,  0,  0x00000100,  0xffffffff,  1,  0,  "Fifo address for the SlowRead bits 00...31");
    createItem("SlowReadHigh",              HAL::MEMORY,  0,  0x00000104,  0xffffffff,  1,  0,  "Fifo address for the SlowRead bits 32...63");
    createItem("SlowReadUctrl",             HAL::MEMORY,  0,  0x00000108,  0xffffffff,  1,  0,  "Fifo address for the SlowRead slink uctrl");
    createItem("SlowReadNext",              HAL::MEMORY,  0,  0x0000010c,  0xffffffff,  1,  0,  "A read triggers the read of the next data work in slowReadFifo");

    createItem("StatusFIFOS",               HAL::MEMORY,  0,  0x00000110,  0xffffffff,  1,  0,  "");
    createItem("SlinkAddress",              HAL::MEMORY,  0,  0x00000114,  0x0000000f,  1,  1,  "");
    createItem("SlinkDeskew",               HAL::MEMORY,  0,  0x00000118,  0x00000001,  1,  1,  "");
    createItem("SlinkEnableDeskew",         HAL::MEMORY,  0,  0x00000118,  0x00000002,  1,  1,  "");
    createItem("DAQmode",                   HAL::MEMORY,  0,  0x0000011c,  0x00000001,  1,  1,  "");
    createItem("resetFIFO",                 HAL::MEMORY,  0,  0x0000011c,  0x40000000,  1,  1,  "");
    createItem("masterResetFIFO",           HAL::MEMORY,  0,  0x0000011c,  0x80000000,  1,  1,  "");

    createItem("TimerCounter64_1",          HAL::MEMORY,  0,  0x00000120,  0xffffffff,  1,  0,  "64 bit wide Timer counter, 1st loc (1 us units)");
    createItem("TimerCounter64_2",          HAL::MEMORY,  0,  0x00000124,  0xffffffff,  1,  0,  "64 bit wide Timer counter (2nd loc)");
    createItem("TriggerCounter64_1",        HAL::MEMORY,  0,  0x00000128,  0xffffffff,  1,  0,  "64 bit wide event counter (1st loc)");
    createItem("TriggerCounter64_2",        HAL::MEMORY,  0,  0x0000012c,  0xffffffff,  1,  0,  "64 bit wide event counter (2nd loc)");
    createItem("SlinkTest",                 HAL::MEMORY,  0,  0x00000130,  0x00000001,  1,  1,  "");
    createItem("SlinkDCBalance",            HAL::MEMORY,  0,  0x00000134,  0x00000001,  1,  1,  "");
    createItem("SlinkBadCRC2_Link0",        HAL::MEMORY,  0,  0x00000144,  0xffffffff,  1,  0,  "");
    createItem("SlinkBadCRC2_Link1",        HAL::MEMORY,  0,  0x0000014c,  0xffffffff,  1,  0,  "");
    createItem("SlinkBadCRC1_Link0",        HAL::MEMORY,  0,  0x00000140,  0xffffffff,  1,  0,  "");
    createItem("SlinkBadCRC1_Link1",        HAL::MEMORY,  0,  0x00000148,  0xffffffff,  1,  0,  "");
    createItem("FEDBadCRC1_Link0",          HAL::MEMORY,  0,  0x00000150,  0xffffffff,  1,  0,  "");
    createItem("FEDBadCRC2_Link0",          HAL::MEMORY,  0,  0x00000154,  0xffffffff,  1,  0,  "");
    createItem("FEDBadCRC1_Link1",          HAL::MEMORY,  0,  0x00000158,  0xffffffff,  1,  0,  "");
    createItem("FEDBadCRC2_Link1",          HAL::MEMORY,  0,  0x0000015c,  0xffffffff,  1,  0,  "");
    createItem("LFFOnCycles1_Link0",        HAL::MEMORY,  0,  0x00000160,  0xffffffff,  1,  0,  "");
    createItem("LFFOnCycles2_Link0",        HAL::MEMORY,  0,  0x00000164,  0x00000fff,  1,  0,  "");
    createItem("FEDClk_Link0",              HAL::MEMORY,  0,  0x00000164,  0x0ffff000,  1,  0,  "");
    createItem("LFFOnCycles1_Link1",        HAL::MEMORY,  0,  0x00000168,  0xffffffff,  1,  0,  "");
    createItem("LFFOnCycles2_Link1",        HAL::MEMORY,  0,  0x0000016c,  0x00000fff,  1,  0,  "");
    createItem("FEDClk_Link1",              HAL::MEMORY,  0,  0x0000016c,  0x0ffff000,  1,  0,  "");
    createItem("CountDataReceive",          HAL::MEMORY,  0,  0x00000174,  0xffffffff,  1,  0,  "is incremented for every word received from SLINK");
    createItem("expsrcid_0",                HAL::MEMORY,  0,  0x00000178,  0x00000fff,  1,  1,  "expected fed source id link 0; written by host");
    createItem("expsrcid_1",                HAL::MEMORY,  0,  0x00000178,  0x0fff0000,  1,  1,  "expected fed source id link 1; written by host");
    createItem("latchFifoMonCounters",      HAL::MEMORY,  0,  0x0000017c,  0x00000000,  0,  1,  "command to latch the counters to monitor the almost full flags of the input fifos");
    createItem("baseClkCounter_0",          HAL::MEMORY,  0,  0x00000180,  0xffffffff,  1,  0,  "lower 32 bit of 64MHz clock counter");
    createItem("baseClkCounter_1",          HAL::MEMORY,  0,  0x00000184,  0xffffffff,  1,  0,  "higher 32 bit of 64MHzclock counter");
    createItem("Fifo_Counter0_Link0",      HAL::MEMORY,  0,  0x00000188,  0xffffffff,  1,  0,  "lower 32 bit of input fifo 0 almost full counter");
    createItem("Fifo_Counter1_Link0",      HAL::MEMORY,  0,  0x0000018c,  0xffffffff,  1,  0,  "higher 32 bit of input fifo 0 almost full counter");
    createItem("Fifo_Counter0_Link1",      HAL::MEMORY,  0,  0x00000190,  0xffffffff,  1,  0,  "lower 32 bit of input fifo 1 almost full counter");
    createItem("Fifo_Counter1_Link1",      HAL::MEMORY,  0,  0x00000194,  0xffffffff,  1,  0,  "higher 32 bit of input fifo 1 almost full counter");
	       

    /******************************** items for the embedded FEDEmulator ***************************************/
    /*          item                           AdrSpace BAR      address         mask   r   w   description    */

    createItem("enablePCITrigger",         HAL::MEMORY,  0,  0x00000000,  0x00000400,  1,  1,  "");
    createItem("eventCounterOn",           HAL::MEMORY,  0,  0x00000000,  0x00000800,  1,  1,  "1: eventId from Counter; 0: eventId from descriptor");
    createItem("suspendGenerator",         HAL::MEMORY,  0,  0x00000000,  0x00001000,  1,  1,  "overlaps with TestLINKctn (not used in generator)");
    createItem("autoTrigger",              HAL::MEMORY,  0,  0x00000000,  0x00008000,  1,  1,  "overlaps with TestLINKctn (not used in generator)");
    createItem("startGenerator",           HAL::MEMORY,  0,  0x00000000,  0x00010000,  1,  1,  "");
    createItem("sdramMask",                HAL::MEMORY,  0,  0x00000028,  0x0000ffff,  1,  1,  "!! overlaps with WCHistBin");

    createItem("softwareTrigger",          HAL::MEMORY,  0,  0x00000090,  0x00000000,  0,  1,  "generate a software trigger");
    createItem("wrongInsert",              HAL::MEMORY,  0,  0x0000009c,  0x00000001,  0,  1,  "generate synch error by inserting a wrong event number");
    createItem("wrongReplace",             HAL::MEMORY,  0,  0x0000009c,  0x00000002,  0,  1,  "generate synch error by replacing a event with a wrongly numbered one");
    createItem("missEvent",                HAL::MEMORY,  0,  0x0000009c,  0x00000004,  0,  1,  "generate synch error by omitting an event number");
    createItem("changeSequence",           HAL::MEMORY,  0,  0x0000009c,  0x00000008,  0,  1,  "generate synch error by changing the event number sequence");
    createItem("resync",                   HAL::MEMORY,  0,  0x000000a0,  0x00000001,  0,  1,  "emulate a resync: current evt no to 1");

    createItem("makeCRCError",             HAL::MEMORY,  0,  0x00000094,  0x00000000,  0,  1,  "generate a crc error for next generated event");
    createItem("firstEventNumber",         HAL::MEMORY,  0,  0x00000098,  0xffffffff,  1,  1,  "first event number");
    /* in design 1 this number is a counter of descriptor words in the fifo. The MSB is the fifo fulll flag (but 11). A descriptor has 2 words!
       in design 2 this number is a counter which counts the pendging triggers (0-511). */
    createItem("pendingTriggers",          HAL::MEMORY,  0,  0x00000050,  0x00000fff,  1,  0,  "is MemBlockFree in the DAQ firmware. mask fff for design 1; 1f for design 2");
    createItem("sttsReadyThreshold",       HAL::MEMORY,  0,  0x000000a4,  0x000001ff,  1,  1,  "for smaller values in pending triggers give ready");
    createItem("sttsBusyThreshold",        HAL::MEMORY,  0,  0x000000a8,  0x000001ff,  1,  1,  "for larger values in pending triggers give busy");

    createItem("GeneratorEnable",          HAL::MEMORY,  0,  0x00000000,  0x00010000,  1,  1,  "Only for generator version");

    createItem("controlWord",              HAL::MEMORY,  0,  0x00000000,  0xffffffff,  1,  1,  "the entire control word for debugging");
    /**********************************************************************************************************/
}
 
