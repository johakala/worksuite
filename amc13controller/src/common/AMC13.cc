#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include <iomanip>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include "d2s/utils/Exception.hh"
#include "d2s/utils/loggerMacros.h"
#include "amc13controller/amc13Constants.h"
#include "amc13controller/HardwareDebugger.hh"
#include "hal/PCIAddressTableASCIIReader.hh"
#include "uhal/ConnectionManager.hpp"
#include "uhal/log/log.hpp"
#include "uhal/log/exception.hpp"
#include "toolbox/math/random.h"
#include "toolbox/regex.h"
#include "hal/linux/StopWatch.hh"
#include "d2s/utils/Watchdog.hh"
#include "d2s/utils/Utils.hh"
//#include "amc13/AMC13EventGenerator.hh"
#include "amc13controller/DataSourceFactory.hh"
#include "amc13/Exception.hh"

amc13controller::AMC13::AMC13(xdaq::Application *xdaq,
        amc13controller::ApplicationInfoSpaceHandler &appIS,
        amc13controller::StatusInfoSpaceHandler &statusIS,
        amc13controller::InputStreamInfoSpaceHandler &inputIS,
        amc13controller::OutputStreamInfoSpaceHandler &outputIS,
        amc13controller::AMC13Monitor &monitor
        /*amc13controller::HardwareLocker &hwLocker*/)
: logger_(xdaq->getApplicationLogger()),
    appIS_(appIS),
    statusIS_(statusIS),
    monitor_(monitor),
    //hwLocker_(hwLocker),
    amc13IS_(xdaq, this),
    //streamConfigIS_( xdaq, this, &appIS_, streams_ ), //JRF TODO these are currently in the AMC13Controller. Check if that's gonna work
    inputIS_(inputIS),
    outputIS_(outputIS),
    deviceLock_(toolbox::BSem::FULL, true), //JRF note. this is set to recursive, so the same thread can lock it multiple times without causing a deadlock. of course it must unlock it the same number of times.
    //JRF removing this
    //dataTracker_(amc13IS_, &amc13Device_P)
    //JRF replaced this with a vector of streamDataTrackers
    //,dataTracker2_( tcpIS_, &amc13Device_P )
    deviceT1_P(NULL),
    deviceT2_P(NULL),
    deviceAmc13_P(NULL),
    IPBusManager_P(NULL)


{
    DEBUG("Constructor of AMC13Controller");
    HAL::StopWatch watch(1);
    watch.reset();
    watch.start();
    //amc13Device_P = NULL;
    slot_ = 0;
    hwCreated_ = false;
    //const DataTracker inputStreamDataTracker(inputIS_, &amc13Device_P);
    for (int i = 0; i < NB_INPUT_STREAMS; ++i)
    {
        inputStreams_.push_back(false); //add one entry for each output stream, index is the stream id. this vector is used only to store the enable boolean for that stream
        //streamBpCounterHigh_.push_back(0);
        //streamBpCounterOld_.push_back(0);
        //inputStreamDataTrackers_.push_back(inputStreamDataTracker); //JRF add the 4 data trackers we require for the inputstream infospace tracking
        //inputIS_.registerTrackerItems(inputStreamDataTrackers_[i]);
    }
    operationMode_ = "n.a.";
    for (int i = 0; i < NB_OUTPUT_STREAMS; ++i)
    {
        outputStreams_.push_back(false); //add one entry for each output stream, index is the stream id. this vector is used only to store the enable boolean for that stream
        streamBpCounterHigh_.push_back(0);
        streamBpCounterOld_.push_back(0);
    }					 

    //JRF TODO shall we put this back once we've removed all the input and tcp stream items
    //monitor.addInfoSpace( &amc13IS_ );
    //JRF NOTE that the inputIS_ is managed by the AMC13Controller so no need to do this here.

    //-----SET FLASH LIST ITEMS-----
    std::string tmp[] =
    {
        "READY_Percent",
        "Run_Count",
        "State_Raw",
        "State_Enc"
    };
    flashListItems_ = std::set<std::string>( tmp, tmp + sizeof(tmp)/sizeof(tmp[0]) );
    //------------------------------
    uhal::setLogLevelTo( uhal::Warning() );
    watch.stop();
    uint32_t stime = watch.read();
    watch.reset();
    std::stringstream info;
    info << "AMC13 hardware object constructor took "
        << stime << "us.";
    INFO(info.str());
}

amc13controller::AMC13::~AMC13()
{     
    this->suspendEvents();
    shutdownHwDevice();
}


//JRF TODO, might want to remove this, now I'm building in the Launcher code directly, eventually we can use it from the boston libs, but for now I copied the files into our project.
/*
   int amc13controller::AMC13::AMC13Initialize(std::vector<std::string> strArg,
   std::vector<uint64_t> intArg) {

   uint32_t amcMask = 0;
   amc13::AMC13* amc13 = deviceAmc13_P; // for convenience

   if( strArg.size() ==0) {
   printf("usage: i <inputs> <options>\n");
   return 0;
   }

   bool fakeTTC = false;       // "T" option for loop-back TTC
   bool fakeData = false;      // "F" for fake data
   bool monBufBack = false;    // "B" for monitor buffer backpressure
   bool runMode = true;        // "N" to disable run mode

   for( size_t i=0; i<strArg.size(); i++) {

   char fchar = strArg[i].c_str()[0];

   if( isdigit( fchar) ) {
   amcMask = amc13->parseInputEnableList( strArg[i], true); // use 1-based numbering
   printf("parsed list \"%s\" as mask 0x%x\n", strArg[i].c_str(), amcMask);
   } else if( fchar == '*') {    // special case
   amcMask = amc13->read( amc13::AMC13Simple::T1, "STATUS.AMC_LINK_READY_MASK");
   printf("Generated mask 0x%03x from STATUS.AMC_LINK_READY_MASK\n", amcMask);
   } else if( isalpha( fchar) && (strArg[i].size() == 1)) {                  // it's a letter
   switch( toupper(strArg[i].c_str()[0])) {
   case'T':                // TTS outputs TTC
   printf("Enabling TTS as TTC for loop-back\n");
   fakeTTC = true;
   break;
   case 'F':               // fake data
   printf("Enabling fake data\n");
   fakeData = true;
   break;
   case 'B':               // monitor buffer backpressure
   printf("Enabling monitor buffer backpressure, EvB will stop when MB full\n");
   monBufBack = true;
   break;
   case 'N':               // no run mode
   printf("Disable run mode\n");
   runMode = false;
   break;
   default:
   printf("Error: Unknown option: %s. Not initializing AMC13!\n", strArg[i].c_str());
   return 0;
   }
   } else {
   printf("Error: Unknown option: %s. Not initializing AMC13!\n", strArg[i].c_str());
   return 0;
   }
   }

   switch ( amc13->read(amc13::AMC13::T1, "CONF.SFP.ENABLE_MASK" ) ) {
   case 0:
   case 1:
   if ( amcMask > 0 ) {
   break;
   }
   else {
   printf("Must enable at least one AMC1-AMC12 slot\n");
   return 0;
   }
   case 3:
   if ( ((amcMask & 0b000000111111) > 0) && ((amcMask & 0b111111000000) > 0) ) {
   break;
   }
   else {
   printf("2 DAQ channels: must enable at least one AMC1-AMC6 and one AMC7-AMC12\n");
return 0;
}
case 7:
if ( ((amcMask & 0b000000001111) > 0) && ((amcMask & 0b000011110000) > 0) && ((amcMask & 0b111100000000) > 0) ) {
    break;
}
else {
    printf("3 DAQ channels: must enable at least one from each AMC1-AMC4, AMC5-AMC8, AMC9-AMC12\n");
    return 0;
}
default:
amc13::Exception::UnexpectedRange e;
e.Append("Error in AMC13Initialize - CONF.SFP.ENABLE_MASK not 0, 1, 3, or 7\n");
throw e;
}

try {

    amc13->endRun();          // take out of run mode
    printf("AMC13 out of run mode\n");
    amc13->fakeDataEnable( fakeData);
    amc13->localTtcSignalEnable( fakeTTC);
    amc13->monBufBackPressEnable(monBufBack);
    amc13->AMCInputEnable( amcMask);
    if( runMode) {
        amc13->startRun();
        printf("AMC13 is back in run mode and ready\n");
    } else {
        printf("AMC13 is *not* in run mode.  Use \"start\" to start run\n");
    }
} catch (uhal::exception::exception & e) {
    printf("Argh!  Something threw in the control functions\n");
}

return 0;
}
*/


//Function to map the names used in the UI to the corresponding nodes.
void amc13controller::AMC13::mapNames()
{
    std::ofstream elog( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/debug/wildcard.e")).c_str() );
    std::vector< std::pair<std::string,Board> > chips;
    chips.push_back(std::pair<std::string,Board>("T1",T1));
    chips.push_back(std::pair<std::string,Board>("T2",T2));
    int dupCount = 0;

    for (int i = 0; i < 2; i++)
    {
        uhal::Node const &baseNode = ((i==0) ? deviceT1_P : deviceT2_P)->getNode();

        for (uhal::Node::const_iterator iNode = baseNode.begin(); iNode != baseNode.end();
                iNode++)
        {
            boost::unordered_map<std::string,std::string> parameters = iNode->getParameters();

            if(parameters.find("Status") == parameters.end()) continue;

            std::string path = iNode->getPath();

            if ( path.size() <= 3 ) DEBUG("Path too short.");
            if( path.substr(path.size()-3) == "_LO" )
            {
                int bitShift;
                getAddress(*iNode, &bitShift);
                continue;
            }

            //create UI name for given monitorable.
            std::string parRow(parameters["Row"]), parCol(parameters["Column"]);
            //NB: If either row or column name begins with _ (many of them are
            //simply _2 or _3) then it provides no useful information and should
            //not be included.
            if ( parRow.substr(0,1) == "_" ) parRow = "";
            else if ( parCol.substr(0,1) == "_" ) parCol = "";
            else parRow += "_"; //if both are non blank, add a delimeter.

            std::string uiName = parRow + parCol;
     
            parseUiName(&uiName, &(*iNode), "prewildcard");

            //---------- Insert Wildcards ----------//
            insertWildcards(&uiName);
            //--------------------------------------//

            parseUiName(&uiName, &(*iNode), "postwildcard");

            if (uiMap_.find(uiName) != uiMap_.end())
            {
                dupCount++;
                if (uiDups_.find(uiName) == uiDups_.end())
                {
                    //add the one already in uiDups.
                    uiDups_.insert( std::pair<std::string, const uhal::Node*>
                            (uiName,uiMap_[uiName].second) );
                }
                uiDups_.insert( std::pair<std::string, const uhal::Node*>
                        (uiName, &(*iNode)) );
            }


            uiMap_.insert(
                    std::pair<std::string, std::pair<Board,const uhal::Node*> >(
                        uiName,
                        std::pair<Board,const uhal::Node*>(chips[i].second, &(*iNode))
                        )
                    );
        }
    }

}

void amc13controller::AMC13::insertWildcards(std::string *str)
{
    std::stringstream ss(*str);
    std::string newStr = "";
    char c;

    //------exceptions-----
    //Quantities which could be stream-like, but for whatever reason we would prefer
    //them not to be, may be added here to prevent wildcards being inserted.
    if ( str->substr( 0, std::min( uint32_t(12),uint32_t(str->size()) ) ) 
            == "ROM_Data_SFP" ) return;
    if ( str->substr( 0, std::min( uint32_t(9),uint32_t(str->size()) ) ) 
            == "Words_SFP" ) return;
    //---------------------
    ss >> std::noskipws;

    while (ss >> c)
    {
        //look for pattern SFPx:
        if (c == 'S')
        {
            newStr += c;
            if (ss >> c)
            {
                newStr += c;
                if (c == 'F')
                {
                    if (ss >> c)
                    {
                        newStr += c;
                        if (c == 'P')
                        {
                            if (ss >> c)
                            {
                                if (isdigit(c))
                                {
                                    newStr += "*";
                                    if (ss >> c)
                                    {
                                        newStr+= c;
                                    }
                                }
                                else newStr+= c;
                            }
                        }
                    }
                }
            }
        }
        //look for pattern AMCxx
        else if (c == 'A')
        {
            newStr += c;
            if (ss >> c)
            {
                newStr += c;
                if (c == 'M')
                {
                    if (ss >> c)
                    {
                        newStr += c;
                        if (c == 'C')
                        {
                            if (ss >> c)
                            {
                                if (isdigit(c))
                                {
                                    std::string amcX = "";
                                    amcX += c;
                                    if (ss >> c)
                                    {
                                        amcX += c;
                                        if (isdigit(c) && amcX != "13")
                                        {
                                            newStr += "**";
                                        }
                                        else
                                        {
                                            newStr+= amcX;
                                        }
                                    }   
                                }
                                else newStr += c;
                            }
                        }
                    }
                }
            }
        }
        else newStr += c;
    }

    *str = newStr; 
}

    void
amc13controller::AMC13::parseStreamDescription(std::string *description, 
        uint32_t streamSize, uint32_t streamOffset)
{
    std::string desc = *description;
    insertWildcards(&desc);

    //check if any wildcards exist, if not we need not edit description.
    size_t lastStar = desc.find_last_of('*');
    if (lastStar == std::string::npos) return;

    //check the description didn't already have SFP0..2 type format.
    bool isDoubleDot = false;
    if ( desc.size() > (lastStar+3) ) 
        isDoubleDot = ( desc.substr(lastStar+1,2) == ".." );
    if (isDoubleDot) return; //we don't need to edit the original description.

    //insert 0..2 or relevant numbers:
    std::stringstream newDesc;
    size_t firstStar = ( desc.at(lastStar-1) == '*' ) ?
        lastStar - 1 : lastStar;

    newDesc << desc.substr(0,firstStar) << streamOffset << ".."
        << streamSize + streamOffset - 1 << desc.substr(lastStar+1);

    (*description) = newDesc.str();
}

void amc13controller::AMC13::printDups()
{
    std::ofstream outFile( (std::string(std::getenv("AMC13CONTROLLER_ROOT")) 
                + std::string("/debug/nodes_dups.txt")).c_str() );
    for (std::multimap<std::string, const uhal::Node*>::iterator
            it = uiDups_.begin(); it != uiDups_.end(); it++)
    {
        const uhal::Node *iNode = it->second;
        int intNull;
        outFile << it->first << " -- " << getAddress(*iNode,&intNull) << '\n';
    }
}

void amc13controller::AMC13::printMap()
{
    std::ofstream outFile( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/debug/nodes_map.txt")).c_str() );
    int intNull;
    for (boost::unordered_map<std::string, std::pair<Board,const uhal::Node*> >::iterator
            it = uiMap_.begin(); it != uiMap_.end(); it++)
    {
        const uhal::Node *iNode = it->second.second;
        std::string addr = iNode->getPath().substr(4);
        bool isSplit = (addr.substr( addr.size()-3 ) == "_HI");
        //(addr.find("_HI") == addr.size()-3);
        std::string desc = isSplit ? loWords_[addr.substr(0,addr.size()-3)]
            ->getDescription()                 :
            iNode->getDescription()                ;
        boost::unordered_map<std::string,std::string> parameters = iNode->getParameters();
        outFile << std::left << std::setw(30) << it->first << std::setw(40) 
            << getAddress(*iNode,&intNull)
            << std::string(isSplit ? "\t(64)":"\t(32)")
            << " [" << parameters["Status"] << "]"
            << std::endl << "- " << desc << "\n";
    }
}

void amc13controller::AMC13::printOrderedMap()
{
    std::ofstream outFile( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/debug/nodes_ordered_map.txt")).c_str() );
    boost::unordered_map<std::string, std::multimap<int,const uhal::Node*> > tables;

    for (boost::unordered_map<std::string, std::pair<Board,const uhal::Node*> >::iterator
            it = uiMap_.begin(); it != uiMap_.end(); it++)
    {
        const uhal::Node *iNode = it->second.second;
        boost::unordered_map<std::string,std::string> parameters = iNode->getParameters();

        std::string tableName = parameters["Table"];
        int statusLevel;
        sscanf(parameters["Status"].c_str(),"%d",&statusLevel);

        if (tables.find(tableName) == tables.end()) //if table not logged
        {
            std::multimap<int,const uhal::Node*> newTable;
            newTable.insert( std::pair<int,const uhal::Node*>(statusLevel,iNode) );
            tables.insert( std::pair<std::string, std::multimap<int,const uhal::Node*> >
                    (tableName, newTable) );
        }
        else //if table already exists, add to it
        {
            tables[tableName].insert(std::pair<int,const uhal::Node*>
                    (statusLevel, iNode) );
        }
    }

    //loop over new map of tables:
    for (boost::unordered_map<std::string, std::multimap<int,const uhal::Node*> >::iterator
            it = tables.begin(); it != tables.end(); it++)
    {
        outFile << it->first << "\n=================================\n";
        std::multimap<int,const uhal::Node*> iTable = it->second;

        std::multimap<int,std::multimap<std::string,std::map<std::string,const uhal::Node*> > >
            mappedTable;

        //create a new map which orders rows and columns alphabetically.
        for (std::multimap<int,const uhal::Node*>::iterator it2 = iTable.begin();
                it2 != iTable.end(); it2++)
        {
            const uhal::Node *iNode = it2->second;
            boost::unordered_map<std::string,std::string>
                parameters = iNode->getParameters();
            std::string column = parameters["Column"];
            std::string row = parameters["Row"];

            std::map<std::string,const uhal::Node*> inner;
            inner.insert( std::pair<std::string,const uhal::Node*>(column, iNode) );

            std::multimap<std::string,std::map<std::string,const uhal::Node*> > mid;
            mid.insert( std::pair<std::string,std::map<std::string,const uhal::Node*> >
                    (row, inner) );

            mappedTable.insert( std::pair //...
                    <int,std::multimap<std::string,std::map<std::string,const uhal::Node*> > >
                    (it2->first, mid) );
        }

        //Loop over new mapped table and print values.
        for (std::multimap<int,std::multimap<
                std::string,std::map<std::string,const uhal::Node*> > >::iterator
                it2 = mappedTable.begin(); it2 != mappedTable.end(); it2++)
        {
            int level = it2->first;
            std::multimap<std::string,std::map<std::string,const uhal::Node*> >
                iRow = it2->second;

            for (std::multimap<std::string,std::map<std::string,const uhal::Node*> >
                    ::iterator it3 = iRow.begin(); it3 != iRow.end(); it3++)
            {
                std::string row = it3->first;
                std::map<std::string,const uhal::Node*> iCol = it3->second;

                for (std::map<std::string,const uhal::Node*>::iterator
                        it4 = iCol.begin(); it4 != iCol.end(); it4++)
                {
                    std::string column = it4->first;
                    const uhal::Node *iNode = it4->second;

                    std::string addr = iNode->getPath().substr(4);
                    bool isSplit = ( addr.substr(addr.size()-3) == "_HI" );
                    std::string desc = isSplit ? loWords_[addr.substr(0,addr.size()-3)]
                        ->getDescription()                 :
                        iNode->getDescription()                ;
                    std::string size = isSplit ? "(64)":"(32)";
                    boost::unordered_map<std::string,std::string> 
                        parameters = iNode->getParameters();

                    std::string line = row + "_" + column + " -- " + desc;
                    outFile << std::left << std::setw(100) << line
                        << size << " Status: " << level << "\n";
                }
            }
        } 
        outFile << "---------------------------------\n\n";
    }

}


void amc13controller::AMC13::generateNodeCode()
{
    std::ofstream outFile( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/debug/generated_code.txt")).c_str() );
    boost::unordered_map
        <
        std::string,
        std::set< std::pair<std::string,const uhal::Node*> >  
            >
            tables;
    //idea here is to be able to sort the vector by the uiName, so that the result is
    //alphebetically ordered.
    //Could optionally extend to nested pairs if we want values sorted by status level
    //first, hence use of set<pair> rather than map.

    for (boost::unordered_map<std::string, std::pair<Board,const uhal::Node*> >::iterator
            it = uiMap_.begin(); it != uiMap_.end(); it++)
    {
        std::string uiName = it->first;
        const uhal::Node *iNode = it->second.second;

        boost::unordered_map<std::string,std::string> 
            parameters = iNode->getParameters();

        std::string tableName = parameters["Table"];

        if (tables.find(tableName) == tables.end()) //if table not logged
        {
            std::set< std::pair<std::string,const uhal::Node*> > newTable;
            newTable.insert
                ( 
                 std::pair<std::string,const uhal::Node*> (uiName,iNode) 
                );
            tables.insert
                ( 
                 std::pair
                 <
                 std::string,
                 std::set< std::pair<std::string,const uhal::Node*> > 
                 >
                 (tableName, newTable) 
                );
        }
        else //if table already exists, add to it
        {
            tables[tableName].insert
                (
                 std::pair<std::string,const uhal::Node*> (uiName, iNode)
                );
        }
    }

    std::ostringstream singleIS, inStreamIS, outStreamIS, 
        singleMon, inStreamMon, outStreamMon;

    boost::unordered_map 
        <
        std::string,
        std::set< std::pair<std::string,const uhal::Node*> >
            >::iterator iTable;

    int errCount = 0;

    //Add enabled items to input and output streamIS
    inStreamIS << "createbool( \"Enabled\", false, \"\", PROCESS, \
        \"'1' if this AMC input is enabled\", 1 );\n";
    outStreamIS << "createbool( \"Enabled\", false, \"\", PROCESS, \
        \"'1' if this SFP output is enabled\", 1 );\n";

    for ( iTable = tables.begin(); iTable != tables.end(); iTable++)
    {
        std::string tableName = iTable->first;
        //exceptions:
        if (tableName.find("HCAL") != std::string::npos) continue;
        //we are not interested in the HCAL values.
        if (tableName.find("TTC_History") != std::string::npos) continue;
        //this should exclude two tables, there are far too many values in
        //here to display in a table on the webpage.

        std::set< std::pair<std::string,const uhal::Node*> > items = iTable->second;

        bool hasSingles(false), hasInputs(false), hasOutputs(false);
        //bools to determine what kind of item sets are required for each table.


        std::set< std::pair<std::string,const uhal::Node*> >::iterator iItem;
        for ( iItem = items.begin(); iItem != items.end(); iItem++)
        {
            std::string uiName = iItem->first;
            const uhal::Node *node = iItem->second;
            //exceptions:
            if (node->getPermission() == uhal::defs::WRITE) continue;

            //Get status level:
            boost::unordered_map<std::string,std::string> 
                params = node->getParameters();
            int statusLevel;
            sscanf(params["Status"].c_str(),"%d",&statusLevel);

            //Get size of value and description:
            std::string addr = node->getPath().substr(4);
            bool isSplit = ( addr.substr(addr.size()-3) == "_HI" );
            std::string desc = isSplit ? loWords_[addr.substr(0,addr.size()-3)]
                ->getDescription()                 :
                node->getDescription()                ;
            std::string dataSize = isSplit ? "64":"32";
            std::string updateType = (tableName == "0_Board") ? "NOUPDATE" :
                ( (flashListItems_.count(uiName)) ? "ALWAYSUPDATE" : "UPDATE" );
            //(std::string("HW") + dataSize);

            //Work out format:
            std::string parFormat = params["Format"];
            std::string format = (parFormat.substr(0,3) == "TTS") ? parFormat :
                ( (parFormat == "d") ? "dec" : "hex" );
            //if ( format.substr(0,3) != "TTS" ) format = "hex";

            //pointer for generic output to a specific stream:
            std::ostringstream *outPntr;

            //Check if this is a stream quantity and determine size if so.
            int streamSize = uiDups_.count(uiName);
            if ( streamSize == 1 || streamSize == 0 ) 
                //0 allows for not present, given we are searching the duplicates
            {
                if (!hasSingles)
                {
                    hasSingles = true;
                    //Add relevant item set:
                    singleMon << "\nnewItemSet(\"" << tableName << "\", 1);\n";
                    singleIS << "\n//----------" << tableName << "----------\n";
                } 

                //------Monitor Code-----
                singleMon << "addItem(\"" << tableName << "\", \"" << uiName
                    << "\", is);\n";    
                //-----------------------
                outPntr = &singleIS;
            }
            else if ( streamSize == 3 ) //indicates an output stream
            {
                parseStreamDescription(&desc, streamSize, 0);
                //This code will go within a for loop, with counter i. 
                if (!hasOutputs)
                {
                    hasOutputs = true;
                    //Add relevant item sets (need 3):
                    outStreamMon << "\nname << \"" << tableName << "_\" << i;\n"
                        << "newItemSet(name.str(), 1);\n";
                    outStreamIS << "\n//----------" << tableName << "----------\n";

                    //print enabled property for this table
                    outStreamMon << "addItem(name.str(), \"Enabled\", \
                        is, \"\", i);\n";    
                }
                //add enabled value to top of slinkexpress tables:
                /*if(!Slink_Express_ && tableName == "Slink_Express")
                {
                    Slink_Express_ = true;
                    outStreamMon << "addItem(name.str(), \"Enabled\", \
                        is, \"\", i);\n";    
                    outStreamIS << "createuint32( \"Enabled\", 0, \"dec\", PROCESS, \
                        \"'1' if this SFP output is enabled\", 1 );\n";
                }*/

                //------Monitor Code-----
                outStreamMon << "addItem(name.str(), \"" << uiName
                    << "\", is, \"\", i);\n";
                //-----------------------
                outPntr = &outStreamIS;
            }
            else if ( streamSize == 12 )
            {
                parseStreamDescription(&desc, streamSize, 1);
                //This code will go within a for loop, with counter i. 
                if (!hasInputs)
                {
                    hasInputs= true;
                    //Add relevant item sets (need 3):
                    inStreamMon << "\nname << \"" << tableName << "_\" << i;\n"
                        << "newItemSet(name.str(), 1);\n";
                    inStreamIS << "\n//----------" << tableName << "----------\n";
                    
                    //print enabled property for this table:
                    inStreamMon << "addItem(name.str(), \"Enabled\", \
                        is, \"\", i);\n";    
                }
                //add enabled value to top of table:
                /*if (!AMC_Links_ && tableName == "AMC_Links")
                {
                    AMC_Links_ = true;
                    inStreamMon << "addItem(name.str(), \"Enabled\", \
                        is, \"\", i);\n";    
                    inStreamIS << "createuint32( \"Enabled\", 0, \"dec\", PROCESS, \
                        \"'1' if this AMC input is enabled\", 1 );\n";
                }*/

                //------Monitor Code-----
                inStreamMon << "addItem(name.str(), \"" << uiName
                    << "\", is, \"\", i);\n";    
                //-----------------------
                outPntr = &inStreamIS;
            }
            else //should not be the case...
            {
                errCount++;
                ERROR("In printOrderedMap2: unrecognised streamsize when searching "
                        "uiDups_. streamsize: " << streamSize << ", value: "
                        << uiName << ".");
                continue;
            }
            //Infospace output:
            (*outPntr) << "createuint" << dataSize << "( \"" << uiName << "\", 0, "
                << "\"" << format << "\", " << updateType << ", \"" << desc 
                << "\", " << statusLevel << ");\n";
        }

        if (hasInputs) inStreamMon << "name.str(\"\");\n";
        if (hasOutputs) outStreamMon << "name.str(\"\");\n";

    }

    outFile << "========== utils::Monitor : single ==========\n" << singleMon.str()
        << "\n========== utils::Monitor : input =========\n" << inStreamMon.str()
        << "\n========== utils::Monitor : output ==========\n" << outStreamMon.str()
        << "\n========== IS : single ==========\n" << singleIS.str()
        << "\n========== IS : input ==========\n" << inStreamIS.str()
        << "\n========== IS : output ==========\n" << outStreamIS.str();

    generateAMC13MonitorFile(&singleMon, &inStreamMon, &outStreamMon);
    generateInfoSpaceFiles(&singleIS, &inStreamIS, &outStreamIS);

    //INFO("Generated node code, total " << errCount << " errors.");

}

void amc13controller::AMC13::generateAMC13MonitorFile( std::ostringstream *singleStream,
        std::ostringstream *inputStream,
        std::ostringstream *outputStream  )
{
    std::ifstream proto( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/codegen/AMC13MonitorProto.cc")).c_str() );
    std::ofstream outFile( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/codegen/AMC13Monitor.cc")).c_str() );
    //don't immediately replace existing file in case of error.

    std::string inLine;
    //search prototype and look for escape sequence -- `!
    while (std::getline(proto, inLine))
    {
        if ( inLine.substr(0,2) == "`!" )
        {
            if (inLine.substr(2,1) == "s") //singleStream
                outFile << singleStream->str();
            else if (inLine.substr(2,1) == "i") //inputStream
                outFile << inputStream->str(); 
            else if (inLine.substr(2,1) == "o") //outputStream
                outFile << outputStream->str();
            else std::cout << "Error in generateAMC13MonitorFile: unrecognised "
                "escape sequence\n" << inLine << inLine.substr(2,3);
        }
        else outFile << inLine << "\n";
    }
}

void amc13controller::AMC13::generateInfoSpaceFiles( std::ostringstream *singleStream,
        std::ostringstream *inputStream,
        std::ostringstream *outputStream  )
{
    //Single stream:
    std::ifstream proto( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/codegen/StatusInfoSpaceHandlerProto.cc")).c_str() );
    std::ofstream outFile( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/codegen/StatusInfoSpaceHandler.cc")).c_str() ); 

    std::string inLine;

    //search prototype and look for escape sequence -- `!
    while (std::getline(proto, inLine))
    {
        if ( inLine.substr(0,2) == "`!" )
            outFile << singleStream->str();
        else outFile << inLine << "\n";
    }
    proto.close();
    outFile.close();

    //Input stream
    proto.open( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/codegen/InputStreamInfoSpaceHandlerProto.cc")).c_str() );
    outFile.open( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/codegen/InputStreamInfoSpaceHandler.cc")).c_str() );

    //search prototype and look for escape sequence -- `!
    while (std::getline(proto, inLine))
    {
        if ( inLine.substr(0,2) == "`!" )
            outFile << inputStream->str();
        else outFile << inLine << "\n";
    }
    proto.close();
    outFile.close();

    //Output stream
    proto.open( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/codegen/OutputStreamInfoSpaceHandlerProto.cc")).c_str() );
    outFile.open( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/codegen/OutputStreamInfoSpaceHandler.cc")).c_str() );

    //search prototype and look for escape sequence -- `!
    while (std::getline(proto, inLine))
    {
        if ( inLine.substr(0,2) == "`!" )
            outFile << outputStream->str();
        else outFile << inLine << "\n";
    }
    proto.close();
    outFile.close();
}

//Function for initially testing printing of status registers as done in the base libs
//(st command in Tool2). (HGC)
//Gets all nodes and their names and values and prints them to a text file
void amc13controller::AMC13::printStatus()
{
    std::ofstream outFile( (std::string(std::getenv("AMC13CONTROLLER_ROOT")) 
                + std::string("/debug/nodes_all.txt")).c_str() );
    std::ofstream logFile( (std::string(std::getenv("AMC13CONTROLLER_ROOT"))
                + std::string("/debug/nodes_all.log")).c_str() );

    std::vector< std::pair<std::string,Board> > chips;
    chips.push_back(std::pair<std::string,Board>("T1",T1));
    chips.push_back(std::pair<std::string,Board>("T2",T2));

    int exceptionCount = 0;
    int successCount = 0;
    int noReadCount = 0;
    int splitSuccess = 0;
    int splitFailure = 0;

    for (int i = 0; i < 2; i++)
    { 
        uhal::Node const &baseNode = ((i==0) ? deviceT1_P : deviceT2_P)->getNode(); 

        for (uhal::Node::const_iterator iNode = baseNode.begin(); iNode != baseNode.end();
                iNode++)
        {
            boost::unordered_map<std::string,std::string> parameters = iNode->getParameters();
            if(parameters.find("Status") == parameters.end()) continue;
            //for (boost::unordered_map<std::string,std::string>::iterator
            //     iMap = parameters.begin(); iMap != parameters.end(); iMap++)
            //{ outFile << iMap->first << "\t|\t" << iMap->second << "\n"; }

            int bitShift;
            std::string address = getAddress(*iNode,&bitShift);

            if (bitShift == -2) continue;

            //If this is the HI word of a LO/HI pair, then the description will be stored
            //in the corresponding LO word. Have to access it from there:
            std::string description;
            if (bitShift == -1) description = iNode->getDescription(); //if non split register
            else
            {
                if (loWords_.find(address) != loWords_.end())
                {
                    description = loWords_[address]->getDescription();
                    splitSuccess++;
                }
                else 
                {
                    std::cout << "Missing description: " << address << "\n";
                    splitFailure++;
                }
                iNode++; //avoid reprocessing Lo node (was already done in previous if)
            }

            std::string table = parameters["Table"];
            std::string row = parameters["Row"];
            std::string column = parameters["Column"]; 

            uint64_t nodeValue = -1; //rogue. Unread registers will be 0xffffffff
            std::string exceptionType;

            try
            {
                nodeValue = (bitShift < 0) ? readRegister(chips[i].second,address)
                    : readRegister(chips[i].second,address,bitShift);
                exceptionType = "no exception";
                successCount++;
            }
            catch(uhal::exception::NoBranchFoundWithGivenUID &e)
            {
                std::cout << "Exception: uhal::exception::NoBranchFoundWithGivenUID\n"
                    << "\tfor address " << address << "\n"; 
                exceptionType = "uhal::exception::NoBranchFoundWithGivenUID";
                exceptionCount++;
                logFile << "Error accessing: " << address << "\n\t\tbitshift value was: "
                    << bitShift << "\n";
            }
            catch(uhal::exception::ReadAccessDenied &e)
            {
                noReadCount++;
                nodeValue= -2; //rogue value for write only nodes
            }
            catch(...)
            {
                std::cout << "Exception: default exception\n"
                    << "\tfor address " << address << "\n";
                exceptionType = "default exception";
                exceptionCount++;
            }

            outFile << chips[i].first << "::" << table << "::" << row << "::" 
                << column << " " << address << " << " << std::hex << nodeValue
                << "\nDescription: " << description
                << "\nException: " << exceptionType 
                << "\n---------------------------------------\n";
        }
    }
    outFile << std::dec << "Total successes: " << successCount 
        << ", total failures: " << exceptionCount << ", write only: "
        << noReadCount << "\n" << "HI/LO Success: " << splitSuccess
        << ", HI/LO Failure: " << splitFailure << "\n";

}

std::string amc13controller::AMC13::getAddress(uhal::Node const &node, int *bitShift)
{
    std::string address = node.getPath().substr(4);
    //It appears as though node.getPath() always prepends "TOP." to the address
    //needed to access the register. substr(4) removes this.
    boost::to_upper(address);

    //special case: accounting for an error with lower case in an address name:
    if (address == "STATUS.SFP.SFP0.LSC.LINK_STATUS")
        address = "STATUS.SFP.SFP0.LSC.LINK_Status";

    *bitShift = -1; //rogue value of -1 indicates that the register is not
    //split between HI and LO words.

    //reconstruct any registers split betweeen Hi and Lo:
    //[Adapted from code in base libs Status.cc SparseCellMatrix::Add]

    bool isLo( address.substr(address.size()-3) == "_LO" );
    bool isHi( address.substr(address.size()-3) == "_HI" );
    if (isLo || isHi)
    {
        std::string baseAddress = address.substr(0,address.size()-3);
        if (isHi)
        {
            //only process if Lo word has been placed. If not, add to a 'queue'
            if (loWords_.find(baseAddress) != loWords_.end())
            {
                uint32_t mask = loWords_[baseAddress]->getMask(); //get mask of low word
                //count number of non-zero bits in mask, i.e. number of bits used by low word
                while ( (mask & 0x1) == 0) 
                {
                    mask >>= 1;
                }
                int count = 0;
                while ( (mask & 0x1) == 1) 
                {
                    count++;
                    mask >>= 1;
                }
                *bitShift = count;
            }
            else
            {
                hiWords_.insert(std::pair<std::string, const uhal::Node*>(baseAddress,&node) );
                *bitShift = -2; //rogue value to indicate it's not ready for processing.
            }

        } 
        else //i.e. isLo
        {
            *bitShift = -2; //rogue value -2 indicates LO word has just been added
            //N.B: No bit shifting necessary for the low word, this is enforced in the
            //overloaded readRegister method. 
            loWords_.insert(std::pair<std::string, const uhal::Node*>(baseAddress,&node) );
            //this allows to keep track of when the low word has been placed, and also makes
            //the mask accessible when placing the hi word
            if (hiWords_.find(baseAddress) != hiWords_.end())
            {
                getAddress(*hiWords_[baseAddress],bitShift);
            }
        }
        address = baseAddress;
    }


    return address;
}

/**
 * Algorithm: Scan through all possible PCI indices (0-15) and probe if 
 * an FRL is present at this index. If an FRL is found, read out the slot
 * number for the corresponding FRL (the slot number is read from the 
 * backplane: It has some lines which for each slot encodes the slot number.
 * These lines are mapped to a register in the Bridge FPGA). If the read
 * slot number is equal to the slotnumber which has been given to this 
 * program as configuration parameter, we have found the correct frl.
 **/
void amc13controller::AMC13::createAMC13Device() throw(utils::exception::Exception)
{
    /* JRF TODO, this needs putting back once we have a hardwareLocker class for AMC13
     *        if (!hwLocker_.lockedByUs())
     *               return;
     *                      */

     if (hwCreated_) return;


    DEBUG( "createAMC13Device()" );

    try
    {
        std::string connectionFile = std::string("file://") + 
                std::string(std::getenv("CACTUS_ROOT")) +
                std::string("/etc/amc13/") +
                appIS_.getstring("ConnectionXML");
        IPBusManager_P = new uhal::ConnectionManager(connectionFile);
    }
    catch (uhal::exception::exception &e)
    {
        throw utils::exception::AMC13Exception("Connection file not found.",
                e.what(), __FILE__, __LINE__, "AMC13::configure");
    }

    try {
        if (deviceAmc13_P == NULL) {

            //Create the uhal HwInterface object for the T1 board
            try {
                deviceT1_P = new uhal::HwInterface(
                        IPBusManager_P->getDevice( appIS_.getstring("Connection_Prefix") 
                            + std::string(".T1") )                );
            }
            catch (uhal::exception::exception & e) {
                //if this throws, give context 
                e.append("- In AMC13 constructor allocating T1.");
                throw;
            }
        }

        //Create the uhal HwInterface object for the T2 board
        if (deviceAmc13_P == NULL) {

            try {
                deviceT2_P = new uhal::HwInterface(
                        IPBusManager_P->getDevice( appIS_.getstring("Connection_Prefix") 
                            + std::string(".T2") )                );
            }
            catch (uhal::exception::exception & e) {
                //if this throws, give context 
                e.append("- In AMC13 constructor allocating T2.");
                throw;
            }
        }

        if (deviceAmc13_P == NULL) {
            try { 
                deviceAmc13_P = new amc13::AMC13(*deviceT1_P, *deviceT2_P);
            } 
            catch (uhal::exception::exception & e) {
                //if this throws, give context 
                e.append("- In AMC13 constructor allocating T2.");
                throw;
            }
        }

    }
    catch (uhal::exception::exception &e)
    {
        std::ostringstream msg;
        msg << "No AMC13 Card found for Connection PRefix " << appIS_.getstring("Connection_Prefix");
        ERROR(msg.str());
        XCEPT_RAISE(utils::exception::AMC13Exception, msg.str());
    }

    hwCreated_ = true;

}

void amc13controller::AMC13::shutdownHwDevice() throw(utils::exception::AMC13Exception)
{

    this->hwlock();

    hwCreated_ = false;

    //boardType_ = amc13controller::AMC13::UNKNOWN;

    if (fwChecker_P)
    {
        delete fwChecker_P;
        fwChecker_P = NULL;
    }

    this->hwunlock();
}
/*
   amc13controller::SlinkExpressCore *
   amc13controller::AMC13::getSlinkExpressCore()
   {
//if (slCore_P)
//    return slCore_P;
//else
return NULL;
}
*/
/////////////////////////////////// state transistions  ////////////////////////////

void amc13controller::AMC13::instantiateHardware() throw(utils::exception::Exception)
{
    operationMode_ = appIS_.getstring("OperationMode");

    //JRF Loop over all inputStreams and set the inputStreams_ boolean vectors. These contain enable flags for use elsewhere.
    //JRF outputStreams_ are set on configure as it now depends on the fedEnableMask which is passed in during configure state change.
    uint32_t index(0);

    for (std::vector<bool>::iterator streams_it = inputStreams_.begin(); streams_it != inputStreams_.end(); ++streams_it)
    {
        index = streams_it - inputStreams_.begin();
        *streams_it = dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[index].getField("enable"))->value_;
        //dataSource_[index] = dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[index].getField("DataSource"))->value_; //appIS_.getstring( "DataSource" );
    }

    this->createAMC13Device();

    //JRF TODO, do we need to do something similar for the AMC13?
    // We have created the Hardware successfully. In case we had been killed
    // previously while transferring data, it could be that the TCP connections
    // are still happily alive and transfer data. We try to shut down the
    // connections here. This should at least guarantee that we do not send
    // data anymore, even if the receiver does not receive our reset.
    // We reset both streams in any case. (It could be that one of the streams
    // is not anymore in the configuraton, but if it was in the previous
    // configuration we want to be sure that no data is flowing from that
    // stream anymore.)
    for (std::vector<bool>::iterator streams_it = inputStreams_.begin(); streams_it != inputStreams_.end(); ++streams_it)
    {
        try
        {
            //amc13Device_P->setBit("eth_10gb_req_TCP_rst", HAL::HAL_NO_VERIFY, stream_offset);
        }
        catch (HAL::HardwareAccessException &e)
        {
            FATAL("Hardware Access failed.");
            XCEPT_RETHROW(utils::exception::AMC13Exception,
                    "Hardware access failed.", e);
        }
    } //end for loop
}

amc13::AMC13 * amc13controller::AMC13::getAMC13Device() {
	return deviceAmc13_P;
}

void amc13controller::AMC13::configure() throw(utils::exception::AMC13Exception)
{
    // At this stage we know that there is at least one stream enabled (otherwise
    // the AMC13Controller would not have called us) and the hardwre has been
    // instantiated. We check once more if the instantiation was successfull:
    //------MOVED FROM CONSTRUCTOR -------
    //uhal connection setup (HGC):
    try
    {
        //if (!hwCreated_)
        //    this->createAMC13Device();
        if (!hwCreated_)
            return;
        this->hwlock();

        if ( operationMode_ == AMC13_MODE_MONITOR ) 
        {
            INFO( "We are running in Monitoring only mode, we do not need to configure the hardware. " );
            return;
        }


        if ( operationMode_ == AMC13_MODE_NORMAL )
        {
            INFO( "Setting Mode to NORAML" );
        }
        else if ( operationMode_ == AMC13_MODE_EFED )
        {
            INFO( "Setting Mode to EFED" );
        }
        else if ( operationMode_ == AMC13_MODE_NO_DAQ )
        {
            INFO( "Setting Mode to NO_DAQ" );
        }
        else if ( operationMode_ == AMC13_MODE_LEAF )
        {
            INFO( "Setting Mode to LEAF" );
        }

        DEBUG( "configure starts here." );
        deviceAmc13_P->reset( amc13::AMC13Simple::T1);
        deviceAmc13_P->resetCounters();


	//JRF now we parse the fedEnableMask so we know which outputs are enabled 
	utils::fedEnableMask fedEnableMask = utils::parseFedEnableMask( appIS_.getstring("fedEnableMask") );
	uint32_t fedId(0);
	uint8_t mask = 0x0;
	uint32_t index(0);
	try {
		for (std::vector<bool>::iterator streams_it = outputStreams_.begin(); streams_it != outputStreams_.end(); ++streams_it)
		{
			index = streams_it - outputStreams_.begin();
			//*streams_it = dynamic_cast<xdata::Boolean *>(appIS_.getvectoroutputs("OutputPorts")[index].getField("enable"))->value_;

			fedId = dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvectoroutputs("OutputPorts")[index].getField("srcId"))->value_;
			//dynamic_cast<xdata::Boolean *>(appIS_.getvectoroutputs("OutputPorts")[index].getField("srcId"))->value_; //cfgInfoSpaceP_->getUInt32("fedId");
			try {
				mask = fedEnableMask.at(fedId);
				std::cout << "fedEnableMask for fedID: " << fedId  << ", = " << (uint32_t)mask << std::endl;
				if ((mask&0x5)==0x1)
					*streams_it = true;
				else 
					*streams_it = false;
			} catch (...) {
				*streams_it = false;
			}

		}
	}
	catch (...)
	{
		std::string msg =
			toolbox::toString("Expected FED ID %d in the FED vector but could not find it.",
					fedId);
		XCEPT_RAISE(utils::exception::AMC13Exception, msg.c_str());
	}



	//JRF configure inputs. TODO, move to a method
	// Must have at least one AMC enabled for each DAQ channel.
	// We can check this with a bitwise and, since the the first 12 bits of amcMask
	// simply represent if an AMC is enabled or not. Leaving the number to & with
	// in binary is a better visual representation than using a hex number.

	uint32_t amcMask(0);
	for (std::vector<bool>::iterator streams_it = inputStreams_.begin(); streams_it != inputStreams_.end(); ++streams_it)
	{
		if (*streams_it) {
			index = streams_it - inputStreams_.begin();
			amcMask |= (1<<index);
		}
	}
	std::cout << amcMask << std::endl;

	deviceAmc13_P->endRun();
	deviceAmc13_P->AMCInputEnable( amcMask);

	deviceAmc13_P->fakeDataEnable( appIS_.getbool("genEnable"));
	if ( appIS_.getbool("genEnable") )
		deviceAmc13_P->write(amc13::AMC13Simple::T1,"CONF.AMC.FAKE_DATA_SIZE",appIS_.getuint32("genSize"));
	deviceAmc13_P->localTtcSignalEnable( false); //we are using TCDS
	deviceAmc13_P->monBufBackPressEnable(false);
	// JRF end of configure inputs.
	//

	// JRF configure DAQ outputs
	// JRF TODO, move this to a method
	uint32_t daqMask(0);
	for (std::vector<bool>::iterator streams_it = outputStreams_.begin(); streams_it != outputStreams_.end(); ++streams_it)
	{
		if (*streams_it) {
			index = streams_it - outputStreams_.begin();
			daqMask |= (1<<index);
		}
	}

	deviceAmc13_P->sfpOutputEnable( daqMask );

	if (daqMask == 0)
		deviceAmc13_P->daqLinkEnable( false);
	else
		deviceAmc13_P->daqLinkEnable( true);

	deviceAmc13_P->resetDAQ();
	// JRF End of configure DAQ outputs

	// JRF configure source IDs for daq links
	for (std::vector<bool>::iterator streams_it = outputStreams_.begin(); streams_it != outputStreams_.end(); ++streams_it)
	{
		if (*streams_it) {
			index = streams_it - outputStreams_.begin();
			deviceAmc13_P->setFEDid(index, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvectoroutputs("OutputPorts")[index].getField("srcId"))->value_);
		}
	}
	// JRF END configure Source IDs

	//JRC Configure some global flags
	deviceAmc13_P->localTtcSignalEnable( appIS_.getbool("localTtc") );
	deviceAmc13_P->monBufBackPressEnable( appIS_.getbool("monBufBp") );
	deviceAmc13_P->daqIgnore( appIS_.getbool("ignoreDaq") );

	//JRF, TODO move this to enable()!!!
	deviceAmc13_P->startRun();

    }
    catch (uhal::exception::exception &e)
    {
	    DEBUG("Caught in AMC13.");
	    throw utils::exception::Exception("Connection file not found.",
			    e.what(), "AMC13", __LINE__, "configure");
    }

    //----------------------------
    //Now print T1 firmware version to console:
    //ValWord<uint32_t> fvT1(0);
    //try {
    //    fvT1 = deviceT1_P->getNode("STATUS.FIRMWARE_VERS").read();
    //    deviceT1_P->dispatch();
    //}
    //catch (uhal::exception::exception & e) {
    //    e.append("- In AMC13 constructor getting firmware version.");
    //    throw;
    //}
    //std::cout << "\n\n\nharrysprintoutishere:\n" << std::hex << fvT1 << std::endl;
    //----------------------------
    //Call function to print Status registers to console
    //printStatus();
    //----------------------------
    mapNames();
    printMap();
    printOrderedMap();
    printDups();
    generateNodeCode();
    //HGC TODO Add a seperate StreamInfoSpace (stream size 4) for BGOs if needed.
    //This will require adding BGO* options to the wildcard function as well.
    //----------------------------
    //Now use fwchecker to update firmware version on webpage (through statusIS_)
    fwChecker_P = new amc13controller::FirmwareChecker(this, appIS_.getbool("noFirmwareVersionCheck"));

    //statusIS_.setuint32("Info_T1_Ver", fwChecker_P->getAMC13T1FirmwareVersion());
    //statusIS_.setuint32("Info_T2_Rev", fwChecker_P->getAMC13T2FirmwareVersion());
    //...and the rest of the values in the board table 
    //statusIS_.setuint32("Info_SerialNo", this->readRegister("Info_SerialNo"));
    //statusIS_.setuint32("Info_T2_SerNo", this->readRegister("Info_T2_SerNo"));
    //statusIS_.setuint32("Output_FED", this->readRegister("Output_FED"));

    statusIS_.setuint64( "Info_DNA", this->readRegister("Info_DNA") );
    statusIS_.setuint32( "Info_Init_B", this->readRegister("Info_Init_B") );
    statusIS_.setuint32( "Info_SerialNo", this->readRegister("Info_SerialNo") );
    statusIS_.setuint32( "Info_T1_Done", this->readRegister("Info_T1_Done") );
    statusIS_.setuint32( "Info_T1_Ver", this->readRegister("Info_T1_Ver") );
    statusIS_.setuint64( "Info_T2_DNA", this->readRegister("Info_T2_DNA") );
    statusIS_.setuint32( "Info_T2_Rev", this->readRegister("Info_T2_Rev") );
    statusIS_.setuint32( "Info_T2_SerNo", this->readRegister("Info_T2_SerNo") );
    statusIS_.setuint32( "Output_FED", this->readRegister("Output_FED") );
    statusIS_.setuint64( "info_SerialNo", this->readRegister("info_SerialNo") );
    //----------------------
    //if (!hwCreated_)
    //    return;

//    this->hwlock();
    //// catch all hardware access related problems:
    //try
    //{
    try
    {
	    // in case we were killed brute force, we need to switch DAQ-OFF here.
	    DEBUG("Switching DAQ-OFF in slink express links which participate in this run");
	    for (std::vector<bool>::iterator streams_it = outputStreams_.begin(); streams_it != outputStreams_.end(); ++streams_it)
	    {
		    if (*streams_it)
			    daqOff(streams_it - outputStreams_.begin());
	    }
    }
    catch (...)
    {
	    //JRF TODO, handle this exception correctly, we should raise an error, allow completion of configure and then go to failed so that the monitoring pages are ok
	    ERROR("daqOff Failed!!!");
    }

    this->hwunlock();


    //    // we need to do the same with the 10Gbit link

    /* 
    //set enabled parameters:
    for (uint32_t i = 0; i < NB_INPUT_STREAMS; i++)
    {
    bool iEnabled = dynamic_cast<xdata::Boolean *>(
    appIS_.getvector("InputPorts").at(i).getField("enable")
    )->value_;
    DEBUG( i << " enabled? bool: " << iEnabled << ", uint32: " << uint32_t(iEnabled));
    inputIS_.setCurrentStream( i );
    inputIS_.setuint32( std::string("Enabled"), uint32_t(iEnabled) );
    }

    inputIS_.writeInfoSpace();
    */

    //JRF now we reset the QSFP
    //amc13Device_P->setBit("QSFP_reset_r_bit");

    //    //JRF Now we loop over all streams and if they are enabled we enable the hardware for that stream.
    //    uint32_t stream_offset(0), index(0);
    //    for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    //    {
    //        stream_offset = (streams_it - streams_.begin()) * 0x4000; // this is the HAL address offset to apply for each stream

    //        if (*streams_it)
    //        {                                                //if we are using the stream, we enable it.
    //            amc13Device_P->setBit("QSFP_reset_r_bit"); //JRF TODO we might want to reset the stream regardless whether it's in or out of the run.
    //            amc13Device_P->setBit("enable_FED_link", HAL::HAL_DO_VERIFY, stream_offset);
    //        }
    //    }

    //    //JRF Note. We stop the loop here and do the wait, like that we save a bit of time since we wait once for all 4 streams.

    //    //JRF We don't unlock the hardware here because we are using a recursive lock.
    //    ::usleep(1000); // The reset takes 600us

    //    //uint64_t dest;
    //    //amc13Device_P->read64( "FEDx_link_setup", &dest  );
    //    //std::cout << "FEDx_Link_Setup value " << dest << std::endl;

    //    //////////////////////////////////////////////////////////////////////////
    //    //                                                                      //
    //    // Handle the input side:   Slink Express                               //
    //    //                                                                      //
    //    //////////////////////////////////////////////////////////////////////////

    //    //JRF Note. we do this outside the loop since all the streams are handled inside the method. We should change the name to setDataSources() plural!
    //    DEBUG("Setting up the Data Source");
    //    amc13controller::DataSourceIF *sourceHandler = DataSourceFactory::createDataSource(amc13Device_P, appIS_, logger_);
    //    if (sourceHandler == NULL)
    //        DEBUG("Wrong DATA SOURCE");
    //    //JRF TODO handle this error gracefully...
    //    else
    //    {
    //        sourceHandler->setDataSource();
    //        if (operationMode_ != AMC13_MODE)
    //            //delete (dynamic_cast<amc13controller::AMC13DataSource *>(sourceHandler));
    //            DEBUG("Wrong DATA SOURCE");
    //        //JRF TODO, handle other cases. Note. for the moment we only have this mode.
    //    }
    //    //delete(sourceHandler);

    //    //std::cout << "DEBUG!!!" << std::endl;

    //    for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    //    {
    //        index = streams_it - streams_.begin();
    //        stream_offset = (index)*0x4000;
    //        if (*streams_it)
    //        { //JRF  only do anything to the stream if it's being used. TODO check that the hardware locking etc is ok when not using streams.

    //            DEBUG("reset_status_counters in AMC13");
    //            amc13Device_P->setBit("reset_status_counters", HAL::HAL_NO_VERIFY, stream_offset); //RESET_COUNTERS
    //            streamBpCounterHigh_[index] = 0;
    //            streamBpCounterOld_[index] = 0;

    //            INFO("Suspend Events (stop event generator in case it was not done during a brutal kill of the previous run)");
    //            this->suspendEvents(); // in case previous run was brute force killed

    //            DEBUG("Setting FED IDs for streams which participate in this run.");
    //            amc13Device_P->write("SLINKXpress_FEDID_set", dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("expectedFedId"))->value_, HAL::HAL_DO_VERIFY, stream_offset); //L10gb_FEDID_SlX0 //JRF NOTE changed the name from streams to InputPorts
    //                                                                                                                                                                                                                   //JRF note we have to mirror this value to the flat application infospace paramter for samim:
    //                                                                                                                                                                                                                   //std::stringstream tmpName;
    //                                                                                                                                                                                                                   //tmpName.str("");
    //                                                                                                                                                                                                                   //tmpName << "expectedFedId_" << index;
    //                                                                                                                                                                                                                   //appIS_.setuint32( tmpName.str(), dynamic_cast<xdata::UnsignedInteger32*>(appIS_.getvector( "InputPorts" )[index].getField("expectedFedId"))->value_ );

    //            DEBUG("Set the backpressure generation");
    //            amc13Device_P->write("Window_trg_stop", appIS_.getuint32("Window_trg_stop"));                                                                                                                        //dynamic_cast<xdata::UnsignedInteger32*>(appIS_.getvector( "InputPorts" )[index].getField("Window_trg_stop"))->value_, HAL::HAL_DO_VERIFY, stream_offset  );//Window_trg_stop
    //            amc13Device_P->write("nb_frag_before_BP", dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("nb_frag_before_BP"))->value_, HAL::HAL_DO_VERIFY, stream_offset); //nb_frag_before_BP_L0

    //            //////////////////////////////////////////////////////////////////////////
    //            //                                                                      //
    //            // Setup the 10GB TCP link to the DAQ                                   //
    //            //                                                                      //
    //            //////////////////////////////////////////////////////////////////////////

    //            // JRF check that the links are up.
    //            DEBUG("Check that the 10Gb DAQ Ethernet link is up"); //JRF TODO, add the stream number to the debug statements.
    //            try
    //            {
    //                //JRF polling the serdes status we look for bits 0, 2, 3 and 4 to be set.
    //                //Bit(0) : Asserted to indicate that the block synchronizer has established synchronization
    //                //Bit(1) : Asserted by the BER monitor block to indicate a Sync Header high bit error rate greater than 10-4.
    //                //Bit(2) : Asserted when the TX channel is ready to transmit data. Because the readyLatency on this Avalon-ST interface is 0,the MAC may drive tx_ready as soon as it comes out of reset.
    //                //Bit(3) : Asserted when the RX reset is complete.
    //                //Bit(4) : When asserted, indicates that the PCS is sending data to the MAC. Because the readyLatency on this Avalon-ST interface is 0,the MAC must be ready to receive data whenever this sign
    //                //JRF TODO put this back in when dom fixes the firmware...
    //                uint32_t value;
    //                //amc13Device_P->pollItem( "eth_10gb_serdes_status", 0x1D, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL , stream_offset);
    //                //JRF TODO, we should distinguish between SERDES on Ethernet and SERDES for SLink. make sure we have status params for infospace for both with sensible names
    //                amc13Device_P->read("eth_10gb_serdes_status", &value, stream_offset); //SERDES_STATUS
    //                                                                                        //JRF TODO, store this value in the stream infospace for monitoring... unless we do this exclusively in the monitor.

    //                //std::cout << "serdes status: " << value << std::endl;
    //            }
    //            catch (HAL::TimeoutException &e)
    //            {
    //                std::stringstream err;
    //                err << "Could not poll on DAQ link status. (poll result: " << pollres << " )";
    //                ERROR(err.str());
    //                //mdioInterface_P->mdiounlock();
    //                this->hwunlock();
    //                XCEPT_RETHROW(utils::exception::AMC13Exception, err.str(), e);
    //            }

    //            //JRF This has never been used... but we'll add it just in case...
    //            uint32_t netmask = makeIPNum(appIS_.getstring("IP_NETMASK"));                                       //although we set this for each stream, we will use only a global parameter.
    //            amc13Device_P->write("eth_10gb_ctrl_IP_network_def", netmask, HAL::HAL_DO_VERIFY, stream_offset); //IP_NETMASK
    //            if (netmask)
    //            {
    //                uint32_t gateway = makeIPNum(appIS_.getstring("IP_GATEWAY"));
    //                if (gateway == 0)
    //                {
    //                    std::stringstream msg;
    //                    msg << "The netmask is set to " << appIS_.getstring("IP_NETMASK")
    //                        << " but the gateway is set to 0. You must set the gateway correctly if you set the netmask, "
    //                        << "since the AMC13Controller assumes you want to route your packets over a gateway if you "
    //                        << "set the netmask. (You can also leave the netmask at 0 and operate the AMC13 in the amc13-network "
    //                        << "without being able to route packets to other networks.)";
    //                    this->hwunlock();
    //                    XCEPT_RAISE(utils::exception::AMC13Exception, msg.str());
    //                }
    //                else
    //                {
    //                    amc13Device_P->write("eth_10gb_ctrl_IP_gateway_def", gateway, HAL::HAL_DO_VERIFY, stream_offset);
    //                }
    //            }

    //            std::string sourceIPstr = dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[streams_it - streams_.begin()].getField("SourceIP"))->value_; //appIS_.getstring( "SourceIP" );
    //            uint32_t sourceIp;
    //            if (sourceIPstr == "auto") //JRF note that this mode is almost never used any more. I leave it in for completeness
    //            {
    //                amc13Device_P->read("eth_10gb_ctrl_IPS_def", &sourceIp, stream_offset); //IP_SOURCE
    //                if (sourceIp == 0)
    //                {
    //                    std::stringstream msg;
    //                    msg << "The Source IP address for stream " << index << " is set to 0 in the hardware even though it should have been set automatically "
    //                        << "by some funky script or by DHCP. This automatic mechanism seem to have failed. You need "
    //                        << "to either debug this, or set the IP_SOURCE parameter to the correct ip address of the AMC13.";
    //                    this->hwunlock();
    //                    XCEPT_RAISE(utils::exception::AMC13Exception, msg.str());
    //                }
    //            }
    //            else //JRF in this case we now check the boolean SourceIpOverride, if it is true, we write the IP if it is false, we check the IP.
    //            {

    //                if (appIS_.getbool("SourceIpOverride"))
    //                {
    //                    sourceIp = makeIPNum(sourceIPstr);
    //                    // In this case we simply override what is already in the SourceIP from the xml.
    //                    amc13Device_P->write("eth_10gb_ctrl_IPS_def", sourceIp, HAL::HAL_DO_VERIFY, stream_offset); //IP_SOURCE
    //                }
    //                else
    //                {
    //                    // In this case we check what is in the AMC13 first and throw and error if it is different to the XML
    //                    amc13Device_P->read("eth_10gb_ctrl_IPS_def", &sourceIp, stream_offset); //IP_SOURCE
    //                    if (sourceIp != makeIPNum(sourceIPstr))
    //                    {
    //                        std::stringstream err;
    //                        err << "Source IP on AMC13 in slot "
    //                            << getSlot()
    //                            << ", Input Port "
    //                            << index
    //                            << ", differs from that in the configuration. IP on board: "
    //                            << std::hex << sourceIp << std::dec << ", (" << makeIPString(sourceIp) << ")"
    //                            << ", Hostname in configuration: "
    //                            << sourceIPstr
    //                            << " ("
    //                            << makeIPString(makeIPNum(sourceIPstr)) << " (" << makeIPNum(sourceIPstr) << ") "
    //                            << "). Normally the IP address should have been set automatically by the ferolconfig service which runs at PC boot time on the frlpc40 machines."
    //                            << " If the IP address is ZERO, then you should restart this service to recover the correct IP address. If the IP address is WRONG you should contact an expert. If you ARE the expert, good luck!"
    //                            << std::endl;
    //                        ERROR(err.str());

    //                        XCEPT_DECLARE(utils::exception::AMC13Exception, top, err.str());
    //                        throw(top);
    //                    }
    //                }
    //            }

    //            // check that our IP address and our MAC addresse are unique on the network.
    //            doArpProbe(index);

    //            amc13Device_P->write("eth_10gb_ctrl_IPD_def",
    //                                   makeIPNum(dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[index].getField("DestinationIP"))->value_),
    //                                   HAL::HAL_DO_VERIFY, stream_offset);                                      //IP_DEST
    //            amc13Device_P->write("eth_10gb_ctrl_IP_network_def", 0x0, HAL::HAL_DO_VERIFY, stream_offset); //JRF TODO check if we need to set this at all... //
    //            amc13Device_P->write("eth_10gb_ctrl_IP_gateway_def", 0x0, HAL::HAL_DO_VERIFY, stream_offset); //JRF TODO check if we need to set this at all... //
    //            amc13Device_P->write("eth_10gb_port_Src_def",
    //                                   dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("TCP_SOURCE_PORT"))->value_,
    //                                   HAL::HAL_DO_VERIFY, stream_offset); //TCP_SOURCE_PORT_FED0
    //            amc13Device_P->write("eth_10gb_port_Dest_def",
    //                                   dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("TCP_DESTINATION_PORT"))->value_,
    //                                   HAL::HAL_DO_VERIFY, stream_offset);                  //TCP_DESTINATION_PORT_FED0
    //            amc13Device_P->setBit("tx_ready_bit", HAL::HAL_DO_VERIFY, stream_offset); //New
    //            amc13Device_P->setBit("rx_ready_bit", HAL::HAL_DO_VERIFY, stream_offset); //New

    //            //eth_10gb_ctrl_Init_def
    //            amc13Device_P->write("eth_10gb_ctrl_Init_def", appIS_.getuint32("TCP_CONFIGURATION"), HAL::HAL_NO_VERIFY, stream_offset); //TCP_CONFIGURATION_FED0
    //            amc13Device_P->write("TCP_OPTIONS_MSS_SCALE", appIS_.getuint32("TCP_OPTIONS_MSS_SCALE"));                                 //TCP_OPTIONS_MSS_SCALE_FED0
    //            amc13Device_P->write("eth_10gb_Cong_wind_def",
    //                                   dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("TCP_CWND"))->value_,
    //                                   HAL::HAL_DO_VERIFY, stream_offset); //TCP_CWND_FED0 //JRF NOTE, this is the only TCP tuning param which is specific to each stream for the moment.
    //            amc13Device_P->write("eth_10gb_ctrl_timer_RTT_def", appIS_.getuint32("TCP_TIMER_RTT"));
    //            amc13Device_P->write("eth_10gb_ctrl_timer_RTT_sync_def", appIS_.getuint32("TCP_TIMER_RTT_SYN"));
    //            amc13Device_P->write("eth_10gb_ctrl_timer_persist_def", appIS_.getuint32("TCP_TIMER_PERSIST"));
    //            amc13Device_P->write("eth_10gb_ctrl_thresold_retrans_def", appIS_.getuint32("TCP_REXMTTHRESH"));
    //            amc13Device_P->write("eth_10gb_ctrl_Rexmt_CWND_Sh_def", appIS_.getuint32("TCP_REXMTCWND_SHIFT"));
    //        } //JRF end of if(streams_)
    //    }     //JRF End of Loop over steams_

    //    // Issue an ARP request and wait for the Answer. With a timeout.
    //    // Since the connection is point to point, we do not need to retry.
    //    // If ARP packets get lost, there is something strange in our setup.
    //    uint32_t maxTries = appIS_.getuint32("MAX_ARP_Tries");
    //    uint32_t arpTimeout = appIS_.getuint32("ARP_Timeout_Ms");
    //    uint32_t arpok(0);
    //    uint32_t tries(0);

    //    for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    //    {
    //        arpok = 0;
    //        tries = 0;
    //        index = streams_it - streams_.begin();
    //        stream_offset = (index)*0x4000;
    //        if (*streams_it)
    //        {
    //            while ((!arpok) && (tries < maxTries))
    //            {

    //                tries++;
    //                amc13Device_P->setBit("eth_10gb_ARP_req", HAL::HAL_NO_VERIFY, stream_offset);
    //                try
    //                {
    //                    amc13Device_P->pollItem("eth_10gb_MAC_rcv_OK", 1, arpTimeout,
    //                                              &pollres, HAL::HAL_POLL_UNTIL_EQUAL, stream_offset);
    //                    arpok = 1;
    //                    DEBUG("ARP reply is ok");
    //                    //amc13IS_.setuint32( "TCP_ARP_REPLY_OK", 1 ); //JRF TODO require one for each stream so this must move to the streamInfoSpace.
    //                    arpok = 1;
    //                }
    //                catch (HAL::TimeoutException &e)
    //                {
    //                    std::stringstream err;
    //                    err << "Did not get a response to ARP request for trial " << tries;
    //                    WARN(err.str());
    //                }
    //            }

    //            if (!arpok)
    //            { //JRF TODO fix this error message to loop over the streams.
    //                std::stringstream msg;
    //                msg << "Could not perform an arp request. Tried " << maxTries << " times. Destination is "
    //                    << appIS_.getstring("DestinationIP");
    //                //JRF since Vitess MDIO is not needed any more, we could replace this code with some code that checks the configuration of the equivalent part of the amc13 stream.
    //                //JRF removing this not needed // mdioInterface_P->mdioRead( 0, 0x8000, &tmp, 1); //JRF TODO what is this vitess stuff do we have 4 of these now?
    //                ERROR(msg);
    //                this->hwunlock();
    //                XCEPT_RAISE(utils::exception::AMC13Exception, msg.str());
    //            }

    //            // We do this here because it is fixed throughout a run. no need to monitor this more than once.
    //            // Now read back the Mac addresses of the destination for monitoring purposes.
    //            uint64_t mac_dest;
    //            //JRF TODO, rename these registers ... maybe. Note also. The destination is per stream so we need to store this info in the ApplicationInfospace so that the TCPStreamInfoSpace can grab
    //            //the values from the application infoSpace when monitoring them.
    //            //amc13Device_P->read( "MAC_DEST_LO", &mac_dest_lo);
    //            //amc13Device_P->read( "MAC_DEST_HI", &mac_dest_hi);
    //            amc13Device_P->read64("eth_10gb_Dest_MAC_address", &mac_dest, static_cast<uint64_t>(stream_offset));
    //            //JRF TODO add a new setuint64 method that just takes a 64 bit number
    //            //JRF TODO move this item to HardwareStatus...
    //            amc13IS_.setuint64("DestinationMACAddress", mac_dest & 0xFFFFFFFF, (mac_dest >> 32) & 0xFFFFFFFF, true);
    //        }
    //    } // end for loop

    //    // Open the TCP connections
    //    for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    //    {
    //        index = streams_it - streams_.begin();
    //        if (*streams_it)
    //        {
    //            openConnection(index);
    //            DEBUG("stream Open"); //JRF TODO add stream number to debug
    //        }
    //    }

    //    //JRF Now we can read back certain montorables which we want to monitor only once after configure._base
    //    for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    //    {
    //        index = streams_it - streams_.begin();
    //        stream_offset = (index)*0x4000;
    //        if (*streams_it)
    //        {
    //            //This loop is to fill those monitor once items on the streams if they exist
    //        }
    //    }
    //    uint32_t value;
    //    amc13Device_P->read("eth_10gb_qsfp_setup", &value);
    //    statusIS_.setbool("QSPF_present", ((value & 0x1) == 0) ? 0 : 1, true);               //QSPF_present
    //    statusIS_.setbool("QSFP_int", ((value & 0x2) == 0) ? 0 : 1, true);                   //QSFP_int
    //    statusIS_.setbool("QSFP_low_power_r_bit", ((value & 0x4) == 0) ? 0 : 1, true);       //QSFP_low_power_r_bit
    //    statusIS_.setbool("DDR3_0_Ready_bit", ((value & 0x8) == 0) ? 0 : 1, true);           //DDR3_0_Ready_bit
    //    //statusIS_.setbool("DDR3_0_Calibration_ok_bit", ((value & 0x10) == 0) ? 0 : 1, true); //DDR3_0_Calibration_ok_bit

    //    // JRF TODO check how this works and reinstate:
    //    // amc13Device_P->write( "ENA_PAUSE_FRAME", appIS_.getbool( "ENA_PAUSE_FRAME" ) );
    //}
    //catch (HAL::HardwareAccessException &e)
    //{
    //    ERROR("A hardware exception occured : " << e.what());
    //    //mdioInterface_P->mdiounlock();
    //    this->hwunlock();
    //    XCEPT_RETHROW(utils::exception::AMC13Exception, "A hardware exception occured.", e);
    //}

    //this->hwunlock();
}

void amc13controller::AMC13::stop() throw(utils::exception::AMC13Exception)
{
	// There is not much to do here. We assume the previous run stopped perfectly and the
	// amc13 is still up and ready to run. No connections will be closed down. No reset
	// will be done.
	this->hwlock();
	this->suspendEvents();
	this->hwunlock();
}

///////////////////////////////////////////////////////////////////////////
//
// setup event generator if events need to be generated in AMC13
//
// Logic:
//    o If we are in AMC13 Data Geenerator mode setup the event generator
//    o If data comes via the optical slink
//      set DAQ-ON on the sender side.
//       - if the data source is the core generator set up that generator.
//
//  The code below is written for clarity and not for optimal compactness.
//
//////////////////////////////////////////////////////////////////////////

void amc13controller::AMC13::enable() throw(utils::exception::AMC13Exception)
{
	if (!hwCreated_)
		return;

	// Some monitoring counters are reset.
	this->hwlock();
	try
	{
		//JRF TODO loop over all streams....
		//JRF TODO reset counters and links


		streamBpCounterHigh_[0] = 0; //JRF TODO implement counters for all 4 streams.
		streamBpCounterHigh_[1] = 0;
		streamBpCounterOld_[0] = 0;
		streamBpCounterOld_[1] = 0;


		// reset sequence number and reset fifo in sender core; needs to be done before using the core.
	}
	catch (HAL::HardwareAccessException &e)
	{
		ERROR("A hardware exception occured when resetting counters : " << e.what());
		this->hwunlock();
		XCEPT_RETHROW(utils::exception::AMC13Exception, "A hardware exception occured.", e);
	}

	//JRF doens't exist any more
	//dataTracker_.reset();
	//JRF reset the used streamDataTrackers in loop
	//uint32_t index(0);
	for (std::vector<bool>::iterator streams_it = inputStreams_.begin(); streams_it != inputStreams_.end(); ++streams_it)
	{
		if (*streams_it)
		{
			//index = streams_it - inputStreams_.begin();
			//tcpStreamDataTrackers_[index].reset(); // register the Data Tracker for each stream, passing in the stream id
			//inputStreamDataTrackers_[index].reset();
		}
	}

	//dataTracker2_.reset();

	////////////////////////////////////////// AMC13_MODE //////////////////////////////////////////////

	try
	{
		if (operationMode_ == AMC13_MODE_NORMAL)
		{

			for (std::vector<bool>::iterator streams_it = inputStreams_.begin(); streams_it != inputStreams_.end(); ++streams_it)
			{
				//index = streams_it - inputStreams_.begin();
				if (*streams_it)
				{
					//if (dataSource_[index] == GENERATOR_SOURCE) //JRF TODO fix this, should do for all streams.
					//{

					//JRF Note, this was also not used in the ferol
					//this->setupEventGen();
					//DEBUG("setting emulator mode in amc13");
					//AMC13EventGenerator fevgen(amc13Device_P, logger_);
					/*fevgen.setupEventGenerator(dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_bytes"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_Max_bytes"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_Stdev_bytes"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Delay_ns"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Delay_Stdev_ns"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("N_Descriptors"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Seed"))->value_,
					  index,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("expectedFedId"))->value_);*/

					//JRF TODO, once we have an external trigger mode, we must test this.
					/*std::string triggerMode = appIS_.getstring("AMC13TriggerMode");
					  if (triggerMode == AMC13_EXTERNAL_TRIGGER_MODE)
					  {
					  amc13Device_P->write("thresold_busy", 0x180, HAL::HAL_DO_VERIFY, stream_offset);
					  amc13Device_P->write("thresold_ready", 0x100, HAL::HAL_DO_VERIFY, stream_offset);
					//JRF TODO, Add this when it's implemented in the firmware
					//amc13Device_P->setBit("GEN_TRIGGER_EXT", HAL::HAL_DO_VERIFY, stream_offset);
					}
					else if (triggerMode == AMC13_AUTO_TRIGGER_MODE)
					{
					//JRF TODO set whatever we have to set for AUTO Triggering.
					//amc13Device_P->resetBit("GEN_TRIGGER_EXT", HAL::HAL_DO_VERIFY, stream_offset);
					}
					else
					{
					WARN("Illegal AMC13 Trigger mode encounteredL " << triggerMode << ". Using AUTO trigger mode.");
					}*/
					//JRF for now we don't use the loop, we only use the memory.
					//JRF TODO configure the generator here.

					//  ::usleep(1000);
					//}

					//else if (dataSource_[index] == L10G_CORE_GENERATOR_SOURCE) //JRF TODO, we must check the dataSource on each stream and configure accordingly, Don't forget to implement the monitoring correctly when different streams use different sources.!
					//{
					//JRF TODO loop over all streams, remmeber to pass in the stream number for each stream... Of course we have to fix the Slink core and generator classes to work for the amc13.
					//  try
					//{
					// setting DAQ-OFF in the slink express sender core (Should be already off) and enable internal event generator.
					// JRF TODO check if this is needed, probably not
					//  daqOff(index, true);

					//SlinkExpressEventGenerator evgeni(amc13Device_P, slCore_P, logger_); // Paying homage to a great expert of CMS !!! (even though a letter is missing...)
					/*evgeni.setupEventGenerator(dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_bytes"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_Max_bytes"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_Stdev_bytes"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Delay_ns"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Delay_Stdev_ns"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("N_Descriptors"))->value_,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Seed"))->value_,
					  index,
					  dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("expectedFedId"))->value_);
					  */
					// JRF TODO Start the generator
					// }
					// catch (HAL::HardwareAccessException &e)
					//{
					//    ERROR("Failed to Configure L10G_CORE_GENERATOR_SOURCE, ");
					//    XCEPT_RETHROW(utils::exception::AMC13Exception,
					//            "Hardware access failed.", e);
					//}
					// }
					// else if (dataSource_[index] == L10G_SOURCE)
					//{
					//    // setting DAQ-ON in the slink express sender core
					//    DEBUG("switching on DAQ: 10G daqon");
					//    daqOn(index); // JRF TODO... again we need to do this on all enabled streams...
					//}
				}
			}
			//JRF call resumeEvents(), this method only resumes events for Generated modes
			this->resumeEvents();
		}
	}
	catch (HAL::HardwareAccessException &e)
	{
		FATAL("Hardware Access failed. " << e.what());
		this->hwunlock();
		XCEPT_RETHROW(utils::exception::AMC13Exception,
				"Hardware access failed.", e);
	}

	this->hwunlock();
}

/////////////////////////////////////////////////////
// reset tcp/ip conncetion
// call routine to destroy hardare devices
// reset the data trackers
/////////////////////////////////////////////////////
void amc13controller::AMC13::halt() throw(utils::exception::AMC13Exception)
{
	if (!hwCreated_)
		return;
	//JRF TODO rewrite this for AMC13
	//JRF TODO add all 4 streams and remove handling of 6G links.
	this->hwlock();
	this->suspendEvents();
	try
	{
		//uint32_t index(0);
		for (std::vector<bool>::iterator streams_it = inputStreams_.begin(); streams_it != inputStreams_.end(); ++streams_it)
		{
			if (*streams_it) //JRF if the stream is enabled then we do the business
			{
				//index = streams_it - inputStreams_.begin();
			}
		}
	}
	catch (HAL::HardwareAccessException &e)
	{
		FATAL("Hardware Access failed.");
		this->hwunlock();
		XCEPT_RETHROW(utils::exception::AMC13Exception,
				"Hardware access failed.", e);
	}

    //clear node maps. Must re-configure to regain hardware access.
    uiMap_.clear();
    uiDups_.clear();
    loWords_.clear();
    hiWords_.clear();

	// would be better to only update the amc13 infospace

	// the two functions below also take the hw lock, but they are in the same thread.
	// monitor_.updateAllInfospaces();
	shutdownHwDevice();
	this->hwunlock();
}

void amc13controller::AMC13::suspendEvents() throw(utils::exception::AMC13Exception)
{
	if (!hwCreated_)
		return;
	//JRF TODO, fix this... 
	if (operationMode_ != AMC13_MODE_NORMAL )
		return; //JRF TODO, maybe remove this if we only have one operation mode.
	this->hwlock();
	uint32_t index(0);

	try
	{
		for (std::vector<bool>::iterator streams_it = inputStreams_.begin(); streams_it != inputStreams_.end(); ++streams_it)
		{
			if (*streams_it) //JRF if the stream is enabled then we do the business
			{
				index = streams_it - inputStreams_.begin();

			} //end if
		}     // end for loop
	}
	/*    catch (HAL::HardwareAccessException &e)
	      {
	      FATAL("Hardware Access failed.");
	      this->hwunlock();
	      XCEPT_RETHROW(utils::exception::AMC13Exception,
	      "Hardware access failed.", e);
	      }*/
	catch (utils::exception::AMC13Exception &e)
	{
		WARN("Communication problem during suspendEvents() with slink no " << index << "  : " << e.what());
	}

	this->hwunlock();
}

void amc13controller::AMC13::resumeEvents() throw(utils::exception::AMC13Exception)
{
	if (!hwCreated_)
		return;
	if (operationMode_ != AMC13_MODE_NORMAL)
		return; //JRF TODO, can maybe remove this if we only have one mode.

	this->hwlock();

	try
	{
		//uint32_t index(0);
		for (std::vector<bool>::iterator streams_it = inputStreams_.begin(); streams_it != inputStreams_.end(); ++streams_it)
		{
			if (*streams_it) //JRF if the stream is enabled then we do the business
			{
				//       index = streams_it - inputStreams_.begin();

			}
		}
	}
	catch (HAL::HardwareAccessException &e)
	{
		FATAL("Hardware Access failed.");
		this->hwunlock();
		XCEPT_RETHROW(utils::exception::AMC13Exception,
				"Hardware access failed.", e);
	}
	this->hwunlock();
}

void amc13controller::AMC13::hwlock()
{
	deviceLock_.take();
}

void amc13controller::AMC13::hwunlock()
{
	deviceLock_.give();
}

/////////////////////////////////// update monitoring info ////////////////////////////

bool amc13controller::AMC13::updateInfoSpace(utils::InfoSpaceHandler *is, uint32_t streamNo)
{
	this->hwlock();

	if (!hwCreated_ && ( is->name() != "StreamConfig") )//JRF If hwCreated is false, we are not using the hardware, so we should not monitor anything except the enable on the StreamConfig InfoSpace
	{
		this->hwunlock();
		return false; //don't push when we're not monitoring
	}
	std::list<utils::InfoSpaceHandler::ISItem> items = is->getItems();
	std::list<utils::InfoSpaceHandler::ISItem>::const_iterator it;

	//HGC TODO does the following if statement, and code within, duplicate that in
	//PROCESS section below?
	//JRF we get here if we are monitoring one of the stream info spaces.
	if (!hwCreated_) //JRF note that !hwCreated_ implies that all streams are disabled. 
	{
		for (it = items.begin(); it != items.end(); it++)
		{
			//JRF First we check all streams are disabled. If they are, we only update the enable flag and then return
			if ((*it).gethwname() == "enable")
			{
				DEBUG(it->name);
				is->setbool((*it).name, dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[streamNo].getField("enable"))->value_);
				break; //JRF we have set the only thing we need to set in this case.
			}
			continue; // skip this item go to the next, we are looking for "enable" only while the stream is disable.
		}
		this->hwunlock();
		is->writeInfoSpace();
		return true; // we have to push in this case for StreamConfig
	}

	bool streamEnabled = true;
	if ( is->name() == "InputStream" || is->name() == "OutputStream" )
	{
		streamEnabled = (is->name() == "InputStream") ? inputStreams_.at(streamNo)
			: outputStreams_.at(streamNo);
	}

	//JRF we only get here if we have the hardware lock.

	//uint32_t stream_offset = streamNo * 0x4000; //JRF TODO replace 0x4000 by a constant.
	// Later there are checks which should be done only in specific states. Since the state changes
	// happen in a different thread there is a potential race condition which results in hardware register
	// values which were sampled at the previous state. Therefore we first sample the state here and once
	// more at the end of the retrieval  of hardware values and apply the checks only if there is not
	// state change in between.
	//std::string state1 = statusIS_.getstring("stateName");

	//JRF TODO, we should do this for each stream...
	try
	{
		//JRF TODO, add the SFP items to InputStreamInfoSpace...
		//try
		//{
		//    //JRF TODO, put this back in when ready. readSFP(streamNo);
		//}
		//catch (utils::exception::AMC13Exception &e)
		//{
		//    // the run should continue... we only read mdio for monitoring purposes
		//    WARN(e.what());
		//}

		for (it = items.begin(); it != items.end(); it++)
		{
			/////////////////// items read from hardware registers ///////////////////
			//Instead of handling HW32 and HW64 values separately, like in FEROL40,
			//just assign all hardware values a default 'UPDATE'. The readRegister
			//method should handle the different data sizes appropriately.
			if ( it->update >= utils::InfoSpaceHandler::UPDATE )
				//here >= means it includes the ALWAYSUPDATE enum as well.
			{
				if ( it->setNames.find(streamNo) == it->setNames.end() )
				{
					ERROR("Missing set for stream " << streamNo 
							<< " with item " << it->name);
					continue;
				}
				std::string setName = it->setNames.find(streamNo)->second;
				uint32_t setLevel = monitor_.getItemSetLevel(setName);

				if ( it->update != utils::InfoSpaceHandler::ALWAYSUPDATE )
				{
					if ( (it->level > setLevel) || (!streamEnabled) ) continue;
				} //else DEBUG("ALWAYSUPDATE: " << it->name);

				//if ( it->name == "LINK_OK" && streamNo == 1 )
				//    DEBUG("Link ok passed for disabled stream. StreamEnabled "
				//            << streamEnabled );

				uint64_t value;
				value = this->readRegister(it->name, streamNo);
				if ( it->type == utils::InfoSpaceHandler::UINT32 )
					is->setuint32(it->name, value);
				else if ( it->type == utils::InfoSpaceHandler::UINT64 )
					is->setuint64(it->name, value);
				else ERROR("Item of type UPDATE is neither UINT32 or UINT64: "
						<< it->name);
			}

			else if ( it->update == utils::InfoSpaceHandler::PROCESS )
			{
				if ( it->name == "Enabled" )
				{
					bool isEnabled;
					std::string vecName;
					if ( is->name() == "InputStream" ) 
					{
						vecName = "InputPorts";
						isEnabled = dynamic_cast<xdata::Boolean *>
							(appIS_.getvector(vecName).at(streamNo).getField("enable"))
							->value_;
					}
					else if (is->name() == "OutputStream" )
					{
						vecName = "OutputPorts";
						isEnabled = dynamic_cast<xdata::Boolean *>
							(appIS_.getvectoroutputs(vecName).at(streamNo).
							 getField("enable"))
							->value_;
					}
					else
					{
						DEBUG("Enabled property found not in input or output streams");
						continue;
					}
					is->setbool( "Enabled", isEnabled );
				}
			}

		}
	}
	catch (HAL::HardwareAccessException &e)
	{
		ERROR(e.what());
	}
	catch (utils::exception::AMC13Exception &e)
	{
		ERROR(e.what());
	}

	this->hwunlock();

	is->writeInfoSpace();

	// At this point we can do some post processing. Spontaneous state changes
	// which are indications of failures should be detetected here:

	// If we are in the state configured, enabled or paused we should have the
	// connections up for enabled streams
	//std::string state2 = statusIS_.getstring("stateName");

	// For the time being the following checks should only be executed on the old AMC13_IS

	// When the migration of the monitoring is finished this AMC13_IS dissapears and the
	// checks will be executed on the new Input Info space handler.
	// JRF TODO, fix this and/or remove it.
	/*if (is->name() == "AMC13_IS")
	  {

	  if ((state1 == state2) && (state1 == "Configured" || state1 == "Enabled" || state1 == "Paused"))
	  {

	  if (appIS_.getvector("InputPorts")[0].getField("enable") && (is->getuint32("TCP_CONNECTION_ESTABLISHED_FED0") == 0))
	  {
	  std::stringstream msg;
	  msg << "Lost TCP/IP connection in stream 0." << tcpErrorStream0.str();
	  ERROR(msg.str());
	  XCEPT_RAISE(utils::exception::AMC13Exception, msg.str());
	  }
	  if (appIS_.getvector("InputPorts")[1].getField("enable") && (is->getuint32("TCP_CONNECTION_ESTABLISHED_FED1") == 0))
	  {
	  std::stringstream msg;
	  msg << "Lost TCP/IP connection in stream 1." << tcpErrorStream1.str();
	  ;
	  ERROR(msg.str());
	  XCEPT_RAISE(utils::exception::AMC13Exception, msg.str());
	  }
	  if ((appIS_.getvector("InputPorts")[0].getField("enable") || appIS_.getvector("InputPorts")[1].getField("enable")) &&
	  is->getuint32("SERDES_STATUS") == 0)
	  {
	  std::stringstream msg;
	  msg << "Lost SERDES. 10Gb SERDES to DAQ not up any more.";
	  ERROR(msg.str());
	  XCEPT_RAISE(utils::exception::AMC13Exception, msg.str());
	  }

	// check if there has appeared a conflict on the network. However, do not send a ARP_PROBE!
	// this is protected with a hwlock internally
	this->doArpProbe(0, false); //JRF todo, we must add a loop over streams here... not even sure what this does yet...
	}
	}*/
	return true;
}

////////////////////////////// utilities ////////////////////////////

	std::string
amc13controller::AMC13::makeIPString(uint32_t ip) throw(utils::exception::AMC13Exception)
{
	std::string res = "";
	char buf[INET_ADDRSTRLEN];
	//ip = ((ip & 0xff000000) >> 24) + ((ip & 0xff0000) >> 8) + ((ip & 0xff00) << 8) + ((ip & 0xff) << 24);
	const char *tmp = inet_ntop(AF_INET, &ip, buf, INET_ADDRSTRLEN);
	if (tmp == NULL)
	{
		std::string msg = "Could not convert IP to string : " + ip;
		ERROR(msg);
		XCEPT_RAISE(utils::exception::AMC13Exception, msg);
	}
	else
	{
		res = std::string(tmp);
	}
	return res;
}

	uint32_t
amc13controller::AMC13::makeIPNum(std::string hoststr) throw(utils::exception::AMC13Exception)
{
	uint32_t dec;
	std::string ipstr;

	// Check if ipstr is an IP or a hostname
	std::string regex = "([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})";
	if (!toolbox::regx_match(hoststr, regex))
	{
		// we have a hostname
		struct hostent res;
		struct hostent *resptr;
		int myerrno;
		char buf[1024];
		// This should be re-tried if it does not work!!!
		int ret = gethostbyname_r(hoststr.c_str(), &res, buf, 1024, &resptr, &myerrno);
		if (ret || !resptr)
		{
			// We have a problem
			char msg[1024];
			char *err;
			if (myerrno == -1)
				err = strerror_r(errno, msg, 1024);
			else
				err = (char *)hstrerror(myerrno);
			FATAL(std::string(err));
			XCEPT_RAISE(utils::exception::AMC13Exception, std::string(err));
		}
		char *addr = res.h_addr_list[0];
		ipstr = makeIPString(*((uint32_t *)addr));
		DEBUG("Found address " << ipstr << " from hoststring " << hoststr);
	}
	else
	{
		ipstr = hoststr;
	}

	int res = inet_pton(AF_INET, ipstr.c_str(), &dec);
	if (res != 1)
	{
		std::string msg = "Could not convert IP string to number : " + ipstr;
		ERROR(msg);
		XCEPT_RAISE(utils::exception::AMC13Exception, msg);
	}
	else
	{
		dec = ((dec & 0xff000000) >> 24) + ((dec & 0xff0000) >> 8) + ((dec & 0xff00) << 8) + ((dec & 0xff) << 24);
	}

	return dec;
}

	std::string
amc13controller::AMC13::uint64ToMac(uint64_t mac)
{
	std::stringstream result;
	uint32_t byte1 = mac & 0xffll;
	uint32_t byte2 = (mac & 0xff00ll) >> 8;
	uint32_t byte3 = (mac & 0xff0000ll) >> 16;
	uint32_t byte4 = (mac & 0xff000000ll) >> 24;
	uint32_t byte5 = (mac & 0xff00000000ll) >> 32;
	uint32_t byte6 = (mac & 0xff0000000000ll) >> 40;

	result << std::setw(2) << std::setfill('0') << std::hex << byte6 << ":" << std::setw(2) << byte5 << ":" << std::setw(2) << byte4 << ":" << std::setw(2) << byte3 << ":" << std::setw(2) << byte2 << ":" << std::setw(2) << byte1;

	return result.str();
}

void amc13controller::AMC13::doArpProbe(uint32_t stream_no, bool doProbe)
{
	//JRF TODO Remove this
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////// a temporary hack to play with the Serdes ///////////////////

void amc13controller::AMC13::controlSerdes(bool on) throw(utils::exception::Exception)
{

	//JRF TODO, check all this, what it's for do we need it... remove...
	//
	//////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////
	// this hack is needed to toggle link up and down without having configured the amc13.   //
	// The hardware device needs to be created to play with this bit. We will use this in    //
	// future again since there are problems in the switch at point 5.                       //
	bool hwhack = false;
	//////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////

	if (!hwCreated_)
	{
		ERROR("Cannot play with the serdes/laser since the HardwareDevice has not been created yet!");

		// return;

		//////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////

		hwhack = true;

		this->createAMC13Device();
	}
	//////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////

	try
	{

		if (on)
		{
			this->hwlock();
			uint32_t pollres;
			try
			{
				//amc13Device_P->pollItem("SERDES_STATUS", 1, 5000, &pollres,
				//                          HAL::HAL_POLL_UNTIL_EQUAL);
				this->hwunlock();
				DEBUG("SERDES should be up now.");
			}
			catch (HAL::TimeoutException &e)
			{
				this->hwunlock();
				std::stringstream err;
				err << "Could not initialize AMC13 SerDes. (poll result: " << pollres << " )";
				ERROR(err.str());
				XCEPT_RETHROW(utils::exception::AMC13Exception, err.str(), e);
			}
		}
		//            else
		//                {
		//                    this->hwlock();
		//                    amc13Device_P->write( "SERDES_INIT_AMC13", 0x100 );
		//                    this->hwunlock();
		//                    DEBUG( "SERDES should be down now");
		//                }
	}
	catch (HAL::HardwareAccessException &e)
	{
		this->hwunlock();
		XCEPT_RETHROW(utils::exception::AMC13Exception,
				"Problem with hardware access while playing with 10G SERDES.", e);
	}

	//////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////
	if (hwhack)
	{
		//JRF TODO do we need this?
	}
	//////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////
}

void amc13controller::AMC13::readSFP(uint32_t portNo) throw(utils::exception::AMC13Exception)
{
	//JRF TODO rewrite this to access the sfp through the new address table, rather than through the vitess and MDIO.
	if (false) //amc13Device_P == NULL)
	{
		std::stringstream err;
		err << "Illegal operation: You must configure the AMC13Controller before accessing the hardware!";
		XCEPT_DECLARE(utils::exception::AMC13Exception, top, err.str());
		throw(top);
	}

	uint32_t length = 128;
	char sdata[length];
	u_char *data;
	// remove the resets
	//uint32_t oldReset(0);

	std::stringstream portNoStr;
	portNoStr << portNo;

	//DEBUG( "release the reset of both vitesse chips (VT0/1_RESET_N to '1')" );
	//JRF TODO fix the following line
	//amc13Device_P->read("SERDES_SETUP_AMC13", &oldReset );
	//std::cout << " old reset " <<  std::hex << oldReset << std::endl;
	/*if ((portNo == 0) && ((oldReset & 0x80) == 0))
	  {
	  return;
	  }
	  else if ((portNo == 1) && ((oldReset & 0x80000000) == 0))
	  {
	  return;
	  }
	  else if (portNo > 1)
	  {
	  return;
	  }*/

	// first thing to do is to check if there is a SFP plugged into the cage. This can be
	// done to read the state of the MSTCODE[1] pin which is mapped to bit 14 in register 0xE607 of device 1 (1xE607)
	//
	uint32_t value(0);
	//JRF TODO repalce mdio stuff with direct access...
	//mdioInterface_P->mdioRead( portNo, 0xE607, &value, 1 );
	bool sfpPresent = !(value & 0x4000);

	if (!sfpPresent)
	{
		inputIS_.setstring(std::string("SFP_VENDOR"), "n.a.");
		inputIS_.setstring(std::string("SFP_PARTNO"), "n.a.");
		inputIS_.setstring(std::string("SFP_DATE"), "n.a.");
		inputIS_.setstring(std::string("SFP_SN"), "n.a.");
		inputIS_.setdouble(std::string("SFP_VCC"), 0.0);
		inputIS_.setdouble(std::string("SFP_TEMP"), 0.0);
		inputIS_.setdouble(std::string("SFP_BIAS"), 0.0);
		inputIS_.setdouble(std::string("SFP_TXPWR"), 0.0);
		inputIS_.setdouble(std::string("SFP_RXPWR"), 0.0);
		return;
	}

	// now we reset the two wire interface
	//mdioInterface_P->mdioWrite( portNo, 0xEF01, 0x8000, 1);

	// setup the reading of the conventional SFP memory.
	// This is found at address A0. It needs to be shifted >>1 for the
	// Vitesse.
	//mdioInterface_P->mdioWrite( portNo, 0x8002, 0x0050 );
	//mdioInterface_P->mdioWrite( portNo, 0x8004, 0x1000 );
	try
	{
		//          INFO("first i2c");
		i2cCmd(portNo, 0x0002);
	}
	catch (utils::exception::AMC13Exception &e)
	{
		return;
		//XCEPT_RETHROW( utils::exception::AMC13Exception, "", e);
	}

	//mdioInterface_P->mdioReadBlock( portNo, 0x8007, sdata, length );

	std::string vendor(&(sdata[20]), 16);
	std::string partNumber(&(sdata[40]), 20);
	std::string serialNo(&(sdata[68]), 16);
	std::string date(&(sdata[84]), 8);

	// Infospace Items are not pushed through since this function is called in the
	// updateInfospace method, which in the end dows a global push through of all
	// infospace variables.
	inputIS_.setstring(std::string("SFP_VENDOR"), vendor);
	inputIS_.setstring(std::string("SFP_PARTNO"), partNumber);
	inputIS_.setstring(std::string("SFP_DATE"), date);
	inputIS_.setstring(std::string("SFP_SN"), serialNo);

	// Now do the readout of the Enhanced Feature set memory (Address 0xA2).
	// The Vitesse again wants this address to be shifted >> 1 so that it becaomes 0x51.
	//mdioInterface_P->mdioWrite( portNo, 0x8002, 0x0051 );
	//mdioInterface_P->mdioWrite( portNo, 0x8004, 0x1000 );
	//    INFO("Second i2c");
	i2cCmd(portNo, 0x0002);

	//mdioInterface_P->mdioReadBlock( portNo, 0x8007, sdata, length );

	data = (u_char *)sdata;

	double temp = getTemp(&data[96]);
	double Vcc = getVoltage(&data[98]);
	double txBias = getCurrent(&data[100]);
	double txPower = getPower(&data[102]);
	double rxPower = getPower(&data[104]);

	inputIS_.setdouble(std::string("SFP_VCC"), Vcc);
	inputIS_.setdouble(std::string("SFP_TEMP"), temp);
	inputIS_.setdouble(std::string("SFP_BIAS"), txBias);
	inputIS_.setdouble(std::string("SFP_TXPWR"), txPower);
	inputIS_.setdouble(std::string("SFP_RXPWR"), rxPower);

	inputIS_.setdouble(std::string("SFP_VCC_L_ALARM"), getVoltage(&data[10]));
	inputIS_.setdouble(std::string("SFP_VCC_H_ALARM"), getVoltage(&data[8]));
	inputIS_.setdouble(std::string("SFP_VCC_L_WARN"), getVoltage(&data[14]));
	inputIS_.setdouble(std::string("SFP_VCC_H_WARN"), getVoltage(&data[12]));

	inputIS_.setdouble(std::string("SFP_TEMP_L_ALARM"), getTemp(&data[2]));
	inputIS_.setdouble(std::string("SFP_TEMP_H_ALARM"), getTemp(&data[0]));
	inputIS_.setdouble(std::string("SFP_TEMP_L_WARN"), getTemp(&data[6]));
	inputIS_.setdouble(std::string("SFP_TEMP_H_WARN"), getTemp(&data[4]));

	inputIS_.setdouble(std::string("SFP_BIAS_L_ALARM"), getCurrent(&data[18]));
	inputIS_.setdouble(std::string("SFP_BIAS_H_ALARM"), getCurrent(&data[16]));
	inputIS_.setdouble(std::string("SFP_BIAS_L_WARN"), getCurrent(&data[22]));
	inputIS_.setdouble(std::string("SFP_BIAS_H_WARN"), getCurrent(&data[20]));

	inputIS_.setdouble(std::string("SFP_TXPWR_L_ALARM"), getPower(&data[26]));
	inputIS_.setdouble(std::string("SFP_TXPWR_H_ALARM"), getPower(&data[24]));
	inputIS_.setdouble(std::string("SFP_TXPWR_L_WARN"), getPower(&data[30]));
	inputIS_.setdouble(std::string("SFP_TXPWR_H_WARN"), getPower(&data[28]));

	inputIS_.setdouble(std::string("SFP_RXPWR_L_ALARM"), getPower(&data[34]));
	inputIS_.setdouble(std::string("SFP_RXPWR_H_ALARM"), getPower(&data[32]));
	inputIS_.setdouble(std::string("SFP_RXPWR_L_WARN"), getPower(&data[38]));
	inputIS_.setdouble(std::string("SFP_RXPWR_H_WARN"), getPower(&data[36]));
}

double amc13controller::AMC13::getTemp(u_char *data)
{
	return ((double)((int16_t)(256 * (uint32_t)data[0] + (uint32_t)data[1]))) / 256.0;
}

double amc13controller::AMC13::getVoltage(u_char *data)
{
	return ((double)(256 * (uint32_t)data[0] + (uint32_t)data[1])) / 10000.0;
}

double amc13controller::AMC13::getCurrent(u_char *data)
{
	return (256 * (uint32_t)data[0] + (uint32_t)data[1]) * 2 / 1000.;
}

double amc13controller::AMC13::getPower(u_char *data)
{
	return ((double)(256 * (uint32_t)data[0] + (uint32_t)data[1])) / 10.0;
}

void amc13controller::AMC13::i2cCmd(uint32_t portNo, uint32_t value) throw(utils::exception::AMC13Exception)
{
	std::string i2cState = checkI2CState(portNo);
	//    INFO("before i2c state is " << i2cState);
	if ((i2cState != "idle") && (i2cState != "success"))
	{
		std::stringstream msg;
		msg << "Before write operation I2C bus in state " << i2cState << " for Vitesse No. " << portNo;
		ERROR(msg.str());
		XCEPT_DECLARE(utils::exception::AMC13Exception, top, msg.str());
		throw(top);
	}
	//mdioInterface_P->mdioWrite( portNo, 0x8000, value );

	i2cState = checkI2CState(portNo);
	//    INFO("after checking i2c state is " << i2cState);

	if ((i2cState != "idle") && (i2cState != "success"))
	{
		std::stringstream msg;
		msg << "After write operation I2C bus in state " << i2cState << " for Vitesse No. " << portNo;
		INFO(msg.str());
		XCEPT_DECLARE(utils::exception::AMC13Exception, top, msg.str());
		throw(top);
	}
}

std::string amc13controller::AMC13::checkI2CState(uint32_t portNo) throw(utils::exception::AMC13Exception)
{
	std::string state;
	uint32_t tmp(0);
	uint32_t iloop = 0;
	do
	{

		//mdioInterface_P->mdioRead( portNo, 0x8000, &tmp );

		if ((tmp & 0x0C) == 0x00)
		{
			state = "idle";
		}
		else if ((tmp & 0x0C) == 0x04)
		{
			state = "success";
		}
		else if ((tmp & 0x0C) == 0x08)
		{
			state = "inProgress";
		}
		else
		{
			state = "failed";
		}

		iloop++;
		::usleep(1000);

	} while ((state == "inProgress") && (iloop <= 1000));

	return state;
}

	int32_t
amc13controller::AMC13::daqOff(uint32_t link, bool generatorOn)
{
	try
	{
		// setting DAQ-OFF in the slink express sender core (Should be already off)
		//slCore_P->write("SlX_DAQ_ON_OFF", data, link);
	}
	catch (utils::exception::AMC13Exception &e)
	{
		WARN("Communication problem during DAQ-OFF with slink no " << link << "  : " << e.what());
		return -1;
	}
	return 0;
}

	int32_t
amc13controller::AMC13::daqOn(uint32_t link)
{
	try
	{
		// setting DAQ-ON in the slink express sender core
		//slCore_P->write("SlX_DAQ_ON_OFF", data, link);
	}
	catch (utils::exception::AMC13Exception &e)
	{
		WARN("Communication problem during DAQ-ON with slink no " << link << "  : " << e.what());
		return -1;
	}
	return 0;
}

//JRF TODO Remove the following method.
	int32_t
amc13controller::AMC13::resyncSlinkExpress(uint32_t link)
{
	if (false) //amc13Device_P == NULL)
		return -1;
	return 0;
}

uint32_t amc13controller::AMC13::readRegister(amc13controller::AMC13::Board chip, const std::string& reg)
{
	uhal::ValWord<uint32_t> result;

	switch(chip)
	{
		case T1:
			result = deviceT1_P->getNode(reg).read();
			deviceT1_P->dispatch();
			break;
		case T2:
			result = deviceT2_P->getNode(reg).read();
			deviceT2_P->dispatch();
			break;
		default:
			XCEPT_RAISE(utils::exception::AMC13Exception, "- bad chip given to AMC13::readRegister.");
	}

	return result;
}

uint64_t amc13controller::AMC13::readRegister(amc13controller::AMC13::Board chip, const std::string& reg,
		int bitShift)
{
	if (bitShift < 0) XCEPT_RAISE(utils::exception::AMC13Exception,"Negative bit shift when reconstructing registers.");
	uint64_t result(0);
	uhal::ValWord<uint32_t> readValue;
	uhal::HwInterface* device;

	switch(chip)
	{
		case T1:
			device = deviceT1_P; 
			break;
		case T2:
			device = deviceT2_P;
			break;
		default:
			XCEPT_RAISE(utils::exception::AMC13Exception, "- bad chip given to AMC13::readRegister.");
	}

	readValue = device->getNode(reg+std::string("_LO")).read();
	device->dispatch();
	result += uint64_t(readValue.value());

	readValue = device->getNode(reg+std::string("_HI")).read();
	device->dispatch();
	result += uint64_t(readValue.value()) << bitShift;

	return result;
}

uint64_t amc13controller::AMC13::readRegister(std::string uiName, uint32_t stream)
{
	int bitShift;

	if (uiMap_.find(uiName) == uiMap_.end()) //if map not found
	{
		XCEPT_RAISE(utils::exception::AMC13Exception, "no register found "
				"corresponding to given value name " + uiName);
		return -1;
	}

	//determine if quantity needs stream:
	bool isStream( uiName.find('*') != std::string::npos || 
			uiDups_.find(uiName) != uiDups_.end() );


	std::string address = getAddress(*(uiMap_[uiName].second), &bitShift);
	if (uiName == "AMC_words__2")
		//this is a special case that we have to deal with by address rather than
		//register name because, for some entirely unknown reason, the register name
		//has a space in it, which breaks everything.
	{
		//bitshift is 32 for these registers (all masks are ffffffff)
		int bitShift = 32;        

		//pattern for each of the _LO registers is as followss:
		uint32_t address = 0x00000818 + 0x0080*stream;

		uint64_t result(0);
		uhal::ValWord<uint32_t> readValue;
		uhal::HwInterface* device;

		switch(uiMap_[uiName].first)
		{
			case T1:
				device = deviceT1_P;
				break;
			case T2:
				device = deviceT2_P;
				break;
			default:
				XCEPT_RAISE(utils::exception::AMC13Exception, 
						"- bad chip given to AMC13::readRegister.");
		}

		readValue = device->getClient().read(address);
		device->getClient().dispatch();
		result += uint64_t(readValue.value());

		readValue = device->getClient().read(address+1);
		device->getClient().dispatch();
		result += uint64_t(readValue.value()) << bitShift;

		return result;
	}

	if (isStream)
	{
		insertWildcards(&address);
		//DEBUG("Address: " << address << ", stream: " << stream);
		uint16_t firstStar = address.find('*'); 
		if (firstStar == std::string::npos) ERROR("in readRegister : expected stream-type quantity but no stream found. " << uiName << ", " << address);
		else
		{
			char indexedAddress[500];
			bool starAtEnd(false), twoStarsInARow(false);
			starAtEnd = ( firstStar == (address.size() - 1) );
			//Only check for a second star after the first if the first star is
			//not at the end (for safety):
			if (!starAtEnd) twoStarsInARow = ( address.at(firstStar+1) == '*' );

			if ( twoStarsInARow ) 
			{
				snprintf( indexedAddress, sizeof(indexedAddress), "%s%02u%s",
						address.substr(0,firstStar).c_str(), stream+1,
						address.substr(firstStar+2).c_str() ); 
			}
			else //there is only one star
			{
				snprintf( indexedAddress, sizeof(indexedAddress), "%s%u%s",
						address.substr(0,firstStar).c_str(), stream, 
						address.substr(firstStar+1).c_str() );
			}
			address = indexedAddress;
		}
	}

	if (bitShift >= 0)
		return readRegister(uiMap_[uiName].first, address, bitShift);
	else
		return readRegister(uiMap_[uiName].first, address);
}

	uint32_t
amc13controller::AMC13::getSlot()
{
	return slot_;
}

	void
amc13controller::AMC13::parseUiName(std::string *name, const uhal::Node *node,
		std::string stage)
{
	std::string uiName = *name;
	//Handles special exceptions to uiName from the standard pattern.
	//The stage parameter determines which checks should be made.
	if ( stage == "prewildcard" || stage == "all" )
	{
		if ( uiName.substr(0,uiName.size()-1) == "Enable_BGO" )
		{
			int bitShift;
			if ( getAddress(*node, &bitShift).substr(14) == "ENABLE_REPEAT" )
			{
				//std::cout << "------- MAP -------- : " << uiName << "\n";
				uiName = std::string("Enable_Repeat") + uiName.substr(6);
				//std::cout << "----------MAP RENAME ------------ : " << uiName << "\n";
			}
		}
		else if ( uiName.substr(0,uiName.size()-1) == "Words_SFP" )
		{
			int bitShift;
			if ( getAddress(*node, &bitShift).substr(16) == "WORD_COUNT" )
			{
				//std::cout << "------- MAP -------- : " << uiName << "\n";
				uiName = std::string("Word_Count") + uiName.substr(5);
				//std::cout << "----------MAP RENAME ------------ : " << uiName << "\n";
			}
		}
	}

	if ( stage == "postwildcard" || stage == "all" )
	{
		if ( uiName == "SrcID" )
		{
			std::string desc = node->getDescription();
			char sfpNo = desc.at(desc.find_first_of("012"));
			uiName = std::string("SrcID_SFP") + sfpNo;
		}
	}

	*name = uiName;
}


	boost::unordered_map< std::string, std::pair<const uhal::Node*,const uhal::Node*> >
amc13controller::AMC13::getDebugNodeMap()
{
	boost::unordered_map< std::string, const uhal::Node*> lowNodes;
	boost::unordered_map< std::string, const uhal::Node*> highNodes;
	//_LO nodes, and nodes which are neither HI nor LO will go in lowNodes,
	//_HI nodes will go in highNodes.

	//Loop over all nodes:
	for (int i = 0; i < 2; i++)
	{
		uhal::Node const &baseNode = ((i==0) ? deviceT1_P : deviceT2_P)->getNode();

		for (uhal::Node::const_iterator iNode = baseNode.begin();
				iNode != baseNode.end(); iNode++)
		{
			std::string addr = iNode->getPath();
			//------------------------------------------------------------------
			//Get UI name for current node, but with numbers appended if it
			//is a generically named stream quantity:
			boost::unordered_map<std::string,std::string>
				parameters = iNode->getParameters();
			//create UI name for given monitorable, as done in mapNames
			std::string parRow(parameters["Row"]), parCol(parameters["Column"]);
			if ( parRow.substr(0,1) == "_" ) parRow = "";
			else if ( parCol.substr(0,1) == "_" ) parCol = "";
			else parRow += "_"; //if both are non blank, add a delimeter.
			std::string uiName = parRow + parCol;
			parseUiName(&uiName, &(*iNode), "all");

			//If name is the same for multiple streams it will be in the uiDups map.
			//For these, append the relevant number to the name
			size_t streamSize = uiDups_.count(uiName);            
			if ( streamSize > 1 )
			{
				size_t firstNo = addr.find_first_of("012");
				size_t secondNo = addr.find_first_of( "0123456789", firstNo+1 );
				if ( streamSize == 3 )
				{
					if ( secondNo == (firstNo+1) )
					{
						if ( addr.substr(firstNo,2) != "13" )
							ERROR("Logic error in debug node map. Code 1");
						else
							uiName += addr.at( addr.find_first_of("012",secondNo+1) );
					}
					else
					{
						uiName += addr.at(firstNo);
					}
				}
				else if ( streamSize == 12 )
				{
					if ( secondNo == std::string::npos )
						ERROR("Logic error in debug node map. Code 2");
					else
					{
						if ( addr.substr(firstNo,2) == "13" )
						{
							uiName += addr.substr(
									addr.find_first_of("01",secondNo+1), 2 );
						}
						else uiName += addr.substr( firstNo, 2 );
					}
				}
			}
			//------------------------------------------------------------------
			//Determine if it is HI, LO, or neither, then add to the relevant map.
			//std::string addrEnd = addr.substring(addr.size()-3);
			( (addr.substr(addr.size()-3) == "_HI") ? highNodes : lowNodes )
				.insert( std::pair<std::string, const uhal::Node*>(uiName,&(*iNode)) );
			//------------------------------------------------------------------
		}
	}
	//Loop over the larger map (lowNodes), and put them into a new map with a pair
	//of nodes:
	boost::unordered_map< std::string, std::pair<const uhal::Node*,const uhal::Node*> >
		rtn;

	boost::unordered_map<std::string, const uhal::Node*>::iterator it;

	for ( it = lowNodes.begin(); it != lowNodes.end(); it++ )
	{
		const uhal::Node *blankNode = NULL;
		std::pair<const uhal::Node*, const uhal::Node*> nodePair(it->second, blankNode);
		rtn.insert( std::pair< std::string, 
				std::pair<const uhal::Node*,const uhal::Node*> >
				(it->first,nodePair) );
	}

	//Now loop over the highNodes, and add them to the relevant place in the new map.
	for ( it = highNodes.begin(); it != highNodes.end(); it++ )
	{
		//find the corresponding low item in the map (should have same uiName)
		boost::unordered_map<   std::string, 
			std::pair<const uhal::Node*,const uhal::Node*> >::iterator
				mapIt = rtn.find(it->first);

		if ( mapIt == rtn.end() ) ERROR("Logic error in debug map. Code 3");
		else
		{
			mapIt->second.second = it->second;
		}
	}

	return rtn;
}
