#ifndef __AMC13DataSource
#define __AMC13DataSource

#include "amc13controller/OpticalDataSource.hh"
#include "amc13controller/MdioInterface.hh"

namespace amc13controller
{
    class AMC13DataSource : public OpticalDataSource {
    public:
        AMC13DataSource( HAL::HardwareDeviceInterface *device_P,
                         utils::InfoSpaceHandler &appIS,
                         Logger logger );
	~AMC13DataSource ();
        void setDataSource() const;
    };
}

#endif /* __AMC13DataSource */
