#ifndef __AMC13Monitor
#define __AMC13Monitor

#include "xdaq/Application.h"
#include "d2s/utils/Monitor.hh"
#include "amc13controller/ApplicationInfoSpaceHandler.hh"
#include "amc13controller/StreamConfigInfoSpaceHandler.hh"
#include "amc13controller/AMC13InfoSpaceHandler.hh"
#include "amc13controller/StatusInfoSpaceHandler.hh"
#include "amc13controller/InputStreamInfoSpaceHandler.hh"
#include "amc13controller/OutputStreamInfoSpaceHandler.hh"


/************************************************************************
 *
 *
 *     @short 
 *
 *       @see 
 *    @author $Author: schwick $
 *   @version $Revision: 1.6 $
 *      @date $Date: 2001/03/06 17:07:51 $
 *
 *      Modif 
 *
 **//////////////////////////////////////////////////////////////////////

namespace amc13controller {
    class AMC13Monitor : public utils::Monitor 
    {
    public:
        AMC13Monitor( Logger &logger, ApplicationInfoSpaceHandler &appIS, xdaq::Application *xdaq, utils::ApplicationStateMachineIF &fsm );

        void addInfoSpace( amc13controller::StreamConfigInfoSpaceHandler *is );
        void addInfoSpace( amc13controller::AMC13InfoSpaceHandler *is );
        void addInfoSpace( amc13controller::StatusInfoSpaceHandler *is );
        void addInfoSpace( amc13controller::InputStreamInfoSpaceHandler *is );
        void addInfoSpace( amc13controller::OutputStreamInfoSpaceHandler *is );

    private:
        void addApplInfoSpaceItemSets( amc13controller::ApplicationInfoSpaceHandler *is );
    };
}

#endif /* __AMC13Monitor */
