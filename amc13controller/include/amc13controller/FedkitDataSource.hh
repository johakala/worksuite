#ifndef __FedkitDataSource
#define __FedkitDataSource

#include "amc13controller/OpticalDataSource.hh"

namespace amc13controller
{
    class FedkitDataSource : public OpticalDataSource 
    {
    public:
        FedkitDataSource( HAL::HardwareDeviceInterface *device_P,
                          utils::InfoSpaceHandler &appIS,
                          Logger logger );
        void setDataSource()  const;
    };
}

#endif /* __FedkitDataSource */
