#ifndef __AMC13HardwareLocker
#define __AMC13HardwareLocker
#include "d2s/utils/HardwareLocker.hh"
#include "amc13controller/ApplicationInfoSpaceHandler.hh"
#include "amc13controller/StatusInfoSpaceHandler.hh"

namespace amc13controller
{
    class HardwareLocker : public utils::HardwareLocker {

    public:

        HardwareLocker( Logger &logger,
                        ApplicationInfoSpaceHandler &appIS,
                        StatusInfoSpaceHandler &statusIS);

        bool lock();

/*        bool unlock();
        bool lockedByUs();

        std::string updateLockStatus();

    private:

        uint32_t getSlotUnit();

    private:

        Logger logger_;
        ApplicationInfoSpaceHandler &appIS_;
        StatusInfoSpaceHandler &statusIS_;

        ipcutils::SemaphoreArray *hwLock_P;

        std::string lockStatus_;

        uint32_t slotunit_;
  */      
    };
}

#endif /* __HardwareLocker */
