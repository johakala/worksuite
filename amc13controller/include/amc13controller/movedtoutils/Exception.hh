#ifndef __amc13_Exception
#define __amc13_Exception

#include "xcept/Exception.h"

/* This definition is now in XDAQ:
#define XCEPT_DEFINE_EXCEPTION(NAMESPACE1 , EXCEPTION_NAME) \
namespace NAMESPACE1 { \
namespace exception { \
class EXCEPTION_NAME: public xcept::Exception \
{\
	public: \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function ): \
		xcept::Exception(name, message, module, line, function) \
	{}; \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception e ): \
		xcept::Exception(name, message, module, line, function, e) \
	{}; \
}; \
} \
}
#define XCEPT_DECLARE_NESTED( EXCEPTION, VAR, MSG, PREVIOUS ) \
EXCEPTION VAR( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__, PREVIOUS)
*/


/************************************************************************
*
*
*     @short Baseclass of all amc13 exceptions.
*            
*            It  is  useful  to  define  a  baseclass  for  all  amc13 
*            exceptions since then you  can catch on this baseclass in
*            order to catch all possible subclasses with one command.
*
*       @see 
*    @author $Author: schwick $
*   @version $Revision: 1.6 $
*      @date $Date: 2001/03/06 17:07:51 $
*
*
**//////////////////////////////////////////////////////////////////////
namespace utils {
    namespace exception {
        class Exception : public xcept::Exception {
        public:
            Exception( std::string name, std::string message, std::string module, int line, std::string function )
                : xcept::Exception(name, message, module, line, function)
            {};
            Exception( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception e )
                : xcept::Exception(name, message, module, line, function, e)
            {};
        };
    }
}


#define D2S_UTILS_DEFINE_EXCEPTION( EXCEPTION_NAME )  \
namespace utils { \
    namespace exception {                                               \
        class EXCEPTION_NAME : public utils::exception::Exception        \
        {                                                               \
	public:                                                         \
            EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function ): \
		utils::exception::Exception(name, message, module, line, function) \
            {};                                                         \
            EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception e ): \
		utils::exception::Exception(name, message, module, line, function, e) \
            {};                                                         \
        };  \
    }       \
}


// Here are the AMC13 Exceptions which we use in the amc13 applicaton
D2S_UTILS_DEFINE_EXCEPTION( ConfigurationProblem )
D2S_UTILS_DEFINE_EXCEPTION( SoftwareProblem )
D2S_UTILS_DEFINE_EXCEPTION( HardwareAccessFailed )
D2S_UTILS_DEFINE_EXCEPTION( HardwareNotAvailable )
D2S_UTILS_DEFINE_EXCEPTION( IllegalStateTransition )
D2S_UTILS_DEFINE_EXCEPTION( SOAPTransitionProblem )

D2S_UTILS_DEFINE_EXCEPTION( FrlException )
D2S_UTILS_DEFINE_EXCEPTION( AMC13Exception )
D2S_UTILS_DEFINE_EXCEPTION( SlinkTestFailed )

D2S_UTILS_DEFINE_EXCEPTION( RCMSNotificationError )
D2S_UTILS_DEFINE_EXCEPTION( DataCorruptionDetected )

#endif /* __amc13_Exception */

