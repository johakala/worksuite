#ifndef __Watchdog
#define __Watchdog
#include <sys/time.h>
#include <stdint.h>

namespace utils
{
    class Watchdog {
    public:
        Watchdog( uint32_t timeoutval );
        void start();
        bool timeout();
    private:
        uint32_t subtractTime( struct timeval& t, struct timeval& sub);
        uint32_t timeoutval;
        timeval startTime;
    };
}

#endif /* __Watchdog */
