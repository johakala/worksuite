#ifndef __WebStaticContentTab
#define __WebStaticContentTab

#include "d2s/utils/WebTabIF.hh"
#include <string>

namespace utils
{
    class WebStaticContentTab : public WebTabIF {
    public:
        WebStaticContentTab( std::string name,
                             std::string contents 
                             );
        void print( std::ostream *out );
        void jsonUpdate( std::ostream *out );
        std::string getName();
        
    private:
        std::string name_;
        std::string contents_;
        
    };
}

#endif /* __WebStaticContentTab */
