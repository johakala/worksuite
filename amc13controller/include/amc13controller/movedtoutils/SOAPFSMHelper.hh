// $Id: SOAPFSMHelper.hh,v 1.3 2009/02/18 17:16:04 cschwick Exp $
#include "xoap/MessageReference.h"
#include <string>

#ifndef _amc13_SOAPFSMHelper_h_
#define _amc13_SOAPFSMHelper_h_

namespace utils
{
    class SOAPFSMHelper 
    {
    public:
        //SOAPFSMHelper( toolbox::fsm::FiniteStateMachine &fsm, xdaq::Application *appl );
        SOAPFSMHelper( );
        
        static xoap::MessageReference makeSoapReply( const std::string command, 
                                                     const std::string answerString  );
        
        static xoap::MessageReference makeSoapFaultReply( const std::string command, 
                                                          const std::string answerString  );
        
        static xoap::MessageReference makeFsmSoapReply( const std::string event, 
                                                        const std::string state );

    private:
    };
}

#endif /* _amc13_SOAPFSMHelper_h_ */

