#ifndef __ApplicationInfoSpaceHandler
#define __ApplicationInfoSpaceHandler

#include "xdaq2rc/ClassnameAndInstance.h"

#include "amc13controller/AMC13BaseInfoSpaceHandler.hh"
namespace amc13controller {
    class ApplicationInfoSpaceHandler : public AMC13BaseInfoSpaceHandler, 
                                        public xdata::ActionListener
    {
    public:
        ApplicationInfoSpaceHandler( xdaq::Application *xdaq );
        // callback for Parameter Setting
        void actionPerformed( xdata::Event& e );
        
        // a special call for the ApplicationInfoSpace to set the parameters of the statelistener.
        void setRCMSStateListenerParameters(  xdata::Bag<xdaq2rc::ClassnameAndInstance> *RcmsStateListenerParameters,
                                              xdata::Boolean *FoundRcmsStateListener );

	void mirrorFromFlatParams();
	void mirrorToFlatParams();
    private:
        
    };
}

#endif /* __ApplicationInfoSpaceHandler */
