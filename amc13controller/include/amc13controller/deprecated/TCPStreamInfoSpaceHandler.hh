#ifndef __TCPStreamInfoSpaceHandler
#define __TCPStreamInfoSpaceHandler

#include "d2s/utils/StreamInfoSpaceHandler.hh"
#include "amc13controller/DataTracker.hh"

namespace amc13controller
{
    class TCPStreamInfoSpaceHandler:public utils::StreamInfoSpaceHandler {
    public:
        TCPStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater, utils::InfoSpaceHandler *appIS );

        void registerTrackerItems( DataTracker &tracker );
  
    private:
    };
}

#endif /* __TCPStreamInfoSpaceHandler */
