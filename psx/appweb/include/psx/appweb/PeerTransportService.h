// $Id: PeerTransportService.h,v 1.1 2006/02/08 13:56:54 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_appweb_PeerTransportService_h_
#define _psx_appweb_PeerTransportService_h_

#include "psx/PeerTransportService.h"
 
namespace psx 
{
    namespace appweb 
    {    
    
		class PeerTransportService: public psx::PeerTransportService
		{
      			public:			

			xoap::MessageReference post(const std::string & url, xoap::MessageReference  request) 
				throw (psx::exception::Exception );

			
		};
	}	
}

#endif 

