// $Id: PeerTransportService.cc,v 1.1 2006/02/08 13:56:54 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/appweb/PeerTransportService.h"
#include "appWeb/appWeb.h"

#include "xoap/MessageFactory.h"
#include "psx/exception/Exception.h"
 

xoap::MessageReference psx::appweb::PeerTransportService::post(const std::string & url, xoap::MessageReference  request) throw (psx::exception::Exception )
{
	std::vector<std::string> actions = request->getMimeHeaders()->getHeader("SOAPAction");
	std::string soapAction;
	if ( actions.size() > 0 )
	{
		soapAction = actions[0];
	
	}

	
	
	std::string data;
	request->writeTo(data);
	
	
	
	// Parse URL to extract information
	MaUrl urlObj;
	urlObj.parse((char*)url.c_str());

	MprBuf headerBuf;
	headerBuf.putFmt("POST %s HTTP/1.1\r\n", url.c_str());
	headerBuf.putFmt("Host: %s\r\n", urlObj.host);
	headerBuf.putFmt("User-Agent: %s\r\n", MPR_HTTP_CLIENT_NAME);	
	headerBuf.putFmt("Connection: Keep-Alive\r\n");	
	headerBuf.putFmt("Content-Length: %d\r\n", data.size());
	headerBuf.putFmt("Content-Type: multipart/related; type=\"text/xml\"\r\n");
	headerBuf.putFmt("SOAPAction: %s\r\n",soapAction.c_str());
	headerBuf.putFmt("\r\n");
	headerBuf.addNull();
	// POST a request
	MaClient client;
	client.setKeepAlive(true);
	client.setPort(urlObj.port);
	client.sendRequest(urlObj.host, urlObj.port, &headerBuf, (char*)data.c_str(), data.size());

	int code = client.getResponseCode();
	
	if ( code != 200 ) 
	{
		std::stringstream msg;
		msg << "HTTP error code: " << code;
		XCEPT_RAISE(psx::exception::Exception, msg.str() );
	}
	
	int contentLength;
	char * content = client.getResponseContent(&contentLength); 
	
	xoap::MessageReference reply;
	
	if ( content )
	{
		try 
		{
			reply = xoap::createMessage(content, contentLength);	
		}
		catch (xoap::exception::Exception& e)
		{
		
			XCEPT_RETHROW(psx::exception::Exception, "failed to receive SOAP response", e );
		}
	
	}
	else
	{
		std::stringstream msg;
		msg << "Received empty response";
		XCEPT_RAISE(psx::exception::Exception, msg.str() );
	}
	
	return reply;
} 

