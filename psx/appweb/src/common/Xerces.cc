// $Id: Xerces.cc,v 1.1 2006/02/08 11:07:43 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/dom/DOM.hpp"
#include "psx/appweb/Xerces.h"

XERCES_CPP_NAMESPACE_USE;
 

psx::appweb::Xerces * psx::appweb::Xerces::instance_ = 0;

psx::appweb::Xerces::Xerces()
{
		XMLPlatformUtils::Initialize();
}

void psx::appweb::Xerces::init()
{
	if ( instance_ == 0 )
	{
		instance_ = new psx::appweb::Xerces();
	}
	
}

