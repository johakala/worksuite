// $Id: Module.cc,v 1.2 2006/02/15 11:04:18 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
#include <fstream>

#include "psx/appweb/Xerces.h"
#include "psx/appweb/smi/Module.h"


// The name of the module muts have the first character in Uppercase
int mprPSXSMIInit(void *handle)
{
    	mprLog(0, "In mprPSXSMIInit()\n");
    	new psx::appweb::smi::Module(handle);
    	return 0;
}
 
psx::appweb::smi::Module::Module(void *handle) : MaModule("PSXPVSS", handle) 
{
	dns_ = "";
	mprLog(0, "psx::appweb::smi::Module::Module(void *handle) : MaModule(\"PSXSMI\", handle)\n");
	
	//Make sure the initialization is perfromed only once
	psx::appweb::Xerces::init();
	
	service_ = new psx::appweb::smi::HandlerService();
	
}

psx::appweb::smi::Module::~Module()
{
    	//
    	//  Put cleanup code here for when AppWeb is exiting
    	//
	mprLog(0, "psx::appweb::smi::Module::~Module()\n");
	if ( service_ != 0 )
		delete service_;
}

int psx::appweb::smi::Module::parseConfig(char *key, char *value, MaServer *server,
		        MaHost *host, MaAuth *auth, MaDir* dir, 
			MaLocation *location)
{
	mprLog(0, "psx::appweb::smi::Module::parseConfig\n");
	
  	if (mprStrCmpAnyCase(key, "DIMDnsNode") == 0) 
	{
		//
		//  Put code here to parse the "value". Return 1 to indicate
		//  we have processed this directive.
		//
		//std::cout << "Found projectName directive: " << value << std::endl;
		dns_ = value;
		return 1;
	}
	
	
	return 0;
}
			
int psx::appweb::smi::Module::start()
{
	mprLog(0, "psx::appweb::smi::Module::start\n");
 	
	service_->init(dns_);
	return 0;
}

void psx::appweb::smi::Module::stop()
{
	mprLog(0, "::appweb::Module::stop\n");	
}
