// $Id: HandlerService.cc,v 1.5 2006/02/17 13:32:07 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <iostream>
#include <fstream>

#include "xoap/MessageFactory.h"

#include "psx/exception/Exception.h"


#include "psx/appweb/smi/HandlerService.h"
#include "psx/appweb/smi/Handler.h"

#include "psx/mapi/ApplicationService.h"


psx::appweb::smi::HandlerService::HandlerService() :  MaHandlerService("psxSMIHandlerService")
{
	mprLog(0, "psx::appweb::smi::HandlerService::HandlerService()\n");
	as_ = 0;
}

void psx::appweb::smi::HandlerService::init(const std::string& dns)
{
	// Static function to initialize application service before create object 
	psx::mapi::ApplicationService::init(dns);
}

psx::mapi::ApplicationService * psx::appweb::smi::HandlerService::getApplicationService()
{	
	return as_;
}
psx::appweb::smi::HandlerService::~HandlerService()
{
	mprLog(0, "psx::appweb::smi::HandlerService::~HandlerService()\n");
	if ( as_ != 0 )
		delete as_;
}

//
// Setup the SimpleHandler service. One-time setup for a given host.
//

int psx::appweb::smi::HandlerService::start()
{
    	mprLog(0, "psx::appweb::smi::HandlerService::start()\n");
    
	// mapi service is only created once, just like the HandlerService is only created once
	//
	as_= new psx::mapi::ApplicationService(this);
	as_->activate();
	
   	 return 0;
}

int psx::appweb::smi::HandlerService::stop()
{
    mprLog(0, "psx::appweb::smi::HandlerService::stop()\n");
    return 0;
}

//
// New Handler factory. Create a new psx::appweb::smi::Handler for an incoming 
// HTTP request for a given server
//
MaHandler * psx::appweb::smi::HandlerService::newHandler(MaServer *server,  MaHost *host, char *extensions)
{
    mprLog(0, "psx::appweb::smi::HandlerService::newHandler()\n");
    return new psx::appweb::smi::Handler(extensions,this);
}

