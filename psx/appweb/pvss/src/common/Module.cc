// $Id: Module.cc,v 1.4 2006/02/23 16:30:58 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
#include <fstream>

#include "psx/appweb/Xerces.h"
#include "psx/appweb/pvss/Module.h"


// The name of the module muts have the first character in Uppercase
int mprPSXPVSSInit(void *handle)
{
    	mprLog(0, "In mprPSXPVSSInit()\n");
    	new psx::appweb::pvss::Module(handle);
    	return 0;
}
 
psx::appweb::pvss::Module::Module(void *handle) : MaModule("PSXPVSS", handle) 
{
	mprLog(0, "psx::appweb::pvss::Module::Module(void *handle) : MaModule(\"PSXPVSS\", handle)\n");
	
	//Make sure the initialization is perfromed only once
	psx::appweb::Xerces::init();

	projectName_ = "";
	projectNumber_  = ""; // no -num option
	databaseManager_ = "";
	eventManager_ =  "";
	service_ = new psx::appweb::pvss::HandlerService();
	
}

psx::appweb::pvss::Module::~Module()
{
    	//
    	//  Put cleanup code here for when AppWeb is exiting
    	//
	mprLog(0, "psx::appweb::pvss::Module::~Module()\n");
	if ( service_ != 0 )
		delete service_;
}

int psx::appweb::pvss::Module::parseConfig(char *key, char *value, MaServer *server,
		        MaHost *host, MaAuth *auth, MaDir* dir, 
			MaLocation *location)
{
	mprLog(0, "psx::appweb::pvss::Module::parseConfig\n");
	
  	if (mprStrCmpAnyCase(key, "PVSSProjectName") == 0) 
	{
		//
		//  Put code here to parse the "value". Return 1 to indicate
		//  we have processed this directive.
		//
		//std::cout << "Found projectName directive: " << value << std::endl;
		
		projectName_ = value;
		return 1;
	}
	
	if (mprStrCmpAnyCase(key, "PVSSProjectNumber") == 0) 
	{
		//
		//  Put code here to parse the "value". Return 1 to indicate
		//  we have processed this directive.
		//
		//std::cout << "Found projectNumber directive: " << value  << std::endl;
		
		projectNumber_ = value;
		return 1;
	}
	
	if (mprStrCmpAnyCase(key, "PVSSDatabaseManager") == 0) 
	{
		//
		//  Put code here to parse the "value". Return 1 to indicate
		//  we have processed this directive.
		//
		//std::cout << "Found projectNumber directive: " << value  << std::endl;
		
		databaseManager_ = value;
		return 1;
	}
	
	if (mprStrCmpAnyCase(key, "PVSSEventManager") == 0) 
	{
		//
		//  Put code here to parse the "value". Return 1 to indicate
		//  we have processed this directive.
		//
		//std::cout << "Found projectNumber directive: " << value  << std::endl;
		
		eventManager_ = value;
		return 1;
	}
	
	return 0;
}
			
int psx::appweb::pvss::Module::start()
{
	mprLog(0, "psx::appweb::pvss::Module::start\n");
 	
	service_->init(projectName_, projectNumber_, databaseManager_, eventManager_);
	return 0;
}

void psx::appweb::pvss::Module::stop()
{
	mprLog(0, "::appweb::Module::stop\n");	
}
