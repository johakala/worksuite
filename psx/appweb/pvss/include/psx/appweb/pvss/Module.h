// $Id: Module.h,v 1.2 2006/02/23 15:58:54 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_appweb_pvss_Module_h_
#define _psx_appweb_pvss_Module_h_

#include <string>

#include "appWeb/appWeb.h"

#include "psx/appweb/pvss/HandlerService.h"


extern "C" {
    extern int mprPSXPVSSInit(void *handle);
};
    
namespace psx {
	namespace appweb {
		namespace pvss {
			class Module : public MaModule 
			{
			      public:
				Module(void *handle);
				~Module();
				int parseConfig(char *key, char *value, MaServer *server,
		        			MaHost *host, MaAuth *auth, MaDir* dir, 
						MaLocation *location);
				int start();
				void stop();

			     private:

			     psx::appweb::pvss::HandlerService *service_;
			     std::string projectName_;
			     std::string projectNumber_;
			     std::string databaseManager_;
			     std::string eventManager_;	
			};
		}	

	}
}
#endif 
