// $Id: Handler.h,v 1.1 2006/02/07 14:35:43 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_appweb_pvss_Handler_h_
#define _psx_appweb_pvss_Handler_h_

#include "appWeb/appWeb.h"

// This file include the functions to communicate with PVSS
//
#include "psx/sapi/ApplicationService.h"
#include "psx/sapi/exception/Exception.h"
 
namespace psx 
{
    namespace appweb 
    {    
    	namespace pvss {
	
    		class HandlerService;
    
    		/*! Per incoming SOAP request, one object of class Handler is created.
		*/
		class Handler: public MaHandler 
		{
      			public:
			
			// psx::sapi::ApplicationService* as is created by psx::appweb::pvss::HandlerService
			//
			Handler(char * extensions, psx::appweb::pvss::HandlerService * hs);
			
			~Handler();
			MaHandler *cloneHandler();
			int setup(MaRequest *rq);
			int matchRequest(MaRequest *rq, char *uri, int uriLen);
			void postData(MaRequest *rq, char *buf, int len);
			int run(MaRequest *rq);

			private:
			
			MprBuf * postBuf;
			psx::appweb::pvss::HandlerService * hs_;
		};
	}	
    }
}
#endif 

