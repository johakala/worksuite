// $Id: SMI.h,v 1.2 2008/04/08 13:42:30 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _SMI_h_
#define _SMI_h_

#include "xdaq/WebApplication.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"

#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xgi/Method.h"
#include "cgicc/HTMLClasses.h"

#define PSX_NS_URI "http://xdaq.cern.ch/xdaq/xsd/2006/psx-10.xsd"
#define PSX_SMI_NS_URI "http://xdaq.web.cern.ch/xdaq/xsd/2006/psx-smi-10.xsd"

class SMI: public xdaq::Application  
{	
	public:
		
	XDAQ_INSTANTIATOR();
	
	SMI(xdaq::ApplicationStub * s);
	
	//
	// SOAP Callback  
	//
	xoap::MessageReference notify (xoap::MessageReference msg) throw (xoap::exception::Exception);
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void getState(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void sendCommand(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void connect(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void disconnect(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void take(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void release(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
	
	void showForm(xgi::Output * out);
	
	protected:
	
	std::string response_;
	std::string lastObject_;
	std::string lastDomain_;
};

#endif
