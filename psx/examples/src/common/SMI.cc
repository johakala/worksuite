// $Id: SMI.cc,v 1.5 2008/04/08 13:42:30 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "SMI.h"
#include "xgi/Utils.h"

XDAQ_INSTANTIATOR_IMPL(SMI)

SMI::SMI(xdaq::ApplicationStub * s): xdaq::Application(s) 
{	
	getApplicationDescriptor()->setAttribute("icon", "/daq/psx/images/PSX.png");

	xoap::bind(this, &SMI::notify, "notify", PSX_SMI_NS_URI );

	xgi::bind(this,&SMI::Default, "Default");
	xgi::bind(this,&SMI::getState, "getState");
	xgi::bind(this,&SMI::sendCommand, "sendCommand");
	xgi::bind(this,&SMI::connect, "connect");
	xgi::bind(this,&SMI::disconnect, "disconnect");
	xgi::bind(this,&SMI::take, "take");
	xgi::bind(this,&SMI::release, "release");
}


//
// SOAP Callback  
//
xoap::MessageReference SMI::notify (xoap::MessageReference msg) throw (xoap::exception::Exception)
{
	std::cout << "Received notify: " << std::endl;
	
	msg->writeTo(std::cout);
	
	std::cout << std::endl;
	
	// reply to caller		
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName( "notifyResponse", "smi", PSX_SMI_NS_URI);
	// xoap::SOAPBodyElement e = 
	envelope.getBody().addBodyElement ( responseName );
	return reply;		
}

void SMI::Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{	
	this->showForm(out);
}

void SMI::getState(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		std::string object = cgi["object"]->getValue();
		std::string domain = cgi["domain"]->getValue();
		lastObject_ = object;
		lastDomain_ = domain;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			// Create message:
			// <smi:getState xmlns:smi=\"http://xdaq.web.cern.ch/xdaq/xsd/2006/psx-smi-10.xsd\" domain=\"name\" object=\"name\" >
			//
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("getState","smi",PSX_SMI_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);

			xoap::SOAPName objectAttribute = env.createName("object","","");
			bodyElem.addAttribute(objectAttribute, object);

			xoap::SOAPName domainAttribute = env.createName("domain","","");
                        bodyElem.addAttribute(domainAttribute, domain);
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor* d = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("smi", 0);
			xdaq::ApplicationDescriptor* s = this->getApplicationDescriptor();
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *s, *d);

			reply->writeTo(response_);
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }	
}

void SMI::sendCommand(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string object = cgi["object"]->getValue();
		 std::string domain = cgi["domain"]->getValue();
		std::string command = cgi["command"]->getValue();
		std::string owner = cgi["owner"]->getValue();
		lastObject_ = object;
		lastDomain_ = domain;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			// Create SOAP message
			// <smi:send xmlns:smi=\"http://xdaq.web.cern.ch/xdaq/xsd/2006/psx-smi-10.xsd\" domain=\"name\" object=\"name\" owner=\"name\" command=\"name\"/>
			//
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("send","smi",PSX_SMI_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);

			xoap::SOAPName domainAttribute = env.createName("domain","","");
			bodyElem.addAttribute(domainAttribute, domain);

			xoap::SOAPName objectAttribute = env.createName("object","","");
			bodyElem.addAttribute(objectAttribute, object);
			
			xoap::SOAPName commandAttribute = env.createName("command","","");
			bodyElem.addAttribute(commandAttribute, command);
			
			xoap::SOAPName ownerAttribute = env.createName("owner","","");
			bodyElem.addAttribute(ownerAttribute, owner);
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor* d = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("smi", 0);
			xdaq::ApplicationDescriptor* s = this->getApplicationDescriptor();
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *s, *d);

			reply->writeTo(response_);
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }	
}

void SMI::connect(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string object = cgi["object"]->getValue();
		std::string domain = cgi["domain"]->getValue();
		lastObject_ = object;
		lastDomain_ = domain;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("connect","smi",PSX_SMI_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
			
			xoap::SOAPName urlAttribute = env.createName("url","","");
			bodyElem.addAttribute(urlAttribute, this->getApplicationContext()->getContextDescriptor()->getURL());
			
			xoap::SOAPName actionAttribute = env.createName("action","","");
			bodyElem.addAttribute(actionAttribute, this->getApplicationDescriptor()->getURN());

			xoap::SOAPName domainAttribute = env.createName("domain","","");
			bodyElem.addAttribute(domainAttribute, domain);

			xoap::SOAPName objectAttribute = env.createName("object","","");
			bodyElem.addAttribute(objectAttribute, object);
			
			xoap::SOAPName contextAttribute = env.createName("context","","");
			bodyElem.addAttribute(contextAttribute, this->getApplicationDescriptor()->getURN());
			
			xoap::SOAPName ownerAttribute = env.createName("owner","","");
			bodyElem.addAttribute(ownerAttribute, "none");
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor* d = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("smi", 0);
			xdaq::ApplicationDescriptor* s = this->getApplicationDescriptor();
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *s, *d);
		
			xoap::SOAPEnvelope replyEnv = reply->getEnvelope();
			xoap::SOAPBody replyBody = replyEnv.getBody();
			if (replyBody.hasFault())
			{
				xoap::SOAPFault f = replyBody.getFault();
				response_ = "Failed to connect: ";
				response_ += f.getFaultString();
			}
			else
			{
				std::vector<xoap::SOAPElement> v = replyBody.getChildElements();
				
				for (unsigned int i = 0; i < v.size(); i++)
				{
					xoap::SOAPName n = v[i].getElementName();
					if ( n.getLocalName() == "connectResponse" )
					{
						// find transaction id
						response_ = "Transaction id is ";
						xoap::SOAPName tidAttribute("id","","");
						response_ += v[i].getAttributeValue(tidAttribute);
					}
				} 
			}

			// reply->writeTo(response_);
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }	
}

void SMI::disconnect(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string transaction = cgi["transaction"]->getValue();

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			// Create SOAP message
			// <smi:disconnect xmlns:smi=\"http://xdaq.web.cern.ch/xdaq/xsd/2006/psx-smi-10.xsd\" id=\"$1\"/>
			//
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("disconnect","smi",PSX_SMI_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
			xoap::SOAPName idAttribute = env.createName("id","","");
			bodyElem.addAttribute(idAttribute, transaction);
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor* d = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("smi", 0);
			xdaq::ApplicationDescriptor* s = this->getApplicationDescriptor();
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *s, *d);

			reply->writeTo(response_);
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }	
}

void SMI::take(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		// retrieve value and update exported variable
		std::string object = cgi["object"]->getValue();
		std::string domain = cgi["domain"]->getValue();
		std::string owner = cgi["owner"]->getValue();
		
		lastObject_ = object;
		lastDomain_ = domain;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("take","smi",PSX_SMI_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
			
			xoap::SOAPName domainAttribute = env.createName("domain","","");
			bodyElem.addAttribute(domainAttribute, domain);

			xoap::SOAPName objectAttribute = env.createName("object","","");
			bodyElem.addAttribute(objectAttribute, object);
			
			xoap::SOAPName ownerAttribute = env.createName("owner","","");
			bodyElem.addAttribute(ownerAttribute, owner);
			
			// Maybe better to use a checkbox here - don't know how to use it
			xoap::SOAPName exclusiveAttribute = env.createName("exclusive","","");
			
			if (cgi.queryCheckbox("exclusive"))
			{
				bodyElem.addAttribute(exclusiveAttribute, "yes");
			}
			else
			{
				bodyElem.addAttribute(exclusiveAttribute, "no");
			}
			
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor* d = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("smi", 0);
			xdaq::ApplicationDescriptor* s = this->getApplicationDescriptor();
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *s, *d);

			reply->writeTo(response_);
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }
}

void SMI::release(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

	try
        {
                cgicc::Cgicc cgi(in);

		std::string object = cgi["object"]->getValue();
		std::string domain = cgi["domain"]->getValue();
		std::string owner = cgi["owner"]->getValue();
		lastObject_ = object;
		lastDomain_ = domain;

		// constructed using XOAP
		xoap::MessageReference msg = xoap::createMessage();

		try
		{
			// Create SOAP message
			// <smi:release xmlns:smi=\"http://xdaq.web.cern.ch/xdaq/xsd/2006/psx-smi-10.xsd\" object=\"DeviceController\" owner=\"pippo\" all=\"true\">
			//
			xoap::SOAPEnvelope env = msg->getEnvelope();
			xoap::SOAPBody body = env.getBody();
			xoap::SOAPName cmdName = env.createName("release","smi",PSX_SMI_NS_URI);
			xoap::SOAPBodyElement bodyElem = body.addBodyElement(cmdName);
			
			xoap::SOAPName domainAttribute = env.createName("domain","","");
			bodyElem.addAttribute(domainAttribute, domain);

			xoap::SOAPName objectAttribute = env.createName("object","","");
			bodyElem.addAttribute(objectAttribute, object);
			
			xoap::SOAPName ownerAttribute = env.createName("owner","","");
			bodyElem.addAttribute(ownerAttribute, owner);
			
			// Maybe better to use a checkbox here - don't know how to use it
			xoap::SOAPName allAttribute = env.createName("all","","");
			
			if (cgi.queryCheckbox("all"))
			{
				bodyElem.addAttribute(allAttribute, "yes");
			}
			else
			{
				bodyElem.addAttribute(allAttribute, "no");
			}
		}
		catch(xoap::exception::Exception& xe)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to create message", xe);	
		}

		// Send SOAP message
		try
		{	
			xdaq::ApplicationDescriptor* d = getApplicationContext()->getDefaultZone()->getApplicationDescriptor("smi", 0);
			xdaq::ApplicationDescriptor* s = this->getApplicationDescriptor();
			xoap::MessageReference reply = getApplicationContext()->postSOAP(msg, *s, *d);

			reply->writeTo(response_);
		} 
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW (xgi::exception::Exception, "Failed to send SOAP message", e);		
		}

		// re-display form page and response
		this->showForm(out);		
        }
        catch (const std::exception & e)
        {
                 XCEPT_RAISE(xgi::exception::Exception, e.what());
        }
}


void SMI::showForm(xgi::Output * out)
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	
	*out << cgicc::head() << std::endl;
		
	*out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/daq/psx/html/psx.css\">";
	
	*out << cgicc::head() << std::endl;
	
	xgi::Utils::getPageHeader
		(out, 
		"SMI SOAP Exchange", 
		getApplicationDescriptor()->getContextDescriptor()->getURL(),
		getApplicationDescriptor()->getURN(),
		"/daq/psx/images/PSX.png"
		);

	*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
	*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;
	*out << cgicc::title("SMI Access") << std::endl;

	// This method can be invoked using Linux 'wget' command
	// e.g http://lxcmd101:1972/urn:xdaq-application:lid=23/setParameter?value=24
	std::string method = toolbox::toString("/%s/",getApplicationDescriptor()->getURN().c_str());

	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Get State") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"getState") << std::endl;
	*out << cgicc::label("Object") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","domain").set("value", lastDomain_) << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","object").set("value", lastObject_) << std::endl;
	
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Send Command") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"sendCommand") << std::endl;
	*out << cgicc::label("Object") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","domain").set("value", lastDomain_) << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","object").set("value", lastObject_) << std::endl;
	*out << cgicc::label("Command") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "10").set("name","command") << std::endl;
	*out << cgicc::label("Owner") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "10").set("name","owner") << std::endl;
	
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Connect to Object") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"connect") << std::endl;
	*out << cgicc::label("Object") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","domain").set("value", lastDomain_) << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","object").set("value", lastObject_) << std::endl;
	
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Disconnect from Object") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"disconnect") << std::endl;
	*out << cgicc::label("Transaction id") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "8").set("name","transaction") << std::endl;
	
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Take tree") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"take") << std::endl;
	*out << cgicc::label("Object") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","domain").set("value", lastDomain_) << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","object").set("value", lastObject_) << std::endl;
	*out << cgicc::label("Owner") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "10").set("name","owner") << std::endl;
	*out << cgicc::label("Exclusive") << std::endl;
	*out << cgicc::input().set("type","checkbox").set("name","exclusive") << std::endl;
	
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;
	
	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
        *out << cgicc::legend("Release tree") << cgicc::p() << std::endl;
	*out << cgicc::form().set("method","POST").set("action", method+"release") << std::endl;

	*out << cgicc::label("Object") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","domain").set("value", lastDomain_) << std::endl;
	*out << cgicc::input().set("type","text").set("size", "30").set("name","object").set("value", lastObject_) << std::endl;
	*out << cgicc::label("Owner") << std::endl;
	*out << cgicc::input().set("type","text").set("size", "10").set("name","owner") << std::endl;
	*out << cgicc::label("All") << std::endl;
	*out << cgicc::input().set("type","checkbox").set("name","all") << std::endl;
	
	*out << cgicc::input().set("type","submit").set("value","Send")  << std::endl;	
	
	*out << cgicc::form() << std::endl;
	*out << cgicc::fieldset() << std::endl;

	*out << cgicc::hr() << std::endl;

	*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;
	*out << cgicc::legend("SOAP Response") << cgicc::p() << std::endl;
	*out << cgicc::textarea().set("rows","20").set("cols","100") << std::endl;

	*out << response_;

	*out << cgicc::textarea() << std::endl;
	*out << cgicc::fieldset() << std::endl;
			
	xgi::Utils::getPageFooter(*out);
	*out <<  cgicc::html() << std::endl;
}
