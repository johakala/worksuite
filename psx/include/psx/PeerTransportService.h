// $Id: PeerTransportService.h,v 1.2 2006/02/08 15:17:53 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_PeerTransportService_h_
#define _psx_PeerTransportService_h_

#include "xoap/MessageReference.h"
#include "psx/exception/Exception.h"
#include <string>

namespace psx 
{    
	class PeerTransportService 
	{
	      public:
	      
	      	//! Implemented for each Web server using server specific technology for sending SOAP
		/* Do not forget to set the SOAPAction field using getMIMEHeaders on the request
	         */
		virtual xoap::MessageReference post(const std::string& url, xoap::MessageReference  request)
			throw (psx::exception::Exception) = 0;
	};

}

#endif 
