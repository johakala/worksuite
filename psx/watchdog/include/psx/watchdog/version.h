#ifndef _psx_watchdog_version_h_
#define _psx_watchdog_version_h_

#include "config/PackageInfo.h"

#define PSXWATCHDOG_VERSION_MAJOR 2
#define PSXWATCHDOG_VERSION_MINOR 0
#define PSXWATCHDOG_VERSION_PATCH 0

#undef PSXWATCHDOG_PREVIOUS_VERSIONS

#define PSXWATCHDOG_VERSION_CODE PACKAGE_VERSION_CODE(PSXWATCHDOG_VERSION_MAJOR, PSXWATCHDOG_VERSION_MINOR, PSXWATCHDOG_VERSION_PATCH)
#ifndef PSXWATCHDOG_PREVIOUS_VERSIONS
#define PSXWATCHDOG_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PSXWATCHDOG_VERSION_MAJOR, PSXWATCHDOG_VERSION_MINOR, PSXWATCHDOG_VERSION_PATCH)
#else
#define PSXWATCHDOG_FULL_VERSION_LIST PSXWATCHDOG_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PSXWATCHDOG_VERSION_MAJOR, PSXWATCHDOG_VERSION_MINOR, PSXWATCHDOG_VERSION_PATCH)
#endif


namespace psxwatchdog
{
	const std::string package = "psxwatchdog";
	const std::string versions = PSXWATCHDOG_FULL_VERSION_LIST;
	const std::string summary = "Watchdog of PSX service";
	const std::string description = "Application monitors health of PSX service and restarts it if necessary";
	const std::string authors = "D. Simelevicius and L. Orsini";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
