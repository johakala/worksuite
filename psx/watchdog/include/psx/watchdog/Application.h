#ifndef _psx_watchdog_Application_h_
#define _psx_watchdog_Application_h_

#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptorFactory.h"

#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "cgicc/HTMLClasses.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPConstants.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/AttachmentPart.h"

#include "xdata/String.h"

#include "psx/watchdog/exception/Exception.h"

namespace psx {
	namespace watchdog {

		class Application: public xdaq::Application, public xgi::framework::UIManager, public toolbox::task::TimerListener, public xdata::ActionListener
		{
		public:
			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
			virtual ~Application();
			void Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);

		private:
			void timeExpired(toolbox::task::TimerEvent& e);
			void actionPerformed(xdata::Event& e);

			void startWatchdogTimer(const time_t& waitTime, const time_t& checkPeriod);
			bool ping();
			void kill();
			std::string prepareKillLine(xdaq::ApplicationDescriptor* psxDesc, const xdaq::ApplicationDescriptor* watchdogDesc) throw(psx::watchdog::exception::Exception);
			std::string prepareRestartLine(xdaq::ApplicationDescriptor* psxDesc, const xdaq::ApplicationDescriptor* watchdogDesc) throw(psx::watchdog::exception::Exception);
			void createPingMessage(const xoap::MessageReference& msg);
			void createPSXDescriptor(xdaq::ApplicationDescriptor*& psxDesc, xdaq::ContextDescriptor*& psxContext) throw(psx::watchdog::exception::Exception);

			//Ping period
			xdata::UnsignedInteger checkPeriod_;
			//Timeout of PSX server
			xdata::UnsignedInteger timeout_;
			//Waiting period after the kill before resuming usual pinging
			xdata::UnsignedInteger waitTime_;
			//The data point for a watchdog to read
			xdata::String dataPoint_;
			//PSX application ID
			xdata::UnsignedInteger psxId_;
			//PSX port
			xdata::UnsignedInteger psxPort_;
			//PSX servicename as in <zone>.<servicename>@<hostname>.service
			xdata::String psxServiceName_;
			//If "true" uses kill command after unsuccessful ping
			xdata::Boolean killPsx_;

			toolbox::TimeVal watchdogStart_;
			toolbox::TimeVal lastPing_;
			toolbox::TimeVal lastUnsuccessfulPing_;
			unsigned int killCount_;

			xdaq::ApplicationDescriptorFactory* applicationDescriptorFactory_;
			xdaq::ContextDescriptor* psxContext_;
			xdaq::ApplicationDescriptor* psxDesc_;
			const xdaq::ApplicationDescriptor* watchdogDesc_;

			//Kill command line
			std::string pkill_;

			//Restart command line
			std::string restart_;

			// Mutex is used to synchronize calls to startWatchdogTimer()
			pthread_mutex_t startTimerMutex_;
			pthread_mutexattr_t startTimerMutexAttr_;

			// Mutex is used to synchronize calls to ping()
			pthread_mutex_t pingMutex_;
			pthread_mutexattr_t pingMutexAttr_;
		};

	}
}

#endif
