#include "psx/watchdog/Application.h"

#define PSX_PVSS_NS_URI "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd"

XDAQ_INSTANTIATOR_IMPL(psx::watchdog::Application)

psx::watchdog::Application::Application(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception):
	xdaq::Application(s), xgi::framework::UIManager(this), killCount_(0), applicationDescriptorFactory_(0), psxContext_(0), psxDesc_(0), watchdogDesc_(0), pkill_(""), restart_("")
{
	s->getDescriptor()->setAttribute("icon","/psx/watchdog/images/watchdog.png");

	pthread_mutexattr_init(&startTimerMutexAttr_);
	pthread_mutexattr_settype(&startTimerMutexAttr_, PTHREAD_MUTEX_RECURSIVE_NP);
	pthread_mutex_init(&startTimerMutex_, &startTimerMutexAttr_);

	pthread_mutexattr_init(&pingMutexAttr_);
	pthread_mutexattr_settype(&pingMutexAttr_, PTHREAD_MUTEX_RECURSIVE_NP);
	pthread_mutex_init(&pingMutex_, &pingMutexAttr_);

	applicationDescriptorFactory_ = xdaq::ApplicationDescriptorFactory::getInstance();

	xgi::framework::deferredbind(this, this, &Application::Default, "Default");

	//Get application descriptor of watchdog
	watchdogDesc_ = this->getApplicationDescriptor();

	//Watchdog start time used in a webpage
	watchdogStart_ = toolbox::TimeVal::gettimeofday();

	timeout_ = 30; //default PSX timeout
	checkPeriod_ = 60; //default ping period
	waitTime_ = 120; //default wait time after kill
	dataPoint_ = "_Event.License.ExpirationCnt:_original.._value"; //default data point
	psxId_ = 0;
	psxPort_ = 0;
	psxServiceName_ = "";
	killPsx_ = true;

	xdata::InfoSpace * is = this->getApplicationInfoSpace();
	is->fireItemAvailable("checkPeriod", &checkPeriod_);
	is->fireItemAvailable("timeout", &timeout_);
	is->fireItemAvailable("waitTime", &waitTime_);
	is->fireItemAvailable("dataPoint", &dataPoint_);
	is->fireItemAvailable("psxId", &psxId_);
	is->fireItemAvailable("psxPort", &psxPort_);
	is->fireItemAvailable("psxServiceName", &psxServiceName_);
	is->fireItemAvailable("killPsx", &killPsx_);

	is->addItemChangedListener("checkPeriod", this);
	is->addItemChangedListener("waitTime", this);
	is->addItemChangedListener("psxId", this);
	is->addItemChangedListener("psxPort", this);
	is->addItemChangedListener("psxServiceName", this);
	is->addItemChangedListener("killPsx", this);

	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

psx::watchdog::Application::~Application()
{
	if ( applicationDescriptorFactory_->hasDescriptor(psxDesc_) )
	{
		applicationDescriptorFactory_->destroyDescriptor(psxDesc_);
	}
	delete psxContext_;

	pthread_mutexattr_destroy(&pingMutexAttr_);
	pthread_mutex_destroy(&pingMutex_);

	pthread_mutexattr_destroy(&startTimerMutexAttr_);
	pthread_mutex_destroy(&startTimerMutex_);
}

void psx::watchdog::Application::Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;
	*out << cgicc::caption() << std::endl;
	*out << "PSX watchdog information" << std::endl;
	*out << cgicc::caption() << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Monitored PSX service") << std::endl;
	*out << cgicc::th("PSX service name") << std::endl;
	*out << cgicc::th("Application id") << std::endl;
	*out << cgicc::th("Data point") << std::endl;
	*out << cgicc::th("Watchdog start") << std::endl;
	*out << cgicc::th("Check period (s)") << std::endl;
	*out << cgicc::th("Timeout (s)") << std::endl;
	*out << cgicc::th("Wait time (s)") << std::endl;
	*out << cgicc::th("Last ping") << std::endl;
	*out << cgicc::th("Last unsuccessful ping") << std::endl;
	*out << cgicc::th("Use kill command") << std::endl;
	if (killPsx_)
	{
		*out << cgicc::th("Kill count") << std::endl;
	}
	else
	{
		*out << cgicc::th("Restart count") << std::endl;
	}
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr() << std::endl;
	pthread_mutex_lock(&pingMutex_);
	if ( psxDesc_ != 0 )
	{
		*out << cgicc::td() << psxDesc_->getContextDescriptor()->getURL() << cgicc::td() << std::endl;
	}
	pthread_mutex_unlock(&pingMutex_);
	*out << cgicc::td() << psxServiceName_.toString() << cgicc::td() << std::endl;
	*out << cgicc::td() << psxId_ << cgicc::td() << std::endl;
	*out << cgicc::td() << dataPoint_.toString() << cgicc::td() << std::endl;
	*out << cgicc::td() << watchdogStart_.toString(toolbox::TimeVal::gmt) << cgicc::td() << std::endl;
	*out << cgicc::td() << checkPeriod_ << cgicc::td() << std::endl;
	*out << cgicc::td() << timeout_ << cgicc::td() << std::endl;
	*out << cgicc::td() << waitTime_ << cgicc::td() << std::endl;
	*out << cgicc::td();
	if ( lastPing_ != toolbox::TimeVal::zero() )
	{
		*out << lastPing_.toString(toolbox::TimeVal::gmt);
	}
	*out << cgicc::td() << std::endl;
	*out << cgicc::td();
	if ( lastUnsuccessfulPing_ != toolbox::TimeVal::zero() )
	{
		*out << lastUnsuccessfulPing_.toString(toolbox::TimeVal::gmt);
	}
	*out << cgicc::td() << std::endl;
	*out << cgicc::td() << killPsx_.toString() << cgicc::td() << std::endl;
	*out << cgicc::td() << killCount_ << cgicc::td() << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

void psx::watchdog::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	pthread_mutex_lock(&pingMutex_);

	if ( psxDesc_ == 0 )
	{
		pthread_mutex_unlock(&pingMutex_);
		std::string msg = "PSX descriptor was not created. ";
		XCEPT_RAISE(psx::watchdog::exception::Exception, msg);
	}

	toolbox::TimeVal currentTime = toolbox::TimeVal::gettimeofday();
	lastPing_ = currentTime;

	if ( !this->ping() )
	{
		lastUnsuccessfulPing_ = currentTime;
		this->kill();
		//Restarting timer in order to apply wait time and give enough time for PSX service to be restarted
		this->startWatchdogTimer(waitTime_, checkPeriod_);
	}

	pthread_mutex_unlock(&pingMutex_);
}

void psx::watchdog::Application::createPSXDescriptor(xdaq::ApplicationDescriptor*& psxDesc, xdaq::ContextDescriptor*& psxContext) throw(psx::watchdog::exception::Exception)
{
	//Deleting previous descriptor
	if ( applicationDescriptorFactory_->hasDescriptor(psxDesc) )
	{
		applicationDescriptorFactory_->destroyDescriptor(psxDesc);
		psxDesc = 0;
	}
	delete psxContext;
	psxContext = 0;

	//Creating new descriptor
	toolbox::net::URL watchdogURL(watchdogDesc_->getContextDescriptor()->getURL());
	toolbox::net::URL psxURL(watchdogURL.getProtocol(), watchdogURL.getHost(), psxPort_, "");

	psxContext = new xdaq::ContextDescriptor(psxURL.toString());
	std::set<std::string> zones;
	zones.insert(this->getApplicationContext()->getDefaultZoneName());
	std::set<std::string> groups;
	groups.insert("dcs");
	try
	{
		psxDesc = xdaq::ApplicationDescriptorFactory::getInstance()->createApplicationDescriptor(psxContext, "psx", psxId_, zones, groups);
	}
	catch (xdaq::exception::DuplicateApplicationDescriptor& e)
	{
		std::string msg = "Cannot create application descriptor for 'psx'.";
		XCEPT_RETHROW(psx::watchdog::exception::Exception, msg, e);
	}
	catch (xdaq::exception::InvalidZone& e)
	{
		std::string msg = "Cannot create application descriptor for 'psx'.";
		XCEPT_RETHROW(psx::watchdog::exception::Exception, msg, e);
	}
}

void psx::watchdog::Application::actionPerformed(xdata::Event& e)
{
	if ( e.type() == "urn:xdaq-event:setDefaultValues" )
	{
		if ( (unsigned int) psxPort_ != 0 )
		{
			//Create PSX descriptor
			try
			{
				this->createPSXDescriptor(psxDesc_, psxContext_);
			}
			catch (psx::watchdog::exception::Exception& e)
			{
				psxDesc_ = 0;
				pthread_mutex_unlock(&pingMutex_);
				std::string msg = "Cannot create PSX descriptor.";
				XCEPT_RETHROW(psx::watchdog::exception::Exception, msg, e);
			}

			//Creating pkill line
			try
			{
				pkill_ = prepareKillLine(psxDesc_, watchdogDesc_);
				restart_ = prepareRestartLine(psxDesc_, watchdogDesc_);
			}
			catch (psx::watchdog::exception::Exception& e)
			{
				pkill_ = "";
				pthread_mutex_unlock(&pingMutex_);
				std::string msg = "Cannot create a pkill command line.";
				XCEPT_RETHROW(psx::watchdog::exception::Exception, msg, e);
			}
		}
		this->startWatchdogTimer(waitTime_, checkPeriod_);
	}
}

void psx::watchdog::Application::startWatchdogTimer(const time_t& waitTime, const time_t& checkPeriod)
{
	const std::string watchdogTimerName = "PSXWatchdog";
	const std::string watchdogJobName = "PSXWatchdogJob";

	pthread_mutex_lock(&startTimerMutex_);

	toolbox::task::TimerFactory* timerFactory = toolbox::task::getTimerFactory();
	toolbox::task::Timer* timer;
	if ( timerFactory->hasTimer(watchdogTimerName) )
	{
		timer = timerFactory->getTimer(watchdogTimerName);
		try
		{
			timer->remove(watchdogJobName);
		}
		catch (...)
		{
			/*
			 * if startWatchdogTimer() is called from timeExpired() then
			 * timer->remove(watchdogJobName) throws NoJobs exception.
			 * There is no way to check if the job exists in advance.
			*/
		}
	}
	else
	{
		timer = timerFactory->createTimer(watchdogTimerName);
	}

	toolbox::TimeInterval interval(checkPeriod, 0);
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
	toolbox::TimeInterval waitInterval(waitTime, 0);
	start += waitInterval;
	timer->scheduleAtFixedRate(start, this, interval, 0, watchdogJobName);

	pthread_mutex_unlock(&startTimerMutex_);
}

void psx::watchdog::Application::createPingMessage(const xoap::MessageReference& msg)
{
	msg->getMimeHeaders()->setHeader("xdaq-pthttp-connection-timeout", timeout_.toString());

	xoap::SOAPPart soap = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	xoap::SOAPName commandName = envelope.createName("dpGet", "psx", PSX_PVSS_NS_URI);
	xoap::SOAPBodyElement commandElement = body.addBodyElement(commandName);

	xoap::SOAPName dpName = envelope.createName("dp", "psx", PSX_PVSS_NS_URI);
	xoap::SOAPElement dpElement = commandElement.addChildElement(dpName);

	xoap::SOAPName dpAttribute = envelope.createName("name", "", PSX_PVSS_NS_URI);
	dpElement.addAttribute(dpAttribute, dataPoint_);
}

bool psx::watchdog::Application::ping()
{
	bool success = true;

	xoap::MessageReference msg = xoap::createMessage();
	this->createPingMessage(msg);

	std::string message;
	msg->writeTo(message);
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Ping message: " + message);

	try
	{
		xoap::MessageReference reply = this->getApplicationContext()->postSOAP(msg, *watchdogDesc_, *psxDesc_);

		std::string message;
		reply->writeTo(message);
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Ping reply: " + message);

		xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
		xoap::SOAPBody body = envelope.getBody();

		if ( body.hasFault() )
		{
			success = false;
			std::stringstream msg;
			msg << "PSX watchdog ping was unsuccessful. ";
			msg << "For the data point: \"" << dataPoint_.toString() << "\". ";
			msg << "Reply has the following fault: ";
			msg << "fault code = \"" << body.getFault().getFaultCode() << "\", ";
			msg << "fault string = \"" << body.getFault().getFaultString() << "\".";
			XCEPT_DECLARE(psx::watchdog::exception::Exception, q, msg.str());
			this->notifyQualified("warning", q);
		}
	}
	catch (xdaq::exception::Exception& e)
	{
		success = false;
		std::stringstream msg;
		msg << "PSX watchdog ping was unsuccessful. ";
		msg << "For the data point: \"" << dataPoint_.toString() << "\".";
		XCEPT_DECLARE_NESTED(psx::watchdog::exception::Exception, q, msg.str(), e);
		this->notifyQualified("warning", q);
	}
	catch (...)
	{
		success = false;
		std::stringstream msg;
		msg << "PSX watchdog ping was unsuccessful. ";
		msg << "For the data point: \"" << dataPoint_.toString() << "\".";
		XCEPT_DECLARE(psx::watchdog::exception::Exception, q, msg.str());
		this->notifyQualified("warning", q);
	}

	return success;
}

void psx::watchdog::Application::kill()
{
	if (killPsx_)
	{
		system(pkill_.c_str());
	}
	else
	{
		system(restart_.c_str());
	}
	killCount_++;
}

std::string psx::watchdog::Application::prepareKillLine(xdaq::ApplicationDescriptor* psxDesc, const xdaq::ApplicationDescriptor* watchdogDesc) throw(psx::watchdog::exception::Exception)
{
	const std::string username = "root";
	unsigned int port;
	std::string zone;
	std::string regex;
	std::string pkill = "";
	std::string psxHostname = "";
	std::string watchdogHostname = "";

	try
	{
		toolbox::net::URL psxUrl(psxDesc->getContextDescriptor()->getURL());
		psxHostname = psxUrl.getHost();
		port = psxUrl.getPort();
	}
	catch (toolbox::net::exception::MalformedURL& e)
	{
		std::string msg = "Malformed PSX service URL.";
		XCEPT_RETHROW(psx::watchdog::exception::Exception, msg, e);
	}

	try
	{
		toolbox::net::URL watchdogUrl(watchdogDesc->getContextDescriptor()->getURL());
		watchdogHostname = watchdogUrl.getHost();
	}
	catch (toolbox::net::exception::MalformedURL& e)
	{
		std::string msg = "Malformed PSX watchdog URL.";
		XCEPT_RETHROW(psx::watchdog::exception::Exception, msg, e);
	}

	if ( toolbox::tolower(psxHostname).compare(toolbox::tolower(watchdogHostname)) == 0 )
	{
		//Get zone of PSX service
		zone = this->getApplicationContext()->getDefaultZoneName();

		std::stringstream portTokens;
		portTokens << "-p\\s+" << port;

		std::stringstream zoneTokens;
		zoneTokens << "-z\\s+" << zone;

		//Formatting PSX process recognition regex
		std::stringstream regexss;
		regexss << "xdaq\\.exe.*?";

		//Catching both possibilities of port and zone appearance in the command line
		regexss << "(";
		regexss << "\\s";
		regexss << portTokens.str();
		regexss << "\\s.*?";
		regexss << zoneTokens.str();
		regexss << "(\\s|$)";
		regexss << "|";
		regexss << "\\s";
		regexss << zoneTokens.str();
		regexss << "\\s.*?";
		regexss << portTokens.str();
		regexss << "(\\s|$)";
		regexss << ")";

		regex = regexss.str();

		pkill = "pkill -9 -u " + username + " -f \"" + regex + "\"";
	}
	else
	{
		std::string msg = "PSX watchdog should run on the same machine as PSX server.";
		XCEPT_RAISE(psx::watchdog::exception::Exception, msg);
	}

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Kill line: " + pkill);

	return pkill;
}

std::string psx::watchdog::Application::prepareRestartLine(xdaq::ApplicationDescriptor* psxDesc, const xdaq::ApplicationDescriptor* watchdogDesc) throw(psx::watchdog::exception::Exception)
{
	std::string zone;
	std::string restartline = "";
	std::string psxHostname = "";
	std::string watchdogHostname = "";
	std::string localHostname = "";

	char host_name[255];
	if (gethostname(host_name, 255) == 0)
	{
		localHostname = host_name;
	}

	try
	{
		toolbox::net::URL psxUrl(psxDesc->getContextDescriptor()->getURL());
		psxHostname = psxUrl.getHost();
	}
	catch (toolbox::net::exception::MalformedURL& e)
	{
		std::string msg = "Malformed PSX service URL.";
		XCEPT_RETHROW(psx::watchdog::exception::Exception, msg, e);
	}

	try
	{
		toolbox::net::URL watchdogUrl(watchdogDesc->getContextDescriptor()->getURL());
		watchdogHostname = watchdogUrl.getHost();
	}
	catch (toolbox::net::exception::MalformedURL& e)
	{
		std::string msg = "Malformed PSX watchdog URL.";
		XCEPT_RETHROW(psx::watchdog::exception::Exception, msg, e);
	}

	if ( toolbox::tolower(psxHostname).compare(toolbox::tolower(watchdogHostname)) == 0 )
	{
		//Get zone of PSX service
		zone = this->getApplicationContext()->getDefaultZoneName();
		restartline = "systemctl restart " + zone + "." + psxServiceName_.toString() + "@" + localHostname + ".service";
	}
	else
	{
		std::string msg = "PSX watchdog should run on the same machine as PSX server.";
		XCEPT_RAISE(psx::watchdog::exception::Exception, msg);
	}

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Restart line: " + restartline);

	return restartline;
}
