// $Id: ApplicationService.cc,v 1.15 2007/04/18 14:44:58 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/ApplicationService.h"
#include "psx/mapi/Request.h"
#include "psx/mapi/RequestFactory.h"
#include "psx/mapi/Monitor.h"
#include "psx/mapi/DisconnectRequest.h"

#include "xoap/domutils.h"
#include "xcept/tools.h"

// SMI includes

#include "dic.hxx"
#include "smixx_common.hxx"
#include "smiuirtl.hxx"

void psx::mapi::ApplicationService::init(const std::string& dnsServer)
{
	if (dnsServer != "")
	{
		DimClient::setDnsNode(dnsServer.c_str());
	}
	else
	{
		std::cout << "Missing DIMDnsNode specification in server configuration file" << std::endl;
	}
}

psx::mapi::ApplicationService::ApplicationService(psx::PeerTransportService * pts)
{
	pts_ = pts;
	
	pthread_mutexattr_init(&(this->lockAttr_));
	pthread_mutexattr_settype(&(this->lockAttr_), PTHREAD_MUTEX_RECURSIVE_NP);
	pthread_mutex_init(&(this->lock_),&(this->lockAttr_));
}

psx::mapi::ApplicationService::~ApplicationService()
{
	pthread_mutexattr_destroy(&(this->lockAttr_));
	pthread_mutex_destroy(&(this->lock_));	
}

xoap::MessageReference psx::mapi::ApplicationService::onRequest(xoap::MessageReference message) 
	throw (psx::exception::Exception)
{
	xoap::MessageReference reply = xoap::createMessage();

	psx::mapi::Request* request = 0;
        try
        {
                 request = psx::mapi::RequestFactory::createRequest(message, this);
        }
        catch (psx::mapi::exception::Exception & e )
        {
                xoap::MessageReference reply = xoap::createMessage();
                xoap::SOAPPart soap = reply->getSOAPPart();
                xoap::SOAPEnvelope envelope = soap.getEnvelope();
                xoap::SOAPBody responseBody = envelope.getBody();
                xoap::SOAPFault f = responseBody.addFault();
                f.setFaultCode("Client");
                f.setFaultString(xcept::stdformat_exception_history(e));
                return reply;                        
        }

	pthread_mutex_lock( &(this->lock_));
	
	// No exception raised -> the request was created
	if (request->type() == "connect")
        {		
		try
		{
			psx::mapi::Monitor* monObject = new psx::mapi::Monitor((psx::mapi::ConnectRequest*)request);

			if (monObject->getState() == 0)
			{
				// Object doesn't exist
				xoap::SOAPPart soap = reply->getSOAPPart();
                		xoap::SOAPEnvelope envelope = soap.getEnvelope();
                		xoap::SOAPBody responseBody = envelope.getBody();
                		xoap::SOAPFault f = responseBody.addFault();
                		f.setFaultCode("Server");
				std::string msg = "Failed to connect to non-existing object ";
				msg += request->getObjectName();
                		f.setFaultString(msg);
				delete monObject;
			}
			else
			{
				try
				{
					monObject->connect();
					this->setTransaction ( monObject->getId(), monObject );

					xoap::SOAPPart soap = reply->getSOAPPart();
					xoap::SOAPEnvelope envelope = soap.getEnvelope();
					xoap::SOAPBody responseBody = envelope.getBody();

					xoap::SOAPName response = envelope.createName("connectResponse","psx", psx::mapi::NSURI);
                			xoap::SOAPElement responseElement = responseBody.addBodyElement(response);
					xoap::SOAPName transactionId = envelope.createName ("id", "", "");				
					responseElement.addAttribute( transactionId, monObject->getId() );
				}
				catch (psx::mapi::exception::Exception& e)
				{
					// Object doesn't exist
					xoap::SOAPPart soap = reply->getSOAPPart();
                			xoap::SOAPEnvelope envelope = soap.getEnvelope();
                			xoap::SOAPBody responseBody = envelope.getBody();
                			xoap::SOAPFault f = responseBody.addFault();
                			f.setFaultCode("Server");
                			f.setFaultString( xcept::stdformat_exception_history(e) );
					delete monObject;
				}
			}
		}
		catch(std::exception & e)
		{
			// An error happened: SMI not reachable
               		xoap::SOAPPart soap = reply->getSOAPPart();
               		xoap::SOAPEnvelope envelope = soap.getEnvelope();
               		xoap::SOAPBody responseBody = envelope.getBody();
               		xoap::SOAPFault f = responseBody.addFault();
               		f.setFaultCode("Server");
			std::string msg = "Failed to perform connect command (SMI subsystem unreachable) on object ";
			msg += request->getObjectName();
               		f.setFaultString(msg);
		}
		catch (...)
		{
			// An error happened
               		xoap::SOAPPart soap = reply->getSOAPPart();
               		xoap::SOAPEnvelope envelope = soap.getEnvelope();
               		xoap::SOAPBody responseBody = envelope.getBody();
               		xoap::SOAPFault f = responseBody.addFault();
               		f.setFaultCode("Server");
			std::string msg = "Failed to perform connect command (unknown exception) on object ";
			msg += request->getObjectName();
               		f.setFaultString(msg);
		}
		
	}
	else if (request->type() == "disconnect")
        {
		try
		{
			psx::mapi::DisconnectRequest* r = dynamic_cast<psx::mapi::DisconnectRequest*>(request);
			//psx::mapi::Monitor* monObject = this->getTransaction( r->getId() );
			this->clearTransaction( r->getId() );
			//delete monObject;
			
			xoap::SOAPPart soap = reply->getSOAPPart();
			xoap::SOAPEnvelope envelope = soap.getEnvelope();
			xoap::SOAPBody responseBody = envelope.getBody();

			xoap::SOAPName response = envelope.createName("disconnectResponse","psx", psx::mapi::NSURI);
                	responseBody.addBodyElement(response);
		}
		catch (psx::mapi::exception::Exception& e)
		{
			// Transaction id doesn't exist
			xoap::SOAPPart soap = reply->getSOAPPart();
                	xoap::SOAPEnvelope envelope = soap.getEnvelope();
                	xoap::SOAPBody responseBody = envelope.getBody();
                	xoap::SOAPFault f = responseBody.addFault();
                	f.setFaultCode("Server");
                	f.setFaultString( xcept::stdformat_exception_history(e) );
		}
		
	}
	else if (request->type() == "getState")
        {
		try 
		{
			SmiObject smio((char*) request->formatObjectName().c_str());
			
			char* r = smio.getState();
			std::string state = "";
			if (r != 0)
			{
				state = r;
			}
			
			if ( state == "" ) 
			{
				// An error happened
                		xoap::SOAPPart soap = reply->getSOAPPart();
                		xoap::SOAPEnvelope envelope = soap.getEnvelope();
                		xoap::SOAPBody responseBody = envelope.getBody();
                		xoap::SOAPFault f = responseBody.addFault();
                		f.setFaultCode("Server");
				std::string msg = "Failed to perform getState command on object ";
				msg += request->getObjectName();
                		f.setFaultString(msg);
			}
			else
			{
				
				if (smio.getBusy() == 1)
				{
					state = "Busy";
				}				

				xoap::SOAPPart soap = reply->getSOAPPart();
				xoap::SOAPEnvelope envelope = soap.getEnvelope();
				xoap::SOAPBody responseBody = envelope.getBody();

				std::string responseString = request->type() + "Response";			
				xoap::SOAPName response = envelope.createName(responseString,"psx", psx::mapi::NSURI);
                		xoap::SOAPElement responseElement = responseBody.addBodyElement(response);
				responseElement.addTextNode( state );			
			}
			
		}
		catch(std::exception & e)
		{
			// An error happened: SMI not reachable
               		xoap::SOAPPart soap = reply->getSOAPPart();
               		xoap::SOAPEnvelope envelope = soap.getEnvelope();
               		xoap::SOAPBody responseBody = envelope.getBody();
               		xoap::SOAPFault f = responseBody.addFault();
               		f.setFaultCode("Server");
			std::string msg = "Failed to perform getState command (SMI subsystem unreachable) on object ";
			msg += request->getObjectName();
               		f.setFaultString(msg);
		}
		catch (...)
		{
			// An error happened
               		xoap::SOAPPart soap = reply->getSOAPPart();
               		xoap::SOAPEnvelope envelope = soap.getEnvelope();
               		xoap::SOAPBody responseBody = envelope.getBody();
               		xoap::SOAPFault f = responseBody.addFault();
               		f.setFaultCode("Server");
			std::string msg = "Failed to perform getState command (unknown exception) on object ";
			msg += request->getObjectName();
               		f.setFaultString(msg);
		}
	}
	else 
	{
		// an ordinary synchronous request
		try
		{
			
			int result = smiui_send_command_wait((char*) request->formatObjectName().c_str(), 
		           				     (char*)request->formatCommand().c_str());
			if ( result != 1)
			{
				// An error happened
                		xoap::SOAPPart soap = reply->getSOAPPart();
                		xoap::SOAPEnvelope envelope = soap.getEnvelope();
                		xoap::SOAPBody responseBody = envelope.getBody();
                		xoap::SOAPFault f = responseBody.addFault();
                		f.setFaultCode("Server");
				std::string msg = "Failed to perform command ";
				msg += request->formatCommand();
				msg += " on object ";
				msg += request->getObjectName();
                		f.setFaultString(msg);
			}
			else
			{
				xoap::SOAPPart soap = reply->getSOAPPart();
				xoap::SOAPEnvelope envelope = soap.getEnvelope();
				xoap::SOAPBody responseBody = envelope.getBody();

				std::string responseString = request->type() + "Response";			
				xoap::SOAPName response = envelope.createName(responseString,"psx", psx::mapi::NSURI);
                        	// xoap::SOAPElement responseElement = 
				responseBody.addBodyElement(response);
			}
			
			/*			
			SmiObject smio((char*) request->formatObjectName().c_str());
			
			int result = 1;
			if (smio.getState() != 0)
			{
				result = smio.sendCommand( (char*)request->formatCommand().c_str() );
				
				for (unsigned long i = 0; i < 3; i++)
				{
					if (smio.getBusy() == 1)
					{
						std::cout << "SMI Object currently busy" << std::endl;
						sleep(1);
					}
					else
					{
						break;
					}					
				}
				
				if ( result != 1)
				{
					// An error happened
                			xoap::SOAPPart soap = reply->getSOAPPart();
                			xoap::SOAPEnvelope envelope = soap.getEnvelope();
                			xoap::SOAPBody responseBody = envelope.getBody();
                			xoap::SOAPFault f = responseBody.addFault();
                			f.setFaultCode("Server");
					std::string msg = "Failed to perform command ";
					msg += request->formatCommand();
					msg += " on object ";
					msg += request->getObjectName();
                			f.setFaultString(msg);
				}
				else
				{
					xoap::SOAPPart soap = reply->getSOAPPart();
					xoap::SOAPEnvelope envelope = soap.getEnvelope();
					xoap::SOAPBody responseBody = envelope.getBody();

					std::string responseString = request->type() + "Response";			
					xoap::SOAPName response = envelope.createName(responseString,"psx", psx::mapi::NSURI);
                        		// xoap::SOAPElement responseElement = 
					responseBody.addBodyElement(response);
				}
			}
			else
			{
				// An error happened
                		xoap::SOAPPart soap = reply->getSOAPPart();
                		xoap::SOAPEnvelope envelope = soap.getEnvelope();
                		xoap::SOAPBody responseBody = envelope.getBody();
                		xoap::SOAPFault f = responseBody.addFault();
                		f.setFaultCode("Server");
				std::string msg = "Failed to communicate to SMI subsystem for command ";
				msg += request->formatCommand();
				msg += " on object ";
				msg += request->getObjectName();
                		f.setFaultString(msg);
			}
			*/		
		}
		catch(std::exception & e)
		{
			// An error happened: SMI not reachable
               		xoap::SOAPPart soap = reply->getSOAPPart();
               		xoap::SOAPEnvelope envelope = soap.getEnvelope();
               		xoap::SOAPBody responseBody = envelope.getBody();
               		xoap::SOAPFault f = responseBody.addFault();
               		f.setFaultCode("Server");
			std::string msg = "Failed to perform command (SMI subsystem unreachable) on object ";
			msg += request->getObjectName();
               		f.setFaultString(msg);
		}
		catch (...)
		{
			// An error happened
               		xoap::SOAPPart soap = reply->getSOAPPart();
               		xoap::SOAPEnvelope envelope = soap.getEnvelope();
               		xoap::SOAPBody responseBody = envelope.getBody();
               		xoap::SOAPFault f = responseBody.addFault();
               		f.setFaultCode("Server");
			std::string msg = "Failed to perform command (unknown exception) on object ";
			msg += request->getObjectName();
               		f.setFaultString(msg);
		}
	}
	pthread_mutex_unlock( &(this->lock_));
	return reply;	
}

psx::PeerTransportService * psx::mapi::ApplicationService::getPeerTransportService()
{
	return pts_;
}

std::vector<std::string> psx::mapi::ApplicationService::getTransactions()
{
	std::vector<std::string> v;
	std::map<std::string, psx::mapi::Monitor*>::iterator i;
	
	for (i = subscribers_.begin(); i != subscribers_.end(); ++i)
	{
		v.push_back( (*i).first );
	}
	return v;
}

std::string psx::mapi::ApplicationService::getTransactionUrl(const std::string& id)
	throw (psx::mapi::exception::Exception)
{
	if (this->hasTransaction(id))
	{
		psx::mapi::Monitor* m = subscribers_[id];
		return m->getURL();
	}
	else
	{
		std::string msg = "Failed to get URL for non-existing transaction id ";
		msg += id;
		XCEPT_RAISE(psx::mapi::exception::Exception, msg);
	}
}

std::string psx::mapi::ApplicationService::getTransactionOwner(const std::string& id)
	throw (psx::mapi::exception::Exception)
{
	if (this->hasTransaction(id))
	{
		psx::mapi::Monitor* m = subscribers_[id];
		return m->getOwner();
	}
	else
	{
		std::string msg = "Failed to get owner for non-existing transaction id ";
		msg += id;
		XCEPT_RAISE(psx::mapi::exception::Exception, msg);
	}
}


void psx::mapi::ApplicationService::setTransaction( const std::string& id, psx::mapi::Monitor* obj)
{
	subscribers_[id] = obj;
}


void psx::mapi::ApplicationService::clearTransaction (const std::string& id)
{
	if (subscribers_.find(id) != subscribers_.end())
	{
		psx::mapi::Monitor* monObject = this->getTransaction( id );
		delete monObject;
		subscribers_.erase(id);		
	}
}

bool psx::mapi::ApplicationService::hasTransaction (const std::string& id)
{
	return (subscribers_.find(id) != subscribers_.end() );
}

psx::mapi::Monitor* psx::mapi::ApplicationService::getTransaction(const std::string& id) throw (psx::mapi::exception::Exception)
{
	std::map<std::string, psx::mapi::Monitor*>::iterator i;
	i = subscribers_.find(id);
	if (i != subscribers_.end())
	{
		return (*i).second;
	}
	else
	{
		std::string msg = "Cannot find transaction id ";
		msg += id;
		XCEPT_RAISE(psx::mapi::exception::Exception, msg);
	}
}


void psx::mapi::ApplicationService::notify(const std::string& object,
					   const std::string& url,	
					   const std::string& action,	
					   const std::string& context,	
					   const std::string& id,	
					   const std::string& state )
{

	psx::mapi::Notification * notification = new psx::mapi::Notification(object, url, action, context, id, state);
	notificationQueue_.push(notification);

	
	
}

void* psx_mapi_application_service_svc(void* arg)
{

	psx::mapi::ApplicationService* as = (psx::mapi::ApplicationService*) arg;
	
	as->run(); // never exist until signal tells to exist (CTRL-C, kill SIGTERM)
		
	return 0;
}

void psx::mapi::ApplicationService::activate()
{
	pthread_t *thr = new pthread_t();
	pthread_create(thr, 0, psx_mapi_application_service_svc, (void*) this);
	pthread_detach(*thr);
	return;
}


void psx::mapi::ApplicationService::run()
{
	for (;;)
	{
		psx::mapi::Notification* notification = 0;
		
		try
		{
			notification = notificationQueue_.pop(0, 0);
			
			pthread_mutex_lock( &(this->lock_));
			// Process notification
			// Create SOAP reply
			xoap::MessageReference notify = xoap::createMessage();
			xoap::SOAPPart soap = notify->getSOAPPart();
			xoap::SOAPEnvelope envelope = soap.getEnvelope();
			xoap::SOAPBody responseBody = envelope.getBody();

			xoap::SOAPName notifyCommand = envelope.createName("notify","smi", psx::mapi::NSURI);
			xoap::SOAPElement responseElement = responseBody.addBodyElement(notifyCommand);

			xoap::SOAPName transactionId = envelope.createName("id","","");
  			responseElement.addAttribute(transactionId, notification->getId() );
	
			xoap::SOAPName contextName = envelope.createName("context","","");
  			responseElement.addAttribute(contextName,  notification->getContext());
	
			xoap::SOAPName objectName = envelope.createName("object","","");
  			responseElement.addAttribute(objectName,  notification->getObjectName());
	
			responseElement.addTextNode( notification->getState());

			notify->getMimeHeaders()->setHeader("SOAPAction",  notification->getAction());
	
			/*
			std::cout << std::endl;
			notify->writeTo(std::cout);
        		std::cout << std::endl;
			*/
	
			try
			{
				xoap::MessageReference reply = pts_->post( notification->getURL(), notify);
				xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
				if ( b.hasFault() )
				{
					xoap::SOAPFault f = b.getFault();
					std::cerr << "failed to notify to URL:" <<  notification->getURL() << ", " << f.getFaultString() << std::endl;		
				}
			}
			catch (psx::exception::Exception & e)
			{
				std::cerr << "failed to notify to URL:" <<  notification->getURL() << ", " << xcept::stdformat_exception_history(e) << std::endl;
			}
			pthread_mutex_unlock( &(this->lock_));
			
			delete notification;
		}
		catch (toolbox::exception::Timeout& e)
		{
			pthread_mutex_unlock( &(this->lock_));
			delete notification;
			std::cout << "Fatal error in SMI work loop, exiting..., " << e.what() << std::endl;
			return;
		}
	}
}

bool psx::mapi::ApplicationService::isConnected()
{
	int version = 0;
        DimCurrentInfo dns("DIS_DNS/VERSION_NUMBER",10,-1);
        
        version = dns.getInt();
        if(version != -1)
	{
                return true; // DNS running
	}

        return false; // an error or DNS not running
}
