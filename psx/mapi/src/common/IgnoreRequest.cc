// $Id: IgnoreRequest.cc,v 1.3 2008/04/08 13:42:30 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/IgnoreRequest.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::IgnoreRequest::IgnoreRequest (psx::mapi::ApplicationService * as) : psx::mapi::Request("ignore",as)
{
	owner_ = "";
}

std::string psx::mapi::IgnoreRequest::formatObjectName()
{
	 std::string format = "";
        if (domain_ != "")
        {
                format = domain_;
        }
        else
        {
                format = name_;
        }

        format += "::";
        format += name_;
        format += "_FWM";
        return format;
}


std::string psx::mapi::IgnoreRequest::formatCommand()
{
	std::string format = "IGNORE";
	
	
	if (owner_ != "")
	{
		format += "/OWNER=\"";
		format += owner_;
		format += "\"";
	}
	
	return format;
}



