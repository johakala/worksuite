// $Id: GetStateRequest.cc,v 1.4 2006/07/27 16:30:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/GetStateRequest.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::GetStateRequest::GetStateRequest (psx::mapi::ApplicationService * as) : psx::mapi::Request("getState",as)
{
	owner_ = "";
}

std::string psx::mapi::GetStateRequest::formatObjectName()
{
	std::string format = "";
	if (domain_ != "")
	{
		format = domain_;
	}
	else
	{
		format = name_;
	}
	format += "::";
	format += name_;
	return format;
}


std::string psx::mapi::GetStateRequest::formatCommand()
{
	return "";
}



