// $Id: ConnectRequest.cc,v 1.2 2008/04/08 13:42:30 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/ConnectRequest.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::ConnectRequest::ConnectRequest (psx::mapi::ApplicationService * as) : psx::mapi::Request("connect",as)
{
	owner_ = "";
	url_ = "";
	context_ = "";
	action_ = "";
}

void psx::mapi::ConnectRequest::setURL(const std::string & url )
{
	url_ = url;
}
                        
void psx::mapi::ConnectRequest::setContext(const std::string & context)
{
	context_ = context;
}

void psx::mapi::ConnectRequest::setAction(const std::string & action)
{
	action_ = action;
}

std::string psx::mapi::ConnectRequest::getURL()
{
	return url_;
}

std::string psx::mapi::ConnectRequest::getContext()
{
	return context_;
}

std::string psx::mapi::ConnectRequest::getAction()
{
	return action_;
}

std::string psx::mapi::ConnectRequest::formatObjectName()
{
 	std::string format = "";
        if (domain_ != "")
        {
                format = domain_;
        }
        else
        {
                format = name_;
        }

        format += "::";
        format += name_;
        return format;

}

std::string psx::mapi::ConnectRequest::formatCommand()
{
	std::string format = "";
	
	/*
	if (exclusive_)
	{
		format += "/EXCLUSIVE=\"yes\"";
	}
	
	if (owner_ != "")
	{
		format += "/OWNER=\"";
		format += owner_;
		format += "\"";
	}
	*/
	
	return format;
}



