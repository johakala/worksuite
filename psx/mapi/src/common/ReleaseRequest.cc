// $Id: ReleaseRequest.cc,v 1.3 2008/04/08 13:42:29 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/ReleaseRequest.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::ReleaseRequest::ReleaseRequest (psx::mapi::ApplicationService * as) : psx::mapi::Request("release",as)
{
	owner_ = "";
	applyToChildren_ = false;
}

void psx::mapi::ReleaseRequest::applyToChildren(bool all)
{
	applyToChildren_ = all;
}

std::string psx::mapi::ReleaseRequest::formatObjectName()
{
	 std::string format = "";
        if (domain_ != "")
        {
                format = domain_;
        }
        else
        {
                format = name_;
        }

        format += "::";
        format += name_;
        format += "_FWM";
        return format;
}


std::string psx::mapi::ReleaseRequest::formatCommand()
{
	std::string format = "RELEASE";
	if ( applyToChildren_ )
	{
		format = "RELEASEALL";
	}
	
	
	if (owner_ != "")
	{
		format += "/OWNER=\"";
		format += owner_;
		format += "\"";
	}
	
	return format;
}



