// $Id: TakeRequest.cc,v 1.3 2008/04/08 13:42:29 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/TakeRequest.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::TakeRequest::TakeRequest (psx::mapi::ApplicationService * as) : psx::mapi::Request("take",as)
{
	exclusive_ = false;
	owner_ = "";
}

void psx::mapi::TakeRequest::setExclusive(bool exclusive)
{
	exclusive_ = exclusive;
}

std::string psx::mapi::TakeRequest::formatObjectName()
{
	 std::string format = "";
        if (domain_ != "")
        {
                format = domain_;
        }
        else
        {
                format = name_;
        }

        format += "::";
        format += name_;
	format += "_FWM";
        return format;
}

std::string psx::mapi::TakeRequest::formatCommand()
{
	std::string format = "TAKE";
	
	if (exclusive_)
	{
		format += "/EXCLUSIVE=\"yes\"";
	}
	
	if (owner_ != "")
	{
		format += "/OWNER=\"";
		format += owner_;
		format += "\"";
	}
	
	return format;
}



