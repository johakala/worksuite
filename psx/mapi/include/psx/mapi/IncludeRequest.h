// $Id: IncludeRequest.h,v 1.1 2006/02/15 11:01:45 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_mapi_IncludeRequest_h_
#define _psx_mapi_IncludeRequest_h_

#include <string>
#include <set>

#include "psx/mapi/Request.h"
     
namespace psx {
	namespace mapi {
	
		class ApplicationService;
		
		class IncludeRequest : public psx::mapi::Request
		{
      			public:
			
			IncludeRequest( psx::mapi::ApplicationService * as);
			
			
			std::string formatCommand();
			std::string formatObjectName();


		
		};
	}
}

#endif 

