// $Id: Notification.h,v 1.1 2006/02/17 13:19:17 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_mapi_Notification_h_
#define _psx_mapi_Notification_h_

#include <string>
     
namespace psx {
	namespace mapi {
		class Notification 
		{
      			public:
			
			Notification(const std::string& object,
					const std::string& url,
					const std::string& action,
					const std::string& context,
					const std::string& id,
					const std::string& state);
						
			std::string getURL();
			std::string getAction();
			std::string getContext();
			std::string getState();
			std::string getId();			
			std::string getObjectName();

			private:

			std::string object_;
			std::string url_;
			std::string action_;
			std::string context_;
			std::string id_;
			std::string state_;
		};
	}
}

#endif 

