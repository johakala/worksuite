// $Id: Request.h,v 1.2 2006/07/27 16:30:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_mapi_Request_h_
#define _psx_mapi_Request_h_

#include <string>
#include <set>
     
namespace psx {
	namespace mapi {
	
		class ApplicationService;
		
		class Request 
		{
      			public:
			
			Request(const std::string & type, psx::mapi::ApplicationService * as) ;
			virtual ~Request();
		
			virtual std::string type();
			
			virtual psx::mapi::ApplicationService * getApplicationService();
			
			/*! The domain name may be empty. In this case the formatted
                           object name is OBJ::OBJ, otherwise it is DOMAIN::OBJ
                        */
                        void setDomainName(const std::string & name);
                        std::string getDomainName();

			void setObjectName(const std::string & name);
			std::string getObjectName();
			
			void setOwner(const std::string & name);
			std::string getOwner();
			
			virtual std::string formatCommand() = 0;
			virtual std::string formatObjectName() = 0;
			
			protected:
			
			std::string type_;
			psx::mapi::ApplicationService * as_;
			std::string domain_;
			std::string name_;
			std::string owner_;
		};
	}
}

#endif 

