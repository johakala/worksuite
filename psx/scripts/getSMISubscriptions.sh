#!/bin/sh
curl --stderr /dev/null \
-H "SOAPAction: urn:xdaq-application:service=psx" \
-d "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" 
  xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" 
  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
<SOAP-ENV:Header>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
	<psx:getSMISubscriptions xmlns:psx=\"http://xdaq.cern.ch/xdaq/xsd/2018/psx-meta.xsd\"></psx:getSMISubscriptions>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>" $1

echo ""
