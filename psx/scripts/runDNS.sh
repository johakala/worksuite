#!/bin/sh
if [ 1 = `ps -fu$USER | grep dns | grep -v grep |wc -l` ] 
then
# then dns is already running ABORT this run
echo
echo "DIM DNS already running"
exit 3
else
export LD_LIBRARY_PATH=${XDAQ_ROOT}/daq/extern/dim/dim/linux
${XDAQ_ROOT}/daq/extern/dim/dim/linux/dns > dns.log &
fi
