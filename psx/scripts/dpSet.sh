#!/bin/sh
curl --stderr /dev/null \
-H "SOAPAction: urn:xdaq-application:service=psx" \
-d "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" 
  xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" 
  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
<SOAP-ENV:Header>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
	<psx:dpSet xmlns:psx=\"http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd\">
		<psx:dp name=\"$1\">$2</psx:dp>
        </psx:dpSet>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>" $3

echo ""
