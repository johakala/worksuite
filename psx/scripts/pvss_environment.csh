setenv PATH ${PVSS_II_HOME}/bin:${PATH}
setenv API_ROOT $PVSS_II_HOME/api
setenv PLATFORM `${API_ROOT}/platform`
setenv LD_LIBRARY_PATH ${PVSS_II_HOME}/bin:$API_ROOT/lib.${PLATFORM}
setenv LD_LIBRARY_PATH ${XDAQ_ROOT}/daq/extern/dim/x86_slc3/lib:$LD_LIBRARY_PATH
setenv LD_LIBRARY_PATH ${XDAQ_ROOT}/daq/extern/smi/x86_slc3/lib:$LD_LIBRARY_PATH
setenv LD_LIBRARY_PATH ${XDAQ_ROOT}/daq/psx/sapi/lib/linux/x86:$LD_LIBRARY_PATH
setenv LD_LIBRARY_PATH ${XDAQ_ROOT}/daq/psx/mapi/lib/linux/x86:$LD_LIBRARY_PATH
