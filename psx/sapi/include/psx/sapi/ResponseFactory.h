// $Id: ResponseFactory.h,v 1.1 2006/02/06 15:52:46 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_ResponseFactory_h_
#define _psx_sapi_ResponseFactory_h_

#include "xoap/MessageReference.h"
#include "psx/sapi/Result.h"
#include "psx/sapi/exception/Exception.h"


namespace psx {
	namespace sapi {
	
		class ResponseFactory
		{
			public:
			
				static xoap::MessageReference createResponse (psx::sapi::Result * result) 
					throw (psx::sapi::exception::Exception);
		};
	}
}

#endif
