// $Id: VariableFactory.h,v 1.3 2006/02/06 15:53:04 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _psx_sapi_VariableFactory_h_
#define _psx_sapi_VariableFactory_h_

#include "Variable.hxx" 

#include "psx/sapi/exception/Exception.h"

#include <string>

namespace psx {
	namespace sapi {
	
		class VariableFactory
		{
			public:
			
			static Variable* createVariable(VariableType vt, const std::string& val)
				throw (psx::sapi::exception::Exception);
				
			static std::string toString(Variable * v);

		};
	
	}
}

#endif
