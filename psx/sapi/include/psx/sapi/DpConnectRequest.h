// $Id: DpConnectRequest.h,v 1.2 2006/02/07 11:11:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DpConnectRequest_h_
#define _psx_sapi_DpConnectRequest_h_

#include <string>
#include <set>

#include "psx/sapi/Request.h"
     
namespace psx {
	namespace sapi {
	
		class ApplicationService;
		
		class DpConnectRequest : public psx::sapi::Request
		{
      			public:
			
			DpConnectRequest( psx::sapi::ApplicationService * as);
			
			std::set<std::string>  & getDpNames();
			
			void addDpName(std::string & name);

			void setURL(const std::string & url );
			
			void setContext(const std::string & context);
			
			void setAction(const std::string & action);
			
			std::string getURL();
			
			std::string getContext();
			
			std::string getAction();
						
			private:
			
			std::set<std::string> dpnames_;
			std::string context_;
			std::string url_;
			std::string action_;
		
		};
	}
}

#endif 

