// $Id: DpDisconnectHotlink.h,v 1.1 2006/02/02 08:04:19 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DpDisconnectHotlink_h_
#define _psx_sapi_DpDisconnectHotlink_h_

#include "psx/sapi/Hotlink.h"
     
namespace psx {
	namespace sapi {
	
		
		class DpDisconnectHotlink:  public Hotlink
		{
      			public:
			
			DpDisconnectHotlink(psx::sapi::Request * request) ;
			~DpDisconnectHotlink();
					
			void hotLinkCallBack(DpMsgAnswer &answer);
			
	 		void hotLinkCallBack(DpHLGroup &group);
			
			
		};
	}
}

#endif 

