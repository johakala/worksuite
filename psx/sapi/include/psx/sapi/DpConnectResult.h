// $Id: DpConnectResult.h,v 1.2 2006/02/02 12:57:23 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DpConnectResult_h_
#define _psx_sapi_DpConnectResult_h_

#include <string>
#include <map>

#include "psx/sapi/Result.h"
     
namespace psx {
	namespace sapi {
	
		class Hotlink;
		
		class DpConnectResult : public psx::sapi::Result
		{
      			public:
			
			DpConnectResult(psx::sapi::Request * request);
		
			std::map<std::string, std::string, std::less<std::string> > &  getData();
			void setTransactionId (const std::string& id);
			std::string& getTransactionId ();
			
			void setTransactionObject(psx::sapi::Hotlink* hotlink);
			psx::sapi::Hotlink* getTransactionObject();
			
			private:
			
			std::map<std::string, std::string, std::less<std::string> > dpValues_;
			std::string id_; // transaction id	
			psx::sapi::Hotlink* hotlink_;		
		};
	}
}

#endif 

