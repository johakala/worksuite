// $Id: Subscriber.h,v 1.1 2006/01/30 08:23:56 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_Subscriber_h_
#define _psx_sapi_Subscriber_h_

     
namespace psx {
	namespace sapi {
	
		
		class Subscriber 
		{
      			public:
			
			Subscriber() 
			{
			
			}
		
			
			
			private:
			
			
		};
	}
}

#endif 

