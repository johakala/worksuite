// $Id: DpGetHotlink.h,v 1.1 2006/02/01 10:34:12 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DpGetHotlink_h_
#define _psx_sapi_DpGetHotlink_h_

#include "psx/sapi/Hotlink.h"
     
namespace psx {
	namespace sapi {
	
		
		class DpGetHotlink:  public Hotlink
		{
      			public:
			
			DpGetHotlink(psx::sapi::Request * request) ;
					
			void hotLinkCallBack(DpMsgAnswer &answer);
			
	 		void hotLinkCallBack(DpHLGroup &group);
			
			
		};
	}
}

#endif 

