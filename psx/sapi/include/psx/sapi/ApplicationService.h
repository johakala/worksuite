// $Id: ApplicationService.h,v 1.19 2007/04/18 14:44:58 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_ApplicationService_h_
#define _psx_sapi_ApplicationService_h_

#include <sys/time.h>
#include <string>
#include <map>
#include <errno.h>
#include <iostream>
#include <signal.h>

#include "Manager.hxx"        // include/Manager
#include "DpIdentifier.hxx"   // include/Basics
#include "FloatVar.hxx"               

#include "HotLinkWaitForAnswer.hxx"   // include/Manager
#include "StartDpInitSysMsg.hxx"      // include/Messages
#include "DpMsgAnswer.hxx"            // include/Messages
#include "DpMsgHotLink.hxx"           // include/Messages
#include "DpHLGroup.hxx"              // include/Basics
#include "DpVCItem.hxx"               // include/Basics
#include "ErrHdl.hxx"                 // include/Basics
#include "ErrClass.hxx"              // include/Basics

#include "toolbox/SyncQueue.h"

#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"

#include "psx/PeerTransportService.h"
#include "psx/ApplicationService.h"
#include "psx/sapi/Request.h"
#include "psx/sapi/Result.h"
#include "psx/sapi/exception/Exception.h"
    
namespace psx {
	namespace sapi {
	
		class Hotlink;
	
		const std::string NSURI = "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd";
		
		/*! Class that implements the PVSS API Manager to access data points
		*/
		class ApplicationService: public Manager, public psx::ApplicationService
		{
      			public:
			
			ApplicationService(psx::PeerTransportService * pts);
			virtual ~ApplicationService();
			
			//! Process an incoming SOAP request
			//
			virtual xoap::MessageReference onRequest(xoap::MessageReference msg)
				throw (psx::exception::Exception);
			
			virtual psx::PeerTransportService * getPeerTransportService();

			/*! Enqueue a result object that can be taken in onRequest to create a SOAP reply
			 * Does not throw, if an error happens internally, needs to be logged
			 */
			void acknowledge(psx::sapi::Result* result);

			/*! Pass a result set to send a SOAP message with data to a subscriber
			 * Does not throw, if an error happens internally, needs to be logged
			 */
			void notify(psx::sapi::Result* result);
			
			//! Submit a request to be handled by PVSS. A response is returned via the wait function
			//
			void submit(psx::sapi::Request* request)
				throw (psx::sapi::exception::Exception);

			/*! Wait for a result as a response to a request
			 * If a timeout occurs, the Result* returned is 0
			 */
			psx::sapi::Result* wait(psx::sapi::Request* request, time_t timeout);
			
			/*! Initialization reading the commandline parameters or configuration file information
			 *  This must be done only once
			 */
			static void init(const std::string & projectName , const std::string & projectNum,
				const std::string & databaseManager , const std::string & eventManager);
			
			//! Create the dispatching thread that communicated with PVSS and call \function svc() internally
			//
			void activate(); 
			
			//! Called in the svc thread routine when \function activate() is called from outside
			//
			void run();
			
			void setTransaction( const std::string& id, psx::sapi::Hotlink* obj);
			
			void clearTransaction (const std::string& id);
			
			bool hasTransaction (const std::string& id);
			
			//! Retrieves the list of all transaction identifiers
			//
			std::vector<std::string> getTransactions();
			
			/*! Retrieves the URL for which a transaction identifier has been set
			 *  \throws an exception if the \param id does not exist
			*/
			std::string getTransactionUrl(const std::string& id)
				throw (psx::sapi::exception::Exception);
			
			/*! Check if the connection to the database manager stands
			    \returns true if the connection is up, false otherwise
			 */
			bool isDataManagerConnected();
			
			/*! Check if the connection to the event manager stands
			    \returns true if the connection is up, false otherwise
			 */
			bool isEventManagerConnected();
			
			/*! Returns for a transaction identifier a previously registered Hotlink object.
			    If the transaction has not been registered, the function throws an exception
			 */
			psx::sapi::Hotlink* getTransaction(const std::string& id)
				throw (psx::sapi::exception::Exception);


			/*! Return the fields of a datapoint, throws if the data point name isn't found */			
			std::set<std::string> getDataPointDefinition(const std::string & name)
				throw (psx::sapi::exception::Exception);
			
			// callback invoked from signal handler
			//
    			virtual void signalHandler(int sig);
			// static void  staticSignalHandler(int sig);			
				    
			private:
			
			// Mutex used to synchronize access between PVSS API functions and the dispatch loop
			pthread_mutex_t mutex_;
			pthread_mutexattr_t mutexAttr_;
			
			// Mutex used to serialize SOAP requests
			pthread_mutex_t lock_;
			pthread_mutexattr_t lockAttr_;			

			static int our_argc_; // number of variables passed to PVSS Resource::init
			static char** our_argv_; // parameters passed to PVSS Resource::init
			
			// Used to send SOAP commands to subscribers on dpConnect requests
			psx::PeerTransportService * pts_;
			
			// Holds the results of completed requests
			toolbox::SyncQueue<psx::sapi::Result*> requestCompletionQueue_;
			
			// The signal handler will set this flag to PVSS_TRUE if the process has to terminate
			static PVSSboolean doExit;
			
			// Holds subscribers of DpConnect requests. The key is a transaction identifier
			// currently maps to (unsigned long) of the Hotlink pointer
			std::map<std::string, psx::sapi::Hotlink*> subscribers_;			
			
			// Recursive internal helper function for getDataPointDefinition
			//
			void addNodeDefinitionToList(
				const DpIdentifier * identifier,
				DpType* dpType,
				DpTypeId dptid, 
				DpIdentification * idtf, 
				DpTypeContainer* container, 
				const DpTypeNode * dptn, 
				std::set<std::string> & names,  
				const std::string & fathername);
			
		};
	}
}

#endif 

