// $Id: DpConnectHotlink.h,v 1.2 2006/02/02 12:57:23 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DpConnectHotlink_h_
#define _psx_sapi_DpConnectHotlink_h_

#include "psx/sapi/Hotlink.h"
     
namespace psx {
	namespace sapi {
	
		
		class DpConnectHotlink:  public Hotlink
		{
      			public:
			
			DpConnectHotlink(psx::sapi::Request * request) ;
			~DpConnectHotlink();
					
			void hotLinkCallBack(DpMsgAnswer &answer);
			
	 		void hotLinkCallBack(DpHLGroup &group);
			
			private:
			
			std::string id_;
		};
	}
}

#endif 

