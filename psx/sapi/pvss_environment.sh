setenv PATH /home/PVSS/pvss2_v3.0/bin:${PATH}
setenv LD_LIBRARY_PATH /home/PVSS/pvss2_v3.0/bin:${LD_LIBRARY_PATH}
setenv PVSS_II_HOME /home/PVSS/pvss2_v3.0
setenv PVSS_II ~/psx/config/config 
setenv API_ROOT $PVSS_II_HOME/api
setenv PLATFORM linux_RH90
setenv LD_LIBRARY_PATH $API_ROOT/lib.${PLATFORM}:${LD_LIBRARY_PATH}
