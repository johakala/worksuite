// $Id: DpGetFieldsRequest.cc,v 1.1 2006/02/04 09:03:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpGetFieldsRequest.h"
#include "psx/sapi/ApplicationService.h"
     

psx::sapi::DpGetFieldsRequest::DpGetFieldsRequest (psx::sapi::ApplicationService * as) : psx::sapi::Request("dpGetFields",as)
{
	name_ = "*";
}

void psx::sapi::DpGetFieldsRequest::setName(const std::string& name)
{
	name_ = name;
}

const char* psx::sapi::DpGetFieldsRequest::getName()
{
	return name_.c_str();	
}


