// $Id: Request.cc,v 1.3 2006/05/05 13:30:14 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/Request.h"
#include "psx/sapi/ApplicationService.h"
     

psx::sapi::Request::Request(const std::string & type, psx::sapi::ApplicationService * as) : type_(type), as_(as)
{
	ack_ = true;
}

psx::sapi::Request::~Request()
{

}

std::string psx::sapi::Request::type()
{
	return type_;
}

psx::sapi::ApplicationService * psx::sapi::Request::getApplicationService()
{
	return as_;
}

void psx::sapi::Request::setAck(bool ack)
{
	ack_ = ack;
}

toolbox::net::UUID psx::sapi::Request::getUUID()
{
	return uuid_;
}
bool psx::sapi::Request::getAck()
{
	return ack_;
}

