// $Id: DpGetHotlink.cc,v 1.5 2006/02/09 13:47:51 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <strstream>

#include "psx/sapi/DpGetHotlink.h"
#include "psx/sapi/DpGetResult.h"
#include "psx/sapi/Utils.h"
#include "psx/sapi/VariableFactory.h"
#include "psx/sapi/exception/Exception.h"
#include "xdaq/Application.h"

psx::sapi::DpGetHotlink::DpGetHotlink(psx::sapi::Request * request) : psx::sapi::Hotlink(request)
{

}

void psx::sapi::DpGetHotlink::hotLinkCallBack(DpMsgAnswer &answer)
{
	psx::sapi::DpGetResult * result = new psx::sapi::DpGetResult(request_);
	
	// MsgType msgType = answer.isAnswerOn();	
	// std::cout << "Entering hotlink callback: " << Msg::getMsgName(msgType) << std::endl;
	const AnswerGroup* group = answer.getFirstGroup();
   	std::map<std::string, std::string, std::less<std::string> > & dpValues = result->getData();
    while ( group != 0 )
	{		
		ErrClass* err = group->getErrorPtr();
		if (err != 0)
		{
			/* Message discarded due to an active/passive switch
			   In this particular case DpGetHotlink object will be reused by WinCC_OA a second time (hotLinkCallBack will be called again)
			   to confirm data point set was successful */
			if ( (err->getErrorType() == ErrClass::ERR_REDUNDANCY) && (err->getErrorId() == ErrClass::REDU_CHANGES_ABORTED) )
			{
				xdaq::Application * application;
				application = dynamic_cast<xdaq::Application *>(request_->getApplicationService()->getPeerTransportService());
				std::stringstream msg;
				msg << "Applying redundancy: errType = " << err->getErrorType() << " errCode = " << err->getErrorId() << " errPrio = " << err->getPriority() << " err = '" << err->getErrorText().c_str();
				XCEPT_DECLARE(psx::sapi::exception::Exception, e, msg.str());
				application->notifyQualified("warning", e);
				return;
			}

                        std::stringstream msg;
                        msg << "Encountered an error."
                            << " Type: '" << ErrTypeToString(err->getErrorType())
                            << "', ID: " << err->getErrorId()
                            << ", message: '" << err->getErrorText().c_str() << "'";
                        result->setError(msg.str());
			break; // out of the loop
		}
		else
		{		
			AnswerItem* item = group->getFirstItem();
			while( item != 0 )
			{
				// Extract Data Point dpID and dpName
				DpIdentifier dpId = item->getDpIdentifier();		
				char * dpname;
				Manager::getName(dpId, dpname);

				std::string dpName = dpname;
				delete [] dpname;
				Variable *value = item->getValuePtr();
 				if (value)      // could be NULL !!
    				{
                                  dpValues[dpName] = toolbox::quote(psx::sapi::VariableFactory::toString(value));
    				}
				else
				{
					std::string msg = "Cannot retrieve value for data point ";
					msg += dpName;
					result->setError(msg.c_str());
					break; // out of the loop
				}
				

				item = group->getNextItem();
			}
			group = answer.getNextGroup();
		}
	}

	request_->getApplicationService()->acknowledge(result);
}

void psx::sapi::DpGetHotlink::hotLinkCallBack(DpHLGroup &group)
{
	// never used
}
			


