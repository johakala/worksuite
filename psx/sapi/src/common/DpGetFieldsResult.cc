// $Id: DpGetFieldsResult.cc,v 1.1 2006/02/04 09:03:38 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpGetFieldsResult.h"
 
psx::sapi::DpGetFieldsResult::DpGetFieldsResult(psx::sapi::Request *request) : psx::sapi::Result(request)
{

}


std::set<std::string> &  psx::sapi::DpGetFieldsResult::getData()
{
	return dpFields_;
}

