// $Id: DpConnectHotlink.cc,v 1.6 2006/02/09 13:47:51 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/



#include "psx/sapi/DpConnectHotlink.h"
#include "psx/sapi/DpConnectResult.h"
#include "psx/sapi/VariableFactory.h"
#include "psx/sapi/Utils.h"
#include "psx/sapi/exception/Exception.h"
#include "xdaq/Application.h"

psx::sapi::DpConnectHotlink::DpConnectHotlink(psx::sapi::Request * request) : psx::sapi::Hotlink(request)
{
	// Convert this pointer to a transaction identifier
	//char oid[char(2*sizeof(unsigned int)+1)]; // +1 for \0 at the end
	//snprintf (oid, 2*sizeof(unsigned int), "%x", (unsigned int) this);
	//id_ = oid;	
	
	std::stringstream val;
	val << std::hex << (void*)this << std::dec;
	id_ = val.str();
}

psx::sapi::DpConnectHotlink::~DpConnectHotlink()
{
	request_->getApplicationService()->clearTransaction(id_);
	delete request_;

}

void psx::sapi::DpConnectHotlink::hotLinkCallBack(DpMsgAnswer &answer)
{
	psx::sapi::DpConnectResult * result = new psx::sapi::DpConnectResult(request_);
	
	std::map<std::string, std::string, std::less<std::string> > & dpValues = result->getData();

	// MsgType msgType = answer.isAnswerOn();
	//std::cout << "Entering DPConnect hotlink callback: " << Msg::getMsgName(msgType) << std::endl;
	const AnswerGroup* group = answer.getFirstGroup();
    	while ( group != 0 )
	{		
		ErrClass* err = group->getErrorPtr();
		if (err != 0)
		{
			/* Message discarded due to an active/passive switch
			   In this particular case DpConnectHotlink object will be reused by WinCC_OA a second time (hotLinkCallBack will be called again)
			   to confirm data point set was successful */
			if ( (err->getErrorType() == ErrClass::ERR_REDUNDANCY) && (err->getErrorId() == ErrClass::REDU_CHANGES_ABORTED) )
			{
				xdaq::Application * application;
				application = dynamic_cast<xdaq::Application *>(request_->getApplicationService()->getPeerTransportService());
				std::stringstream msg;
				msg << "Applying redundancy: errType = " << err->getErrorType() << " errCode = " << err->getErrorId() << " errPrio = " << err->getPriority() << " err = '" << err->getErrorText().c_str();
				XCEPT_DECLARE(psx::sapi::exception::Exception, e, msg.str());
				application->notifyQualified("warning", e);
				return;
			}
                        std::stringstream msg;
                        msg << "Encountered an error."
                            << " Type: '" << ErrTypeToString(err->getErrorType())
                            << "', ID: " << err->getErrorId()
                            << ", message: '" << err->getErrorText().c_str() << "'";
			result->setError(msg.str());
			break; // out of the loop
		}
		else
		{		
			AnswerItem* item = group->getFirstItem();
			while( item != 0 )
			{
				// Extract Data Point dpID and dpName
				DpIdentifier dpId = item->getDpIdentifier();		
				char * dpname;
				Manager::getName(dpId, dpname);
				std::string dpName = dpname;
				delete [] dpname;
				Variable *value = item->getValuePtr();
 				if (value)      // could be NULL !!
    				{				
					dpValues[dpName] = psx::sapi::VariableFactory::toString(value);
    				}
				else
				{
					std::string msg = "Cannot retrieve value for data point ";
					msg += dpName;
					result->setError(msg.c_str());
					break; // out of the loop
				}

				item = group->getNextItem();
			}
			group = answer.getNextGroup();
		}
	}
	
	result->setTransactionId( id_ );
	result->setTransactionObject( this );

	if (!result->hasError())
	{
		// only register the transaction if there was no error
		request_->getApplicationService()->setTransaction(id_, this);
	}
	
	request_->getApplicationService()->acknowledge(result);

}

/* Called each time the subscribed datapoint is changed */
void psx::sapi::DpConnectHotlink::hotLinkCallBack(DpHLGroup &group)
{
	psx::sapi::DpConnectResult * result = new psx::sapi::DpConnectResult(request_);
	
	result->setTransactionId( id_ );
	result->setTransactionObject( this );

	std::map<std::string, std::string, std::less<std::string> > & dpValues = result->getData();

  	// A group consists of pairs of DpIdentifier and values called items.
  	// There is exactly one item for all configs we are connected.
  	for (DpVCItem *item = group.getFirstItem(); item; item = group.getNextItem())
        {	
		DpIdentifier dpId = item->getDpIdentifier();		
		char * dpname;
		Manager::getName(dpId, dpname);
		std::string dpName = dpname;
		delete [] dpname;
		Variable *value = item->getValuePtr();
    		if (value)      // could be NULL !!
    		{
      			dpValues[dpName] = psx::sapi::VariableFactory::toString(value);
    		}
		else
		{
			std::string msg = "Cannot retrieve value for data point ";
			msg += dpName;
			result->setError(msg.c_str());
			break; // out of the loop
		}
        }
	
	request_->getApplicationService()->notify(result);
}
			


