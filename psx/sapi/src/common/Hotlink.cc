// $Id: Hotlink.cc,v 1.4 2006/02/01 10:34:58 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/



#include "psx/sapi/Hotlink.h"

psx::sapi::Hotlink::Hotlink(psx::sapi::Request * request) : request_(request)
{

}

psx::sapi::Request * psx::sapi::Hotlink::getRequest()
{
	return request_;
}

	


