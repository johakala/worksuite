// $Id: DpGetNamesRequest.cc,v 1.1 2006/02/02 08:05:24 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpGetNamesRequest.h"
#include "psx/sapi/ApplicationService.h"
     

psx::sapi::DpGetNamesRequest::DpGetNamesRequest (psx::sapi::ApplicationService * as) : psx::sapi::Request("dpGetNames",as)
{
	pattern_ = "*";
}

void psx::sapi::DpGetNamesRequest::setPattern(const std::string& pattern)
{
	pattern_ = pattern;
}

const char* psx::sapi::DpGetNamesRequest::getPattern()
{
	return pattern_.c_str();	
}


