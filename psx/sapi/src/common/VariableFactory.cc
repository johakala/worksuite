// $Id: VariableFactory.cc,v 1.5 2007/04/19 15:20:49 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdlib.h>
 
#include "psx/sapi/VariableFactory.h"
#include "toolbox/string.h"

#include "DpElement.hxx"
#include "UIntegerVar.hxx"
#include "TextVar.hxx"
#include "IntegerVar.hxx"
#include "FloatVar.hxx"
#include "CharVar.hxx"
#include "BlobVar.hxx"
#include "BitVar.hxx"
#include "Bit32Var.hxx"
#include "DynVar.hxx"

Variable* psx::sapi::VariableFactory::createVariable(VariableType vt, const std::string& val)
	throw (psx::sapi::exception::Exception)
{
	switch (vt)
	{
		case CHAR_VAR:
			return new CharVar((val.c_str())[0]);
		break;
		
		case UINTEGER_VAR:
		{
			unsigned long tmp = strtoul(val.c_str(), 0, 10);
			return new UIntegerVar(tmp);
		}
		break;
		
		case INTEGER_VAR:
		{
			unsigned long tmp = strtol(val.c_str(), 0, 10);
			return new IntegerVar(tmp);
		}
		break;
		
		case FLOAT_VAR:
		{
			float tmp = strtof(val.c_str(), 0);
			return new FloatVar (tmp);
		}
		break;

		case TEXT_VAR:
		{
			return new TextVar(val.c_str());
		}
		break;

		case BLOB_VAR:
		{
			// PVSS_TRUE means copying the data in
			return new BlobVar ((PVSSuchar *)val.c_str(), val.size(), PVSS_TRUE);
		}
		break;
		
		case BIT_VAR:
		{
			// PVSS_TRUE means copying the data in
			if ((toolbox::tolower(val) == "true")
                            || (val == "1"))
			{
				return new BitVar ( PVSS_TRUE);
			}
			else if ((toolbox::tolower(val) == "false")
                                 || (val == "0"))
			{
				return new BitVar ( PVSS_FALSE);
			}
			else
			{
				std::string msg = "Invalid boolean value ";
				msg += val;
				XCEPT_RAISE (psx::sapi::exception::Exception, msg);
			}
		}
		break;
		
		case DYNINTEGER_VAR:
		{
			DynVar * res = new DynVar(INTEGER_VAR);
			std::list<std::string> vals = toolbox::parseTokenList(val, "|");
			for (std::list<std::string>::iterator it = vals.begin(); it != vals.end(); it++)
			{
				unsigned long tmp = strtol((*it).c_str(), 0, 10);
				res->append(new IntegerVar(tmp));
			}
			return res;
		}
		break;

		default:
		{
			std::stringstream msg;
			msg <<  "Could not create variable of type: " << Variable::getTypeName(vt) << " #value: 0x" <<  std::hex << (unsigned long) vt << std::dec << " from value: " << val;
			XCEPT_RAISE (psx::sapi::exception::Exception, msg.str());
		}
		break;
	};
}

std::string psx::sapi::VariableFactory::toString(Variable * v)
{
	VariableType dpt = v->isA();
	CharString value;
	switch (dpt)
	{
		case FLOAT_VAR              : 
			value = v->formatValue("%f");
		break;

		case NO_VAR                 : 
		case NOTYPE_VAR             : 
		case VARIABLE               : 
		case TIME_VAR               : 
		case BIT_VAR                : 
		case INTEGER_VAR            : 
		case UINTEGER_VAR           : 
		case TEXT_VAR               : 
		case BIT32_VAR              : 
		case CHAR_VAR               : 
		case DPIDENTIFIER_VAR       : 
		case DYN_VAR                : 
		case DYNTIME_VAR            : 
		case DYNBIT_VAR             : 
		case DYNINTEGER_VAR         : 
		case DYNUINTEGER_VAR        : 
		case DYNFLOAT_VAR           : 
		case DYNTEXT_VAR            : 
		case DYNBIT32_VAR           : 
		case DYNCHAR_VAR            : 
		case DYNDPIDENTIFIER_VAR    : 
		case REC_VAR                : 
		case FILE_VAR               : 
		case ANYTYPE_VAR            : 
		case ARG_VAR                : 
		case DYNDYNTIME_VAR         : 
		case DYNDYNBIT_VAR          : 
		case DYNDYNINTEGER_VAR      : 
		case DYNDYNUINTEGER_VAR     : 
		case DYNDYNFLOAT_VAR        : 
		case DYNDYNTEXT_VAR         : 
		case DYNDYNBIT32_VAR        : 
		case DYNDYNCHAR_VAR         : 
		case DYNDYNDPIDENTIFIER_VAR :
		case DYNANYTYPE_VAR         : 
		case DYNDYNANYTYPE_VAR      : 
		case EXTTIME_VAR            : 
		case DYNEXTTIME_VAR         : 
		case DYNDYNEXTTIME_VAR      : 
		case LANGTEXT_VAR           : 
		case DYNLANGTEXT_VAR        : 
		case DYNDYNLANGTEXT_VAR     : 
		case ERROR_VAR              : 
		case DYNERROR_VAR           : 
		case DYNDYNERROR_VAR        : 
		case BLOB_VAR               : 
		case DYNBLOB_VAR            : 
		case RECHDL_VAR            : 
		case CONNHDL_VAR           : 
		case CMDHDL_VAR            : 
		case POINTER_VAR           : 
		case SHAPE_VAR             : 
		case IDISPATCH_VAR         : 
		case DYNDYNBLOB_VAR        : 
		case DYNREC_VAR            : 
		case DYNDYNREC_VAR         : 
		case MAPPING_VAR           : 
		case DYNMAPPING_VAR        : 
		case DYNDYNMAPPING_VAR     : 
		case MIXED_VAR             : 
		case DYNMIXED_VAR          : 
		case DYNDYNMIXED_VAR       : 
		default:
			 value =v->formatValue(""); // unknown type
		break;
	}
	return (char*)value;

}

