// $Id: DpSetRequest.cc,v 1.1 2006/02/01 10:34:12 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpSetRequest.h"
#include "psx/sapi/ApplicationService.h"
     

psx::sapi::DpSetRequest::DpSetRequest (psx::sapi::ApplicationService * as) 
	: psx::sapi::Request("dpSet",as)
{

}

std::map<std::string, std::string>& psx::sapi::DpSetRequest::getDpValues()
{
	return dpvalues_;
}

void psx::sapi::DpSetRequest::addDpValue(const std::string & name, const std::string & value)
{
	dpvalues_[name] = value;
}


