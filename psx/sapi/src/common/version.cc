// $Id: version.cc,v 1.1 2007/04/18 14:44:59 lorsini Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/version.h"
#include "config/version.h"
#include "xgi/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(psxsapi)

void psxsapi::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(xgi);  
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xdata);  
	CHECKDEPENDENCY(xdaq);  
}

std::set<std::string, std::less<std::string> > psxsapi::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,xgi);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xdaq);
	 
	return dependencies;
}	
	
