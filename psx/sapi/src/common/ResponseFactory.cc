// $Id: ResponseFactory.cc,v 1.5 2006/05/05 11:15:50 xdaq Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdlib.h>
 
#include "psx/sapi/ApplicationService.h"
#include "psx/sapi/DpSetResult.h"
#include "psx/sapi/DpConnectResult.h"
#include "psx/sapi/DpGetResult.h"
#include "psx/sapi/DpGetNamesResult.h"
#include "psx/sapi/DpGetAllSystemsResult.h"
#include "psx/sapi/DpGetFieldsResult.h"
#include "psx/sapi/Result.h"
#include "psx/sapi/DpDisconnectResult.h"
#include "psx/sapi/ResponseFactory.h"

#include "xoap/domutils.h"
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/MessageReference.h"
xoap::MessageReference psx::sapi::ResponseFactory::createResponse(psx::sapi::Result * result /*, psx::sapi::ApplicationService* as*/) 
	throw (psx::sapi::exception::Exception)
{
	// Create SOAP reply
	xoap::MessageReference reply = xoap::createMessage();
        xoap::SOAPPart soap = reply->getSOAPPart();
        xoap::SOAPEnvelope envelope = soap.getEnvelope();
        xoap::SOAPBody responseBody = envelope.getBody();
     

	if (result != 0)
	{
		if ( result->hasError() )
		{
			xoap::SOAPFault f = responseBody.addFault();
			f.setFaultCode("Server");
			f.setFaultString(result->getError());
			delete result->getRequest();
			delete result;

		}
		else  if ( result->getRequest()->type() == "dpGet" )
		{
			   xoap::SOAPName response = envelope.createName("dpGetResponse","psx", psx::sapi::NSURI);
       			   xoap::SOAPElement responseElement = responseBody.addBodyElement(response);
			
			   psx::sapi::DpGetResult* r = dynamic_cast<psx::sapi::DpGetResult*>(result);
			   std::map<std::string, std::string, std::less<std::string> > & dpValues = r->getData();
			   for ( std::map<std::string, std::string, std::less<std::string> >::iterator i =  dpValues.begin(); i !=  dpValues.end(); i++ )
			   {
				//std::cout <<  "Retrieved dpName: " << (*i).first << " value: " << (*i).second << std::endl;
				xoap::SOAPName dpTag = envelope.createName("dp","psx", psx::sapi::NSURI);
				xoap::SOAPElement dpElement = responseElement.addChildElement(dpTag);
				xoap::SOAPName dpName = envelope.createName("name");
				dpElement.addAttribute(dpName, (*i).first);
				dpElement.addTextNode((*i).second);
				
			   }
			   delete result->getRequest();
			   delete result;
	
		}
		else if ( result->getRequest()->type() == "dpSet" )
		{
			   xoap::SOAPName response = envelope.createName("dpSetResponse","psx", psx::sapi::NSURI);
       			   /*xoap::SOAPElement responseElement = */responseBody.addBodyElement(response);
			
			  // psx::sapi::DpSetResult* r = dynamic_cast<psx::sapi::DpSetResult*>(result);
			 
			  delete result->getRequest();
			  delete result;
		}
		else if ( result->getRequest()->type() == "dpGetNames" )
		{
			   xoap::SOAPName response = envelope.createName("dpGetNamesResponse","psx", psx::sapi::NSURI);
       			   xoap::SOAPElement responseElement = responseBody.addBodyElement(response);
			
			   psx::sapi::DpGetNamesResult* r = dynamic_cast<psx::sapi::DpGetNamesResult*>(result);
			   std::set<std::string>& dpNames = r->getData();
			   for ( std::set<std::string>::iterator i = dpNames.begin(); i != dpNames.end(); i++)
			   {
				xoap::SOAPName dpTag = envelope.createName("dp","psx", psx::sapi::NSURI);
				xoap::SOAPElement dpElement = responseElement.addChildElement(dpTag);
				xoap::SOAPName dpName = envelope.createName("name");
				dpElement.addAttribute(dpName, (*i));				
			   }
			   
			   delete result->getRequest();
			   delete result;
		}
		else if ( result->getRequest()->type() == "dpConnect" )
		{
			// DP Connect response
		
			xoap::SOAPName response = envelope.createName("dpConnectResponse","psx", psx::sapi::NSURI);
			xoap::SOAPElement responseElement = responseBody.addBodyElement(response);

			psx::sapi::DpConnectResult* r = dynamic_cast<psx::sapi::DpConnectResult*>(result);
			std::map<std::string, std::string, std::less<std::string> > & dpValues = r->getData();
			for ( std::map<std::string, std::string, std::less<std::string> >::iterator i =  dpValues.begin(); i !=  dpValues.end(); i++ )
			{
				//std::cout <<  "Retrieved dpName: " << (*i).first << " value: " << (*i).second << std::endl;
				xoap::SOAPName dpTag = envelope.createName("dp","psx", psx::sapi::NSURI);
				xoap::SOAPElement dpElement = responseElement.addChildElement(dpTag);
				xoap::SOAPName dpName = envelope.createName("name");
				dpElement.addAttribute(dpName, (*i).first);
				dpElement.addTextNode((*i).second);
			}

			xoap::SOAPName transactionId = envelope.createName("id","","");
  			responseElement.addAttribute(transactionId, r->getTransactionId() );

			delete result;			
		}	
		else if ( result->getRequest()->type() == "dpDisconnect" )
		{
			   xoap::SOAPName response = envelope.createName("dpDisconnectResponse","psx", psx::sapi::NSURI);
       			   /*xoap::SOAPElement responseElement =  */responseBody.addBodyElement(response);
			
			  // psx::sapi::DpDisconnectResult* r = dynamic_cast<psx::sapi::DpDisconnectResult*>(result);
			   
			   // Diconnect succesfull therefore clear subscriber list
			   //psx::sapi::DpDisconnectRequest * request = dynamic_cast<psx::sapi::DpDisconnectRequest*>(r->getRequest());
			
			   
			   delete result->getRequest();
			   delete result;
			
		}		
		 else if ( result->getRequest()->type() == "dpGetFields" )
                {          xoap::SOAPName response = envelope.createName("dpGetFieldsResponse","psx", psx::sapi::NSURI);
                           xoap::SOAPElement responseElement = responseBody.addBodyElement(response);

                           psx::sapi::DpGetFieldsResult* r = dynamic_cast<psx::sapi::DpGetFieldsResult*>(result);
                           std::set<std::string>& dpNames = r->getData();
                           for ( std::set<std::string>::iterator i = dpNames.begin(); i != dpNames.end(); i++)
                           {
                                xoap::SOAPName dpTag = envelope.createName("dp","psx", psx::sapi::NSURI);
                                xoap::SOAPElement dpElement = responseElement.addChildElement(dpTag);
                                xoap::SOAPName dpName = envelope.createName("name");
                                dpElement.addAttribute(dpName, (*i));
                           }

                           delete result->getRequest();
                           delete result;
                }
		else if ( result->getRequest()->type() == "dpGetAllSystems" )
                {
                           xoap::SOAPName response = envelope.createName("dpGetAllSystemsResponse","psx", psx::sapi::NSURI);
                           xoap::SOAPElement responseElement = responseBody.addBodyElement(response);

                           psx::sapi::DpGetAllSystemsResult* r = dynamic_cast<psx::sapi::DpGetAllSystemsResult*>(result);
                           std::set<std::string>& sysNames = r->getData();
                           for ( std::set<std::string>::iterator i = sysNames.begin(); i != sysNames.end(); i++)
                           {
                                xoap::SOAPName sysTag = envelope.createName("sys","psx", psx::sapi::NSURI);
                                xoap::SOAPElement sysElement = responseElement.addChildElement(sysTag);
                                xoap::SOAPName sysName = envelope.createName("name");
                                sysElement.addAttribute(sysName, (*i));
                           }

                           delete result->getRequest();
                           delete result;
                }


	}
	else
	{
		xoap::SOAPFault f = responseBody.addFault();
		f.setFaultCode("Server");
		f.setFaultString("request timeout, result is empty");
	}

	
	return reply;
}
