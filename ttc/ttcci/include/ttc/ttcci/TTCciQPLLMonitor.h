#ifndef _ttc_ttcci_ttcciqpllmonitor_h_
#define _ttc_ttcci_ttcciqpllmonitor_h_


#include "ttc/monitoring/QPLLMonitor.h"


namespace ttc
{

class TTCciControl;
class TTCciProxy;

class TTCciQPLLMonitor : public QPLLMonitor
{

public:

  TTCciQPLLMonitor(xdaq::Application* app);

protected:

  //! implements ttc::MonitoringListener::addComponents
  virtual void addComponents();

  //! implements ttc::QPLLMonitor::setQPLLLockingState
  virtual void setQPLLLockingState(const Component& component, QPLLLockingState& qpllLockingState);

  //! reimplements ttc::QPLLMonitor::setQPLLLockingStateLatched
  virtual void setQPLLLockingStateLatched(const Component& component, QPLLLockingState& qpllLockingStateLatched);

private:

  //! gets the TTCciControl (casts app to concrete type)
  TTCciControl& getTTCciControl();

  //! gets the TTCci locking proxy object
  TTCciProxy& getTTCciProxy();
};

}


#endif
