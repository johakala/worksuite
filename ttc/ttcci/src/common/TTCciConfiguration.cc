#include "ttc/ttcci/TTCciConfiguration.hh"

#include "ttc/ttcci/TTCci.hh"
#include "ttc/utils/LockMutex.hh"
#include "ttc/utils/ConfigurationItem.hh"
#include "ttc/utils/RAMTriggers.hh"

#include <boost/lexical_cast.hpp>

using namespace std;
using namespace ttc::TTCciConfigWords;


// classes representing TTCci configuration items

namespace ttc
{

//! The 'mother' of all TTCci configuration items
class TTCciConfigurationItem : public ConfigurationItem
{
public:
  TTCciConfigurationItem(TTCci& _ttcci) :
      ttcci(_ttcci)
  {
    logger_ = log4cplus::Logger::getInstance("TTCciConfigurationItem");
  }

protected:
  TTCci &ttcci;

};


class TTCciConfigurationItem_ClockSource : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_ClockSource(TTCci &_ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = NumberOfWords(current_line) - 1;
    int Npars_ok = 0;

    vector<string> values = ttcci.GetSourceListNames("Clock");

    bool found = false;
    for (size_t k = 0; k < values.size(); ++k)
    {
      if (string_val == values[k])
      {
        LOG4CPLUS_INFO(
            logger_,
            "Line " << line_number+1 << ":\t " << varname << " will be set to " << "'" << string_val << "'");

        found = true;
        ++Npars_ok;

        ttcci.SelectClock(INTERFACE(string_val));

        string parname;
        bool isinternal = true;
        if (string_val != INAME(INTERNAL))
        {
          isinternal = false;
          if (FindString(current_line, (parname = WORD_QPLLRESET), string_val))
          {
            ++Npars_ok;
            if (string_val == "YES" || string_val == "Yes" || string_val == "yes")
            {
              ttcci.ResetQPLL(true);
              ttcci.configuringResetsQPLL_ = true;
            }
            else if (string_val == "NO" || string_val == "No" || string_val == "no")
            {
              ttcci.ResetQPLL(false);
              ttcci.configuringResetsQPLL_ = false;
            }
            else
            {
              ostringstream msg;
              msg << "Unknown parameter '" << string_val << "' for " << parname << " in line " << line_number+1;
              XCEPT_RAISE(xcept::Exception, msg.str());
            }
          }

          if (FindString(current_line, (parname = WORD_QPLLAUTORESTART), string_val))
          {
            ++Npars_ok;
            if (string_val == "YES" || string_val == "Yes" || string_val == "yes" || string_val == "on"
                || string_val == "ON" || string_val == "On")
            {
              ttcci.AutoRestartQPLL(true);
            }
            else if (string_val == "NO" || string_val == "No" || string_val == "no" || string_val == "off"
                || string_val == "OFF" || string_val == "Off")
            {
              ttcci.AutoRestartQPLL(false);
            }
            else
            {
              ostringstream msg;
              msg << "Unknown parameter '" << string_val << "' for " << parname << " in line " << line_number+1;
              XCEPT_RAISE(xcept::Exception, msg.str());
            }
          }
        }
        else
        {
          // Just for parameter counting.
          if (FindString(current_line, (parname = WORD_QPLLRESET), string_val))
          {
            ++Npars_ok;
          }
          if (FindString(current_line, (parname = WORD_QPLLAUTORESTART), string_val))
          {
            ++Npars_ok;
          }
        }

        if (FindString(current_line, (parname = WORD_QPLLFREQBITS), string_val))
        {
          uint32_t ulong_val;

          if (GetUnsignedLong(current_line, parname, ulong_val))
          {
            LOG4CPLUS_INFO(
                logger_,
                "TTCci::Configure(): " << "line" << line_number+1 << " setting QPLL frequency bits to 0x" << hex << ulong_val);

            ++Npars_ok;
            ulong_val = (ulong_val & (isinternal ? 0x3f : 0xf));
            ttcci.SetQPLLFrequencyBits(ulong_val, !isinternal);
          }
        }
      }
    }

    if (!found)
    {
      ostringstream msg;
      msg << "Unknown value '" << string_val << "' for " << varname <<" in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " <<dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class TTCciConfigurationItem_TriggerSource : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_TriggerSource(TTCci &_ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    // Trigger inputs.
    int Npars_tot = NumberOfWords(current_line) - 1;
    int Npars_ok = 0;

    vector<string> string_vec;
    bool found = FindStringVector(current_line, WORD_TRIGGERSOURCE, string_vec);

    // This was checked already by the function calling us here.
    assert(found);

    vector<ExternalInterface> itf;
    vector<string> interfaces = ttcci.GetSourceListNames("Trigger");
    vector<string>::iterator myitf;
    for (size_t kk = 0; kk < interfaces.size(); ++kk)
    {
      if ((myitf = find(string_vec.begin(), string_vec.end(), interfaces[kk])) != string_vec.end())
      {
        itf.push_back(INTERFACE((*myitf)));
        ++Npars_ok;
      }
    }

    ttcci.SelectTrigger(itf);

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class TTCciConfigurationItem_BGOSource : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_BGOSource(TTCci& _ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    vector<string> string_vec;
    int Npars_tot = 0, Npars_ok = 0;

    bool found = FindStringVector(current_line, WORD_BGOSOURCE, string_vec);
    assert(found);

    // BGO source.
    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    vector<ExternalInterface> itf;
    vector<string> interfaces = ttcci.GetSourceListNames("BGO");
    vector<string>::iterator myitf;
    for (size_t kk = 0; kk < interfaces.size(); ++kk)
    {
      if ((myitf = find(string_vec.begin(), string_vec.end(), interfaces[kk])) != string_vec.end())
      {
        itf.push_back(INTERFACE((*myitf)));
        ++Npars_ok;
      }
    }

    ttcci.SelectBGOSource(itf);

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }

};


class TTCciConfigurationItem_CyclicGen : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_CyclicGen(TTCci& _ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    // Cyclic generators.
    bool trigger = false;
    if (FindString(current_line, (varname = WORD_CYCLICGEN_TRIGGER), string_val))
    {
      trigger = true;
    }
    else
    {
      varname = WORD_CYCLICGEN_BGO;
      trigger = false;
    }

    int Npars_tot = 0, Npars_ok = 0;
    uint32_t ulong_val;

    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 1;

    size_t id = 0;
    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      id = size_t(ulong_val);

      if ((trigger && id >= ttcci.NCyclicTrigger()) || (!trigger && id >= ttcci.NCyclicBGO()))
      {
        ostringstream msg;
        msg << "Invalid value '" << ulong_val << "' for " << varname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }
    else
    {
      ostringstream msg;
      msg << "Unknown/invalid value '" << string_val << "' for " << varname << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    CyclicTriggerOrBGO* cycl = ttcci.GetCyclic(trigger, id);

    string parname;

    cycl->SetStartBX(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_STARTBX), ulong_val))
    {
      cycl->SetStartBX(ulong_val);
      ++Npars_ok;
    }

    cycl->SetPrescale(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_PRESCALE), ulong_val))
    {
      cycl->SetPrescale(ulong_val);
      ++Npars_ok;
    }

    cycl->SetInitialPrescale(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_INITPRECALE), ulong_val))
    {
      cycl->SetInitialPrescale(ulong_val);
      ++Npars_ok;
    }

    cycl->SetPostscale(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_POSTSCALE), ulong_val))
    {
      cycl->SetPostscale(ulong_val);
      ++Npars_ok;
    }

    cycl->SetPause(0);
    if (GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_PAUSE), ulong_val))
    {
      cycl->SetPause(ulong_val);
      ++Npars_ok;
    }

    cycl->SetRepetitive(true);
    if (FindString(current_line, (parname = WORD_CYCLICGEN_REPETITIVE), string_val))
    {
      ++Npars_ok;
      if (string_val == "y" || string_val == "Y" || string_val == "yes" || string_val == "YES" || string_val == "Yes")
      {
        cycl->SetRepetitive(true);
      }
      else if (string_val == "n" || string_val == "N" || string_val == "no" || string_val == "NO" || string_val == "No")
      {
        cycl->SetRepetitive(false);
      }
      else
      {
        ostringstream msg;
        msg << "Unknown/invalid value '" << parname << string_val << "' for " << varname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    cycl->SetPermanent(false);
    if (FindString(current_line, (parname = WORD_CYCLICGEN_PERMANENT), string_val))
    {
      ++Npars_ok;
      if (string_val == "y" || string_val == "Y" || string_val == "yes" || string_val == "YES" || string_val == "Yes")
      {
        cycl->SetPermanent(true);
      }
      else if (string_val == "n" || string_val == "N" || string_val == "no" || string_val == "NO" || string_val == "No")
      {
        cycl->SetPermanent(false);
      }
      else
      {
        ostringstream msg;
        msg << "Unknown/invalid value '" << parname << string_val << "' for " << varname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (FindString(current_line, (parname = WORD_CYCLICGEN_CHANNEL), string_val))
    {
      ++Npars_ok;
      if (trigger)
      {
        ostringstream msg;
        msg << "Invalid parameter '" << parname << string_val << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
      else
      {
        bool foundch = false;
        for (size_t ib = 0; ib < ttcci.NChannels(); ++ib)
        {
          if (ttcci.bgo[ib].MatchesNameOrAlternative(string_val, true))
          {
            foundch = true;
            cycl->SetBChannel(ib);
            break;
          }
        }

        if (!foundch && GetUnsignedLong(current_line, (parname = WORD_CYCLICGEN_CHANNEL), ulong_val))
        {
          if (ulong_val < ttcci.NChannels())
          {
            cycl->SetBChannel(ulong_val);
            foundch = true;
          }
        }

        if (!foundch)
        {
          ostringstream msg;
          msg << "Unknown/invalid value '" << parname << string_val << "' for " << varname << " in line " << line_number+1;
          XCEPT_RAISE(xcept::Exception, msg.str());
        }
      }
    }

    string dummy;
    if (!FindString(current_line, "DISABLE", dummy) && !FindString(current_line, "Disable", dummy)
        && !FindString(current_line, "disable", dummy))
    {
      cycl->SetEnable();
    }
    else
    {
      cycl->SetEnable(false);
      ++Npars_ok;
    }

    ttcci.WriteCyclicGeneratorToTTCci(trigger, id);

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class TTCciConfigurationItem_OrbitSource : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_OrbitSource(TTCci& _ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    // Orbit source.
    int Npars_tot = 0, Npars_ok = 0;
    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 1;

    vector<ExternalInterface> itf = ttcci.GetSourceList("Orbit");
    if (find(itf.begin(), itf.end(), INTERFACE(string_val)) == itf.end())
    {
      --Npars_ok;

      ostringstream msg;
      msg << "Unknown/invalid value '" << string_val << "' for " << varname << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    ttcci.SelectOrbitSource(INTERFACE(string_val));

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class TTCciConfigurationItem_TriggerInterval : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_TriggerInterval(TTCci& _ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    // Trigger delays for internal trigger.
    int Npars_tot = 0, Npars_ok = 0;
    uint32_t ulong_val;

    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;

    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      LOG4CPLUS_INFO(
          logger_,
          "Line " << line_number+1 << ":\t " << varname << " will be set to '" << string_val << "'");

      ++Npars_ok;
      ttcci.GetRAMTriggers()->triggerdelay.push_back((ulong_val & 0x3ffff));
      ttcci.GetRAMTriggers()->DirectlyWriteTriggerDPRAM = true;
    }
    else
    {
      ostringstream msg;
      msg << "Unknown value '" << string_val << "' for " << varname << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class TTCciConfigurationItem_TriggerFrequency : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_TriggerFrequency(TTCci& _ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    // Fixed trigger frequency.
    double frequency = -1.0;
    bool random = false;

    int Npars_tot = 0, Npars_ok = 0;
    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;

    double double_val;
    if (GetDouble(current_line, varname, double_val))
    {
      LOG4CPLUS_INFO(
          logger_,
          "Line " << line_number+1 << ":\t " << varname << " will be set to " << double_val << " Hz");

      ++Npars_ok;
      frequency = double_val;
    }
    else
    {
      ostringstream msg;
      msg << "Unknown value '" << string_val << "' for " << varname << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    string parname;
    if (FindString(current_line, (parname = WORD_TRIGGER_FREQUENCY_MODE), string_val))
    {
      ++Npars_ok;
      if (string_val == "RANDOM" || string_val == "Random" || string_val == "random")
      {
        random = true;
      }
      else if (string_val == "EQUI" || string_val == "equi" || string_val == "Equi")
      {
        random = false;
      }
      else
      {
        LOG4CPLUS_WARN(
            logger_,
            "Unknown parameter '" << string_val << "' for " << parname << " in line " << line_number+1 << ". "
            "Assuming 'EQUI' instead.");
      }
    }

    if (!ttcci.GetRAMTriggers()->CanSetInternalTriggerFrequency())
    {
      ostringstream msg;
      msg << "Trying to set INTERNAL trigger freq. " << "in line " << dec << line_number+1 <<", but conflict with key word " << WORD_TRIGGER_INTERVAL << ". "
          << "You can EITHER set the internal trigger " << "with a fixed freq. using the key word '" << WORD_TRIGGER_FREQUENCY << "' "
          << "or you can configure it directly through intervals using '" << WORD_TRIGGER_INTERVAL << "'";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    ttcci.GetRAMTriggers()->SetInternalTriggerFrequency(frequency, random);

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class TTCciConfigurationItem_BGOChannelSetup : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_BGOChannelSetup(TTCci& _ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    int Npars_tot = 0, Npars_ok = 0;
    // BGO data.
    Npars_tot = NumberOfWords(current_line) - 1;
    Npars_ok = 0;
    uint32_t chan = 0;
    BGODataLength myL = SINGLE;
    bool repetitive = true;
    uint32_t delay1 = 0, delay2 = 0;
    uint32_t prescale = 0, initprescale = 0, postscale = 0;

    bool foundch = false;
    uint32_t ulong_val;

    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      foundch = true;
    }
    else if (FindString(current_line, varname, string_val))
    {
      for (size_t ib = 0; ib < ttcci.NChannels(); ++ib)
      {
        if (ttcci.bgo[ib].MatchesNameOrAlternative(string_val, true))
        {
          foundch = true;
          ulong_val = ib;
          break;
        }
      }
    }

    if (!foundch)
    {
      ostringstream msg;
      msg << "Unknown value '" << string_val << "' for " << varname << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    chan = ulong_val;
    ++Npars_ok;
    if (chan >= ttcci.NChannels())
    {
      ostringstream msg;
      msg << "Invalid Channel Number " << chan;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    string parname;
    if (FindString(current_line, (parname = WORD_BGOCHANNEL_SETUP_PRESCALE), string_val))
    {
      ++Npars_ok;
      if (GetUnsignedLong(current_line, parname, ulong_val))
      {
        prescale = ulong_val;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (FindString(current_line, (parname = WORD_BGOCHANNEL_SETUP_INITPRESCALE), string_val))
    {
      ++Npars_ok;
      if (GetUnsignedLong(current_line, parname, ulong_val))
      {
        initprescale = ulong_val;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (FindString(current_line, (parname = WORD_BGOCHANNEL_SETUP_POSTSCALE), string_val))
    {
      ++Npars_ok;
      if (GetUnsignedLong(current_line, parname, ulong_val))
      {
        postscale = ulong_val;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (FindString(current_line, (parname = WORD_BGOCHANNEL_SETUP_L), string_val))
    {
      ++Npars_ok;
      if (string_val == "SINGLE" || string_val == "Single" || string_val == "single")
      {
        myL = SINGLE;
      }
      else if (string_val == "DOUBLE" || string_val == "Double" || string_val == "double")
      {
        myL = DOUBLE;
      }
      else if (string_val == "BLOCK" || string_val == "Block" || string_val == "block")
      {
        myL = BLOCK;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (FindString(current_line, (parname = WORD_BGOCHANNEL_SETUP_MODE), string_val))
    {
      ++Npars_ok;
      if (string_val == "YES" || string_val == "Yes" || string_val == "yes")
      {
        repetitive = true;
      }
      else if (string_val == "NO" || string_val == "No" || string_val == "no")
      {
        repetitive = false;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (FindString(current_line, (parname = WORD_BGOCHANNEL_SETUP_DELAY1), string_val))
    {
      ++Npars_ok;
      if (GetUnsignedLong(current_line, parname, ulong_val))
      {
        delay1 = ulong_val;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (FindString(current_line, (parname = WORD_BGOCHANNEL_SETUP_DELAY2), string_val))
    {
      ++Npars_ok;
      if (GetUnsignedLong(current_line, parname, ulong_val))
      {
        delay2 = ulong_val;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    LOG4CPLUS_INFO(
        logger_,
        "TTCci::Configure(): " << "Settings for BGO-channel #" << chan << ":" << endl
        << "      data length " << WORD_BGOCHANNEL_SETUP_L << (myL==SINGLE ? "SINGLE" : (myL==DOUBLE ? "DOUBLE" : "BLOCK")) << "," << endl
        << "      " << WORD_BGOCHANNEL_SETUP_MODE << (repetitive ? "YES" : "NO") << ", T1=" << delay1 << ", T2=" << delay2
        << "      prescale =  0x" << hex << prescale << " = " << dec << prescale << ", postscale = 0x" << hex << postscale << " = " << dec << postscale << endl
        << "      init-prescale (offset) =  0x" << hex << initprescale << " = " << dec << initprescale);

    ttcci.bgo[chan].Set(myL, repetitive, delay1, delay2);
    ttcci.bgo[chan].SetPreAndInitPrescale(prescale, initprescale);
    ttcci.bgo[chan].SetPostscale(postscale);

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok << " out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }

};


class TTCciConfigurationItem_BGOChannelData : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_BGOChannelData(TTCci& _ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    bool has_individual_b_postscale_numbers = ttcci.HasIndividualBPostScaleNumbers();

    // BGO-data for each channel.
    int Npars_tot = NumberOfWords(current_line) - 1;
    int Npars_ok = 0;

    uint32_t chan = 0;
    bool shortword = true;
    bool acommand = false;
    uint32_t mydata = 0;
    bool foundch = false;

    uint32_t ulong_val;
    uint32_t post_scale_value = 0;

    if (GetUnsignedLong(current_line, varname, ulong_val))
    {
      foundch = true;
    }
    else if (FindString(current_line, varname, string_val))
    {
      for (size_t ib = 0; ib < ttcci.NChannels(); ++ib)
      {
        if (ttcci.bgo[ib].MatchesNameOrAlternative(string_val, true))
        {
          foundch = true;
          ulong_val = ib;
          break;
        }
      }
    }

    if (!foundch)
    {
      ostringstream msg;
      msg << "Line " << line_number+1 << ":\t " << varname << ": unknown '" << string_val << "'. "
          << "What should be the channel number? [0, 15].";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    ++Npars_ok;
    chan = ulong_val;
    if (chan >= ttcci.NChannels())
    {
      ostringstream msg;
      msg << "Invalid Channel Number " << chan;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    string parname;
    if (FindString(current_line, (parname = WORD_BGOCHANNEL_DATA_MODE), string_val))
    {
      ++Npars_ok;
      if (string_val == "S" || string_val == "s")
      {
        shortword = true;
      }
      else if (string_val == "L" || string_val == "l")
      {
        shortword = false;
      }
      else if (string_val == "A" || string_val == "a")
      {
        shortword = true;
        acommand = true;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }

    if (GetUnsignedLong(current_line, (parname = WORD_BGOCHANNEL_DATA_POSTSCALE), ulong_val))
    {
      if (!has_individual_b_postscale_numbers)
      {
        ostringstream msg;
        msg << "this TTCci does not support individual postscale values "
            << "(parameter " << WORD_BGOCHANNEL_DATA_POSTSCALE << " in command " << WORD_BGOCHANNEL_DATA << ")" << " "
            << "in line " << line_number+1;

        XCEPT_RAISE(xcept::Exception, msg.str());
      }

      post_scale_value = ulong_val;
      ++Npars_ok;
    }

    if (GetUnsignedLong(current_line, (parname = WORD_BGOCHANNEL_DATA_DATA), ulong_val))
    {
      ++Npars_ok;
      mydata = ulong_val;
    }
    else
    {
      ostringstream msg;
      msg << "No DATA=xx found in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    LOG4CPLUS_INFO(
        logger_,
        "TTCci::Configure(): " << "New data word for channel" << dec << chan << ":" << endl << " data = 0x" << hex << mydata << " (=" << dec << mydata << ") " << "as " << (shortword ? "SHORT" : "LONG") << ", " << endl << "      A-Command=" << (acommand ? "true" : "false"));
    ttcci.bgo[chan].PushBackData(mydata, shortword, acommand, false /* true = do not transmit */);

    if (has_individual_b_postscale_numbers)
    {
      assert(ttcci.bgo[chan].NWords() >= 2);
      // Note that we need to set the value for index NWords() - 2
      // since NWords() - 1 -- the last word -- is the 'end of bgo
      // content word' and NOT the word we've just added.
      ttcci.bgo[chan].SetIndividualPostScaleValue(ttcci.bgo[chan].NWords() - 2, post_scale_value);
    }

    if (Npars_tot != Npars_ok)
    {
      ostringstream msg;
      msg << "Unrecognized parameters for keyword '" << varname << "' in line " << dec << line_number+1
          << " (Recognized only " << Npars_ok <<" out of " << Npars_tot << " parameters)";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }
};


class TTCciConfigurationItem_BData : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_BData(TTCci& _ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    uint32_t bdata = 0;
    bool benable = true, bshort = true;

    uint32_t ulong_val;

    if (GetUnsignedLong(current_line, (varname = WORD_BDATA), ulong_val))
    {
      bdata = ulong_val;
    }
    else
    {
      ostringstream msg;
      msg << "No Bdata set after keyword " << WORD_BDATA << " in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
    string parname;
    if (FindString(current_line, (parname = WORD_BDATA_ENABLE), string_val))
    {
      if (string_val == "TRUE" || string_val == "true" || string_val == string("1"))
      {
        benable = true;
      }
      else if (string_val == "FALSE" || string_val == "false" || string_val == string("0"))
      {
        benable = false;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }
    else
    {
      ostringstream msg;
      msg << "Problem in line " << line_number+1 << " while looking for something like '" << parname << "TRUE'";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    if (FindString(current_line, (parname = WORD_BDATA_L), string_val))
    {
      if (string_val == "SHORT" || string_val == "short" || string_val == "Short")
      {
        bshort = true;
      }
      else if (string_val == "LONG" || string_val == "flong" || string_val == "Long")
      {
        bshort = false;
      }
      else
      {
        ostringstream msg;
        msg << "Unknown value '" << string_val << "' for " << parname << " in line " << line_number+1;
        XCEPT_RAISE(xcept::Exception, msg.str());
      }
    }
    else
    {
      ostringstream msg;
      msg << "Problem in line " << line_number+1 <<" while looking for something like '" << parname << "SHORT'";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    ttcci.ConfigureBData(benable, bshort, bdata);
  }
};


class TTCciConfigurationItem_TrigRule : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_TrigRule(TTCci& _ttcci) :
      TTCciConfigurationItem(_ttcci)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    if (NumberOfWords(current_line) != 3)
    {
      ostringstream msg;
      msg << "Found " << NumberOfWords(current_line)-1 << " arguments while expecting 2 for keyword '" << varname << "' in line " <<line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
    string wd = GetNthWord(1, current_line);
    uint32_t irule;
    stringstream g(wd);
    if (!(g >> irule))
    {
      ostringstream msg;
      msg << "Line " << line_number+1 << ": " << "Unable to extract " << "int(trig) from arg 1='" << wd << "'";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    wd = GetNthWord(2, current_line);
    uint32_t ibx;
    stringstream g2(wd);
    if (!(g2 >> ibx))
    {
      ostringstream msg;
      msg << "Line " << line_number+1 << ": " << "Unable to extract " << "int(N_BX) from arg 2='" << wd << "'";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    if (size_t(irule) < ttcci.FirstTriggerRule() || size_t(irule) >= ttcci.TriggerRuleSize())
    {
      ostringstream msg;
      msg << "Line " << line_number+1 << ": " << "invalid trigger rule " << irule;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    ttcci.SetTriggerRule(size_t(irule), ibx);
  }
};


class TTCciConfigurationItem_ExtTrigDelay : public ttc::TTCciConfigurationItem
{
public:
  TTCciConfigurationItem_ExtTrigDelay(TTCci& _ttcci, unsigned _input_num) :
      TTCciConfigurationItem(_ttcci), input_num(_input_num)
  {
  }

  void configure(
      size_t line_number,
      const string& current_line,
      string varname,
      string string_val)
  {
    if (NumberOfWords(current_line) != 2)
    {
      ostringstream msg;
      msg << "Found " << NumberOfWords(current_line)-1 << " arguments "
          << "while expecting 1 for key word '" << varname << "' in line " << line_number+1;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }
    string wd = GetNthWord(1, current_line);
    int delay;

    stringstream g(wd);
    if (!(g >> delay))
    {
      ostringstream msg;
      msg << "Line " << line_number+1 << ": Unable to extract integer from arg 1='" << wd << "'";
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    if (delay < 0 || delay > 255)
    {
      ostringstream msg;
      msg << "Line " << line_number+1 << ": " << " invalid external trigger input " << input_num << " delay value: " << delay;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    // Check whether the current firmware version supports delaying
    // of this trigger input.
    if ((input_num == 0) & !ttcci.CanDelayExtTrigger0Input())
    {
      ostringstream msg;
      msg << "Line " << line_number+1 << ": " << " this firmware does not support delaying external trigger input " << input_num;
      XCEPT_RAISE(xcept::Exception, msg.str());
    }

    ttcci.SetExtTrigInputDelay(input_num, delay);
  }
protected:
  /** Can be 0 or 1. */
  unsigned input_num;
};

}


// class ttc::TTCciConfiguration (statics)

const string ttc::TTCciConfiguration::GetExtTrigDelayKeyWord(unsigned trig_num)
{
  switch (trig_num)
  {
    case 0:
      return TTCciConfigWords::WORD_EXTTRIG0DELAY;
    case 1:
      return TTCciConfigWords::WORD_EXTTRIG1DELAY;
  }
  throw std::invalid_argument("invalid external trigger input " + boost::lexical_cast<string>(trig_num));
}


// class ttc::TTCciConfiguration (members)

ttc::TTCciConfiguration::TTCciConfiguration(ttc::TTCci& _ttcci)
:
    GenericTTCModuleConfiguration(&_ttcci),
    ttcci(_ttcci)
{
  registerConfigurationCommands();
}


void ttc::TTCciConfiguration::Configure(istream& in)
{
  MutexHandler h(ttcci.periodicmutex);

  readLinesAndJoinContinuedLines(in);

  // Deal with sequences.
  try {
    extractSequences();
  }
  catch(xcept::Exception& e)
  {
    XCEPT_RETHROW(
        xcept::Exception,
        "GenericTTCModuleConfiguration::extractSequences failed", e);
  }

  // Disable sending of L1As and BGOs during configuration, but keep
  // track of the current BGO source settings in case they are needed
  // later on.

  ttcci.DisableL1A();

  vector<ExternalInterface> oldBGOSources = ttcci.CheckBGOSource();
  ttcci.SwitchOffBGOs();

  // A few necessary resets.
  ttcci.MainReset();
  ttcci.GetRAMTriggers()->triggerdelay.clear();
  ttcci.GetRAMTriggers()->triggerFrequency = -1.;
  ttcci.GetRAMTriggers()->DirectlyWriteTriggerDPRAM = false;

  // Reset the BGO channels.
  ttcci.ResetAllBGODataOnTTCci();

  // Reset the cylic generators.
  ttcci.ResetCyclicGenerators(true, true);

  // The actual configuration.
  // NOTE: The clock source change will be done last, since this
  // involves a) a potential clock glitch and b) a possible explicitly
  // requested QPLL reset.
  string clockSettingsLine;
  string bgoSourceSettingsLine;
  for (line_number = 0; line_number < config.size(); ++line_number)
  {
    const string& config_line = config[line_number];
    // Skip empty lines.
    if (config_line.empty())
      continue;
    // Skip the clock source settings but store them for later.
    if (config_line.find(WORD_CLOCKSOURCE) != string::npos)
    {
      clockSettingsLine = config_line;
      continue;
    }
    if (config_line.find(WORD_BGOSOURCE) != string::npos)
    {
      bgoSourceSettingsLine = config_line;
      continue;
    }

    try {
      processSingleLine(config_line);
    }
    catch(xcept::Exception& e)
    {
      XCEPT_RETHROW(
          xcept::Exception,
          "GenericTTCModuleConfiguration::processSingleLine failed "
          "for line '" + config_line + "'", e);
    }
  }

  // Upload the trigger delays.
  ttcci.GetRAMTriggers()->WriteTriggerDelaysToTTCci();

  // Write BGO settings to the hardware.
  ttcci.WriteBGODataToTTCci();

  // Now do the clock source settings if we found any.
  if (!clockSettingsLine.empty())
  {
    try {
      processSingleLine(clockSettingsLine);
    }
    catch(xcept::Exception& e)
    {
      XCEPT_RETHROW(
          xcept::Exception,
          "GenericTTCModuleConfiguration::processSingleLine failed "
          "for line '" + clockSettingsLine + "'", e);
    }
  }

  // Now pick the BGO source. If there were configuration lines
  // specifying BGO sources, use those. Otherwise reinstate the
  // original settings.
  if (!bgoSourceSettingsLine.empty())
  {
    try {
      processSingleLine(bgoSourceSettingsLine);
    }
    catch(xcept::Exception& e)
    {
      XCEPT_RETHROW(
          xcept::Exception,
          "GenericTTCModuleConfiguration::processSingleLine failed "
          "for line '" + bgoSourceSettingsLine + "'", e);
    }
  }
  else
  {
    // Reinstate old BGO source settings.
    ttcci.SelectBGOSource(oldBGOSources);
  }
}


void ttc::TTCciConfiguration::registerConfigurationCommands()
{
  configuration_items[WORD_CLOCKSOURCE] = new TTCciConfigurationItem_ClockSource(ttcci);
  configuration_items[WORD_TRIGGERSOURCE] = new TTCciConfigurationItem_TriggerSource(ttcci);
  configuration_items[WORD_BGOSOURCE] = new TTCciConfigurationItem_BGOSource(ttcci);

  // Note that TTCciConfigurationItem_CyclicGen handles both cyclic
  // BGO and L1A generators.
  configuration_items[WORD_CYCLICGEN_TRIGGER] = new TTCciConfigurationItem_CyclicGen(ttcci);
  configuration_items[WORD_CYCLICGEN_BGO] = new TTCciConfigurationItem_CyclicGen(ttcci);

  configuration_items[WORD_ORBITSOURCE] = new TTCciConfigurationItem_OrbitSource(ttcci);
  configuration_items[WORD_TRIGGER_INTERVAL] = new TTCciConfigurationItem_TriggerInterval(ttcci);
  configuration_items[WORD_TRIGGER_FREQUENCY] = new TTCciConfigurationItem_TriggerFrequency(ttcci);
  configuration_items[WORD_BGOCHANNEL_SETUP] = new TTCciConfigurationItem_BGOChannelSetup(ttcci);
  configuration_items[WORD_BGOCHANNEL_DATA] = new TTCciConfigurationItem_BGOChannelData(ttcci);
  configuration_items[WORD_BDATA] = new TTCciConfigurationItem_BData(ttcci);

  configuration_items[WORD_TRIGRULE] = new TTCciConfigurationItem_TrigRule(ttcci);
  configuration_items[WORD_EXTTRIG0DELAY] = new TTCciConfigurationItem_ExtTrigDelay(ttcci, 0);
  configuration_items[WORD_EXTTRIG1DELAY] = new TTCciConfigurationItem_ExtTrigDelay(ttcci, 1);
}
