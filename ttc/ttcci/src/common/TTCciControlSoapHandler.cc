#include "ttc/ttcci/TTCciControlSoapHandler.hh"

#include "ttc/ttcci/TTCciControl.hh"
#include "ttc/ttcci/TTCci.hh"
#include "ttc/utils/HTMLMacros.hh"

#include "hal/HardwareAccessException.hh"

#include "xcept/tools.h"
#include "xoap/Method.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"


using namespace std;


ttc::TTCciControlSoapHandler::TTCciControlSoapHandler(TTCciControl* _ttcci_control)
:
    ttcci_control(_ttcci_control),
    logger_(log4cplus::Logger::getInstance(
        _ttcci_control->getApplicationLogger().getName() + ".TTCciControlSoapHandler"))
{
  xoap::bind(this, &TTCciControlSoapHandler::userCommand, "userCommand", XDAQ_NS_URI);
  xoap::bind(this, &TTCciControlSoapHandler::userCommand, "ExecuteSequence", XDAQ_NS_URI);
  xoap::bind(this, &TTCciControlSoapHandler::userSequence, "User", XDAQ_NS_URI);
  xoap::bind(this, &TTCciControlSoapHandler::GetCurrentConfiguration, "GetCurrentConfiguration", XDAQ_NS_URI);
}


void ttc::TTCciControlSoapHandler::addMethod(toolbox::lang::Method* m, string name)
{
  ttcci_control->addMethod(m, name);
}


xoap::MessageReference
ttc::TTCciControlSoapHandler::userCommand(xoap::MessageReference msg)
    throw (xoap::exception::Exception)
{
  string commandPar;

  try
  {
    string msgStr;
    msg->writeTo(msgStr);
    LOG4CPLUS_INFO(
        logger_,
        "TTCciControlSoapHandler::userCommand: SOAP message received:" << endl
        << msgStr);

    // get access to the body DOM node
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    DOMNode* bodyNode = envelope.getBody().getDOMNode();

    // search for element node inside body (and make sure there is only one)
    DOMNodeList* childNodes = bodyNode->getChildNodes();
    DOMNode* commandNode = 0;
    for (size_t i = 0; i < childNodes->getLength(); ++i)
    {
      if (childNodes->item(i)->getNodeType() == DOMNode::ELEMENT_NODE)
      {
        if (commandNode)
        {
          XCEPT_RAISE(xcept::Exception, "Invalid TTCci SOAP message: Body has more than one child element");
        }
        commandNode = childNodes->item(i);
      }
    }

    if (!commandNode)
    {
      XCEPT_RAISE(xcept::Exception, "Invalid TTCci SOAP message: Body has no child element");
    }

    // If that's a new-style SOAP message, then parameters are contained
    // in the <xdaq:userCommand> or <xdaq:ExecuteSequence> element.
    map<string, string> parameters;
    extractParametersFromAttributes(commandNode, parameters);

    // If no parameters are found, we assume it's an old-style SOAP message,
    // with parameters contained in the attributes of the body element.
    if (parameters.empty())
    {
      extractParametersFromAttributes(bodyNode, parameters);
    }

    // We require that there be either an xdaq:CommandPar or a xdaq:Param parameter,
    // specifying the command or the sequence to be executed.
    if (parameters.find("xdaq:CommandPar") != parameters.end())
    {
      commandPar = parameters["xdaq:CommandPar"];
    }
    else if (parameters.find("xdaq:Param") != parameters.end())
    {
      commandPar = parameters["xdaq:Param"];
    }
    else
    {
      XCEPT_RAISE(
          xcept::Exception,
          "Invalid TTCci SOAP message: Missing attribute xdaq:CommandPar or xdaq:Param");
    }

    // Now we act according to the SOAP message delivered.

    if (commandPar == "ResetStatus")
    {
      ttcci_control->boardLockingProxy()->ClearLatchedStatus();
    }

    else if (commandPar == "ResetCounters")
    {
      if (ttcci_control->fsm_.getCurrentState() != 'E')
      {
        LOG4CPLUS_INFO(logger_, "Resetting TTCci Counters...");
        ttcci_control->boardLockingProxy()->ResetCounters();
        // Read the counters.
        ttcci_control->ReadTTCciCounters();
      }
      else
      {
        XCEPT_RAISE(
            xcept::Exception,
            "Cannot reset counters in FSM state " + ttcci_control->getFSMStateString());
      }
    }

    else if (commandPar == "ReadCounters")
    {
      LOG4CPLUS_INFO(logger_, "Reading TTCci Counters...");
      ttcci_control->ReadTTCciCounters();
    }

    else if (commandPar == "DumpVMEHistory")
    {
      ttcci_control->boardLockingProxy()->PrintVMEHistory();
    }

    else if (commandPar == "Send VME-BGO")
    {
      ttcci_control->boardLockingProxy()->ExecuteVMEBGO(ttcci_control->currentBGO_);
    }

    else if (commandPar == "Send BGO-Start")
    {
      ttcci_control->boardLockingProxy()->ExecuteVMEBGO(9);
    }

    else if (commandPar == "Send BGO-Stop")
    {
      ttcci_control->boardLockingProxy()->ExecuteVMEBGO(10);
    }

    else if (commandPar == "Read BGO configuration from TTCci")
    {
      // NOTE: The TTCci B-go RAM size is 1024 words of 64 bits each.
      ttcci_control->boardLockingProxy()->ReadBGODataFromTTCci(1024);
    }

    else if (commandPar == "Execute Sequence")
    {
      string sequence_name = parameters["xdaq:sequence_name"];
      ExecuteSequenceImplementation(sequence_name);
    }

    else
    {
      LOG4CPLUS_INFO(logger_, "Assuming this is a sequence...");
      // Assume this is a sequence... it will throw if it's not
      ExecuteSequenceImplementation(commandPar);
    }

    return ttc::createSOAPReplyCommand("userTTCciControlResponse");
  }
  catch (xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(xcept::Exception, q,
        "SOAP callback userCommand for command '" + commandPar + "' failed", e);
    return ttcci_control->failSoapCallback(e);
  }
  catch (std::exception& e)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback userCommand for command '" + commandPar + "' failed with std::exception: " + string(e.what()));
    return ttcci_control->failSoapCallback(q);
  }
  catch (...)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback userCommand for command '" + commandPar + "' failed with unknown exception");
    return ttcci_control->failSoapCallback(q);
  }
}


xoap::MessageReference
ttc::TTCciControlSoapHandler::userSequence(xoap::MessageReference msg)
    throw (xoap::exception::Exception)
{
  try {
    string msgStr;
    msg->writeTo(msgStr);
    LOG4CPLUS_INFO(
        logger_,
        "TTCciControlSoapHandler::UserAction: SOAP message received:" << endl
        << msgStr);

    LOG4CPLUS_INFO(logger_, "TTCciControl::userSequence(): Sequence 'user' called");
    ttcci_control->boardLockingProxy()->ExecuteSequence("user");

    // Generate a SOAP answer.
    return ttc::createSOAPReplyCommand("UserResponse");
  }
  catch (xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(xcept::Exception, q,
        "SOAP callback UserAction failed", e);
    return ttcci_control->failSoapCallback(e);
  }
  catch (std::exception& e)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback UserAction failed with std::exception: " + string(e.what()));
    return ttcci_control->failSoapCallback(q);
  }
  catch (...)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback UserAction failed with unknown exception");
    return ttcci_control->failSoapCallback(q);
  }
}


xoap::MessageReference
ttc::TTCciControlSoapHandler::GetCurrentConfiguration(xoap::MessageReference msg)
    throw (xoap::exception::Exception)
{
  try {
    string msgStr;
    msg->writeTo(msgStr);
    LOG4CPLUS_INFO(
        logger_,
        "TTCciControlSoapHandler::GetCurrentConfiguration: SOAP message received:" << endl
        << msgStr);

    ostringstream buf;
    ttcci_control->boardLockingProxy()->WriteConfiguration(buf);
    return ttc::createSOAPReplyGetCurrentConfiguration(buf.str());
  }
  catch (xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(xcept::Exception, q,
        "SOAP callback GetCurrentConfiguration failed", e);
    return ttcci_control->failSoapCallback(e);
  }
  catch (std::exception& e)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback GetCurrentConfiguration failed with std::exception: " + string(e.what()));
    return ttcci_control->failSoapCallback(q);
  }
  catch (...)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback GetCurrentConfiguration failed with unknown exception");
    return ttcci_control->failSoapCallback(q);
  }
}


void ttc::TTCciControlSoapHandler::ExecuteSequenceImplementation(const string& sequence_name)
{
  if ((sequence_name == "coldReset")
      || (sequence_name == "configure")
      || (sequence_name == "enable")
      || (sequence_name == "stop")
      || (sequence_name == "suspend"))
  {
    // FSM transition sequence
    // Perform a state transition (which will then call the corresponding sequence).
    toolbox::Event::Reference e(new toolbox::Event(sequence_name, this));
    ttcci_control->fsm_.fireEvent(e);
  }
  else
  {
    LOG4CPLUS_INFO(logger_, "Executing '" + sequence_name + "' sequence");
    ttcci_control->boardLockingProxy()->ExecuteSequence(sequence_name);
  }
}


void ttc::TTCciControlSoapHandler::extractParametersFromAttributes(DOMNode* n, map<string, string>& p)
{
  p.clear();
  p = getAllAttributesOfNode(n);

  map<string, string>::iterator it = p.find("xmlns:xdaq");
  if (it != p.end())
  {
    p.erase(it);
  }
}
