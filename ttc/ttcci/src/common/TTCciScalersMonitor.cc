#include "ttc/ttcci/TTCciScalersMonitor.h"

#include "ttc/ttcci/TTCciControl.hh"

#include "xdata/UnsignedInteger32.h"
#include "xdata/Float.h"


using namespace std;


ttc::TTCciScalersMonitor::TTCciScalersMonitor(xdaq::Application* app)
:
    ttc::PeriodicCallback(
        app,
        "ttcci_scalers",
        false,
        30,
        1.,
        uint16_t(120) /* forceUpdateEvery 1h */)
{}


void ttc::TTCciScalersMonitor::addComponents()
{
  string group = getTTCciControl().getCMSGroup();
  string system = getTTCciControl().getCMSSystem();
  string id = getTTCciControl().getBoardID();
  addComponent(group, system, id);
}


void ttc::TTCciScalersMonitor::addItems()
{
  addItemAndCallback<xdata::UnsignedInteger32>("OrbitCount",                     this, &TTCciScalersMonitor::refreshOrbitCount);
  addItemAndCallback<xdata::Float>            ("OrbitRateHz",                    this, &TTCciScalersMonitor::refreshOrbitRateHz);
  addItemAndCallback<xdata::UnsignedInteger32>("L1ACount",                       this, &TTCciScalersMonitor::refreshL1ACount);
  addItemAndCallback<xdata::Float>            ("L1ARateHz",                      this, &TTCciScalersMonitor::refreshL1ARateHz);
  addItemAndCallback<xdata::UnsignedInteger32>("BGORequestCount",                this, &TTCciScalersMonitor::refreshBGORequestCount);
  addItemAndCallback<xdata::Float>            ("BGORequestRateHz",               this, &TTCciScalersMonitor::refreshBGORequestRateHz);
}


void ttc::TTCciScalersMonitor::refreshOrbitCount(const Component& component, ostringstream& value)
{
  // get the current orbit count
  uint32_t orbitCount = getTTCciProxy()->ReadOrbitCounter();

  // get the previous orbit count for this component (there is only one, but well)
  string prevOrbitCountStr = getItemString(component.getKey(), "OrbitCount");
  istringstream iss(prevOrbitCountStr);
  uint32_t prevOrbitCount(0);
  iss >> prevOrbitCount;

  // calculate and save new orbit rate
  mapComponentKeyToOrbitRateHz_[component.getKey()] = (orbitCount-prevOrbitCount) / getRefreshPeriodSecs();

  // update orbit count
  value << orbitCount;
}


void ttc::TTCciScalersMonitor::refreshOrbitRateHz(const Component& component, ostringstream& value)
{
  value << mapComponentKeyToOrbitRateHz_[component.getKey()];
}


void ttc::TTCciScalersMonitor::refreshL1ACount(const Component& component, ostringstream& value)
{
  // get the current L1A count
  uint32_t L1ACount = getTTCciProxy()->ReadEventCounter();

  // get the previous L1A count for this component (there is only one, but well)
  string prevL1ACountStr = getItemString(component.getKey(), "L1ACount");
  istringstream iss(prevL1ACountStr);
  uint32_t prevL1ACount(0);
  iss >> prevL1ACount;

  // calculate and save new L1A rate
  mapComponentKeyToL1ARateHz_[component.getKey()] = (L1ACount-prevL1ACount) / getRefreshPeriodSecs();

  // update L1A count
  value << L1ACount;
}


void ttc::TTCciScalersMonitor::refreshL1ARateHz(const Component& component, ostringstream& value)
{
  value << mapComponentKeyToL1ARateHz_[component.getKey()];
}


void ttc::TTCciScalersMonitor::refreshBGORequestCount(const Component& component, ostringstream& value)
{
  // get the current BGO request count
  uint32_t BGORequestCount = getTTCciProxy()->ReadStrobeCounter();

  // get the previous BGO request count for this component (there is only one, but well)
  string prevBGORequestCountStr = getItemString(component.getKey(), "BGORequestCount");
  istringstream iss(prevBGORequestCountStr);
  uint32_t prevBGORequestCount(0);
  iss >> prevBGORequestCount;

  // calculate and save new BGO request rate
  mapComponentKeyToBGORequestRateHz_[component.getKey()] = (BGORequestCount-prevBGORequestCount) / getRefreshPeriodSecs();

  // update BGO request count
  value << BGORequestCount;
}


void ttc::TTCciScalersMonitor::refreshBGORequestRateHz(const Component& component, ostringstream& value)
{
  value << mapComponentKeyToBGORequestRateHz_[component.getKey()];
}


ttc::TTCciControl& ttc::TTCciScalersMonitor::getTTCciControl()
{
  return dynamic_cast<TTCciControl&>(getApp());
}


ttc::TTCciProxy& ttc::TTCciScalersMonitor::getTTCciProxy()
{
  return getTTCciControl().boardLockingProxy();
}
