#include "ttc/ttcci/version.h"

#include "ttc/utils/version.h"
#include "ttc/monitoring/version.h"
#include "config/version.h"
#include "toolbox/version.h"
#include "xdata/version.h"
#include "xgi/version.h"

GETPACKAGEINFO(ttcttcci)

void ttcttcci::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
  CHECKDEPENDENCY(ttcutils);
  CHECKDEPENDENCY(ttcmonitoring);
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(xgi);
}

std::set<std::string, std::less<std::string> > ttcttcci::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;
  ADDDEPENDENCY(dependencies, ttcutils);
  ADDDEPENDENCY(dependencies, ttcmonitoring);
  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xdata);
  ADDDEPENDENCY(dependencies, xgi);

  return dependencies;
}
