#include "ttc/utils/YuiCheckButton.hh"

#include <sstream>
#include <iostream>


using namespace std;


ttc::YuiCheckButton::YuiCheckButton(
    const string &_name,
    const string &_html_span_name,
    const string &_enabled_label,
    const string &_disabled_label)
:
    YuiWidgetWithPostRequest(_name),
    enabled_label(_enabled_label),
    disabled_label(_disabled_label),
    html_span_name(_html_span_name)
{}


string ttc::YuiCheckButton::getHtmlCode()
{
  ostringstream buf;
  buf
      << "<span id=\"" << html_span_name << "\" class=\"yui-button yui-check-button\">" << endl
      << "  <span class=\"first-child\">" << endl << "    <button type=\"button\">&nbsp;</button>" << endl
      << "  </span>" << endl << "</span>" << endl;

  return buf.str();
}


string ttc::YuiCheckButton::getJavaScriptCreationCode(
    bool initial_value,
    vector<string> on_click_functions)
{
  ostringstream buf;

  // Add our own call back function at the beginning which does a
  // http request.
  on_click_functions.insert(
      on_click_functions.begin(),
      makeMakeSubmitCommandInvocationCode(
          "function() { var val = " + name + ".get('checked'); return val;}"));

  ostringstream overall_callback_func;

  overall_callback_func << on_click_functions[0];

  buf
      << "<script type=\"text/javascript\">" << endl
      << "var " << name << " = makeCheckButton('" << html_span_name << "',"
      << (initial_value ? "true" : "false") << ",'"
      << enabled_label << "','"
      << disabled_label << "',"
      << overall_callback_func.str()
      << ");" << endl;

  // Add more callbacks.
  for (size_t i = 0; i < on_click_functions.size(); ++i)
  {
    buf << "  " << name << ".addListener(\"checkedChange\"," << on_click_functions[i] << "); " << endl;
  }

  buf << "</script>" << endl;

  return buf.str();
}
