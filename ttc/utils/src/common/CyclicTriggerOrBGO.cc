#include "ttc/utils/CyclicTriggerOrBGO.hh"

#include "ttc/utils/Utils.hh"

#include "log4cplus/logger.h"


#define CYCLICL1ACORR 2


using namespace std;


ttc::CyclicTriggerOrBGO::CyclicTriggerOrBGO(
    const bool forTrigger,
    const unsigned id,
    const unsigned int btimecorr)
:
    _trigger(forTrigger),
    _id(id),
    wd_inhibit(0),
    wd_initialpresc(0),
    wd_presc(0),
    wd_postsc(0),
    wd_pause(0),
    wd_type(0),
    BTimeCorrection_(btimecorr)
{
  wd_type = (forTrigger ? 0x1 : 0x10);
  return;
}


void ttc::CyclicTriggerOrBGO::Reset()
{
  SetStartBX(0);
  SetRepetitive();
  SetPermanent(false);
  SetEnable(false);
  SetPostscale(0);
  SetPrescale(0);
  SetPause(0);
  SetInitialPrescale(0);

  if (IsBGO())
    SetTypeWd(0x10);
  else
    SetTypeWd(0x1);
}


bool ttc::CyclicTriggerOrBGO::Changed() const
{
  return IsEnabled()
      || GetStartBX() != 0
      || GetInitialPrescale() != 0
      || GetPrescale() != 0
      || GetPostscale() != 0
      || GetPause() != 0;
}


string ttc::CyclicTriggerOrBGO::GetName() const
{
  stringstream name;
  name << (_trigger ? "Cycl.Trigger " : "Cycl. BGO ") << _id << endl;
  return name.str();
}


void ttc::CyclicTriggerOrBGO::Print(ostream& os) const
{
  os
      << "Cyclic " << dec << (IsTrigger() ? "Trigger" : "BGO")
      << " Generator No. " << GetID()
      << " status: " << (IsEnabled() ? "ENABLED" : "DISABLED") << endl
      << "   Repetitive=" << (IsRepetitive() ? "yes" : "no")
      << " Permanent=" << (IsPermanent() ? "yes" : "no")
      << " StartBX=" << GetStartBX()
      << " InitPrescale=" << GetInitialPrescale() << endl
      << "   Prescale=" << GetPrescale()
      << " Postscale=" << GetPostscale()
      << " Pause=" << GetPause();

  if (IsBGO())
  {
    os << " BGO# " << GetBChannel();
  }

  os << endl;
}


uint32_t ttc::CyclicTriggerOrBGO::GetInhibitWd() const
{
  if (BTimeCorrection_ == 0)
  {
    return wd_inhibit;
  }
  else
  {
    int32_t corr = (IsTrigger() ? CYCLICL1ACORR : BTimeCorrection_);
    uint32_t val = MaskIn(
            wd_inhibit,
            RollOver(int32_t(GetStartBX()) - int32_t(corr)),
            CTB_STARTBX_WDTH,
            CTB_STARTBX_OFFS);
    return val;
  }
}


uint32_t ttc::CyclicTriggerOrBGO::GetInitPrescaleWd() const
{
  if (BTimeCorrection_ == 0)
  {
    return wd_initialpresc + ((IsPermanent() || BTimeCorrection_ == 0) ? 0 : 1);
  }
  else
  {
    int32_t corr = (IsTrigger() ? CYCLICL1ACORR : BTimeCorrection_);
    return (uint32_t) (int32_t(wd_initialpresc) + 1 + FlipOrbit(int32_t(GetStartBX()) - int32_t(corr)));
  }
}


uint32_t ttc::CyclicTriggerOrBGO::GetPrescaleWd() const
{
  return wd_presc;
}


uint32_t ttc::CyclicTriggerOrBGO::GetPostscaleWd() const
{
  return wd_postsc;
}


uint32_t ttc::CyclicTriggerOrBGO::GetPauseWd() const
{
  return wd_pause;
}


uint32_t ttc::CyclicTriggerOrBGO::GetTypeWd() const
{
  return wd_type;
}


void ttc::CyclicTriggerOrBGO::SetInhibitWd(const uint32_t val)
{
  wd_inhibit = val;

  if (BTimeCorrection_ == 0)
  {
    wd_inhibit = val;
  }
  else
  {
    wd_inhibit = val;
    int32_t corr = (IsTrigger() ? CYCLICL1ACORR : BTimeCorrection_);

    wd_inhibit = MaskIn(
            val,
            RollOver(int32_t(GetStartBX()) + int32_t(corr)),
            CTB_STARTBX_WDTH,
            CTB_STARTBX_OFFS);
  }
}


void ttc::CyclicTriggerOrBGO::SetInitPrescaleWd(const uint32_t val)
{
  if (BTimeCorrection_ == 0)
  {
    if (val == 0)
    {
      wd_initialpresc = val;
    }
    else
    {
      wd_initialpresc = val - ((IsPermanent() || BTimeCorrection_ == 0) ? 0 : 1);
    }
  }
  else
  {
    if (val == 0)
    {
      wd_initialpresc = val;
    }
    else
    {
      int32_t corr = (IsTrigger() ? CYCLICL1ACORR : BTimeCorrection_);
      wd_initialpresc = val - 1 + FlipOrbit(int32_t(GetStartBX()) - int32_t(corr));
    }
  }
}


void ttc::CyclicTriggerOrBGO::SetPrescaleWd(const uint32_t val)
{
  wd_presc = val;
}


void ttc::CyclicTriggerOrBGO::SetPostscaleWd(const uint32_t val)
{
  wd_postsc = val;
}


void ttc::CyclicTriggerOrBGO::SetPauseWd(const uint32_t val)
{
  wd_pause = val;
}


void ttc::CyclicTriggerOrBGO::SetTypeWd(const uint32_t val)
{
  if (IsTrigger())
  {
    if (val != 0x1)
    {
      LOG4CPLUS_ERROR(
          getUtilsLogger(),
          "CyclicTriggerOrBGO::SetTypeWd(val=0x" << hex << val << "): "
          << "#" << dec << _id << hex << ": " << "val=0x" << val
          << " but type=" << (IsTrigger() ? "TRIGGER" : "BGO")
          << " (should be 0x1) --> ignoring." << dec);
      return;
    }
  }
  else
  {
    if ((val & 0x10) != 0x10)
    {
      LOG4CPLUS_ERROR(
          getUtilsLogger(),
          "CyclicTriggerOrBGO::SetTypeWd(val=0x" << hex << val << "): " << "#" << dec << _id << hex << ": " << "val=0x" << val << " but type=" << (IsTrigger()?"TRIGGER":"BGO") << " (should be 0x1-) --> ignoring." << dec);
      return;
    }
  }

  wd_type = val;
}


unsigned int ttc::CyclicTriggerOrBGO::GetID() const
{
  return _id;
}


bool ttc::CyclicTriggerOrBGO::IsTrigger() const
{
  return _trigger;
}


bool ttc::CyclicTriggerOrBGO::IsBGO() const
{
  return (!IsTrigger());
}


uint32_t ttc::CyclicTriggerOrBGO::GetStartBX() const
{
  return MaskOut(wd_inhibit, CTB_STARTBX_WDTH, CTB_STARTBX_OFFS);
}


void ttc::CyclicTriggerOrBGO::SetStartBX(const uint32_t val)
{
  wd_inhibit = MaskIn(wd_inhibit, val, CTB_STARTBX_WDTH, CTB_STARTBX_OFFS);
}


bool ttc::CyclicTriggerOrBGO::IsRepetitive() const
{
  return (((wd_inhibit >> CTB_REPETITIVE_BIT) & 1) == 1);
}


void ttc::CyclicTriggerOrBGO::SetRepetitive(const bool repetitive)
{
  SetBit(wd_inhibit, CTB_REPETITIVE_BIT, repetitive);
}


bool ttc::CyclicTriggerOrBGO::IsPermanent() const
{
  return (((wd_inhibit >> CTB_PERMANENT_BIT) & 1) == 1);
}


void ttc::CyclicTriggerOrBGO::SetPermanent(const bool permanent)
{
  SetBit(wd_inhibit, CTB_PERMANENT_BIT, permanent);
}


bool ttc::CyclicTriggerOrBGO::IsEnabled() const
{
  return (((wd_inhibit >> CTB_ENABLE_BIT) & 1) == 1);
}


void ttc::CyclicTriggerOrBGO::SetEnable(const bool enable)
{
  SetBit(wd_inhibit, CTB_ENABLE_BIT, enable);
}


uint32_t ttc::CyclicTriggerOrBGO::GetInitialPrescale() const
{
  return MaskOut(wd_initialpresc, CTB_IPRESC_WDTH, CTB_IPRESC_OFFS);
}


void ttc::CyclicTriggerOrBGO::SetInitialPrescale(const uint32_t val)
{
  wd_initialpresc = MaskIn(wd_initialpresc, val, CTB_IPRESC_WDTH, CTB_IPRESC_OFFS);
}


uint32_t ttc::CyclicTriggerOrBGO::GetPrescale() const
{
  return MaskOut(wd_presc, CTB_PRESC_WDTH, CTB_PRESC_OFFS);
}


void ttc::CyclicTriggerOrBGO::SetPrescale(const uint32_t val)
{
  wd_presc = MaskIn(wd_presc, val, CTB_PRESC_WDTH, CTB_PRESC_OFFS);
}


uint32_t ttc::CyclicTriggerOrBGO::GetPostscale() const
{
  return MaskOut(wd_postsc, CTB_POSTSC_WDTH, CTB_POSTSC_OFFS);
}


void ttc::CyclicTriggerOrBGO::SetPostscale(const uint32_t val)
{
  wd_postsc = MaskIn(wd_postsc, val, CTB_POSTSC_WDTH, CTB_POSTSC_OFFS);
}


uint32_t ttc::CyclicTriggerOrBGO::GetPause() const
{
  return MaskOut(wd_pause, CTB_PAUSE_WDTH, CTB_PAUSE_OFFS);
}


void ttc::CyclicTriggerOrBGO::SetPause(const uint32_t val)
{
  wd_pause = MaskIn(wd_pause, val, CTB_PAUSE_WDTH, CTB_PAUSE_OFFS);
}


uint32_t ttc::CyclicTriggerOrBGO::GetBChannel() const
{
  if (!IsBGO())
  {
    LOG4CPLUS_ERROR(getUtilsLogger(),
        "CyclicTriggerOrBGO::GetBChannel() called, although" << "this is a trigger bank! (id=" << GetID() << ")");
    return 0;
  }
  if (((wd_type >> 4) & 1) != 1)
  {
    LOG4CPLUS_ERROR(
        getUtilsLogger(),
        "CyclicTriggerOrBGO::GetBChannel(): "
        "wd_type=0x" << hex << wd_type << dec
        << ", but should be 0x1- (id=" << GetID() << ")");
    return 0;
  }
  return MaskOut(wd_type, CTB_TYPE_WDTH - 1, CTB_TYPE_OFFS);
}


void ttc::CyclicTriggerOrBGO::SetBChannel(const uint32_t val)
{
  if (!IsBGO())
  {
    LOG4CPLUS_ERROR(
        getUtilsLogger(),
        "CyclicTriggerOrBGO::SetBChannel(val=" << val << ") called, "
        "although this is a trigger generator! (id=" << GetID() << ")");
    return;
  }

  wd_type = 0;
  wd_type = MaskIn(wd_type, val, CTB_TYPE_WDTH - 1, CTB_TYPE_OFFS);
  wd_type |= 0x10;
}


// non-class functions in namespace ttc

ostream& ttc::operator<<(ostream& os, const CyclicTriggerOrBGO& generator)
{
  generator.Print(os);
  return os;
}
