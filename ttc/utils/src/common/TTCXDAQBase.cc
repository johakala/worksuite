#include "ttc/utils/TTCXDAQBase.hh"

#include "ttc/utils/BoardLockingProxy.hh"
#include "ttc/utils/GenericTTCModule.hh"
#include "ttc/utils/HTMLMacros.hh"
#include "ttc/utils/HTMLTable.hh"
#include "ttc/utils/Utils.hh"
#include "ttc/utils/CgiUtils.hh"

#include "xdaq/ApplicationContext.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/XceptSerializer.h"
#include "xcept/tools.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/regex.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include <cstdarg>
#include <algorithm>
#include <utility>


using namespace std;


// nested class TTCXDAQBase::ErrorMessages.

ttc::TTCXDAQBase::ErrorMessages::~ErrorMessages()
{
  clear();
}


ostream& ttc::TTCXDAQBase::ErrorMessages::add(const string& category)
{
  messages_.push_back(make_pair<string, ostringstream*>(category, new ostringstream()));

  return *(messages_.back().second);
}


bool ttc::TTCXDAQBase::ErrorMessages::isEmpty() const
{
  return (messages_.size() == 0);
}


void ttc::TTCXDAQBase::ErrorMessages::clear()
{
  for (vector<pair<string, ostringstream*> >::iterator it = messages_.begin(); it != messages_.end(); ++it)
  {
    delete it->second;
  }
  messages_.clear();
}


void ttc::TTCXDAQBase::ErrorMessages::append(const ErrorMessages& other)
{
  for (vector<pair<string, ostringstream*> >::const_iterator
          it = other.messages_.begin();
          it != other.messages_.end(); ++it)
  {
    messages_.push_back(make_pair<string, ostringstream*>(
        it->first,
        new ostringstream(it->second->str())));
  }
}


string ttc::TTCXDAQBase::ErrorMessages::asHtml() const
{
  ostringstream buf;

  vector<pair<string, string> > msgs = compactified();
  for (vector<pair<string, string> >::const_iterator
      it = msgs.begin();
      it != msgs.end(); ++it)
  {
    buf
        << RED
        << BOLD << it->first << ": " << NOBOLD
        << it->second
        << NOCOL << "<br/>" << endl;
  }

  return buf.str();
}


vector<pair<string, string> >
ttc::TTCXDAQBase::ErrorMessages::compactified() const
{
  // First sort everything (so this looses the time order).
  // Note that we have to make a copy in order not to mess up the original state.

  vector<pair<string, string> > msgs;
  for (vector<pair<string, ostringstream*> >::const_iterator
      it = messages_.begin();
      it != messages_.end(); ++it)
  {
    msgs.push_back(make_pair<string, string>(
        it->first,
        it->second->str()));
  }

  sort(msgs.begin(), msgs.end());

  // Then loop over all messages and count all occurrences.

  map<pair<string, string> , size_t> counts;
  pair<string, string> prev;

  for (vector<pair<string, string> >::const_iterator
      cur = msgs.begin();
      cur != msgs.end(); ++cur)
  {
    if (*cur == prev)
    {
      counts[prev] += 1;
    }
    else
    {
      counts.insert(make_pair<pair<string, string> , size_t>(*cur, 1));
      prev = *cur;
    }
  }

  // Finally build the results string.
  vector<pair<string, string> > res;
  for (map<pair<string, string> , size_t>::const_iterator it = counts.begin(); it != counts.end(); ++it)
  {
    string cat = it->first.first;
    string msg = it->first.second;
    if (it->second > 1)
    {
      ostringstream tmp;
      tmp << " (" << it->second << " times)";
      msg.append(tmp.str());
    }
    res.push_back(make_pair<string, string>(cat, msg));
  }

  return res;
}


// class ttc::TTCXDAQBase - static

string ttc::TTCXDAQBase::ERROR_MESSAGES_DIV = "ErrorMessages";

string const ttc::TTCXDAQBase::supportURL = "https://svnweb.cern.ch/trac/cmsos/query?status=assigned"
    "&status=implemented"
    "&status=new"
    "&status=reopened"
    "&component=ttc_S"
    "&order=priority"
    "&col=id"
    "&col=summary"
    "&col=status"
    "&col=type"
    "&col=priority"
    "&col=milestone"
    "&col=component";


// class ttc::TTCXDAQBase - members

ttc::TTCXDAQBase::TTCXDAQBase(
    xdaq::ApplicationStub* stub,
    const string& className,
    const string& boardType)
:
    xdaq::WebApplication(stub),
    className_(className),
    boardType_(boardType),
    lastPage_("Default"),
    autoRefresh_(false),
    group_("TTC"), // default for when 'group' property not defined in config file
    system_("test"), // default for when 'system' property not defined in config file
    busadaptername_("$VMEBUSADAPTER"),
    Location_(0),
    DisableVMEWrite_(false),
    ReadConfigFromFile_(false),
    asciConfigurationFilePath_("dummy.txt"),
    DisableMonitoring_(false),
    oldfrequency_(-99.0),
    HTMLFieldsInited_(false)
{
  editApplicationDescriptor()->setAttribute("icon",
                                            "/ttc/" + toolbox::tolower(boardType_) +
                                            "/images/" + toolbox::tolower(className_) + ".gif");

  logger_ = log4cplus::Logger::getInstance(getApplicationLogger().getName() + className_);
  time(&tnow_);
  tprevious_ = tnow_;

  // info space items and listeners

  addItem("group",             group_);
  addItem("system",            system_);
  addItem("name",              name_);
  addItem("BusAdapter",        busadaptername_);
  addItem("Location",          Location_);
  addItem("DisableVMEWrite",   DisableVMEWrite_);
  addItem("DisableMonitoring", DisableMonitoring_);

  addItem("StateName",         StateName_,           true,  false); //< retrievable, not changeable
  addItem("stateName",         StateName_,           true,  false); //< retrievable, not changeable; needed by RCMS!

  addItem("Configuration",     ConfigurationString_, false, true);  //< changeable, not retrievable

  getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

  // Set up finite state machine.
  initFSM();

  // SOAP bindings
  xoap::bind(this, &TTCXDAQBase::getStateRCMS, "getState", "urn:rcms-soap:2.0");
  xoap::bind(this, &TTCXDAQBase::changeState,  "coldReset", XDAQ_NS_URI);
  xoap::bind(this, &TTCXDAQBase::changeState,  "configure", XDAQ_NS_URI);
  xoap::bind(this, &TTCXDAQBase::changeState,  "enable",    XDAQ_NS_URI);
  xoap::bind(this, &TTCXDAQBase::changeState,  "stop",      XDAQ_NS_URI);
  xoap::bind(this, &TTCXDAQBase::changeState,  "suspend",   XDAQ_NS_URI);
  xoap::bind(this, &TTCXDAQBase::resetFSM,     "reset",     XDAQ_NS_URI);

  // CGI bindings
  cgi_bind(this, &TTCXDAQBase::HTMLPageMain, "Default", className_);
  cgi_bind(this, &TTCXDAQBase::Command,      "Command");
  cgi_bind(this, &TTCXDAQBase::SoapCommand,  "SoapCommand");
}


ttc::TTCXDAQBase::~TTCXDAQBase()
{}


string ttc::TTCXDAQBase::getCMSGroup() const
{
  return group_.value_;
}


string ttc::TTCXDAQBase::getCMSSystem() const
{
  return system_.value_;
}


string ttc::TTCXDAQBase::getBoardID() const
{
    return name_.value_;
}


bool ttc::TTCXDAQBase::readConfigFromFile()
{
  return ReadConfigFromFile_;
}


string ttc::TTCXDAQBase::pathToASCIIConfigFile()
{
  return ReadConfigFromFile_ ? asciConfigurationFilePath_ : "";
}


string ttc::TTCXDAQBase::getFSMStateString()
{
  char state = fsm_.getCurrentState();
  return fsm_.getStateName(state);
}


void ttc::TTCXDAQBase::addItem(
    const string& itemName,
    xdata::Serializable& item,
    bool retrievable,
    bool changeable)
{
  getApplicationInfoSpace()->fireItemAvailable(itemName, &item);

  if (retrievable)
    getApplicationInfoSpace()->addItemRetrieveListener(itemName, this);

  if (changeable)
    getApplicationInfoSpace()->addItemChangedListener(itemName, this);
}


void ttc::TTCXDAQBase::addItemRetrievable(
    const string& itemName,
    xdata::Serializable& item)
{
  addItem(itemName, item, true);
}


void ttc::TTCXDAQBase::addItemRetrievableChangeable(
    const string& itemName,
    xdata::Serializable& item)
{
  addItem(itemName, item, true, true);
}


void ttc::TTCXDAQBase::itemRetrieveAction(xdata::ItemRetrieveEvent& e)
{
  string itemName = e.itemName();

  if (itemName == "StateName" ||
      itemName == "stateName")
  {
    StateName_ = getFSMStateString();
  }
}


void ttc::TTCXDAQBase::itemChangedAction(xdata::ItemChangedEvent& e)
{
  string itemName = e.itemName();

  if (itemName == "Configuration")
  {
    extractConfigurationSourceParams();
  }
}


void ttc::TTCXDAQBase::setDefaultValuesAction()
{
  boardLockingProxy().initHardwareAccessParams(busadaptername_, Location_, !DisableVMEWrite_);
}


void ttc::TTCXDAQBase::ColdResetAction(toolbox::Event::Reference event)
{}


void ttc::TTCXDAQBase::ConfigureAction(toolbox::Event::Reference event)
{}


void ttc::TTCXDAQBase::EnableAction(toolbox::Event::Reference event)
{}


void ttc::TTCXDAQBase::StopAction(toolbox::Event::Reference event)
{}


void ttc::TTCXDAQBase::SuspendAction(toolbox::Event::Reference event)
{}


xoap::MessageReference
ttc::TTCXDAQBase::changeState(xoap::MessageReference msg)
    throw (xoap::exception::Exception)
{
  string transition;

  try {
    LOG4CPLUS_INFO(logger_, "TTCXDAQBase::changeState");

    xoap::SOAPPart part = msg->getSOAPPart();
    xoap::SOAPEnvelope env = part.getEnvelope();
    xoap::SOAPBody body = env.getBody();

    DOMNode* node = body.getDOMNode();
    DOMNodeList* bodyList = node->getChildNodes();

    for (unsigned int i = 0; i < bodyList->getLength(); i++)
    {
      DOMNode* command = bodyList->item(i);

      if (command->getNodeType() == DOMNode::ELEMENT_NODE)
      {
        // translate transition name from SOAP command into event
        transition = xoap::XMLCh2String(command->getLocalName());
        toolbox::Event::Reference e(new toolbox::Event(transition, this));

        // send event to the FSM
        fsm_.fireEvent(e);
        return ttc::createSOAPReplyFSMTransition(transition + "Response", getFSMStateString());
      }
    }
  }
  catch (xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(xcept::Exception, q,
        "SOAP callback for transition '" + transition + "' failed", e);
    return failSoapCallback(q);
  }
  catch (std::exception& e)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback for transition '" + transition + "' failed with std::exception: " + string(e.what()));
    return failSoapCallback(q);
  }
  catch (...)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback for transition '" + transition + "' failed with unknown exception");
    return failSoapCallback(q);
  }

  XCEPT_DECLARE(xcept::Exception, q,
      "TTCXDAQBase::changeState() called but no command found");
  return failSoapCallback(q);
}


void ttc::TTCXDAQBase::sendSOAPCommandToMe(
    const string& commandName,
    const map<string, string>& variables)
{
  xoap::MessageReference myMsg = ttc::createSOAPMessageCommand(commandName, variables);

  sendSOAPMessage(
      myMsg,
      "command "+commandName,
      this,
      this->getApplicationDescriptor());
}


xoap::MessageReference ttc::TTCXDAQBase::failSoapCallback(xcept::Exception& e)
{
  error_messages.add("ERROR") << originalException(e);
  LOG4CPLUS_ERROR(logger_, compactExceptionHistory(e));
  return ttc::createSOAPReplyException(e);
}


void ttc::TTCXDAQBase::CommandImpl(cgicc::Cgicc& cgi, const string& command)
{}


void ttc::TTCXDAQBase::RedirectMessage(xgi::Output* out, const string& url)
{
  string myurl = url;
  if (myurl.length() == 0)
  {
    myurl = string("/") + getApplicationDescriptor()->getURN() + "/" + lastPage_;
  }

  *out
      << "<br>"
      << BIGGER
      << RED << "NOTE:" << NOCOL << endl
      << " If your browser (e.g. some safari versions) does "
      << BOLD << "not " << NOBOLD << "automatically redirect you to the previous page, " << endl
      << "<a href=\"" << myurl << "\">click here</a>."
      << NOBIGGER << "<br>" << endl;
}


void ttc::TTCXDAQBase::initHTMLFields()
{}


bool ttc::TTCXDAQBase::areHTMLFieldsInited()
{
  return HTMLFieldsInited_;
}


void ttc::TTCXDAQBase::setHTMLFieldsInited()
{
  HTMLFieldsInited_ = true;
}


string ttc::TTCXDAQBase::getCGIRequestPageName(xgi::Input* in)
{
  return in->getenv("PATH_INFO");
}


ttc::CGIMethodSignature* ttc::TTCXDAQBase::getCGIRequestCallback(std::string& command)
{
  if (mapCGIRequestPageNameToCallback_.find(command) != mapCGIRequestPageNameToCallback_.end())
  {
    return mapCGIRequestPageNameToCallback_[command];
  }
  else if (command == "Default")
  {
    XCEPT_RAISE(xcept::Exception, "No CGI callback registered for command 'Default'");
  }
  else
  {
    command = "Default";
    return getCGIRequestCallback(command); //< recursive call
  }
}


void ttc::TTCXDAQBase::PrintHTMLHeader(xgi::Output& out, bool auto_refresh_button)
{
  int slot = boardLockingProxy()->GetInfo().location();
  string busAdapterName = boardLockingProxy().getUsedBusAdapterName();
  string firmwareVersion = boardLockingProxy()->firmwareVersionString();

  ostringstream titleStr;
  titleStr
      << "<div style='color:#3333FF; font-size:1.3em; '>"
      << className_ << " | " << name_.toString()
      << "</div>";

  ostringstream subtitleStr;
  subtitleStr
      << "<div style='color:#455268; font-size:1.1em; '>"
      << busAdapterName << " | "
      << "slot " << slot << " | "
      << "SW " << softwareVersion()  << " | "
      << "FW " << firmwareVersion
      << "</div>";

  string url = getApplicationDescriptor()->getContextDescriptor()->getURL();
  string urn = getApplicationDescriptor()->getURN();

  ostringstream iconStr;
  iconStr
      << getApplicationDescriptor()->getContextDescriptor()->getURL() << "/"
      << getApplicationDescriptor()->getAttribute("icon");


  out
      << "<table style='text-align: left;' border='0' cellpadding='0' cellspacing='2'>" << endl
      << "<tbody>" << endl
      << "<tr>" << endl
      << "<td style='vertical-align: top; '>" << endl;

  string base_url = "/" + getApplicationDescriptor()->getURN() + "/";

  for (unsigned i = 0; i < tab_items.size(); ++i)
  {
    string command_name = tab_items[i].command_name;
    string display_name = tab_items[i].display_name;

    string style = (command_name != lastPage_)
        ? "color:#3333FF; "
        : "color:#3333FF; background-color:#99FF00; ";

    style += "font-size:1.1em; ";

    out
        << "["
        << "<a "
        << "style='" << style << "' "
        << "href=\"" << base_url << command_name << "\">"
        << display_name
        << "</a>"
        << "]";
  }

  out
      << "</td>" << endl
      << "<td style=\"vertical-align: top;\">" << endl;

  if (auto_refresh_button)
  {
    string cmd_url = base_url + "Command";

    out
        << "<td style=\"vertical-align: top;\">" << endl
        << cgicc::form().set("method", "get").set("action", cmd_url).set("enctype", "multipart/form-data") << endl;

    if (autoRefresh_)
      out << cgicc::input().set("type", "submit").set("name", "Command").set("value", "AutoRefresh OFF");
    else
      out << cgicc::input().set("type", "submit").set("name", "Command").set("value", "AutoRefresh");

    out
        << cgicc::form()
        << "</td>" << endl;
  }

  out
      << "</tr>" << endl
      << "</tbody>" << endl
      << "</table>" << endl;
}


void ttc::TTCXDAQBase::PrintHTMLFooter(xgi::Output& out)
{
  out
      << "<hr style=\"width: 100%; height: 2px;\">" << endl
      << "<center>"
      << "<span>"
      << "<a href='" << supportURL << "'>Support</a>" << " | "
      << "<a href='https://twiki.cern.ch/twiki/bin/view/CMS/TTCManual'>Documentation</a>"
      << "</span>"
      << "</center>" << endl;
}


void ttc::TTCXDAQBase::PrintFSMState(xgi::Output& out)
{
  toolbox::fsm::State  s = fsm_.getCurrentState();

  map<char, string>::iterator it = mapStateToColor.find(s);

  // Unsupported states are shown in red.
  string colour = STYLE_ATTR_RED;

  if (it != mapStateToColor.end())
  {
    colour = it->second;
  }

  out
      << "<big style=\"font-weight: bold;\">"
      << "<span style=\"color: " << colour << ";\">"
      << fsm_.getStateName(s)
      << "</span>"
      << "</big>" << endl;
}


void ttc::TTCXDAQBase::PrintFSMTable(xgi::Output& out)
{
  HTMLTable tab(out, 1, 10, 0, "", "center");

  tab.NewCell();
  out << "<b>" << className_ << " Finite State Machine</b>";

  tab.NewCell();
  PrintFSMState(out);

  tab.NewCell("", 4);

  tab.NewRow();

  toolbox::fsm::State s = fsm_.getCurrentState();

  addFSMButton(tab, out, "configure", s == 'H');
  addFSMButton(tab, out, "enable",   (s == 'C') || (s == 'S'));
  addFSMButton(tab, out, "suspend",   s == 'E');
  addFSMButton(tab, out, "stop",     (s == 'E') || (s == 'S'));
  addFSMButton(tab, out, "coldReset", s == 'H');
  addFSMButton(tab, out, "reset",     true, "color: red;");
}


void ttc::TTCXDAQBase::PrintSequenceTable(xgi::Output& out)
{
  HTMLTable tab(out, 1, 10, 0, "", "center");

  tab.NewRow();
  tab.NewCell();
  out << "<b>Execute non-transition sequences</b>" << endl;
  tab.NewCell();

  // Get a list of sequences and filter out the 'standard' ones.
  vector<string> seqNames = boardLockingProxy()->GetSequenceNames();
  std::sort(seqNames.begin(), seqNames.end());
  vector<string> transitionNames;
  transitionNames.push_back("coldReset");
  transitionNames.push_back("configure");
  transitionNames.push_back("enable");
  transitionNames.push_back("stop");
  transitionNames.push_back("suspend");

  bool seqFound = false;

  string url = "/";
  url += getApplicationDescriptor()->getURN();
  url += "/SoapCommand";

  for (vector<string>::const_iterator
      it = seqNames.begin();
      it != seqNames.end(); ++it)
  {
    // Only show transitions that are not associated with state transitions.
    if (std::find(transitionNames.begin(), transitionNames.end(), *it)
        == transitionNames.end())
    {
      out
          << "<form method=\"post\" action=\"" << url << "\" enctype=\"multipart/form-data\">" << endl
          << cgicc::input().set("type", "hidden").set("name", "Command").set("value", "ExecuteSequence")
          << cgicc::input().set("type", "hidden").set("name", "Param").set("value", *it)
          << cgicc::input().set("type", "submit").set("value", *it)
          << "</form>" << endl;

      seqFound = true;
    }
  }

  if (!seqFound)
  {
    out << "No non-transition sequences defined" << endl;
  }

  tab.Close();
}


string ttc::TTCXDAQBase::getFullURL()
{
  string url = getApplicationDescriptor()->getContextDescriptor()->getURL();
  url += "/";
  url += getApplicationDescriptor()->getURN();

  return url;
}


bool ttc::TTCXDAQBase::isTab(const string& pageName)
{
  for (unsigned i = 0; i < tab_items.size(); ++i)
    if (pageName == tab_items[i].command_name)
      return true;

  return false;
}


void ttc::TTCXDAQBase::WriteJavascriptPopupCode(ostream& out)
{
  out
      << "<script type=\"text/javascript\">" << endl
      << "  function openPopup(url, frame_name, width, height)" << endl
      << "    {" << endl
      << "      new_window = window.open(url, frame_name, \"width=\" + width + \",height=\" + height + \",resizable=yes\");" << endl
      << "      new_window.focus();" << endl
      << "      return false;" << endl
      << "    }" << endl
      << "</script>" << endl;
}


void ttc::TTCXDAQBase::extractConfigurationSourceParams()
{
  string Configuration = ConfigurationString_.toString();

  string regex_string = "^[[:space:]]*"
      "\[[[:space:]]*"
      "file[[:space:]]*=[[:space:]]*"
      "([^[:space:]]+)"
      "[[:space:]]*][[:space:]]*$";

  vector<string> matches;
  bool doesMatch = toolbox::regx_match(Configuration, regex_string, matches);

  if (doesMatch && matches.size() == 2)
  {
    ReadConfigFromFile_ = true;
    asciConfigurationFilePath_ = ttc::expandFileName(matches.at(1));
    LOG4CPLUS_INFO( logger_, "Found '[file=...]' in XML variable 'Configuration' "
    "--> using file '" + asciConfigurationFilePath_.toString() + "' for configuration.");
  }
  else
  {
    ReadConfigFromFile_ = false;
    LOG4CPLUS_INFO( logger_, "Did not find '[file=...]' in XML variable 'Configuration' "
    "--> using XML directly for configuration.");
  }
}


string ttc::TTCXDAQBase::GetBodyBackgroundColour()
{
  return DisableVMEWrite_ ? "#CCCCCC" : "#FFFFFF";
}


void ttc::TTCXDAQBase::actionPerformed(xdata::Event& e)
{
  string eventType = e.type();

  ostringstream eventDescr;
  eventDescr << "TTCXDAQBase::actionPerformed for event type '" << eventType << "'";

  try {
    if (xdata::ItemRetrieveEvent* ev = dynamic_cast<xdata::ItemRetrieveEvent*>(&e))
    {
      string itemName = ev->itemName();
      eventDescr << ", item '" << itemName << "'";
      LOG4CPLUS_INFO(logger_, eventDescr.str());
      itemRetrieveAction(*ev);
    }
    else if (xdata::ItemChangedEvent* ev = dynamic_cast<xdata::ItemChangedEvent*>(&e))
    {
      string itemName = ev->itemName();
      eventDescr << ", item '" << itemName << "'";
      LOG4CPLUS_INFO(logger_, eventDescr.str());
      itemChangedAction(*ev);
    }
    else if (eventType == "urn:xdaq-event:setDefaultValues")
    {
      LOG4CPLUS_INFO(logger_, eventDescr.str());
      setDefaultValuesAction();
    }
    else
    {
      XCEPT_RAISE(
          xcept::Exception,
          eventDescr.str() + " failed: no action implemented");
    }
  }
  catch(ttc::exception::BoardProxyParamsNotInited& ex)
  {
    failActionPerformed(eventDescr.str() + " failed: " + xcept::stdformat_exception_history(ex));
  }
  catch(xcept::Exception& ex)
  {
    failActionPerformed(eventDescr.str() + " failed: " + xcept::stdformat_exception_history(ex));
  }
  catch(std::exception& ex)
  {
    failActionPerformed(eventDescr.str() + " failed with std::exception: " + string(ex.what()));
  }
  catch(...)
  {
    failActionPerformed(eventDescr.str() + " failed with unknown exception");
  }
}


void ttc::TTCXDAQBase::failActionPerformed(const string& msg)
{
  LOG4CPLUS_ERROR(logger_, msg);
  error_messages.add("ERROR") << msg;
  XCEPT_RAISE(xdata::exception::Exception, msg);
}


void ttc::TTCXDAQBase::initFSM()
{
  // Exception handler for transitions.
  fsm_.setFailedStateTransitionAction(this, &TTCXDAQBase::transitionFailed);

  // Define the various states, with names and colors.
  fsmAddState('H', "halted",     STYLE_ATTR_RED);
  fsmAddState('C', "configured", STYLE_ATTR_ORANGE);
  fsmAddState('E', "enabled",    STYLE_ATTR_GREEN);
  fsmAddState('S', "suspended",  STYLE_ATTR_ORANGE);

  // Define all state transitions.
  fsmAddTransition('H', 'H', "coldReset", &TTCXDAQBase::ColdResetAction);
  fsmAddTransition('H', 'C', "configure", &TTCXDAQBase::ConfigureAction);
  fsmAddTransition('C', 'E', "enable",    &TTCXDAQBase::EnableAction);
  fsmAddTransition('E', 'C', "stop",      &TTCXDAQBase::StopAction);
  fsmAddTransition('E', 'S', "suspend",   &TTCXDAQBase::SuspendAction);
  fsmAddTransition('S', 'E', "enable",    &TTCXDAQBase::EnableAction);
  fsmAddTransition('S', 'C', "stop",      &TTCXDAQBase::StopAction);

  // Define the initial state and initialise the FSM.
  fsm_.setInitialState('H');
  fsm_.reset();
}


void ttc::TTCXDAQBase::fsmAddState(toolbox::fsm::State state, const string& name, const string& color)
{
  fsm_.addState(state, name);
  mapStateToColor[state] = color;
}


void ttc::TTCXDAQBase::fsmAddTransition(
    toolbox::fsm::State from,
    toolbox::fsm::State to,
    const std::string& input,
    void (TTCXDAQBase::*func)(toolbox::Event::Reference) )
{
  fsm_.addStateTransition(from, to, input, this, &TTCXDAQBase::ExecuteTransition);
  mapTransitionToAction_[input] = func;
}


void ttc::TTCXDAQBase::transitionFailed(toolbox::Event::Reference e)
{
  // reset the FSM in case of a failure (go from 'F' to 'H' state)
  fsm_.reset();

  // throw an exception ==> fsm::FiniteStateMachine::fireEvent will throw
  toolbox::fsm::FailedEvent f = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
  XCEPT_RETHROW(
      toolbox::fsm::exception::Exception,
      "FSM transition failed", f.getException());
}


void ttc::TTCXDAQBase::ExecuteTransition(toolbox::Event::Reference event)
{
  string transition;

  try
  {
    transition = event->type();

    LOG4CPLUS_INFO(logger_, "TTCXDAQBase::ExecuteTransition '" << transition << "'");

    // invoke transition
    (this->*(mapTransitionToAction_[transition]))(event);
  }
  catch (toolbox::fsm::exception::Exception& e)
  {
    XCEPT_RETHROW(
        toolbox::fsm::exception::Exception,
        "FSM action '" + transition + "' failed", e);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(
        toolbox::fsm::exception::Exception,
        "FSM action '" + transition + "' failed", e);
  }
  catch (std::exception const& e)
  {
    XCEPT_RAISE(
        toolbox::fsm::exception::Exception,
        "FSM action '" + transition + "' failed with std::exception: " + string(e.what()));
  }
  catch (...)
  {
    XCEPT_RAISE(
        toolbox::fsm::exception::Exception,
        "FSM action '" + transition + "' failed with unknown exception");
  }
}


xoap::MessageReference
ttc::TTCXDAQBase::getStateRCMS(xoap::MessageReference /*msg*/)
    throw (xoap::exception::Exception)
{
  try {
    return ttc::createSOAPReplyFSMState("getStateResponse", getFSMStateString());
  }
  catch (xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(xcept::Exception, q,
        "SOAP callback getStateRCMS failed", e);
    return failSoapCallback(q);
  }
  catch (std::exception& e)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback getStateRCMS failed with std::exception: " + string(e.what()));
    return failSoapCallback(q);
  }
  catch (...)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback getStateRCMS failed with unknown exception");
    return failSoapCallback(q);
  }
}


xoap::MessageReference
ttc::TTCXDAQBase::resetFSM(xoap::MessageReference msgIn)
    throw (xoap::exception::Exception)
{
  try {
    LOG4CPLUS_INFO(logger_, className_ + " resetting FSM");

    fsm_.reset();
    return ttc::createSOAPReplyFSMTransition(className_ + "FSMReset", getFSMStateString());
  }
  catch (xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(xcept::Exception, q,
        "SOAP callback resetFSM failed", e);
    return failSoapCallback(q);
  }
  catch (std::exception& e)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback resetFSM failed with std::exception: " + string(e.what()));
    return failSoapCallback(q);
  }
  catch (...)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        "SOAP callback resetFSM failed with unknown exception");
    return failSoapCallback(q);
  }
}


void ttc::TTCXDAQBase::Default(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception)
{
  string pageName;

  try
  {
    pageName = getCGIRequestPageName(in);
    LOG4CPLUS_INFO(logger_, "CGI request for page '" << pageName << "'");

    // initialize HTML variables
    initHTMLFields();

    // invoke CGI callback method
    ttc::CGIMethodSignature* m = getCGIRequestCallback(pageName);

    if (isTab(pageName))
      lastPage_ = pageName;

    m->invoke(in, out);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_DECLARE_NESTED(xcept::Exception, q,
        string("CGI request for page '" + pageName + "' failed"), e);
    failCGICallbackDefault(q, *out);
  }
  catch (std::exception& e)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        string("CGI request for page '" + pageName + "' failed with std::exception: ") + e.what());
    failCGICallbackDefault(q, *out);
  }
  catch (...)
  {
    XCEPT_DECLARE(xcept::Exception, q,
        string("CGI request for page '" + pageName + "' failed with unknown exception"));
    failCGICallbackDefault(q, *out);
  }
}


void ttc::TTCXDAQBase::failCGICallbackDefault(xcept::Exception& e, xgi::Output& out)
{
  string urlPreviousPage = "/" + getApplicationDescriptor()->getURN() + "/" + lastPage_;
  string linkStyle = "color:blue; font-size:1.2em; ";
  out << "<a "
            "style='" << linkStyle << "' "
            "href='" << urlPreviousPage << "'>"
          "Back to previous page"
          "</a>"
          "<p/>";

  error_messages.add("ERROR") << originalException(e);
  LOG4CPLUS_ERROR(logger_, compactExceptionHistory(e));
  out << xcept::htmlformat_exception_history(e);
}


void ttc::TTCXDAQBase::Command(xgi::Input* in, xgi::Output* out)
{
  // Create a new cgicc object containing all the CGI data.
  cgicc::Cgicc cgi(in);
  string command = cgi["Command"]->getValue();

  string msg = "Executing CGI command '" + command + "'";
  LOG4CPLUS_INFO(logger_, "TTCXDAQBase::Command: "  << msg);

  // This is the only command that all TTC boards have in common.
  if ((command == "AutoRefresh OFF") || (command == "AutoRefresh"))
  {
    autoRefresh_ = !autoRefresh_;
  }
  else
    CommandImpl(cgi, command);

  XgiOutputHandler xoh(*out);

  xoh
      << "<html>" << endl
      << "<head>" << endl
      << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"0; URL=" << lastPage_ << "\">" << endl
      << "</head>"  << endl
      << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  xoh << cgicc::h1(msg) << endl;
  RedirectMessage(out, lastPage_);
  xoh << "</body></html>" << endl;
}


void ttc::TTCXDAQBase::SoapCommand(xgi::Input* in, xgi::Output* out)
{
  // It would be nice to be able to do: command|param =
  // cgi["Command|Param"]->getValue(); But Cgicc["name"] only
  // returns a valid iterator to the named element if it exists. If
  // it doesn't, there is no obvious way to tell that the iterator
  // is invalid and cannot be dereferenced...  Hence this ugly hack
  // is needed. You gotta love cgicc.
  cgicc::Cgicc cgi(in);
  vector<cgicc::FormEntry> uglycgicc;

  string command, param;

  if (cgi.getElement("Command", uglycgicc))
    command = uglycgicc[0].getValue();

  if (cgi.getElement("Param", uglycgicc))
    param = uglycgicc[0].getValue();

  string msg = "Executing CGI command '" + command + "' via SOAP";
  LOG4CPLUS_INFO(logger_, "TTCXDAQBase::SoapCommand: " << msg);

  // Execute this CGI command via SOAP
  map<string, string> params;
  if (!param.empty()) params["Param"] = param;

  sendSOAPCommandToMe(command, params);

  XgiOutputHandler xoh(*out);

  xoh
      << "<html>" << endl
      << "<head>" << endl
      << "<meta HTTP-EQUIV=\"Refresh\" CONTENT=\"1; URL=" << lastPage_ << "\">" << endl
      << "</head>" << endl
      << "<body bgcolor=\"" << GetBodyBackgroundColour() << "\">" << endl;

  xoh << cgicc::h1(msg) << endl;
  RedirectMessage(out, lastPage_);
  xoh << "</body></html>" << endl;
}


void ttc::TTCXDAQBase::addFSMButton(
    HTMLTable& tab,
    xgi::Output& out,
    const string& action,
    bool enabled,
    const string& style)
{
  string urlSoapCommand =
      "/" + getApplicationDescriptor()->getURN() + "/SoapCommand";

  tab.NewCell();

  out << cgicc::form()
      .set("method", "get")
      .set("action", urlSoapCommand)
      .set("enctype", "multipart/form-data") << endl;

  if (enabled)
  {
    out << cgicc::input()
        .set("type", "submit")
        .set("name", "Command")
        .set("style", style)
        .set("value", action);
  }
  else
  {
    out << cgicc::input()
        .set("type", "submit")
        .set("name", "Command")
        .set("style", style)
        .set("value", action)
        .set("disabled", "true");
  }

  out << cgicc::form();
}


string ttc::TTCXDAQBase::originalException(xcept::Exception& e)
{
  vector<xcept::ExceptionInformation>& ve = e.getHistory();

  return ve.front().getProperty("message");
}


string ttc::TTCXDAQBase::compactExceptionHistory(xcept::Exception& e)
{
  vector<xcept::ExceptionInformation>& ve = e.getHistory();

  ostringstream msg;

  bool rfirst = true;

  for (vector<xcept::ExceptionInformation>::reverse_iterator
      rit = ve.rbegin();
      rit != ve.rend(); ++rit, rfirst = false)
  {
    if (!rfirst) msg << endl << " <= ";
    msg << rit->getProperty("message");
  }

  return msg.str();
}
