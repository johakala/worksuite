#include "ttc/utils/BGOMap.hh"

#include "ttc/utils/GenericTTCModule.hh"
#include "ttc/utils/Utils.hh"


using namespace std;


const int8_t ttc::BGOMap::nchannels = 16;

const uint32_t ttc::BGOMap::BGOMAP_RESYNC_OFFS = 0; // channel 5
const uint32_t ttc::BGOMap::BGOMAP_RESYNC_WDTH = 4;
const uint32_t ttc::BGOMap::BGOMAP_HARDRESET_OFFS = 4; // channel 6
const uint32_t ttc::BGOMap::BGOMAP_HARDRESET_WDTH = 4;
const uint32_t ttc::BGOMap::BGOMAP_ECRST_OFFS = 8; // channel 7
const uint32_t ttc::BGOMap::BGOMAP_ECRST_WDTH = 4;
const uint32_t ttc::BGOMap::BGOMAP_OCRST_OFFS = 12; // channel 8
const uint32_t ttc::BGOMap::BGOMAP_OCRST_WDTH = 4;
const uint32_t ttc::BGOMap::BGOMAP_START_OFFS = 16; // channel 9
const uint32_t ttc::BGOMap::BGOMAP_START_WDTH = 4;
const uint32_t ttc::BGOMap::BGOMAP_STOP_OFFS_TTCCI = 20; // channel 10
const uint32_t ttc::BGOMap::BGOMAP_STOP_OFFS_LTC = 28; // channel 10
const uint32_t ttc::BGOMap::BGOMAP_STOP_WDTH = 4;


ttc::BGOMap::BGOMap(
    GenericTTCModule& _module,
    const Address& _bgo_map_address,
    bool _is_ltc)
:
    module(_module),
    bgo_map_address(_bgo_map_address),
    bgomap(0),
    is_ltc(_is_ltc)
{}


uint32_t ttc::BGOMap::GetBGOMap() const
{
  readBgoMapFromHardware();
  return bgomap;
}


void ttc::BGOMap::SetBGOMap(const uint32_t val)
{
  bgomap = val;
  for (size_t i = 0; i < 5; ++i)
  {
    for (size_t j = i + 1; j < 6; ++j)
    {
      // Are any of the bits of val at j*4 .. j*4 + 3 set?
      if (MaskOut(val, 4, j * 4) != 0 &&
      // Are the bits at j*4 .. j*4+3 equal to the bits
      // i*4..i*4+3?
          MaskOut(val, 4, i * 4) == MaskOut(val, 4, j * 4))
      {
        LOG4CPLUS_WARN(
            module.getLogger(),
            "BGOMap::SetBGOMap(val=0x" << hex << val << dec << "): bits "
            "[" << i*4 << "," << i*4+4 << "] and [" << j*4 << "," << j*4+4 << "] "
            << "denote the same channel.");
      }
    }
  }
  writeBGOMapToHardware();
}


vector<string>
ttc::BGOMap::getChannelNames()
{
  vector<string> retval;

  size_t assigned_names_counter = 0;

  // loop over all BGO channels
  for (int8_t ch = 0; ch < nchannels; ++ch)
  {
    char dum[100];

    // Actually we do not need sprintf here (strcpy would do).
    if (ch == 1)
    {
      sprintf(dum, "BC0");
      ++assigned_names_counter;
    }
    else if (ch == Get_Resynch_BGOCh())
    {
      sprintf(dum, "Resync");
      ++assigned_names_counter;
    }
    else if (ch == Get_HardReset_BGOCh())
    {
      sprintf(dum, "HardReset");
      ++assigned_names_counter;
    }
    else if (ch == Get_ECntReset_BGOCh())
    {
      sprintf(dum, "EC0");
      ++assigned_names_counter;
    }
    else if (ch == Get_OCntReset_BGOCh())
    {
      sprintf(dum, "OC0");
      ++assigned_names_counter;
    }
    else if (ch == Get_Start_BGOCh())
    {
      sprintf(dum, "Start");
      ++assigned_names_counter;
    }
    else if (ch == Get_Stop_BGOCh())
    {
      sprintf(dum, "Stop");
      ++assigned_names_counter;
    }
    else if (ch == 2)
    {
      sprintf(dum, "TestEnable");
      ++assigned_names_counter;
    }
    else if (ch == 3)
    {
      sprintf(dum, "PrivateGap");
      ++assigned_names_counter;
    }
    else if (ch == 4)
    {
      sprintf(dum, "PrivateOrbit");
      ++assigned_names_counter;
    }
    else if (ch == 11)
    {
      sprintf(dum, "StartOfGap");
      ++assigned_names_counter;
    }
    else if (ch == 13)
    {
      sprintf(dum, "WarningTestEnable");
      ++assigned_names_counter;
    }
    else
      sprintf(dum, "Channel%d", ch);

    retval.push_back(dum);
  }

  if (assigned_names_counter != 12)
  {
    LOG4CPLUS_ERROR(
        module.getLogger(),
        "BGOMap::getChannelNames(): Wrong number of assigned BGO channel names found");

    for (int8_t i = 0; i < nchannels; ++i)
    {
      LOG4CPLUS_ERROR(
          module.getLogger(),
          "  BGO " << i << ")\t Name: '" << retval[i] << "'");
    }
  }

  return retval;
}


vector<string>
ttc::BGOMap::getChannelNameAlternatives(int8_t channel)
{
  vector<string> retval;

  if (channel == Get_Resynch_BGOCh())
  {
    retval.push_back("Resynch");
  }

  return retval;
}


int8_t ttc::BGOMap::Get_Resynch_BGOCh() const
{
  return GetBgoChannel(BGOMAP_RESYNC_WDTH, BGOMAP_RESYNC_OFFS);
}


int8_t ttc::BGOMap::Get_HardReset_BGOCh() const
{
  return GetBgoChannel(BGOMAP_HARDRESET_WDTH, BGOMAP_HARDRESET_OFFS);
}


int8_t ttc::BGOMap::Get_ECntReset_BGOCh() const
{
  return GetBgoChannel(BGOMAP_ECRST_WDTH, BGOMAP_ECRST_OFFS);
}


int8_t ttc::BGOMap::Get_OCntReset_BGOCh() const
{
  return GetBgoChannel(BGOMAP_OCRST_WDTH, BGOMAP_OCRST_OFFS);
}


int8_t ttc::BGOMap::Get_Start_BGOCh() const
{
  int8_t retval = GetBgoChannel(BGOMAP_START_WDTH, BGOMAP_START_OFFS);
  if (is_ltc && retval == 0)
    return 9;
  return retval;
}


int8_t ttc::BGOMap::Get_Stop_BGOCh() const
{
  if (is_ltc)
  {
    int8_t retval = GetBgoChannel(BGOMAP_STOP_WDTH, BGOMAP_STOP_OFFS_LTC);
    if (retval > 0)
      return retval;
    return 10;
  }
  else
    return GetBgoChannel(BGOMAP_STOP_WDTH, BGOMAP_STOP_OFFS_TTCCI);
}


void ttc::BGOMap::Set_Resynch_BGOCh(const uint32_t val)
{
  SetBGOChannel(val, BGOMAP_RESYNC_WDTH, BGOMAP_RESYNC_OFFS);
}


void ttc::BGOMap::Set_HardReset_BGOCh(const uint32_t val)
{
  SetBGOChannel(val, BGOMAP_HARDRESET_WDTH, BGOMAP_HARDRESET_OFFS);
}


void ttc::BGOMap::Set_ECntReset_BGOCh(const uint32_t val)
{
  SetBGOChannel(val, BGOMAP_ECRST_WDTH, BGOMAP_ECRST_OFFS);
}


void ttc::BGOMap::Set_OCntReset_BGOCh(const uint32_t val)
{
  SetBGOChannel(val, BGOMAP_OCRST_WDTH, BGOMAP_OCRST_OFFS);
}


void ttc::BGOMap::Set_Start_BGOCh(const uint32_t val)
{
  SetBGOChannel(val, BGOMAP_START_WDTH, BGOMAP_START_OFFS);
}


void ttc::BGOMap::Set_Stop_BGOCh(const uint32_t val)
{
  if (is_ltc)
  {
    SetBGOChannel(val, BGOMAP_STOP_WDTH, BGOMAP_STOP_OFFS_LTC);
  }
  else
  {
    SetBGOChannel(val, BGOMAP_STOP_WDTH, BGOMAP_STOP_OFFS_TTCCI);
  }
}


void ttc::BGOMap::readBgoMapFromHardware() const
{
  uint32_t dummy = module.Read(bgo_map_address, "(BGO_MAP)");
  if (dummy == 0)
  {
    LOG4CPLUS_ERROR(
        module.getLogger(),
        "BGOMap::readBgoMapFromHardware(): "
        "Reading 0x" << hex << dummy << " from hardware! (o.k. for DUMMY(64X) bus adapter)");
  }
  else
    bgomap = dummy;
}


void ttc::BGOMap::writeBGOMapToHardware() const
{
  module.Write(bgo_map_address, bgomap, "(BGO_MAP)");
}


int8_t ttc::BGOMap::GetBgoChannel(uint32_t num_bits, uint32_t offset) const
{
  readBgoMapFromHardware();
  return MaskOut(bgomap, num_bits, offset);
}


void ttc::BGOMap::SetBGOChannel(uint32_t val, uint32_t num_bits, uint32_t offset)
{
  bgomap = MaskIn(bgomap, val, num_bits, offset);
  writeBGOMapToHardware();
}
