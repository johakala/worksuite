#include "ttc/utils/VMEUtils.hh"

#include "hal/VMEHardwareAddress.hh"
#include "hal/AddressTableItem.hh"
#include "hal/VMEConfigurationSpaceHandler.hh"


// class ttc::VMEUtils - nested class DummyAddressTableReader

ttc::VMEUtils::DummyAddressTableReader::DummyAddressTableReader()
{
  itemPointerList.push_back(
      new HAL::AddressTableItem("DUMMY09", "Max address with AM 0x09",
          *new HAL::VMEHardwareAddress(0xfffffffc, 0x09, 4), 0xffffffff, 1, 1));

  itemPointerList.push_back(
      new HAL::AddressTableItem("DUMMY39", "Max address with AM 0x39", *new HAL::VMEHardwareAddress(0x7fffc, 0x39, 4),
          0xffffffff, 1, 1));
}


// class ttc::VMEUtils - nested class DummyVMEAddressTable

ttc::VMEUtils::DummyVMEAddressTable::DummyVMEAddressTable()
:
    HAL::VMEAddressTable("", *(DummyReader.instance()))
{}


// class ttc::VMEUtils - static

Singleton<ttc::VMEUtils::DummyAddressTableReader> ttc::VMEUtils::DummyReader;

Singleton<ttc::VMEUtils::DummyVMEAddressTable> ttc::VMEUtils::DummyAddressTableTemp;


uint32_t ttc::VMEUtils::configread(
    HAL::VMEConfigurationSpaceHandler& handler,
    uint32_t slot,
    std::string item,
    uint32_t offset)
{
  uint32_t temp;
  handler.configRead(item, slot, &temp, offset);
  return temp;
}


std::string ttc::VMEUtils::configreadstr(
    HAL::VMEConfigurationSpaceHandler& handler,
    uint32_t slot,
    const std::string& start,
    const std::string& end)
{
  uint32_t startadd = configread(handler, slot, start);
  uint32_t endadd = configread(handler, slot, end);
  if (startadd == 0 || endadd == 0)
  {
    return "";
  }
  std::vector<unsigned char> buf;
  handler.readROMBlock(slot, startadd, endadd, &buf);
  return std::string(buf.begin(), buf.end());
}


std::string ttc::VMEUtils::configreadzstr(
    HAL::VMEConfigurationSpaceHandler& handler,
    uint32_t slot,
    const std::string& start)
{
  // Read a 32 bit integer.
  uint32_t addr = configread(handler, slot, start);

  if (addr == 0)
    return "";
  std::string s;
  char c;
  try
  {
    while ((c = configread(handler, slot, "romStart", addr)) != '\0')
    {
      s += c;
      addr += 4;
    }
  }
  catch (const HAL::BusAdapterException&)
  {
    // ignore it
  }
  return s;
}


std::string ttc::VMEUtils::make_str(uint32_t src)
{
  const char * s = reinterpret_cast<const char *>(&src);
  std::string ret;
  for (size_t i = 0; i < 4; ++i)
  {
    if (!s[3 - i])
      break;
    ret += s[3 - i];
  }
  return ret;
}


uint32_t ttc::VMEUtils::make_ulong(const std::string& src)
{
  uint32_t ret = 0;
  for (size_t i = 0; i < 4; ++i)
  {
    ret <<= 8;
    ret += (i < src.length() ? src[i] : 0);
  }
  return ret;
}
