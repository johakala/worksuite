#include "ttc/utils/SimpleHtmlTable.hh"

#include "ttc/utils/CyclicStringArray.hh"

#include <cassert>


using namespace std;


ttc::SimpleHtmlTable::SimpleHtmlTable()
:
    row_offset(0),
    col_offset(0),
    table_tag("table")
{}


size_t ttc::SimpleHtmlTable::GetNumRows() const
{
  return cells.size();
}


size_t ttc::SimpleHtmlTable::GetNumColumns(size_t row_number) const
{
  assert(row_number < cells.size());
  return cells[row_number].size();
}


size_t ttc::SimpleHtmlTable::GetTableWidthInCells() const
{
  size_t retval = 0;
  size_t num_rows = GetNumRows();

  for (size_t i = 0; i < num_rows; ++i)
  {
    retval = std::max(retval, GetNumColumns(i));
  }

  return retval;
}


ttc::SimpleHtmlTable::CellData&
ttc::SimpleHtmlTable::GetCell(size_t row, size_t column, bool apply_offset)
{
  if (apply_offset)
  {
    row += row_offset;
    column += col_offset;
  }

  while (row >= GetNumRows())
  {
    cells.push_back(vector<CellData>());
  }

  vector<CellData>& this_row = cells[row];
  while (column >= this_row.size())
  {
    this_row.push_back(CellData());
  }

  last_row_accessed = row;
  last_col_accessed = column;

  return this_row[column];
}


ttc::SimpleHtmlTable::CellData&
ttc::SimpleHtmlTable::last()
{
  return GetCell(last_row_accessed, last_col_accessed, false);
}


ttc::SimpleHtmlTable::CellData&
ttc::SimpleHtmlTable::operator()(size_t row, size_t column)
{
  return GetCell(row, column, true);
}


void ttc::SimpleHtmlTable::InsertEmptyRowBefore(size_t row, bool apply_offset)
{
  size_t cols = GetTableWidthInCells();
  size_t num_rows = GetNumRows();

  if (apply_offset)
  {
    row += row_offset;
  }

  // We need to move rows row..num_rows-1 towards one higher row
  // number (this only makes sense if row is <= num_rows-1,
  // i.e. row + 1 <= num_rows, i.e. row < num_rows).
  if (row < num_rows)
  {
    // Move the rows.
    for (size_t cur_row = num_rows; cur_row > row; --cur_row)
    {
      for (size_t col = 0; col < cols; ++col)
      {
        GetCell(cur_row, col, false) = GetCell(cur_row - 1, col, false);
      }
    }
  }

  ClearRow(row);
}


void ttc::SimpleHtmlTable::InsertEmptyColumnBefore(size_t column, bool apply_offset)
{
  size_t num_cols = GetTableWidthInCells();
  size_t rows = GetNumRows();

  if (apply_offset)
  {
    column += col_offset;
  }

  // We need to move columns column..num_cols-1 towards one higher
  // column number (this only makes sense if column is <=
  // num_cols-1, i.e. column + 1 <= num_cols, i.e. column <
  // num_cols).
  if (column < num_cols)
  {
    // Move the columns.
    for (size_t cur_col = num_cols; cur_col > column; --cur_col)
    {
      for (size_t row = 0; row < rows; ++row)
      {
        GetCell(row, cur_col, false) = GetCell(row, cur_col - 1, false);
      }
    }
  }

  ClearColumn(column);
}


void ttc::SimpleHtmlTable::Print(ostream& os)
{
  table_tag.PrintOpening(os);

  size_t num_rows = GetNumRows();
  size_t num_cols = GetTableWidthInCells();

  for (size_t row = 0; row < num_rows; ++row)
  {
    HtmlTag row_tag("tr");

    row_tag.PrintOpening(os);

    for (size_t col = 0; col < num_cols; ++col)
      // Do not apply the row and column offset.
      GetCell(row, col, false).Print(os);

    row_tag.PrintClosing(os);
    os << endl;
  }

  table_tag.PrintClosing(os);
}


void ttc::SimpleHtmlTable::SetAttrForWholeRow(
    size_t row,
    const string &name,
    const string &value,
    bool apply_offset)
{
  if (apply_offset)
    row += row_offset;

  assert(row < GetNumRows());
  size_t num_cols = GetTableWidthInCells();

  for (size_t col = 0; col < num_cols; ++col)
    GetCell(row, col, false).SetAttr(name, value);
}


void ttc::SimpleHtmlTable::PushOffsets()
{
  offset_stack.push_back(pair<size_t, size_t>(row_offset, col_offset));
}


void ttc::SimpleHtmlTable::PopOffsets()
{
  assert(offset_stack.size() > 0);

  row_offset = offset_stack.back().first;
  col_offset = offset_stack.back().second;

  offset_stack.pop_back();
}


void ttc::SimpleHtmlTable::ApplyAlternatingRowColors(size_t first_row, const char* line_colours[])
{
  CyclicStringArray colours(line_colours);

  size_t num_rows = GetNumRows();

  for (size_t row = first_row; row < num_rows; ++row)
  {
    SetAttrForWholeRow(row, "bgcolor", colours.next(), false);
  }
}


const ttc::SimpleHtmlTable::CellData&
ttc::SimpleHtmlTable::GetCellIfExisting(size_t row, size_t column, bool apply_offset) const
{
  if (apply_offset)
  {
    row += row_offset;
    column += col_offset;
  }

  static CellData empty;

  if (row >= GetNumRows())
  {
    return empty;
  }

  const vector<CellData>& this_row = cells[row];

  if (column >= this_row.size())
  {
    return empty;
  }

  return this_row[column];
}

void ttc::SimpleHtmlTable::ClearRow(size_t row)
{
  size_t cols = GetTableWidthInCells();
  for (size_t col = 0; col < cols; ++col)
  {
    GetCell(row, col, false).content = "&nbsp;";
    GetCell(row, col, false).ClearAllAttrs();
    GetCell(row, col, false).ClearAllStyles();
  }
}


void ttc::SimpleHtmlTable::ClearColumn(size_t column)
{
  size_t rows = GetNumRows();
  for (size_t row = 0; row < rows; ++row)
  {
    GetCell(row, column, false).content = "&nbsp;";
    GetCell(row, column, false).ClearAllAttrs();
    GetCell(row, column, false).ClearAllStyles();
  }
}


void ttc::SimpleHtmlTable::PasteOtherTable(
    size_t row,
    size_t column,
    const SimpleHtmlTable &other_table,
    bool apply_offset,
    bool apply_offset_to_other)
{
  if (apply_offset)
  {
    row += row_offset;
    column += col_offset;
  }

  size_t other_num_rows = other_table.GetNumRows();
  size_t other_num_cols = other_table.GetTableWidthInCells();

  for (size_t cur_row = 0; cur_row < other_num_rows; ++cur_row)
  {
    for (size_t cur_col = 0; cur_col < other_num_cols; ++cur_col)
    {
      GetCell(row + cur_row, column + cur_col, false) = other_table.GetCellIfExisting(cur_row, cur_col,
          apply_offset_to_other);
    }
  }
}
