#include "ttc/monitoring/version.h"

#include "config/version.h"
#include "hal/generic/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xgi/version.h"
#include "xoap/version.h"

GETPACKAGEINFO(ttcmonitoring)

void ttcmonitoring::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(generichal);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdaq);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(xgi);
  CHECKDEPENDENCY(xoap);
}

std::set<std::string, std::less<std::string> > ttcmonitoring::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;
  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, generichal);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);
  ADDDEPENDENCY(dependencies, xdaq);
  ADDDEPENDENCY(dependencies, xdata);
  ADDDEPENDENCY(dependencies, xgi);
  ADDDEPENDENCY(dependencies, xoap);

  return dependencies;
}
