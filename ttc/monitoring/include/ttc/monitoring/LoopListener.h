#ifndef _ttc_monitoring_looplistener_h_
#define _ttc_monitoring_looplistener_h_


#include "ttc/monitoring/DataListener.h"

#include "toolbox/task/TimerListener.h"

#include <string>


namespace toolbox
{
namespace task { class Timer; }
}


namespace ttc
{

/**
 * A LoopListener is a DataListener that monitors registered
 * components and their items continuously in a loop, in a
 * dedicated thread. For basic concepts and functionality see
 * documentation of base class DataListener.
 *
 * USAGE:
 * Derive your own monitoring class from LoopListener and
 * implement the following pure virtual functions:
 *   virtual void addComponents();
 *   virtual void addItems();
 *   virtual bool refreshItems(const Component& component);
 *
 * NOTE:
 * A LoopListener continuously executes, for all components,
 * the code that you put in refreshItems().
 *
 * It may fit your problem best if you monitor a single
 * component with a single item, whose data source is
 * accessible with a mostly blocking function call, e.g. like
 * in receiving data from a socket via recv.
 *
 * In contrast, if you can poll data from your source without
 * having to wait, at any time, then a LoopListener will
 * cause high CPU consumption.
 */
class LoopListener
:
  public DataListener,
  public toolbox::task::TimerListener
{

public:

  /**
   * Constructs a LoopListener for XDAQ application app.
   *
   * See documentation of class DataListener for base class
   * parameters (app, infospaceBaseName, autoInit).
   */
  LoopListener(
      xdaq::Application* app,
      const std::string& infospaceBaseName,
      bool autoInit);

  virtual ~LoopListener();

private:

  //! The name of the timer used to start the monitoring thread.
  std::string timerName_;

  //! The timer used to start the monitoring thread.
  toolbox::task::Timer* timer_;

  //! The scheduled time to start the monitoring thread.
  double startTimeSecs_;

  /**
   * The time when XMAS is presumably ready.
   * Before this time, every update is forced even if nothing
   * changes, such that even a static value shows up in XMAS.
   */
  double xmasReadyTimeSecs_;


  /**
   * Implements launching of the monitoring thread,
   * which happens at the end of DataListener::init().
   */
  virtual void run();

  /**
   * The method running the endless loop.
   * Launched at the scheduled start time via the Timer.
   */
  virtual void timeExpired(toolbox::task::TimerEvent& e);
};

}


#endif
