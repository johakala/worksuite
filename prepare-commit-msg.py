#!/usr/bin/env python
import sys, re
from subprocess import check_output

commit_msg_filepath = sys.argv[1]

#branch == "HEAD" in case of detached head
branch = check_output(['git', 'rev-parse', '--symbolic-full-name', '--abbrev-ref', 'HEAD']).strip()

branch_regex = '^feature_(\d+)(_\w+)?$'
#Do nothing in case we are not on a feature branch
if re.match(branch_regex, branch):
	issue = re.match(branch_regex, branch).group(1)
	with open(commit_msg_filepath, 'r+') as fh:
		commit_msg = fh.read()
		commit_msg_regex = '^references\s#(\d+):\s.+'
		#Avoiding duplication of prefix if it is already present
		if not re.match(commit_msg_regex, commit_msg):
			fh.seek(0, 0)
			#Adding a prefix with an issue number
			new_commit_msg = 'references #%s: %s' % (issue, commit_msg)
			fh.write(new_commit_msg)
