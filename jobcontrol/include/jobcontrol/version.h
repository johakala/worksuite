// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * JobControl is programmed by Alexander Oh                              *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jobcontrol_version_h_
#define _jobcontrol_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define JOBCONTROL_VERSION_MAJOR 2
#define JOBCONTROL_VERSION_MINOR 7
#define JOBCONTROL_VERSION_PATCH 3
#undef JOBCONTROL_PREVIOUS_VERSIONS


//
// Template macros
//
#define JOBCONTROL_VERSION_CODE PACKAGE_VERSION_CODE(JOBCONTROL_VERSION_MAJOR,JOBCONTROL_VERSION_MINOR,JOBCONTROL_VERSION_PATCH)
#ifndef JOBCONTROL_PREVIOUS_VERSIONS
#define JOBCONTROL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(JOBCONTROL_VERSION_MAJOR,JOBCONTROL_VERSION_MINOR,JOBCONTROL_VERSION_PATCH)
#else 
#define JOBCONTROL_FULL_VERSION_LIST  JOBCONTROL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(JOBCONTROL_VERSION_MAJOR,JOBCONTROL_VERSION_MINOR,JOBCONTROL_VERSION_PATCH)
#endif 
namespace jobcontrol
{
	const std::string package  =  "jobcontrol";
	const std::string versions =  JOBCONTROL_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Alexander Oh, Luciano Orsini, Johannes Guleber, Roland Moser, Andrea Petrucci, Andrew Forrest, Dainius Simelevicius";
	const std::string summary = "Controls execution of XDAQ services";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/JobControl";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
