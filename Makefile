# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2018, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# Project level Makefile
#
##

BUILD_HOME:=$(shell pwd)

ifndef PACKAGES
PACKAGES= \
	extern/oracle \
	extern/dim \
	extern/smi \
	extern/yui \
	extern/caen/a2818 \
	extern/caen/a3818 \
	extern/caen/CAENUSBdrvB \
	extern/caen/caenvmelib \
	extern/caen/caencomm \
	extern/caen/caenbridgeupgrade \
	extern/caen/caenupgrader \
	xdaq2rc \
	interface/evb \
	interface/shared \
	evb \
	jobcontrol \
	xpci/drv \
	xpci \
	hal/generic \
	hal/utilities \
	hal/busAdapter/dummy \
	hal/busAdapter/caen \
	hal/busAdapter/pci \
	pheaps/drv/cmem_rcc \
	pheaps \
	jal/jtagChain \
	jal/jtagController \
	jal/jtagSVFSequencer \
	ttc/utils \
	ttc/monitoring \
	ttc/ttcci \
	tts/ttsbase \
	tts/atts \
	tts/cpcibase \
	tts/fmmtd \
	tts/ipcutils \
	tts/fmm \
	tts/fmmcontroller \
	tts/fmmdbi \
	tts/fmmtester \
	d2s/utils \
	d2s/fedemulator \
	d2s/gtpe \
	d2s/gtpecontroller \
	d2s/firmwareloader \
	fedstreamer \
	fedkit \
	ferol \
	ferol40 \
	psx/sapi \
	psx/mapi \
	psx \
	psx/watchdog \
	sentinel \
	sentinel/utils \
	sentinel/probe \
	sentinel/sentineld \
	sentinel/tester \
	sentinel/bridge2g \
	sentinel/spotlight2g \
	sentinel/spotlightocci \
	sentinel/arc/utils \
	sentinel/arc \
	tstore/utils \
	tstore/client \
	tstore \
	tstore/api \
	xmas/admin \
	xmas/broker2g \
	xmas/heartbeat \
	xmas/heartbeat/probe \
	xmas/heartbeat/heartbeatd \
	xmas/las2g \
	xmas/collector2g \
	xmas/utils \
	xmas/sensord \
	xmas/probe \
	xmas/tester \
	xmas/bridge2g \
	xmas/slash2g \
	xmas/store2g \
	xmas/smarthub \
	ibvla \
	pt/udapl \
	pt/ibv \
	gevb2g \
	es/api \
	es/xtreme \
	es/xbeat \
	elastic/api \
	elastic/timestream \
	amc13controller

endif
export PACKAGES

BUILD_SUPPORT=build
export BUILD_SUPPORT

PROJECT_NAME=worksuite
export PROJECT_NAME

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules

Project=$(PROJECT_NAME)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Packages=$(PACKAGES)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules

