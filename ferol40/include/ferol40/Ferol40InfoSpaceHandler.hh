#ifndef __Ferol40InfoSpaceHandler
#define __Ferol40InfoSpaceHandler

#include "xdaq/Application.h"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"
#include "d2s/utils/DataTracker.hh"

namespace ferol40 {

    class Ferol40InfoSpaceHandler : public utils::InfoSpaceHandler 
    {
    public:
        Ferol40InfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater );
        void registerTrackerItems( utils::DataTracker &dataTracker );
    };
}

#endif /* __Ferol40InfoSpaceHandler */
