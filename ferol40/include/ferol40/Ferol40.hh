#ifndef __Ferol40
#define __Ferol40

#include "ferol40/FirmwareChecker.hh"
#include "ferol40/ApplicationInfoSpaceHandler.hh"
#include "ferol40/StatusInfoSpaceHandler.hh"
#include "ferol40/Ferol40InfoSpaceHandler.hh"
#include "ferol40/TCPStreamInfoSpaceHandler.hh"
#include "ferol40/Ferol40Monitor.hh"
#include "d2s/utils/Exception.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"
#include "ferol40/HardwareLocker.hh"
#include "ferol40/MdioInterface.hh"
#include "xdaq/Application.h"
#include "hal/PCIDevice.hh"
#include "hal/PCILinuxBusAdapter.hh"
#include "tts/ipcutils/SemaphoreArray.hh"
#include "log4cplus/logger.h"
#include "toolbox/BSem.h"
#include "d2s/utils/DataTracker.hh"
#include "ferol40/SlinkExpressCore.hh"
#include "ferol40/ConfigSpaceSaver.hh"

namespace ferol40
{
    class Ferol40 : public utils::InfoSpaceUpdater, public ConfigSpaceSaver
    {
    public:

        Ferol40( xdaq::Application *xdaq,
               ApplicationInfoSpaceHandler &appIS,
               StatusInfoSpaceHandler &statusIS,
               InputStreamInfoSpaceHandler &inputIS,
	       Ferol40Monitor &monitor,
               HardwareLocker &hwLocker );
        ~Ferol40();

        void instantiateHardware() throw( utils::exception::Exception );
        void configure() throw( utils::exception::Ferol40Exception );
        void enable() throw( utils::exception::Ferol40Exception);
        void stop() throw( utils::exception::Ferol40Exception );
        void halt() throw( utils::exception::Ferol40Exception);
        void suspendEvents() throw( utils::exception::Ferol40Exception);
        void resumeEvents() throw( utils::exception::Ferol40Exception);

        bool buffersAreEmpty();

        bool updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo = 0 );//JRF NOTE, the default is streamNo 0, if you don't specify, you won't get the right stream.

        void controlSerdes( bool on ) throw ( utils::exception::Exception );

        // used by the firmwarechecker to change FRL firmware type on the fly
        void hwlock();
        void hwunlock();

        //        // public since used by SOAP callback for testing
        //        void setupEventGen() throw ( utils::exception::Ferol40Exception );

        // read the SFP interface
        void readSFP( uint32_t vitesseNo ) throw( utils::exception::Ferol40Exception );
        // Check if the i2C Bus operation terminated correctly or if the bus is idle
        std::string checkI2CState( uint32_t vitesseNo ) throw (utils::exception::Ferol40Exception);
        
        // Start an I2C command. The other command registers must have been set beforehand (8004, 8002 for block read)
        void i2cCmd( uint32_t vitesseNo, uint32_t value ) throw (utils::exception::Ferol40Exception);
        
        //JRF TODO, delete these lines... I think vitesse is not needed for ferol40
        //Reset the Vitesse chip. This reset line is pulled to the value in the argument. Reset active means '0'. 
        //void resetVitesse( uint32_t reset = 0 ) throw( utils::exception::Ferol40Exception );

        // toggle the DAQ on DAQ off of the optical slink
        int32_t daqOff( uint32_t link, bool generatorOn = false );
        int32_t daqOn( uint32_t link );
        int32_t resyncSlinkExpress( uint32_t link );

        // The hardware debugger needs access to the slink express core to read out the registres of the SLINK express.
        SlinkExpressCore *getSlinkExpressCore();

        HAL::HardwareDeviceInterface * getFerol40Device();

        uint32_t getSlot();

    private:
        void createFerol40Device() throw ( utils::exception::Exception );
        void shutdownHwDevice() throw ( utils::exception::Ferol40Exception );
        void openConnection( uint32_t stream );

        std::string makeIPString( uint32_t ip ) throw( utils::exception::Ferol40Exception );

        double getTemp( u_char* data );
        double getVoltage( u_char* data );
        double getCurrent( u_char* data );
        double getPower( u_char* data );
        bool catchTcpError( uint32_t stream, std::stringstream &msg, uint32_t &tcpStatus );

        
        /**
         *
         *     @short Do an ARP-probe.
         *            
         *            This  function checks that  the MAC  addresse and  the IP 
         *            addresse of  this Ferol40 are  unique on the  network. This 
         *            is done by sending an  ARP request for IP address of this
         *            Ferol40  on  the network.  (The  firmware  tries this  four 
         *            times). An answer to  this request indicates a problem on
         *            the  network, since  some other  device has  the  same IP 
         *            addresse.  In this  case the  Ferol40 goes  into  the Error 
         *            state.
         *
         *     @param doProbe: if false no ARP package is sent, but the registers
         *            indicating a conflict on the network are analysed. Note 
         *            that the statemachine analysing replies to ARP requests 
         *            is running continuously in the FEROL40 and not only at the
         *            time, an arp request is done! Setting this parameter to 
         *            true allows this function to be used for updating infospaces
         *            in the monitoring thread.
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void doArpProbe( uint32_t stream_no, bool doProbe = true );

        // makes an 4 byte value which can be written into the register which holds the
        // destination IP addresse. If the hoststr is a ip4 dot-notation of an ip address
        // it is converted directly. In all other cases, the hoststr is interpreted as a 
        // fully qualified hostname with network extension, and gethostbyname is used to
        // retrieve the ip address. 


        /**
         *
         *     @short Make a 4 byte IP number from a string.
         *            
         *            Makes  a 4  byte value  which  can be  written into  the
         *            register which holds the destination IP addresse. If the
         *            hoststr is in ip4 dot-notation it is converted directly.
         *            In  all other  cases, the  hoststr is  interpreted  as a
         *            fully  qualified hostname  with  network extension,  and
         *            gethostbyname is used retrieve the ip.
         *             
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        uint32_t makeIPNum( std::string hoststr ) throw( utils::exception::Ferol40Exception );

        std::string uint64ToMac( uint64_t mac );

    private:
        Logger logger_;
        ApplicationInfoSpaceHandler &appIS_;
        StatusInfoSpaceHandler &statusIS_;
        Ferol40Monitor &monitor_;
        HardwareLocker &hwLocker_;
        Ferol40InfoSpaceHandler ferol40IS_;
        TCPStreamInfoSpaceHandler tcpIS_;
	//JRF TODO, check if we don't need these in here since they are in the Ferol40Controller
	//StreamConfigInfoSpaceHandler streamIS_;
	InputStreamInfoSpaceHandler &inputIS_;
        HAL::PCILinuxBusAdapter busAdapter_;
        toolbox::BSem deviceLock_;
        //DataTracker dataTracker_;
        std::vector<utils::DataTracker> tcpStreamDataTrackers_;
        std::vector<utils::DataTracker> inputStreamDataTrackers_;
        SlinkExpressCore *slCore_P;
        //MdioInterface *mdioInterface_P;
        bool hwCreated_;

        HAL::PCIDevice *ferol40Device_P;
        HAL::PCIAddressTable *ferol40Table_P;
        HAL::PCIAddressTable *slinkExpressTable_P;

        uint32_t slot_;
        std::vector<bool> streams_;
        std::string operationMode_;
        std::vector<std::string> dataSource_;

        FirmwareChecker *fwChecker_P;

        uint32_t ferol40ConfigSpace_[16]; //JRF TODO convert to vector
        std::vector<uint32_t> streamBpCounterHigh_;
        std::vector<uint32_t> streamBpCounterOld_; 

    };
}

#endif /* __Ferol40 */

            
