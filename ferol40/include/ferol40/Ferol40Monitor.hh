#ifndef __Ferol40Monitor
#define __Ferol40Monitor

#include "xdaq/Application.h"
#include "d2s/utils/Monitor.hh"
#include "ferol40/ApplicationInfoSpaceHandler.hh"
#include "ferol40/StreamConfigInfoSpaceHandler.hh"
#include "ferol40/Ferol40InfoSpaceHandler.hh"
#include "ferol40/StatusInfoSpaceHandler.hh"
#include "ferol40/InputStreamInfoSpaceHandler.hh"
#include "ferol40/TCPStreamInfoSpaceHandler.hh"


/************************************************************************
 *
 *
 *     @short 
 *
 *       @see 
 *    @author $Author: schwick $
 *   @version $Revision: 1.6 $
 *      @date $Date: 2001/03/06 17:07:51 $
 *
 *      Modif 
 *
 **//////////////////////////////////////////////////////////////////////

namespace ferol40 {
    class Ferol40Monitor : public utils::Monitor 
    {
    public:
        Ferol40Monitor( Logger &logger, ApplicationInfoSpaceHandler &appIS, xdaq::Application *xdaq, utils::ApplicationStateMachineIF &fsm );

        void addInfoSpace( ferol40::StreamConfigInfoSpaceHandler *is );
        void addInfoSpace( ferol40::Ferol40InfoSpaceHandler *is );
        void addInfoSpace( ferol40::StatusInfoSpaceHandler *is );
        void addInfoSpace( ferol40::InputStreamInfoSpaceHandler *is );
        void addInfoSpace( ferol40::TCPStreamInfoSpaceHandler *is );

    private:
        void addApplInfoSpaceItemSets( ferol40::ApplicationInfoSpaceHandler *is );
    };
}

#endif /* __Ferol40Monitor */
