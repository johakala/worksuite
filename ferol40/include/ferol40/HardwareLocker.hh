#ifndef __Ferol40HardwareLocker
#define __Ferol40HardwareLocker
#include "d2s/utils/HardwareLocker.hh"
#include "ferol40/ApplicationInfoSpaceHandler.hh"
#include "ferol40/StatusInfoSpaceHandler.hh"

namespace ferol40
{
    class HardwareLocker : public utils::HardwareLocker {

    public:

        HardwareLocker( Logger &logger,
                        ApplicationInfoSpaceHandler &appIS,
                        StatusInfoSpaceHandler &statusIS);

        bool lock();

    };
}

#endif /* __HardwareLocker */
