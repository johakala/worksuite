#ifndef __FrlDataSource
#define __FrlDataSource

#include "ferol40/DataSourceIF.hh"
#include "ferol40/ferol40Constants.h"
#include <sstream>
#include "d2s/utils/Exception.hh"

namespace ferol40
{
    class FrlDataSource : public DataSourceIF {
    public:
        FrlDataSource( HAL::HardwareDeviceInterface *device_P,
                       utils::InfoSpaceHandler &appIS,
                       Logger logger );
        void setDataSource() const;
    };
}

#endif /* __FrlDataSource */
