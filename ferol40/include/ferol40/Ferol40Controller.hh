// $Id: Ferol40Controller.hh,v 1.64 2009/04/29 10:25:54 cschwick Exp $
#ifndef _ferol40_Ferol40Controller_h_
#define _ferol40_Ferol40Controller_h_

#include <vector>

#include "log4cplus/logger.h"

#include "d2s/utils/SOAPFSMHelper.hh"
#include "d2s/utils/Exception.hh"
#include "ferol40/ApplicationInfoSpaceHandler.hh"
#include "ferol40/StatusInfoSpaceHandler.hh"
//#include "ferol40/Ferol40StreamInfoSpaceHandler.hh"
#include "ferol40/InputStreamInfoSpaceHandler.hh"
#include "ferol40/StreamConfigInfoSpaceHandler.hh"

//#include "ferol40/Frl.hh"
#include "ferol40/Ferol40.hh"
#include "ferol40/Ferol40WebServer.hh"
#include "ferol40/Ferol40Monitor.hh"
#include "ferol40/HardwareLocker.hh"
#include "ferol40/Ferol40StateMachine.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "xdata/ActionListener.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/exception/Exception.h"

#include "xoap/MessageReference.h"

#include "xdata/UnsignedInteger32.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Table.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/BSem.h"
#include "toolbox/rlist.h"

#include "hal/linux/StopWatch.hh"

// include for RUNControlState Notifier
#include "xdaq2rc/RcmsStateNotifier.h"

namespace ferol40 {

    class Ferol40Controller : public xdaq::Application, public
xgi::framework::UIManager, xdata::ActionListener, utils::InfoSpaceUpdater
    {
    public:
    
        XDAQ_INSTANTIATOR();
    
        Ferol40Controller(xdaq::ApplicationStub* stub)
            throw (xdaq::exception::Exception);

        virtual ~Ferol40Controller();


        void ConfigureAction(toolbox::Event::Reference e)
            throw( toolbox::fsm::exception::Exception );
        void EnableAction(toolbox::Event::Reference e)        
            throw( toolbox::fsm::exception::Exception );
        void StopAction(toolbox::Event::Reference e)
            throw( toolbox::fsm::exception::Exception );
        void SuspendAction(toolbox::Event::Reference e)
            throw( toolbox::fsm::exception::Exception );
        void ResumeAction(toolbox::Event::Reference e)
            throw( toolbox::fsm::exception::Exception );
        void HaltAction(toolbox::Event::Reference e)
            throw( toolbox::fsm::exception::Exception );
        void FailAction(toolbox::Event::Reference e);

        void dumpHardwareRegisters();

        xoap::MessageReference changeState( xoap::MessageReference msg )
            throw (xoap::exception::Exception);

        virtual xoap::MessageReference ParameterSet( xoap::MessageReference msg ) 
            throw (xoap::exception::Exception);

        xoap::MessageReference laserOn( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception);

        xoap::MessageReference laserOff( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception);

        xoap::MessageReference l0DaqOn( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception);

        xoap::MessageReference l0DaqOff( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception);

        xoap::MessageReference readItem( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception); 

        xoap::MessageReference writeItem( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception); 

        xoap::MessageReference softTrigger( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception); 

        xoap::MessageReference sfpStatus( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception);

        //JRF removing becuase no longer needed:
        //xoap::MessageReference resetVitesse( xoap::MessageReference msg ) 
        //    throw( xoap::exception::Exception);

        xoap::MessageReference resetMonitoring( xoap::MessageReference msg )
            throw( xoap::exception::Exception );

        xoap::MessageReference dumpHardwareRegisters( xoap::MessageReference msg )
            throw( xoap::exception::Exception );

        bool updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo );

    protected:
        void actionPerformed( xdata::Event &e );

    private:

        void notifyRCMS( toolbox::fsm::FiniteStateMachine & fsm, std::string msg );
        void monitoringWebPage( xgi::Input *in, xgi::Output *out );
        void debugFerol40( xgi::Input *in, xgi::Output *out);
        void debugFrl( xgi::Input *in, xgi::Output *out);
        void debugSlinkExpress( xgi::Input *in, xgi::Output *out);
        void testPage( xgi::Input *in, xgi::Output *out);
        void debugBridge( xgi::Input *in, xgi::Output *out);
        void jsonUpdate( xgi::Input *in, xgi::Output *out );
        void jsonInfoSpaces( xgi::Input *in, xgi::Output *out );
        void xdaqWebPage( xgi::Input *in, xgi::Output *out );
        void expertDebugging( xgi::Input *in, xgi::Output *out);
        std::string fillDocumentation();

    private:
        // To be initialized before the constructor runs
        Logger logger_;
        HAL::StopWatch timer_;
        //JRF we change the application infospace so that it is made up of one global application infospace plus a vector of 4 streamConfig Info Spaces.
	ApplicationInfoSpaceHandler appIS_; //This is the global applicaiton infospace for the configuration
	//JRF TODO, decide the best way to do this: probably have a separate class called StreamConfigInfoSpace which creates its own infospace distinct from the application infospace
	// then use a translation class which maps between the application infospace and the streamConfigInfoSpace.:q
	//
        StatusInfoSpaceHandler statusIS_;
        InputStreamInfoSpaceHandler inputIS_;
	StreamConfigInfoSpaceHandler streamConfigIS_;
        Ferol40StateMachine fsm_;
        Ferol40Monitor monitor_;
        HardwareLocker hwLocker_;
        // Frl frl_; //< Controls the FRL hardware.
        Ferol40 ferol40_; //< Controls the FEROL40/MOL hardware.
	bool changingState_;
        //Ferol40StateMachine *fsmP_;
        Ferol40WebServer *webServer_P;
        
        std::string operationMode_;
        std::string dataSource_;
        std::string errorString_;
        uint32_t instance_;

        std::string htmlDocumentation_;
    };

}
#endif /* __Ferol40Controller */
