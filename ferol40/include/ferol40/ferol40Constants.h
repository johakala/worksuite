#ifndef __Ferol40Constants
#define __Ferol40Constants

#define NB_STREAMS 0x04 //defines the total number of available streams on the ferol40

#define FEROL40_VENDORID 0xecd6
#define FEROL40_DEVICEID 0xFEA1

//#define MOL_VENDORID 0xecd6
//#define MOL_DEVICEID 0x1124

#define FEROL40_ADDRESSTABLE_FILE "/opt/xdaq/htdocs/ferol40/html/FEROL40AddressTable.dat"
#define SLINKEXPRESS_ADDRESSTABLE_FILE "/opt/xdaq/htdocs/ferol40/html/SlinkExpressAddressTable.dat"

#define FEROL40_DAQ_FIRMWARE_TYPE 0xfea
#define FEROL40_SLINKEXPRESS_FIRMWARE_TYPE 0xfea // now there is one firmware for slink and slinkexpress operation. Not used anymore.

#define FEROL40_DAQ_FIRMWARE_MIN_VERSION 0x0016            // min firmware version for Ferol40
#define FEROL40_SLINKEXPRESS_FIRMWARE_MIN_VERSION 0x0000 // irrelevant since only fea is in use: min firmware version for SLINKExpress

// Operation modes of the Ferol40
#define FEROL40_MODE "FEROL40_MODE" // DAQ with the 6G or 10G optical links of the Ferol40
#define SCAN_CRATE "SCAN_CRATE"

// Data Source for the various operation modes
#define GENERATOR_SOURCE "GENERATOR_SOURCE"
#define L10G_SOURCE "L10G_SOURCE"
#define L10G_CORE_GENERATOR_SOURCE "L10G_CORE_GENERATOR_SOURCE"

// When running with a 'real' input (i.e. not with an internal event generator)
// the input can be connected to the FED or to an event generator in the core.
// the event generator of the core is controlled by the Ferol40.
//#define SLINK_FED_MODE                "SLINK_FED_MODE"             // used to read data from the fed (normal case)
//#define SLINK_CORE_GENERATOR_MODE     "SLINK_CORE_GENERATOR_MODE"  // used to generate data in the core generator of the sender core.

// These options are important for the Ferol40 Emulator mode
#define FEROL40_AUTO_TRIGGER_MODE "FEROL40_AUTO_TRIGGER_MODE"
#define FEROL40_EXTERNAL_TRIGGER_MODE "FEROL40_EXTERNAL_TRIGGER_MODE"
#define FEROL40_SOAP_TRIGGER_MODE "FEROL40_SOAP_TRIGGER_MODE"

#define LOCKFILE "/dev/xpci"
#define LOCK_PROJECT_ID 'f'

#endif /* __Ferol40Constants */
