#ifndef __DataTracker
#define __DataTracker

#include "d2s/utils/InfoSpaceHandler.hh"
#include "hal/PCIDevice.hh"
#include <tr1/unordered_map>

namespace ferol40
{
    class utils::DataTracker {
    public:

        enum UpdateType { HW32, HW64, IF32, IF64 };

        utils::DataTracker(utils::InfoSpaceHandler &is,
		    //InfoSpaceHandler &appIS,
                    HAL::PCIDevice **hwpp );

        // returns != 0 on error
        uint32_t registerRateTracker( std::string name, std::string hwname, 
                                      UpdateType updateType, 
                                      double scale = 1.0 );

        // in case the scale has to be calculated or read from the hardware
        uint32_t updateScale( std::string name, double scale );

        void reset();

        // returns != 0 on error
        uint32_t update() throw( HAL::HardwareAccessException );
	uint32_t update(uint32_t streamNo) throw( HAL::HardwareAccessException );

        double get( std::string name );

    private:
        // return time difference in microseconds
        uint32_t subtractTime(struct timeval& t, struct timeval& sub);

        enum TRACKERTYPE { RATE, INTEGRATOR, MEAN };

        typedef struct {
            TRACKERTYPE tracker_type;
            std::string name;
            std::string hwname;
            UpdateType updateType;
            //            utils::InfoSpaceHandler::ISItem *hwitem;
            double scale;
            double result;
            uint64_t value;
            uint64_t tmp1;
            
        } TrackerItem;

        utils::InfoSpaceHandler *is_;
	//JRF might not need this 
	//InfoSpaceHandler &appIS_;
        std::tr1::unordered_map < std::string, TrackerItem > itemMap_;

        HAL::PCIDevice **hwpp_;

        timeval startTime_;
        timeval lastStop_;
        timeval now_;

    };
}

#endif /* __DataTracker */
