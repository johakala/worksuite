#ifndef __WebTableTab
#define __WebTableTab

#include <string>
#include <iostream>
#include "d2s/utils/Monitor.hh"
#include "d2s/utils/WebTable.hh"
#include "d2s/utils/WebTabIF.hh"

namespace ferol40
{
    class utils::WebTableTab : public utils::WebTabIF {

    public: 
        utils::WebTableTab( std::string name, 
                     std::string description, 
                     uint32_t columns, 
                     utils::Monitor &monitor );

        void print( std::ostream *out );

        void registerTable( std::string name,
                            std::string description,
                            std::string itemSetName );
        
        void jsonUpdate( std::ostream *out );

    private:
        std::string name_;
        std::string description_;
        uint32_t columns_;
        utils::Monitor &monitor_;
        std::list< utils::WebTable > tableList_;
    };
};

#endif /* __WebTableTab */
