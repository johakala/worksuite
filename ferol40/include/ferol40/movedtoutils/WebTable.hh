#ifndef __WebTable
#define __WebTable

#include <string>
#include <iostream>
#include "d2s/utils/Monitor.hh"

namespace ferol40
{
    class utils::WebTable {

    public:
        utils::WebTable( std::string name,
                  std::string description,
                  std::string itemSetName,
                  utils::Monitor &monitor );

        void print( std::ostream *out );
        void jsonUpdate( std::ostream *out );
        
    private:
        std::string htmlEscape( std::string orig ) const;
        std::string name_;
        std::string description_;
        std::string itemSetName_;
        utils::Monitor &monitor_;
    };
}

#endif /* __WebTable */
