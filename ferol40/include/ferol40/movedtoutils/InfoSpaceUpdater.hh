#ifndef __InfoSpaceUpdater
#define __InfoSpaceUpdater

namespace ferol40
{
    class utils::InfoSpaceHandler;
    
    class utils::InfoSpaceUpdater {

    public:
        
        virtual bool updateInfoSpace( utils::InfoSpaceHandler *is, uint32_t streamNo = 0 ) = 0;
        virtual ~InfoSpaceUpdater() {};
        
    };
}

#endif /* __InfoSpaceUpdater */
