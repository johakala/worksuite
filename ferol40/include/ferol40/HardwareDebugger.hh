#ifndef __HardwareDebugger
#define __HardwareDebugger

#include <list>
#include "d2s/utils/HardwareDebugItem.hh"
#include "ferol40/Ferol40.hh"
#include "hal/PCIDevice.hh"
#include "xoap/Method.h"
#include "log4cplus/logger.h"

namespace ferol40 
{

    class Ferol40;

    class HardwareDebugger { 
    public:
        
        HardwareDebugger( Logger logger, Ferol40 *ferol40 ); 

        xoap::MessageReference readItem( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception);

        xoap::MessageReference writeItem( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception);

        std::list< utils::HardwareDebugItem > getSlinkExpressRegisters( uint32_t linkNo );

        void dumpHardwareRegisters( std::string suffix );

        std::list<utils::HardwareDebugItem> getFerol40Registers();

        std::list<utils::HardwareDebugItem> getFrlRegisters();

        std::list<utils::HardwareDebugItem> getBridgeRegisters();


    private:
        std::list< utils::HardwareDebugItem > getPCIRegisters( HAL::HardwareDeviceInterface *device );

        Logger logger_;
        void dumpRegistersToFile( std::string name, const std::list<utils::HardwareDebugItem> &regsisters, std::string suffix = "" );

//        HAL::HardwareDeviceInterface *frlDevice_P;
        HAL::HardwareDeviceInterface *bridgeDevice_P;
        HAL::HardwareDeviceInterface *ferol40Device_P;
        SlinkExpressCore *slexp_P;

        uint32_t slot_;

    };
}
#endif /* __HardwareDebugger */
