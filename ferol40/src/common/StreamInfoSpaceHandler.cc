#include <sstream>
#include <iomanip>
#include "ferol40/StreamInfoSpaceHandler.hh"
#include "d2s/utils/Exception.hh"
#include "d2s/utils/loggerMacros.h"
#include "ferol40/ferol40Constants.h"

ferol40::StreamInfoSpaceHandler::StreamInfoSpaceHandler( xdaq::Application *xdaq,
        std::string name,
        utils::InfoSpaceUpdater *updater, 
        utils::InfoSpaceHandler *appIS,
        bool noAutoPush )
: utils::StreamInfoSpaceHandler( xdaq, name, updater, appIS, noAutoPush, NB_STREAMS )
{
    //DEBUG( "stream : appis ptr " << appIS_P);
    createuint32( "expectedFedId", 0, "", NOUPDATE, "The fedid expected by the configuration for this input stream. This value is used as a key to identify the flashlist entries.");
    createuint32( "slotNumber", 0, "", NOUPDATE, "The slotNumber of the Ferol40 used in the key. (Preferred to be used over expectedFedId, since the database is organised in terms of slot number and streamNumber). ");
    createuint32( "streamNumber", 0, "", NOUPDATE, "The stream number of this entry needed to distinguish streams of the same Ferol40.");
}


void
ferol40::StreamInfoSpaceHandler::update()
{
    if ( (updater_P != NULL) ) {
        //JRF TODO check if we need to loop, I Think not...  
        //we should loop over the info spaces and update them. This method will only work for pushing the info spaces to flashlists, this will not work for the web monitor
        for(uint32_t index(0) ; index < NB_STREAMS ; index++) //Loop over all streams
        {

            if (!dynamic_cast<xdata::Boolean*>(appIS_P->getvector( "InputPorts" )[index].getField("enable"))->value_) 
                    return; // Do not try to monitor disabled streams.
            currentStream_ = index;
            uint32_t fedid = dynamic_cast<xdata::UnsignedInteger32*>(appIS_P->getvector( "InputPorts" )[currentStream_].getField("expectedFedId"))->value_;//appIS_P->getuint32( "expectedFedId_0" );
            this->setuint32( "expectedFedId", fedid );
            this->setuint32( "streamNumber", currentStream_ );
            this->setuint32( "slotNumber", appIS_P->getuint32("slotNumber") );

            bool push = updater_P->updateInfoSpace( this, currentStream_ ); 

            if ( ! this->isNoAutoPush() && push ){
                this->pushInfospace();
         
            }
        }
    } else {
        ERROR("BUG: no updater defined");
    }
}
