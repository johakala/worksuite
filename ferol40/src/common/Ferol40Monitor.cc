#include "ferol40/Ferol40Monitor.hh"
#include "ferol40/loggerMacros.h"
#include "ferol40/ferol40Constants.h"

ferol40::Ferol40Monitor::Ferol40Monitor(Logger &logger,
        ApplicationInfoSpaceHandler &appIS,
        xdaq::Application *xdaq,
        utils::ApplicationStateMachineIF &fsm)
: utils::Monitor(logger, appIS, xdaq, fsm)
{
    this->addApplInfoSpaceItemSets(&appIS);
}

void ferol40::Ferol40Monitor::addApplInfoSpaceItemSets(ferol40::ApplicationInfoSpaceHandler *is)
{
    newItemSet("Application Config");
    addItem("Application Config", "slotNumber", is);
    addItem("Application Config", "OperationMode", is);
    //addItem( "Application Config", "DataSource",                  is);
    //    addItem( "Application Config", "FrlTriggerMode",              is);
    addItem("Application Config", "Ferol40TriggerMode", is);
    addItem("Application Config", "lightStop", is);
    addItem("Application Config", "deltaTMonMs", is);
    //addItem( "Application Config", "Maximal_fragment_size",       is);
    newItemSet("Temp App Config For FM");
    addItem("Temp App Config For FM", "testCounter", is);
    //    addItem( "Temp App Config For FM", "expectedFedId_0",                is);
    //    addItem( "Temp App Config For FM", "expectedFedId_1",                is);
    //    addItem( "Temp App Config For FM", "expectedFedId_2",                is);
    //    addItem( "Temp App Config For FM", "expectedFedId_3",                is);
    addItem("Temp App Config For FM", "Event_Length_bytes_FED0", is);
    addItem("Temp App Config For FM", "Event_Length_bytes_FED1", is);
    addItem("Temp App Config For FM", "Event_Length_bytes_FED2", is);
    addItem("Temp App Config For FM", "Event_Length_bytes_FED3", is);
    addItem("Temp App Config For FM", "Event_Length_Stdev_bytes_FED0", is);
    addItem("Temp App Config For FM", "Event_Length_Stdev_bytes_FED1", is);
    addItem("Temp App Config For FM", "Event_Length_Stdev_bytes_FED2", is);
    addItem("Temp App Config For FM", "Event_Length_Stdev_bytes_FED3", is);
    addItem("Temp App Config For FM", "enableStream0", is);
    addItem("Temp App Config For FM", "enableStream1", is);
    addItem("Temp App Config For FM", "enableStream2", is);
    addItem("Temp App Config For FM", "enableStream3", is);

    /*    newItemSet( "Frl Config" );                                   
          addItem( "Frl Config", "testDurationMs",                      is);
          addItem( "Frl Config", "enableDeskew",                        is);
          addItem( "Frl Config", "enableDCBalance",                     is);
          addItem( "Frl Config", "enableWCHistogram",                   is);
          addItem( "Frl Config", "gen_Thre_busy",                       is);
          addItem( "Frl Config", "gen_Thre_ready",                      is);
          */
    newItemSet("Ferol40 Config");
    //addItem( "Ferol40 Config", "SourceIP",                          is);
    //addItem( "Ferol40 Config", "DestinationIP",                     is);
    addItem("Ferol40 Config", "MAX_ARP_Tries", is);
    addItem("Ferol40 Config", "ARP_Timeout_Ms", is);
    addItem("Ferol40 Config", "Connection_Timeout_Ms", is);
    //addItem( "Ferol40 Config", "Event_Length_bytes_FED0",           is);
    //addItem( "Ferol40 Config", "Event_Length_bytes_FED1",           is);
    //addItem( "Ferol40 Config", "Event_Length_Stdev_bytes_FED0",     is);
    //addItem( "Ferol40 Config", "Event_Length_Stdev_bytes_FED1",     is);
    //addItem( "Ferol40 Config", "Event_Length_Max_bytes_FED0",       is);
    //addItem( "Ferol40 Config", "Event_Length_Max_bytes_FED1",       is);
    //addItem( "Ferol40 Config", "Event_Delay_ns_FED0",               is);
    //addItem( "Ferol40 Config", "Event_Delay_ns_FED1",               is);
    //addItem( "Ferol40 Config", "Event_Delay_Stdev_ns_FED0",         is);
    //addItem( "Ferol40 Config", "Event_Delay_Stdev_ns_FED1",         is);
    //addItem( "Ferol40 Config", "TCP_SOCKET_BUFFER_DDR",             is);
    addItem("Ferol40 Config", "Window_trg_stop", is);

    // newItemSet( name.str() );
    // JRF TODO replace this with special items for the vector. addItem( name.str(), "enableStream0",                  is);
    //addItem( name.str(), "TCP_SOURCE_PORT_FED0",           is);
    //addItem( name.str(), "TCP_DESTINATION_PORT_FED0",      is);
    //addItem( name.str(), "expectedFedId_0",                is);
    //addItem( name.str(), "nb_frag_before_BP_L0",           is);
    //newItemSet( name.str() );
    // addItem( name.str(), "enableStream1",                  is);
    //addItem( name.str(), "TCP_SOURCE_PORT_FED1",           is);
    //addItem( name.str(), "TCP_DESTINATION_PORT_FED1",      is);
    //addItem( name.str(), "expectedFedId_1",                is);
    //addItem( name.str(), "nb_frag_before_BP_L1",           is);
}

void ferol40::Ferol40Monitor::addInfoSpace(ferol40::StreamConfigInfoSpaceHandler *is)
{
    // utils::Monitor::addInfoSpace( is );
    //JRF NOTE,  we need to add items if we want them to appear on the webpage.
    //JRF TODO, figure out why the values are not being filled from the info space. One needs to tell the infospace which stream we are reading. Currently it is using suffixes, which will not work. Look in the base classes for ferol40::StreamInfoSpaceHandler
    std::stringstream name;
    for (uint32_t i(0); i < NB_STREAMS; i++)
    {
        name << "Input Port " << i << " Config";
        newItemSet(name.str());
        addItem(name.str(), "enable", is, "", i);
        addItem(name.str(), "expectedFedId", is, "", i);
        addItem(name.str(), "slotNumber", is, "", i);
        addItem(name.str(), "streamNumber", is, "", i);
        addItem(name.str(), "DataSource", is, "", i);
        addItem(name.str(), "DestinationIP", is, "", i);
        addItem(name.str(), "SourceIP", is, "", i);
        addItem(name.str(), "TCP_SOURCE_PORT", is, "", i);
        addItem(name.str(), "TCP_DESTINATION_PORT", is, "", i);
        addItem(name.str(), "ENA_PAUSE_FRAME", is, "", i);
        addItem(name.str(), "TCP_CWND", is, "", i);
        addItem(name.str(), "nb_frag_before_BP", is, "", i);
        addItem(name.str(), "Maximal_fragment_size", is, "", i);
        name.str("");
        name << "Input Port " << i << " Gen Config";
        newItemSet(name.str());
        addItem(name.str(), "enable", is, "", i);
        addItem(name.str(), "N_Descriptors", is, "", i);
        addItem(name.str(), "Event_Length_bytes", is, "", i);
        addItem(name.str(), "Event_Length_Stdev_bytes", is, "", i);
        addItem(name.str(), "Event_Length_Max_bytes", is, "", i);
        addItem(name.str(), "Event_Delay_ns", is, "", i);
        addItem(name.str(), "Event_Delay_Stdev_ns", is, "", i);
        addItem(name.str(), "Seed", is, "", i);
        name.str("");
    }
}

void ferol40::Ferol40Monitor::addInfoSpace(ferol40::StatusInfoSpaceHandler *is)
{
    newItemSet("Application State");
    addItem("Application State", "instance", is);
    addItem("Application State", "slotNumber", is);
    addItem("Application State", "stateName", is);
    addItem("Application State", "subState", is);
    addItem("Application State", "failedReason", is);
    addItem("Application State", "lockStatus", is);
    newItemSet("Hardware State");
    addItem("Hardware State", "QSPF_present", is);
    addItem("Hardware State", "QSFP_int", is);
    addItem("Hardware State", "QSFP_low_power_r_bit", is);
    addItem("Hardware State", "DDR3_0_Ready_bit", is);
    addItem("Hardware State", "DDR3_0_Ready_bit", is);
    newItemSet("Version State");
    addItem("Version State", "HardwareRevision", is);
    addItem("Version State", "FirmwareVersion", is);
    addItem("Version State", "FirmwareType", is);
}

void ferol40::Ferol40Monitor::addInfoSpace(ferol40::InputStreamInfoSpaceHandler *is)
{
    //Monitor::addInfoSpace( is );
    //JRF TODO, loop over 4 streams here and add the items.
    std::stringstream name;

    for (uint32_t i(0); i < NB_STREAMS; i++)
    {
        name << "Input Port " << i;
        newItemSet(name.str());
        addItem(name.str(), "enable", is, "", i);
        addItem(name.str(), "expectedFedId", is, "", i);
        addItem(name.str(), "slotNumber", is, "", i);
        addItem(name.str(), "streamNumber", is, "", i);
        addItem(name.str(), "SenderFwVersion", is, "", i);
        addItem(name.str(), "slinkxpress_sfpx_setup", is, "", i);
        addItem(name.str(), "EventCounter", is, "", i);
        addItem(name.str(), "TriggerNumber", is, "", i);
        addItem(name.str(), "EventGenRate", is, "", i);
        //addItem( name.str(), "BX", is, "", i );
        addItem(name.str(), "SLinkPacketCRCError", is, "", i);
        addItem(name.str(), "SLinkCRCError", is, "", i);
        addItem(name.str(), "FEDCRCError", is, "", i);
        addItem(name.str(), "FEDFrequency", is, "", i);
        addItem(name.str(), "AccBackpressureSeconds", is, "", i);
        //JRF Moved this to TCP Infospace
        //addItem(name.str(), "AccBIFIBackpressureSeconds", is, "", i);
        addItem(name.str(), "AccSlinkFullSeconds", is, "", i);
        addItem(name.str(), "LatchedTimeFrontendSeconds", is, "", i);
        addItem(name.str(), "LatchedSlinkSenderClockSeconds", is, "", i);
        addItem(name.str(), "WrongFEDIdDetected", is, "", i);
        addItem(name.str(), "WrongFEDId", is, "", i);
        addItem(name.str(), "SyncLostDraining", is, "", i);
        addItem(name.str(), "ExpectedTriggerNumber", is, "", i);
        addItem(name.str(), "ReceivedTriggerNumber", is, "", i);
        addItem(name.str(), "MaxFragSizeReceived", is, "", i);
        addItem(name.str(), "CurrentFragSizeReceived", is, "", i);
        addItem(name.str(), "ErrorFragSizeReceived", is, "", i);
        addItem(name.str(), "NoOfFragmentsCut", is, "", i);
        addItem(name.str(), "BackpressureCounter", is, "", i);
        addItem(name.str(), "BackpressureToSlink", is, "", i);
        addItem(name.str(), "BackpressureToEMU", is, "", i);
        addItem(name.str(), "SlinkReceiverFullCancelAck", is, "", i);
        //addItem(name.str(), "BIFI", is, "", i);
        //addItem(name.str(), "BIFI_MAX", is, "", i);
        addItem(name.str(), "BACK_PRESSURE_BIFI", is, "", i);
        addItem(name.str(), "BackpressureDebug", is, "", i);
        name.str("");
    }
}

void ferol40::Ferol40Monitor::addInfoSpace(ferol40::TCPStreamInfoSpaceHandler *is)
{
    //Monitor::addInfoSpace( is );
    std::stringstream name;
    for (uint32_t i(0); i < NB_STREAMS; i++)
    {
        name << "TCP Stream " << i;
        newItemSet(name.str());
        addItem(name.str(), "TcpThroughput", is, "", i);
        addItem(name.str(), "TcpRetransmit", is, "", i);
        addItem(name.str(), "enable", is, "", i);
        addItem(name.str(), "BIFI", is, "", i);
        addItem(name.str(), "BIFI_MAX", is, "", i);
        // 	  addItem(name.str(), "BACK_PRESSURE_BIFI", is, "", i);
        addItem(name.str(), "TCP_CONNECTION_ESTABLISHED", is, "", i);
        addItem(name.str(), "TCP_STAT_CONNATTEMPT", is, "", i);
        addItem(name.str(), "TCP_STAT_CONNREFUSED", is, "", i);
        addItem(name.str(), "TCP_STAT_CONNRST", is, "", i);
        addItem(name.str(), "TCP_STAT_SNDPROBE", is, "", i);
        addItem(name.str(), "TCP_STAT_SNDPACK", is, "", i);
        addItem(name.str(), "TCP_STAT_SNDREXMITPACK", is, "", i);
        addItem(name.str(), "RTT", is, "", i);
        addItem(name.str(), "AVG_RTT", is, "", i);
        addItem(name.str(), "TCP_STAT_RCVDUPACK", is, "", i);
        addItem(name.str(), "TCP_STAT_PERSIST", is, "", i);
        addItem(name.str(), "TCP_STAT_PERSIST_EXITED", is, "", i);
        addItem(name.str(), "TCP_STAT_SNDBYTE", is, "", i);
        addItem(name.str(), "TCP_CWND", is, "", i);
        addItem(name.str(), "TCP_STAT_CURRENT_WND", is, "", i);
        addItem(name.str(), "TCP_STAT_WND_MAX", is, "", i);
        addItem(name.str(), "TCP_STAT_WND_MIN", is, "", i);
        addItem(name.str(), "TCP_STAT_SND_NXT_UNALIGNED", is, "", i);
        addItem(name.str(), "ACK_DELAY_MAX", is, "", i);
        addItem(name.str(), "ACK_DELAY", is, "", i);
        addItem(name.str(), "AVG_ACK_DELAY", is, "", i);
        //JRF TODO remove this... 
        addItem(name.str(), "TCP_STAT_ACK_DELAY_MAX", is, "", i);
        addItem(name.str(), "Ferol40EventGenRate", is, "", i);
        addItem(name.str(), "GEN_EVENT_NUMBER", is, "", i);
        addItem(name.str(), "GEN_TRIGGER_NUMBER", is, "", i);

        addItem(name.str(), "AccBIFIBackpressureSeconds", is, "", i);
        addItem(name.str(), "LatchedTimeBackendSeconds", is, "", i);

        name.str("");
    }
    for (uint32_t i(0); i < NB_STREAMS; i++)
    {
        name << "Output Port " << i;
        newItemSet(name.str());
        addItem(name.str(), "enable", is, "", i);
        addItem(name.str(), "MAC_RX_GOOD_FRAME_COUNTER", is, "", i);
        addItem(name.str(), "MAC_RX_BAD_FRAME_COUNTER", is, "", i);
        addItem(name.str(), "MAC_RX_PAUSE_FRAME_COUNTER", is, "", i);
        addItem(name.str(), "MAC_RX_PFC_FRAME_COUNTER", is, "", i);
        addItem(name.str(), "MAC_RX_ERROR_STATUS", is, "", i);
        addItem(name.str(), "MAC_TX_FRAME_ERROR_COUNTER", is, "", i);
        addItem(name.str(), "PauseFrameRate", is, "", i);
        addItem(name.str(), "eth_10gb_status0_MAC_core", is, "", i);
        addItem(name.str(), "eth_10gb_status1_MAC_core", is, "", i);
        addItem(name.str(), "eth_10gb_status2_MAC_core", is, "", i);
        addItem(name.str(), "eth_10gb_status3_MAC_core", is, "", i);

        /*
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           addItem(name.str(), "", is, "", i);
           */
        name.str("");
    }

    /*newItemSet("TCP Stream 1" );
      addItem( "TCP Stream 1", "Throughput",                 is);
      addItem( "TCP Stream 1", "Retransmit",                 is);
      addItem( "TCP Stream 1", "BIFI",                       is);
      addItem( "TCP Stream 1", "BIFI_MAX",                   is);
      addItem( "TCP Stream 1", "TCP_CONNECTION_ESTABLISHED", is);
      addItem( "TCP Stream 1", "TCP_STAT_CONNATTEMPT",       is);
      addItem( "TCP Stream 1", "TCP_STAT_CONNREFUSED",       is);

      newItemSet("TCP Stream 2" );
      addItem( "TCP Stream 2", "Throughput",                 is);
      addItem( "TCP Stream 2", "Retransmit",                 is);
      addItem( "TCP Stream 2", "BIFI",                       is);
      addItem( "TCP Stream 2", "BIFI_MAX",                   is);
      addItem( "TCP Stream 2", "TCP_CONNECTION_ESTABLISHED", is);
      addItem( "TCP Stream 2", "TCP_STAT_CONNATTEMPT",       is);
      addItem( "TCP Stream 2", "TCP_STAT_CONNREFUSED",       is);

      newItemSet("TCP Stream 3" );
      addItem( "TCP Stream 3", "Throughput",                 is);
      addItem( "TCP Stream 3", "Retransmit",                 is);
      addItem( "TCP Stream 3", "BIFI",                       is);
      addItem( "TCP Stream 3", "BIFI_MAX",                   is);
      addItem( "TCP Stream 3", "TCP_CONNECTION_ESTABLISHED", is);
      addItem( "TCP Stream 3", "TCP_STAT_CONNATTEMPT",       is);
      addItem( "TCP Stream 3", "TCP_STAT_CONNREFUSED",       is);
      */
}

void ferol40::Ferol40Monitor::addInfoSpace(ferol40::Ferol40InfoSpaceHandler *is)
{
    //Monitor::addInfoSpace( is );
    //JRF TODO this Infospace should be deprecated eventually, all useful entities here should be moved to other infospaces.
    newItemSet("FEROL40");
    addItem("FEROL40", "Ferol40HwRevision", is);
    addItem("FEROL40", "Ferol40FwType", is);
    addItem("FEROL40", "Ferol40FwVersion", is);
    addItem("FEROL40", "Ferol40SnLow", is);
    addItem("FEROL40", "Ferol40SnHi", is);
    addItem("FEROL40", "TCP_STATE_FEDS", is);
    //JRF Moved this to stream infospace
    //addItem( "FEROL40", "TCP_ARP_REPLY_OK",                         is);
    addItem("FEROL40", "DestinationMACAddress", is);
    addItem("FEROL40", "Source_MAC_EEPROM", is);

    addItem("FEROL40", "Link5gb_SFP_status_L0", is);
    addItem("FEROL40", "Link5gb_SFP_status_L1", is);
    addItem("FEROL40", "L5gb_OLstatus_SlX0", is);
    addItem("FEROL40", "L5gb_OLstatus_SlX1", is);
    addItem("FEROL40", "ARP_MAC_CONFLICT", is);
    addItem("FEROL40", "ARP_IP_CONFLICT", is);
    addItem("FEROL40", "ARP_MAC_WITH_IP_CONFLICT_LO", is);
    addItem("FEROL40", "PACKETS_SENT_LO", is);
    addItem("FEROL40", "PacketRate", is);
    addItem("FEROL40", "PACKETS_RECEIVED_LO", is);
    addItem("FEROL40", "PACKETS_RECEIVED_BAD_LO", is);
    addItem("FEROL40", "PAUSE_FRAMES_COUNTER", is);
    addItem("FEROL40", "PauseFrameRate", is);

    newItemSet("FEROL40 Stream 0");
    addItem("FEROL40 Stream 0", "GEN_TRIGGER_CONTROL_FED0", is);
    addItem("FEROL40 Stream 0", "GEN_EVENT_NUMBER_FED0", is);

    addItem("FEROL40 Stream 0", "EventGenRateFed0", is);
    /*addItem( "FEROL40 Stream 0", "L5gb_sync_lost_draining_SlX0",    is);
      addItem( "FEROL40 Stream 0", "L5gb_trg_Bad_rcved_SlX0",         is);
      addItem( "FEROL40 Stream 0", "L5gb_number_of_frag_cut_Sl0",     is);
      addItem( "FEROL40 Stream 0", "L5gb_curr_frag_size_Sl0",         is);
      addItem( "FEROL40 Stream 0", "L5gb_max_frag_Rcv_Sl0",           is);
      addItem( "FEROL40 Stream 0", "L5gb_curr_trig_Sl0",              is);
      addItem( "FEROL40 Stream 0", "L5gb_curr_BX_Sl0",                is);
      addItem( "FEROL40 Stream 0", "L5gb_curr_FEDID_Sl0",             is);
      addItem( "FEROL40 Stream 0", "L5gb_Wrong_FEDID_Sl0",            is);
    //    addItem( "FEROL40 Stream 0", "",    is);
    //addItem( "FEROL40 Stream 0", "L5gb_good_event_SlX0",            is);
    //addItem( "FEROL40 Stream 0", "L5gb_bad_event_SlX0",             is);  
    addItem( "FEROL40 Stream 0", "L5gb_received_event_SlX0_LO",     is);
    addItem( "FEROL40 Stream 0", "L5gb_bad_pack_received_SlX0",     is);
    addItem( "FEROL40 Stream 0", "L5gb_FEDID_error_SlX0",           is);   
    addItem( "FEROL40 Stream 0", "L5gb_FED_CRC_ERR_SlX0_LO",        is);
    addItem( "FEROL40 Stream 0", "L5gb_SLINK_CRC_ERR_SlX0_LO",      is);*/
    addItem("FEROL40 Stream 0", "Throughput_FED0", is);
    addItem("FEROL40 Stream 0", "Retransmit_FED0", is);
    addItem("FEROL40 Stream 0", "BIFI_FED0", is);
    addItem("FEROL40 Stream 0", "BIFI_MAX_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_CONNECTION_ESTABLISHED_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_CONNATTEMPT_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_CONNREFUSED_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_CONNRST_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_SNDPROBE_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_SNDPACK_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_SNDREXMITPACK_FED0", is);
    addItem("FEROL40 Stream 0", "RTT_FED0", is);
    addItem("FEROL40 Stream 0", "AVG_RTT_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_RCVDUPACK_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_PERSIST_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_PERSIST_EXITED_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_SNDBYTE_FED0_LO", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_CURRENT_WND_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_WND_MAX_FED0", is);
    addItem("FEROL40 Stream 0", "TCP_STAT_WND_MIN_FED0", is);

    newItemSet("FEROL40 Stream 1");
    addItem("FEROL40 Stream 1", "GEN_TRIGGER_CONTROL_FED1", is);
    addItem("FEROL40 Stream 1", "GEN_EVENT_NUMBER_FED1", is);
    addItem("FEROL40 Stream 1", "EventGenRateFed1", is);
    /*    addItem( "FEROL40 Stream 1", "L5gb_sync_lost_draining_SlX1",    is);
          addItem( "FEROL40 Stream 1", "L5gb_trg_Bad_rcved_SlX1",         is);
          addItem( "FEROL40 Stream 1", "L5gb_number_of_frag_cut_Sl1",     is);
          addItem( "FEROL40 Stream 1", "L5gb_curr_frag_size_Sl1",         is);
          addItem( "FEROL40 Stream 1", "L5gb_max_frag_Rcv_Sl1",           is);
          addItem( "FEROL40 Stream 1", "L5gb_curr_trig_Sl1",              is);
          addItem( "FEROL40 Stream 1", "L5gb_curr_BX_Sl1",                is);
          addItem( "FEROL40 Stream 1", "L5gb_curr_FEDID_Sl1",             is);
          addItem( "FEROL40 Stream 1", "L5gb_Wrong_FEDID_Sl1",            is);
    //    addItem( "FEROL40 Stream 1", "",          is);
    //addItem( "FEROL40 Stream 1", "L5gb_good_event_SlX1",            is);
    //addItem( "FEROL40 Stream 1", "L5gb_bad_event_SlX1",             is);
    addItem( "FEROL40 Stream 1", "L5gb_received_event_SlX1_LO",     is);
    addItem( "FEROL40 Stream 1", "L5gb_bad_pack_received_SlX1",     is);
    addItem( "FEROL40 Stream 1", "L5gb_FEDID_error_SlX1",           is);
    addItem( "FEROL40 Stream 1", "L5gb_FED_CRC_ERR_SlX1_LO",        is);
    addItem( "FEROL40 Stream 1", "L5gb_SLINK_CRC_ERR_SlX1_LO",      is);*/
    addItem("FEROL40 Stream 1", "Throughput_FED1", is);
    addItem("FEROL40 Stream 1", "Retransmit_FED1", is);
    addItem("FEROL40 Stream 1", "BIFI_FED1", is);
    addItem("FEROL40 Stream 1", "BIFI_MAX_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_CONNECTION_ESTABLISHED_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_CONNATTEMPT_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_CONNREFUSED_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_CONNRST_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_SNDPROBE_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_SNDPACK_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_SNDREXMITPACK_FED1", is);
    addItem("FEROL40 Stream 1", "RTT_FED1", is);
    addItem("FEROL40 Stream 1", "AVG_RTT_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_RCVDUPACK_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_PERSIST_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_PERSIST_EXITED_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_SNDBYTE_FED1_LO", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_CURRENT_WND_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_WND_MAX_FED1", is);
    addItem("FEROL40 Stream 1", "TCP_STAT_WND_MIN_FED1", is);

    /*newItemSet( "FEROL40 SFP 0" );                               
      addItem( "FEROL40 SFP 0", "SFP_VENDOR_0",                       is);     
      addItem( "FEROL40 SFP 0", "SFP_PARTNO_0",                       is);     
      addItem( "FEROL40 SFP 0", "SFP_DATE_0",                         is);     
      addItem( "FEROL40 SFP 0", "SFP_SN_0",                           is);     
      addItem( "FEROL40 SFP 0", "SFP_TEMP_0",                         is);     
      addItem( "FEROL40 SFP 0", "SFP_VCC_0",                          is);     
      addItem( "FEROL40 SFP 0", "SFP_BIAS_0",                         is);     
      addItem( "FEROL40 SFP 0", "SFP_TXPWR_0",                        is);     
      addItem( "FEROL40 SFP 0", "SFP_RXPWR_0",                        is);     

      newItemSet( "FEROL40 SFP 1" );                               
      addItem( "FEROL40 SFP 1", "SFP_VENDOR_1",                       is);     
      addItem( "FEROL40 SFP 1", "SFP_PARTNO_1",                       is);     
      addItem( "FEROL40 SFP 1", "SFP_DATE_1",                         is);     
      addItem( "FEROL40 SFP 1", "SFP_SN_1",                           is);     
      addItem( "FEROL40 SFP 1", "SFP_TEMP_1",                         is);     
      addItem( "FEROL40 SFP 1", "SFP_VCC_1",                          is);     
      addItem( "FEROL40 SFP 1", "SFP_BIAS_1",                         is);     
      addItem( "FEROL40 SFP 1", "SFP_TXPWR_1",                        is);     
      addItem( "FEROL40 SFP 1", "SFP_RXPWR_1",                        is);     

      newItemSet( "Thresholds SFP0" );
      addItem( "Thresholds SFP0", "SFP_VCC_L_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_VCC_H_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_VCC_L_WARN_0", is);
      addItem( "Thresholds SFP0", "SFP_VCC_H_WARN_0", is);
      addItem( "Thresholds SFP0", "SFP_TEMP_L_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_TEMP_H_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_TEMP_L_WARN_0", is);
      addItem( "Thresholds SFP0", "SFP_TEMP_H_WARN_0", is);
      addItem( "Thresholds SFP0", "SFP_BIAS_L_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_BIAS_H_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_BIAS_L_WARN_0", is);
      addItem( "Thresholds SFP0", "SFP_BIAS_H_WARN_0", is);
      addItem( "Thresholds SFP0", "SFP_TXPWR_L_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_TXPWR_H_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_TXPWR_L_WARN_0", is);
      addItem( "Thresholds SFP0", "SFP_TXPWR_H_WARN_0", is);
      addItem( "Thresholds SFP0", "SFP_RXPWR_L_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_RXPWR_H_ALARM_0", is);
      addItem( "Thresholds SFP0", "SFP_RXPWR_L_WARN_0", is);
      addItem( "Thresholds SFP0", "SFP_RXPWR_H_WARN_0", is);

      newItemSet( "Thresholds SFP1" );
      addItem( "Thresholds SFP1", "SFP_VCC_L_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_VCC_H_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_VCC_L_WARN_1", is);
      addItem( "Thresholds SFP1", "SFP_VCC_H_WARN_1", is);
      addItem( "Thresholds SFP1", "SFP_TEMP_L_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_TEMP_H_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_TEMP_L_WARN_1", is);
      addItem( "Thresholds SFP1", "SFP_TEMP_H_WARN_1", is);
      addItem( "Thresholds SFP1", "SFP_BIAS_L_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_BIAS_H_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_BIAS_L_WARN_1", is);
      addItem( "Thresholds SFP1", "SFP_BIAS_H_WARN_1", is);
      addItem( "Thresholds SFP1", "SFP_TXPWR_L_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_TXPWR_H_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_TXPWR_L_WARN_1", is);
      addItem( "Thresholds SFP1", "SFP_TXPWR_H_WARN_1", is);
      addItem( "Thresholds SFP1", "SFP_RXPWR_L_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_RXPWR_H_ALARM_1", is);
      addItem( "Thresholds SFP1", "SFP_RXPWR_L_WARN_1", is);
      addItem( "Thresholds SFP1", "SFP_RXPWR_H_WARN_1", is);
      */}
