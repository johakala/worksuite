#include <sys/types.h>
#include "ferol40/ApplicationInfoSpaceHandler.hh"
#include "ferol40/loggerMacros.h"
#include "ferol40/ferol40Constants.h"

ferol40::ApplicationInfoSpaceHandler::ApplicationInfoSpaceHandler(xdaq::Application *xdaq)
    : utils::InfoSpaceHandler(xdaq, xdaq->getApplicationInfoSpace(), "Appl_IS")
// All values below are configuration parameters and should be set before "Configure"
{
    createDocumentationSeparator("Parameters which control the behaviour of the Application. They are not directly related to the hardware.");
    // Temporary hack to make RCMS and scripts happy
    createstring("stateName", "uninitialized", "", PROCESS, "A temporary (or not so temporary...) hack to make tools like RCMS happy. These want to query the stateName variable in the ApplicationInfospace. They have no means to easily access other Infospaces.");
    // The following parameters influence the behaviour of the entire application.
    createbool("noFirmwareVersionCheck", false, "", NOUPDATE, "This parameter is intended for debugging only. If set to true, the utils::version checking in the FPGA will be switched off. Do not use this option in production or serious test setups!");
    createbool("lightStop", false, "", NOUPDATE, "If this parameter is set to false, a \"Stop\" command will execute a \"Halt\" and a \"Configure\". If set to true, a faster transition is executed: Then a softwareReset is executed, the DAQ_mode is reset adn the FifoMonCounters and the event size histogramming are disabled. Finally the expected source FED IDs are set in the hardware. In the Ferol40 the event generators are stopped if the Ferol40EmulatorMode was active. Then some monitoring counters are reset. TCP/IP connections are not touched.");
    createuint32("slotNumber", 0, "", NOUPDATE, "The slot numbers are counted from the right side of the crate and start with '1'. (Slot 0 is occupied by the crate controller module.)");
    createuint32("deltaTMonMs", 2000, "", NOUPDATE, "The time interval in ms between successive updates of the monitoring information."); //
    //JRF TODO change the modes to something more sensible like NORMAL, FED_KIT.
    createstring("OperationMode", FEROL40_MODE, "", NOUPDATE, "Defines the Mode of operation: <br>\"FEROL40_MODE\" for receiving data via the the Ferol40 (optical inputs or from the internal event generator in the FEROL40)");

    //JRF TODO do we still need this?
    createbool("enableSpy", false, "", NOUPDATE, " ...not sure if still used...");

    //JRF TODO, we must figure out a clever way to replace the functionality of the InfoSpaceHadler for those parameters inside the vector.
    createuint32("testCounter", 0, "", NOUPDATE, "This is a dummy parameter which is counting up to see that the AJAX monitoring is working. It is useless to set this parameter to some value.");

    createDocumentationSeparator("Parameters related to the data streams. The Ferol40 can be operated with up to 2 streams. At the input they either correspond to the SLINK ports, to the SLINKexpress ports or to the  event generators, In the output they are handled as different TCP/IP streams. They always go to the same IP destination address but to different ports.");
    // JRF now we fill the vector of bags with default values.
    xdata::Vector<xdata::Bag<utils::StreamConf> > tempInputPortsVector;
    const xdata::Bag<utils::StreamConf> bag;
    for (int i = 0; i < NB_STREAMS; i++)
    {
        tempInputPortsVector.push_back(bag);
    }
    createvector("InputPorts", tempInputPortsVector, "", NOUPDATE, "This Vector contains the configuration params for each stream");
    createuint32("Window_trg_stop", 0, "", NOUPDATE, "To throttle the throughput of the Ferol40 the streams can be configured to accept at maximum a maximal number of triggers in a given time window. This parameter defines the width of the time-window in units of 6.4ns.");
    createstring("Ferol40TriggerMode", "FEROL40_AUTO_TRIGGER_MODE", "", NOUPDATE, "If events are generated in the FEROL40 there are different ways to trigger these events. \"FEROL40_AUTO_TRIGGER_MODE\" generates events in an endless loop. \"FEROL_EXTERNAL_TRIGGER_MODE\" generates an event every time a trigger signal from the backplane is received. Usually these signals are provided by the AMC13 ");


    createDocumentationSeparator("The following parameters are required by the JobControl to pass parameters for specific run types. These should be deprecated once the framework allows to send the correct vector of bags structure.");
    createbool("SourceIpOverride", false, "", NOUPDATE, "Override the Source IP with the value in the xml");
    
    //JRF the following set of 4 values is used until the framework can handle vectors.
    //JRF TODO, these values should be mirrored with the equivalent parameters in the streamConfigInfoSpace so that when samim sets them by soap messages, they get propagated to the hardware.
    //
    createbool("enableStream0", false, "", NOUPDATE, "Enable Stream 0.");
    createbool("enableStream1", false, "", NOUPDATE, "Enable Stream 1.");
    createbool("enableStream2", false, "", NOUPDATE, "Enable Stream 2.");
    createbool("enableStream3", false, "", NOUPDATE, "Enable Stream 3.");

    createuint32("Event_Length_bytes_FED0", 0x0, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration.");
    createuint32("Event_Length_bytes_FED1", 0x0, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration. ");
    createuint32("Event_Length_bytes_FED2", 0x0, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration.");
    createuint32("Event_Length_bytes_FED3", 0x0, "", NOUPDATE, "The minimal event size is 0x18 (3 64bit words). The software enforces this minimum if smaller values are provided in the configuration. ");

    createuint32("Event_Length_Stdev_bytes_FED0", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated");
    createuint32("Event_Length_Stdev_bytes_FED1", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated");
    createuint32("Event_Length_Stdev_bytes_FED2", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated");
    createuint32("Event_Length_Stdev_bytes_FED3", 0x0, "", NOUPDATE, "If set to 0 a fixed size events are generated");

    //JRF TODO, check if we need this
    //createbool( "gotoFailedOnWrongFedId", true, "", NOUPDATE, "If set to true (default) the application goes into the failed state whenever a fragement is encountered at the input, of which the fedId does not match the expectet fedId.");

    createDocumentationSeparator("Ferol40 Configuration parameters related to the basic setup of the DAQ link.");
    //JRF TODO set the default value here correctly for FEROL40
    createuint32("MAC_HI_FEROL40", 0x080030, "", NOUPDATE, "The MAC addresses of the Ferol40s are programmed into a on-board PROM. They all start with the bytes 08:00:30. Therefore this parameter should not be changed. The software checks that the MAC address of the PROM really starts with the byte combination given in this parameter. A mismatch is an indication that the PROM was not correctly programmed and the application goes to \"Failed\". The module needs to be exchanged.");
    createuint32("MAX_ARP_Tries", 20, "", NOUPDATE, "The maximum number of ARP retries during the TCP/IP connection setup. Do not decrease this number since some switches need a substantial number of re-tries or time before they reply correctly.");
    createuint32("ARP_Timeout_Ms", 500, "", NOUPDATE, "The number of ms to wait for an ARP reply before the next retry.");
    createuint32("Connection_Timeout_Ms", 5000, "", NOUPDATE, "The time to wait for a reply to the \"syn\" packets the FEROL40 sends during TCP/IP connection setup. SYN packets are regularly sent during this time.");
    createstring("IP_NETMASK", "0.0.0.0", "", NOUPDATE, "If the Ferol40 should send packets to another network via a router/geteway, the netmask and the gateway need to be set beforehand. If the netmask is left at 0.0.0.0 the Ferol40 will work, but no routing of the packets is possible.");
    createstring("IP_GATEWAY", "0.0.0.0", "", NOUPDATE, "If the Ferol40 should send packets to another network via a router/geteway, the netmask and the gateway need to be set beforehand.");
    
    createDocumentationSeparator("Parameters to tune the 10 Gbps TCP/IP connection to the DAQ");
    //JRF TODO We may need to Move these into the bag so that we can have different settings per stream. For now I'll leave them as global params.
    createuint32("TCP_CONFIGURATION", 0x00004000, "hex", NOUPDATE, "Bits 15..0: sets the size of the TCP window for this stream.<br> Bit 16: The TCP push flag will be set in each outgoing packet.<br> Bit 18: no_delay will be used in the TCP statemachine.<br> Bit 19: The fast re-transmit will be disabled. <br> Bit 20: TIMER_STOP is disabled. ");
    createuint32("TCP_OPTIONS_MSS_SCALE", 0x00012300, "hex", NOUPDATE, "");
    createuint32("TCP_TIMER_RTT", 312500, "", NOUPDATE, "");
    createuint32("TCP_TIMER_RTT_SYN", 312500000, "", NOUPDATE, "");
    createuint32("TCP_TIMER_PERSIST", 625000, "", NOUPDATE, "");
    createuint32("TCP_REXMTTHRESH", 3, "", NOUPDATE, "");
    createuint32("TCP_REXMTCWND_SHIFT", 6, "", NOUPDATE, "");
    //JRF TDOD, check with dominique if these parameters are used any more...
    createDocumentationSeparator("The following registers exist in the hardware but currently do not have any function.");
    createuint32("TCP_OPTION_TIMESTAMP", 0, "", NOUPDATE, "");
    createuint32("TCP_OPTION_TIMESTAMP_REPLY", 0, "", NOUPDATE, "");

    //TODO reinstate this if necessary.
    //std::cout << getDocumentation() << std::endl;

    // Bind setting of default parameters
    xdaq->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

// callback for Parameter Setting
void ferol40::ApplicationInfoSpaceHandler::actionPerformed(xdata::Event &e)
{
    DEBUG("actionPerformed called with type " << e.type());
    if (e.type() == "urn:xdaq-event:setDefaultValues")
    {

        DEBUG("actionPerformed for setDefaultValues");
        // read all values from the infospace so that they are displayed in
        // the local monitoring.
        readInfoSpace();
        // this pushes the changes into the monitoring system by firing the monitoring events:
        pushInfospace();
    }
}

// special method to set the parameters needed by the RCMSStateListener. This is a structure and a bool to monitor.
void ferol40::ApplicationInfoSpaceHandler::setRCMSStateListenerParameters(xdata::Bag<xdaq2rc::ClassnameAndInstance> *RcmsStateListenerParameters,
                                                                          xdata::Boolean *FoundRcmsStateListener)
{
    is_P->lock();
    is_P->fireItemAvailable("rcmsStateListener", RcmsStateListenerParameters);
    is_P->fireItemAvailable("foundRcmsStateListener", FoundRcmsStateListener);
    is_P->unlock();
    cpis_P->lock();
    cpis_P->fireItemAvailable("rcmsStateListener", RcmsStateListenerParameters);
    cpis_P->fireItemAvailable("foundRcmsStateListener", FoundRcmsStateListener);
    cpis_P->unlock();
}

void ferol40::ApplicationInfoSpaceHandler::mirrorToFlatParams()
{
    readInfoSpace();
    std::stringstream tmpName;
    tmpName.str("");
    
    for (int i = 0; i < 4; i++) {
        
        tmpName << "enableStream" << i;
        setbool(tmpName.str().c_str(), dynamic_cast<xdata::Boolean *>(getvector("InputPorts")[i].getField("enable"))->value_);
        tmpName.str("");
        
        tmpName << "Event_Length_bytes_FED" << i;
        setuint32(tmpName.str().c_str(), dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[i].getField("Event_Length_bytes"))->value_);
        tmpName.str("");
        
        tmpName << "Event_Length_Stdev_bytes_FED" << i;
        setuint32(tmpName.str().c_str(), dynamic_cast<xdata::UnsignedInteger32 *>(getvector("InputPorts")[i].getField("Event_Length_Stdev_bytes"))->value_);
        tmpName.str("");
    }

    writeInfoSpace();
    pushInfospace();
}

void ferol40::ApplicationInfoSpaceHandler::mirrorFromFlatParams()
{
    //JRF NOTE, we mirror the flat params used by the Function Manager to configure small parts of the infospace during state changes:
    //enableStream0,1,2,3
    //Event_Length_bytes_FED0,1,2,3
    //Event_Length_Stdev_bytes_FED0,1,2,3
    
    std::stringstream tmpName;
    tmpName.str("");
    
    for (int i = 0; i < 4; i++) {
        
        tmpName << "enableStream" << i;
        setvectorelementbool("InputPorts", "enable", getbool(tmpName.str().c_str()), i, true);
        tmpName.str("");
        
        tmpName << "Event_Length_bytes_FED" << i;
        setvectorelementuint32("InputPorts", "Event_Length_bytes", getuint32(tmpName.str().c_str()), i, true);
        tmpName.str("");
        
        tmpName << "Event_Length_Stdev_bytes_FED" << i;
        setvectorelementuint32("InputPorts", "Event_Length_Stdev_bytes", getuint32(tmpName.str().c_str()), i, true);
        tmpName.str("");
    }
}
