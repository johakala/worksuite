#include <sstream>
#include "ferol40/FrlDataSource.hh"
#include "d2s/utils/Exception.hh"
#include "ferol40/ferol40Constants.h"
#include "ferol40/loggerMacros.h"

ferol40::FrlDataSource::FrlDataSource( HAL::HardwareDeviceInterface *device_P,
                                     utils::InfoSpaceHandler &appIS,
                                     Logger logger)
    : DataSourceIF( device_P, appIS, logger )
{
}

void
ferol40::FrlDataSource::setDataSource() const
{
    if ( ( dataSource_ == GENERATOR_SOURCE ) ||
         ( dataSource_ == SLINK_SOURCE ) )
        {
            DEBUG( "Setting data source to 0x0. ");
            device_P->write( "FRAGMENT_DATA_SOURCE", 0x0 );
        }
    else
        {
            std::stringstream msg;
            msg << "Encountered illegal DataSource for operationMode \"" 
                << operationMode_ 
                << "\" : \""
                << dataSource_
                << "\". Cannot continue!";
            ERROR( msg );
            XCEPT_RAISE( utils::exception::Ferol40Exception, msg.str() );

        }
}


