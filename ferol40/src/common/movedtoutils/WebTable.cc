#include "d2s/utils/WebTable.hh"
#include "d2s/utils/WebServer.hh"
#include <vector>

utils::WebTable::WebTable( std::string name,
                           std::string description,
                           std::string itemSetName,
                           utils::Monitor &monitor )
    : monitor_( monitor )
{
    name_ = name;
    description_ = description;
    itemSetName_ = itemSetName;
}

void
utils::WebTable::print( std::ostream *out )
{

    if ( name_ == "__notab__" ) 
            return;

    std::list< std::vector<std::string> > items = monitor_.getFormattedItemSet( itemSetName_ );
    std::list< std::vector<std::string> >::const_iterator it;
    bool tableEnabled(true);

    for ( it = items.begin(); it != items.end(); it++ )
    {
	if ( ( (*it)[0] == "enable" ) && ((*it)[1] == "0") )
	    tableEnabled = false;
    }

    if (tableEnabled) 
    {

    	*out << "<p class=\"itemTableTitle\">" << name_ << "</p>\n";
    	*out << "<p class=\"tableDescription\">" << description_ << "</p>\n";
    	*out << "<table class=\"itemtable\">\n"
    	     << "  <tr><th>item</th><th>value</th></tr>\n";
    } 
    else 
    {
	*out << "<p class=\"itemTableTitleGrayed\">" << name_ << "</p>\n";
        *out << "<p class=\"tableDescriptionGrayed\">" << description_ << "</p>\n";
        *out << "<table class=\"itemtablegrayed\">\n"
             << "  <tr><th>item</th><th>value</th></tr>\n";

    }
//    std::list< std::vector<std::string> > items = monitor_.getFormattedItemSet( itemSetName_ );
//    std::list< std::vector<std::string> >::const_iterator it;

    uint32_t ix = 0;
    std::string cssclass = "even";
    for ( it = items.begin(); it != items.end(); it++ )
        {
            if ( ix%2 == 0 ) 
                {
                    cssclass = "even";
                }
            else
                {
                    cssclass = "odd";
                }
            *out << "<tr class=\"" << cssclass << "\"><td title=\""<< htmlEscape((*it)[2]) << "\">" << (*it)[0] << "</td><td id=\"" << itemSetName_ << "_" << (*it)[0] << "\">" << (*it)[1] << "</td></tr>\n";
            ix++;
        }
    *out << "</table>\n";

}

void 
utils::WebTable::jsonUpdate( std::ostream *out )
{

    if ( name_ == "__notab__" ) return;
    *out << "\"" << itemSetName_ << "\" : [ \n";
    std::list< std::vector<std::string> > items = monitor_.getFormattedItemSet( itemSetName_ );
    std::list< std::vector<std::string> >::const_iterator it;

    for ( it = items.begin(); it != items.end(); it++ )
        {
            std::string val = utils::WebServer::jsonEscape( (*it)[1] );
            *out << "{ \"name\":\"" << itemSetName_ << "_" << (*it)[0] << "\",\"value\":\"" << val << "\" },\n";
        }
    *out << " ],\n";

}

std::string
utils::WebTable::htmlEscape( std::string orig ) const
{
    std::string::const_iterator it = orig.begin();
    std::string res;
    
    for ( it = orig.begin(); it != orig.end(); it++ )
        {
            if ( (*it) == '"' ) 
                {
                    res.append( "&quot;" );
                }
            else
                {
                    res.append(1,*it);
                }
        }


    size_t pos = 0;
    while ( (pos = res.find( "<br>", 0 )) != std::string::npos ) 
        {
            res.replace( pos, 4, "\n" );
            pos = 0;
        };
        

    return res;
}



