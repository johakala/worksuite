#include "d2s/utils/WebTableTab.hh"

utils::WebTableTab::WebTableTab(  std::string name, 
                                  std::string description, 
                                  uint32_t columns,
                                  utils::Monitor &monitor )
    : monitor_ (monitor)
{
    name_ = name;
    description_ = description;
    columns_ = columns;
}

void
utils::WebTableTab::registerTable( std::string name,
                                   std::string description,
                                   std::string itemSetName )
{
    utils::WebTable webtable( name, description, itemSetName, monitor_ );
    tableList_.push_back( webtable );
}

void
utils::WebTableTab::print( std::ostream *out )
{
    uint32_t icol = 1;
    *out << "\n\
        <div class=\"tab-page\" id=\"page1\">\n\
        <h2 class=\"tab\">" << name_ << "</h2>\n\
        <div class=\"tabdescription\">" << description_ << "</div>\n\
        <table class=\"tabtable\">\n";
    std::list< utils::WebTable >::iterator it;
    for ( it = tableList_.begin(); it != tableList_.end(); it++ ) 
        {

            if ( icol == 1 )
                {
                    *out << "<tr>\n";
                }
            *out << "  <td>\n";
            (*it).print( out );
            *out << " </td>\n";
            if ( icol == columns_ ) 
                {
                    *out << "</tr>\n";
                    icol = 1;
                }
            else 
                {
                    icol ++;
                }
        }

    *out << "</table>\n";
    *out << "</div>\n";

}


void 
utils::WebTableTab::jsonUpdate( std::ostream *out )
{
    std::list< utils::WebTable >::iterator it;
    for ( it = tableList_.begin(); it != tableList_.end(); it++ )
        {
            (*it).jsonUpdate( out );
        }            
}
