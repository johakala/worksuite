#include "ferol40/FrlInfoSpaceHandler.hh"
#include "ferol40/loggerMacros.h"

ferol40::FrlInfoSpaceHandler::FrlInfoSpaceHandler( xdaq::Application* xdaq, utils::InfoSpaceUpdater *updater )
    : utils::InfoSpaceHandler( xdaq, "Frl_IS", updater )
{
    createuint32( "slotNumber", 0, "", NOUPDATE);
    createuint32( "FEROL40_BA", 0, "hex", NOUPDATE );
    createuint32( "CMCFwVersion_L0", 0, "hex", NOUPDATE );
    createuint32( "CMCFwVersion_L1", 0, "hex", NOUPDATE );

    createuint64( "Time_LO", 0, "", HW64, "A free running counter at 1Mhz used for rate calculations");
    createuint64( "EventCount_L0_LO", 0);
    createuint64( "EventCount_L1_LO", 0);
    createuint32( "TrigNo_L0", 0);
    createuint32( "TrigNo_L1", 0);
    createuint32( "BX0", 0 );
    createuint32( "BX1", 0 );

    createuint64( "CRC_error_L0_LO", 0, "", HW64, "Slink CRC errors for link 0.");
    createuint64( "CRC_error_L1_LO", 0, "", HW64, "Slink CRC errors for link 1.");
    createuint64( "CRC_FED_L0_LO", 0, "", HW64, "FED CRC errors for link 0.");
    createuint64( "CRC_FED_L1_LO", 0, "", HW64, "FED CRC errors for link 0.");

    createuint64( "BP_L0_LO", 0, "", PROCESS, "A 44 bit number which measures the time the SLINK sender card applies ink-full to the FED. The unit is number of clock cycles of the FED clock (the FED clock is a clock signal provided by the FED to the SLINK sender card). This number is transfered regularly to the FRL via the SLINK and is latched into a register in the FRL. This register is read via this function. The counter is reset each time the SLINK command mode is entered." );
    createuint64( "BP_L1_LO", 0, "", PROCESS, "A 44 bit number which measures the time the SLINK sender card applies ink-full to the FED. The unit is number of clock cycles of the FED clock (the FED clock is a clock signal provided by the FED to the SLINK sender card). This number is transfered regularly to the FRL via the SLINK and is latched into a register in the FRL. This register is read via this function. The counter is reset each time the SLINK command mode is entered.");
    createuint64( "FIFO_BP_L0_LO", 0, "", HW64, "Counts the number of clock cycles (100MHz clock) during which the input fifo of the SLINK receiver is almost full. (The FRL then sends a signal to the sender side which causes SLINK_full to be asserted)");
    createuint64( "FIFO_BP_L1_LO", 0, "", HW64, "Counts the number of clock cycles (100MHz clock) during which the input fifo of the SLINK receiver is almost full. (The FRL then sends a signal to the sender side which causes SLINK_full to be asserted)");
    createuint32( "FED_freq_L0", 0, "", HW32, "counts the number of fed-clocks for 100ms (i.e. divide by 100 to get the fed clock in MHz)"); 
    createuint32( "FED_freq_L1", 0, "", HW32, "counts the number of fed-clocks for 100ms (i.e. divide by 100 to get the fed clock in MHz)");
    createuint64( "BP_running_cnt_LO", 0);

    createuint32( "Biggest_frag_size_received_L0", 0, "", HW32, "The biggest fragment size in 64bit words received on this link during this run");
    createuint32( "Biggest_frag_size_received_L1", 0, "", HW32, "The biggest fragment size in 64bit words received on this link during this run");
    createuint32( "Current_frag_size_received_L0", 0, "", HW32, "This counter counts the incoming 64bit words when they are read out of the input fifo of the FRL. This value is useful if the DAQ is stuck and this link is in the middle of receiving a fragment.");
    createuint32( "Current_frag_size_received_L1", 0, "", HW32, "This counter counts the incoming 64bit words when they are read out of the input fifo of the FRL. This value is useful if the DAQ is stuck and this link is in the middle of receiving a fragment.");

    createdouble( "AccSlinkFullSec_L0", 0, "", NOUPDATE, "This item contains the time in seconds during which backpressure was asserted to the slink sender card. It is calculated based on the counter in the slink sender (see above)." );
    createdouble( "AccSlinkFullSec_L1", 0, "", NOUPDATE, "This item contains the time in seconds during which backpressure was asserted to the slink sender card. It is calculated based on the counter in the slink sender (see above).");    

    createuint32( "gen_pending_trg_L0", 0);
    createuint32( "gen_pending_trg_L1", 0);

    createuint32( "FrlFwVersion", 0, "hex", NOUPDATE );
    createuint32( "FrlFwType", 0, "hex", NOUPDATE );
    createuint32( "FrlHwRevision", 0, "hex", NOUPDATE );
    createuint32( "BridgeFwVersion", 0, "hex", NOUPDATE );
    createuint32( "BridgeFwType", 0, "hex", NOUPDATE );
    createuint32( "BridgeHwRevision", 0, "hex", NOUPDATE );
    createuint32( "FedIdWrong_Stream0", false, "", HW32, "Indicates that the expected fed-id does not correspond to the fed-id read from the SLINK fragments.");
    createuint32( "FEDID_WRONG_L0", 0, "", HW32, "If a wrong Fed-id has been detected this register contains the last wrong number which has been detected in an incoming SLINK header");
    createuint32( "FedIdWrong_Stream1", false, "", HW32, "Indicates that the expected fed-id does not correspond to the fed-id read from the SLINK fragments.");
    createuint32( "FEDID_WRONG_L1", 1, "", HW32, "If a wrong Fed-id has been detected this register contains the last wrong number which has been detected in an incoming SLINK header");
    createbool( "FrlFwChanged", false, "", NOUPDATE );
}
