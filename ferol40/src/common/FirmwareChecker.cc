#include "ferol40/FirmwareChecker.hh"
#include "ferol40/loggerMacros.h"
#include <sstream>
#include <iomanip>
#include "ferol40/Ferol40.hh"

ferol40::FirmwareChecker::FirmwareChecker(HAL::PCIDevice *ferol40Device, bool dontFail)
{

    ferol40Device->read("FirmwareVersion", &ferol40FwVersion_);
    ferol40Device->read("FirmwareType", &ferol40FwType_);
    ferol40Device->read("HardwareRevision", &ferol40HwRevision_);

    fpgaType_ = FEROL40;
    dontFail_ = dontFail;
}

bool ferol40::FirmwareChecker::checkFirmware(std::string mode, std::string dataSource, std::string &errorstr, bool *changedFw)
{
    bool result = true;
    if (fpgaType_ == FEROL40)
    {
        result = checkFerol40Firmware(mode, dataSource, errorstr);
    }
    else
    {
        result = false;
        errorstr = "Illegal fpgaType encountered (probably a software bug)";
    }

    if (dontFail_)
        result = true;

    return result;
}

bool ferol40::FirmwareChecker::checkFerol40Firmware(std::string mode, std::string dataSource, std::string &errorstr)
{
    bool ret = true;
    std::stringstream error;

    // There are two generations of FRLs which are used.
    // The newer type has bit 0 of the type set to 1
    // whereas the older has this bit set to 0. We accept
    // both utils::versions since they are functionally compatible.

    // check the FEROL40 type:
    uint32_t requiredFirmwareType = 0;
    uint32_t requiredFirmwareVersion = 0;

    requiredFirmwareType = FEROL40_DAQ_FIRMWARE_TYPE;
    requiredFirmwareVersion = FEROL40_DAQ_FIRMWARE_MIN_VERSION;

    if (ferol40FwType_ != requiredFirmwareType)
    {
        ret = false;
        error << "Error verifying FEROL40 firmware type - expected "
              << std::hex << requiredFirmwareType << " read "
              << ferol40FwType_;
    }

    // check the ferol40 fw utils::version
    if (ferol40FwVersion_ < requiredFirmwareVersion)
    {
        ret = false;
        error << "Error verifying FEROL40 firmware utils::version - expected >= "
              << std::hex << requiredFirmwareVersion << " read "
              << ferol40FwVersion_;
    }
    errorstr = error.str();

    return ret;
}

uint32_t

ferol40::FirmwareChecker::getFerol40FirmwareVersion() const
{
    return ferol40FwVersion_;
}

uint32_t
ferol40::FirmwareChecker::getFerol40FirmwareType() const
{
    return ferol40FwType_;
}

uint32_t
ferol40::FirmwareChecker::getFerol40HardwareRevision() const
{
    return ferol40HwRevision_;
}

//void ferol40::FrlFirmwareChecker::reloadFrlFirmware( Frl *frl,
//                                                   Ferol40 *ferol40,
//                                                   uint32_t firmwareId )
//{
//    return;
//
//    // this is not so easy: first the monitoring loops have to be shut down.
//    // No access during this gymnastics! Also the Ferol40 must not be touched
//    // since during the procedure the hardware issues a PCI reset to the ferol40
//    // so that also the ferol40 looses the PCI configuration and all other
//    // configuration. Therefore this reload has to be handled differently...
//
//    HAL::PCIDevice *frlDevice = frl->frlDevice_P;
//    HAL::PCIDevice *bridgeDevice = frl->bridgeDevice_P;
//    HAL::PCIDevice *ferol40Device = ferol40->ferol40Device_P;
//    this->saveFrlConfigSpace( frlDevice );
//    this->saveFerol40ConfigSpace( ferol40Device );
//    uint32_t ctrl = (firmwareId << 16) + 0x04; // firmware Id to bit 16..18, bit 2 set to trigger reload
//    bridgeDevice->write( "firmwareControl", ctrl );
//    ::sleep(4);
//    this->writeBackFrlConfigSpace( frlDevice );
//    this->writeBackFrlConfigSpace( ferol40Device );
//    std::cout << "all done" << std::endl;
//}
