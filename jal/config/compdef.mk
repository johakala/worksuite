##################################################################
#
# compdef.mk
#
#  Compiler definitions
#
#
#################################################################

##########################
# Compiler section
##########################
CC  = cc
CCC = gcc

##########################
# Flags section
##########################
CCDEFAULT    = -c -O3 -Wall -Wstrict-prototypes \
               -D_POSIX_C_SOURCE -D_POSIX_THREAD_SEMANTICS 
CCFLAGS      = $(CCDEFAULT)

##########################
# Source code file section
##########################
CFILES  := $(shell echo *.c)
OCFILES  = $(CFILES:.c=.o)
CCFILES := $(shell echo *.cpp)
OFILES   = $(CCFILES:.cpp=.o)

##########################
# Linker section
##########################
LL     = g++
LFLAGS =
LMAP   = link.map


#################################################################
#
# End of compdef.mk
#
#################################################################
