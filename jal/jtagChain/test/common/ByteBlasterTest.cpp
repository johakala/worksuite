#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagController/ByteBlasterJTAGController.h"

#include <iostream>


using namespace std;
using namespace jal;

int main() {

  try {

    // create the ByetBlaster JTAG Controller
    ByteBlasterJTAGController jtag_controller;

    // open JTAG Chain for chain 0 on the controller
    JTAGChain jtag_chain( jtag_controller, 0 );
    
    // reset the chain
    jtag_chain.gotoState( JTAGState::RESET );

    // prepare JTAG scan data vectors
    JTAGScanData in_data(32,0xff); // Input data: 32 bytes of FF
    JTAGScanData response_data;    // vector for output data 

    // do a 256 bit DR Scan
    jtag_chain.scanDR(256, in_data, response_data);

    cout << "Result of DR Scan: " << response_data << endl;
    
    // run the JTAG clock for 100 cycles in IDLE state
    jtag_chain.runTest ( 100, JTAGState::IDLE, JTAGState::IDLE );

    // do a 256 bit IR Scan
    jtag_chain.scanIR(256, in_data, response_data);

    cout << "Result of IR Scan: " << response_data << endl;

  }
  catch (exception &e) {
    cout << "Unknown Exception caught: " << e.what() << endl;
    return 1;
  }
  return 0;
}

