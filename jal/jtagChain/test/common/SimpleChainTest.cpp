#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagController/ScanPSC100JTAGController.h"
#include "jal/jtagController/HALScanPSC100Adapter.h"

#include "hal/VMEDevice.hh"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEAddressTableASCIIReader.hh"
#include "hal/SBS620x86LinuxBusAdapter.hh"

#include <iostream>

#include <stdint.h>

using namespace std;
using namespace HAL;
using namespace jal;

int main() {

  uint32_t VMEbase = 0xA10000;
  char *addrtable_fn = "/home/sakulin/AddressTable_Outcard.txt";

  try {

    VMEAddressTableASCIIReader addressTableReader(addrtable_fn);
    VMEAddressTable addressTable("Outcard address table", addressTableReader);

    SBS620x86LinuxBusAdapter busAdapter(0);

    VMEDevice outcard(addressTable, busAdapter, VMEbase);

    // create a HAL adapter for a ScanPSC type JTAG Controller
    HALScanPSC100Adapter scanpsc_adapter (outcard, "PSC100_BASE"); 

    // create the JTAG Controller using the adapter
    ScanPSC100JTAGController jtag_controller( scanpsc_adapter );

    // open JTAG Chain for chain 0 on the controller
    JTAGChain jtag_chain( jtag_controller, 0 );
    
    // reset the chain
    jtag_chain.gotoState( JTAGState::RESET );

    // prepare JTAG scan data vectors
    JTAGScanData in_data(32,0xff); // Input data: 32 bytes of FF
    JTAGScanData response_data;    // vector for output data 

    // do a 256 bit DR Scan
    jtag_chain.scanDR(256, in_data, response_data);

    cout << "Result of DR Scan: " << response_data << endl;
    
    // run the JTAG clock for 100 cycles in IDLE state
    jtag_chain.runTest ( 100, JTAGState::IDLE, JTAGState::IDLE );

    // do a 256 bit IR Scan
    jtag_chain.scanIR(256, in_data, response_data);

    cout << "Result of IR Scan: " << response_data << endl;

  }
  catch (exception &e) {
    cout << "Unknown Exception caught: " << e.what() << endl;
    return 1;
  }
  return 0;
}

