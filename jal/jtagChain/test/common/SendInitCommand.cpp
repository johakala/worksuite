#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagController/ScanPSC100JTAGController.h"
#include "jal/jtagController/HALScanPSC100Adapter.h"

#include "hal/VMEDevice.hh"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEAddressTableASCIIReader.hh"
#include "hal/SBS620x86LinuxBusAdapter.hh"

#include <iostream>

#include <stdint.h>

using namespace std;
using namespace HAL;
using namespace jal;

int main() {

  uint32_t VMEbase = 0xA10000;
  char *addrtable_fn = "/home/sakulin/AddressTable_Outcard.txt";

  try {

    VMEAddressTableASCIIReader addressTableReader(addrtable_fn);
    VMEAddressTable addressTable("Outcard address table", addressTableReader);

    SBS620x86LinuxBusAdapter busAdapter(0);

    VMEDevice outcard(addressTable, busAdapter, VMEbase);

    // create a HAL adapter for a ScanPSC type JTAG Controller
    HALScanPSC100Adapter scanpsc_adapter (outcard, "PSC100_BASE"); 

    // create the JTAG Controller using the adapter
    ScanPSC100JTAGController jtag_controller( scanpsc_adapter );

    // set the system clock frequency of the hardware
    jtag_controller.setSCKFrequency(1.e6); // Outcard works with 1 MHz JTAG clock

    // set debug level
    // jtag_controller.setDebugFlag(4); 

    // open a JTAG Chain on the controller
    JTAGChain jtag_chain( jtag_controller, 0 );
    
    // reset the chain
    jtag_chain.gotoState( JTAGState::RESET );

    // prepare JTAG scan data vectors
    JTAGScanData in_data(32,0xff); // Input data: 32 bytes of FF
    JTAGScanData response_data;    // vector for output data 

    // do a DR Scan
    jtag_chain.scanDR(256, in_data, response_data);

    cout << "Result of DR Scan: " << response_data << endl;
    
    // run the JTAG clock for 100 cycles in IDLE state
    jtag_chain.runTest ( 100, JTAGState::IDLE, JTAGState::IDLE );

    // do an IR Scan
    jtag_chain.scanIR(256, in_data, response_data);

    cout << "Result of IR Scan: " << response_data << endl;

    //    cout << "Size of response data: " << response_data.size() << endl;

    JTAGScanData cmd_init_conf(7,0xff);

    cmd_init_conf[4] = 0x18;
    cmd_init_conf[3] = 0x7f;
    cmd_init_conf[2] = 0xff;
    cmd_init_conf[1] = 0xff;
    cmd_init_conf[0] = 0xff;


    JTAGScanData cmd_bypass(7,0xff);
    
    cout << "Sending init_conf command: " << endl;
    jtag_chain.scanIR(50, cmd_init_conf, response_data);    
    cout << "Result of IR Scan: " << response_data << endl;

//     cout << "Runtest idle for 1000 cycles " << endl;
//     jtag_chain.runTest ( 1000, JTAGState::IDLE, JTAGState::IDLE );
    
//     // reset the chain
//     jtag_chain.gotoState( JTAGState::RESET );

//     // do a DR Scan
//     jtag_chain.scanDR(256, in_data, response_data);
//     cout << "Result of DR Scan: " << response_data << endl;

//     cout << "Sending bypass command: " << endl;
//     jtag_chain.scanIR(50, cmd_bypass, response_data);    
//     cout << "Result of IR Scan: " << response_data << endl;

  }
  catch(HardwareAccessException &e) {
    cout << "Caught HardwareAccessException: " << e.what() << "\n" << endl;
    return 1;
  }
  catch (exception &e) {
    cout << "Unknown Exception caught: " << e.what() << endl;
    return 1;
  }
  return 0;
}

