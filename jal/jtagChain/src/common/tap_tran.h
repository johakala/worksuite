/******************************************************************************
*******************************************************************************

  Copyright 1994 National Semiconductor Corporation
 
  National Semiconductor Corporation makes no warranty for the use of
  its products and assumes no responsibility for any errors which may
  appear in this file nor does it make a commitment to update the
  information contained herein.

  Filename: tap_tran.h

  Description: Header file for IEEE 1149.1 TAP transition data structures.

  Revision History:

    Revision   Date     Comments
    -----------------------------------------------------------------------
    1.00       01MAY95  Initial Revision
    1.02         Oct95  QA/Clean-up.

                                                      SS $Revision: 1.2 $
*******************************************************************************
******************************************************************************/
#ifndef _TAP_TRAN_H
#define _TAP_TRAN_H

/* Next State TMS values.  Can be used to get to any legal next state. */
#define NS_TLR  1
#define NS_RTI  0
#define NS_PDR  0
#define NS_PIR  0

#define NS_SEDR 1
#define NS_CPDR 0
#define NS_SHDR 0
#define NS_E1DR 1
#define NS_E2DR 1
#define NS_UPDR 1

#define NS_SEIR 1
#define NS_CPIR 0
#define NS_SHIR 0
#define NS_E1IR 1
#define NS_E2IR 1
#define NS_UPIR 1

struct tap_state_transitions {
  uint8_t tms;
  uint8_t num_tcks;
};

/* Look-up Table for TMS values of TAP state transitions*/
/* This defines a two dimentional array of structures.  The first index */
/* indicates the start state, the second index indicates the end state. */

static const struct tap_state_transitions Tap_transitions[4][4] = {
  /* Sequence from present state TLR */
  { {NS_TLR, 1},
    {NS_RTI, 1},
    {NS_RTI | NS_SEDR << 1 | NS_CPDR << 2 | NS_E1DR << 3 | NS_PDR << 4, 5},
    {NS_RTI | NS_SEDR << 1 | NS_SEIR << 2 | NS_CPIR << 3 | NS_E1IR << 4 | NS_PIR << 5, 6}
  },
  /* Sequence from present state RTI */
  { {NS_SEDR | NS_SEIR << 1 | NS_TLR <<2, 3},
    {NS_RTI, 0},
    {NS_SEDR | NS_CPDR << 1 | NS_E1DR << 2 | NS_PDR << 3, 4},
    {NS_SEDR | NS_SEIR << 1 | NS_CPIR << 2 | NS_E1IR << 3 | NS_PIR << 4, 5}
  },
  /* Sequence from present state PDR */
  { {NS_E2DR | NS_UPDR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_TLR << 4, 5},
    {NS_E2DR | NS_UPDR << 1 | NS_RTI << 2, 3},
    {NS_E2DR | NS_UPDR << 1 | NS_SEDR << 2 | NS_CPDR << 3 | NS_E1DR << 4 | NS_PDR << 5, 6},
    {NS_E2DR | NS_UPDR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_CPIR << 4 | NS_E1IR << 5 | NS_PIR << 6, 7}
  },
  /* Sequence from present state PIR */
  { {NS_E2IR | NS_UPIR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_TLR << 4, 5},
    {NS_E2IR | NS_UPIR << 1 | NS_RTI << 2, 3},
    {NS_E2IR | NS_UPIR << 1 | NS_SEDR << 2 | NS_CPDR << 3 | NS_E1DR << 4 | NS_PDR << 5, 6},
    {NS_E2IR | NS_UPIR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_CPIR << 4 | NS_E1IR << 5 | NS_PIR << 6, 7}
  }
};

static const struct tap_state_transitions Tms_to_shir[4] = {
  /* Sequence from present state to SHIR */
  {NS_RTI | NS_SEDR << 1 | NS_SEIR << 2 | NS_CPIR << 3 | NS_SHIR << 4, 5},
  {NS_SEDR | NS_SEIR << 1 | NS_CPIR << 2 | NS_SHIR << 3, 4},
  {NS_E2DR | NS_UPDR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_CPIR << 4 | NS_SHIR << 5, 6},

  {NS_E2IR | NS_UPIR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_CPIR << 4 | NS_SHIR << 5, 6}
  // Hannes Sakulin: return from Pause via update and capture
  //  { NS_E2IR | NS_SHIR << 1, 2}
};

static const struct tap_state_transitions Tms_to_shdr[4] = {
  /* Sequence from present state to SHDR */
  {NS_RTI | NS_SEDR << 1 | NS_CPDR << 2 | NS_SHDR << 3, 4},
  {NS_SEDR | NS_CPDR << 1 | NS_SHDR << 2, 3},
  //  {NS_E2DR | NS_SHDR << 1, 2},
  // Hannes Sakulin: return from Pause via update and capture
  {NS_E2DR | NS_UPDR << 1 | NS_SEDR << 2 | NS_CPDR << 3 | NS_SHDR << 4, 5},
  {NS_E2IR | NS_UPIR << 1 | NS_SEDR << 2 | NS_CPDR << 3 | NS_SHDR << 4, 5}
};

static const struct tap_state_transitions Tms_to_upir[4] = {
  /* Sequence from present state to UPIR */
  {NS_RTI | NS_SEDR << 1 | NS_SEIR << 2 | NS_CPIR << 3 | NS_E1IR << 4 | NS_UPIR << 5, 6},
  {NS_SEDR | NS_SEIR << 1 | NS_CPIR << 2 | NS_E1IR << 3 | NS_UPIR << 4, 5},
  {NS_E2DR | NS_UPDR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_CPIR << 4 | NS_E1IR << 5 | NS_UPIR << 6, 7},
  {NS_E2IR | NS_UPIR << 1, 2}
};

static const struct tap_state_transitions Tms_to_updr[4] = {
  /* Sequence from present state to UPDR */
  {NS_RTI | NS_SEDR << 1 | NS_CPDR << 2 | NS_E1DR << 3 | NS_UPDR << 4, 5},
  {NS_SEDR | NS_CPDR << 1 | NS_E1DR << 2 | NS_UPDR << 3, 4},
  {NS_E2DR | NS_UPDR << 1, 2},
  {NS_E2IR | NS_UPIR << 1 | NS_SEDR << 2 | NS_CPDR << 3 | NS_E1DR << 4 | NS_UPDR << 5, 6}
};

static const struct tap_state_transitions Tms_from_update[4] = {
  /* Sequence from update-ir/dr to end state */
  {NS_SEDR | NS_SEIR << 1 | NS_TLR << 2, 3},
  {NS_RTI, 1},
  {NS_SEDR | NS_CPDR << 1 | NS_E1DR << 2 | NS_PDR << 3, 4},
  {NS_SEDR | NS_SEIR << 1 | NS_CPIR << 2 | NS_E1IR << 3 | NS_PIR << 4, 5}
};

// static const struct tap_state_transitions Tms_from_shir[4] = {
//   /* Sequence from shift-ir to end state */
//   {NS_E1IR | NS_UPIR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_TLR << 4, 5},
//   {NS_E1IR | NS_UPIR << 1 | NS_RTI << 2, 3},
//   {NS_E1IR | NS_UPIR << 1 | NS_SEDR << 2 | NS_CPDR << 3 | NS_E1DR << 4 | NS_PDR << 5, 6},
//   {NS_E1IR | NS_UPIR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_CPIR << 4 | NS_E1IR << 5 | NS_PIR << 6, 7}
// };
// static const struct tap_state_transitions Tms_from_shdr[4] = {
//   /* Sequence from shift-ir to end state */
//   {NS_E1DR | NS_UPDR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_TLR << 4, 5},
//   {NS_E1DR | NS_UPDR << 1 | NS_RTI << 2, 3},
//   {NS_E1DR | NS_UPDR << 1 | NS_SEDR << 2 | NS_CPDR << 3 | NS_E1DR << 4 | NS_PDR << 5, 6},
//   {NS_E1DR | NS_UPDR << 1 | NS_SEDR << 2 | NS_SEIR << 3 | NS_CPIR << 4 | NS_E1IR << 5 | NS_PIR << 6, 7}
// };

static const struct tap_state_transitions Tms_from_e1ir[4] = {
  /* Sequence from shift-ir to end state */
  {NS_UPIR | NS_SEDR << 1 | NS_SEIR << 2 | NS_TLR  << 3, 4},
  {NS_UPIR | NS_RTI  << 1, 2},
  {NS_UPIR | NS_SEDR << 1 | NS_CPDR << 2 | NS_E1DR << 3 | NS_PDR  << 4, 5},
  //  {NS_UPIR | NS_SEDR << 1 | NS_SEIR << 2 | NS_CPIR << 3 | NS_E1IR << 4 | NS_PIR << 5, 6}
  {NS_PIR, 1} // Hannes Sakulin - needed for Altera => have to check if it works with xilinx.
};
static const struct tap_state_transitions Tms_from_e1dr[4] = {
  /* Sequence from shift-ir to end state */
  {NS_UPDR | NS_SEDR << 1 | NS_SEIR << 2 | NS_TLR  << 3, 4},
  {NS_UPDR | NS_RTI  << 1, 2},
  //  {NS_UPDR | NS_SEDR << 1 | NS_CPDR << 2 | NS_E1DR << 3 | NS_PDR  << 4, 5},
  {NS_PDR, 1}, // Hannes Sakulin - needed for Altera => have to check if it works with xilinx.
  {NS_UPDR | NS_SEDR << 1 | NS_SEIR << 2 | NS_CPIR << 3 | NS_E1IR << 4 | NS_PIR << 5, 6}
};

#endif

