#include "jal/jtagChain/JTAGScanData.h"

#include <iomanip>

using namespace std;

ostream& jal::operator<<(ostream &os, const jal::JTAGScanData& d) {
    
    for (jal::JTAGScanData::const_reverse_iterator it = d.rbegin(); it!=d.rend(); it++)
      os << hex << setw(2) << setfill('0') << ((int) *it) << dec << setfill (' ');
    return os;

}


void jal::JTAGScanData::expand(uint32_t bitcount, uint8_t default_byte) {

  // expand if not specified
  if (size() == 0 && bitcount != 0) {

    uint32_t bytecount = (bitcount+7) / 8;
    uint32_t start_bit = bitcount % 8;
    uint32_t unused_bits = (8-start_bit) % 8;

    resize(bytecount, default_byte);

    // clear the upper bits in the last byte.
    (*this)[bytecount-1] &= ( 0xff >> unused_bits );
  }
    
};


void jal::JTAGScanData::add(JTAGScanData const& d, 
			    uint32_t initial_bitcount, 
			    uint32_t bits_to_add, 
			    uint8_t default_byte) {

  // return if no bits to add
  if (bits_to_add == 0) 
    return;

  // no adding if both JTAGScanData have size 0 (i.e. both parameters are not specified)
  if (size() == 0 && d.size() == 0)
    return;

  // expand self (in case it is not specified)
  expand(initial_bitcount, default_byte);

  // expand additional data (in case it is not specified)
  jal::JTAGScanData expanded_d(d);
  expanded_d.expand(bits_to_add, default_byte);

  uint32_t initial_bytecount = (initial_bitcount+7) / 8;
  uint32_t start_bit = initial_bitcount % 8;

  // shrink the vector if necessary
  if (size() > initial_bytecount) {
    cout << "jal::JTAGScanData::add(): warning!!! there are too many bytes in the JTAGScanData. This should not happen." << endl;
    while (size() > initial_bytecount) 
      pop_back();
  }

  // grow the vector if it is shorter than the initial bitcount (leading zeros)
  while (size() < initial_bytecount) 
    push_back (0);

  // now add the bits
  uint32_t added_bits = 0;
  jal::JTAGScanData::const_iterator it = expanded_d.begin();
  for (;it!=expanded_d.end();it++) {
      
    if (start_bit==0)
      push_back( (*it) );
    else {
      uint32_t data = (uint32_t) (*it);
      data <<= start_bit;
	
      // add part of data to last byte
      back() |= ( data & 0xff);
      added_bits += 8-start_bit;
	
      // store rest of data in new byte (but only if they are used)
      if (added_bits < bits_to_add) {
	push_back ( ( data & 0xff00 ) >> 8 );
	added_bits += start_bit;
      }

    }
  }
}

// return a range from a JTAGScanData
jal::JTAGScanData jal::JTAGScanData::getRange (uint32_t start_bit, 
					       uint32_t nbits) {

  jal::JTAGScanData tmp;
    

  uint8_t tmp_byte = 0;
  uint32_t target_bit =0;
  for (uint32_t ibit=start_bit; ibit<start_bit+nbits; ibit ++) {

    // read the bit
    uint32_t ibyte = ibit /8;
    uint32_t ibit_inbyte = ibit % 8;
      
    uint8_t byte = ( ibyte  < size())  ? (*this)[ibyte] : 0;
    uint8_t bit = ( byte & ( 1<<ibit_inbyte) ) ? 1: 0;
      
    // write it
    uint32_t target_bit_inbyte = target_bit % 8;
    if (bit) tmp_byte |= ( 1<< target_bit_inbyte);

    target_bit++;
    if ( (target_bit % 8) == 0) {
      tmp.push_back (tmp_byte);
      tmp_byte = 0;
    }
  }

  if ( (target_bit % 8) != 0) {
    tmp.push_back (tmp_byte);
  }

  return tmp;
}



