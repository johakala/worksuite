#include "jal/jtagChain/JTAGState.h"

#include <string>
#include <map>

using namespace std;

jal::JTAGState::JTAGState( std::string const& text ) {
  static bool first = true;
  static map<string, int> statemap;

  if (first) {
    statemap["RESET"] = jal::JTAGState::RESET;
    statemap["IDLE"] = jal::JTAGState::IDLE;
    statemap["DRPAUSE"] = jal::JTAGState::PAUSEDR;
    statemap["IRPAUSE"] = jal::JTAGState::PAUSEIR;
    statemap["DRSELECT"] = jal::JTAGState::SEDR;
    statemap["DRCAPTURE"] = jal::JTAGState::CPDR;
    statemap["DRSHIFT"] = jal::JTAGState::SHDR;
    statemap["DREXIT1"] = jal::JTAGState::E1DR;
    statemap["DREXIT2"] = jal::JTAGState::E2DR;
    statemap["DRUPDATE"] = jal::JTAGState::UPDR;
    statemap["IRSELECT"] = jal::JTAGState::SEIR;
    statemap["IRCAPTURE"] = jal::JTAGState::CPIR;
    statemap["IRSHIFT"] = jal::JTAGState::SHIR;
    statemap["IREXIT1"] = jal::JTAGState::E1IR;
    statemap["IREXIT2"] = jal::JTAGState::E2IR;
    statemap["IRUPDATE"] = jal::JTAGState::UPIR;

    first = false;
  }

  if ( statemap.find(text) == statemap.end() ) 
    _val = jal::JTAGState::UNDEF;
  else
    _val = statemap[text];
}
