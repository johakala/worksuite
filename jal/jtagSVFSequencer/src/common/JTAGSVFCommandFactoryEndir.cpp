#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryEndir.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandEndir.h"

#include "jal/jtagChain/JTAGState.h"

using namespace std;

jal::JTAGSVFCommand* jal::JTAGSVFCommandFactoryEndir::create(vector<string> const& args) 
  throw (jal::SVFSyntaxException) {

  if (args.size() != 2) 
    XCEPT_RAISE(jal::SVFSyntaxException, "Endir command needs exactly one parameter.");
  
  jal::JTAGState state( args[1] );

  if ( !state.isStable() )
    XCEPT_RAISE(jal::SVFSyntaxException, "Endir needs stable state as parameter.");
  
  return (jal::JTAGSVFCommand *) new jal::JTAGSVFCommandEndir( state );
}
