/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/05/15 13:03:20 $
 *
 *
 **/
#include "jal/jtagSVFSequencer/version.h"
#include "jal/jtagChain/version.h"
#include "jal/jtagController/version.h"
#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(jaljtagSVFSequencer)

void jaljtagSVFSequencer::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(jaljtagChain);  
	CHECKDEPENDENCY(jaljtagController);  
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
}

std::set<std::string, std::less<std::string> > jaljtagSVFSequencer::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,jaljtagChain); 
	ADDDEPENDENCY(dependencies,jaljtagController); 
	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	 
	return dependencies;
}	
	
