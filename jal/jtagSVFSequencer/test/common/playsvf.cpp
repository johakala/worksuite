#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagController/DTTFJTAGController.h"
#include "jal/jtagController/ScanPSC100JTAGController.h"
#include "jal/jtagController/HALScanPSC100Adapter.h"
#include "jal/jtagSVFSequencer/JTAGSVFSequencer.h"

#include "hal/VMEDevice.hh"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEAddressTableASCIIReader.hh"
#include "hal/SBS620x86LinuxBusAdapter.hh"
#include "hal/VMEDummyBusAdapter.hh"
#include "hal/linux/StopWatch.hh"

#include <iostream>
#include <iomanip>

#include <ctype.h>
#include <string>
#include <stdint.h>
#include <sstream>

using namespace std;
using namespace HAL;
using namespace jal;

int main(int argc, char **argv) {
  
  // say hello
  cout << endl;
  cout << "====================================" << endl;
  cout << "This is playsvf  ($Revision: 1.1 $)" << endl;
  cout << "====================================" << endl;
  cout << "A utility to play SVF files to a JTAG chain via HAL and VME." << endl;
  cout << "Use with care. Incorrect use may damage the hardware. No explicit or implied warranty." << endl;
  cout << endl;

  // the default parameters
  uint32_t VMEbase = 0xA10000;
  string addrtable_fn = "/home/hsakulin/jtag_test2/AddressTable_Outcard.txt";
  bool use_dttf_jctrl = false;
  bool check_only = false;
  bool debug_mode = false;
  bool verbose_mode = false;
  bool override_errors = false;
  double SCK_frequency = 1.e6;

  // display help
  if (argc==1) {
    cout << "Usage: " << argv[0] << " [-A=hex_addr] [-T=fn] [-J] [-C] [-D] [-V] [-O] filename.svf" << endl;
    cout << "     -A=hex_addr     set base VME address of board (default=" << hex << VMEbase << dec << ")" << endl;
    cout << "     -T=fn           set address table file name (default=" << addrtable_fn << ")" << endl;
    cout << "                       the file contains the base address of the ScanPSC/DTTF JTAG Controller as item \"PSC100_BASE\"" << endl;
    cout << "     -J              use (DTTF VHDL JTAG controller instead of ScanPSC100)" << endl;
    cout << "     -C              check only. Only read svf file and check syntax, no hardware access." << endl;
    cout << "     -D              debug only. No hardware access. This will use the dummy bus adapter." << endl;
    cout << "     -V              verbose. display lots of debug output." << endl;
    cout << "     -O              override errors. Continue upon compare error. Only applicable in debug mode." << endl;
    cout << endl;
    exit(0);
  }

  // parse options
  for (int i=1; i<argc;i++) {
    if (argv[i][0] == '-') {
      switch ( toupper( argv[i][1] ) ) {
      case 'A' : 
	if (argv[i][2] == '=') {
	  // quick conversion to hex  
	  argv[i][0] = ' ';
	  argv[i][1] = '0';
	  argv[i][2] = 'x';
	  std::istringstream iss( argv[i] );
	  bool success = ( iss >> std::hex >> VMEbase );
	  if (!success) { cout << "error parsing vme base address" << endl; exit(-1);}
	}
	else { cout << "error parsing vme base address" << endl; exit(-1);}
	break;
      case 'T' :
	if (argv[i][2] == '=') {
	  addrtable_fn = &(argv[i][3]);
	}
	else { cout << "error parsing address table file name" << endl; exit(-1);}
	break;
      case 'J' : use_dttf_jctrl = true; SCK_frequency = 10.e6; break;
      case 'C' : check_only = true; break;
      case 'D' : debug_mode = true; break;
      case 'V' : verbose_mode = true; break;
      case 'O' : override_errors = true; break;
      }
    }
  }

  string svf_filename = argv[argc-1];

  // display options
  cout << "The following settings will be used:" << endl << endl;
  cout << "  Bus Adapter:            " << (debug_mode?"DUMMY":"SBS620(BIT3)") << endl;
  cout << "  Base address:           " << hex << VMEbase << dec << endl;
  cout << "  Address table filename: " << addrtable_fn << endl;
  cout << "  JTAG Controller type:   " << (use_dttf_jctrl?"DTTF-VHDL":"SCANPSC100") 
       << " system clock frequency " << SCK_frequency/1.e6 << " MHz" << endl;
  cout << "  Check only:             " << (check_only?"YES":"NO") << endl;
  cout << "  Debug only:             " << (debug_mode?"YES":"NO") << endl;
  cout << "  Verbose:                " << (verbose_mode?"YES":"NO") << endl;
  cout << "  Override Errors:        " << (debug_mode?(override_errors?"YES":"NO"):"ONLY APPLICABLE IN DEBUG MODE") << endl;
  cout << endl;
  cout << "  SVF File: " << svf_filename << endl;
  cout << endl;
  cout << endl;

  try {
    StopWatch sw(0);

    /// Load the SVF file
    cout << "loading SVF file " << svf_filename << endl;
    JTAGSVFSequencer seq;
    sw.start();
    seq.loadSVFFile(svf_filename.c_str());
    sw.stop();

    // (an error will cause an exception and terminate the program)
    cout << "SVF file loaded into memory without problems. (time elapsed = " << (double)sw.read()/1.e6 << " sec)" << endl;
    cout << endl;

    if (check_only) {
      cout << "check complete. bye." << endl;
      exit(0);
    }

    // warn the user
    if (!debug_mode) {
      cout << "Hardware will now be accessed with the above parameters. " << endl;
      cout << " - Please check the parameters carefully. " << endl;
      cout << " - Make sure that the above frequency is not below the actual frequency used in hardware." << endl;
      string answer;
      do {
	cout << endl << "Are you sure to continue (yes/no)? ";
	cin >> answer;
	if (answer=="no") exit(0);
      } while (answer!="yes");
    
    }
  
    VMEAddressTableASCIIReader addressTableReader( addrtable_fn.c_str() );
    VMEAddressTable addressTable("Outcard address table", addressTableReader);

    // dynamically select bus adapter
    VMEBusAdapterInterface *busAdapter=0;
    if (debug_mode) busAdapter = new  VMEDummyBusAdapter(VMEDummyBusAdapter::VERBOSE_OFF);
    else {
      busAdapter = new SBS620x86LinuxBusAdapter(0);
    }

    {
      VMEDevice outcard(addressTable, *busAdapter, VMEbase);

      // create a HAL adapter for a ScanPSC type JTAG Controller
      HALScanPSC100Adapter scanpsc_adapter (outcard, "PSC100_BASE"); 

      // dynamically select JTAG controller type
      ScanPSC100JTAGController *jtag_controller=0;
      if (use_dttf_jctrl) jtag_controller = new DTTFJTAGController ( scanpsc_adapter, SCK_frequency );
      else 
	jtag_controller = new ScanPSC100JTAGController ( scanpsc_adapter, SCK_frequency );

      if (debug_mode) jtag_controller->setDebugFlag(5);
      else if (verbose_mode) jtag_controller->setDebugFlag(4);

      {
	// open JTAG Chain for chain 0 on the controller
	JTAGChain jtag_chain( *jtag_controller, 0 );
      
	if (debug_mode && override_errors) 
	  seq.setOverrideError(true);

	// run it
	bool status = seq.execute( jtag_chain );
	if (status)
	  cout << "successfully played SVF file." << endl;
	else
	  cout << "error playing SVF file." << endl;

      }
      delete jtag_controller;
    }
    delete busAdapter;

  }
  catch (exception &e) {
    cout << "Unknown Exception caught: " << e.what() << endl;
    return 1;
  }
  return 0;
}

