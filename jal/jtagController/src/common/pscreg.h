/******************************************************************************
*******************************************************************************

  Copyright 1996 National Semiconductor Corporation

  No warranties expressed or implied.

  Note: National Semiconductor reserves the right to modify this file without notice.
            For the latest revision, contact the Scan Applications technical support line
            at (800) 341-0392 (ext 4500), Monday - Friday  from 9am to 5pm EST. 

  Filename: pscreg.h

  Description: Header file for pscdrv.c (the National SCANPSC110 Embedded
	       Boundary Scan Controller device driver). Contains SCANPSC100
                     register definitions.

  Revision History:

    Revision   Date     Comments
    -----------------------------------------------------------------------
    1.00       01May95  Initial Revision  
    1.01         Sep95  PSC_LSA changed to BSC_LSA to make more generic.
    1.02         Oct95  QA/Clean-up.

                                                      SS $Revision: 1.1 $
*******************************************************************************
******************************************************************************/
#ifndef _PSCREG_H
#define _PSCREG_H

/* SCANPSC100 register offsets */
#define PSC_WRITE_TDO   0x00  /* Write only */
#define PSC_READ_CNT_1  0x00  /* Read only */
#define PSC_WRITE_SSC   0x01  /* Write only */
#define PSC_READ_TDI    0x01  /* Read only */
#define PSC_WRITE_TMS0  0x02  /* Write only */
#define PSC_READ_CNT_2  0x02  /* Read only */
#define PSC_WRITE_TMS1  0x03  /* Write only */
#define PSC_READ_CNT_3  0x03  /* Read only */
#define PSC_WRITE_CNT   0x04  /* Write only */
#define PSC_READ_CNT_0  0x04  /* Read only */
#define PSC_MODE0       0x05  /* Read/Write */
#define PSC_MODE1       0x06  /* Read/Write */
#define PSC_MODE2       0x07  /* Read/Write */

/* PSC100 Mode Register bit assignments */
/* Mode 0 */
#define TDO_ENABLE       0x80
#define TDI_ENABLE       0x40
#define CNT32_ENABLE     0x20
#define TMS0_ENABLE      0x10
#define TMS1_ENABLE      0x08
#define CNT32_FRZ_ENABLE 0X04
#define TMS_HIGH_ENABLE  0x02
#define LOOP_ARND_ENABLE 0x01
/* Mode 1 */
#define TDO_INT_ENABLE       0x80
#define TDI_INT_ENABLE       0x40
#define CNT32_INT_ENABLE     0x20
#define PRPG_ENABLE          0x10
#define SSC_ENABLE           0x08
#define FRZ_PIN_ENABLE       0x04
#define LOOP_BACK_TDO        0x01
#define LOOP_BACK_TMS0       0x02
#define LOOP_BACK_TMS1       0x03
#define LOOP_BACK_OFF_MASK   0xFC /* AND with Psc_mode1 to turn off loop back */
/* Mode 2 */
#define RESET_PSC            0x02
#define SINGLE_STEP          0x01
/* Mode 2 Write only */
#define SET_CONT_UPDATE      0x08
#define UPDATE_STATUS        0x04
/* Mode 2 Read only */
#define TDO_STATUS           0x80
#define TDI_STATUS           0x40
#define CNT32_STATUS         0x20
#define TMS0_STATUS          0x10
#define TMS1_STATUS          0x08
#define CONT_UPDATE          0x04

#endif
