#include "jal/jtagController/Timer.h"

#include <sys/time.h>
#include <time.h>

#include <iostream>
using namespace std;

uint32_t jal::Timer::_overhead_musec = 0;
uint64_t jal::Timer::_clocks_per_sec = 0;

jal::Timer::Timer() {

}


jal::Timer::~Timer() {
}

void jal::Timer::sleepMicros(uint32_t musec) {

  // busy-wait if pause is shorter than 100ms. Otherwise we would loose too much time with task switches due to many small pauses
  // otherwise busy-wait

  // 2013 measurement on Intel Xeon X3405 (Nehalem)
  // 
  // FEROL (no big effect)
  // time spent in sleep when doing nanosleep always:   158 s when 155 s needed 
  // time spent in sleep when doing busy wait below 100 ms:  155 s when 155s needed
  //
  // FRL (22 s saved)
  // time spent in sleep when doing nanosleep always:   170 s when 146 s needed
  // time spent in sleep when doing busy wait below 100 ms:  148 s when 146 s needed

  if (musec > 100000) {   

    struct timespec tv,tr;
    tv.tv_sec = musec / 1000000;
    tv.tv_nsec = (musec % 1000000) * 1000;

    nanosleep(&tv, &tr);
  }
  else {
    struct timeval before, after, diff;
    int nchange=0, diff_old=0;
    gettimeofday(&before, 0);
    do {
      gettimeofday(&after, 0);
      timersub(&after, &before, &diff);
      if (diff.tv_usec != 0 && diff.tv_usec != diff_old)
	nchange++;
      diff_old=diff.tv_usec;
    }
    while (( (uint32_t) (diff.tv_usec+diff.tv_sec*1000000) <= musec) || nchange < 2);  

    // require the clock to have ticked twice to assure minimum length of pause
  }
}


void jal::Timer::calibrate() {

}
