#include "jal/jtagController/ByteBlasterJTAGController.h"

/// HS, 13.05.2004. this is just an experimental implementation.

#include <unistd.h> 
#include <sys/io.h> 
#include <stdio.h>
#include <assert.h>

#include "fcntl.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "sys/ioctl.h"
#include "linux/ppdev.h"


using namespace std;

#define ALTERAPROG

#ifdef ALTERAPROG

/* output */
#define JTAG_TMS (1 << 1)
#define JTAG_CLK (1 << 0)
#define JTAG_TDI (1 << 6)
#define JTAG_PWR (0)
#define JTAG_TRST (0)
/* input */
#define JTAG_TDO (1<<7)

#endif 


jal::ByteBlasterJTAGController::ByteBlasterJTAGController() :
  _desiredTCKfrequency(1.e6), _debug_flag(0), _perm_bits(0x80), _initialized(false) {
}

jal::ByteBlasterJTAGController::~ByteBlasterJTAGController() {

  if (_initialized) {
    ioctl (_fd, PPRELEASE);
    close(_fd);
  }
}

/** lock the chain. Useful if multiple processes are using the chain concurrently */
void jal::ByteBlasterJTAGController::lock() {
  //TBD
};


void jal::ByteBlasterJTAGController::unlock() {
  //TBD 
};


void jal::ByteBlasterJTAGController::shift(uint32_t num_bits, 
					   vector<uint8_t> const& data_out, 
					   vector<uint8_t> &  data_in,
					   bool doRead, 
					   bool autoTMS) 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  if (!_initialized) init();

  uint32_t num_bytes = (num_bits+7) / 8;

  // grow the vector for response data if necessary
  if (doRead && data_in.capacity() < num_bytes) data_in.resize(num_bytes); 

  for (uint32_t i=0;i<num_bits; i++) {

    uint32_t byte_index = i/8;
    uint8_t bit_index = i%8;
    uint8_t bit_mask = 1 << bit_index;

    uint8_t value = JTAG_PWR;
    if (byte_index < num_bytes &&  (data_out[byte_index] & bit_mask) == bit_mask  )
      value |= JTAG_TDI;
    
    if (autoTMS && i == num_bits-1)
      value |= JTAG_TMS;
    
    writeJTAG(value);
    writeJTAG(value | JTAG_CLK);

    if (doRead) {

      if (bit_index == 0) data_in[byte_index]=0;

      if (readJTAG() & JTAG_TDO)

      data_in[byte_index] |= bit_mask;
    }

  }

}



void jal::ByteBlasterJTAGController::sequenceTMS(uint32_t num_bits, uint32_t tms)   
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  if (!_initialized) init();

  if (num_bits>16) 
    XCEPT_RAISE(jal::OutOfRangeException, "sequenceTMS(): num_bits out of range");

  for (uint32_t i=0;i<num_bits; i++) {
    uint8_t value = JTAG_PWR;
    uint32_t bit_mask = (1<<i);
    value |= (   (tms & bit_mask)  ==  bit_mask  ) ? JTAG_TMS : 0;
    
    writeJTAG(value);
    writeJTAG(value | JTAG_CLK);
  }
}


void jal::ByteBlasterJTAGController::pulseTCK(uint32_t num_tcks, bool tmshigh, jal::PulseStage stage) 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  // currently no support for separating pulseTCK into some pulses and then a pause
  if (stage == jal::PULSESTAGE_PAUSE) return;

  if (!_initialized) init();

  uint8_t value = JTAG_PWR;
  value |= tmshigh ? JTAG_TMS : 0;
  for (uint32_t i=0; i<num_tcks; i++) {
    writeJTAG(value);
    writeJTAG(value | JTAG_CLK);
  }
  writeJTAG(value);
}



void jal::ByteBlasterJTAGController::init() 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  static bool first=true;
  assert(first);
  first=false;
  
  // open the parallel port
  _fd = open ("/dev/parport0", O_RDWR);

  if (_fd==-1) 
    XCEPT_RAISE(jal::HardwareException, "init(): open error");

  // try to get exclusive access to the parallel port
  // this may cause the next command to fail
//   if (ioctl(_fd, PPEXCL)) 
//     throw runtime_error ("jal::ByteBlasterJTAGController::init(): PPCLAIM error");
      
  // claim the parallel port
  if (ioctl(_fd, PPCLAIM)) 
    XCEPT_RAISE(jal::HardwareException, "init(): PPCLAIM error");

#ifdef ALTERAPROG
   // activate autofeed to enable output.
  uint8_t c = 0x02;

  // write to the control register
  if (ioctl(_fd, PPWCONTROL, &c)) 
    XCEPT_RAISE(jal::HardwareException, "init(): PPWCONTROL error");
  
#endif
   
//   int r = ioperm(port, 3, 1)==0;
  
//   if (r!=0) 
//     throw runtime_error ("jal::ByteBlasterJTAGController::init(): ioperm error");

// #ifdef ALTERAPROG
//   // activate autofeed to enable output.
//   outb(2, port + 2);
// #endif

  trst(true);
  trst(false);

  _initialized = true;
}


uint8_t jal::ByteBlasterJTAGController::readJTAG() { 
  //	return ~inb(port+1); 
  uint8_t d;

  // read a byte from the status register
  ioctl (_fd, PPRSTATUS, &d);
  return ~d;
}

void jal::ByteBlasterJTAGController::writeJTAG(uint8_t data) { 

  _port_value = data; 

  uint8_t d = data | _perm_bits;

  // write a byte to the parallel port
  ioctl (_fd, PPWDATA, &d);
}



void jal::ByteBlasterJTAGController::trst(bool state) {
	
  if (!state)
    {
      /* put trst line to 0 (ouput and zero) */
      //OUTB&=~JTAG_TRST;
      _perm_bits|=JTAG_TRST;
      writeJTAG(_port_value);
    }
  else
    {
      /* put trst line high Z (input) */
      _perm_bits&=~JTAG_TRST;
      writeJTAG(_port_value);
    }
}
