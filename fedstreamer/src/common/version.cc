// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2010, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "fedstreamer/version.h"
#include "config/version.h"
#include "xgi/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(fedstreamer)

void fedstreamer::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(xgi);  
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xdata);  
	CHECKDEPENDENCY(xdaq);  
}

std::set<std::string, std::less<std::string> > fedstreamer::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,xgi);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xdaq);
	 
	return dependencies;
}	
	
