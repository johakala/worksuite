/**
*      @file FMMTestBench.cc
*
*            Console App to control tts::FMM, tts::FMMTester and combined tests.
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.14 $
*     $Date: 2007/03/27 07:53:34 $
*
*
**/
#include "tts/fmmtester/FMMTestCrate.hh"
#include "tts/fmmtester/FMMTesterCard.hh"
#include "tts/fmm/FMMDeadTimeMonitor.hh"
#include "tts/fmm/FMMRegisterTest.hh"
#include "tts/fmm/FMMZBTTest.hh"
#include "tts/fmm/FMMFunctionalTest.hh"
#include "tts/fmm/FMMRandomTransitionTest.hh"
#include "tts/fmm/FMMPoissonTransitionGenerator.hh"
#include "tts/fmm/FMMTransitionTest.hh"
#include "tts/fmm/FMMOpenInputsTest.hh"
#include "tts/fmm/FMMFullSelfTest.hh"

#include "tts/fmmtester/FMMExtTransitionTest.hh"
#include "tts/fmmtester/FMMExtSequenceTest.hh"
#include "tts/fmmtester/FMMTesterRAMSTest.hh"
#include "tts/fmmtester/FMMStage2Test.hh"
#include "tts/fmmdbi/FMMDBI.hh"

namespace tts {
  namespace fmmdbi {
    tts::FMMDBI* getOracleDBI(std::string user, std::string pwd, std::string db);
  }
}


#include "tts/fmmtd/FMMTDCard.hh"
#include "tts/fmm/FMMTTResetTest.hh"
#include "tts/ttsbase/TTSState.hh"

#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include <cmath>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>

#include <sys/ioctl.h>




void display_fmm_status( tts::FMMCard* fmm );
void display_fmm_menu( std::vector<tts::FMMCard*> const &fmms, uint32_t active_fmm, tts::FMMTDCard* td );
bool do_fmm_action( std::string const& st, std::vector<tts::FMMCard*> &fmms, uint32_t& active_fmm, tts::FMMTDCard* td);
tts::TTSState readState();

void display_tester_status( tts::FMMTesterCard* fmmt );
void display_tester_menu( std::vector<tts::FMMTesterCard*> const &testers, uint32_t active_tester );
bool do_tester_action( std::string const& st, std::vector<tts::FMMTesterCard*> &testers, uint32_t& active_tester);

void display_combined_menu( );
bool do_combined_action( std::string const& st, 
			 std::vector<tts::FMMCard*> &fmms, uint32_t& active_fmm,
			 std::vector<tts::FMMTesterCard*> &testers, uint32_t& active_tester, tts::FMMTDCard* td);

void run_stage2_test (std::vector<tts::FMMCard*> &fmms, std::vector<tts::FMMTesterCard*> &testers, tts::FMMTDCard* td);

int main() {

  try {

    std::cout << "==== FMMTestBench version $Revision: 1.14 $  started ... " << std::endl << std::endl;
    bool dummy = false;
    tts::FMMTestCrate crate(dummy);
  
    std::cout << std::endl << "==== detected " << crate.numFMMs() << " FMMs in the crate." << std::endl;
    

    //
    // instantiate FMM drivers
    //
    std::vector<tts::FMMCard*> fmms;
    for (uint32_t i=0; i<crate.numFMMs(); i++) {
      fmms.push_back( &crate.getFMM(i) );
      std::cout << "FMM nr " << i << ": "
	   << fmms[i]->getSerialNumber() << std::endl;
    }

    tts::FMMTDCard* td = 0;
    if (crate.hasTD()) {
      td = &crate.getTD();
    }

    //
    // instantiate FMM Tester cards
    //

    std::vector<tts::FMMTesterCard*> testers;
    std::cout << std::endl << "==== detected " << crate.numFMMTesters() << " FMMTesters in the crate." << std::endl;
    for (uint32_t i=0; i<crate.numFMMTesters(); i++) {
      testers.push_back( &crate.getFMMTester(i) );
      std::cout << "FMMTester nr " << i << ": "
	   << testers[i]->getSerialNumber() << std::endl;
    }

    uint32_t active_fmm = 0;
    uint32_t active_tester = 0;
    std::string st;

    //--------------------------------------------------------------------------------
    do {
      if (fmms.size() > 0)
	display_fmm_menu( fmms, active_fmm, td );

      if (testers.size() > 0)
	display_tester_menu( testers, active_tester );

      if (fmms.size()>0 && testers.size()>0) 
	display_combined_menu();

      std::cout << "q ... quit" << std::endl;
      std::cout << std::endl;
      std::cout << "your selection: ";
      std::cin >> st;
      std::cout << std::endl;

      
      bool valid_fmm_cmd = false;
      if (fmms.size()>0) 
	valid_fmm_cmd = do_fmm_action( st, fmms, active_fmm, td);

      bool valid_tester_cmd = false;
      if (testers.size()>0) 
	valid_tester_cmd = do_tester_action( st, testers, active_tester );

      bool valid_combined_cmd = false;
      if (fmms.size()>0 && testers.size()>0) 
	valid_combined_cmd = do_combined_action( st, fmms, active_fmm, testers, active_tester, td );

      if ( (!valid_tester_cmd) && (!valid_fmm_cmd) && !(valid_combined_cmd) && (st!="q"))
	std::cout << "i'm afraid i don't know what you are talking about ... " << std::endl;

    } while (st != "q");
    //--------------------------------------------------------------------------------

    std::cout << std::endl << "==== FMMTestBench done ... " << std::endl;

  }  catch (std::exception &e) {

    std::cout << "*** Exception occurred : " << std::endl;
    std::cout << e.what() << std::endl;

  }

  
return 0;
}

// not very clean, but an easy solution to share test code
// between the FMM and FMMTester
// Using a library was not possible since the generic makefile 
// framework does not create libraries for test code

#include "../../../fmm/src/common/fmm_console_helpers.icc"


void display_tester_status(tts::FMMTesterCard* fmmt) {

  std::vector<tts::TTSState> ostates;
  fmmt->readTestOutputs(ostates);
  std::cout << "Test Out [19:0]= ";
  for (int i=ostates.size()-1 ; i>=0; i--) 
    std::cout << " " << ostates[i];
  std::cout << std::endl;

  std::vector<tts::TTSState> states;
  fmmt->readTestInputs(states);
  std::cout << "Test Inp [0:3]=  " << states << std::endl;

  std::cout << "Mode = " << (fmmt->isRAMMode()?"SEQUENCE":"REGISTER")
	    << "  Sequence = " << (fmmt->sequenceRunning()?"RUNNING":"STANDBY")
	    << "  Num.Seq. = " << fmmt->readNumberOfSequences()
	    << "  Clock.Sel. = " << fmmt->readSequenceClockSelection() 
	    << " (" << 20.e6 / powl(2,fmmt->readSequenceClockSelection()) << " Hz)" <<  std::endl;

}

void display_tester_menu(std::vector<tts::FMMTesterCard*> const &testers, uint32_t active_tester ) {
  std::cout << std::endl;
  std::cout << "--->>> FMMTester " << std::dec << active_tester << "/"
       << testers.size() <<	" <<<---SN=" << testers[active_tester]->getSerialNumber() 
       << "--------------------------------------------" << std::endl;
  display_tester_status(testers[active_tester]);
  std::cout << "--------------------------------------------------------------------------------" << std::endl;
  if (testers.size() > 1) 
    std::cout << "t0  ... change active tester" << std::endl;
  std::cout << "FMMTester ACCESS FUNCTIONS      (using the FMMTesterCard class)" << std::endl;
  std::cout << "tal ... get writelock for tester            tar ... release write lock for tester" << std::endl;
  std::cout << "tan ... disable locking" << std::endl;
  std::cout << "tss ... Toggle Sequence mode/register mode    >>>SEQUENCE MODE FUNCTIONS<<<" << std::endl;
  std::cout << "                                           " << std::endl;
  std::cout << ">>>REGISTER MODE FUNCTIONS<<<               tsg ... generate randoms sequence (only in reg mode)" << std::endl;
  std::cout << "tso ... Set tester output                   tsc ... select sequence clock" << std::endl;
  std::cout << "                                            tsn ... select number of sequences" << std::endl;
  std::cout << "                                            taq ... launch seQuence" << std::endl;
  std::cout << "tra ... rEset all                           taa ... abort sequence" << std::endl;
  std::cout << "tsl ... Toggle LEDs                         tdf ... read input FIFOs" << std::endl;
  std::cout << "ttr ... RAMS random self test               tds ... display sequence entries (only in reg mode)" << std::endl;
}

bool do_tester_action( std::string const& st, std::vector<tts::FMMTesterCard*> &testers, uint32_t& active_tester) {
  //--------------------------------------------------------------------------------
  // FMMTester ACCESS Functions
  //--------------------------------------------------------------------------------

  if (st == "t0") {
    if (testers.size()>1) {
      std::cout << "FMMTesters in the crate:" << std::endl;
      for (uint32_t i=0; i<testers.size(); i++) {
	std::cout << "FMMTester nr " << i << ": "
		  << testers[i]->getSerialNumber() << std::endl;
      }
      uint32_t af =0;  
      std::cout << "Select which FMMTester (0-" << std::dec << testers.size()-1 << "): "; 
      std::cin >> std::dec >> af;
      if (af < testers.size()) active_tester = af;
    }  
  }
  else if (st == "tal") {
    bool success = testers[active_tester]->getWriteLock();

    if (success)
      std::cout << "successfully obtained write lock for this FMMTester";
    else {
      std::cout << "*** could not obtain write lock for this FMMtester. Lock is owned by process PID=" 
	   << std::dec << testers[active_tester]->getWriteLockOwnerPID() << std::endl;
      std::cout<< "Stale locks can be reset using 'daq/tts/ipcutils/test/linux/x86/cpci_lock_util.exe r'.";
    }

  }
  else if (st == "tar") {
    testers[active_tester]->releaseWriteLock();
  }
  else if (st == "tan") {
    testers[active_tester]->disableLocking();
  }
  else if (st == "tss") {
    std::cout << "Enter '1' for sequence mode, '0' for register mode : ";
    bool seq;
    std::cin >> seq;
	  
    testers[active_tester]->toggleRAMMode(seq);
  } 
  else if (st == "tso") {
    int istate=0;
    std::cout << "Number of channel to change (0..19): ";
    std::cin >> std::dec >> istate;
    std::cout << "Simulated State - ";
    tts::TTSState state = readState();
	  
    testers[active_tester]->setTestOutput(istate, state);
  } 
  else if (st == "tsi") {
    std::vector<tts::TTSState> states;
    testers[active_tester]->readTestInputs(states);
    std::cout << "Current tester input states: " << states << std::endl;	  
  } 
  else if (st == "tsl") {
    std::cout << "Enter setting for LEDs 1 to 4 (hex) 0..F: ";
    uint32_t led;
    std::cin >> std::hex >> led;
	  
    testers[active_tester]->setLEDs(led);
  } 
  else if (st == "tra") {
    std::cout << "Resetting all Tester ... " << std::endl;
    testers[active_tester]->reset();
  } 
  else if (st == "tsc") {
    uint32_t clk_sel;
    std::cout << "Enter clock selection for sequence (0=20MHz, 1=10MHz, 2=5MHz, 31=0.009 Hz): ";
    std::cin >> std::dec >> clk_sel;

    testers[active_tester]->selectSequenceClock(clk_sel);
  } 
  else if (st == "tsn") {
    int value;
    std::cout << "Enter Number of sequences (0 to 127, or -1 for indefinte) : ";
    std::cin >> std::dec >> value;

    testers[active_tester]->setNumberOfSequences(value);
  } 
  else if (st == "taq") {
    std::cout << "Launching sequence ... " << std::endl;
    testers[active_tester]->startSequence();
  } 
  else if (st == "taa") {
    std::cout << "Aborting sequence ... " << std::endl;
    testers[active_tester]->stopSequence();
  } 
  else if (st == "tsg") { 
    std::cout << "generating random transition sequence ... " << std::endl;
    tts::FMMPoissonTransitionGenerator tg;
    std::vector<tts::TTSState> states(20, tts::TTSState::READY);
    std::cout << "states before loop: " << states << std::endl; 
    std::vector<std::vector<tts::TTSState> > state_mem;
    for (int i=0; i<100; i++) {
      std::cout << "addr " << i << ": writing " << states << std::endl;
      state_mem.push_back( states );
      testers[active_tester]->writeTestEntry(i, states);

      std::vector<tts::TTSState> verify_states;
      testers[active_tester]->readTestEntry(i, verify_states);
      if (states!=verify_states) {
	std::cout << "verify error at address " << i << std::endl;
	std::cout << "written  : " << states << std::endl;
	std::cout << "read back: " << verify_states << std::endl;
      }



      tg.getNewStates(states);
    }	  
    for (int i=0; i<100; i++) {
      std::vector<tts::TTSState> verify_states;
      testers[active_tester]->readTestEntry(i, verify_states);
      if (state_mem[i]!=verify_states) {
	std::cout << "verify error at address " << i << std::endl;
	std::cout << "written  : " << state_mem[i] << std::endl;
	std::cout << "read back: " << verify_states << std::endl;
      }
    }	  





    // 	    std::vector<TTSState> verify_states;
    // 	    testers[active_tester]->readTestEntry(i, verify_states);
    // 	    if (states!=verify_states) {
    // 	      std::cout << "verify error at address " << i << std::endl;
    // 	      std::cout << "written  : " << states << std::endl;
    // 	      std::cout << "read back: " << verify_states << std::endl;
    // 	    }


  } 
  else if (st == "tds") {
    uint32_t from, to;
    std::cout << "Enter first address in sequence buffer (hex) 0 .. 7FF) :";
    std::cin >> std::hex >> from;
    std::cout << "Enter last address in sequence buffer (hex) 0 .. 7FF) :";
    std::cin >> std::hex >> to;

    for(uint32_t add=from; add!=(to+1) % tts::FMMTesterCard::RAMSsize; add = (add+1) % tts::FMMTesterCard::RAMSsize) {
      std::vector<tts::TTSState> states;
      testers[active_tester]->readTestEntry(add, states);
      std::cout << "addr = " << std::setw(6) << std::hex << add << "   " << states << std::endl;
    }	  	  
  } 
  else if (st == "tdf") { 
    std::cout << "Tester Input FIFO content:" << std::endl;
    std::vector<tts::TTSState> states;
    uint32_t i=0;
    while (testers[active_tester]->readTestInputFIFOs(states) ) {
      std::cout << std::dec << i++ << " from FIFO: " << states << std::endl;
    };
	    
  } 
  else if (st == "ttr") { 
    tts::FMMTesterRAMSTest rt(testers[active_tester]->device());
    rt.run();
  }
  else return false;

  return true;
}


void display_combined_menu( ) {
  std::cout << std::endl;
  std::cout << "FMM TESTS WITH TESTER" << std::endl;
  std::cout << "ctt ... external transition test (using FMMTester in step mode, random config)" << std::endl;
  std::cout << "cts ... external sequence test (using FMMTester in sequence)" << std::endl;
  std::cout << "ctm ... external sequence with multiple sequences" << std::endl;
  std::cout << "ct2 ... FMM Stage 2 Test" << std::endl;
  std::cout << "cal ... Try to lock all Testers, FMMs in the crate and the FMMTD" << std::endl;
  std::cout << "can ... disable locking for all Testers, FMMs in the crate and the FMMTD" << std::endl;
};


bool do_combined_action( std::string const& st, 
			 std::vector<tts::FMMCard*> &fmms, uint32_t& active_fmm,
			 std::vector<tts::FMMTesterCard*> &testers, uint32_t& active_tester, tts::FMMTDCard* td) {
  if (st == "ctt") {
    tts::FMMPoissonTransitionGenerator tg;
    tts::FMMExtTransitionTest tt(*fmms[active_fmm], *testers[active_tester], tg, std::cout, tts::FMMBasicTest::V_INFO);
    tt.run(500000);
  } 
  else if (st == "cts") { 
    tts::FMMPoissonTransitionGenerator tg;
    tts::FMMExtSequenceTest st(*fmms[active_fmm], *testers[active_tester], tg, std::cout, tts::FMMBasicTest::V_INFO);
    const uint32_t nloops =  1;
    const uint32_t clk_sel_max = 10;
    for (int clk_sel=clk_sel_max; clk_sel>=0; clk_sel--) {
      st.setTestParameters(clk_sel);
      st.run(nloops);
    }
  } 
  else if (st == "ctm") { 
    tts::FMMPoissonTransitionGenerator tg;
    tts::FMMExtSequenceTest st(*fmms[active_fmm], *testers[active_tester], tg, std::cout, tts::FMMBasicTest::V_INFO);
    const uint32_t nloops = 1 ;
    const uint32_t clk_sel_max = 10;
    const uint32_t nsequence = 115; // cannot do 127 because of spurious entries 
    for (int clk_sel=clk_sel_max; clk_sel>=0; clk_sel--) {
      st.setTestParameters(clk_sel, nsequence);
      st.run(nloops);
    }
  }
  else if (st == "ct1") { 
    tts::FMMPoissonTransitionGenerator tg;
    tts::FMMExtSequenceTest st(*fmms[active_fmm], *testers[active_tester], tg, std::cout, tts::FMMBasicTest::V_INFO);
    const uint32_t nloops = 1 ;
    const uint32_t clk_sel_max = 10;
    const uint32_t nsequence = 1;
    for (int clk_sel=clk_sel_max; clk_sel>=1; clk_sel--) {
      st.setTestParameters(clk_sel, nsequence);
      st.run(nloops);
    }
  }
  else if (st == "ct2") {
    run_stage2_test(fmms, testers, td);
  }
  else if (st == "can") {
    std::cout << "Disabling locking for all FMMTesters ... " << std::endl;
    for (uint32_t i=0;i<testers.size();i++) {
      testers[i]->disableLocking();
    }
    std::cout << "Disabling locking for all FMMs ... " << std::endl;
    for (uint32_t i=0;i<fmms.size();i++) {
      fmms[i]->disableLocking();
    }
    if (td) td->disableLocking();
  }
  else if (st == "cal") {
    std::cout << "Attempting to lock all FMMTesters ... " << std::endl;
    for (uint32_t i=0;i<testers.size();i++) {
      std::cout << "Tester " << i << ": ";
      if ( testers[i]->haveWriteLock() )
	std::cout << "already have lock" << std::endl;
      else if ( testers[i]->getWriteLock() ) 
	std::cout << "successfully obtained lock" << std::endl;
      else
	std::cout << "could not obtain lock - locked by PID=" << testers[i]->getWriteLockOwnerPID() << std::endl;
    }	

    std::cout << "Attempting to lock all FMMs ... " << std::endl;
    for (uint32_t i=0;i<fmms.size();i++) {
      std::cout << "FMM " << i << ": ";
      if ( fmms[i]->haveWriteLock() )
	std::cout << "already have lock" << std::endl;
      else if ( fmms[i]->getWriteLock() ) 
	std::cout << "successfully obtained lock" << std::endl;
      else
	std::cout << "could not obtain lock - locked by PID=" << fmms[i]->getWriteLockOwnerPID() << std::endl;
    }	

    if (td) {
      std::cout << "Attempting to lock FMMTD:  ";

      if ( td->haveWriteLock() )
	std::cout << "already have lock" << std::endl;
      else if ( td->getWriteLock() ) 
	std::cout << "successfully obtained lock" << std::endl;
      else
	std::cout << "could not obtain lock - locked by PID=" << td->getWriteLockOwnerPID() << std::endl;
    }


  }
  else return false;
  
  return true;
};


void configure_stage2_test(std::vector<tts::FMMCard*> &fmms,
			   std::vector<tts::FMMTesterCard*> &testers,
			   std::vector<std::pair<uint32_t,uint32_t> > &tester_fmm_mapping) {
  
  // display FMMs in the crate

  std::cout << "FMMs in the crate:" << std::endl;
  for (uint32_t i=0; i<fmms.size(); i++) {
    std::string sn = fmms[i]->getSerialNumber();
    std::cout << "Nr " << std::setw(2) << std::dec << i << ": "
	 << "SN=" << sn
	 << "  Xilinx Firmware ID=" << fmms[i]->readFirmwareRev()
	 << "  Altera Firmware ID=" << fmms[i]->readFirmwareRevAltera()
	 << std::endl;
  }

  // display FMMTesters in the crate

  std::cout << std::endl << "FMM Testers in the crate:" << std::endl;
  for (uint32_t i=0; i<testers.size(); i++) {
    std::string sn = testers[i]->getSerialNumber();
    std::cout << "Nr " << std::setw(2) << std::dec << i << ": "
	 << "SN=" << sn
	 << "  Xilinx Firmware ID=" << testers[i]->readFirmwareRev()
	 << "  Altera Firmware ID=" << testers[i]->readFirmwareRevAltera()
	 << std::endl;
  }

  uint32_t nerr = 0;
  do {
    char ch;
    do {
      std::cout << std::endl << "Configure manually or autodetect [warning: autodetect will access all FMMTesters and FMMs for which write locks were obtained] (m/a)? ";
      std::cin >> ch;
    } while (ch!='m' && ch!='a');

    tester_fmm_mapping.clear();
    if (ch=='m') {
      std::cout << "Manual test setup:" << std::endl;
      int itester;
      do {
	std::cout << "Enter tester number to set up new Tester-FMM pair (0 - " << testers.size()-1 << " or -1 to stop) :" ; 
	std::cin >> itester;
	if ( itester>=0 && itester < (int)testers.size() ) {
	  std::cout << "Enter FMM number to test with tester " << itester << " (0 - " << fmms.size()-1 << ") : ";
	  uint32_t ifmm;
	  std::cin >> ifmm;
	  if ( ifmm < fmms.size() ) {
	    std::cout << "Going to test FMM " << ifmm << " with tester " << itester << std::endl; 
	    tester_fmm_mapping.push_back( std::make_pair( (uint32_t) itester, ifmm)  );
	  }
	  else 
	    std::cout << "fmm nr out of range" << std::endl;
	}
      } while (itester!=-1);

    }
    else { // auto detect
      std::cout << "Autodetecting setup" << std::endl;

      for (uint32_t itester = 0; itester < testers.size(); itester++) 
	if (testers[itester]->haveWriteLock()) {
	  std::cout << "  Checking Tester " << itester << " ..." << std::endl;
	  
	  testers[itester]->toggleRAMMode(false);
	  std::vector<bool> connected(fmms.size(), true);
	  
	  for (uint32_t istate=0; istate< 0x10; istate++) {
	    testers[itester]->setTestOutput(0, tts::TTSState(istate) );
	    for (uint32_t ifmm = 0; ifmm < fmms.size(); ifmm++) 
	      if (fmms[ifmm]->haveWriteLock()) {
		fmms[ifmm]->setMask(0x00000);
		if  (fmms[ifmm]->readInputs()[0] != tts::TTSState(istate) )
		  connected[ifmm] = false;
	      }
	      else {
		connected[ifmm] = false;
	      }
	  }

	  uint32_t i_connfmm=0, n_connfmm=0;
	  for (uint32_t ifmm = 0; ifmm < fmms.size(); ifmm++) 
	    if (connected[ifmm])
	      { i_connfmm=ifmm; n_connfmm++;}

	  if (n_connfmm > 1) {
	    std::cout << "Error: tester " << itester << " seems to be connected to more than 1 FMM" << std::endl;
	    exit(-1);
	  }

	  if (n_connfmm == 0) {
	    std::cout << "tester " << itester << " seems to be unconnected" << std::endl;
	  }

	  if (n_connfmm == 1) {
	    std::cout << "Going to test FMM " << i_connfmm << " with tester " << itester << std::endl; 
	    tester_fmm_mapping.push_back( std::make_pair(itester, i_connfmm) );
	  }
	}
    }
 

    // verify the setup

    std::cout << std::endl << "Verifying the test setup ..." << std::endl;
    nerr=0;

    std::vector<std::pair<uint32_t,uint32_t> >::const_iterator it = tester_fmm_mapping.begin();
    for (;it != tester_fmm_mapping.end(); it++) {
    
      testers[(*it).first]->toggleRAMMode(false);
      fmms[(*it).second]->setMask(0x00000);

      //
      // First check tester outputs
      //
   
      for (uint32_t ioutput=0; ioutput<20; ioutput++) {

	std::vector<bool> connected(20, true);

	for (uint32_t istate=0; istate< 0x10; istate++) {
	  testers[(*it).first]->setTestOutput(ioutput, tts::TTSState(istate) );
      
	  for (uint32_t iinput = 0; iinput < 20; iinput++) {
	    if (fmms[(*it).second]->readInputs()[iinput] != tts::TTSState(istate) )
	      connected[iinput] = false;
	  }
	}

	uint32_t i_conninput=0, n_conninput=0;
	for (uint32_t iinput = 0; iinput < 20; iinput++) {
	  if (connected[iinput])
	    { i_conninput=iinput; n_conninput++;};
	}
      
	if (n_conninput > 1) {
	  std::cout << "Error: tester " << (*it).first << " output " << ioutput
	       << " seems to be connected to more than 1 FMM input" << std::endl;
	  nerr++;
	}

	if (n_conninput < 1) {
	  std::cout << "Error: tester " << (*it).first << " output " << ioutput
	       << " seems to be unconnected " << std::endl;
	  nerr++;
	}

	if (n_conninput == 1 && ioutput!=i_conninput) {
	  std::cout << "Error: tester " << (*it).first << " output " << ioutput
	       << " seems to be connected to FMM " << (*it).second << " input " << i_conninput << std::endl;
	  nerr++;
	}

      }

      //
      // then check tester inputs
      //
      fmms[(*it).second]->enableTestOutputs(0xf);

      for (uint32_t ioutput=0; ioutput<4; ioutput++) {

	std::vector<bool> connected(4, true);
	for (uint32_t istate=0; istate< 0x10; istate++) {
	  fmms[(*it).second]->setTestOutputValue(ioutput, tts::TTSState(istate) );
      
	  for (uint32_t iinput = 0; iinput < 4; iinput++) {
	    std::vector<tts::TTSState> inputs;
	    testers[(*it).first]->readTestInputs(inputs);
	    if (inputs[iinput] != tts::TTSState(istate) )
	      connected[iinput] = false;
	  }
	}

	uint32_t i_conninput=0, n_conninput=0;
	for (uint32_t iinput = 0; iinput < 4; iinput++) {
	  if (connected[iinput])
	    { i_conninput=iinput; n_conninput++;};
	}
      
	if (n_conninput > 1) {
	  std::cout << "Error: FMM " << (*it).second << " output " << ioutput
	       << " seems to be connected to more than 1 FMMTester input" << std::endl;
	  nerr++;
	}

	if (n_conninput < 1) {
	  std::cout << "Error: FMM " << (*it).second << " output " << ioutput
	       << " seems to be unconnected " << std::endl;
	  nerr++;
	}

	if (n_conninput == 1 && ioutput!=i_conninput) {
	  std::cout << "Error: FMM " << (*it).second << " output " << ioutput
	       << " seems to be connected to Tester " << (*it).first << " input " << i_conninput << std::endl;
	  nerr++;
	}
      }
    }
    
    if (nerr!=0) {
      std::cout << "Please correct the above errors...." << std::endl;
    }
  }
  while (nerr!=0);
  
  std::cout << "Test setup verified ... " << std::endl;

}

  



void run_stage2_test (std::vector<tts::FMMCard*> &fmms,
		      std::vector<tts::FMMTesterCard*> &testers,
		      tts::FMMTDCard* td) {

  std::vector<std::pair<uint32_t,uint32_t> > tester_fmm_mapping;
  configure_stage2_test(fmms, testers, tester_fmm_mapping);

  if (tester_fmm_mapping.size() == 0) {
    std::cout << "No FMMs connected to Testers found. returning. " << std::endl;
    return;
  }
    

  tts::FMMDBI* dbi = tts::fmmdbi::getOracleDBI("CMS_DAQ_FMM_HARDWARE", "fmmhardware2012", "cmsr");
  bool continuous_db_update = true;

  //
  // display setup and check in DB
  //
  bool all_fmms_indb = true;
  std::vector<std::pair<uint32_t,uint32_t> >::const_iterator it = tester_fmm_mapping.begin();
  for (;it != tester_fmm_mapping.end(); it++) {
    std::string sn = fmms[(*it).second]->getSerialNumber();
    bool indb = dbi->boardDefined(sn);
    std::cout << "  Tester nr " << (*it).first << " <==> FMM Nr " << (*it).second 
	 << " SN=" << sn
	 << " defined in DB = " << indb << std::endl;

    if (!indb) all_fmms_indb = false;
  }

  if (!all_fmms_indb) {
    std::cout << "Error: not all FMMs are defined in the database." << std::endl;
    std::cout << "Do you wish to (c)ontinue without logging the results of the FMMs which are not defined" << std::endl;
    std::cout << "               (i)nsert the missing boards into the DB" << std::endl;
    std::cout << "               (a)bort ?" << std::endl;
    char ch;
    std::cin >> ch;
    if (ch=='i') {
      for (;it != tester_fmm_mapping.end(); it++) {
	std::string sn = fmms[(*it).second]->getSerialNumber();
	if (!dbi->boardDefined(sn)) {
	  std::cout << "Inserting board " << sn << " into database." << std::endl;
	  dbi->insertBoard(sn, "FMM");
	}
      }      
    }
    else if (ch != 'c') return;
  }
  
  if (!td) 
    std::cout << "Warning: no Trigger Distributor card present. Backplane Reset of Time Tag will NOT be tested." << std::endl << std::endl;
  else if (! td->haveWriteLock() )
    std::cout << "Warning: do not have write lock for Trigger Distributor. Backplane Reset of Time Tag will NOT be tested." << std::endl << std::endl;
    
  std::cout << "Do you want to start the FMM Stage2 Test for the above Tester/FMM combinations? (y/n)";
  char ch;
  std::cin >> ch;
  if (ch != 'y') return;

  std::cout << "Registering test(s) in the database ..." << std::endl;
  
  std::vector<std::pair<int, tts::FMMStage2Test*> > tests; 
  

  for (it = tester_fmm_mapping.begin(); it != tester_fmm_mapping.end(); it++) {

    std::string sn = fmms[(*it).second]->getSerialNumber();
    
    int testid = -1;
    if (dbi->boardDefined(sn)) {
      testid = dbi->logTest(fmms[(*it).second]->getSerialNumber(), 
			   getenv("LOGNAME"),
			   getenv("HOSTNAME"));
      dbi->logStage2Details(testid, 
			   fmms[(*it).second]->readFirmwareRevAltera(), 
			   fmms[(*it).second]->readFirmwareRev(), 
			   testers[(*it).first]->getSerialNumber(),
			   testers[(*it).first]->readFirmwareRevAltera(), 
			   testers[(*it).first]->readFirmwareRev() );
    }

    tts::FMMStage2Test* fs2 = new tts::FMMStage2Test(*fmms[(*it).second], *testers[(*it).first], (td && td->haveWriteLock()) ? td : 0 );
    tests.push_back( std::make_pair(testid, fs2) );

    // log results
    std::vector <tts::FMMBasicTestResult> results;
    fs2->getResults(results);
    dbi->logBasicTestResult(testid, results);
  }


  //
  // Start the test
  //
  
  time_t tstart;
  time(&tstart);
  std::cout << "===FMM Stage2 Test  started on " << ctime(&tstart) << std::endl;

  bool done = false;
  do {

    std::vector<std::pair<int, tts::FMMStage2Test*> >::iterator testit = tests.begin();
    for (; testit!=tests.end(); testit++) {
      (*testit).second->run();
      (*testit).second->printStatus();

      if (continuous_db_update && (*testit).first != -1) {
	dbi->updateStage2Details( (*testit).first );

	std::vector <tts::FMMBasicTestResult> results;
	(*testit).second->getResults(results);
	dbi->updateBasicTestResult( (*testit).first, results);
      }

      if (kbhit()) break;
    }
      
    time_t tlap;
    time(&tlap);

    std::cout << "====================== accumulated test time = " << deltat_to_hms(tlap-tstart) 
	 << " <to end the test press ENTER and wait>"<< std::endl;

    if (kbhit()) {
      std::cout << "Do you really want to end the test (yes/no)? ";      
      std::string dummy;
      std::cin >> dummy;
      if (dummy == "yes") 
	done = true;
      else
	std::cout << "Continuing test..." << std::endl;
    }
    
  } while (! done );
  
  //
  // Perform open inputs test
  // 

  std::cout << std::endl;
  std::cout << "One more test remains to be done. This test requires all inputs of the FMM(s) disconnected." << std::endl;
  std::cout << "If you want to perform this test, please disconnect all FMM input cables before continuing." << std::endl;
  std::cout << "Perform the FMMOpenInputsTest (y/n) ?";
  char ch1 ;
  std::cin >> ch1;

  if (ch1 == 'y') {
    
    std::vector<std::pair<int, tts::FMMStage2Test*> >::iterator testit = tests.begin();
    for (; testit!=tests.end(); testit++) {
      tts::FMMOpenInputsTest ot( (*testit).second->card() );
      ot.run();

      // log results
      tts::FMMBasicTestResult result;
      ot.getTestResult(result);
      dbi->logBasicTestResult( (*testit).first, result);
    }

    std::cout << "FMMOpenInputsTest completed. Results logged to database." << std::endl;
  }


  //
  // Finish the test
  //

  time_t tstop;
  time(&tstop);
  std::cout << "===FMM Stage2 Test finished on " << ctime(&tstop) 
       << "  [duration = " << (tstop-tstart) << " sec]" << std::endl;

  std::vector<std::pair<int, tts::FMMStage2Test*> >::iterator testit = tests.begin();
  for (; testit!=tests.end(); testit++) 
    delete (*testit).second;
}
