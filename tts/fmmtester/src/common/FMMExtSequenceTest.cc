/**
*      @file FMMExtSequenceTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.10 $
*     $Date: 2007/03/27 07:53:34 $
*
*
**/
#include "tts/fmmtester/FMMExtSequenceTest.hh"
#include "tts/fmm/FMMLogicEmulator.hh"

#include <iostream>
#include <iomanip>
#include <cmath>
#include <stdlib.h>

std::string tts::FMMExtSequenceTest::getVersion() const {
  return "$Revision: 1.10 $";
}

std::string tts::FMMExtSequenceTest::getParameters() const {
  std::stringstream ss;
  if (_randomcfg)
    ss << "random FMM configurations.";
  else
    ss << _cfg;

  ss << "  clk_sel=" << _clk_sel << " (" << 20.e6 / powl(2,_clk_sel) << " Hz)" 
     << " nsequence=" << _nsequence
     << " inputfifo_delay = " << _inputfifo_delay;

  return ss.str();
}

bool tts::FMMExtSequenceTest::_run(uint32_t nloops) {

  if (_nsequence>127)
    XCEPT_RAISE( xcept::Exception, "FMMExtSequenceTest::runExtSequenceTest(): error: nsequence out of range");

  if ( doLog(V_INFO) )
    _os << "Performing external transition test ..." << std::endl;

  // initialize the  tester
  _tcard.toggleRAMMode(false);
  _tcard.resetAll();
  _tcard.setInputFIFODelay(_inputfifo_delay);
  //
  // Before starting set all states to ready
  //
  _tcard.setTestOutputs( std::vector<TTSState>(20,tts::TTSState::DISCONNECT1) );
  if ( doLog(V_INFO) )
    _os << "FMMTester initialized..." << std::endl;
  
  // initialize the FMM
  _card.resetAll();
  _card.toggleSimuMode(false);
  _card.toggleDMAMode(false);
  _card.enableTestOutputs(0x0);
  if ( doLog(V_INFO) )
    _os << "FMM initialized..." << std::endl;

  const uint32_t nb_word = 1024;

  uint64_t nb_error = 0;
 
  for (uint32_t j=0; j<nloops; j++) {

    if ( doLog(V_INFO) )
      _os << std::endl << "Starting loop " << std::dec << (j+1) << " / " << nloops << " ..." << std::endl;
    //
    // set the FMM configuration
    //
    tts::FMMConfiguration cfg = _cfg;
    if (_randomcfg) {
      cfg = getRandomConfiguration();
      if ( doLog(V_INFO) )
	_os << "Random ";
    }
    if ( doLog(V_INFO) )
      _os << cfg << std::endl;

    _card.setConfig(cfg);
    tts::FMMLogicEmulator emu(cfg);

    if ( doLog(V_INFO) )
      _os << "Simulating " << std::dec << nb_word << " transitions..." << std::flush;
   
    std::vector<std::vector<tts::TTSState> > genstates;
    std::vector<std::vector<tts::TTSState> > transitions;
    _tcard.toggleRAMMode(false); 

    //
    // generate nb_word transitions
    //

    std::vector<TTSState> last_states(20, tts::TTSState::_next);
    std::vector<TTSState> states(20, tts::TTSState::DISCONNECT1);

    for (uint32_t i = 0; i < nb_word; i++) {

      _tcard.writeTestEntry(i, states); 
      genstates.push_back( states );

      if (states != last_states) { 
	transitions.push_back(states); 
	last_states = states;
      }

      // get a random std::vector of states
      _tg.getNewStates(states);
      // force last states to all disconnect so that dead time counters can be ckeched.
      if (i==nb_word-2)
	states = std::vector<TTSState>(20, tts::TTSState::DISCONNECT1);
    }

    //
    // launch the sequence
    //
    
    if ( doLog(V_INFO) )
      _os << "actual number of transitions is " << transitions.size()
	  << "  starting sequence of " << _nsequence << " repetitions" << std::endl;

    //    uint32_t clk_sel = _clk_sel; clk_sel_max - (j % (clk_sel_max+1));
    if ( doLog(V_INFO) )
      _os << "  Clock.Sel. = " << _clk_sel
	  << " (" << 20.e6 / powl(2,_clk_sel) << " Hz)" << std::endl;

    _tcard.toggleRAMMode(true);
    _card.resetHistoryAddress();
    _card.clearHistoryMemory();
    _card.resetTransitionMissCounter();
    _card.resetDeadTimeMonitors();

    _tcard.setNumberOfSequences(_nsequence);
    _tcard.selectSequenceClock(_clk_sel);
    _tcard.resetTFIFO();

    std::vector<TTSState> outstates; // read first outstate before starting sequence
    _tcard.readTestInputs(outstates);

    _tcard.startSequence();
  
    if ( doLog(V_INFO) )
      _os << "waiting for sequence to complete" << std::endl;
   
    // wait for sequnce to complete
    while ( _tcard.sequenceRunning() );

    //
    // check the FMM output in the Tester input FIFOs
    //
    if ( doLog(V_INFO) )
      _os << "Checking FMM output in tester FIFO for first " << nb_word << " transitions." << std::endl;
    
    for (uint32_t i=0; i < nb_word; i++) {
      
      if ( (outstates[0] != emu.getResultA( genstates[i] )) ||
	   (outstates[1] != emu.getResultA( genstates[i] )) ||
	   (outstates[2] != emu.getResultB( genstates[i] )) ||
	   (outstates[3] != emu.getResultB( genstates[i] )) ) {
	if (doLog(V_ERR)) {
	  _os << errPos() << " " << cfg << ";  "
	      << "output is different from simulation at transition " << std::dec << i << " :" << std::endl;
	  _os << "FMM output = " << outstates
	      << "   Emulation ResultA = " << emu.getResultA( genstates[i] ) 
	      << "   Emulation ResultB = " << emu.getResultB( genstates[i] ) << std::endl;
	  _os << "simulated states:" << genstates[i] << std::endl;
	}
	nb_error++;
      }

	
      // 	_os <<	" transition " << std::dec << i << " :" 
      // 	    << "FMM output = " << outstates
      // 	    << "   Emulation ResultA = " << emu.getResultA( *it ) 
      // 	    << "   Emulation ResultB = " << emu.getResultB( *it ) << std::endl;

      if (_tcard.readTestInputFIFOs( outstates ) == false && i!=nb_word-1) {
	if (doLog(V_ERR)) 
	  _os << "error: FIFO already empty at transition " << std::dec << i << std::endl; 
	nb_error++;
      }
    }

    //
    // Check the history memory
    //

    if ( doLog(V_INFO) )
      _os << "Checking "  << " history entries..." << std::flush;

    uint32_t history_address = 0;
    std::vector<TTSState> last_history_states(20, tts::TTSState::DISCONNECT1);
    uint32_t nskip=0;
    uint32_t nspurious=0;
    uint32_t n_trt_bit = 0;

    if (_card.readTransitionMissCounter() != 0) 
      if ( doLog(V_ERR) ) 
	_os << std::endl
	    << errPos() << "  " << cfg << ";  "
	    << "ATTENTION: transition miss has been detected. TRT counter =" 
	    << _card.readTransitionMissCounter() << std::endl;

    for (uint32_t k=0; k<_nsequence && history_address<tts::FMMCard::FMMHistMemSize; k++) {
      for (uint32_t i = 0; i < transitions.size() && history_address<tts::FMMCard::FMMHistMemSize; i++) {
      
	// set masked channels to ready
	for (int l=0; l<20; l++)
	  if (cfg.getMask() & (1<<l)) transitions[i][l]=tts::TTSState::READY;

	// always skip very first transition (since it is already at output before starting)
	if (k==0 && i==0)
	  continue;

	// skip transition if it was not there after taking into account the mask.
	if ( ( i>0 && transitions[i] == transitions[i-1])  ||
	     ( i==0 && k>0 && transitions[transitions.size()-1] == transitions[0]) )
	continue;

	tts::FMMHistoryItem hi;
	std::vector<tts::TTSState> history_states;

	bool skip_this_item = false;
	do {
	  skip_this_item = false;

	  _card.readHistoryItem(history_address, hi);
	  if (hi.getTransitionMissBit()) {
	    if ( doLog(V_ERR) ) 
	      _os << errPos() << "  " << cfg << ";  "
		  << "attn: read history item with TRT miss bit set! transition " << std::dec << i 
		  << ", history address " << std::dec << history_address 
		  << std::endl;
	    n_trt_bit++;
	  }

	  history_states = hi.getInputStates();

	  if ( ( history_states != transitions[i] ) &&
	       isIntermediateState( history_states, last_history_states, transitions[i]) ) {
	    skip_this_item = true;
	    nskip++;
	    if (isSpuriousIntermediateState( history_states, last_history_states, transitions[i]) ) {
	      nspurious++;
	    }
	  } 
	  //	  _os << "incrementing history address: old = " << std::dec << history_address;
	  // history_address = (history_address+1) % FMMCard::FMMHistMemSize;
	  //	  _os << "  new=" << history_address << std::endl;
	  history_address++;
	  last_history_states = history_states;
	}
	while (skip_this_item && history_address < tts::FMMCard::FMMHistMemSize);

	if ( history_states != transitions[i] ) {
	  if ( doLog(V_ERR) ) {
	    _os << errPos() << "  " << cfg << ";  " 
		<< "compare error at transition " << std::dec << i 
		<< ", history address " << std::dec << history_address 
		<< " TRT-miss bit = " << hi.getTransitionMissBit() << ":" << std::endl;
	    _os << "simulated states:" << transitions[i] << std::endl;
	    _os << "history states:  " << history_states << std::endl;;
	  }
	  nb_error++;
	}
      }    
    }
    if ( doLog(V_INFO) ) {
      _os << " Done! last history address was " << std::dec << history_address << "; "
	  << std::dec << nskip << " history items skipped, " 
	  << std::dec << nspurious << " of them were spurious" << std::endl;
      if (n_trt_bit>0)
	_os << "ATTN: History items with TRT bit: " << n_trt_bit << std::endl;
    }
    //
    // check dead-time monitors
    //
    if ( doLog(V_INFO) )
      _os << "Checking dead-time counters ..." << std::endl;
    
    std::vector<uint32_t> dc;

    _card.readDeadTimeCounters(dc);
    
    std::vector<uint32_t> dt;
    computeDeadTime(genstates, dt);
    
    for (int i=0; i<tts::FMMCard::NumCounters; i++) {

      if ( (i%22)==20 || (i%22)==21 ) 
	continue;
 
      // dead time counters for inputs at indices 0 to 19 and 22 to 41

      uint64_t dt_seq = (uint64_t) dt[i] * _nsequence;
      dt_seq *= 2 * (uint64_t) powl(2,_clk_sel);
      
      double err = fabs( (double)dt_seq-(double)dc[i] )  / (double) dt_seq;

      bool masked = cfg.getMask() & (1<<(i%22));

      if (err>0.01 && !masked) {
	if ( doLog(V_ERR) )
	  _os << errPos() << "  " << cfg << ";  " 
	      << "ERROR: dead-times disagree by more than 1 % " << std::endl;     
	nb_error++;
      }
      const char* deadtime_categories[] = { "/BUSY   ", "/WARNING", "/READY  ", "/OOS    ", "/ERROR  " };

      if ( doLog(V_DEBUG) || ( doLog(V_ERR) && err > 0.01 && !masked) ) {
	_os << "ch = " << std::setw(2) << std::dec << i%20 
	    << deadtime_categories[i/22];

	if ( cfg.getMask() & (1<<(i%22)) ) 
	  _os << " MASKED" << std::endl;
	else {
	  _os << "  sequ dead time (ticks) = " << std::setw(10) << dt_seq  
	      << "  hw dead time (ticks) = " << std::setw(10) << dc[i] 
	      << "  err = " << err * 100. << " %" << std::endl;
	}
      }
    }

  }

  _nloops+= nloops;
  _nerrors+= nb_error;

  return (nb_error == 0);
}

void tts::FMMExtSequenceTest::computeDeadTime(std::vector<std::vector<tts::TTSState> > const& transitions,
					 std::vector<uint32_t> & deadtimes) {

  // first 20 entries in deadtimes are times in busy state, next 20 are times in warning.

  deadtimes.resize(tts::FMMCard::NumCounters);
  std::vector<uint32_t>::iterator it=deadtimes.begin();
  for (; it != deadtimes.end(); it++) 
    (*it)=0;
  
  
  std::vector<std::vector<tts::TTSState> >::const_iterator tr_it = transitions.begin();
  for (; tr_it != transitions.end(); tr_it++) {
    for (int i=0; i<20; i++) {
      if ( (*tr_it)[i] == tts::TTSState::BUSY ) 
	deadtimes[i]++;
      if ( (*tr_it)[i] == tts::TTSState::WARNING ) 
	deadtimes[i+22]++;
      if ( (*tr_it)[i] == tts::TTSState::READY ) 
	deadtimes[i+44]++;
      if ( (*tr_it)[i] == tts::TTSState::OUTOFSYNC ) 
	deadtimes[i+66]++;
      if ( (*tr_it)[i] == tts::TTSState::ERROR ) 
	deadtimes[i+88]++;
    }
  }
};


bool tts::FMMExtSequenceTest::isIntermediateState(std::vector<tts::TTSState> const& states,
					     std::vector<tts::TTSState> const& initial_states,
					     std::vector<tts::TTSState> const& final_states) {

  if (states.size()!=initial_states.size() ||
      states.size()!=final_states.size())
    XCEPT_RAISE( xcept::Exception, "FMMExtSequenceTest::isIntermediateState(): state std::vectors must have same size");

  bool is_intermediate = true;

  for (uint32_t i=0; i<states.size(); i++) {

    if (initial_states[i] == final_states[i]) {
      if (states[i]!=initial_states[i])
	is_intermediate = false;
    } 

  }
    
  return is_intermediate;
}


bool tts::FMMExtSequenceTest::isSpuriousIntermediateState(std::vector<tts::TTSState> const& states,
						     std::vector<tts::TTSState> const& initial_states,
						     std::vector<tts::TTSState> const& final_states) {

  if (states.size()!=initial_states.size() ||
      states.size()!=final_states.size())
    XCEPT_RAISE( xcept::Exception, "FMMExtSequenceTest::isIntermediateState(): state std::vectors must have same size");

  bool is_intermediate = true;
  bool is_spurious = false;

  for (uint32_t i=0; i<states.size(); i++) {

    if (initial_states[i] == final_states[i]) {
      if (states[i]!=initial_states[i])
	is_intermediate = false;
    } 
    else {
      if (states[i]!=initial_states[i] &&
 	  states[i]!=final_states[i])
	is_spurious = true;
    }
    
  } 
  
  return is_spurious && is_intermediate;
}


tts::FMMConfiguration tts::FMMExtSequenceTest::getRandomConfiguration() {

  tts::FMMConfiguration cfg;

  cfg.toggleDualMode( rand() > (RAND_MAX/2) );
  cfg.setMask( rand() & 0xfffff );
  cfg.setThreshold20( (uint64_t) rand() * 19 / RAND_MAX);
  cfg.setThreshold10A( (uint64_t) rand() * 9 / RAND_MAX);
  cfg.setThreshold10B( (uint64_t) rand() * 9 / RAND_MAX);

  return cfg;

}

