/**
*      @file FMMTesterRAMSTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.6 $
*     $Date: 2007/03/27 07:53:34 $
*
*
**/
#include "tts/fmmtester/FMMTesterRAMSTest.hh"
#include <stdlib.h>

std::string tts::FMMTesterRAMSTest::getVersion() const {
  return "$Revision: 1.6 $";
}

bool tts::FMMTesterRAMSTest::_run(uint32_t nloops) {

  const uint32_t nb_word = 4096; // 4kwords to test

  HAL::HalVerifyOption verify_flag = HAL::HAL_NO_VERIFY;

  std::cout << std::dec << nloops << " loops of " << nb_word 
       << " random words will be performed...Be patient...:-)" << std::endl;
  std::cout << "Test will last approx. " << nloops / 1000 
       << " minutes..." << std::endl;
  std::cout << "Masking all inputs to avoid memory corruption..." << std::endl;

  const uint32_t mask = 0xfffff;
  uint32_t nb_error = 0;

  for (uint32_t j = 0; j < nloops; j++) {
    uint32_t expected[nb_word];

    for (uint32_t i = 0; i < nb_word; i++) {	//generate random numbers, store them to expected[] and write them to ZBT      
      expected[i] = rand() & mask;
      _fmmt.write("RAMS", expected[i], verify_flag, 4 * i);
    }

    for (uint32_t i = 0; i < nb_word; i++) {	//read stored values and compare them to expected[]
      uint32_t returned;
      _fmmt.read("RAMS", &returned, 4 * i);
      if (returned != expected[i]) {
	std::cout << "error at offset :" << std::hex << i 
		  << " expected :" << std::hex << expected[i] 
		  << " read :" << std::hex << returned << std::endl;
	nb_error++;
      }
    }
    std::cout << "loop " << std::dec << j + 1 << "/" << nloops << " done..." << std::endl;

  }
  std::cout << "RAMS Memory test done ! " << std::dec << nb_error << " errors" << std::endl;
  
  _nloops+=nloops;
  _nerrors+=nb_error;

  return (nb_error == 0);
}
