/**
*      @file FMMExtTransitionTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:34 $
*
*
**/
#include "tts/fmmtester/FMMExtTransitionTest.hh"
#include "tts/fmm/FMMLogicEmulator.hh"
#include <stdlib.h>

std::string tts::FMMExtTransitionTest::getVersion() const {
  return "$Revision: 1.7 $";
}

std::string tts::FMMExtTransitionTest::getParameters() const {
  std::stringstream ss;
  if (_random)
    ss << "random FMM configurations.";
  else
    ss << _cfg;

  return ss.str();
}

bool tts::FMMExtTransitionTest::_run(uint32_t nloops) {

  if ( doLog(V_INFO) )
    _os << "Performing external transition test ..." << std::endl;

  // initialize the  tester
  _tcard.resetAll();
  _tcard.toggleRAMMode(false);
  if ( doLog(V_INFO) )
    _os << "FMMTester initialized..." << std::endl;
  
  // initialize the FMM
  _card.resetAll();
  _card.toggleSimuMode(false);
  _card.toggleDMAMode(false);
  _card.enableTestOutputs(0x0);
  if ( doLog(V_INFO) )
    _os << "FMM initialized..." << std::endl;

  //  const uint32_t nb_word = 131072;
  const uint32_t nb_word = 120000;

  uint64_t nb_error = 0;
 
  for (uint32_t j=0; j < nloops; j++) {

    if ( doLog(V_INFO) )
      _os << "Starting loop " << std::dec << (j+1) << " / " << nloops << " ..." << std::endl;

    //
    // Before starting set all states to ready
    //
    _tcard.setTestOutputs( std::vector<TTSState> (20, tts::TTSState::READY) );

    //
    // set the FMM configuration
    //
    tts::FMMConfiguration cfg = _cfg;
    if (_random) {
      cfg = getRandomConfiguration();
      if ( doLog(V_INFO) )
	_os << "Random ";
    }
    if ( doLog(V_INFO) )
      _os << cfg << std::endl << std::flush;

    _card.setConfig(cfg);
    tts::FMMLogicEmulator emu(cfg);

    // reset history address after changing config
    _card.resetHistoryAddress(); 

    if ( doLog(V_INFO) )
      _os << "Simulating " << std::dec << nb_word << " transitions..." << std::flush;   

    std::vector<std::vector<tts::TTSState> > transitions;

    //
    // generate nb_word transitions
    //

    std::vector<TTSState> last_states(20, tts::TTSState::_next);
    for (uint32_t i = 0; i < nb_word; i++) {

      std::vector<tts::TTSState> states;
      // get a random std::vector of states
      _tg.getNewStates(states);
      
      _tcard.setTestOutputs(states); 

      // remember the states if they caused a transition
      if (states != last_states) { 
	transitions.push_back(states); 
	last_states = states;
      }

      // check the output at the tester
      std::vector <tts::TTSState> outstates;
      _tcard.readTestInputs(outstates);
      tts::TTSState emu_resultA = emu.getResultA(states);

      if (outstates[0] != emu_resultA ||
	  outstates[1] != emu_resultA) {
	if ( doLog(V_ERR) ) {
	  _os << errPos() << " " << cfg << ";  " 
	      << "output is different from simulation at transition " << std::dec << i << " :" << std::endl;
	  _os << "FMM output[0] = " << outstates[0] 
	      << "  FMM output[1] = " << outstates[1] 
	      << "   Emulation = " << emu_resultA << std::endl;

	  _os << "simulated states:" << states << std::endl;
	}
	nb_error++;
      }
      
      // check the output at the tester
      tts::TTSState emu_resultB = emu.getResultB(states);

      if (outstates[2] != emu_resultB ||
	  outstates[3] != emu_resultB) {
	if ( doLog(V_ERR) ) {
	  _os << errPos() << " " << cfg << ";  " 
	      << "output is different from simulation at transition " << std::dec << i << " :" << std::endl;
	  _os << "FMM output[2] = " << outstates[2] 
	      << "  FMM output[3] = " << outstates[3] 
	      << "   Emulation = " << emu_resultB << std::endl;
	  _os << "simulated states:" << states << std::endl;
	}
	nb_error++;
      }
      
      // check the priority encoder input
      uint32_t funcA = _card.readFuncA();
      if (funcA != emu.getFuncA(states)) {
	if ( doLog(V_ERR) ) {
	  _os << errPos() << " " << cfg << ";  "
	      << "priority encoder input is different from simulation at transition " << std::dec << i << " :" << std::endl;
	  _os << "FMM output A = 0x" << std::hex << funcA << "   Emulation A = 0x" << std::hex << emu.getFuncA(states) << std::endl;
	}
	nb_error++;
      }

      // check the priority encoder input
      uint32_t funcB = _card.readFuncB();
      if (funcB != emu.getFuncB(states)) {
	if ( doLog(V_ERR) ) {
	  _os << errPos() << " " << cfg << ";  "
	      << "priority encoder input is different from simulation at transition " << std::dec << i << " :" << std::endl;
	  _os << "FMM output B = 0x" << std::hex << funcB << "   Emulation B = 0x" << std::hex << emu.getFuncB(states) << std::endl;
	}
	nb_error++;
      }
      
      // automatically stop if history memory gets too full
      if (_card.readHistoryAddress() > tts::FMMCard::FMMHistMemSize-20) {
	if ( doLog(V_INFO) ) 
	  _os << std::endl 
	      << "stopping transiaition generation at transition " << i 
	      << " because history memory is getting full." << std::endl;       
	break;
      }

    }

    if ( doLog(V_INFO) )
      _os << " Done! => number of actual transitions is " << std::dec << transitions.size() << std::endl;

    //
    // Check the history memory
    //

    if ( doLog(V_INFO) )
      _os << "Checking history entries..." << std::flush;

    uint32_t history_address = 0;
    tts::FMMHistoryItem lhi;
    std::vector<TTSState> last_history_states(20, tts::TTSState::READY);
    uint32_t nskip=0;
    uint32_t nspurious=0;
    //bool previous_was_spurious=false;
    for (uint32_t i = 0; i < transitions.size(); i++) {

      // set masked channels to ready
      for (int k=0; k<20; k++)
	if (cfg.getMask() & (1<<k)) transitions[i][k]=tts::TTSState::READY;

      // skip transition if it was not there after taking into account the mask.
      if ( (i==0 && transitions[i] == std::vector<TTSState> (20, tts::TTSState::READY) ) ||
	   (i>0 && transitions[i] == transitions[i-1]))
	continue;

      tts::FMMHistoryItem hi;
      std::vector<tts::TTSState> history_states;

      bool skip_this_item = false;
      do {
	skip_this_item = false;

	_card.readHistoryItem(history_address, hi);
	history_states = hi.getInputStates();

// 	if (previous_was_spurious)
// 	  _os << "delta-t of spurious item and this item is: " << hi.getTimestamp() - lhi.getTimestamp() << std::endl;
	//previous_was_spurious = false;

	if ( ( history_states != transitions[i] ) &&
	     isIntermediateState( history_states, last_history_states, transitions[i]) ) {
	  skip_this_item = true;
	  nskip++;
	  if (isSpuriousIntermediateState( history_states, last_history_states, transitions[i]) ) {
	    nspurious++;
	    //previous_was_spurious = true;
	  }
	} 
	history_address = (history_address+1) % tts::FMMCard::FMMHistMemSize;
	last_history_states = history_states;
	lhi = hi;
      }
      while (skip_this_item);

      if ( history_states != transitions[i] ) {
	if ( doLog(V_ERR) ) {
	  _os << errPos() << " " << cfg << ";  "
	      << "compare error at transition " << std::dec << i 
	      << ", history address " << std::dec << history_address << " :" << std::endl;
	  _os << "simulated states:" << transitions[i] << std::endl;
	  _os << "history states:  " << history_states << std::endl;;
	}
	nb_error++;
      }

    }
    
    if ( doLog(V_INFO) )
      _os << " Done! " 
	  << std::dec << nskip << " history items skipped, " 
	  << std::dec << nspurious << " of them were spurious" << std::endl;
  }

  _nloops += nloops;
  _nerrors += nb_error;

  return (nb_error == 0);
}

bool tts::FMMExtTransitionTest::isIntermediateState(std::vector<tts::TTSState> const& states,
					       std::vector<tts::TTSState> const& initial_states,
					       std::vector<tts::TTSState> const& final_states) {

  if (states.size()!=initial_states.size() ||
      states.size()!=final_states.size())
    XCEPT_RAISE( xcept::Exception, "FMMExtTransitionTest::isIntermediateState(): state std::vectors must have same size");

  bool is_intermediate = true;

  for (uint32_t i=0; i<states.size(); i++) {

    if (initial_states[i] == final_states[i]) {
      if (states[i]!=initial_states[i])
	is_intermediate = false;
    } 

  }
    
  return is_intermediate;
}


bool tts::FMMExtTransitionTest::isSpuriousIntermediateState(std::vector<tts::TTSState> const& states,
						       std::vector<tts::TTSState> const& initial_states,
						       std::vector<tts::TTSState> const& final_states) {

  if (states.size()!=initial_states.size() ||
      states.size()!=final_states.size())
    XCEPT_RAISE( xcept::Exception, "FMMExtTransitionTest::isIntermediateState(): state std::vectors must have same size");

  bool is_intermediate = true;
  bool is_spurious = false;

  for (uint32_t i=0; i<states.size(); i++) {

    if (initial_states[i] == final_states[i]) {
      if (states[i]!=initial_states[i])
	is_intermediate = false;
    } 
    else {
      if (states[i]!=initial_states[i] &&
 	  states[i]!=final_states[i])
	is_spurious = true;
    }
    
  } 
  
  return is_spurious && is_intermediate;
}


tts::FMMConfiguration tts::FMMExtTransitionTest::getRandomConfiguration() {

  tts::FMMConfiguration cfg;

  cfg.toggleDualMode( rand() > (RAND_MAX/2) );
  cfg.setMask( rand() & 0xfffff );
  cfg.setThreshold20( (uint64_t) rand() * 19 / RAND_MAX);
  cfg.setThreshold10A( (uint64_t) rand() * 9 / RAND_MAX);
  cfg.setThreshold10B( (uint64_t) rand() * 9 / RAND_MAX);

  return cfg;

}

