/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/03/28 12:06:24 $
 *
 *
 **/
#include "tts/fmmtester/version.h"
#include "tts/fmm/version.h"
#include "tts/fmmdbi/version.h"
#include "tts/fmmtd/version.h"
#include "tts/cpcibase/version.h"
#include "tts/ttsbase/version.h"
#include "tts/ipcutils/version.h"

#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(ttsfmmtester)

void ttsfmmtester::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(ttsfmm);  
	CHECKDEPENDENCY(ttsfmmdbi);  
	CHECKDEPENDENCY(ttsfmmtd);  
	CHECKDEPENDENCY(ttscpcibase);  
	CHECKDEPENDENCY(ttsttsbase);  
	CHECKDEPENDENCY(ttsipcutils);  

	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  

	CHECKDEPENDENCY(toolbox); 
}

std::set<std::string, std::less<std::string> > ttsfmmtester::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,ttsfmmtd);
	ADDDEPENDENCY(dependencies,ttsfmm);
	ADDDEPENDENCY(dependencies,ttsfmmdbi);
	ADDDEPENDENCY(dependencies,ttscpcibase);
	ADDDEPENDENCY(dependencies,ttsttsbase);
	ADDDEPENDENCY(dependencies,ttsipcutils);

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);

	ADDDEPENDENCY(dependencies,toolbox);
	 
	return dependencies;
}	
	
