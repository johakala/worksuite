/**
 *      @file FMMHistoryLogger.cc
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.8 $
 *     $Date: 2007/03/27 07:53:31 $
 *
 *
 **/
#include "tts/fmmcontroller/FMMHistoryLogger.hh"
#include "tts/fmmcontroller/FMMHistoryConsumer.hh"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <unistd.h>

#include "stdlib.h"

tts::FMMHistoryLogger::FMMHistoryLogger( std::string const& logFileBaseName,
					 uint32_t logFileSizeLimitBytes,
					 bool enableLogRotation)
  : _logFileBaseName(logFileBaseName), 
    _logFileSizeLimitBytes(logFileSizeLimitBytes),
    _enableLogRotation(enableLogRotation),
    _filenum(0) {
};

tts::FMMHistoryLogger::~FMMHistoryLogger() {

};


void tts::FMMHistoryLogger::setRunNumber( uint32_t runNumber) {
  _runNumber = runNumber;
}

void tts::FMMHistoryLogger::beforeMonitorStart() {

  removeOldLogFiles();
  _filenum=0;
  std::string fn = makeFileName();
  _of.open(fn.c_str(), std::ofstream::trunc);
  const bool starting = true;
  printLogHeader( starting );
};

void tts::FMMHistoryLogger::afterMonitorStop( uint64_t extendedTimeStamp ) {
  
  time_t now;
  now = time(NULL);
    
  _of << "--------------------------------------------------" << std::endl;
  _of << "- FMM history dump stopped at " << ctime(&now) << std::endl;
  _of << "--------------------------------------------------" << std::endl << std::flush;
  _of.close();
};


void tts::FMMHistoryLogger::processItem( tts::FMMHistoryItem const& hi, uint32_t address ) {

  checkFileSize();
      
  _of << "addr = " << std::setw(6) << std::hex << address
      << " t=" << std::setw(15) << hi.getTimestampString();

  std::vector<tts::TTSState> states = hi.getInputStates();
  for (std::vector<tts::TTSState>::reverse_iterator it1 = states.rbegin(); it1!=states.rend(); it1++)
    _of << " " << (*it1).getShortName();

  if (hi.getTransitionMissBit())
    _of << " <= transition(s) missed before this item";
    
  _of << std::endl << std::flush;
} 

void tts::FMMHistoryLogger::processWarning( std::string const& warn_string ) {

  _of << warn_string << std::endl << std::flush;
}


void tts::FMMHistoryLogger::removeOldLogFiles() {
  
  if (! _enableLogRotation)
    return;

  std::stringstream rm_cmd;
  rm_cmd << "rm -f " << _logFileBaseName << "_run*" << std::dec << (_runNumber % 10) << "_file?" ;

  system( rm_cmd.str().c_str() );
}

std::string tts::FMMHistoryLogger::makeFileName() {
  
  std::stringstream ss;

  ss << _logFileBaseName << "_run" << std::dec << _runNumber << "_file" << std::setfill('0') << std::setw(1) << _filenum;

  return ss.str();
}

void tts::FMMHistoryLogger::printLogHeader( bool starting ) {
     
  time_t now;
  now = time(NULL);

  _of << "-------------------------------------------------------------------------------------------------------------------" << std::endl;
  _of << "- FMM history dump " << (starting?"started":"continued in this file") << " at " << ctime(&now);
  _of << "- runNumber = " << std::dec << _runNumber << std::endl;
  _of << "-------------------------------------------------------------------------------------------------------------------" << std::endl;
  _of << "address       time since start       i19 i18 i17 i16 i15 i14 i13 i12 i11 i10 i9  i8  i7  i6  i5  i4  i3  i2  i1  i0" << std::endl;
  _of << "-------------------------------------------------------------------------------------------------------------------" << std::endl << std::flush;
}

void tts::FMMHistoryLogger::checkFileSize() {

  const uint32_t num_files = 2;

  if ( _of.tellp() > (std::streampos) _logFileSizeLimitBytes ) {
    _filenum = ( _filenum + 1 ) % num_files;
    std::string fn = makeFileName();

    _of << std::endl << "-- log continued in " << fn << std::endl;
    _of.close();
    _of.open(fn.c_str(), std::ofstream::trunc);

    const bool starting = false;
    printLogHeader( starting );
  }
}



