#! /usr/bin/perl

die "Usage: load_db.pl serialnumber revision user host" unless scalar(@ARGV) == 4;

my $sn=       $ARGV[0];
my $revision= $ARGV[1];
my $username= $ARGV[2];
my $host=     $ARGV[3];

my $sqlfile = "/tmp/loadcmd.sql";

open(TMP, ">", $sqlfile) or die "error opening tmp file";

print TMP "INSERT INTO CMS_DAQ_HSAKULIN.FW_LOAD_LOG (fwid, serialnumber, loaddate, login, host) \n";
print TMP "VALUES((SELECT ID FROM CMS_DAQ_HSAKULIN.FIRMWARE \n";
print TMP "       WHERE revision = '${revision}'\n";
print TMP "             AND board_type = ( SELECT TYPE FROM CMS_DAQ_HSAKULIN.BOARD \n";
print TMP "                                WHERE serialnumber='${sn}' ) \n";
print TMP "       ),\n";
print TMP "       \'${sn}\', \n";
print TMP "       SYSDATE,\n"; 
print TMP "       \'${username}\',\n"; 
print TMP "       \'${host}\');\n";
print TMP "COMMIT;\n";


close TMP;

$out = `sqlplus CMS_DAQ_HSAKULIN/cms_rules\@devdb10 < ${sqlfile}`;

if ( $out =~ /1 row created/ ) {
    print "successfully updated DB.\n"; 
}
else {
    print "ERROR updating DB.\n"; 
    print "$out\n";
}



