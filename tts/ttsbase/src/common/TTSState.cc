/**
*      @file TTSState.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:35 $
*
*
**/
#include "tts/ttsbase/TTSState.hh"

#include <sstream>


std::string tts::TTSState::getName() const {

  std::string tmp;

  switch( _val ) {
  case DISCONNECT1 : tmp = "Disconnected"; break;
  case WARNING     : tmp = "Warning"; break;
  case OUTOFSYNC   : tmp = "OutOfSync"; break;
  case BUSY        : tmp = "Busy"; break;
  case READY       : tmp = "Ready"; break;
  case IDLE        : tmp = "Idle"; break;
  case ERROR       : tmp = "Error"; break;
  case INVALID     : tmp = "Invalid"; break;
  case DISCONNECT2 : tmp = "Disconnected"; break;
  default : tmp = _val>0xf? "ImpossibleState-This Should not happen" : "Illegal"; break;
  } 

  return tmp;
}

std::string tts::TTSState::getLongName() const {

  std::string tmp = getName();

  std::ostringstream os;
  os << "(0x" << std::hex << _val << ")";

  tmp += os.str();

  return tmp;
}

std::string tts::TTSState::getShortName() const {

  std::string tmp;

  switch( _val ) {
  case DISCONNECT1 : tmp = "D"; break;
  case WARNING     : tmp = "W"; break;
  case OUTOFSYNC   : tmp = "S"; break;
  case BUSY        : tmp = "B"; break;
  case READY       : tmp = "R"; break;
  case IDLE        : tmp = "i"; break;
  case ERROR       : tmp = "E"; break;
  case INVALID     : tmp = "I"; break;
  case DISCONNECT2 : tmp = "D"; break;
  default : tmp = _val>0xf? "ImpossibleState-This Should not happen" : "I"; break;
  } 

  std::ostringstream os;
  os << "_" << std::hex << _val;

  tmp += os.str();

  return tmp;
};

std::ostream& operator<< (std::ostream& os, const tts::TTSState& state) {
  os << state.getShortName();
  return os;
};

std::ostream& operator<< (std::ostream& os, const std::vector<tts::TTSState> & states) {
  std::vector<tts::TTSState>::const_iterator it = states.begin();
  for (; it != states.end(); it++)
    os << (*it) << " ";

  return os;
};
