/**
*      @file ATTSConsole.cc
*
*            Console App to control ATTS Interface card
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:25 $
*
*
**/
#include "tts/atts/ATTSInterfaceCard.hh"
#include "tts/atts/ATTSCrate.hh"
#include "tts/ttsbase/TTSState.hh"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdint.h>

void display_atts_status( tts::ATTSInterfaceCard* atts );
void display_atts_menu( std::vector<tts::ATTSInterfaceCard*> const &atts, uint32_t active_atts );
bool do_atts_action( std::string const& st, std::vector<tts::ATTSInterfaceCard*> &atts, uint32_t& active_atts);
tts::TTSState readState();

int main() {

  try {

    std::cout << "==== ATTSConsole version $Revision: 1.7 $  started ... " << std::endl << std::endl;
    tts::ATTSCrate* crate = tts::ATTSCrate::getInstance();

    std::vector<tts::ATTSInterfaceCard*> atts;
    for (uint32_t index=0; index < (uint32_t) crate->numATTSInterfaceCards(); ++index) {
      tts::ATTSInterfaceCard* card = & crate->getATTSInterfaceCard(index);
      atts.push_back( card );
      std::cout << "index = " << index << " SN=" << card->getSerialNumber() << "   GeoSlot=" << card->getGeoSlot() << std::endl; 
    }
    
    if (atts.size() == 0) {
      std::cout << "Bye!" << std::endl;
      exit (-1);
    }

    uint32_t active_atts = 0;
    std::string st;

    //--------------------------------------------------------------------------------
    do {
      if (atts.size() > 0)
	display_atts_menu( atts, active_atts );

      std::cout << "q ... quit" << std::endl;
      std::cout << std::endl;
      std::cout << "your selection: ";
      std::cin >> st;
      std::cout << std::endl;

      
      bool valid_atts_cmd = false;
      if (atts.size()>0) 
	valid_atts_cmd = do_atts_action( st, atts, active_atts );

      if ( (!valid_atts_cmd) && (st!="q"))
	std::cout << "i'm afraid i don't know what you are talking about ... " << std::endl;

    } while (st != "q");
    //--------------------------------------------------------------------------------

    std::cout << std::endl << "==== ATTSConsole done ... " << std::endl;

    atts.clear(); // do NOT delete the pointers

  }  catch (std::exception &e) {

    std::cout << "*** Exception occurred : " << std::endl;
    std::cout << e.what() << std::endl;

  }

  
return 0;
}


void display_atts_status(tts::ATTSInterfaceCard* atts) {

  std::cout << ">Simu mode is " << ( atts->isSimuMode() ? "ON " : "OFF" ) << std::endl;

  std::vector<tts::TTSState> tcsStates = atts->readTCSPartitionStates();
  std::cout << "TCS States (inputs)  [11..0] : ";
  for (int i=tcsStates.size()-1 ; i>=0; i--) 
     std::cout << " " << tcsStates[i];
  std::cout << std::endl;

  std::cout << "DAQ States (outputs) [11..0] : ";
  for (int i=11; i>=0; i--) 
     std::cout << " " << atts->readbackDAQPartitionState(i);
  std::cout << std::endl;

  std::cout << "Simulated Inputs     [11..0] : ";
  std::vector<tts::TTSState> simuStates = atts->readbackSimulatedInputStates();
  for (int i=simuStates.size()-1 ; i>=0; i--) 
     std::cout << " " << simuStates[i];
  std::cout << std::endl;
  

}

void display_atts_menu(std::vector<tts::ATTSInterfaceCard*> const &atts, uint32_t active_atts ) {
  std::cout << std::endl;
  std::cout << "--->>> ATTSInterfaceCard " << std::dec << active_atts << "/"
       << atts.size() <<	" <<<---SN=" << atts[active_atts]->getSerialNumber()  
       << "--------------------------------------------" << std::endl;
  display_atts_status(atts[active_atts]);
  std::cout << "--------------------------------------------------------------------------------" << std::endl;
  if (atts.size() > 1) 
    std::cout << "0  ... change active ATTSInterfaceCard" << std::endl;
 
  std::cout << "1 ... write item                                 3 ... print address table" << std::endl;
  std::cout << "2 ... read item                                  7 ... dump all registers" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "aso ... set DAQPartition state (output)" << std::endl;
  std::cout << "ass ... toggle simulation mode" << std::endl;
  std::cout << "asi ... set simulated input" << std::endl;
}

bool do_atts_action( std::string const& st, std::vector<tts::ATTSInterfaceCard*> &atts, uint32_t& active_atts) {
  //--------------------------------------------------------------------------------
  // ATTSester ACCESS Functions
  //--------------------------------------------------------------------------------

  if (st == "0") {
    if (atts.size()>1) {
      std::cout << "ATTS Interfaces in the crate:" << std::endl;
      for (uint32_t i=0; i<atts.size(); i++) {
	std::cout << "ATTS Interface  nr " << i << ": "
		  << atts[i]->getSerialNumber() << std::endl;
      }
      uint32_t af =0;  
      std::cout << "Select which ATTS Interface (0-" << std::dec << atts.size()-1 << "): "; 
      std::cin >> std::dec >> af;
      if (af < atts.size()) active_atts = af;
    }  
  }
  else if (st == "1") {
    std::string item;
    std::cout << "Enter item : ";
    std::cin >> item;

    uint32_t value;
    std::cout << "Enter value (hex) : ";
    std::cin >> std::hex >> value;

    atts[active_atts]->device().write(item, value);
  } 
  else if (st == "2") {
    std::string item;
    std::cout << "Enter item : ";
    std::cin >> item;

    uint32_t value;
    atts[active_atts]->device().read(item, &value);

    std::cout << "result : " << std::hex << std::setfill('0') << std::setw(8) <<
      value << std::endl;
  } 
  else if (st == "3") {
    atts[active_atts]->device().printAddressTable();
  } 
  else if (st == "7") {
    const char* reg_names[] = { "ID", "CNTL", "STAT", "Test_in_ready",
			  "Test_in_busy", "Test_in_synch", "Test_in_overflow",
			  "Input_ready",
			  "Input_busy", "Input_synch", "Input_overflow", 
			  "Output0",
			  "Output1",
			  "Output2",
			  "Output3",
			  "Output4",
			  "Output5",
			  "Output6",
			  "Output7",
			  "Output8",
			  "Output9",
			  "Output10",
			  "Output11" };

    for (uint32_t i=0; i<sizeof(reg_names)/sizeof(char*); i++) {
      uint32_t val;
      atts[active_atts]->device().read( reg_names[i], &val);
      std::cout << reg_names[i] << " value is : " << std::hex << val << std::endl; 
    }
  }
  else if (st == "ass") {
    std::cout << "Enter '1' for simulation mode, '0' for normal operation : ";
    bool simu;
    std::cin >> simu;
	  
    atts[active_atts]->toggleSimuMode(simu);
  } 
  else if (st == "aso") {
    int istate=0;
    std::cout << "Number of channel to change (0..11): ";
    std::cin >> std::dec >> istate;
    std::cout << "State to signal - ";
    tts::TTSState state = readState();
	  
    atts[active_atts]->setDAQPartitionState(istate, state);
  } 
  else if (st == "asi") {
    int istate=0;
    std::cout << "Number of channel to change (0..11): ";
    std::cin >> std::dec >> istate;
    std::cout << "Simulated state to set - ";
    tts::TTSState state = readState();
	  
    atts[active_atts]->setSimulatedInputState(istate, state);
  } 
  else return false;

  return true;
}


tts::TTSState readState() {
  tts::TTSState state = tts::TTSState::INVALID;
  std::string entry;
  std::cout << "R, B, S, W, E, d(=0x0), D(=0xF), I(=0x3) or hex with prefix '0x': ";
  std::cin >> entry;
  if (entry.find("0x")==0) {
    int istate;
    std::istringstream is(entry.substr(2));
    is >> std::hex >> istate;
    state = istate;
  } else {
    switch(entry.c_str()[0]) {
    case 'R' : state = tts::TTSState::READY; break; 
    case 'B' : state = tts::TTSState::BUSY; break; 
    case 'S' : state = tts::TTSState::OUTOFSYNC; break; 
    case 'W' : state = tts::TTSState::WARNING; break; 
    case 'E' : state = tts::TTSState::ERROR; break; 
    case 'd' : state = tts::TTSState::DISCONNECT1; break; 
    case 'D' : state = tts::TTSState::DISCONNECT2; break; 
    case 'I' : state = tts::TTSState::INVALID; break; 
    }
  }
  std::cout << "selected STATE: " << state.getLongName() << std::endl;
  return state;
}
