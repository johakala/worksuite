#include "tts/fmmdbi/FMMOracleDBI.hh"

#include <sstream>
#include <iostream>
#include <stdlib.h>


using namespace oracle::occi;

namespace tts {
  namespace fmmdbi {
    tts::FMMDBI* getOracleDBI(std::string user, std::string pwd, std::string db) {
      return new tts::FMMOracleDBI(user,pwd, db);
    }
  }
}

tts::FMMOracleDBI::FMMOracleDBI(std::string user, std::string pwd, std::string db) {

  _env = Environment::createEnvironment();
  _conn = _env->createConnection( user, pwd, db);
  _stmt = _conn->createStatement(); 

}


tts::FMMOracleDBI::~FMMOracleDBI() {

  _conn->terminateStatement(_stmt); 
  _env->terminateConnection(_conn); 
  Environment::terminateEnvironment(_env); 

}


bool tts::FMMOracleDBI::boardDefined(std::string serialnumber, std::string type) {
  
  bool found = false;

  try {

    _stmt->setSQL("SELECT serialnumber, type FROM board WHERE serialnumber=:1 AND type=:2");
    _stmt->setString(1, serialnumber);
    _stmt->setString(2, type);

    ResultSet* rs = _stmt->executeQuery();

    if (rs->next()) {
      //       std::cout << "Found in db: SN=" << rs->getString(1) 
      // 	   << ", type=" << rs->getString(2) << std::endl;
      found = true;
    }


  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
  }

  return found;

}

void tts::FMMOracleDBI::insertBoard(std::string serialnumber, std::string type) {

  try {

    _stmt->setSQL("INSERT INTO board(serialnumber, type) VALUES (:1, :2)");
    _stmt->setString(1, serialnumber);
    _stmt->setString(2, type);

    _stmt->executeUpdate();
  }
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
  }

}





int tts::FMMOracleDBI::getFirmwareId(std::string board_type, std::string chip_type, std::string revision) {

  int fwid = -1;

  try {
    _stmt->setSQL("SELECT id FROM firmware WHERE board_type=:1 AND chip_type=:2 AND revision=:3");
    _stmt->setString(1, board_type);
    _stmt->setString(2, chip_type);
    _stmt->setString(3, revision);

    ResultSet* rs = _stmt->executeQuery();

    if (rs->next()) {
      fwid = rs->getNumber(1);
    }
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
  }

  return fwid;
}

int tts::FMMOracleDBI::insertLog(std::string login, std::string comment) {
 
  // insert log
  _stmt->setSQL("INSERT INTO log VALUES(log_logid_sq.nextval, sysdate, :1, :2)");
  _stmt->setString(1, login);
  _stmt->setString(2, comment);
  _stmt->executeUpdate();

  // get logid
  
  ResultSet *rs = _stmt->executeQuery("SELECT log_logid_sq.currval FROM dual");
  rs->next(); 
  int logid = rs->getNumber(1);

  return logid;
}

int tts::FMMOracleDBI::insertTest(std::string sn, int logid, int result) {
 
  // insert test
  _stmt->setSQL("INSERT INTO test VALUES(test_testid_sq.nextval, :1, :2, :3)");
  _stmt->setString(1, sn);
  _stmt->setNumber(2, logid);
  _stmt->setNumber(3, result);
  _stmt->executeUpdate();

  // get logid
  
  ResultSet *rs = _stmt->executeQuery("SELECT test_testid_sq.currval FROM dual");
  rs->next(); 
  int testid = rs->getNumber(1);

  return testid;
}


void tts::FMMOracleDBI::logFirmwareLoad(std::string serialnumber, 
				   int fwid,
				   std::string login,
				   std::string host) {
  
  try {
    _stmt->setSQL("INSERT INTO fw_load_log (fwid, serialnumber, loaddate, login, host) VALUES(:1, :2, SYSDATE, :3, :4)");
    _stmt->setNumber(1, fwid);
    _stmt->setString(2, serialnumber);
    _stmt->setString(3, login);
    _stmt->setString(4, host);
    _stmt->executeUpdate();
    
    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }

}



// void FMMOracleDBI::logFirmwareLoad(std::string serialnumber, int fwid) {
  
//   try {
//     int logid = insertLog("automatic fw load.");
//     // insert fw load log
    
//     _stmt->setSQL("INSERT INTO fw_load_log VALUES(:1, :2, :3, :4)");
//     _stmt->setNumber(1, fwid);
//     _stmt->setString(2, serialnumber);
//     _stmt->setNumber(3, logid);
//     _stmt->setString(4, getenv("HOSTNAME"));
//     _stmt->executeUpdate();
    
//     _conn->commit();
//   } 
//   catch (SQLException &e) {
//     std::cout << "SQL exception :" << e.getMessage() << std::endl;
//     std::cout << "rolling back transaction" << std::endl;
//     _conn->rollback();
//   }

// }




// int FMMOracleDBI::logTest(std::string serialnumber, int result) {
  
//   int testid = -1;

//   try {
//     int logid = insertLog("automatic selftest log.");
//     testid = insertTest(serialnumber, logid, result);

//     // log test details

//     // get the test id
//     _conn->commit();
//   } 
//   catch (SQLException &e) {
//     std::cout << "SQL exception :" << e.getMessage() << std::endl;
//     std::cout << "rolling back transaction" << std::endl;
//     _conn->rollback();
//   }

//   return testid;
// }




int tts::FMMOracleDBI::logTest(std::string serialnumber, 
			  std::string login,	      
			  std::string host) {
  
  int testid = -1;

  try {
    // insert test
    _stmt->setSQL("INSERT INTO test (testid, serialnumber, testdate, login, host) VALUES(test_testid_sq.nextval, :1, SYSDATE, :2, :3)");
    _stmt->setString(1, serialnumber);
    _stmt->setString(2, login);
    _stmt->setString(3, host);
    //    _stmt->setNumber(4, result);
    _stmt->executeUpdate();

    // get logid
    
    ResultSet *rs = _stmt->executeQuery("SELECT test_testid_sq.currval FROM dual");
    rs->next(); 
    testid = rs->getNumber(1);

    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }

  return testid;
}



// void FMMOracleDBI::updateTest(int testid, int result) {
  
//   try {
    
//     // insert test
//     _stmt->setSQL("UPDATE test SET result = :1 WHERE testid = :2");
//     _stmt->setNumber(1, result);
//     _stmt->setNumber(2, testid);
//     _stmt->executeUpdate();

//   // get logid

//     // log test details

//     // get the test id
//     _conn->commit();
//   } 
//   catch (SQLException &e) {
//     std::cout << "SQL exception :" << e.getMessage() << std::endl;
//     std::cout << "rolling back transaction" << std::endl;
//     _conn->rollback();
//   }

// }


void tts::FMMOracleDBI::logStage1Details(int testid,
				    double milliamps_3v3,
				    double milliamps_5v) {
  try {

    _stmt->setSQL("INSERT INTO fmm_stage1_details VALUES (:1, :2, :3)");
    _stmt->setNumber(1, testid);
    _stmt->setNumber(2, milliamps_3v3);
    _stmt->setNumber(3, milliamps_5v);
    _stmt->executeUpdate();

    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }
}


void tts::FMMOracleDBI::logSelfTestDetails(int testid,
				      std::string fwid_altera,
				      std::string fwid_xilinx) {
  
  try {

    _stmt->setSQL("INSERT INTO fmm_selftest_details(testid, fwid_altera, fwid_xilinx, finish_time) VALUES (:1, :2, :3, sysdate)");
    _stmt->setNumber(1, testid);
    _stmt->setString(2, fwid_altera);
    _stmt->setString(3, fwid_xilinx);
    _stmt->executeUpdate();

    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }
}

void tts::FMMOracleDBI::updateSelfTestDetails(int testid) {
  
  try {

    _stmt->setSQL("UPDATE fmm_selftest_details SET finish_time = sysdate WHERE testid = :1");
    _stmt->setNumber(1, testid);
    _stmt->executeUpdate();

    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }
}

void tts::FMMOracleDBI::logStage2Details(int testid,
				    std::string fwrev_altera,
				    std::string fwrev_xilinx,
				    std::string tester_sn,
				    std::string tester_fwrev_altera,
				    std::string tester_fwrev_xilinx) {
  
  try {

    _stmt->setSQL("INSERT INTO fmm_stage2_details VALUES (:1, :2, :3, sysdate, :4, :5, :6)");
    _stmt->setNumber(1, testid);
    _stmt->setString(2, fwrev_altera);
    _stmt->setString(3, fwrev_xilinx);
    _stmt->setString(4, tester_sn);
    _stmt->setString(5, tester_fwrev_altera);
    _stmt->setString(6, tester_fwrev_xilinx);
    _stmt->executeUpdate();

    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }
}

void tts::FMMOracleDBI::updateStage2Details(int testid) {
  
  try {

    _stmt->setSQL("UPDATE fmm_stage2_details SET finish_time = sysdate WHERE testid = :1");
    _stmt->setNumber(1, testid);
    _stmt->executeUpdate();

    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }
}


void tts::FMMOracleDBI::logBasicTestResult(int testid, tts::FMMBasicTestResult const& result) {
  
  try {

    _stmt->setSQL("INSERT INTO fmm_basictest_result VALUES (:1, :2, :3, :4, :5, :6, :7, :8)");
    _stmt->setNumber(1, testid);
    _stmt->setString(2, result.testname);
    _stmt->setString(3, result.testversion);
    _stmt->setNumber(4, (uint32_t) result.nloops);
    _stmt->setNumber(5, (uint32_t) result.nerrors);
    std::stringstream ss;
    ss << std::dec << result.nusecs;
    _stmt->setString(6, ss.str());
    _stmt->setString(7, result.errlog);
    _stmt->setString(8, result.testparameters);
    _stmt->executeUpdate();

    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }

}

void tts::FMMOracleDBI::updateBasicTestResult(int testid, tts::FMMBasicTestResult const& result) {
  
  try {
    
    _stmt->setSQL("UPDATE fmm_basictest_result SET nloops = :1, nerrors = :2, duration_seconds = :3, errlog = :4  WHERE testid = :5 AND testname = :6");
    _stmt->setNumber(1, (uint32_t) result.nloops);
    _stmt->setNumber(2, (uint32_t) result.nerrors);
    std::stringstream ss;
    ss << std::dec << result.nusecs;
    _stmt->setString(3, ss.str());
    _stmt->setString(4, result.errlog);
    _stmt->setNumber(5, testid);
    _stmt->setString(6, result.testname);
    _stmt->executeUpdate();

    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }

}

void tts::FMMOracleDBI::logBasicTestResult(int testid, std::vector<tts::FMMBasicTestResult> const& results) {

  std::vector <tts::FMMBasicTestResult>::const_iterator it = results.begin();
  for (; it != results.end(); it++) {
    logBasicTestResult(testid, (*it) );
  }

}

void tts::FMMOracleDBI::updateBasicTestResult(int testid, std::vector<tts::FMMBasicTestResult> const& results) {

  std::vector <tts::FMMBasicTestResult>::const_iterator it = results.begin();
  for (; it != results.end(); it++) {
    updateBasicTestResult(testid, (*it) );
  }

}

void tts::FMMOracleDBI::insertBoardLog(std::string serialnumber, 
				  std::string login,
				  std::string comment) {
  
  try {

    int logid = insertLog(login, comment);

    _stmt->setSQL("INSERT INTO board_log VALUES (:1, :2)");
    _stmt->setString(1, serialnumber);
    _stmt->setNumber(2, logid);

    _stmt->executeUpdate();

    _conn->commit();
  } 
  catch (SQLException &e) {
    std::cout << "SQL exception :" << e.getMessage() << std::endl;
    std::cout << "rolling back transaction" << std::endl;
    _conn->rollback();
  }
}
