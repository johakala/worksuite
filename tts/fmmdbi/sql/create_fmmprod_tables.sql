CREATE TABLE board (
serialnumber      VARCHAR2(20) PRIMARY KEY,
type              VARCHAR2(10),
CONSTRAINT type_check1 
  CHECK (type IN ('FMM', 'FMMT', 'ATTS'))
);


CREATE TABLE fmmuser (
login             VARCHAR2(40) PRIMARY KEY,
fistname          VARCHAR2(100),
lastname          VARCHAR2(100),
email             VARCHAR2(100)
);

CREATE SEQUENCE log_logid_sq;

CREATE TABLE log (
logid             NUMBER PRIMARY KEY,
logdate           DATE,
login             VARCHAR2(100),
remarks           VARCHAR2(2048)
);

CREATE TABLE board_log (
serialnumber      VARCHAR2(20),
logid             NUMBER,
FOREIGN KEY (serialnumber) REFERENCES board(serialnumber), 
FOREIGN KEY (logid) REFERENCES log(logid) 
);

CREATE TABLE firmware (
id                NUMBER PRIMARY KEY,
board_type        VARCHAR2(20),
chip_type         VARCHAR2(20),
revision          VARCHAR2(9),
filename          VARCHAR2(200),
version_soft      VARCHAR2(200),
logdate           DATE,
login             VARCHAR2(100),
remarks           VARCHAR2(2048),
CONSTRAINT board_type_check1 
  CHECK (board_type IN ('FMM', 'FMMT', 'ATTS')),
CONSTRAINT chip_type_check1 
  CHECK (chip_type IN ('XILINX', 'ALTERA'))
);

CREATE TABLE fw_load_log (
fwid              NUMBER,
serialnumber      VARCHAR2(20),
loaddate          DATE,
login             VARCHAR2(100),
host              VARCHAR2(200),

FOREIGN KEY (serialnumber) REFERENCES board(serialnumber), 
FOREIGN KEY (fwid) REFERENCES firmware(id)
);

CREATE SEQUENCE test_testid_sq;

CREATE TABLE test (
testid            NUMBER PRIMARY KEY,
serialnumber      VARCHAR2(20),
testdate          DATE,
login             VARCHAR2(100),
host              VARCHAR2(200),
FOREIGN KEY (serialnumber) REFERENCES board(serialnumber),
);

CREATE TABLE test_log (
testid            NUMBER,
logid             NUMBER,
FOREIGN KEY (testid) REFERENCES test(testid),
FOREIGN KEY (logid) REFERENCES log(logid) 
);

CREATE TABLE fmm_stage1_details (
testid            NUMBER,
milliamps_3V3     NUMBER,
milliamps_5V      NUMBER,
FOREIGN KEY (testid) REFERENCES test(testid)
);


CREATE TABLE fmm_selftest_details (
testid            NUMBER,

fwid_altera       VARCHAR2(8),
fwid_xilinx       VARCHAR2(9),
finish_time       DATE,

FOREIGN KEY (testid) REFERENCES test(testid)
); 

CREATE TABLE fmm_stage2_details (
testid              NUMBER,

fwid_altera         VARCHAR2(8),
fwid_xilinx         VARCHAR2(9),
finish_time         DATE,
tester_sn           VARCHAR2(20),
tester_fwrev_altera VARCHAR2(9),
tester_fwrev_xilinx VARCHAR2(9),

FOREIGN KEY (testid) REFERENCES test(testid)
); 


CREATE TABLE fmm_basictest_result (
testid            NUMBER,
testname          VARCHAR2(30),
testversion       VARCHAR2(30),
nloops            NUMBER,
nerrors           NUMBER,
duration_seconds  NUMBER,
errlog            VARCHAR2(2048),
testparameters    VARCHAR2(2048),

FOREIGN KEY (testid) REFERENCES test(testid)
);