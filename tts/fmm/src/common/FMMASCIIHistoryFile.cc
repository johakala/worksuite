/**
*      @file FMMASCIIHistoryFile.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.5 $
*     $Date: 2007/03/27 07:53:27 $
*
*
**/
#include "tts/fmm/FMMASCIIHistoryFile.hh"

#include <vector>
#include <iostream>
#include <iomanip>


tts::FMMASCIIHistoryFile::FMMASCIIHistoryFile(std::string const& filename) : _of("fn", std::iostream::ate) {

}

tts::FMMASCIIHistoryFile::~FMMASCIIHistoryFile() {

  close();
}


void tts::FMMASCIIHistoryFile::save(tts::FMMHistoryItem const& hi, bool trmiss) {

  _of // << "addr = " << std::setw(6) << std::hex <<_lastAddress 
      << " t=" << std::setw(15) << hi.getTimestamp();

  _of << " " << trmiss;

  std::vector<tts::TTSState> states = hi.getInputStates();

  std::vector<tts::TTSState>::iterator it1 = states.begin();
  for (; it1!=states.end(); it1++)
    _of << " " << (*it1).getShortName();
    
  _of << std::endl;
}

void tts::FMMASCIIHistoryFile::close() {

  _of.close();

}
