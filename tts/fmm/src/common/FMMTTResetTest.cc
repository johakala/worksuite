/**
*      @file FMMTTResetTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:28 $
*
*
**/
#include "tts/fmm/FMMTTResetTest.hh"

std::string tts::FMMTTResetTest::getVersion() const {
  return "$Revision: 1.7 $";
};


bool tts::FMMTTResetTest::_run(uint32_t nloops) {


  if ( doLog(V_INFO) )
    _os << "===FMMTTResetTest===" << std::endl;

  uint64_t nb_error = 0;

  for (uint32_t j=0; j<nloops; j++) {
    for (uint32_t i=0;i<100;i++) {

      _td.togglePCIMode(true);

      _card.toggleSimuMode(true);
      _card.toggleDMAMode(false);
      _card.setMask(0x0);
      _card.setSimulatedInputStates( std::vector<TTSState> (20, tts::TTSState::READY) );
      
      //
      // test with reset masked
      //

      _card.resetAll(); //resets time tag and transition miss counter and history memory write address

      _card.resetHistoryAddress();

      _card.toggleTimeTagExtResetEnable(false);

      for (uint32_t k=0;k<50;k++) {
	_card.setSimulatedInputState(0, tts::TTSState::BUSY);
	_card.setSimulatedInputState(0, tts::TTSState::READY);
      }
      if (_card.readHistoryAddress() != 100) {
	if (doLog(V_ERR)) {
	  _os << errPos() 
	      << "error: history address does not match after 100 simulated transitions." 
	      << " address = " << _card.readHistoryAddress() << std::endl;
	}
	nb_error++;
      }
      _td.generateResetPulse();
      _card.setSimulatedInputState(0, tts::TTSState::BUSY);
      _card.setSimulatedInputState(0, tts::TTSState::READY);
      
      tts::FMMHistoryItem hi_before;
      _card.readHistoryItem(99, hi_before);
      tts::FMMHistoryItem hi_after;
      _card.readHistoryItem(100, hi_after);
      
      if (hi_before.getTimestamp() > hi_after.getTimestamp()) {
	if (doLog(V_ERR)) {
	  _os << errPos() 
	      << "  error: masking of reset pulse did not work." 
	      << "  timestamp before (masked) reset = " << hi_before.getTimestamp() 
	      << "  timestamp after (masked) reset = " << hi_after.getTimestamp() 
	      << std::endl;
	}
	nb_error++;
      }
      
      //
      // test with reset open
      //

      {
	_card.resetAll(); //resets time tag and transition miss counter and history memory write address

	_card.resetHistoryAddress();

	_card.toggleTimeTagExtResetEnable(true);

	for (uint32_t k=0;k<50;k++) {
	  _card.setSimulatedInputState(0, tts::TTSState::BUSY);
	  _card.setSimulatedInputState(0, tts::TTSState::READY);
	}
	if (_card.readHistoryAddress() != 100) {
	  if (doLog(V_ERR)) {
	    _os << errPos() 
		<< "  error: history address does not match after 100 simulated transitions." 
		<< "  address = " << _card.readHistoryAddress() << std::endl;
	  }
	  nb_error++;
	}
	_td.generateResetPulse();
	//	_card.resetTimeTag();

	_card.setSimulatedInputState(0, tts::TTSState::BUSY);
	_card.setSimulatedInputState(0, tts::TTSState::READY);
      
	tts::FMMHistoryItem hi_before;
	_card.readHistoryItem(99, hi_before);
	tts::FMMHistoryItem hi_after;
	_card.readHistoryItem(100, hi_after);
      
	if (hi_before.getTimestamp() < hi_after.getTimestamp()) {
	  if (doLog(V_ERR)) {
	    _os << errPos() 
		<< "  error: reset pulse did not work." 
		<< "  timestamp before reset = " << hi_before.getTimestamp() 
		<< "  timestamp after reset = " << hi_after.getTimestamp() 
		<< std::endl;
	  }
	  nb_error++;
	}
      }      

    }
  }

  if (doLog(V_INFO) )
    _os << " Done! " << nb_error << " errors." << std::endl;

  _nloops += nloops;
  _nerrors += nb_error;

  return (nb_error == 0);
}

