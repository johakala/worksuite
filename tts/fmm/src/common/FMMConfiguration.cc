/**
*      @file FMMConfiguration.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.5 $
*     $Date: 2007/03/27 07:53:27 $
*
*
**/
#include "tts/fmm/FMMConfiguration.hh"
#include <iomanip>


std::ostream& operator<<(std::ostream& os, tts::FMMConfiguration const& conf) {

  os << "FMMConfiguration: mode=" << ( conf.isDualMode() ? "DUAL  " : "SINGLE" )
     << "  mask=0x" << std::hex << conf.getMask()
     << "  threshold20=" << std::dec << conf.getThreshold20()
     << "  threshold10A=" << std::dec << conf.getThreshold10A()
     << "  threshold10B=" << std::dec << conf.getThreshold10B();

  return os;
}
