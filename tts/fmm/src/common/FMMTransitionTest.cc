/**
*      @file FMMTransitionTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.6 $
*     $Date: 2007/03/27 07:53:28 $
*
*
**/
#include "tts/fmm/FMMTransitionTest.hh"
#include "tts/fmm/FMMLogicEmulator.hh"
#include <stdlib.h>

std::string tts::FMMTransitionTest::getVersion() const {
  return "$Revision: 1.6 $";
};

std::string tts::FMMTransitionTest::getParameters() const {
  std::stringstream ss;
  if (_random)
    ss << "random FMM configurations.";
  else
    ss << _cfg;

  return ss.str();
}

bool tts::FMMTransitionTest::_run(uint32_t nloops) {


  if ( doLog(V_INFO) )
    _os << "===FMMTransitionTest===" << std::endl;

  _card.resetAll();

  _card.toggleSimuMode(true);
  _card.toggleDMAMode(false);

  const uint32_t nb_word = 131072;
  uint64_t nb_error = 0;

  //
  // Before starting set all states to ready
  //
  std::vector<TTSState> last_states(20, tts::TTSState::READY);
  _card.setSimulatedInputStates(last_states);
  _card.resetHistoryAddress();
  uint32_t history_address = 0;

 
  for (uint32_t j=0; j<nloops; j++) {

    if ( doLog(V_INFO) )
      _os << "Starting loop " << std::dec << (j+1) << " / " << nloops << " ..." << std::endl;

    tts::FMMConfiguration cfg;
    if (_random) {
      cfg = getRandomConfiguration();
    if ( doLog(V_INFO) )
      _os << "Random ";
    }
    else 
      cfg = _cfg;

    if ( doLog(V_INFO) )
      _os << cfg << std::endl;

    _card.setConfig(cfg);
    tts::FMMLogicEmulator emu(cfg);

    if ( doLog(V_INFO) )
      _os << "Simulating " << std::dec << nb_word << " transitions...";
   
    std::vector<std::vector<tts::TTSState> > transitions;

    for (uint32_t i = 0; i < nb_word; i++) {

      std::vector<tts::TTSState> states;
      // get a random std::vector of states
      _tg.getNewStates(states);
      
      // set masked inputs to READY (has to be done since mask is not used in simulation mode of FMM)
      for (int k=0;k<20;k++)
	if (cfg.getMask() & (1<<k)) states[k]=tts::TTSState::READY; 

      _card.setSimulatedInputStates(states); 

      // remeber the states if they caused a transition
      if (states != last_states) { 
	transitions.push_back(states); 
	last_states = states;
      }

      // check the output
      tts::TTSState outstate = _card.readResultA();
      if (outstate != emu.getResultA(states)) {
	if ( doLog(V_ERR) ) {
	  _os << errPos() << " cfg=" << cfg << "; " 
	      <<"output is different from simulation at transition " << std::dec << i << " :" << std::endl;
	  _os << "FMM output A = " << outstate << "   Emulation A = " << emu.getResultA(states) << std::endl;
	  _os << "simulated states:" << states << std::endl;
	}
	nb_error++;
      }
      
      // check the output
      outstate = _card.readResultB();
      if (outstate != emu.getResultB(states)) {
	if ( doLog(V_ERR) ) {
	  _os << errPos() << " cfg=" << cfg << "; " 
	      << "output is different from simulation at transition " << std::dec << i << " :" << std::endl;
	  _os << "FMM output B = " << outstate << "   Emulation B = " << emu.getResultB(states) << std::endl;

	  _os << "simulated states:" << states << std::endl;
	}
	nb_error++;
      }
      
      // check the priority encoder input
      uint32_t func = _card.readFuncA();
      if (func != emu.getFuncA(states)) {
	if ( doLog(V_ERR) ) {
	  _os << errPos() << " cfg=" << cfg << "; " 
	      << "priority encoder input is different from simulation at transition " << std::dec << i << " :" << std::endl;
	  _os << "FMM output A = 0x" << std::hex << func << "   Emulation A = 0x" << std::hex << emu.getFuncA(states) << std::endl;
	}
	nb_error++;
      }

      // check the priority encoder input
      func = _card.readFuncB();
      if (func != emu.getFuncB(states)) {
	if ( doLog(V_ERR) ) {
	  _os << errPos() << " cfg=" << cfg << "; " 
	      << "priority encoder input is different from simulation at transition " << std::dec << i << " :" << std::endl;
	  _os << "FMM output B = 0x" << std::hex << func << "   Emulation B = 0x" << std::hex << emu.getFuncB(states) << std::endl;
	}
	nb_error++;
      }

    }

    if ( doLog(V_INFO) ) {
      _os << " Done! => number of actual transitions is " << std::dec << transitions.size() << std::endl;
      _os << "Checking history entries...";
    }

    for (uint32_t i = 0; i < transitions.size(); i++) {

      tts::FMMHistoryItem hi;
      _card.readHistoryItem(history_address, hi);

      if (transitions[i] != hi.getInputStates()) {

	if ( doLog(V_ERR) ) {
	  _os << errPos() << " cfg=" << cfg << "; " 
	      << "compare error at transition " << std::dec << i << " :" << std::endl;
	  _os << "simulated states:" << transitions[i] << std::endl;	
	  _os << "history states:  " << hi.getInputStates() << std::endl;
	}

	nb_error++;
      }

      history_address = (history_address+1) % tts::FMMCard::FMMHistMemSize;
    }

    if (doLog(V_INFO) )
      _os << " Done! " << nb_error << " errors." << std::endl;
  }

  _nloops += nloops;
  _nerrors += nb_error;

  return (nb_error == 0);
}


tts::FMMConfiguration tts::FMMTransitionTest::getRandomConfiguration() {

  tts::FMMConfiguration cfg;

  cfg.toggleDualMode( rand() > (RAND_MAX/2) );
  cfg.setMask( rand() & 0xfffff );
  cfg.setThreshold20( (uint64_t) rand() * 19 / RAND_MAX);
  cfg.setThreshold10A( (uint64_t) rand() * 9 / RAND_MAX);
  cfg.setThreshold10B( (uint64_t) rand() * 9 / RAND_MAX);

  return cfg;

}
