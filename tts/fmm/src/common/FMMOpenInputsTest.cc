/**
*      @file FMMOpenInputsTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:27 $
*
*
**/
#include "tts/fmm/FMMOpenInputsTest.hh"

std::string tts::FMMOpenInputsTest::getVersion() const {
  return "$Revision: 1.7 $";
};


bool tts::FMMOpenInputsTest::_run(uint32_t nloops) {


  if ( doLog(V_INFO) )
    _os << "===FMMOpenInputsTest===" << std::endl;

  uint64_t nb_error = 0;

  for (uint32_t j=0; j<nloops; j++) {
    for (uint32_t i=0;i<100;i++) {

      _card.toggleSimuMode(true);
      _card.toggleDMAMode(false);
      _card.setMask(0x0);

      _card.resetAll(); //resets time tag and transition miss counter
      _card.resetHistoryAddress();

      uint32_t trtmiss = _card.readTransitionMissCounter();
      uint32_t history_address = _card.readHistoryAddress();
      std::vector<tts::TTSState> inputstates = _card.readInputs();

      if (trtmiss != 0) {
	if (doLog(V_ERR))
	  _os << errPos() << " error: trtmiss not zero. trtmiss = " << trtmiss << std::endl; 
	nb_error++;
      }
      if (history_address != 0) {
	if (doLog(V_ERR))
	  _os << errPos() << " error: history address not zero. history_address = " << history_address << std::endl; 
	nb_error++;
      }
      if (inputstates != std::vector<TTSState>(20,tts::TTSState::DISCONNECT2) ) {
	if (doLog(V_ERR))
	  _os << errPos() << " error: input states not disconnected. inputstates = " << inputstates << std::endl;
	nb_error++;
      }
    }
  }

  if (doLog(V_INFO) )
    _os << " Done! " << nb_error << " errors." << std::endl;

  _nloops += nloops;
  _nerrors += nb_error;

  return (nb_error == 0);
}

