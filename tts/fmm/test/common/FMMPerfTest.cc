/**
*      @file FMMPerfTest.cc
*
*            Performance test for PCI memory and config space accesses.
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:29 $
*
*
**/
#include <iostream>

#include "tts/fmm/FMMCrate.hh"
#include "tts/fmm/FMMCard.hh"

#include "hal/PCIDeviceIdentifier.hh"
#include "hal/PCIAddressTable.hh"
#include "hal/linux/StopWatch.hh"
#include <stdint.h>

using namespace HAL;

int main() {

  try {
    tts::FMMCrate crate;
  
    if (crate.numFMMs() < 1) { 
      std::cout << "no FMMs found. bye!" << std::endl;
      return -1;
    }

    // just get the first device ...
    PCIDevice& dev = crate.getFMM(0).device();
    StopWatch sw(1);

    uint32_t n_acc = 500000;

    //--------------------------------------------------------------------------------
    std::cout << "Trying " << n_acc << " memory space reads   ";
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val;
      dev.read("STAT", &val);
    }
    sw.stop();
    std::cout << (sw.read() / 1000000.) << " sec needed. "
	 << (sw.read() / (double) n_acc ) << " usec / call" << std::endl;

    //--------------------------------------------------------------------------------
    std::cout << "Trying " << n_acc << " memory space writes  ";
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val = 0;
      dev.write("CNTL", val);
    }
    sw.stop();
    std::cout << sw.read() / 1000000. << " sec needed. "
	 << sw.read() / (double) n_acc << " usec / call" << std::endl;


    //--------------------------------------------------------------------------------
    std::cout << "Trying " << n_acc << " memory space unmaskedWrites  ";
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val = 0;
      dev.unmaskedWrite("CNTL", val);
    }
    sw.stop();
    std::cout << sw.read() / 1000000. << " sec needed. "
	 << sw.read() / (double) n_acc << " usec / call" << std::endl;


    //--------------------------------------------------------------------------------
    std::cout << "Trying " << n_acc << " config space reads   " ;
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val;
      dev.read("JTAG_TDO", &val);
    }
    sw.stop();
    std::cout << sw.read() / 1000000. << " sec needed. "
	 << sw.read() / (double) n_acc << " usec / call" << std::endl;

    //--------------------------------------------------------------------------------
    std::cout << "Trying " << n_acc << " config space writes  ";
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val = 0;
      dev.write("JTAG_ENABLE", val);
    }
    sw.stop();
    std::cout << sw.read() / 1000000. << " sec needed. "
	 << sw.read() / (double) n_acc << " usec / call" << std::endl;;

    //--------------------------------------------------------------------------------
    std::cout << "Trying " << n_acc << " config space unmaskedWrites  ";
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val = 0;
      dev.unmaskedWrite("JTAG_ENABLE", val);
    }
    sw.stop();
    std::cout << sw.read() / 1000000. << " sec needed. "
	 << sw.read() / (double) n_acc << " usec / call" << std::endl;;


    //--------------------------------------------------------------------------------
    uint32_t stat_addr = dev.getAddressTableInterface().getGeneralHardwareAddress("STAT").getAddress();
    uint32_t stat_bar = dev.getAddressTableInterface().getGeneralHardwareAddress("STAT").getMapId();

    std::cout << "Trying " << n_acc << " memory space reads with memoryRead()   ";
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val;
      dev.memoryRead(stat_addr, stat_bar, &val);
    }
    sw.stop();
    std::cout << (sw.read() / 1000000.) << " sec needed. "
	 << (sw.read() / (double) n_acc ) << " usec / call" << std::endl;


    //--------------------------------------------------------------------------------
    uint32_t cntl_addr = dev.getAddressTableInterface().getGeneralHardwareAddress("CNTL").getAddress();
    uint32_t cntl_bar = dev.getAddressTableInterface().getGeneralHardwareAddress("CNTL").getMapId();

    std::cout << "Trying " << n_acc << " memory space writes with memoryWrite()  ";
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val = 0;
      dev.memoryWrite(cntl_addr, cntl_bar, val);
    }
    sw.stop();
    std::cout << (sw.read() / 1000000.) << " sec needed. "
	 << (sw.read() / (double) n_acc ) << " usec / call" << std::endl;

    
    //--------------------------------------------------------------------------------
    PCIBusAdapterInterface& ba = crate.getPCIBusAdapter();

    PCIDeviceIdentifier *devid;
    std::vector<uint32_t> base;
    // quick and dirty hack to get the deviceidentifierpointer
    ba.findDeviceByVendor(0xecd6, 0xfd4d, 0, (PCIAddressTable&) dev.getAddressTableInterface(), &devid, base);


    uint32_t jtdo_addr = dev.getAddressTableInterface().getGeneralHardwareAddress("JTAG_TDO").getAddress();

    std::cout << "Trying " << n_acc << " config space reads with configRead()  ";
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val = 0;
      ba.configRead(*devid, jtdo_addr, &val);
    }
    sw.stop();
    std::cout << (sw.read() / 1000000.) << " sec needed. "
	 << (sw.read() / (double) n_acc ) << " usec / call" << std::endl;


    //--------------------------------------------------------------------------------
    uint32_t jen_addr = dev.getAddressTableInterface().getGeneralHardwareAddress("JTAG_ENABLE").getAddress();

    std::cout << "Trying " << n_acc << " config space writes with configWrite()  ";
    sw.start();
    for (uint32_t i=0; i<n_acc; i++) {
      uint32_t val = 0;
      ba.configWrite(*devid, jen_addr, val);
    }
    sw.stop();
    std::cout << (sw.read() / 1000000.) << " sec needed. "
	 << (sw.read() / (double) n_acc ) << " usec / call" << std::endl;

  }  catch (std::exception &e) {

    std::cout << "*** Exception occurred : " << std::endl;
    std::cout << e.what() << std::endl;

  }

  return 0;
}
