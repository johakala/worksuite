/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/03/28 12:06:22 $
 *
 *
 **/
#include "tts/cpcibase/version.h"
#include "tts/ipcutils/version.h"

//fixme: add HAL
//fixme: add generic PCI ?

#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(ttscpcibase)

void ttscpcibase::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(ttsipcutils);  
}

std::set<std::string, std::less<std::string> > ttscpcibase::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,ttsipcutils);
	 
	return dependencies;
}	
	
