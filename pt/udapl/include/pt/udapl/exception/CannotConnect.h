// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_ptdapl_exception_CannotConnect_h_
#define _pt_ptdapl_exception_CannotConnect_h_

#include "pt/udapl/exception/Exception.h"

namespace pt
{
	namespace udapl
	{
		namespace exception
		{
			class CannotConnect : public pt::udapl::exception::Exception
			{
				public:
					CannotConnect (std::string name, std::string message, std::string module, int line, std::string function)
						: pt::udapl::exception::Exception(name, message, module, line, function)
					{
					}

					CannotConnect (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
						: pt::udapl::exception::Exception(name, message, module, line, function, e)
					{
					}
			};
		}
	}
}

#endif
