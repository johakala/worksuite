// $Id: Buffer.h,v 1.4 2006/09/20 16:24:36 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udapl_Buffer_h_
#define _pt_udapl_Buffer_h_

// for getting PAGE_SIZE
#include<sys/types.h>
#include<sys/user.h>

#include "toolbox/mem/Buffer.h"
#include "toolbox/mem/Pool.h"
#include "pt/udapl/dat/MemoryRegion.h"

namespace pt
{
	namespace udapl
	{
		class Buffer : public toolbox::mem::Buffer
		{
			public:

				//! Create a physical memory buffer wrapper to externally allocated chunk of memory
				/*! Upon deletion of a HeapBuffer object, the externally allocated
				 memory block will also be deleted. This is responsibility of the Allocator
				 */
				Buffer (pt::udapl::dat::MemoryRegion * mr, toolbox::mem::Pool * pool, size_t size, void* address);

				pt::udapl::dat::MemoryRegion * getMemoryRegion ();

				// this cookies can be set dynamically to determine the context information

				DAT_CONTEXT endpoint_;
				DAT_CONTEXT operation_;
				DAT_LMR_TRIPLET local_iov;

			protected:

				// For the time being, forbid the copy constructor
				Buffer (const Buffer& c)
					: toolbox::mem::Buffer(c)
				{
				}

			public:

				pt::udapl::dat::MemoryRegion * mr_;

		};
	}
}

#endif
