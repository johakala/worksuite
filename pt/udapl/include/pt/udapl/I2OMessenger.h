// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_I2OMessenger_h
#define _pt_udapl_I2OMessenger_h

#include "i2o/Messenger.h"
#include "toolbox/mem/Reference.h"
#include "pt/udapl/Address.h"
#include "pt/udapl/dat/EndPoint.h"
//#include "pt/udapl/PeerTransport.h"

namespace pt
{
	namespace udapl
	{

		class PeerTransport;

		class I2OMessenger : public i2o::Messenger
		{
			public:

				I2OMessenger (pt::udapl::PeerTransport * pt, pt::udapl::dat::EndPoint * ep, pt::Address::Reference local, pt::Address::Reference destination);

				virtual ~I2OMessenger ();

				pt::Address::Reference getLocalAddress ();
				pt::Address::Reference getDestinationAddress ();

				void send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler, void* context) throw (pt::exception::Exception);

			private:

				pt::udapl::PeerTransport * pt_;
				pt::Address::Reference destination_;
				pt::Address::Reference local_;

			public:

				pt::udapl::dat::EndPoint * ep_;
				bool isConnected_;

		};
	}
}

#endif

