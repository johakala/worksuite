// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_dat_InterfaceAdapter_h_
#define _pt_udapl_dat_InterfaceAdapter_h_

#include <string>
#include <list>

#include "pt/udapl/dat/Utils.h"
#include "pt/udapl/exception/Exception.h"
//#include "pt/udapl/dat/MemoryRegion.h"

namespace pt
{
	namespace udapl
	{
		namespace dat
		{

			class EndPoint;
			class MemoryRegion;
			class PublicServicePoint;

			class InterfaceAdapter
			{

					friend class EndPoint;
					friend class MemoryRegion;
					friend class PublicServicePoint;

				public:

					InterfaceAdapter (const std::string & name) throw (pt::udapl::exception::Exception);

					virtual ~InterfaceAdapter ();

					EndPoint* createEndPoint () throw (pt::udapl::exception::Exception);

					void destroyEndPoint (EndPoint* ep) throw (pt::udapl::exception::Exception);

					MemoryRegion* createMemoryRegion (unsigned char* bufffer, DAT_UINT64 size) throw (pt::udapl::exception::Exception);

					void destroyMemoryRegion (MemoryRegion* mr) throw (pt::udapl::exception::Exception);

					pt::udapl::dat::PublicServicePoint* listen (DAT_CONN_QUAL port) throw (pt::udapl::exception::Exception);

					void shutdown (pt::udapl::dat::PublicServicePoint* psp) throw (pt::udapl::exception::Exception);

				public:

					DAT_EVD_HANDLE recv_evd_hdl_;
					DAT_EVD_HANDLE reqt_evd_hdl_;
					DAT_EVD_HANDLE conn_evd_hdl_;
					DAT_EVD_HANDLE creq_evd_hdl_;
					DAT_EVD_HANDLE se_evd_hdl_;
					DAT_CNO_HANDLE cno_handle_;
					std::string name_;
				private:

					DAT_IA_HANDLE ia_handle_;
					DAT_EVD_HANDLE async_evd_hdl_;
					DAT_PZ_HANDLE pz_handle_;

					DAT_IA_ATTR ia_attr_;
					//DAT_PROVIDER_ATTR provider_attr_;

					// keep memory of all regions, removed at DTOR if any
					std::list<MemoryRegion *> memoryRegistry_;

					// keep memory of all endpoint, removed at DTOR if any
					std::list<EndPoint *> endpointRegistry_;

					// keep memory of all endpoint, removed at DTOR if any
					std::list<pt::udapl::dat::PublicServicePoint*> pspRegistry_;

			};
		}
	}
}

#endif
