// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_dat_ErrorHandler_h_
#define _pt_udapl_dat_ErrorHandler_h_

#include <iostream>
#include <string>

#include "xcept/Exception.h"

namespace pt
{
	namespace udapl
	{
		namespace dat
		{
			class ErrorHandler
			{
				public:
					virtual ~ErrorHandler ()
					{
					}

					virtual void handleError (xcept::Exception& ex) = 0;

			};
		}
	}
}

#endif
