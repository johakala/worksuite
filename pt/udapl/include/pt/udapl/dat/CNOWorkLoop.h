// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_dat_CNOWorkLoop_h_
#define _pt_udapl_dat_CNOWorkLoop_h_

#include <iostream>
#include <string>

#include "pt/udapl/dat/Utils.h"
#include "pt/udapl/dat/EventHandler.h"
#include "pt/udapl/dat/ErrorHandler.h"
#include "pt/udapl/exception/Exception.h"
#include "toolbox/lang/Class.h"
#include "toolbox/task/WaitingWorkLoop.h"

namespace pt
{
	namespace udapl
	{
		namespace dat
		{
			class CNOWorkLoop : public toolbox::lang::Class
			{
				public:

					CNOWorkLoop (const std::string & name, DAT_CNO_HANDLE cno_handle, pt::udapl::dat::EventHandler * event_handler, pt::udapl::dat::ErrorHandler * error_handler) throw (pt::udapl::exception::Exception);

					virtual ~CNOWorkLoop ();

					bool process (toolbox::task::WorkLoop* wl);

				protected:

					DAT_CNO_HANDLE cno_handle_;
					toolbox::task::WorkLoop* workLoop_;
					toolbox::task::ActionSignature* process_;
					pt::udapl::dat::EventHandler * event_handler_;
					pt::udapl::dat::ErrorHandler * error_handler_;
					std::string name_;

			};
		}
	}
}

#endif
