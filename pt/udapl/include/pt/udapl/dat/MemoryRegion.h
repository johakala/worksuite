// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_dat_MemoryRegion_h
#define _pt_udapl_dat_MemoryRegion_h

#include <string>

#include "pt/udapl/dat/Utils.h"
#include "pt/udapl/exception/Exception.h"

namespace pt
{
	namespace udapl
	{

		namespace dat
		{

			class InterfaceAdapter;
			class EndPoint;

			class MemoryRegion
			{
					friend class InterfaceAdapter;
					friend class EndPoint;

				public:

					MemoryRegion (pt::udapl::dat::InterfaceAdapter * ia, unsigned char* buffer, DAT_UINT64 size) throw (pt::udapl::exception::Exception);

					virtual ~MemoryRegion ();

					/*
					 DAT_UINT64 GetFullSize() const { return fFullSize; }
					 unsigned char* GetFullBuffer() const { return fFullBuffer; }

					 void SetUsedPart(unsigned char* usestart, DAT_UINT64 usesize);

					 DAT_UINT64 GetSize() const { return fSize; }
					 unsigned char* GetBuffer() const { return fBuffer; }

					 DAT_LMR_TRIPLET* GetIOV() { return &fLmrTriplet; }

					 DAT_LMR_CONTEXT  GetLMRContext() const { return lmr_context; }

					 DAT_RMR_CONTEXT  GetRMRContext() const { return rmr_context; }
					 */

				private:

					DAT_VADDR reg_addr_; /* start of registered area */
					DAT_VLEN reg_size_; /* size of registered area */

					DAT_LMR_HANDLE lmr_handle_; /* local access */
					DAT_LMR_CONTEXT lmr_context_;
					DAT_RMR_CONTEXT rmr_context_;

					pt::udapl::dat::InterfaceAdapter * ia_;
					unsigned char* buffer_;
					DAT_UINT64 size_;

					/*LO NO RDAM for te moment
					 DAT_RMR_HANDLE  rmr_handle;
					 DAT_RMR_CONTEXT rmr_context;

					 bool            enable_rdma_write;
					 bool            enable_rdma_read;
					 */

					/*
					 unsigned char* fFullBuffer;
					 DAT_UINT64  fFullSize;

					 unsigned char* fBuffer;
					 DAT_UINT64  fSize;

					 DAT_LMR_TRIPLET fLmrTriplet;
					 bool fUseExternalBuffer;
					 */
			};
		}
	}
}

#endif
