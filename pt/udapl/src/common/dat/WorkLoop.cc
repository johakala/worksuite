// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/udapl/dat/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include <sstream>

pt::udapl::dat::WorkLoop::WorkLoop (const std::string & name, DAT_EVD_HANDLE evd_handle, pt::udapl::dat::EventHandler * event_handler, pt::udapl::dat::ErrorHandler * error_handler) throw (pt::udapl::exception::Exception)
	: evd_handle_(evd_handle), event_handler_(event_handler), error_handler_(error_handler)
{
	name_ = name;
	process_ = toolbox::task::bind(this, &WorkLoop::process, "process");
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "waiting");

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		XCEPT_RETHROW(pt::udapl::exception::Exception, "Failed to submitjob", e);

	}
	catch (std::exception& se)
	{
		std::stringstream msg;
		msg << "Failed to submit notification to worker thread, caught standard exception '";
		msg << se.what() << "'";
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}
	catch (...)
	{
		XCEPT_RAISE(pt::udapl::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}
}

pt::udapl::dat::WorkLoop::~WorkLoop ()
{
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->cancel();
}

bool pt::udapl::dat::WorkLoop::process (toolbox::task::WorkLoop* wl)
{
	//std::cout << "Activated workloop " << name_ << std::endl;

	DAT_RETURN ret;
	DAT_COUNT nmore;
	DAT_EVENT event;

	//ret = dat_evd_wait(this->evd_handle_,DAT_TIMEOUT_INFINITE, 1, &event, &nmore);
	ret = dat_evd_dequeue(this->evd_handle_, &event);
// if empty, go away
	if ((ret & DAT_QUEUE_EMPTY) != 0) return true;

	/*
	 if ( DAT_GET_TYPE(ret) == DAT_TIMEOUT_EXPIRED)
	 {
	 // keep going
	 return true;
	 }
	 */

	if (ret != DAT_SUCCESS)
	{
		const char *major_msg, *minor_msg;
		dat_strerror(ret, &major_msg, &minor_msg);
		std::stringstream msg;
		msg << "Fatal error waiting for event on " << name_ << " with code:" << major_msg << ":" << minor_msg;
		std::cerr << msg << std::endl;
		XCEPT_DECLARE(pt::udapl::exception::Exception, e, msg.str());
		error_handler_->handleError(e);
		return false;
	}

	// handoff event to listener
	//std::cout << "handle event before" << std::endl;
	event_handler_->handleEvent(event);
	//std::cout << "handle event done" << std::endl;
	return true;
}

