// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/udapl/dat/CNOWorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include <sstream>

pt::udapl::dat::CNOWorkLoop::CNOWorkLoop (const std::string & name, DAT_CNO_HANDLE cno_handle, pt::udapl::dat::EventHandler * event_handler, pt::udapl::dat::ErrorHandler * error_handler) throw (pt::udapl::exception::Exception)
	: cno_handle_(cno_handle), event_handler_(event_handler), error_handler_(error_handler)
{
	name_ = name;
	process_ = toolbox::task::bind(this, &CNOWorkLoop::process, "process");

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "waiting")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "waiting")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		XCEPT_RETHROW(pt::udapl::exception::Exception, "Failed to submitjob", e);

	}
	catch (std::exception& se)
	{
		std::stringstream msg;
		msg << "Failed to submit notification to worker thread, caught standard exception '";
		msg << se.what() << "'";
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}
	catch (...)
	{
		XCEPT_RAISE(pt::udapl::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}
}

pt::udapl::dat::CNOWorkLoop::~CNOWorkLoop ()
{
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "waiting")->cancel();
}

bool pt::udapl::dat::CNOWorkLoop::process (toolbox::task::WorkLoop* wl)
{
	//std::cout << "Activated workloop " << name_ << std::endl;

	DAT_RETURN ret;
	DAT_COUNT nmore;
	DAT_EVENT event;

	DAT_EVD_HANDLE evd;

	//ret = dat_evd_wait(this->evd_handle_, DAT_TIMEOUT_INFINITE, 1, &event, &nmore);
	ret = dat_cno_wait(cno_handle_, DAT_TIMEOUT_INFINITE, &evd);

	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Fatal error waiting for CNO,  with code:" << pt::udapl::dat::errorToString(ret);
		std::cerr << msg << std::endl;
		XCEPT_DECLARE(pt::udapl::exception::Exception, e, msg.str());
		error_handler_->handleError(e);
		return false;
	}

	nmore = 1;

	while (nmore)
	{
		ret = dat_evd_wait(evd, 0, 1, &event, &nmore);
		if (DAT_GET_TYPE(ret) == DAT_TIMEOUT_EXPIRED)
		{
			break;
		}

		if (ret != DAT_SUCCESS)
		{
			std::stringstream msg;
			msg << "Fatal error waiting for event on CNO,  with code:" << pt::udapl::dat::errorToString(ret);
			std::cerr << msg << std::endl;
			XCEPT_DECLARE(pt::udapl::exception::Exception, e, msg.str());
			error_handler_->handleError(e);
			return false;
		}

		/*	if ( evd == m_se_evd )
		 {
		 dbg( core, "returning\n" );
		 return NULL;
		 }
		 */

		//std::cout << "handle event before" << std::endl;
		event_handler_->handleEvent(event);
		//std::cout << "handle event done" << std::endl;
	}

	return true;
}

