// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include "pt/udapl/dat/InterfaceAdapter.h"

#include <linux/in6.h>
#include <linux/in.h>

#include "pt/udapl/dat/MemoryRegion.h"
#include "pt/udapl/dat/EndPoint.h"
#include "pt/udapl/dat/PublicServicePoint.h"

#include "pt/udapl/dat/Utils.h"

#define DEFAULT_QUEUE_LENGTH 8192

//#define DEFAULT_EP_QUEUE_LENGTH 32
//#define DEFAULT_EP_QUEUE_LENGTH 100

pt::udapl::dat::InterfaceAdapter::InterfaceAdapter (const std::string & name) throw (pt::udapl::exception::Exception)
	: name_(name)
{
	ia_handle_ = DAT_HANDLE_NULL;
	async_evd_hdl_ = DAT_HANDLE_NULL;

	pz_handle_ = DAT_HANDLE_NULL;

	recv_evd_hdl_ = DAT_HANDLE_NULL;
	reqt_evd_hdl_ = DAT_HANDLE_NULL;
	conn_evd_hdl_ = DAT_HANDLE_NULL;
	creq_evd_hdl_ = DAT_HANDLE_NULL;

	DAT_RETURN ret = dat_ia_open((char*) name.c_str(), DEFAULT_QUEUE_LENGTH, &async_evd_hdl_, &ia_handle_);

	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to open interface " << name << " with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}

	//create CNO
	ret = dat_cno_create(ia_handle_, DAT_OS_WAIT_PROXY_AGENT_NULL, &cno_handle_);
	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to create CNO " << name << " with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}

	ret = dat_evd_modify_cno(async_evd_hdl_, cno_handle_);
	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to modify CNO " << name << " with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}

	ret = dat_pz_create(ia_handle_, &pz_handle_);
	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to create PZ for IA " << name << " with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}

	// Create 3 event dispatchers - recv, request, connect

	ret = dat_evd_create(ia_handle_, DEFAULT_QUEUE_LENGTH, DAT_HANDLE_NULL, DAT_EVD_DTO_FLAG, &recv_evd_hdl_);

	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to create receive ED for IA " << name << " with code:" << pt::udapl::dat::errorToString(ret);
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}

	ret = dat_evd_create(ia_handle_, DEFAULT_QUEUE_LENGTH, DAT_HANDLE_NULL, (DAT_EVD_FLAGS)(DAT_EVD_DTO_FLAG | DAT_EVD_RMR_BIND_FLAG), &reqt_evd_hdl_);
	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to create request ED for IA " << name << " with code:" << pt::udapl::dat::errorToString(ret);
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}

	ret = dat_evd_create(ia_handle_, DEFAULT_QUEUE_LENGTH, DAT_HANDLE_NULL, DAT_EVD_CONNECTION_FLAG, &conn_evd_hdl_);
	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to create connection ED for IA " << name << " with code:" << pt::udapl::dat::errorToString(ret);
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}

	// Create event dispatcher for connection requests, PSP

	ret = dat_evd_create(ia_handle_, DEFAULT_QUEUE_LENGTH, DAT_HANDLE_NULL, DAT_EVD_CR_FLAG, &creq_evd_hdl_);
	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to create connection request ED for IA " << name << " with code:" << pt::udapl::dat::errorToString(ret);
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}

	ret = dat_evd_create(ia_handle_, DEFAULT_QUEUE_LENGTH, cno_handle_, DAT_EVD_SOFTWARE_FLAG, &se_evd_hdl_);
	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to create CNO dispatcher for IA " << name << " with code:" << pt::udapl::dat::errorToString(ret);
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}

	// SRQ preparation

	/*
	 DAT_SRQ_HANDLE srq_handle_;

	 std::cout << "Going to create SRQ" << std::endl;

	 DAT_SRQ_ATTR srq_attr;
	 srq_attr.max_recv_dtos = 64;
	 srq_attr.max_recv_iov = 64;
	 srq_attr.low_watermark = DAT_SRQ_LW_DEFAULT;

	 ret = dat_srq_create (ia_handle_,pz_handle_, &srq_attr , &srq_handle_);
	 if (ret != DAT_SUCCESS) {
	 std::stringstream msg;
	 msg << "Failed to create SRQ for IA " << name << " with code:" << pt::udapl::dat::errorToString(ret);
	 XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	 }

	 std::cout << "Going to query SRQ" << std::endl;
	 DAT_SRQ_PARAM srq_param;
	 ret =  dat_srq_query ( srq_handle_, DAT_SRQ_FIELD_ALL,  &srq_param);
	 if (ret != DAT_SUCCESS) {
	 std::stringstream msg;
	 msg << "Failed to query SRQ for IA " << name << " with code:" << pt::udapl::dat::errorToString(ret);
	 XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	 }

	 std::cout << "SRQ srq_state: " << srq_param.srq_state << std::endl;
	 std::cout << "SRQ max_recv_dtos: " << srq_param.max_recv_dtos << std::endl;
	 std::cout << "SRQ max_recv_iov: " << srq_param.max_recv_iov << std::endl;
	 std::cout << "SRQ low_watermark: " << srq_param.low_watermark << std::endl;
	 std::cout << "SRQ available_dto_count: " << srq_param.available_dto_count << std::endl;
	 std::cout << "SRQ outstanding_dto_count: " << srq_param.outstanding_dto_count << std::endl;
	 */

	/* Query the IA */
/*
	ret = dat_ia_query(ia_handle_, &async_evd_hdl_, DAT_IA_ALL, &ia_attr_, DAT_PROVIDER_FIELD_ALL, &provider_attr_);

	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to query attributes for IA  " << name_ << ", with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}
*/

}

pt::udapl::dat::InterfaceAdapter::~InterfaceAdapter ()
{
	DAT_RETURN ret;

	ret = dat_evd_free(creq_evd_hdl_);

	ret = dat_evd_free(conn_evd_hdl_);

	ret = dat_evd_free(reqt_evd_hdl_);

	ret = dat_evd_free(recv_evd_hdl_);

	ret = dat_pz_free(pz_handle_);

	ret = dat_ia_close(ia_handle_, DAT_CLOSE_GRACEFUL_FLAG);
	if (ret != DAT_SUCCESS)
	{
		ret = dat_ia_close(ia_handle_, DAT_CLOSE_ABRUPT_FLAG);
		std::cerr << "Error in dat_ia_close (abrupt) error" << std::endl;
	}
}

pt::udapl::dat::EndPoint* pt::udapl::dat::InterfaceAdapter::createEndPoint () throw (pt::udapl::exception::Exception)
{
	pt::udapl::dat::EndPoint* ep = new pt::udapl::dat::EndPoint(this);
	endpointRegistry_.push_back(ep);
	return ep;
}

void pt::udapl::dat::InterfaceAdapter::destroyEndPoint (pt::udapl::dat::EndPoint* ep) throw (pt::udapl::exception::Exception)
{
	for (std::list<pt::udapl::dat::EndPoint*>::iterator i = endpointRegistry_.begin(); i != endpointRegistry_.end(); i++)
	{
		if ((*i) == ep)
		{
			endpointRegistry_.erase(i);
			delete ep;
			return;
		}
	}

	XCEPT_RAISE(pt::udapl::exception::Exception, "Cannot destroy endpoint, not found");

}

pt::udapl::dat::MemoryRegion* pt::udapl::dat::InterfaceAdapter::createMemoryRegion (unsigned char* buffer, DAT_UINT64 size) throw (pt::udapl::exception::Exception)
{
	pt::udapl::dat::MemoryRegion* ep = new pt::udapl::dat::MemoryRegion(this, buffer, size);
	memoryRegistry_.push_back(ep);
	return ep;

}

void pt::udapl::dat::InterfaceAdapter::destroyMemoryRegion (pt::udapl::dat::MemoryRegion* mr) throw (pt::udapl::exception::Exception)
{
	for (std::list<pt::udapl::dat::MemoryRegion*>::iterator i = memoryRegistry_.begin(); i != memoryRegistry_.end(); i++)
	{
		if ((*i) == mr)
		{
			memoryRegistry_.erase(i);
			delete mr;
			return;
		}
	}

	XCEPT_RAISE(pt::udapl::exception::Exception, "Cannot destroy memory region, not found");

}

pt::udapl::dat::PublicServicePoint* pt::udapl::dat::InterfaceAdapter::listen (DAT_CONN_QUAL port) throw (pt::udapl::exception::Exception)
{
	pt::udapl::dat::PublicServicePoint* psp = new pt::udapl::dat::PublicServicePoint(this, port);
	pspRegistry_.push_back(psp);
	return psp;

}

void pt::udapl::dat::InterfaceAdapter::shutdown (pt::udapl::dat::PublicServicePoint* psp) throw (pt::udapl::exception::Exception)
{
	for (std::list<pt::udapl::dat::PublicServicePoint*>::iterator i = pspRegistry_.begin(); i != pspRegistry_.end(); i++)
	{
		if ((*i) == psp)
		{
			pspRegistry_.erase(i);
			delete psp;
			return;
		}
	}

	XCEPT_RAISE(pt::udapl::exception::Exception, "Cannot destroy memory region, not found");

}

/*



 void TBasic::PrintAttributes()
 {

 DOUT((5, "*****  DAPL  Characteristics  *****"));
 DOUT((5, "Provider: %s  Version %d.%d  DAPL %d.%d",
 provider_attr.provider_name,
 provider_attr.provider_version_major,
 provider_attr.provider_version_minor,
 provider_attr.dapl_version_major,
 provider_attr.dapl_version_minor ));
 DOUT((5, "Adapter: %s by %s Version %d.%d",
 ia_attr.adapter_name,
 ia_attr.vendor_name,
 ia_attr.hardware_version_major,
 ia_attr.hardware_version_minor ));
 DOUT((5, "Supporting:"));
 DOUT((5, "\t%d EPs with %d DTOs and %d RDMA/RDs each",
 ia_attr.max_eps,
 ia_attr.max_dto_per_ep,
 ia_attr.max_rdma_read_per_ep ));
 DOUT((5, "\t%d EVDs of up to %d entries ",
 ia_attr.max_evds,
 ia_attr.max_evd_qlen));
 DOUT((5, "\tIOVs of up to %d elements",
 ia_attr.max_iov_segments_per_dto ));
 DOUT((5, "\t%d LMRs (and %d RMRs) of up to 0x" F64x " bytes",
 ia_attr.max_lmrs,
 ia_attr.max_rmrs,
 ia_attr.max_lmr_block_size));
 DOUT((5, "\tMaximum MTU 0x" F64x " bytes, RDMA 0x" F64x " bytes",
 ia_attr.max_mtu_size,
 ia_attr.max_rdma_size));
 DOUT((5, "\tMaximum Private data size %d bytes",
 provider_attr.max_private_data_size ));

 DAT_SOCK_ADDR6* ip6_addr = (DAT_SOCK_ADDR6 *) ia_attr.ia_address_ptr;

 if (ip6_addr->sin6_family == AF_INET6 )
 {
 DOUT((5, "\tLocal IP address  %x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x:%x",
 ip6_addr->sin6_addr.s6_addr[0],
 ip6_addr->sin6_addr.s6_addr[1],
 ip6_addr->sin6_addr.s6_addr[2],
 ip6_addr->sin6_addr.s6_addr[3],
 ip6_addr->sin6_addr.s6_addr[4],
 ip6_addr->sin6_addr.s6_addr[5],
 ip6_addr->sin6_addr.s6_addr[6],
 ip6_addr->sin6_addr.s6_addr[7],
 ip6_addr->sin6_addr.s6_addr[8],
 ip6_addr->sin6_addr.s6_addr[9],
 ip6_addr->sin6_addr.s6_addr[10],
 ip6_addr->sin6_addr.s6_addr[11],
 ip6_addr->sin6_addr.s6_addr[12],
 ip6_addr->sin6_addr.s6_addr[13],
 ip6_addr->sin6_addr.s6_addr[14],
 ip6_addr->sin6_addr.s6_addr[15]));
 }
 //   else if (ip6_addr->sin6_family == AF_INET )
 else
 {
 struct sockaddr_in *ip_addr = (struct sockaddr_in *)ia_attr.ia_address_ptr;
 int rval = (int) ip_addr->sin_addr.s_addr;

 DOUT((5, "\tLocal IP address %d.%d.%d.%d",
 (rval >>  0) & 0xff,
 (rval >>  8) & 0xff,
 (rval >> 16) & 0xff,
 (rval >> 24) & 0xff));
 }

 DOUT((5, "***** ***** ***** ***** ***** *****"));
 }

 bool TBasic::GetLocalIp4Addr(char* buf)
 {
 DAT_SOCK_ADDR6* ip6_addr = (DAT_SOCK_ADDR6 *) ia_attr.ia_address_ptr;
 if (ip6_addr->sin6_family == AF_INET6 ) return false;

 struct sockaddr_in *ip_addr = (struct sockaddr_in *)ia_attr.ia_address_ptr;

 int rval = ip_addr->sin_addr.s_addr;

 sprintf(buf,"%d.%d.%d.%d",
 (rval >>  0) & 0xff,
 (rval >>  8) & 0xff,
 (rval >> 16) & 0xff,
 (rval >> 24) & 0xff);
 return true;
 }




 long TBasic::GetEVDQueueLen(DAT_EVD_HANDLE evd_handle)
 {
 DAT_EVD_PARAM  evd_param;
 DAT_RETURN ret =
 dat_evd_query(evd_handle, DAT_EVD_FIELD_ALL, &evd_param);

 if (ret!=DAT_SUCCESS) {
 EOUT((ret, "GetEVDQueueLen error"));
 return 0;
 }

 return evd_param.evd_qlen;
 }

 long TBasic::GetSendQueueLen()
 {
 return GetEVDQueueLen(reqt_evd_hdl);
 }

 long TBasic::GetRecvQueueLen()
 {
 return GetEVDQueueLen(recv_evd_hdl);
 }

 */
