// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <sstream>
#include "pt/udapl/dat/PublicServicePoint.h"
#include "pt/udapl/dat/InterfaceAdapter.h"
#include "pt/udapl/dat/Utils.h"

pt::udapl::dat::PublicServicePoint::PublicServicePoint (pt::udapl::dat::InterfaceAdapter * ia, DAT_CONN_QUAL port) throw (pt::udapl::exception::Exception)
	: ia_(ia)
{

	DAT_RETURN ret = dat_psp_create(ia_->ia_handle_, port, ia_->creq_evd_hdl_, DAT_PSP_CONSUMER_FLAG, &psp_handle_);

	if (ret != DAT_SUCCESS)
	{

		std::stringstream msg;
		msg << "Failed to create PSP for IA " << ia_->name_ << " with code:" << pt::udapl::dat::errorToString(ret);
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}

}

pt::udapl::dat::PublicServicePoint::~PublicServicePoint ()
{
	dat_psp_free(psp_handle_);
}

/*
 ostream &operator<<(ostream &cout, pt::udapl::dat::PublicServicePoint emp)
 {

 DAT_EP_PARAM ep_params;
 DAT_RETURN ret = dat_ep_query (emp.ep_handle_, DAT_EP_FIELD_EP_ATTR_ALL, &ep_params);
 if (ret!=DAT_SUCCESS) {
 cout << "Failed to retrieve EP attributes" << std::endl;
 }

 cout << "EP (default S/R size is " << ep_params.ep_attr.max_request_dtos << "/" << ep_params.ep_attr.max_recv_dtos << std::endl;


 return cout;

 }
 */
