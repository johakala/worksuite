// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/udapl/Application.h"
#include "xgi/framework/Method.h"
#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"
#include "pt/PeerTransportAgent.h"
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"
#include "pt/udapl/dat/InterfaceAdapter.h"
#include "pt/udapl/PeerTransport.h"
#include "pt/udapl/Allocator.h"
#include "pt/udapl/dat/PollingWorkLoop.h"
#include "pt/udapl/dat/WaitingWorkLoop.h"

XDAQ_INSTANTIATOR_IMPL (pt::udapl::Application)

pt::udapl::Application::Application (xdaq::ApplicationStub* stub) throw (xdaq::exception::Exception)
	: xdaq::Application(stub), xgi::framework::UIManager(this)
{

	stub->getDescriptor()->setAttribute("icon", "/pt/udapl/images/udapl_64.png");
	stub->getDescriptor()->setAttribute("icon16x16", "/pt/udapl/images/udapl_16.png");

	this->senderPoolSize_ = 0x100000;
	this->receiverPoolSize_ = 0x100000;

	this->recvPoolName_ = "rudapl";
	this->sendPoolName_ = "sudapl";
	this->stateName_ = "Halted";	

	// Bind CGI callbacks
	xgi::framework::deferredbind(this, this, &pt::udapl::Application::Default, "Default");

	iaName_ = "unknown";
	this->getApplicationInfoSpace()->fireItemAvailable("iaName", &iaName_);
	this->getApplicationInfoSpace()->fireItemAvailable("receiverPoolSize", &receiverPoolSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("senderPoolSize", &senderPoolSize_);

	this->getApplicationInfoSpace()->fireItemAvailable("recvPoolName", &recvPoolName_);
	this->getApplicationInfoSpace()->fireItemAvailable("sendPoolName", &sendPoolName_);
	this->getApplicationInfoSpace()->fireItemAvailable("stateName", &stateName_);

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	xoap::bind(this, &pt::udapl::Application::fireEvent, "Configure", XDAQ_NS_URI);
	xoap::bind(this, &pt::udapl::Application::fireEvent, "configure", XDAQ_NS_URI);
	xoap::bind(this, &pt::udapl::Application::fireEvent, "Init", XDAQ_NS_URI);
	xoap::bind(this, &pt::udapl::Application::fireEvent, "init", XDAQ_NS_URI);
	xoap::bind(this, &pt::udapl::Application::fireEvent, "enable", XDAQ_NS_URI);
	xoap::bind(this, &pt::udapl::Application::fireEvent, "Enable", XDAQ_NS_URI);

}

//
// run control requests current paramater values
//
void pt::udapl::Application::actionPerformed (xdata::Event& e)
{

	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{

		// create IA
		pt::udapl::dat::InterfaceAdapter * ia = 0;

		try
		{
			//ia = new pt::udapl::dat::InterfaceAdapter("ofa-v2-ib0");
			ia = new pt::udapl::dat::InterfaceAdapter(iaName_.toString());
		}
		catch (pt::udapl::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "Failed to create interface adapter", e);
		}

		//getApplicationInfoSpace()->fireItemAvailable("poolName",&poolName_);

		toolbox::net::URN urn("toolbox-mem-pool", sendPoolName_);
		//poolName_ = urn.toString();

		toolbox::mem::Pool * pool = 0;
		try
		{
			//toolbox::mem::Allocator* a = new pt::udapl::Allocator(ia, 0x1f400000); // allows 2000 * 256KB
			//toolbox::mem::Allocator* a = new pt::udapl::Allocator(ia, 0x7D00000); // allows 500 * 256KB
			toolbox::mem::Allocator* a = new pt::udapl::Allocator(ia, "toolbox-mem-allocator-udapl-sender", senderPoolSize_);
			pool = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
			pool->setHighThreshold((unsigned long) ((xdata::UnsignedIntegerT) senderPoolSize_ * 0.9));
			pool->setLowThreshold((unsigned long) ((xdata::UnsignedIntegerT) senderPoolSize_ * 0.5));
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "Cannot create default memory pool", e);
		}

		toolbox::mem::Pool * rpool = 0;
		toolbox::net::URN rurn("toolbox-mem-pool", recvPoolName_);
		try
		{
			// toolbox::mem::Allocator* a = new pt::udapl::Allocator(ia, 0x1f400000); // allows 2000 * 256KB
			//toolbox::mem::Allocator* a = new pt::udapl::Allocator(ia, 0x7D00000); // allows 500 * 256KB
			toolbox::mem::Allocator* a = new pt::udapl::Allocator(ia, "toolbox-mem-allocator-udapl-receiver", receiverPoolSize_);
			rpool = toolbox::mem::getMemoryPoolFactory()->createPool(rurn, a);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "Cannot create default memory pool", e);
		}

		pt_ = 0;

		try
		{
			pt_ = new pt::udapl::PeerTransport(this, ia, pool, rpool);
		}
		catch (pt::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "Cannot create peer transport", e);
		}

		pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
		pta->addPeerTransport(pt_);

		// Create work loops, automatically activated
		acceptorWorkLoop_ = new pt::udapl::dat::WaitingWorkLoop("udapl-acceptor", ia->creq_evd_hdl_, pt_, pt_);

		connectorWorkLoop_ = new pt::udapl::dat::WaitingWorkLoop("udapl-connector", ia->conn_evd_hdl_, pt_, pt_);

		receiverWorkLoop_ = new pt::udapl::dat::PollingWorkLoop("udapl-receiver", ia->recv_evd_hdl_, pt_, pt_);
		senderWorkLoop_ = new pt::udapl::dat::PollingWorkLoop("udapl-sender", ia->reqt_evd_hdl_, pt_, pt_);
		cnoWorkLoop_ = new pt::udapl::dat::CNOWorkLoop("udapl-cno", ia->cno_handle_, pt_, pt_);

		//pt::udapl::dat::TxRxWorkLoop * txrxWorkLoop            = 0;
		//txrxWorkLoop =  new pt::udapl::dat::TxRxWorkLoop("udapl-txrx",    ia->reqt_evd_hdl_, ia->recv_evd_hdl_, pt, pt);
		//
		//
		std::cout << "DONE MAKING UDAPL SET DEFAULT" << std::endl;
	}

}

xoap::MessageReference pt::udapl::Application::fireEvent (xoap::MessageReference msg) throw (xoap::exception::Exception)
{
	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();
	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();

	for (unsigned int i = 0; i < bodyList->getLength(); i++)
	{
		DOMNode* command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			std::string commandName = toolbox::tolower(xoap::XMLCh2String(command->getLocalName()));
			
			std::stringstream ss;
			ss << "Received SOAP command '" << commandName << "'";
			LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());

			if ((commandName == "enable") || (commandName == "init"))
			{
				stateName_ = "Enabled"; 
			}
			else if (commandName == "configure")
			{
				stateName_ = "Configured"; 
			}

			xoap::MessageReference reply = xoap::createMessage();
			xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
			xoap::SOAPName responseName = envelope.createName(commandName + "Response", "xdaq", XDAQ_NS_URI);

			xoap::SOAPBodyElement responseElement = envelope.getBody().addBodyElement(responseName);
			xoap::SOAPName stateName = envelope.createName("state", "xdaq", XDAQ_NS_URI);
    			xoap::SOAPElement stateElement = responseElement.addChildElement(stateName);
    			xoap::SOAPName attributeName = envelope.createName("stateName", "xdaq", XDAQ_NS_URI);
			stateElement.addAttribute(attributeName, stateName_);
			return reply;
		}
	}

	XCEPT_RAISE(xcept::Exception, "SOAP command not found");
}


void pt::udapl::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;

	*out << cgicc::caption("Work Loop Counters");

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Workloop") << std::endl;
	*out << cgicc::th("Counter") << std::endl;
	*out << cgicc::th("Value") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr() << std::endl;

	*out << cgicc::td("udapl-receiver") << std::endl;
	*out << cgicc::td("emptyDispatcherQueueCounter") << std::endl;
	std::stringstream val1;
	val1 << receiverWorkLoop_->emptyDispatcherQueueCounter_;
	*out << cgicc::td(val1.str()) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("udapl-received")<< std::endl;
	*out << cgicc::td("receivedEventCounter") << std::endl;
	std::stringstream val2;
	val2 << receiverWorkLoop_->receivedEventCounter_;
	*out << cgicc::td(val2.str()) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::td("udapl-sender") << std::endl;
	*out << cgicc::td("emptyDispatcherQueueCounter") << std::endl;
	std::stringstream val3;
	val3 << senderWorkLoop_->emptyDispatcherQueueCounter_;
	*out << cgicc::td(val3.str()) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("udapl-sender") << std::endl;
	*out << cgicc::td("receivedEventCounter") << std::endl;
	std::stringstream val4;
	val4 << senderWorkLoop_->receivedEventCounter_;
	*out << cgicc::td(val4.str()) << std::endl;
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << cgicc::table().set("class","xdaq-table").set("style", " width: 100%;") << std::endl;

	*out << cgicc::caption("Sender enpoint status");

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Address") << std::endl;
	*out << cgicc::th("Credit") << std::endl;
	*out << cgicc::th("ep_state") << std::endl;
	*out << cgicc::th("recv_idle") << std::endl;
	*out << cgicc::th("request_idle") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	for (std::list<pt::udapl::I2OMessenger *>::iterator i = pt_->messengers_.begin(); i != pt_->messengers_.end(); i++)
	{

		// query sender endpoint
		DAT_EP_STATE ep_state;
		DAT_BOOLEAN recv_idle;
		DAT_BOOLEAN request_idle;
		(*i)->ep_->getStatus(&ep_state, &recv_idle, &request_idle);

		*out << cgicc::tr() << std::endl;

		*out << cgicc::td("Address") << std::endl;
		std::stringstream value;
		value << (*i)->ep_->credits_;
		*out << cgicc::td(value.str()) << std::endl;
		*out << cgicc::td(pt::udapl::dat::epStateToString(ep_state));
		std::stringstream v1;
		v1 << recv_idle;
		*out << cgicc::td(v1.str());
		std::stringstream v2;
		v2 << request_idle;
		*out << cgicc::td(v2.str());

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	*out << cgicc::table().set("class","xdaq-table").set("style", "width: 100%;") << std::endl;

	*out << cgicc::caption("Receiver Enpoint Status");

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("address") << std::endl;
	*out << cgicc::th("credit") << std::endl;
	*out << cgicc::th("ep_state") << std::endl;
	*out << cgicc::th("recv_idle") << std::endl;
	*out << cgicc::th("request_idle") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	for (std::list<pt::udapl::dat::EndPoint*>::iterator i = pt_->accepted_.begin(); i != pt_->accepted_.end(); i++)
	{

		// query sender endpoint
		DAT_EP_STATE ep_state;
		DAT_BOOLEAN recv_idle;
		DAT_BOOLEAN request_idle;
		(*i)->getStatus(&ep_state, &recv_idle, &request_idle);

		*out << cgicc::tr() << std::endl;

		*out << cgicc::td("address") << std::endl;
		std::stringstream value;
		value << (*i)->credits_;
		*out << cgicc::td(value.str()) << std::endl;
		*out << cgicc::td(pt::udapl::dat::epStateToString(ep_state));
		std::stringstream v1;
		v1 << recv_idle;
		*out << cgicc::td(v1.str());
		std::stringstream v2;
		v2 << request_idle;
		*out << cgicc::td(v2.str());

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

