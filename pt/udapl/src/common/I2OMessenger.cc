// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci * 
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/udapl/I2OMessenger.h"
#include "pt/udapl/PeerTransport.h"
#include "pt/udapl/Address.h"

pt::udapl::I2OMessenger::I2OMessenger (pt::udapl::PeerTransport * pt, pt::udapl::dat::EndPoint * ep, pt::Address::Reference destination, pt::Address::Reference local)
	: pt_(pt), destination_(destination), local_(local), ep_(ep), isConnected_(false)
{
	dynamic_cast<pt::udapl::Address &>(*destination_).toString();
	std::cout << "creating messenger for local address " << dynamic_cast<pt::udapl::Address &>(*local).toString() << " remote " << dynamic_cast<pt::udapl::Address &>(*destination).toString() << std::endl;
}

pt::udapl::I2OMessenger::~I2OMessenger ()
{
}

void pt::udapl::I2OMessenger::send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler, void* context) throw (pt::exception::Exception)
{
	if (!isConnected_)
	{
		// launch connection and synchronize
		pt::udapl::Address & address = dynamic_cast<pt::udapl::Address &>(*destination_);

		try
		{
			pt_->connect(ep_, address.getHost(), address.getPortNum());
			//	ep_->connect(address.getHost(), address.getPortNum());
			size_t retryNumber = 0;
			size_t maxNumberOfRetry = 100;
			while (retryNumber < maxNumberOfRetry)
			{
				std::cout << "check if connected " << isConnected_ << " retrynnum " << retryNumber << std::endl;
				if (isConnected_) break;
				::usleep(10000); // 10ms
				retryNumber++;
			}

			if (retryNumber == maxNumberOfRetry)
			{
				std::cout << "timeout " << isConnected_ << std::endl;
				XCEPT_RAISE(pt::exception::Exception, "failed to connect endpoint, timeout");

			}

		}
		catch (pt::udapl::exception::Exception & e)
		{
			std::cout << "fail to schedule connect " << isConnected_ << std::endl;
			XCEPT_RETHROW(pt::exception::Exception, "failed to connect endpoint", e);
		}

	}

	// check for credits

	//std::cout << "before posting frame " << isConnected_ << std::endl;
	pt_->post(msg, ep_, handler, context);

}

pt::Address::Reference pt::udapl::I2OMessenger::getLocalAddress ()
{
	return local_;
}

pt::Address::Reference pt::udapl::I2OMessenger::getDestinationAddress ()
{
	return destination_;
}
