// $Id: CommittedHeapBuffer.cc,v 1.5 2008/07/18 15:27:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/udapl/PeerTransport.h"
#include "pt/udapl/Buffer.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "pt/exception/ReceiveFailure.h"
#include "pt/udapl/dat/Utils.h"
#include "i2o/i2o.h"
#include "xcept/tools.h"

//#ifndef DAT_DTO_SEND
//#define DAT_DTO_SEND 0
//#endif

//#ifndef DAT_DTO_RECEIVE
//#define DAT_DTO_RECEIVE 1
//#endif

pt::udapl::PeerTransport::PeerTransport (xdaq::Application * owner, pt::udapl::dat::InterfaceAdapter * ia, toolbox::mem::Pool * pool, toolbox::mem::Pool * rpool) throw (pt::exception::Exception)
	: xdaq::Object(owner), mutex_(toolbox::BSem::FULL, true), ia_(ia), pool_(pool), rpool_(rpool)
{

	factory_ = toolbox::mem::getMemoryPoolFactory();
	receiverLoop_ = new pt::udapl::ReceiverLoop("udapl-i2o-dispatcher-loop", 0);

}

pt::udapl::PeerTransport::~PeerTransport ()
{

	// TBD

}

pt::Messenger::Reference pt::udapl::PeerTransport::getMessenger (pt::Address::Reference destination, pt::Address::Reference local) throw (pt::exception::UnknownProtocolOrService)
{

	// Look if a messenger from the local to the remote destination exists and return it.	
	// If it doesn't exist, create it and return it.
	// It accept the service to be null, in this case, it assume that it is soap service. Of course this is not secure but it provide a
	// useful omission as default
	if ((destination->getService() == "i2o") && (local->getService() == "i2o") && (destination->getProtocol() == "udapl") && (local->getProtocol() == "udapl"))
	{

		// Create new messenger
		if (local->equals(destination))
		{
			// Creating a local messenger (within same logical host == executive) should not be supported.
			// In this case another transport protocol should be used.
			//
			// http::SOAPLoopbackMessenger* m = new http::SOAPLoopbackMessenger(destination, local);
			// soapMessengers_.push_back(m);
			// return m;
			std::string msg = "DAPL protocol communication within the same XDAQ process is not supported. Use the fifo transport for local communication.";
			XCEPT_RAISE(pt::exception::UnknownProtocolOrService, msg);
		}
		else
		{
			// create remote messenger
			pt::udapl::I2OMessenger* m = new pt::udapl::I2OMessenger(this, ia_->createEndPoint(), destination, local);
			mutex_.take();
			messengers_.push_back(m); // we remember any created messenger
			mutex_.give();
			return pt::Messenger::Reference(m);
			// note: we are not the owner of messenger, but the application context messenger cache!
		}
	}
	else
	{
		std::string msg = "Cannot handle protocol service combination, destination protocol was :";
		msg += destination->getProtocol();
		msg += " destination service was:";
		msg += destination->getService();
		msg += " while local protocol was:";
		msg += local->getProtocol();
		msg += "and local service was:";
		msg += local->getService();
		XCEPT_RAISE(pt::exception::UnknownProtocolOrService, msg);
	}

}

pt::TransportType pt::udapl::PeerTransport::getType ()
{
	return pt::SenderReceiver;
}

// The FIFO transport does not support any addresses. It is always locally available
//
pt::Address::Reference pt::udapl::PeerTransport::createAddress (const std::string& url, const std::string& service) throw (pt::exception::InvalidAddress)
{
	return pt::Address::Reference(new pt::udapl::Address(url, service));
}

pt::Address::Reference pt::udapl::PeerTransport::createAddress (std::map<std::string, std::string, std::less<std::string> >& address) throw (pt::exception::InvalidAddress)
{
	std::string protocol = address["protocol"];

	if (protocol == "udapl")
	{
		std::string url = protocol;

		XCEPT_ASSERT(address["hostname"] != "", pt::exception::InvalidAddress, "Cannot create address, hostname not specified");
		XCEPT_ASSERT(address["port"] != "", pt::exception::InvalidAddress, "Cannot create address, port number not specified");

		url += "://";
		url += address["hostname"];
		url += ":";
		url += address["port"];

		std::string service = address["service"];
		if (service != "")
		{
			if (!this->isServiceSupported(service))
			{
				std::string msg = "Cannot create address, specified service for protocol ";
				msg += protocol;
				msg += " not supported: ";
				msg += service;
				XCEPT_RAISE(pt::exception::InvalidAddress, msg);
			}
		}

		return this->createAddress(url, service);
	}
	else
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}
}

std::string pt::udapl::PeerTransport::getProtocol ()
{
	return "udapl";
}

void pt::udapl::PeerTransport::post (toolbox::mem::Reference * reference, pt::udapl::dat::EndPoint * ep, toolbox::exception::HandlerSignature * handler, void * context) throw (pt::exception::Exception)
{
	size_t size = reference->getDataSize();
	PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) reference->getDataLocation();

	if (size == (unsigned long) (frame->MessageSize << 2))
	{

		DAT_CONTEXT cookie;
		cookie.as_ptr = reference; // remember which reference
		pt::udapl::Buffer * buffer = 0;
		try
		{
			buffer = dynamic_cast<pt::udapl::Buffer*>(reference->getBuffer());
			if (buffer == 0)
			{
				XCEPT_RAISE(pt::exception::Exception, "Failed to cast, invalid memory allocator/pool");
			}
		}
		catch (std::bad_cast & e)
		{
			XCEPT_RAISE(pt::exception::Exception, e.what());
		}
		buffer->endpoint_.as_ptr = ep; // remember endpoint
		buffer->operation_.as_64 = 0xcafecafe; // remember operation 

		size_t retry = 0;
		size_t maxNumberOfRetry = 10;
		while (retry < maxNumberOfRetry)
		{
			try
			{

				ep->postSendBuffer(buffer->mr_, cookie, &(buffer->local_iov), size);
				break;
			}
			catch (pt::udapl::exception::InsufficientResources & e)
			{
				std::cerr << "failed to post " << retry << std::endl;
				if (retry == maxNumberOfRetry)
				{
					XCEPT_RETHROW(pt::exception::Exception, "Failed to send", e);
				}
				retry++;
				::usleep(1000);
			}
			catch (pt::udapl::exception::Exception & e)
			{
				std::cerr << "failed to post generic  " << retry << std::endl;
				XCEPT_RETHROW(pt::exception::Exception, "Failed to post message for sending", e);
			}
		}

		if (retry > 0) std::cout << "loss of time,  retried " << retry << " times" << std::endl;
	}
	else
	{
		std::string msg = toolbox::toString("Frame size %d and Reference data size %d  do not match (frame dropped)", frame->MessageSize << 2, size);
		reference->release();
		XCEPT_RAISE(pt::exception::Exception, msg);

	}

}

void pt::udapl::PeerTransport::config (pt::Address::Reference address) throw (pt::exception::Exception)
{
	// just tell interface adpater we are list
	try
	{
		pt::udapl::Address & a = dynamic_cast<pt::udapl::Address &>(*address);
		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "going to listen on port " << a.getPortNum());
		ia_->listen(a.getPortNum());
	}
	catch (pt::udapl::exception::Exception &e)
	{
		XCEPT_RETHROW(pt::exception::Exception, "Failed to configure receiver port on IA", e);

	}
}

std::vector<std::string> pt::udapl::PeerTransport::getSupportedServices ()
{
	std::vector < std::string > s;
	s.push_back("i2o");
	return s;
}

bool pt::udapl::PeerTransport::isServiceSupported (const std::string& service)
{
	if (service == "i2o")
		return true;
	else
		return false;
}

void pt::udapl::PeerTransport::addServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{

	pt::PeerTransportReceiver::addServiceListener(listener);
	if (listener->getService() == "i2o")
	{
		listener_ = dynamic_cast<i2o::Listener *>(listener);
		receiverLoop_->listener_ = listener_;
	}
}

void pt::udapl::PeerTransport::removeServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{
	pt::PeerTransportReceiver::removeServiceListener(listener);
	if (listener->getService() == "i2o")
	{
		listener_ = 0;
	}
}

void pt::udapl::PeerTransport::removeAllServiceListeners ()
{
	pt::PeerTransportReceiver::removeAllServiceListeners();
	listener_ = 0;
}

void pt::udapl::PeerTransport::handleError (xcept::Exception & e)
{
	std::cerr << "Asynchronous error " << xcept::stdformat_exception_history(e) << std::endl;
}

// this callback can be invoked by several threads
void pt::udapl::PeerTransport::handleEvent (DAT_EVENT & event)
{
	DAT_RETURN ret;
	// 
	//
	//
	if (event.event_number == DAT_CONNECTION_EVENT_ESTABLISHED)
	{
		mutex_.take();

		// prepare endpoint for sending
		for (std::list<pt::udapl::I2OMessenger *>::iterator i = messengers_.begin(); i != messengers_.end(); i++)
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Establishing connection for sender");
			if ((*i)->ep_->ep_handle_ == event.event_data.connect_event_data.ep_handle) // retrieve messenger object containing endpoint
			{
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "enable connection for sender");
				(*i)->isConnected_ = true;
				mutex_.give();
				return;
			}
		}

		// prepare endpoint for receiving 
		for (std::list<pt::udapl::dat::EndPoint *>::iterator j = accepted_.begin(); j != accepted_.end(); j++)
		{
			if ((*j)->ep_handle_ == event.event_data.connect_event_data.ep_handle) // retrieve end point object
			{
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Establishing connection for receiver");
				// post 12 buffers of 256 kB on establised receiver endpoint
				for (size_t k = 0; k < MAX_UDAPL_RECEIVE_BUFFERS; k++)
				{
					//std::cout << "going to push buffer on receiver endpoint" << std::endl;
					toolbox::mem::Reference * reference;

					try
					{
						reference = factory_->getFrame(rpool_, PT_UDAPL_MAX_MTU_SIZE); // 256KB max size for i2o messages
					}
					catch (toolbox::mem::exception::Exception & ex)
					{
						std::stringstream msg;
						msg << "Failed to allocate buffer for incoming message";
						XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
						this->getOwnerApplication()->notifyQualified("fatal", e);
						mutex_.give();
						return;
					}

					pt::udapl::Buffer * buffer = dynamic_cast<pt::udapl::Buffer*>(reference->getBuffer());
					try
					{
						buffer = dynamic_cast<pt::udapl::Buffer*>(reference->getBuffer());
					}
					catch (std::bad_cast & e)
					{
						XCEPT_DECLARE(pt::exception::ReceiveFailure, ex, e.what());
						this->getOwnerApplication()->notifyQualified("fatal", ex);
						reference->release();
						mutex_.give();
						return;
					}

					buffer->endpoint_.as_ptr = (*j); // remember endpoint object
					buffer->operation_.as_64 = 0xcedecede;

					DAT_CONTEXT cookie;
					cookie.as_ptr = reference; // rememeber which reference

					try
					{
						(*j)->postRecvBuffer(buffer->mr_, cookie, &(buffer->local_iov), PT_UDAPL_MAX_MTU_SIZE);
					}
					catch (pt::udapl::exception::InsufficientResources & e)
					{
						std::stringstream msg;
						msg << "Failed to allocate buffer for incoming message";
						XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
						this->getOwnerApplication()->notifyQualified("fatal", ex);
						reference->release();
						mutex_.give();
						return;
					}
					catch (pt::udapl::exception::Exception & e)
					{
						std::stringstream msg;
						msg << "Failed to allocate buffer for incoming message";
						XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
						this->getOwnerApplication()->notifyQualified("fatal", ex);
						reference->release();
						mutex_.give();
						return;

					}

				}
				mutex_.give();
				return;
			}
		}

		mutex_.give();
	}
	else if (event.event_number == DAT_CONNECTION_EVENT_PEER_REJECTED || event.event_number == DAT_CONNECTION_EVENT_NON_PEER_REJECTED || event.event_number == DAT_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR || event.event_number == DAT_CONNECTION_EVENT_DISCONNECTED || event.event_number == DAT_CONNECTION_EVENT_BROKEN || event.event_number == DAT_CONNECTION_EVENT_UNREACHABLE || event.event_number == DAT_CONNECTION_EVENT_TIMED_OUT)
	{
		std::cout << "******************** BAD BAD BAD " << std::endl;
		// could not establish 
		// notify error here
		std::stringstream msg;
		msg << "Failed establishing connection" << ia_->name_ << " with code:" << pt::udapl::dat::eventToString(event.event_number);
		XCEPT_DECLARE(pt::udapl::exception::Exception, e, msg.str());
		this->getOwnerApplication()->notifyQualified("error", e);
		return;

	}
	else if (event.event_number == DAT_CONNECTION_REQUEST_EVENT)
	{
		mutex_.take();
		pt::udapl::dat::EndPoint * ep = ia_->createEndPoint();

		DAT_CR_PARAM cr_param;

		ret = dat_cr_query(event.event_data.cr_arrival_event_data.cr_handle, DAT_CR_FIELD_ALL, &cr_param);
		if (ret != DAT_SUCCESS)
		{
			std::stringstream msg;
			msg << "Failed quering connection request " << ia_->name_ << " with code:" << pt::udapl::dat::errorToString(ret);
			XCEPT_DECLARE(pt::udapl::exception::Exception, e, msg.str());
			this->getOwnerApplication()->notifyQualified("error", e);
			mutex_.give();
			return;
		}

		/*

		 char ipaddr[100];
		 DAT_SOCK_ADDR6* ip6_addr = (DAT_SOCK_ADDR6 *) ia_attr.ia_address_ptr;
		 struct sockaddr_in *ip_addr = (struct sockaddr_in *)ia_attr.ia_address_ptr;
		 int rval = ip_addr->sin_addr.s_addr;
		 sprintf(ipaddr,"%d.%d.%d.%d", (rval >>  0) & 0xff, (rval >>  8) & 0xff, (rval >> 16) & 0xff, (rval >> 24) & 0xff);
		 */

		const char* remoteaddr = (const char*) cr_param.private_data;
		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Accepting connection from " << remoteaddr);
		/*
		 if (cr_param.private_data_size>0)
		 {
		 EP->SetAdrreses(ipaddr, remoteaddr);
		 DOUT((4,"GET: %s", remoteaddr));
		 }
		 */

		// send that we accept connection
		ret = dat_cr_accept(event.event_data.cr_arrival_event_data.cr_handle, ep->ep_handle_, 0, (DAT_PVOID) 0);
		if (ret != DAT_SUCCESS)
		{
			ia_->destroyEndPoint(ep);
			std::stringstream msg;
			msg << "Failed accepting connection" << ia_->name_ << " with code:" << pt::udapl::dat::errorToString(ret);
			XCEPT_DECLARE(pt::udapl::exception::Exception, e, msg.str());
			this->getOwnerApplication()->notifyQualified("error", e);
			mutex_.give();
			return;

		}

		// keep record of accepted connections
		accepted_.push_back(ep);
		mutex_.give();
	}
	else if (event.event_number == DAT_DTO_COMPLETION_EVENT)
	{
		toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.dto_completion_event_data.user_cookie.as_ptr;

		PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) ref->getDataLocation();
		ref->setDataSize(frame->MessageSize << 2);
		//ref->setDataSize(event.event_data.dto_completion_event_data.transfered_length);
		pt::udapl::Buffer * buffer = dynamic_cast<pt::udapl::Buffer*>(ref->getBuffer());
		DAT_UINT64 operation = buffer->operation_.as_64;

		//if ( event.event_data.dto_completion_event_data.operation == c )
		if ((operation == 0xcafecafe) /*&& (event.event_data.dto_completion_event_data.operation == DAT_DTO_SEND)*/)
		{
			//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "send was perfromed , free buffer");
#ifdef PT_UDAPL_FLOW_CONTROL
			pt::udapl::dat::EndPoint * ep = static_cast<pt::udapl::dat::EndPoint *>(buffer->endpoint_.as_ptr);
			ep->incrementCredits();
#endif

			ref->release();
		}
		//else if ( event.event_data.dto_completion_event_data.operation == DAT_DTO_RECEIVE )
		else if ((operation == 0xcedecede) /*&& (event.event_data.dto_completion_event_data.operation == DAT_DTO_RECEIVE)*/)
		{
			// find the endpoint and feed with a new buffer might cost a linear loop for each receive operation
			//event.event_data.dto_completion_event_data.ep_handle;

			//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "received buffer, try to replace with a new fresh");

			pt::udapl::dat::EndPoint * ep = static_cast<pt::udapl::dat::EndPoint *>(buffer->endpoint_.as_ptr);

			// re-submit a buffer on EndPoint immediately, assume it never goes out of memory here
			toolbox::mem::Reference * reference;
			try
			{
				reference = factory_->getFrame(rpool_, PT_UDAPL_MAX_MTU_SIZE); // 256KB max size for i2o messages
			}
			catch (toolbox::mem::exception::Exception & ex)
			{
				std::cerr << " OH SO VERY BAD BAD" << std::endl;
				std::stringstream msg;
				msg << "Failed to allocate buffer for incoming message";
				XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("fatal", e);
				return;
			}

			DAT_CONTEXT cookie;
			cookie.as_ptr = reference; // rememeber which reference
			pt::udapl::Buffer * buffer = dynamic_cast<pt::udapl::Buffer*>(reference->getBuffer());
			buffer->endpoint_.as_ptr = ep;
			buffer->operation_.as_64 = 0xcedecede;
			try
			{
				//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "ppost fresh buffer to receiver");
				ep->postRecvBuffer(buffer->mr_, cookie, &(buffer->local_iov), PT_UDAPL_MAX_MTU_SIZE);
			}
			catch (pt::udapl::exception::InsufficientResources & e)
			{
				std::cerr << " OH SO BAD BAD" << std::endl;
				std::stringstream msg;
				msg << "Failed to allocate buffer for incoming message";
				XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}
			catch (pt::udapl::exception::Exception & e)
			{
				std::cerr << " OH BAD BAD" << std::endl;
				std::stringstream msg;
				msg << "Failed to allocate buffer for incoming message";
				XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;

			}

			//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "going o dispatch i2o message");
			// deliver buffer to application for processing	
			if (listener_ != 0)
			{
				//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "found listener, processIncomingMessage");
				listener_->processIncomingMessage(ref);
				//receiverLoop_->dispatchQueue_.push(ref);
			}
			else
			{
				std::cerr << " OH BAD" << std::endl;
				std::string msg = toolbox::toString("No Listener for i2o found in ReceiverLoop of pt::udapl");
				//std::cout << msg << std::endl;
				if (ref) ref->release();
				XCEPT_DECLARE(pt::exception::ReceiveFailure, e, msg);
				this->getOwnerApplication()->notifyQualified("fatal", e);
				return;
			}
		}
		else
		{
			std::cerr << " very bad not undertood DTO" << pt::udapl::dat::eventToString(event.event_number) << " with " << event.event_data.dto_completion_event_data.operation << " and " << operation << std::endl;
			std::cerr << "expected either " << DAT_DTO_SEND << " or " << DAT_DTO_RECEIVE << std::endl;

		}
	}
	else
	{
		std::cerr << " very BAD not understood event " << pt::udapl::dat::eventToString(event.event_number) << std::endl;
		std::stringstream msg;
		msg << "Invalidf event id " << event.event_number << "(" << pt::udapl::dat::eventToString(event.event_number) << ")";
		XCEPT_DECLARE(pt::udapl::exception::Exception, e, msg.str());
		this->getOwnerApplication()->notifyQualified("error", e);
		return;
	}

}

void pt::udapl::PeerTransport::connect (pt::udapl::dat::EndPoint * ep, const std::string & host, size_t port) throw (pt::exception::Exception)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "schedule connection");

	try
	{
		ep->connect(host, port);
	}
	catch (pt::udapl::exception::Exception & e)
	{
		XCEPT_RETHROW(pt::exception::Exception, "Failed to submit connect", e);
	}

}
