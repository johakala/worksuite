// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_ibv_I2OMessenger_h
#define _pt_ibv_I2OMessenger_h

#include "i2o/Messenger.h"
#include "toolbox/mem/Reference.h"
#include "pt/ibv/Address.h"
#include "pt/ibv/exception/Exception.h"

#include "ibvla/QueuePair.h"

namespace pt
{
	namespace ibv
	{

		class PeerTransport;

		class I2OMessenger : public i2o::Messenger
		{
			public:

				I2OMessenger (pt::ibv::PeerTransport * pt, ibvla::QueuePair & qp, pt::Address::Reference local, pt::Address::Reference destination);

				virtual ~I2OMessenger ();

				pt::Address::Reference getLocalAddress ();
				pt::Address::Reference getDestinationAddress ();

				void send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler, void* context) throw (pt::exception::Exception);

				friend std::ostream& operator<< (std::ostream &sout, I2OMessenger & mes);

				void postConnect () throw (pt::ibv::exception::Exception);

			private:

				pt::ibv::PeerTransport * pt_;
				pt::Address::Reference destination_;
				pt::Address::Reference local_;

			public:

				ibvla::QueuePair qp_;
				bool isConnected_;

				size_t outstandingRequests_;

		};
	}
}

#endif

