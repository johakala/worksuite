// $Id$
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_ibv_RelayWorkLoop_h_
#define _pt_ibv_RelayWorkLoop_h_

#include <iostream>
#include <string>

#include "pt/ibv/exception/Exception.h"
#include "toolbox/lang/Class.h"
#include "toolbox/task/PollingWorkLoop.h"
#include "i2o/Listener.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/rlist.h"
#include "toolbox/squeue.h"
#include "toolbox/BSem.h"

namespace pt
{
	namespace ibv 
	{
		class RelayWorkLoop : public toolbox::lang::Class
		{
			public:

				RelayWorkLoop (const std::string & name, size_t size) throw (pt::ibv::exception::Exception);

				virtual ~RelayWorkLoop ();

				bool process (toolbox::task::WorkLoop* wl);

				void handOver(toolbox::mem::Reference* ref) throw (pt::ibv::exception::Exception);
				void addListener(i2o::Listener* listener);

			protected:

				std::string name_;
				toolbox::task::WorkLoop* workLoop_;
				toolbox::task::ActionSignature* process_;

				i2o::Listener* listener_;

				typedef struct recvbuf {
						toolbox::mem::Reference* ref;
				} RecvBuf;

				toolbox::rlist<RecvBuf>  * relayQueue_;
		};
	}
}

#endif
