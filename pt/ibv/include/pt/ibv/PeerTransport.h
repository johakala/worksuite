// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_ibv_PeerTransport_h
#define _pt_ibv_PeerTransport_h

#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"

#include "i2o/Listener.h"

#include "pt/exception/Exception.h"
#include "pt/ibv/exception/InsufficientResources.h"
#include "pt/ibv/exception/InternalError.h"
#include "pt/ibv/I2OMessenger.h"
#include "pt/ibv/RelayWorkLoop.h"

#include "xdata/Boolean.h"

#include "pt/Address.h"
#include "xdaq/Object.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/TimeVal.h"



#include "ibvla/QueuePair.h"
#include "ibvla/EventHandler.h"
#include "ibvla/WorkRequestHandler.h"

#include "ibvla/EventWorkLoop.h"
#include "ibvla/CompletionWorkLoop.h"
#include "ibvla/ConnectionRequest.h"
#include "ibvla/AcceptorListener.h"

//#define PT_UDAPL_MAX_MTU_SIZE 0x40000

namespace pt
{
	namespace ibv
	{
		const size_t MAX_ASYNC_EVENT = 19 + 1;
		const size_t MAX_WC_EVENT = 22 + 1; // number of possible errors + 2

		class PeerTransport : public pt::PeerTransportSender, public pt::PeerTransportReceiver, public xdaq::Object, public ibvla::EventHandler, public ibvla::WorkRequestHandler, public ibvla::AcceptorListener
		{
			public:

				//PeerTransport (xdaq::Application * parent, ibvla::Context & context, ibvla::ProtectionDomain & pd, toolbox::mem::Pool * spool, toolbox::mem::Pool * rpool, xdata::UnsignedInteger cqSize, xdata::UnsignedInteger sendQPSize, xdata::UnsignedInteger recvQPSize, xdata::UnsignedInteger maxMessageSize, xdata::UnsignedInteger deviceMTU, xdata::Boolean sendWithTimeout, xdata::Boolean useRelay) throw (pt::exception::Exception);
				PeerTransport (xdaq::Application * parent) throw (pt::exception::Exception);

				~PeerTransport ();

				//! Enqueue a binary message to the peer transport's work loop. Raise an exception if queue is full
				//
				void post (toolbox::mem::Reference* ref, ibvla::QueuePair & qp, toolbox::exception::HandlerSignature* handler, void* context) throw (pt::ibv::exception::InsufficientResources, pt::ibv::exception::InternalError);

				//! Retrieve the type of peer transport (Sender or Receiver or both)
				//
				pt::TransportType getType ();

				pt::Address::Reference createAddress (const std::string& url, const std::string& service) throw (pt::exception::InvalidAddress);

				pt::Address::Reference createAddress (std::map<std::string, std::string, std::less<std::string> >& address) throw (pt::exception::InvalidAddress);

				//! Returns the implemented protocol ("loopback" in this version)
				//
				std::string getProtocol ();

				//! Retrieve a list of supported services ("i2o" only in this version)
				//
				std::vector<std::string> getSupportedServices ();

				//! Returns true if a service is supported ("i2o" only in this version), false otherwise
				//
				bool isServiceSupported (const std::string& service);

				//! Retrieve a loopback messenger for the fifo peer transport that allows context internal application communication
				//
				pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local) throw (pt::exception::UnknownProtocolOrService);

				//! Internal function to add a message processing listener for this peer transport
				//
				void addServiceListener (pt::Listener* listener) throw (pt::exception::Exception);

				//! Internal function to remove a message processing listener for this peer transport
				//
				void removeServiceListener (pt::Listener* listener) throw (pt::exception::Exception);

				//! Internal function to remove all message processing listeners for this peer transport
				//
				void removeAllServiceListeners ();

				//! Function to configure this peer transport with a loopback address
				//
				void config (pt::Address::Reference address) throw (pt::exception::Exception);

				void connect (ibvla::QueuePair & qp, const std::string & host, size_t port, size_t path) throw (pt::exception::Exception);

				std::list<pt::Messenger::Reference> getMessengers ();

				typedef struct qpInfo
				{
						qpInfo (std::string ip, std::string host, std::string port)
						{
							this->ip = ip;
							this->host = host;
							this->port = port;
							send_count = 0;
							recv_count = 0;
							error_count = 0;
							remote_qpn = 0;
							startConnectionTime = toolbox::TimeVal::zero();
							endConnectionTime = toolbox::TimeVal::zero();


						}

						std::string host;
						std::string port;
						std::string ip;
						size_t send_count;
						size_t recv_count;
						size_t error_count;
						size_t remote_qpn;
						toolbox::TimeVal startConnectionTime;
						toolbox::TimeVal endConnectionTime;

				} QP_INFO;

				std::list<ibvla::QueuePair> getAcceptedQueuePairs ();

				size_t asyncEventCounter_[MAX_ASYNC_EVENT]; // There are currently 19 defined async events
				size_t workCompletionCounter_[MAX_WC_EVENT]; // 21 errors + success (which is 0)

			protected:

				std::list<ibvla::QueuePair> acceptedQP_;

				// ibvla callbacks
				void handleEvent (struct ibv_async_event * event);
				void handleWorkRequest (struct ibv_wc * wc);
				void handleWorkRequestError (struct ibv_wc * wc);
				void connectionRequest (ibvla::ConnectionRequest id);

				void postReceiveBuffer (toolbox::mem::Pool * pool, ibvla::QueuePair & qp, size_t size);

				void moveQueuePairIntoError (ibvla::QueuePair & qp);
				void resetQueuePair (ibvla::QueuePair & qp);

				void cleanUpWorkRequestError (struct ibv_wc * wc) throw (pt::ibv::exception::Exception);

				std::list<pt::Messenger::Reference> messengers_;
				//std::list<ibvla::QueuePair> accepted_;

			protected:

				toolbox::BSem mutex_;

				ibvla::EventWorkLoop * ewl_;
				ibvla::CompletionWorkLoop * cwls_;
				ibvla::CompletionWorkLoop * cwlr_;

				pt::ibv::RelayWorkLoop *rwl_;

				i2o::Listener* listener_;

				ibvla::Context context_;
				ibvla::ProtectionDomain pd_;
				ibvla::CompletionQueue cqs_;
				ibvla::CompletionQueue cqr_;

				ibvla::Acceptor * acceptor_;

				toolbox::mem::Pool * spool_;
				//toolbox::mem::Pool * rpool_;
				toolbox::mem::MemoryPoolFactory * factory_;

				// configuration variables
				//xdata::UnsignedInteger cqSize_;
				//xdata::UnsignedInteger sendQPSize_;
				//xdata::UnsignedInteger recvQPSize_;

			public:

				//xdata::UnsignedInteger maxMessageSize_;
				//xdata::UnsignedInteger deviceMTU_;

				xdata::UnsignedInteger outstandingRequests_;
				xdata::UnsignedInteger totalSent_;
				xdata::UnsignedInteger totalRecv_;

				xdata::UnsignedInteger errors_;
				xdata::UnsignedInteger events_;

				ibvla::CompletionWorkLoop * getCompletionWorLoopReceiver();
				ibvla::CompletionWorkLoop * getCompletionWorLoopSender();

				//xdata::Boolean sendWithTimeout_;

				//xdata::Boolean useRelay_;
		};

	}
}

#endif
