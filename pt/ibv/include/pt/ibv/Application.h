// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_ibv_Application_h_
#define _pt_ibv_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "i2o/i2o.h"

#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"

#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationContext.h"

#include "i2o/Method.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/net/URN.h"
#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/fsm/FailedEvent.h"

#include <list>
#include <algorithm>
#include <map>
#include "toolbox/PerformanceMeter.h"

#include "pt/ibv/PeerTransport.h"

//#include "xgi/WSM.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "xoap/MessageReference.h"
#include "xgi/framework/Method.h"
#include "xdaq2rc/RcmsStateNotifier.h"

namespace pt
{
	namespace ibv
	{
		class Application : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
		{
			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* stub) throw (xdaq::exception::Exception);

				void actionPerformed (xdata::Event& e);

				void resetAsyncEventCount (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
				void resetWorkCompletionCount (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);

				void SenderTabPage (xgi::Output * sout);
				void ReceiverTabPage (xgi::Output * sout);
				void SettingsTabPage (xgi::Output * sout);
				void EventsTabPage (xgi::Output * sout);

				xoap::MessageReference fireEvent (xoap::MessageReference msg) throw (xoap::exception::Exception);

				void autoConnect ();
		
				// User callback invoked when failed state is entered
				void failed (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);
				// callback invoked when failed state is entered
				void inFailed (toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception);

				void notifyRCMS( std::string msg );

			    	/**
			     	* Callback invoked when the state machine of the application has changed.
			     	*/
			    	void stateChanged(toolbox::fsm::FiniteStateMachine & fsm) throw (toolbox::fsm::exception::Exception);
				void FailAction(toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception);

				ibvla::EventWorkLoop * ewl_;
				ibvla::CompletionWorkLoop * cwl_;

				pt::ibv::PeerTransport * pt_;

				xdata::String stateName_;
				xdata::String iaName_;
				xdata::String recvPoolName_;
				xdata::String sendPoolName_;
				
				xdata::String memAllocTimeout_;
				
				xdata::UnsignedLong receiverPoolSize_;
				xdata::UnsignedLong senderPoolSize_;

				xdata::Double recvPoolHighThreshold_;
				xdata::Double recvPoolLowThreshold_;

				xdata::Double sendPoolHighThreshold_;
				xdata::Double sendPoolLowThreshold_;

				// IBVLA specific configurable variables
				xdata::UnsignedInteger completionQueueSize_;
				xdata::UnsignedInteger sendQueuePairSize_;
				xdata::UnsignedInteger recvQueuePairSize_;

				xdata::UnsignedInteger maxMessageSize_; // max size that is to be sent over wire
				xdata::Boolean maxMessageSizeCheck_;

				xdata::UnsignedInteger deviceMTU_; // can be one of 256, 512, 1024, 2048, 4096, used for ib device configuration

				xdata::Boolean sendWithTimeout_;
				xdata::Boolean useRelay_;

				toolbox::fsm::AsynchronousFiniteStateMachine fsm_; // the actual state machine
                        	// RCMS state notifier
                        	xdaq2rc::RcmsStateNotifier rcmsStateNotifier_; 
                                bool retrivedRcmsStateListener_;
			protected:

		};
	}
}
#endif
