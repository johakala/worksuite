// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _ptibv_version_h_
#define _ptibv_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define PTIBV_VERSION_MAJOR 2
#define PTIBV_VERSION_MINOR 5
#define PTIBV_VERSION_PATCH 0
// If any previous versions available E.g. #define PTIBV_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef PTIBV_PREVIOUS_VERSIONS 

//
// Template macros
//
#define PTIBV_VERSION_CODE PACKAGE_VERSION_CODE(PTIBV_VERSION_MAJOR,PTIBV_VERSION_MINOR,PTIBV_VERSION_PATCH)
#ifndef PTIBV_PREVIOUS_VERSIONS
#define PTIBV_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(PTIBV_VERSION_MAJOR,PTIBV_VERSION_MINOR,PTIBV_VERSION_PATCH)
#else 
#define PTIBV_FULL_VERSION_LIST  PTIBV_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PTIBV_VERSION_MAJOR,PTIBV_VERSION_MINOR,PTIBV_VERSION_PATCH)
#endif 
namespace ptibv
{
	const std::string package = "ptibv";
	const std::string versions = PTIBV_FULL_VERSION_LIST;
	const std::string summary = "XDAQ I2O peer transport based on ibverbs";
	const std::string description = "XDAQ I2O peer transport based on ibverbs";
	const std::string authors = "Luciano Orsini, Andrea Petrucci, Andrew Forrest";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif

