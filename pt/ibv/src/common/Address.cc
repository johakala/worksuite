// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iostream>
#include <sstream>

#include "pt/ibv/Address.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"

// only for debugging inet_ntoa

pt::ibv::Address::Address (const std::string & url, const std::string& service, size_t ibPort, size_t ibPath)
	: url_(url), ibPort_(ibPort), ibPath_(ibPath)
{
	/*
	if (service_ != "i2o")
	{
		// std::string msg = "Cannot create pt::ibv::Address from url, unsupported service ";
		// msg += service;
		// XCEPT_RAISE (pt::exception::Exception, msg);
	}
	*/
	service_ = service;
}

pt::ibv::Address::~Address ()
{
}

std::string pt::ibv::Address::getService ()
{
	return service_;
}

std::string pt::ibv::Address::getProtocol ()
{
	return url_.getProtocol();
}

std::string pt::ibv::Address::toString ()
{
	return url_.toString();
}

std::string pt::ibv::Address::getURL ()
{
	return url_.toString();
}

std::string pt::ibv::Address::getHost ()
{
	return url_.getHost();
}

std::string pt::ibv::Address::getPort ()
{
	std::ostringstream o;
	if (url_.getPort() > 0) o << url_.getPort();
	return o.str();
}

size_t pt::ibv::Address::getIBPort ()
{
	return ibPort_;
}

size_t pt::ibv::Address::getIBPath ()
{
	return ibPath_;
}

unsigned short pt::ibv::Address::getPortNum ()
{
	return (url_.getPort());
}

std::string pt::ibv::Address::getPath ()
{
	std::string path = url_.getPath();
	if (path.empty())
	{
		return "/";
	}
	else
	{
		if (path[0] == '/')
		{
			return path;
		}
		else
		{
			path.insert(0, "/");
			return path;
		}
	}
}

std::string pt::ibv::Address::getServiceParameters ()
{
	return url_.getPath();
}

bool pt::ibv::Address::equals (pt::Address::Reference address)
{
	return ((this->toString() == address->toString()) && (this->getService() == address->getService()));
}
