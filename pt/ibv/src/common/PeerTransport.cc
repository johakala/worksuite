// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/ibv/PeerTransport.h"
#include "ibvla/Buffer.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "pt/exception/ReceiveFailure.h"
#include "pt/ibv/exception/InsufficientResources.h"
#include "pt/ibv/exception/ConnectionRequestFailure.h"
#include "i2o/i2o.h"
#include "xcept/tools.h"

#include "ibvla/Acceptor.h"
#include "ibvla/Allocator.h"
#include "ibvla/Connector.h"
#include "ibvla/Utils.h"
#include "ibvla/exception/QueueFull.h"
#include "ibvla/exception/InternalError.h"

#include "toolbox/net/Utils.h"
#include "toolbox/string.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "pt/ibv/Application.h"

//pt::ibv::PeerTransport::PeerTransport (xdaq::Application * parent, ibvla::Context & context, ibvla::ProtectionDomain & pd, toolbox::mem::Pool * spool, toolbox::mem::Pool * rpool, xdata::UnsignedInteger cqSize, xdata::UnsignedInteger sendQPSize, xdata::UnsignedInteger recvQPSize, xdata::UnsignedInteger maxMessageSize, xdata::UnsignedInteger deviceMTU, xdata::Boolean sendWithTimeout, xdata::Boolean useRelay) throw (pt::exception::Exception)
//	: xdaq::Object(parent), mutex_(toolbox::BSem::FULL, true), context_(context), pd_(pd), spool_(spool), rpool_(rpool), completionQueueSize_(cqSize), sendQPSize_(sendQPSize), recvQPSize_(recvQPSize), maxMessageSize_(maxMessageSize), deviceMTU_(deviceMTU), sendWithTimeout_(sendWithTimeout), useRelay_(useRelay)
pt::ibv::PeerTransport::PeerTransport (xdaq::Application * parent) throw (pt::exception::Exception)
	: xdaq::Object(parent), mutex_(toolbox::BSem::FULL, true)
{
	pt::ibv::Application * app = dynamic_cast<pt::ibv::Application *>(parent);

	std::list < ibvla::Device > devices;
	try
	{
		devices = ibvla::getDeviceList();
	}
	catch (ibvla::exception::Exception & exibv)
	{
		XCEPT_RETHROW(pt::exception::Exception, "Can not access IB devices", exibv);
	}

	if (devices.size() == 0)
	{
		ibvla::freeDeviceList (devices);
		std::stringstream msg;
		msg << "No IB devices found";
		XCEPT_RAISE(pt::exception::Exception, msg.str());
	}

	LOG4CPLUS_INFO(app->getApplicationLogger(), "Found " << devices.size() << " devices");

	ibvla::Device device;

	std::list<ibvla::Device>::iterator i;
	for (i = devices.begin(); i != devices.end(); i++)
	{
		if ((*i).getName() ==  app->iaName_.toString())
		{
			LOG4CPLUS_INFO(app->getApplicationLogger(), "Found device name = '" << (*i).getName() << "', guid = ' 0x" << std::hex << bswap_64((*i).getGUID()) << std::dec << "'");
			break;
		}
	}

	if (i == devices.end())
	{
		ibvla::freeDeviceList (devices);
		std::stringstream msg;
		msg << "Could not find device '" << app->iaName_.toString() << "', peer transport configuration failure" << std::endl;

		msg << "Available devices :" << std::endl;
		for (i = devices.begin(); i != devices.end(); i++)
		{
			msg << (*i).getName() << std::endl;
		}

		XCEPT_RAISE(pt::exception::Exception, msg.str());
	}

	context_ = ibvla::createContext((*i));

	pd_ = context_.allocateProtectionDomain();

	toolbox::net::URN urn("toolbox-mem-pool", app->sendPoolName_);

	spool_ = 0;
	try
	{
		size_t sps = app->senderPoolSize_;
		toolbox::mem::Allocator* a = new ibvla::Allocator(pd_, "toolbox-mem-allocator-ibv-sender", sps);
		spool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
		spool_->setHighThreshold((unsigned long) ((xdata::UnsignedLongT) app->senderPoolSize_ * (xdata::DoubleT) app->sendPoolHighThreshold_));
		spool_->setLowThreshold((unsigned long) ((xdata::UnsignedLongT) app->senderPoolSize_ * (xdata::DoubleT) app->sendPoolLowThreshold_));
	}
	catch (toolbox::mem::exception::Exception & e)
	{
		XCEPT_RETHROW(pt::exception::Exception, "Cannot create sender memory pool", e);
	}

	// Check for bad parameters
	if ((size_t) app->completionQueueSize_ < 1)
	{
		XCEPT_RAISE(pt::exception::Exception, "Bad configuration, completionQueueSize must be > 0");
	}

	/*
	spool_(spool);
	completionQueueSize_(cqSize);
	sendQPSize_(sendQPSize);
	recvQPSize_(recvQPSize);
	maxMessageSize_(maxMessageSize);
	deviceMTU_(deviceMTU);
	sendWithTimeout_(sendWithTimeout);
	useRelay_ (useRelay);
	*/

	// reset the counters
	for (size_t i = 0; i < MAX_ASYNC_EVENT; i++)
	{
		asyncEventCounter_[i] = 0;
	}
	for (size_t i = 0; i < MAX_WC_EVENT; i++)
	{
		workCompletionCounter_[i] = 0;
	}

	factory_ = toolbox::mem::getMemoryPoolFactory();

	ewl_ = new ibvla::EventWorkLoop("pt::ibv::eventworkloop", context_, this);

	// This block finds all the ports and then prints out information about them. We dont use any of it here.
	/*
	 ibv_device_attr d_att = context_.queryDevice();

	 uint8_t num_ports = d_att.phys_port_cnt;

	 LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), "Device has " << (unsigned int) num_ports << " ports");

	 for (int i = 1; i <= num_ports; i++)
	 {
	 struct ibv_port_attr p_att = context_.queryPort(i);
	 LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(), "Found port : lid = '0x" << std::hex << ntohs(p_att.lid) << std::dec << "'");
	 }
	 */

	// (size, user context (void*), comp_vector)
	std::cout << "cqSize == " << app->completionQueueSize_ << ", sendQPSize == " << app->sendQueuePairSize_ << ", recvQPSize == " << app->recvQueuePairSize_ << ", maxMessageSize == " << app->maxMessageSize_ << ", MTU == " << app->deviceMTU_ << std::endl;
	cqs_ = context_.createCompletionQueue(app->completionQueueSize_, 0, 0);
	cqr_ = context_.createCompletionQueue(1, 0, 0);

	cwls_ = new ibvla::CompletionWorkLoop("pt::ibv::completionworkloops", cqs_, this);
	//cwlr_ = new ibvla::CompletionWorkLoop("pt::ibv::completionworkloopr", cqr_, this);

	//acceptor_ = new ibvla::Acceptor("pt::ibv::acceptor", this, deviceMTU_);

	outstandingRequests_ = 0;
	totalSent_ = 0;
	totalRecv_ = 0;

	errors_ = 0;
	events_ = 0;

	rwl_ = 0;
	if (app->useRelay_)
	{
		rwl_ = new pt::ibv::RelayWorkLoop("pt::ibv::relayworkloop", (xdata::UnsignedIntegerT) app->completionQueueSize_);
	}

	cwlr_ = 0;
	cwls_ = 0;
}

pt::ibv::PeerTransport::~PeerTransport ()
{
}

pt::Messenger::Reference pt::ibv::PeerTransport::getMessenger (pt::Address::Reference destination, pt::Address::Reference local) throw (pt::exception::UnknownProtocolOrService)
{
	pt::ibv::Application * app = dynamic_cast<pt::ibv::Application *>(this->getOwnerApplication());

	if ((destination->getService() == "i2o") && (local->getService() == "i2o") && (destination->getProtocol() == "ibv") && (local->getProtocol() == "ibv"))
	{
		if (local->equals(destination))
		{
			// Creating a local messenger (within same logical host == executive) should not be supported.
			std::string msg = "IBV protocol communication within the same XDAQ process is not supported. Use the fifo transport for local communication.";
			XCEPT_RAISE(pt::exception::UnknownProtocolOrService, msg);
		}
		else
		{
			mutex_.take();

			pt::ibv::Address & destAddr = dynamic_cast<pt::ibv::Address&>(*destination);

			for (std::list<pt::Messenger::Reference>::iterator mi = messengers_.begin(); mi != messengers_.end(); mi++)
			{
				pt::Messenger::Reference refcnt = *mi;
				pt::ibv::I2OMessenger * messenger = dynamic_cast<pt::ibv::I2OMessenger*>(&(*refcnt));
				// this is not precise enough, it fails if you have two processes on the same machine
				//if ( messenger->getDestinationAddress()->toString() == destination->toString() )

				pt::ibv::Address & currentAddr = dynamic_cast<pt::ibv::Address&>(*(messenger->getDestinationAddress()));

				if ((currentAddr.getHost() == destAddr.getHost()) && (currentAddr.getPort() == destAddr.getPort()))
				{
					LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Messenger already created then use cached");

					pt::Messenger::Reference mr;
					mr = *mi;

					mutex_.give();

					return mr;
				}
			}

			// create remote messenger

			pt::ibv::Address & daddress = dynamic_cast<pt::ibv::Address &>(*(destination));
			std::string port = daddress.getPort();
			std::string hostname = daddress.getHost();

			std::string IPNumber = inet_ntoa(toolbox::net::getSocketAddressByName(hostname, (uint16_t)(toolbox::toUnsignedLong(port))).sin_addr);

			ibvla::QueuePair qp = pd_.createQueuePair(cqs_, cqr_, app->sendQueuePairSize_, 0, new struct qpInfo(IPNumber, hostname, port));
			pt::ibv::I2OMessenger* m = new pt::ibv::I2OMessenger(this, qp, destination, local);

			pt::Messenger::Reference mr(m);
			messengers_.push_back(mr); // we remember any created messenger

			mutex_.give();

			return mr;
		}
	}
	else
	{
		std::string msg = "Cannot handle protocol service combination, destination protocol was :";
		msg += destination->getProtocol();
		msg += " destination service was:";
		msg += destination->getService();
		msg += " while local protocol was:";
		msg += local->getProtocol();
		msg += "and local service was:";
		msg += local->getService();
		XCEPT_RAISE(pt::exception::UnknownProtocolOrService, msg);
	}
}

pt::TransportType pt::ibv::PeerTransport::getType ()
{
	return pt::SenderReceiver;
}

// The FIFO transport does not support any addresses. It is always locally available
//
pt::Address::Reference pt::ibv::PeerTransport::createAddress (const std::string& url, const std::string& service) throw (pt::exception::InvalidAddress)
{
	return pt::Address::Reference(new pt::ibv::Address(url, service, 1, 0)); // 1 == default IB port number, 0 = default src_path_bits
}

pt::Address::Reference pt::ibv::PeerTransport::createAddress (std::map<std::string, std::string, std::less<std::string> >& address) throw (pt::exception::InvalidAddress)
{
	std::string protocol = address["protocol"];

	std::string ibport = address["ibport"];
	if (ibport == "")
	{
		ibport = "1";
	}

	std::string path = address["path"];
	if (path == "")
	{
		path = "0";
	}

	if (protocol == "ibv")
	{
		std::string url = protocol;

		XCEPT_ASSERT(address["hostname"] != "", pt::exception::InvalidAddress, "Cannot create address, hostname not specified");
		XCEPT_ASSERT(address["port"] != "", pt::exception::InvalidAddress, "Cannot create address, port number not specified");

		url += "://";
		url += address["hostname"];
		url += ":";
		url += address["port"];

		std::string service = address["service"];
		if (service != "")
		{
			if (!this->isServiceSupported(service))
			{
				std::string msg = "Cannot create address, specified service for protocol ";
				msg += protocol;
				msg += " not supported: ";
				msg += service;
				XCEPT_RAISE(pt::exception::InvalidAddress, msg);
			}
		}

		//return this->createAddress(url, service, ibport);
		size_t ibPortNum = toolbox::toUnsignedLong(ibport);
		size_t ibPath = toolbox::toUnsignedLong(path);
		return pt::Address::Reference(new pt::ibv::Address(url, service, ibPortNum, ibPath));
	}
	else
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}
}

std::string pt::ibv::PeerTransport::getProtocol ()
{
	return "ibv";
}

void pt::ibv::PeerTransport::post (toolbox::mem::Reference * reference, ibvla::QueuePair & qp, toolbox::exception::HandlerSignature * handler, void * context) throw (pt::ibv::exception::InsufficientResources, pt::ibv::exception::InternalError)
{
	if (reference == 0)
	{
		XCEPT_RAISE(pt::ibv::exception::InternalError, "Failed to post send, null reference");
	}
	if (reference->getNextReference() != 0)
	{
		XCEPT_RAISE(pt::ibv::exception::InternalError, "Failed to post send, reference chains not supported");
	}


	size_t size = reference->getDataSize();

	pt::ibv::Application * app = dynamic_cast<pt::ibv::Application *>(this->getOwnerApplication());

	if ( app->maxMessageSizeCheck_ )
	{
		if (size > app->maxMessageSize_)
		{
			std::stringstream ss;
			ss << "Failed to post send, message size '" << size << "' greater than max allowed (" << app->maxMessageSize_ << ")";
			XCEPT_RAISE(pt::ibv::exception::InternalError, ss.str());
		}
	}

	PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) reference->getDataLocation();

	if (size == (unsigned long) (frame->MessageSize << 2))
	{
		ibvla::Buffer * buffer = 0;
		try
		{
			buffer = dynamic_cast<ibvla::Buffer*>(reference->getBuffer());
			if (buffer == 0)
			{
				XCEPT_RAISE(pt::ibv::exception::InternalError, "Failed to cast, invalid memory allocator/pool, expected ibvla memory pool");
			}
		}
		catch (std::bad_cast & e)
		{
			XCEPT_RAISE(pt::ibv::exception::InternalError, e.what());
		}

		memset(&(buffer->send_wr), 0, sizeof(buffer->send_wr));

		buffer->op_sge_list = buffer->buffer_sge_list;
		buffer->op_sge_list.addr = (uint64_t) reference->getDataLocation();
		buffer->op_sge_list.length = size;

		buffer->send_wr.wr_id = (uint64_t) reference; // cookie
		buffer->send_wr.sg_list = &(buffer->op_sge_list);
		buffer->send_wr.num_sge = 1;
		buffer->send_wr.opcode = IBV_WR_SEND;
		buffer->send_wr.send_flags = IBV_SEND_SIGNALED;
		buffer->send_wr.next = 0;
		buffer->opcode = ibvla::SEND_OP;
		buffer->qp_ = qp;

		try
		{
			//std::cout << "Sending message size == " << reference->getDataSize() << ", buffer->sge_list.length == " << buffer->op_sge_list.length << std::endl;
			qp.postSend(buffer->send_wr);
		}
		catch (ibvla::exception::QueueFull & e)
		{
			std::stringstream ss;
			ss << "Failed to post message for sending";
			XCEPT_RETHROW(pt::ibv::exception::InsufficientResources, ss.str(), e);
		}
		catch (ibvla::exception::InternalError & e)
		{
			std::stringstream ss;
			ss << "Failed to post message for sending";
			XCEPT_RETHROW(pt::ibv::exception::InternalError, ss.str(), e);
		}

		/*
		 size_t retry = 0;
		 size_t maxNumberOfRetry = 10;
		 while (retry <= maxNumberOfRetry)
		 {
		 try
		 {
		 qp.postSend(buffer->send_wr);
		 break;
		 }

		 catch (ibvla::exception::QueueFull & e)
		 {
		 //std::cout << "Failed to post, queuesize = " << qpSize_ << ", outstanding = " << outstandingRequests_ << ", tries = " << retry << std::endl;
		 if (retry == maxNumberOfRetry)
		 {
		 std::stringstream ss;
		 ss << "Failed to post message for sending, tried " << maxNumberOfRetry << " times";
		 XCEPT_RETHROW(pt::exception::Exception, ss.str(), e);
		 }
		 retry++;
		 ::usleep(1000);
		 }
		 catch (ibvla::exception::InternalError & e)
		 {
		 std::stringstream ss;
		 ss << "Failed to post message for sending";
		 XCEPT_RETHROW(pt::exception::Exception, ss.str(), e);
		 }
		 }

		 if (retry > 0) std::cout << "loss of time,  retried " << retry << " times" << std::endl;
		 */

		outstandingRequests_++;
	}
	else
	{
		std::string msg = toolbox::toString("Frame size %d and Reference data size %d  do not match (frame dropped)", frame->MessageSize << 2, size);
		//reference->release();
		XCEPT_RAISE(pt::ibv::exception::InternalError, msg);
	}
}

void pt::ibv::PeerTransport::config (pt::Address::Reference address) throw (pt::exception::Exception)
{
	pt::ibv::Application * app = dynamic_cast<pt::ibv::Application *>(this->getOwnerApplication());

	pt::ibv::Address & a = dynamic_cast<pt::ibv::Address &>(*address);

	toolbox::mem::Pool * rpool = 0;
	std::stringstream poolName;
	poolName << app->recvPoolName_.toString() << "-" << a.getPort();
	toolbox::net::URN rurn("toolbox-mem-pool", poolName.str());
	try
	{
		std::stringstream allocatorName;
		allocatorName << "toolbox-mem-allocator-ibv-receiver-" << a.getPort();

		toolbox::mem::Allocator* allocator = new ibvla::Allocator(pd_, allocatorName.str(), app->receiverPoolSize_);

		rpool = toolbox::mem::getMemoryPoolFactory()->createPool(rurn, allocator);
		rpool->setHighThreshold((unsigned long) ((xdata::UnsignedLongT) app->receiverPoolSize_ * (xdata::DoubleT) app->recvPoolHighThreshold_));
		rpool->setLowThreshold((unsigned long) ((xdata::UnsignedLongT) app->receiverPoolSize_ * (xdata::DoubleT) app->recvPoolLowThreshold_));
	}
	catch (toolbox::mem::exception::Exception & e)
	{
		XCEPT_RETHROW(pt::exception::Exception, "Cannot create receiver memory pool", e);
	}

	try
	{
		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "going to listen on host:port " << a.getHost() << ":" << a.getPortNum());
		ibvla::CompletionQueue cqr = context_.createCompletionQueue(app->completionQueueSize_, 0, 0);
		ibvla::CompletionWorkLoop * cwlr = new ibvla::CompletionWorkLoop("pt::ibv::completionworkloopr-" + a.getPort(), cqr, this);
		cwlr_ = cwlr;
		ibvla::Acceptor * acceptor = new ibvla::Acceptor("pt::ibv::acceptor-" + a.getPort(), this, app->deviceMTU_, cqr, rpool, a.getIBPort(), a.getIBPath());

		//acceptor_->listen("10.177.33.93", "3000");
		acceptor->listen(a.getHost(), a.getPort());
	}
	catch (pt::ibv::exception::Exception &e)
	{
		XCEPT_RETHROW(pt::exception::Exception, "Failed to configure receiver port on IA", e);
	}

}

ibvla::CompletionWorkLoop * pt::ibv::PeerTransport::getCompletionWorLoopReceiver()
{
	return cwlr_;
}

ibvla::CompletionWorkLoop * pt::ibv::PeerTransport::getCompletionWorLoopSender()
{
	return cwls_;

}

std::vector<std::string> pt::ibv::PeerTransport::getSupportedServices ()
{
	std::vector < std::string > s;
	s.push_back("i2o");
	return s;
}

bool pt::ibv::PeerTransport::isServiceSupported (const std::string& service)
{
	if (service == "i2o")
		return true;
	else
		return false;
}

void pt::ibv::PeerTransport::addServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{
	pt::ibv::Application * app = dynamic_cast<pt::ibv::Application *>(this->getOwnerApplication());

	pt::PeerTransportReceiver::addServiceListener(listener);
	if (listener->getService() == "i2o")
	{
		if (app->useRelay_)
		{
			rwl_->addListener(dynamic_cast<i2o::Listener *>(listener));
		}
		listener_ = dynamic_cast<i2o::Listener *>(listener);
	}
}

void pt::ibv::PeerTransport::removeServiceListener (pt::Listener* listener) throw (pt::exception::Exception)
{
	pt::PeerTransportReceiver::removeServiceListener(listener);
	if (listener->getService() == "i2o")
	{
		listener_ = 0;
	}
}

void pt::ibv::PeerTransport::removeAllServiceListeners ()
{
	pt::PeerTransportReceiver::removeAllServiceListeners();
	listener_ = 0;
}

void pt::ibv::PeerTransport::connect (ibvla::QueuePair & qp, const std::string & host, size_t port, size_t path) throw (pt::exception::Exception)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "schedule connection");

	pt::ibv::Application * app = dynamic_cast<pt::ibv::Application *>(this->getOwnerApplication());

	try
	{
		std::stringstream p;
		p << port;
		ibvla::Connector conn(app->deviceMTU_, app->sendWithTimeout_);
		conn.connect(qp, host, p.str(), path);
	}
	catch (ibvla::exception::Exception & e)
	{
		this->moveQueuePairIntoError(qp);
		XCEPT_RETHROW(pt::exception::Exception, "Failed to submit connect", e);
	}

	try
	{
		struct ibv_qp_attr attr;
		struct ibv_qp_init_attr init_attr;

		qp.query(0, attr, init_attr);

		((struct qpInfo *) qp.getContext())->remote_qpn = attr.dest_qp_num;
	}
	catch (ibvla::exception::Exception & e)
	{
		this->moveQueuePairIntoError(qp);
		XCEPT_RETHROW(pt::exception::Exception, "Failed to query QP", e);
	}
}

/**
 * Handle events as they are received.
 */
void pt::ibv::PeerTransport::connectionRequest (ibvla::ConnectionRequest id)
{
	pt::ibv::Application * app = dynamic_cast<pt::ibv::Application *>(this->getOwnerApplication());

	ibvla::QueuePair qp;
	try
	{
		qp = pd_.createQueuePair(cqs_, id.acceptor_->cqr_, 0, app->recvQueuePairSize_, new struct qpInfo(id.ip_, id.hostname_, ""));
		//qp = factory.getQueuePair(id.stuff); // this is a thought for the future

		// init qp
		ibv_qp_attr qpattr;
		memset(&qpattr, 0, sizeof(qpattr));

		qpattr.qp_state = IBV_QPS_INIT;
		qpattr.pkey_index = 0;
		qpattr.port_num = id.acceptor_->ibPort_; // physical port number
		qpattr.qp_access_flags = 0;

		qp.modify(qpattr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
	}
	catch (ibvla::exception::Exception & e)
	{
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Rejecting connection due to failure to create QP");
		id.acceptor_->reject(id);

		XCEPT_DECLARE_NESTED(pt::ibv::exception::ConnectionRequestFailure, ex, "Caught exception while accepting", e);
		this->getOwnerApplication()->notifyQualified("fatal", ex);
	}

	try
	{
		if (app->recvQueuePairSize_ == (unsigned) 0)
		{
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "recvQueuePairSize_ is set to 0, no buffers posted to receive, client may hang");
		}

		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Posting " << app->recvQueuePairSize_ << " receive buffers of " << app->maxMessageSize_ << "bytes");
		//If allocation time for memory exceeds the timeout, then there is a problem with the system and someone needs to be notified.
		toolbox::TimeVal t1 = toolbox::TimeVal::gettimeofday();

		for (size_t i = 0; i < app->recvQueuePairSize_; i++)
		{
			this->postReceiveBuffer(id.acceptor_->rpool_, qp, app->maxMessageSize_);
		}
		toolbox::TimeVal t2 = toolbox::TimeVal::gettimeofday();
		toolbox::TimeInterval time = t2 - t1;
		
		toolbox::TimeInterval timeout;
		timeout.fromString(app->memAllocTimeout_);
		if (time >= timeout)
		{
			std::stringstream msg;
			msg << "Allocation time for recieve buffers exceeded timeout: " << app->recvQueuePairSize_ << " times " << app->maxMessageSize_ << " byte buffers took " << time.toString();
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, msg.str());
			this->getOwnerApplication()->notifyQualified("warn", e); 
		}
	}
	catch (ibvla::exception::Exception & e)
	{
		moveQueuePairIntoError(qp);

		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Rejecting connection due to failure to post receive buffers");
		id.acceptor_->reject(id);

		XCEPT_DECLARE_NESTED(pt::ibv::exception::ConnectionRequestFailure, ex, "Caught exception while accepting", e);
		this->getOwnerApplication()->notifyQualified("fatal", ex);
	}
	try
	{
		id.acceptor_->accept(id, qp);
	}
	catch (ibvla::exception::Exception & e)
	{
		moveQueuePairIntoError(qp);

		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Rejecting connection due to failure to accept");
		id.acceptor_->reject(id);

		XCEPT_DECLARE_NESTED(pt::ibv::exception::ConnectionRequestFailure, ex, "Caught exception while accepting", e);
		this->getOwnerApplication()->notifyQualified("fatal", ex);
	}

	try
	{
		struct ibv_qp_attr attr;
		struct ibv_qp_init_attr init_attr;

		qp.query(0, attr, init_attr);

		((struct qpInfo *) qp.getContext())->remote_qpn = attr.dest_qp_num;
	}
	catch (ibvla::exception::Exception & e)
	{
		this->moveQueuePairIntoError(qp);
		XCEPT_RETHROW(pt::exception::Exception, "Failed to query QP", e);
	}

	acceptedQP_.push_back(qp);
}

void pt::ibv::PeerTransport::handleEvent (struct ibv_async_event * event)
{
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Asynchronous event : " << ibvla::toString(event));

// To extract things from the event
//ibvla::QueuePair qp (pd_, event->element.qp);
//ibvla::CompletionQueue cq (context_, event->element.cq);

	events_++;

	if ((size_t) event->event_type < MAX_ASYNC_EVENT)
	{
		asyncEventCounter_[event->event_type]++;
	}
	else
	{
		std::stringstream ss;
		ss << "Event index overflow : " << event->event_type;
		;
		LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
		this->getOwnerApplication()->notifyQualified("fatal", e);
	}

	switch (event->event_type)
	{
		/* QP events */
		case IBV_EVENT_QP_FATAL:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_QP_FATAL : Failure to generate completions for a QP";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			// Error generating completions or the associated CQ is in error. Move QP into error.

			ibvla::QueuePair qp(pd_, event->element.qp);
			this->moveQueuePairIntoError(qp); // no guarantee this will work as CQ may be in error

			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_EVENT_QP_REQ_ERR:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_QP_REQ_ERR : Transport error violation";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			// QP is automatically moved into error state. The cause of the error is bad behavior on the remote side.
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_EVENT_QP_ACCESS_ERR:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_QP_ACCESS_ERR : Probable R_Key violation";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			// this is probably a programming error
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_EVENT_COMM_EST:
		{
			//LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "IBV_EVENT_COMM_EST");
			// This event is received when a QP that is in a "Ready to Receive" state receives its FIRST message.
			// We don't care.
		}
			break;
		case IBV_EVENT_SQ_DRAINED:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_SQ_DRAINED : Drain complete";
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		}
			break;
		case IBV_EVENT_PATH_MIG:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_PATH_MIG : We do not use path migration so if you are reading this, something has gone wrong. Very, very wrong.";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_EVENT_PATH_MIG_ERR:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_PATH_MIG_ERR : We do not use path migration so if you are reading this, something has gone wrong. Very, very wrong.";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_EVENT_QP_LAST_WQE_REACHED:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_QP_LAST_WQE_REACHED : We do not use SRQ's, so this event should not be able to happen";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;

			/* CQ events */
		case IBV_EVENT_CQ_ERR:
		{
			// This event is triggered when a Completion Queue (CQ) overrun occurs or (rare condition) due to a
			// protection error. When this happens, there are no guarantees that completions from the CQ can be
			// pulled. All of the QPs associated with this CQ either in the Read or Send Queue will also get the
			// IBV_EVENT_QP_FATAL event. When this event occurs, the best course of action is for the user
			// to destroy and recreate the resources
			std::stringstream ss;
			ss << "IBV_EVENT_CQ_ERR : Completion Queue fatal, all resources should be destroyed and recreated";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;

			/* SRQ events */
		case IBV_EVENT_SRQ_ERR:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_SRQ_ERR : We do not use SRQ's";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_EVENT_SRQ_LIMIT_REACHED:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_SRQ_LIMIT_REACHED : We do not use SRQ's";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;

			/* Port events */
		case IBV_EVENT_PORT_ACTIVE:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_PORT_ACTIVE : A port has just become active";
			LOG4CPLUS_INFO(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		}
			break;
		case IBV_EVENT_PORT_ERR:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_PORT_ERR : A port has become inactive. QP's using this port may timeout";
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		}
			break;
		case IBV_EVENT_LID_CHANGE:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_LID_CHANGE : Subnet manager has changed the LID for a port. Existing connections may be compromised";
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		}
			break;
		case IBV_EVENT_PKEY_CHANGE:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_PKEY_CHANGE : Subnet manager has changed the P_Key table for a port. Existing connections may be compromised";
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		}
			break;
		case IBV_EVENT_GID_CHANGE:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_GID_CHANGE : Subnet manager has changed the GID for a port. Existing connections may be compromised";
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		}
			break;
		case IBV_EVENT_SM_CHANGE:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_SM_CHANGE : The subnet manager has changed. Existing connections may be compromised";
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		}
			break;
		case IBV_EVENT_CLIENT_REREGISTER:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_CLIENT_REREGISTER : Subnet manager has requested client re-registration for a port";
			LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		}
			break;

			/* RDMA device events */
		case IBV_EVENT_DEVICE_FATAL:
		{
			std::stringstream ss;
			ss << "IBV_EVENT_DEVICE_FATAL : CATASTROPHIC. Port and/or channel adapter are unusable. Behavior is now undefined. Recommend terminating process.";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;

		default:
		{
			std::stringstream ss;
			ss << "Unknown asynchronous event received";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
	}
}

void pt::ibv::PeerTransport::cleanUpWorkRequestError (struct ibv_wc * wc) throw (pt::ibv::exception::Exception)
{
	try
	{
		toolbox::mem::Reference * inref = reinterpret_cast<toolbox::mem::Reference *>(wc->wr_id);
		ibvla::Buffer * inbuffer = dynamic_cast<ibvla::Buffer*>(inref->getBuffer());
		ibvla::QueuePair qp = inbuffer->qp_;

		inref->release();

		this->moveQueuePairIntoError(qp);
	}
	catch (const std::bad_cast& e)
	{
		XCEPT_RAISE(pt::ibv::exception::Exception, "Failed to cast during work request error cleanup");
	}
}

void pt::ibv::PeerTransport::handleWorkRequestError (struct ibv_wc * wc)
{
	if ((size_t) wc->status < MAX_WC_EVENT)
	{
		workCompletionCounter_[wc->status]++;
	}
	else
	{
		std::stringstream ss;
		ss << "Work-Completion status index overflow : " << wc->status;
		LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		XCEPT_DECLARE(pt::ibv::exception::InternalError, ex, ss.str());
		this->getOwnerApplication()->notifyQualified("fatal", ex);
	}

	switch (wc->status)
	{
		case IBV_WC_LOC_LEN_ERR: // receiver side
		{
			std::stringstream ss;
			ss << "IBV_WC_LOC_LEN_ERR : Posted buffer is not big enough for the incoming message";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_REM_INV_REQ_ERR: // sender side
		{
			std::stringstream ss;
			ss << "IBV_WC_REM_INV_REQ_ERR : Receiver did not provide a large enough buffer for the attempted send";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_LOC_QP_OP_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_LOC_QP_OP_ERR : QP error, connection closed and memory reclaimed";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_LOC_EEC_OP_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_LOC_EEC_OP_ERR : Local error, could not execute incoming request packet";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::InternalError, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_WC_LOC_PROT_ERR:
		{
			std::stringstream ss;
			ss << "Block size is smaller than the data to be sent (see 6.2.5 IBV_WC_LOC_PROT_ERR)";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_WR_FLUSH_ERR:
		{
			/*
			 This event is generated when an invalid remote error is thrown when the responder detects an
			 invalid request. It may be that the operation is not supported by the request queue or there is insufficient
			 buffer space to receive the request.
			 */
			// KNOWN CAUSES :
			// 1. buffer->sge_list.length != remote buffer->sge_list.length
			// 2. work requested posted to a queue pair for sending with inactive remote qp and a work request has already returned a IBV_WC_RETRY_EXC_ERR error
			std::stringstream ss;
			ss << "Work request flushed from QP with error code";
			LOG4CPLUS_TRACE(this->getOwnerApplication()->getApplicationLogger(), ss.str());

			// need to recycle the buffer is there is one
			try
			{
				toolbox::mem::Reference * inref = reinterpret_cast<toolbox::mem::Reference *>(wc->wr_id);
				ibvla::Buffer * inbuffer = dynamic_cast<ibvla::Buffer*>(inref->getBuffer());
				ibvla::QueuePair qp = inbuffer->qp_;

				if (inbuffer->opcode == ibvla::DESTROY_OP)
				{
					std::stringstream sd;
					sd << "Received DESTROY_OP, destroying QP #" << qp.qp_->qp_num;
					pd_.destroyQueuePair(qp);
					LOG4CPLUS_INFO(this->getOwnerApplication()->getApplicationLogger(), sd.str());
				}
				else
				{
					// print qp information
					std::stringstream sd;
					sd << "Work request flushed from QP with error code on QP #" << qp;
					LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), sd.str());
					// the qp should probably be moved into an error state?
				}

				inref->release();
			}
			catch (const std::bad_cast& e)
			{
				// no buffer, or a really super bad fatal error, that is so bad we cannot detect it and dont know to crash
				std::stringstream es;
				es << "Could not handle flush with error : Failed cast";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE(pt::ibv::exception::InternalError, ex, es.str());
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
		}
			break;
		case IBV_WC_MW_BIND_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_MW_BIND_ERR : Memory management error, check access permissions";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::InternalError, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_WC_BAD_RESP_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_BAD_RESP_ERR : Unexpected transport layer OPCODE, closing connection";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_LOC_ACCESS_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_LOC_ACCESS_ERR : RDMA write error, we do not use RDMA write operation";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::InternalError, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_WC_REM_ACCESS_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_REM_ACCESS_ERR : An RDMA or atomic operation was attempted, and generated a protection error";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::InternalError, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_WC_REM_OP_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_REM_OP_ERR : A remote error occurred, closing connection";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_RETRY_EXC_ERR:
		{
			/*
			 This event is generated when a sender is unable to receive feedback from the receiver. This means
			 that either the receiver just never ACKs sender messages in a specified time period, or it has been
			 disconnected or it is in a bad state which prevents it from responding.
			 */
			// For us, probably remote application has terminated
			std::stringstream ss;
			ss << "IBV_WC_RETRY_EXC_ERR : Send timeout (no response), closing connection";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_RNR_RETRY_EXC_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_RNR_RETRY_EXC_ERR : Send timeout (ReceiverNotReady NoAcK retry count exceeded), receiver should provide more receive buffers, closing connection";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_LOC_RDD_VIOL_ERR:
		{
			// RDD == "Reliable Datagram Domain"
			// EEC === Each end of the RDC
			// RDC == "Reliable Datagram Channel"
			std::stringstream ss;
			ss << "IBV_WC_LOC_RDD_VIOL_ERR : EEC error, we do not use datagram services";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::InternalError, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_WC_REM_INV_RD_REQ_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_REM_INV_RD_REQ_ERR : Invalid RD message, we do not use datagram services";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::InternalError, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_WC_REM_ABORT_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_REM_ABORT_ERR : Remote error, closing connection";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_INV_EECN_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_INV_EECN_ERR : Invalid 'End to End Context Number', we do not use datagram services";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::InternalError, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_WC_INV_EEC_STATE_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_INV_EEC_STATE_ERR : EEC error, we do not use datagram services";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::InternalError, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_WC_FATAL_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_FATAL_ERR : CATASTROPHIC transport error, restart the device driver or reboot machine";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		case IBV_WC_RESP_TIMEOUT_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_RESP_TIMEOUT_ERR : Response timeout, receiver is not ready to process incoming requests, closing connection";
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			try
			{
				this->cleanUpWorkRequestError(wc);
			}
			catch (pt::ibv::exception::Exception & e)
			{
				std::stringstream es;
				es << "Could not recover from WC error";
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), es.str());
				XCEPT_DECLARE_NESTED(pt::ibv::exception::InternalError, ex, es.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
			}
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("error", e);
		}
			break;
		case IBV_WC_GENERAL_ERR:
		{
			std::stringstream ss;
			ss << "IBV_WC_GENERAL_ERR : Unknown transport error, treating as CATASTROPHIC";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
		default:
		{
			std::stringstream ss;
			ss << "Could not match error to list of known errors (" << ibvla::toString(wc) << "), treating as CATASTROPHIC";
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			XCEPT_DECLARE(pt::ibv::exception::Exception, e, ss.str());
			this->getOwnerApplication()->notifyQualified("fatal", e);
		}
			break;
	}
	return;
}

void pt::ibv::PeerTransport::handleWorkRequest (struct ibv_wc * wc)
{
	if (wc->status != IBV_WC_SUCCESS)
	{
		handleWorkRequestError(wc);
		return;
	}

	pt::ibv::Application * app = dynamic_cast<pt::ibv::Application *>(this->getOwnerApplication());

	switch (wc->opcode)
	{
		/* QP events */
		case IBV_WC_SEND:
		{
			outstandingRequests_--;
			totalSent_++;
			LOG4CPLUS_TRACE(this->getOwnerApplication()->getApplicationLogger(), "Send operation complete, message number " << totalSent_);

			toolbox::mem::Reference * ref = reinterpret_cast<toolbox::mem::Reference *>(wc->wr_id);
			ibvla::Buffer * inbuffer = dynamic_cast<ibvla::Buffer*>(ref->getBuffer());
			ibvla::QueuePair qp = inbuffer->qp_;

			((struct qpInfo *) qp.getContext())->send_count++;

			ref->release();
		}
			break;
		case IBV_WC_RECV:
		{
			totalRecv_++;
			LOG4CPLUS_TRACE(this->getOwnerApplication()->getApplicationLogger(), "Receive operation complete, message number " << totalRecv_);

			toolbox::mem::Reference * inref = reinterpret_cast<toolbox::mem::Reference *>(wc->wr_id);
			ibvla::Buffer * inbuffer = dynamic_cast<ibvla::Buffer*>(inref->getBuffer());
			ibvla::QueuePair qp = inbuffer->qp_;

			toolbox::mem::Pool * pool = inbuffer->getPool();

			((struct qpInfo *) qp.getContext())->recv_count++;

			PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) inref->getDataLocation();
			inref->setDataSize(frame->MessageSize << 2);

			if (wc->byte_len != inref->getDataSize())
			{
				// error
				if (inref) inref->release();
				std::stringstream msg;
				msg << "Inconsistent size, receive size = " << wc->byte_len << ", i2o size = " << inref->getDataSize();
				XCEPT_DECLARE(pt::exception::ReceiveFailure, e, msg.str());
				this->getOwnerApplication()->notifyQualified("fatal", e);
				return;
			}

			LOG4CPLUS_TRACE(this->getOwnerApplication()->getApplicationLogger(), "Providing new buffer for receiving");
			this->postReceiveBuffer(pool, qp, app->maxMessageSize_);

			// now return inref to application
			if (listener_ != 0)
			{
				LOG4CPLUS_TRACE(this->getOwnerApplication()->getApplicationLogger(), "Dispatching received buffer to application");

				if (app->useRelay_)
				{
					rwl_->handOver(inref);
				}
				else
				{
					listener_->processIncomingMessage(inref);
				}
			}
			else
			{
				std::string msg = toolbox::toString("No listener for i2o found in PT of pt::ibv");
				//std::cout << msg << std::endl;
				if (inref) inref->release();
				XCEPT_DECLARE(pt::exception::ReceiveFailure, e, msg);
				this->getOwnerApplication()->notifyQualified("fatal", e);
				return;
			}
		}
			break;

		default:
		{
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), "Unknown work request has arrived with success flag set");
		}
			break;
	}
}

void pt::ibv::PeerTransport::postReceiveBuffer (toolbox::mem::Pool * pool, ibvla::QueuePair & qp, size_t size)
{
	toolbox::mem::Reference * ref;

	/*
	 * This will not cause deadlock, but is not adaptive. It provides a little head room for bad configurations, better to fix the configuration
	 */
	/*
	 if (rpool_->isHighThresholdExceeded())
	 {
	 size_t retries = 0;
	 struct timespec req;
	 req.tv_sec = 0;
	 req.tv_nsec = 10 * 1000000L; // 10 ms

	 while (retries < 10)
	 {
	 if (rpool_->isLowThresholdExceeded())
	 {
	 ::nanosleep(&req, (struct timespec *) NULL);
	 }
	 else
	 {
	 break;
	 }
	 retries++;
	 }
	 if (retries >= 10)
	 {
	 LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Receive pool high threshold exceeded over allowed period of time");
	 }
	 }
	 */

	try
	{
		ref = factory_->getFrame(pool, size); // make configurable
	}
	catch (toolbox::mem::exception::Exception & ex)
	{
		std::stringstream msg;
		msg << "Failed to allocate post receive buffer";
		XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
		this->getOwnerApplication()->notifyQualified("fatal", e);
		return;
	}

	ibvla::Buffer * buffer = dynamic_cast<ibvla::Buffer*>(ref->getBuffer());

	memset(&(buffer->recv_wr), 0, sizeof(buffer->recv_wr));

	buffer->op_sge_list = buffer->buffer_sge_list;

	buffer->recv_wr.wr_id = (uint64_t) ref;
	buffer->recv_wr.sg_list = &(buffer->op_sge_list);
	buffer->recv_wr.num_sge = 1;
	buffer->recv_wr.next = 0;
	buffer->opcode = ibvla::RECV_OP;
	buffer->qp_ = qp;

	try
	{
		//std::cout << "->>>> the len of the posted buffer is: " << buffer->op_sge_list.length << std::endl;

		qp.postRecv(buffer->recv_wr);
	}
	catch (pt::ibv::exception::InsufficientResources & e)
	{
		ref->release();
		std::stringstream msg;
		msg << "Failed to allocate buffer for incoming message";
		XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
		this->getOwnerApplication()->notifyQualified("fatal", ex);
		return;
	}
	catch (pt::ibv::exception::Exception & e)
	{
		ref->release();
		std::stringstream msg;
		msg << "Failed to allocate buffer for incoming message";
		XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
		this->getOwnerApplication()->notifyQualified("fatal", ex);
		return;
	}
}

void pt::ibv::PeerTransport::moveQueuePairIntoError (ibvla::QueuePair & qp)
{
	if (qp.getState() == IBV_QPS_ERR)
	{
		return;
	}

	ibv_qp_attr qpattr;
	memset(&qpattr, 0, sizeof(qpattr));
	qpattr.qp_state = IBV_QPS_ERR;

	try
	{
		qp.modify(qpattr, IBV_QP_STATE);
	}
	catch (ibvla::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "Failed to move a QueuePair into a reset state : " << qp;
		XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
		this->getOwnerApplication()->notifyQualified("fatal", ex);
		return;
	}
}

void pt::ibv::PeerTransport::resetQueuePair (ibvla::QueuePair & qp)
{
// need to determine the allowed states we can enter with. At moment, only support queues in error state, to avoid complications with draining uncompleted work requests
	if (qp.getState() != IBV_QPS_ERR)
	{
		std::stringstream msg;
		msg << "Attempted to reset a QueuePair that was not in an error state : " << qp;
		XCEPT_DECLARE(pt::exception::ReceiveFailure, ex, msg.str());
		this->getOwnerApplication()->notifyQualified("fatal", ex);
		return;
	}

	ibv_qp_attr qpattr;
	memset(&qpattr, 0, sizeof(qpattr));
	qpattr.qp_state = IBV_QPS_RESET;

	try
	{
		qp.modify(qpattr, IBV_QP_STATE);
	}
	catch (ibvla::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "Failed to move a QueuePair into a reset state : " << qp;
		XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, ex, msg.str(), e);
		this->getOwnerApplication()->notifyQualified("fatal", ex);
		return;
	}
}

std::list<pt::Messenger::Reference> pt::ibv::PeerTransport::getMessengers ()
{
	return messengers_;
}

std::list<ibvla::QueuePair> pt::ibv::PeerTransport::getAcceptedQueuePairs ()
{
	return acceptedQP_;
}
