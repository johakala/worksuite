// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest                              *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/ibv/RelayWorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include <sstream>
#include "toolbox/utils.h"

pt::ibv::RelayWorkLoop::RelayWorkLoop (const std::string & name, size_t size) throw (pt::ibv::exception::Exception)
	: name_(name), listener_(0)
{
	//std::cout << "{ test output } relay queue size is " << size  << std::endl;

	relayQueue_ = toolbox::rlist < RecvBuf > ::create("pt-ibv-rlist-relayqueue", size);

	process_ = toolbox::task::bind(this, &pt::ibv::RelayWorkLoop::process, "process");

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		XCEPT_RETHROW(pt::ibv::exception::Exception, "Failed to submitjob", e);

	}
	catch (std::exception& se)
	{
		std::stringstream msg;
		msg << "Failed to submit notification to worker thread, caught standard exception '";
		msg << se.what() << "'";
		XCEPT_RAISE(pt::ibv::exception::Exception, msg.str());
	}
	catch (...)
	{
		XCEPT_RAISE(pt::ibv::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}

}

pt::ibv::RelayWorkLoop::~RelayWorkLoop ()
{
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->cancel();
}

void pt::ibv::RelayWorkLoop::handOver (toolbox::mem::Reference* ref) throw (pt::ibv::exception::Exception)
{
	try
	{
		RecvBuf d;
		d.ref = ref;
		relayQueue_->push_back(d);
	}
	catch (toolbox::exception::RingListFull & e)
	{
		//std::cout << "{ test output } error ring list is full "<< std::endl;
		XCEPT_RETHROW(pt::ibv::exception::Exception, "Failed to relay message", e);
	}
}

void pt::ibv::RelayWorkLoop::addListener (i2o::Listener* listener)
{
	listener_ = listener;
}

bool pt::ibv::RelayWorkLoop::process (toolbox::task::WorkLoop* wl)
{
	//std::cout << "{ test output } activated relay loop... "<< std::endl;
	for (;;)
	{
		while (!relayQueue_->empty())
		{
			RecvBuf d = relayQueue_->front();
			relayQueue_->pop_front();

			if (listener_ != 0)
			{
				listener_->processIncomingMessage(d.ref);
				//std::cout << "{ test output }  reference was  " << std::hex << reference << std::dec << std::endl;
			}
			else
			{
				d.ref->release();
				std::cout << "Fatal : message received with no listener, cannot relay" << std::endl;
			}
		}
	}
	return true;
}

