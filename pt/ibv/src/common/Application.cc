// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/ibv/Application.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "xgi/Method.h"
#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"
#include "pt/PeerTransportAgent.h"

#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"

#include "xgi/framework/Method.h"

#include "pt/ibv/PeerTransport.h"
#include "ibvla/Allocator.h"
#include "ibvla/Device.h"
#include "ibvla/Utils.h"

#include "toolbox/string.h"
#include "toolbox/fsm/FailedEvent.h"

XDAQ_INSTANTIATOR_IMPL (pt::ibv::Application)

pt::ibv::Application::Application (xdaq::ApplicationStub* stub) throw (xdaq::exception::Exception)
	: xdaq::Application(stub), 
	xgi::framework::UIManager(this),
	fsm_("StateMachine"),
	rcmsStateNotifier_(getApplicationLogger(), const_cast<xdaq::ApplicationDescriptor*>(getApplicationDescriptor()),getApplicationContext())
{
	retrivedRcmsStateListener_ = false;
	this->iaName_ = "unknown";

	this->senderPoolSize_ = 0x100000;
	this->receiverPoolSize_ = 0x100000;

	this->recvPoolName_ = "ribv";
	this->sendPoolName_ = "sibv";

	this->completionQueueSize_ = 16;
	this->sendQueuePairSize_ = 16;
	this->recvQueuePairSize_ = 16;

	this->maxMessageSize_ = 4096;
	this->maxMessageSizeCheck_ = true;
	this->deviceMTU_ = 4096;

	this->recvPoolHighThreshold_ = 0.9;
	this->recvPoolLowThreshold_ = 0.7;

	this->sendPoolHighThreshold_ = 0.9;
	this->sendPoolLowThreshold_ = 0.7;

	this->sendWithTimeout_ = true;
	this->useRelay_ = false;
	this->stateName_ = "Halted";

	this->memAllocTimeout_ = "PT5S";

	stub->getDescriptor()->setAttribute("icon", "/pt/ibv/images/pt-ibv-icon.png");
	stub->getDescriptor()->setAttribute("icon16x16", "/pt/ibv/images/pt-ibv-icon.png");

	// Define general state machine
	fsm_.addState('H', "Halted",this, &pt::ibv::Application::stateChanged);
	fsm_.addState('C', "Connecting", this, &pt::ibv::Application::stateChanged);
	fsm_.addState('E', "Enabled",this, &pt::ibv::Application::stateChanged);
	fsm_.addState('S', "Halting", this, &pt::ibv::Application::stateChanged);

	fsm_.addStateTransition('H', 'C', "connect");
	fsm_.addStateTransition('C', 'E', "enable");
	fsm_.addStateTransition('E', 'S', "halt");
	fsm_.addStateTransition('S', 'H', "done");
	fsm_.addStateTransition('H', 'F', "fail");
	fsm_.addStateTransition('C', 'F', "fail");
	fsm_.addStateTransition('E', 'F', "fail");
	fsm_.addStateTransition('S', 'F', "fail");

	fsm_.setFailedStateTransitionAction( this, &pt::ibv::Application::FailAction );
	fsm_.setFailedStateTransitionChanged(this, &pt::ibv::Application::stateChanged );
	
	fsm_.setInitialState('H');
	fsm_.setStateName('F', "Failed"); // give a name to the 'F' state

	fsm_.reset();

	// Bind CGI callbacks
	xgi::framework::deferredbind(this, this, &pt::ibv::Application::Default, "Default");
	xgi::bind(this, &pt::ibv::Application::resetAsyncEventCount, "resetAsyncEventCount");
	xgi::bind(this, &pt::ibv::Application::resetWorkCompletionCount, "resetWorkCompletionCount");
	//
	// Bind SOAP callbacks
	//
	xoap::bind(this, &pt::ibv::Application::fireEvent, "Connect", XDAQ_NS_URI );	
	xoap::bind(this, &pt::ibv::Application::fireEvent, "connect", XDAQ_NS_URI );	
	xoap::bind(this, &pt::ibv::Application::fireEvent, "Halt", XDAQ_NS_URI );	
	xoap::bind(this, &pt::ibv::Application::fireEvent, "halt", XDAQ_NS_URI );	
	xoap::bind(this, &pt::ibv::Application::fireEvent, "Fail", XDAQ_NS_URI );	
	xoap::bind(this, &pt::ibv::Application::fireEvent, "fail", XDAQ_NS_URI );	

	this->getApplicationInfoSpace()->fireItemAvailable("iaName", &iaName_);
	this->getApplicationInfoSpace()->fireItemAvailable("memAllocTimeout", &memAllocTimeout_);
	this->getApplicationInfoSpace()->fireItemAvailable("receiverPoolSize", &receiverPoolSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("senderPoolSize", &senderPoolSize_);

	this->getApplicationInfoSpace()->fireItemAvailable("recvPoolName", &recvPoolName_);
	this->getApplicationInfoSpace()->fireItemAvailable("sendPoolName", &sendPoolName_);

	this->getApplicationInfoSpace()->fireItemAvailable("recvPoolHighThreshold", &recvPoolHighThreshold_);
	this->getApplicationInfoSpace()->fireItemAvailable("recvPoolLowThreshold", &recvPoolLowThreshold_);
	this->getApplicationInfoSpace()->fireItemAvailable("sendPoolHighThreshold", &sendPoolHighThreshold_);
	this->getApplicationInfoSpace()->fireItemAvailable("sendPoolLowThreshold", &sendPoolLowThreshold_);

	this->getApplicationInfoSpace()->fireItemAvailable("completionQueueSize", &completionQueueSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("sendQueuePairSize", &sendQueuePairSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("recvQueuePairSize", &recvQueuePairSize_);

	this->getApplicationInfoSpace()->fireItemAvailable("maxMessageSize", &maxMessageSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("maxMessageSizeCheck", &maxMessageSizeCheck_);
	this->getApplicationInfoSpace()->fireItemAvailable("deviceMTU", &deviceMTU_);

	this->getApplicationInfoSpace()->fireItemAvailable("sendWithTimeout", &sendWithTimeout_);
	this->getApplicationInfoSpace()->fireItemAvailable("useRelay", &useRelay_);
	this->getApplicationInfoSpace()->fireItemAvailable("stateName", &stateName_);

	getApplicationInfoSpace()->fireItemAvailable("rcmsStateListener", rcmsStateNotifier_.getRcmsStateListenerParameter());
	getApplicationInfoSpace()->fireItemAvailable("foundRcmsStateListener", rcmsStateNotifier_.getFoundRcmsStateListenerParameter());
	rcmsStateNotifier_.subscribeToChangesInRcmsStateListener(getApplicationInfoSpace());


	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

void pt::ibv::Application::FailAction (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
	toolbox::fsm::FailedEvent& fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);

	const std::string msg = "Called FailAction of Peer Transport pt::ibv\n";
	LOG4CPLUS_ERROR(getApplicationLogger(), msg << xcept::stdformat_exception_history(fe.getException()));

	// Notify the sentinel
	XCEPT_DECLARE_NESTED(pt::ibv::exception::Exception, sentinelException, msg, fe.getException());
	this->notifyQualified("error", sentinelException);

}

void pt::ibv::Application::stateChanged (toolbox::fsm::FiniteStateMachine &fsm) throw (toolbox::fsm::exception::Exception)
{
	std::string state = fsm.getStateName (fsm.getCurrentState());
	stateName_ = state;
	LOG4CPLUS_INFO (getApplicationLogger(), "New state is:" << state );
	
	if (state == "Enabled" || state == "Halted"){
		//Send the state notification to RCMS
		notifyRCMS("Changed stated to " + state);
	}
	else if (state == "Failed") {
		notifyRCMS("Changed state to " + state);
	}
	else if (state == "Connecting")
	{
		try
        	{
			// Connect action
			this->autoConnect();	
			toolbox::Event::Reference e(new toolbox::Event("enable",this));
			fsm.fireEvent(e);
        	}
        	catch (xdaq::exception::Exception& e)
        	{
                	LOG4CPLUS_ERROR (getApplicationLogger(), xcept::stdformat_exception_history(e));
        	}
	}
	else if (state == "Halting")
	{
		try
        	{
			// Halt action
			toolbox::Event::Reference e(new toolbox::Event("done",this));
			fsm.fireEvent(e);
        	}
        	catch (xdaq::exception::Exception& e)
        	{
                	LOG4CPLUS_ERROR (getApplicationLogger(), xcept::stdformat_exception_history(e));
        	}
	}

}

void pt::ibv::Application::notifyRCMS(std::string msg ) 
{

    std::string statestr = fsm_.getStateName(fsm_.getCurrentState());

    try
        {
            // if the statenotifier has not been used yet, we have to find him first
            if (!retrivedRcmsStateListener_) 
                {

  			rcmsStateNotifier_.findRcmsStateListener();
			retrivedRcmsStateListener_ = rcmsStateNotifier_.getFoundRcmsStateListenerParameter();
                }
            rcmsStateNotifier_.stateChanged( statestr, msg );
        }

    catch(xcept::Exception &e)
        {

            XCEPT_DECLARE_NESTED( xdaq::exception::Exception, top, "Cannot notify RCMS about asynchronous transition to state \"" + statestr + "\". Continueing anyway...", e);
            this->notifyQualified("error", top);

        }
}



//
// run control requests current paramater values
//
void pt::ibv::Application::actionPerformed (xdata::Event& e)
{

	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{

		pt_ = 0;

		try
		{
			pt_ = new pt::ibv::PeerTransport(this);
		}
		catch (pt::exception::Exception & e)
		{
			XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, "Cannot create peer transport", e);
			this->notifyQualified("fatal", ex);
			try
			{
				toolbox::Event::Reference input(new toolbox::Event("fail", this));
				fsm_.fireEvent(input);
			}
			catch (toolbox::fsm::exception::Exception & exception)
			{
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, "Invalid command", exception);
				this->notifyQualified("fatal", ex);
			}
			return;
		}

		pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
		pta->addPeerTransport(pt_);

		try
		{
			toolbox::Event::Reference input(new toolbox::Event("Configure", this));
			fsm_.fireEvent(input);
		}
		catch (toolbox::fsm::exception::Exception & exception)
		{
			XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, "Invalid command", exception);
			this->notifyQualified("fatal", ex);
		}
		LOG4CPLUS_INFO(this->getApplicationLogger(), "IBV configured");
	}
}
xoap::MessageReference pt::ibv::Application::fireEvent (xoap::MessageReference msg) throw (xoap::exception::Exception)
{
	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope env = part.getEnvelope();
	xoap::SOAPBody body = env.getBody();
	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();

	for (unsigned int i = 0; i < bodyList->getLength(); i++)
	{
		DOMNode* command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			std::string state = "";
			std::string commandName = toolbox::tolower(xoap::XMLCh2String(command->getLocalName()));

			std::stringstream ss;
			ss << "Received SOAP command '" << commandName << "'";
			LOG4CPLUS_INFO(this->getApplicationLogger(), ss.str());
			try {
				std::map<std::string, toolbox::fsm::State, std::less<std::string> >  transitions = fsm_.getTransitions(fsm_.getCurrentState());	
				std::map<std::string, toolbox::fsm::State, std::less<std::string> >::iterator i;
				for ( i = transitions.begin(); i != transitions.end(); i++ ){
					if ((*i).first == commandName) {
						state = fsm_.getStateName ((*i).second);
						LOG4CPLUS_INFO(getApplicationLogger(), "transitions command="+(*i).first + " " + (*i).second );
						break;
					}
				}
				if (state=="") {
					state = fsm_.getStateName(fsm_.getCurrentState());
				}				
				toolbox::Event::Reference e(new toolbox::Event(commandName,this));
				fsm_.fireEvent(e);
				
			} catch (toolbox::fsm::exception::Exception & e)
			{
				XCEPT_RETHROW(xoap::exception::Exception, "invalid command", e);
			}		
	
			xoap::MessageReference reply = xoap::createMessage();
			xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
			xoap::SOAPName responseName = envelope.createName(commandName + "Response", "xdaq", XDAQ_NS_URI);

			xoap::SOAPBodyElement responseElement = envelope.getBody().addBodyElement(responseName);
			xoap::SOAPName stateName = envelope.createName("state", "xdaq", XDAQ_NS_URI);
			xoap::SOAPElement stateElement = responseElement.addChildElement(stateName);
			xoap::SOAPName attributeName = envelope.createName("stateName", "xdaq", XDAQ_NS_URI);
			stateElement.addAttribute(attributeName, state);
			return reply;
		}
	}

	XCEPT_RAISE(xcept::Exception, "SOAP command not found");
}


void pt::ibv::Application::autoConnect ()
{
	std::vector<const xdaq::Network*> networks = this->getApplicationContext()->getNetGroup()->getNetworks();
	for (std::vector<const xdaq::Network*>::iterator n = networks.begin(); n != networks.end(); n++)
	{
		if ((*n)->getProtocol() == "ibv")
		{
			if ((*n)->isEndpointExisting(this->getApplicationDescriptor()->getContextDescriptor()))
			{
				pt::Address::Reference local = (*n)->getAddress(this->getApplicationDescriptor()->getContextDescriptor());

				std::stringstream ss;
				ss << "Found local network : " << (*n)->getName() << " - " << (*n)->getProtocol() << " - " << local->toString();

				LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());

				std::vector<xdaq::ContextDescriptor*> contexts = this->getApplicationContext()->getContextTable()->getContextDescriptors();

				for (std::vector<xdaq::ContextDescriptor*>::iterator c = contexts.begin(); c != contexts.end(); c++)
				{
					if ((*c)->getURL() != this->getApplicationDescriptor()->getContextDescriptor()->getURL())
					{
						if ((*n)->isEndpointExisting((*c)))
						{
							LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Found Endpoint matching in network " << (*n)->getName() << " for remote context " << (*c)->getURL());

							pt::Address::Reference remote;
							try
							{
								remote = (*n)->getAddress(*c);
							}
							catch (...)
							{
								// there is no matching address for this network
								continue;
							}
							if (!remote->equals(local))
							{
								pt::ibv::Address & rAddr = dynamic_cast<pt::ibv::Address&>(*remote);
								std::stringstream ss;
								ss << "Found remote network (cacheing messenger) : " << (*n)->getName() << " - " << (*n)->getProtocol() << " url: " << rAddr.toString();

								LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());

								pt::Messenger::Reference mr = pt::getPeerTransportAgent()->getMessenger(remote, local);
								pt::ibv::I2OMessenger & m = dynamic_cast<pt::ibv::I2OMessenger&>(*mr);
								try
								{
									m.postConnect();
								}
								catch (pt::ibv::exception::Exception & e)
								{
									LOG4CPLUS_ERROR(this->getApplicationLogger(), "failed to connect cached messenger");
									this->notifyQualified("error", e);
									try
									{
										toolbox::Event::Reference input(new toolbox::Event("fail", this));
										fsm_.fireEvent(input);
									}
									catch (toolbox::fsm::exception::Exception & exception)
									{
										XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, "Invalid command", exception);
										this->notifyQualified("fatal", ex);
									}
								}
							}
							std::string enabled("Enabled");
							if (fsm_.getStateName(fsm_.getCurrentState()).compare(enabled) != 0)
							{
								try
								{
									toolbox::Event::Reference input(new toolbox::Event("enable", this));
									fsm_.fireEvent(input);
								}
								catch (toolbox::fsm::exception::Exception & exception)
								{
									XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, "Invalid command", exception);
									this->notifyQualified("fatal", ex);
								}
							}
						}
						else
						{
							LOG4CPLUS_DEBUG(this->getApplicationLogger(), "No Endpoint matching in network " << (*n)->getName() << " for remote context " << (*c)->getURL());
						}
					}
				}
			}
		}
	}
}

void pt::ibv::Application::resetAsyncEventCount (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	size_t n = 0;
	try
	{
		cgicc::Cgicc cgi(in);

		cgicc::form_iterator rl = cgi.getElement("en");
		if (rl != cgi.getElements().end())
		{
			n = cgi["en"]->getIntegerValue();
		}
	}
	catch (const std::exception & e)
	{
		std::cout << "Failed to reset async event count, xgi error" << std::endl;
		return;
	}

	pt_->asyncEventCounter_[n] = 0;

	*out << "<html><head><meta http-equiv=\"refresh\" content=\"0; URL=Default\"></head></html>";
}

void pt::ibv::Application::resetWorkCompletionCount (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	size_t n = 0;
	try
	{
		cgicc::Cgicc cgi(in);

		cgicc::form_iterator rl = cgi.getElement("en");
		if (rl != cgi.getElements().end())
		{
			n = cgi["en"]->getIntegerValue();
		}
	}
	catch (const std::exception & e)
	{
		std::cout << "Failed to reset work completion count, xgi error" << std::endl;
		return;
	}

	if (n == 100)
	{
		pt_->totalSent_ = 0;
	}
	else if (n == 101)
	{
		pt_->totalRecv_ = 0;
	}
	else
	{
		pt_->workCompletionCounter_[n] = 0;
	}

	*out << "<html><head><meta http-equiv=\"refresh\" content=\"0; URL=Default\"></head></html>";
}

void pt::ibv::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	if (stateName_ != "failed")
	{
		*out << "<div class=\"xdaq-tab\" title=\"Output\">" << std::endl;
		this->SenderTabPage(out);
		*out << "</div>";

		*out << "<div class=\"xdaq-tab\" title=\"Input\">" << std::endl;
		this->ReceiverTabPage(out);
		*out << "</div>";

		*out << "<div class=\"xdaq-tab\" title=\"Settings\">" << std::endl;
		this->SettingsTabPage(out);
		*out << "</div>";

		*out << "<div class=\"xdaq-tab\" title=\"Events\">" << std::endl;
		this->EventsTabPage(out);
		*out << "</div>";
	}
	else
	{
		*out << "<div class=\"xdaq-tab\" title=\"Info\">" << std::endl;
		*out << "The IBV peer transport is in failed state and the HyperDAQ page with more detail information is not available." << std::endl;
		*out << "</div>";
	}

	*out << "</div>";
}

void pt::ibv::Application::SenderTabPage (xgi::Output * sout)
{
	std::list < pt::Messenger::Reference > messengers = pt_->getMessengers();

	*sout << cgicc::table().set("class", "xdaq-table") << std::endl;

	*sout << cgicc::caption(iaName_) << std::endl;

	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tr();
	*sout << cgicc::th("IP");
	*sout << cgicc::th("Host");
	*sout << cgicc::th("Port");
	*sout << cgicc::th("Sent");
	*sout << cgicc::th("Scheduled sends");
	*sout << cgicc::th("Local QPN");
	*sout << cgicc::th("Remote QPN");
	*sout << cgicc::th("QP State");
	*sout << cgicc::th("Connection Time");
	//*sout << cgicc::th("");
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tbody() << std::endl;

	for (std::list<pt::Messenger::Reference>::iterator i = messengers.begin(); i != messengers.end(); i++)
	{
		pt::ibv::I2OMessenger * m = dynamic_cast<pt::ibv::I2OMessenger*>(&(*(*i)));
		*sout << *m << std::endl;
	}

	*sout << cgicc::tbody();
	*sout << cgicc::table();
}

void pt::ibv::Application::ReceiverTabPage (xgi::Output * sout)
{

	*sout << cgicc::table().set("class", "xdaq-table") << std::endl;
	*sout << cgicc::caption(iaName_) << "-" << " receive completion queue " << std::endl;

	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tr();
	*sout << cgicc::th("Idle");
	*sout << cgicc::th("Actual receive");
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::thead() << std::endl;


	*sout << cgicc::tbody() << std::endl;
	*sout << cgicc::tr();
	*sout << "<td>" << pt_->getCompletionWorLoopReceiver()->emptyDispatcherQueueCounter_ << "</td>";
	*sout << "<td>" << pt_->getCompletionWorLoopReceiver()->receivedEventCounter_ << "</td>";
	*sout << cgicc::tr();
	*sout << cgicc::tbody();
	*sout << cgicc::table();


	// queue pair statistics

	std::list < ibvla::QueuePair > qps = pt_->getAcceptedQueuePairs();

	*sout << cgicc::table().set("class", "xdaq-table") << std::endl;

	*sout << cgicc::caption(iaName_) << std::endl;

	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tr();
	*sout << cgicc::th("IP");
	*sout << cgicc::th("Hostname");
	*sout << cgicc::th("Recv count");
	*sout << cgicc::th("Local QPN");
	*sout << cgicc::th("Remote QPN");
	*sout << cgicc::th("QP State");
	//*sout << cgicc::th("");
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tbody() << std::endl;

	for (std::list<ibvla::QueuePair>::iterator i = qps.begin(); i != qps.end(); i++)
	{
		struct pt::ibv::PeerTransport::qpInfo * info = ((struct pt::ibv::PeerTransport::qpInfo *) (*i).getContext());
		*sout << cgicc::tr();
		*sout << "<td>" << info->ip << "</td>"; // ip
		*sout << "<td>" << info->host << "</td>"; // hostname
		*sout << "<td>" << info->recv_count << "</td>"; //
		*sout << "<td>";
		*sout << (*i).qp_->qp_num << std::endl; // qp
		*sout << "</td>";
		*sout << "<td>";
		*sout << info->remote_qpn << std::endl; // qp
		*sout << "</td>";

		if ((*i).getState() == IBV_QPS_ERR)
		{
			*sout << "<td class=\"xdaq-red\">";
		}
		else if ((*i).getState() == IBV_QPS_INIT)
		{
			*sout << "<td class=\"xdaq-blue\">";
		}
		else
		{
			*sout << "<td>";
		}

		*sout << ibvla::stateToString(*i) << std::endl; // qp
		*sout << "</td>";

		//*sout << "<td><a href=\"destroyAcceptedQueuePair?qpn=" << (*i).qp_->qp_num << "\">Destroy</a></td>";
		//*sout << "<td><input type=\"button\" class=\"red-button\" onClick=\"parent.location='destroyAcceptedQueuePair?qpn=" << (*i).qp_->qp_num << "'\" value=\"Destroy\"></td>" << std::endl;
		*sout << cgicc::tr() << std::endl;
	}

	*sout << cgicc::tbody();
	*sout << cgicc::table();
}

void pt::ibv::Application::SettingsTabPage (xgi::Output * sout)
{
	*sout << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;

	*sout << cgicc::tbody() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Device Name" << " <span class=\"xdaq-tooltip-msg\">" << "NIC name" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << iaName_.toString() << "</td>";
	*sout << cgicc::tr() << std::endl;

	size_t realMTU = ibvla::mtu_to_enum(deviceMTU_);
	if (realMTU == 1)
		realMTU = 256;
	else if (realMTU == 2)
		realMTU = 512;
	else if (realMTU == 3)
		realMTU = 1024;
	else if (realMTU == 4)
		realMTU = 2048;
	else if (realMTU == 5)
		realMTU = 4096;
	else
		realMTU = 0;

	const int MB = 1048576.0;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "MTU" << " <span class=\"xdaq-tooltip-msg\">" << "Configured max transfer unit for the device" << "</span></th>";

	if (realMTU == 0)
	{
		*sout << "<td class=\"xdaq-red\" style=\"text-align:center;\">";
	}
	else
	{
		*sout << "<td style=\"text-align:center;\">";
	}
	*sout << realMTU << "</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Max Message Size" << " <span class=\"xdaq-tooltip-msg\">" << "Maximum message size that can be sent" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << maxMessageSize_ << "</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Completion Queue Size" << " <span class=\"xdaq-tooltip-msg\">" << "Size of the completion queues (1 for send, 1 for receive)" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << completionQueueSize_ << "</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Send Queue Size" << " <span class=\"xdaq-tooltip-msg\">" << "Maximum number of messages that can be queued to send at once" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << sendQueuePairSize_ << "</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Receive Queue Size" << " <span class=\"xdaq-tooltip-msg\">" << "Number of buffers that are posted to receive into" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << recvQueuePairSize_ << "</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Receive Queue Memory" << " <span class=\"xdaq-tooltip-msg\">" << "How much memory is taken up by the receive queue (recvQueuePairSize * maxMessageSize)" << "</span></th>";
	*sout << "<td style=\"text-align:center;\">" << ((double) (recvQueuePairSize_ * maxMessageSize_)) / MB << " MB</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Receive Pool Size" << "</th>";
	*sout << "<td style=\"text-align:center;\">" << ((double) receiverPoolSize_) / MB << " MB</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Send Pool Size" << "</th>";
	*sout << "<td style=\"text-align:center;\">" << ((double) senderPoolSize_) / MB << " MB</td>";
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<th class=\"xdaq-tooltip\">" << "Use Relay" << "</th>";
	if (useRelay_)
	{
		*sout << "<td style=\"text-align:center;\">true</td>";
	}
	else
	{
		*sout << "<td style=\"text-align:center;\">false</td>";
	}
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tbody();
	*sout << cgicc::table();
}

void pt::ibv::Application::EventsTabPage (xgi::Output * sout)
{
	*sout << "<table><tr><td style=\"vertical-align:top; padding-right: 20px;\">" << std::endl;

	//////////

	std::stringstream myurl;
	myurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN() << "/";

	*sout << cgicc::table().set("class", "xdaq-table") << std::endl;

	*sout << cgicc::caption("Asynchronous Events") << std::endl;

	*sout << cgicc::thead() << std::endl;
	*sout << cgicc::tr();
	*sout << cgicc::th("Name");
	*sout << cgicc::th("Count");
	*sout << cgicc::th("");
	*sout << cgicc::tr();
	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tbody() << std::endl;

	for (size_t i = 0; i < MAX_ASYNC_EVENT; i++)
	{
		*sout << cgicc::tr();
		*sout << "<td class=\"xdaq-tooltip\" style=\"text-align: right;\">" << ibvla::asyncEventToStringLiteral(i) << " <span class=\"xdaq-tooltip-msg\">" << ibvla::asyncEventToString(i) << "</span></td>";
		*sout << "<td>" << pt_->asyncEventCounter_[i] << "</td>";
		*sout << "<td><input type=\"button\" onClick=\"parent.location='" << myurl.str() << "resetAsyncEventCount?en=" << i << "'\" value=\"Reset\"></td>" << std::endl;

		*sout << cgicc::tr() << std::endl;
	}

	*sout << cgicc::tbody();
	*sout << cgicc::table();

	//////////

	*sout << "</td><td style=\"vertical-align:top; padding-right: 20px;\">" << std::endl;

	//////////

	*sout << cgicc::table().set("class", "xdaq-table") << std::endl;

	*sout << cgicc::caption("Work Completions") << std::endl;

	*sout << cgicc::thead() << std::endl;
	*sout << cgicc::tr();
	*sout << cgicc::th("Name");
	*sout << cgicc::th("Count");
	*sout << cgicc::th("");
	*sout << cgicc::tr();
	*sout << cgicc::thead() << std::endl;

	*sout << cgicc::tbody() << std::endl;

	// Do total sent / received first
	*sout << cgicc::tr();
	*sout << "<td style=\"text-align: right;\">Sent</td>";
	*sout << "<td>" << pt_->totalSent_ << "</td>";
	*sout << "<td><input type=\"button\" onClick=\"parent.location='" << myurl.str() << "resetWorkCompletionCount?en=100'\" value=\"Reset\"></td>" << std::endl;
	*sout << cgicc::tr() << std::endl;

	*sout << cgicc::tr();
	*sout << "<td style=\"text-align: right;\">Received</td>";
	*sout << "<td>" << pt_->totalRecv_ << "</td>";
	*sout << "<td><input type=\"button\" onClick=\"parent.location='" << myurl.str() << "resetWorkCompletionCount?en=101'\" value=\"Reset\"></td>" << std::endl;
	*sout << cgicc::tr() << std::endl;

	for (size_t i = 1; i < MAX_WC_EVENT; i++) // 0 == success
	{
		*sout << cgicc::tr();
		*sout << "<td class=\"xdaq-tooltip\" style=\"text-align: right;\">" << ibvla::workCompletionToStringLiteral(i) << " <span class=\"xdaq-tooltip-msg\">" << ibvla::workCompletionToString(i) << "</span></td>";

		if (pt_->workCompletionCounter_[i] != 0)
		{
			*sout << "<td class\"xdaq-red\">";
		}
		else
		{
			*sout << "<td>";
		}

		*sout << pt_->workCompletionCounter_[i] << "</td>";
		*sout << "<td><input type=\"button\" onClick=\"parent.location='" << myurl.str() << "resetWorkCompletionCount?en=" << i << "'\" value=\"Reset\"></td>" << std::endl;
		*sout << cgicc::tr() << std::endl;
	}

	*sout << cgicc::tbody();
	*sout << cgicc::table();

	//////////

	*sout << "</td></tr></table>";
}

