#ifndef _ptEther_h_
#define _ptEther_h_

#include "pt.h"
#include "vxioslib.h"
#include "Task.h"
#include "SmartBufPool.h"

struct etherAddr : public Addr {
	short sap;
	char host[6];
};

class ptEther: public pt {


	public:
	
	
	class EtherSendEntry: public SAP {
			public:
		EtherSendEntry(Address * address, ptEther * ptp) 
		{
			address_ =  *(etherAddr*)address;
			ptp_ = ptp;	
		}
		void put(char * buf, int len) {
			ptp_->EtherWrite(buf, len, &address_);
		}	
	
		void put (BufRef * ref, int len ) {
			ptp_->EtherWrite(ref,len, &address_);
		}
	
	
		bool pollRecv() {
			XDAQ_DEBUG (("*** Cannot call receive on SendEntry!"));
		}
	
		void waitRecvLoop()	{
			XDAQ_DEBUG (("*** Cannot call receive on SendEntry!"));
		}
		
		protected:
			etherAddr address_;
			ptEther * ptp_;
		};


	class EtherRecvEntry: public SAP {

		public:
	
		EtherRecvEntry(Address * addr, ptEther * ptp, i2oProtocolIn * pin) 
		{
			pin_ = pin;
			ptp_ = ptp;
		}

	   	inline void waitRecvLoop () {
	       int originator;
	       BufRef * ref;
	       for (;;) {
	    	   ref = ptp_->EtherRead(&originator);
	    	   pin_->in(ref,originator);
	       }
	   	}
	
	   	bool pollRecv () {
	       		int originator;
	       		BufRef * ref = ptp_->EtherPollRead(&originator);
	       		if ( ref != (BufRef*)0 ) {
				pin_->in(ref,originator);
				return true;
			} else  return false;	
	   	}

	   	void put (BufRef * ref, int len ) {
	       XDAQ_DEBUG(("*** Error: not implemented"));
	   	}
	
	   	void put (char* buf, int len ){
	       XDAQ_DEBUG(("*** Error: not implemented"));
	   	}
	
	   	protected:
	
	   	i2oProtocolIn * pin_;
	   	ptEther * ptp_;

	};

	ptEther(pta * ptap): pt(ptap) 
	{	
		pout_ = new i2oProtocolOut(ptap);
	}
		
	~ptEther() {
		int i;
		
		delete pout_;
		
		for (i=0; i < pinTable_.size(); i++) {
			delete pinTable_[i];
		}
		for (i=0; i < recvEtherTable_.size(); i++) {
			delete recvEtherTable_[i];
		}

		for (i=0; i < sendEtherTable_.size(); i++) {
			delete sendEtherTable_[i];
		}
	}
	/*
	int transportType()
	{
		return (XDAQ_TRANSPORT_ETHER);
	}
	*/
	SendObj * createSendObj(Address * local, Address * remote) {
	
		EtherSendEntry * tmpEntry = new EtherSendEntry(remote, this);
		sendEtherTable_.push_back(tmpEntry);
		
		return 	new SendObj(tmpEntry, pout_, this);
	}
	
    
	RecvObj * createRecvObj(Address * address) {
	
		i2oProtocolIn * pin = new i2oProtocolIn(ptap_);
		pinTable_.push_back(pin);
		
		pin->addOriginator(0); // this is a stub so no originators
		// to be set

		EtherRecvEntry * tmpEntry = new EtherRecvEntry(address, this, pin);

		recvEtherTable_.push_back(tmpEntry);
		RecvObj * tmpRecvObj = new RecvObj(tmpEntry , pout_);
		//if (! pollingMode_ )
		//	tmpRecvObj->activate();
		return 	tmpRecvObj;
	
	}
	Address * createAddress (DOM_Node addressNode)
	{
		etherAddr * addr = new etherAddr();
		char ether[6];
		
		string etherNumber = getNodeAttribute(addressNode, "ether");
		string sapNumber = getNodeAttribute(addressNode, "sap");
		
		etherAsciiToBinary(etherNumber,ether);
		
		memcpy((char*)addr->host,ether, 6);
		addr->sap =  atoi(sapNumber);
		return addr;
	}
	
	inline BufRef * EtherRead(int * originator) {
		return (BufRef *)0;
	}
	inline BufRef * EtherPollRead(int * originator) {
		return (BufRef *)0;
	}
	
		
	inline void EtherWrite ( BufRef * ref , int len, etherAddr * addr) {

	}
	
	inline void EtherWrite ( char * buf, int len , etherAddr * addr) {
		
	}
	
	protected:
	
	vector<EtherRecvEntry*> recvEtherTable_;
	vector<EtherSendEntry*> sendEtherTable_;
	
	i2oProtocolOut* pout_;
	vector<i2oProtocolIn*>  pinTable_;

	pta* ptap_;
	//BSem * mutex_;
	int Mtu_;

};	
#endif
