#ifndef _EtherPort_h_
#define _EtherPort_h_

#include "pt.h"
#include "vxioslib.h"
#include "SmartBufPool.h"
#include "BufRef.h"
#include <string>
#include "SmartAllocator.h"
#include "Allocator.h"

class EtherPort 
{
	public:
        
	class BufferWrapper: public Buffer 
	{
        
	public:
	
	 void * userAddr() 
	 {
                return buf_; 
         };
         
	 void * physAddr() 
	 {
                return buf_;
         };
         
	 void * kernelAddr() 
	 {
                return buf_;
         };
         
	 unsigned long size() 
	 {
                return len_;
         };  // in bytes
         
         void buffer(char * buf, unsigned long len) 
         {
                buf_ = buf; 
                len_ = len;
         }
         
         protected:
         
         unsigned long len_;
         char * buf_;
         
};


	EtherPort(string interface, unsigned int unit, unsigned int sap, etherAddr & addr)
	{
		protoName_ = toString("%s_%d_%d",interface.c_str(),unit,sap);
		handle_ = new vxetherhandle(interface.c_str(),unit,protoName_.c_str(),sap);
		addr_ = addr;	
	        wrapBufferAllocator_ = new SmartAllocator<BufferWrapper>(10000);
	}
	
	~EtherPort()
	{
		delete handle_;
	
	}

	bool loopback(char * ether) 
	{
		if ( memcmp(ether,addr_.host,6) == 0 )
			return true;
		else 
			return false;	
	}
	
	inline  BufRef * EtherRead(int * originator) 
	{
		char* bufPtr;
		BufRef * bufRef = (BufRef*)0;
		XDAQ_DEBUG(("waiting for  ethernet packet... "));
		M_BLK_ID pNetBuff = handle_->wait();
		if ( pNetBuff != NULL ) 
		{
			// if loop back skip it
			if ( loopback(&(pNetBuff->mBlkHdr.mData[6])) ) 
			{
				XDAQ_DEBUG(("received a loop back packet, skip it"));
				::netMblkClChainFree((M_BLK_ID)pNetBuff);
				return (BufRef*)0;
			}
		
			XDAQ_DEBUG(("received an ether packet ptap_: %s",protoName_.c_str()));
			*originator = 0;
			XDAQ_DEBUG(("dumping packet"));
			//dump(pNetBuff->mBlkHdr.mData, 40);
			XDAQ_DEBUG(("going to allocate bufRef"));
			bufRef = executive->frameRefAlloc();
			XDAQ_DEBUG(("going to preapre bufRef: %#x",bufRef));
			bufPtr = pNetBuff->mBlkHdr.mData + 14 + 2;
			XDAQ_DEBUG(("going to insert buffer in bufRef"));
		        BufferWrapper *  bufWrap = wrapBufferAllocator_->alloc();
                        bufWrap->buffer(bufPtr,2048);
                        bufRef->insert(bufWrap,freeHandleEther, wrapBufferAllocator_, pNetBuff, 0);
			
		}
		
		XDAQ_DEBUG(("return packet in BufRef: %#x", bufRef));
		return bufRef;
	}
	
	inline BufRef * EtherPollRead(int * originator) 
	{
		char* bufPtr;
		BufRef * bufRef = (BufRef*)0;
		M_BLK_ID pNetBuff = handle_->poll();
		if ( pNetBuff != NULL ) 
		{
			// if loop back skip it
			if ( loopback(&(pNetBuff->mBlkHdr.mData[6])) ) 
			{
				XDAQ_DEBUG(("received a loop back packet, skip it"));
				::netMblkClChainFree((M_BLK_ID)pNetBuff);
				return (BufRef*)0;
			}
			
			bufRef = executive->frameRefAlloc();
			bufPtr = pNetBuff->mBlkHdr.mData + 14 + 2;
			//dump(pNetBuff->mBlkHdr.mData, 40);
                        BufferWrapper *  bufWrap = wrapBufferAllocator_->alloc();
                        bufWrap->buffer(bufPtr,2048);
                        bufRef->insert(bufWrap,freeHandleEther, wrapBufferAllocator_, pNetBuff, 0);				
		}
		return bufRef;
	}
	/*	
	inline void EtherWrite ( BufRef * ref , int len, etherAddr * addr) 
	{
		XDAQ_DEBUG(("sending ether packet to%x:%x:%x:%x:%x:%x:",addr->host[0],addr->host[1],addr->host[2],addr->host[3],addr->host[4],addr->host[5]));
		XDAQ_DEBUG(("sending ether packet protocol:%d len:%d",addr->sap, len));
		handle_->write(addr, ref->buf_, len);
		
		executive->frameFree(ref);
	}
	*/
	
	inline void EtherWrite ( char * buf, int len , etherAddr * addr) 
	{	
		handle_->write(addr,buf, len);		
	}
	inline void EtherWrite ( char * buf, int len , etherAddr * addr, BufRef * freeRef) 
	{	
		handle_->write(addr,buf, len);	
		executive->frameFree(freeRef);	
	}
	
	string getProtoName()
	{
		return protoName_;
	}
	
	static void freeHandleEther(BufRef * ref) 
	{	
		::netMblkClChainFree((M_BLK_ID)(ref->context2()));
                SmartAllocator<BufferWrapper> * allocator =   (( SmartAllocator<BufferWrapper> *)ref->context1());
                allocator->free((BufferWrapper*)ref->buffer());       
	}

	protected:
	
	vxetherhandle * handle_;
	string protoName_;
	etherAddr addr_;
        SmartAllocator<BufferWrapper> * wrapBufferAllocator_;
};



#endif
